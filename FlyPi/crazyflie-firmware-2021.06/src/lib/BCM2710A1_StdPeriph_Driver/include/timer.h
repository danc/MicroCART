#pragma once

#include "common.h"

void timer_init();
void timer_sleep(uint32_t ms);
uint64_t timer_get_ticks();
#pragma once

#include "peripherals/gpio.h"

typedef enum _GPIOFunc {
    GFInput = 0,
    GFOutput = 1,
    GFALT0 = 4,
    GFALT1 = 5,
    GFALT2 = 6,
    GFALT3 = 7,
    GFALT4 = 3,
    GFALT5 = 2
} GPIOFunc;

void gpio_pin_set_func(uint8_t pinNumber, GPIOFunc func);

void gpio_pin_enable(uint8_t pinNumber);
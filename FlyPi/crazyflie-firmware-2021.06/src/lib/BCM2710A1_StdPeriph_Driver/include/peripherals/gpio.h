#pragma once

#include "peripherals/base.h"
#include "common.h"

struct GPIOPinData {
    uint32_t reserved;
    uint32_t data[2];
};

struct GPIORegs {
    uint32_t func_select[6];
    struct GPIOPinData output_set;
    struct GPIOPinData output_clear;
    struct GPIOPinData level;
    struct GPIOPinData ev_detect_status;
    struct GPIOPinData re_detect_enable;
    struct GPIOPinData fe_detect_enable;
    struct GPIOPinData hi_detect_enable;
    struct GPIOPinData lo_detect_enable;
    struct GPIOPinData async_re_detect;
    struct GPIOPinData async_fe_detect;

    uint32_t reserved;
    uint32_t pupd_enable;
    uint32_t pupd_enable_clocks[2];
};

#define REGS_GPIO ((struct GPIORegs *) (PBASE + 0x00200000))
#pragma once

#include "peripherals/base.h"
#include "common.h"

struct timer_regs
{
    uint32_t control_status;
    uint32_t counter_lo;
    uint32_t counter_hi;
    uint32_t compare[4];
};

#define REGS_TIMER ((struct timer_regs *)(PBASE + 0x00003000))

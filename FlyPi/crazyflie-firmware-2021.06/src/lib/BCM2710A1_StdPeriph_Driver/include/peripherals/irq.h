#pragma once

#include "peripherals/base.h"
#include "common.h"

struct arm_irq_regs
{
    uint32_t irq0_pending_0;
    uint32_t irq0_pending_1;
    uint32_t irq0_pending_2;
    uint32_t fiq_control;
    uint32_t irq0_enable_1;
    uint32_t irq0_enable_2;
    uint32_t irq0_enable_0;
    uint32_t res;
    uint32_t irq0_disable_1;
    uint32_t irq0_disable_2;
    uint32_t irq0_disable_0;
};

#define REGS_IRQ ((struct arm_irq_regs *)(PBASE + 0X0000B200))

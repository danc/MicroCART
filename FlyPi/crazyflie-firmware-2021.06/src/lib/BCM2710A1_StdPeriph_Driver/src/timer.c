#include "peripherals/timer.h"
#include "peripherals/irq.h"

void timer_init(){} //fill with timers that we care about

uint64_t timer_get_ticks(){
    uint32_t hi = REGS_TIMER->counter_hi;
    uint32_t lo = REGS_TIMER->counter_lo;

    if(hi != REGS_TIMER->counter_hi){
        hi = REGS_TIMER->counter_hi;
        lo = REGS_TIMER->counter_lo;
    }

    return ((uint64_t)hi << 32) | lo;
}

//sleep in milliseconds
void timer_sleep(uint32_t ms){
    uint64_t start = timer_get_ticks();

    while(timer_get_ticks() < start + (ms * 1000)){}
}
#include "utils.h"
#include "peripherals/irq.h"
#include "timer.h"

void enable_interrupt_controller(){
    REGS_IRQ->irq0_enable_1 = 0; //null for now but add irq regs that we care about
}

void handle_irq(){
    uint32_t irq;

    irq = REGS_IRQ->irq0_pending_1;

    while(irq){} //fill with interrupt handlers that we care about
}
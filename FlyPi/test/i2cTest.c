#include "i2c.h"
#include "common.h"
#include <stdlib.h>

//NOTE: Will need to use mmamp to test baremetal code on linux if baremetal OS is not setup yet
int main(void){
    i2c_init();

    /*
     * Motor 1: 0xFFFF
     * Motor 2: 0x7FFF
     * Motor 3: 0x3FFF
     * Motor 4: 0x00FF
     */
    uint8_t buffer[] = {0xFF, 0xFF, 0x7F, 0xFF, 0x3F, 0xFF, 0x00, 0xFF};
    switch(i2c_send(0x04, buffer, 8)){
        case(0):
            printf("Success!\n");
            break;
        case(1):
            printf("ACK Error!\n");
            break;
        case(2):
            printf("Data Loss!\n");
            break;
        case(3):
            printf("Clock Timeout\n");
            break;
        default:
            printf("Unknown Status\n");
            break;
    }
}
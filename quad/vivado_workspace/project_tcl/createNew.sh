#!/bin/bash

folder=${PWD##*/}
if [[ $folder == project_tcl ]]
then
  cd ..
fi
folder=${PWD##*/}
if [[ $folder != vivado_workspace ]]
then
  printf "wrong folder\n"
else
  mkdir -p $1/src/constrs
  cp Zybo-Z7-Master.xdc $1/src/constrs/Zybo-Z7-Master.xdc
  chmod +w $1/src/constrs/Zybo-Z7-Master.xdc
  echo vivado -nojou -nolog -mode batch -source project_tcl/zybo_blank.tcl -tclargs --project_name $1
  vivado -nojou -nolog -mode batch -source project_tcl/zybo_blank.tcl -tclargs --project_name $1
fi

/******************************************************************************
 *
 * Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
 ******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xuartps.h"
#include "sleep.h"
#include "math.h"

int main() {
	init_platform();


	volatile uint32_t* buttonsGPIO =
			(volatile uint32_t*) (XPAR_AXI_GPIO_2_BASEADDR);
	volatile uint32_t* pwmRecord =
			(volatile uint32_t*) (XPAR_PWM_RECORDER_0_S_AXI_BASEADDR);
	volatile uint32_t* pwmGenerate =
			(volatile uint32_t*) (XPAR_PWM_SIGNAL_OUT_0_S_AXI_BASEADDR);
	while (1) {
		print(
				"Please connect JB0 and JB1 with an external jumper.\r\nWhen ready, press any button.\n\r");
		while (buttonsGPIO[0] == 0)
			;
		print("Thank You :)\r\n");

		const int min_period = 1000;
		const int max_period = 5 * 1000 * 1000;
		const float period_step_factor = 1.5;
		int period;
		for (period = min_period; period < max_period; period *=
				period_step_factor) {

			pwmGenerate[1] = period / 2;
			pwmGenerate[0] = period;

			int duty_iteration;
			const int num_duty_iterations = 100;
			for (duty_iteration = 0; duty_iteration < num_duty_iterations;
					duty_iteration++) {
				pwmGenerate[1] = ((duty_iteration + 1) * pwmGenerate[0])
						/ (num_duty_iterations + 1);

				usleep(period / 20);

				int error = pwmRecord[0] - pwmGenerate[1];
				int abserror = (error < 0) ? -error : error;
				if (abserror > pwmGenerate[0] / 1000) {
					xil_printf("%5d Hz, %3d%% Duty Cycle: %d cycle error\r\n",
							(50 * 1000 * 1000 / pwmGenerate[0]),
							(100 * pwmGenerate[1]) / pwmGenerate[0], error);
				}

			}
			usleep(period / 5);
		}
	}
	cleanup_platform();
	return 0;
}

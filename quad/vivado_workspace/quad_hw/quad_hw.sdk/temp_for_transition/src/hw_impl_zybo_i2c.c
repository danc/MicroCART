#include "hw_impl_zybo.h"
#include "xiicps.h"
// System configuration registers
// (Please see Appendix B: System Level Control Registers in the Zybo TRM)
#define IIC_SYSTEM_CONTROLLER_RESET_REG_ADDR 	(0xF8000224)
#define IO_CLK_CONTROL_REG_ADDR 				(0xF800012C)

// Error code could be returned on an iic read if the watchdog timer triggers
#define IIC_RX_TIMEOUT_FAILURE (-88)
/*
// IIC0 Registers
#define IIC0_CONTROL_REG_ADDR 		(0xE0004000)
#define IIC0_STATUS_REG_ADDR 		(0xE0004004)
#define IIC0_SLAVE_ADDR_REG 		(0xE0004008)
#define IIC0_DATA_REG_ADDR 			(0xE000400C)
#define IIC0_INTR_STATUS_REG_ADDR 	(0xE0004010)
#define IIC0_TRANFER_SIZE_REG_ADDR	(0xE0004014)
#define IIC0_INTR_EN			    (0xE0004024)
#define IIC0_TIMEOUT_REG_ADDR 		(0xE000401C)
*/
//Interrupt Status Register Masks
#define ARB_LOST       (0x200)
#define RX_UNF          (0x80)
#define TX_OVF          (0x40)
#define RX_OVF          (0x20)
#define SLV_RDY         (0x10)
#define TIME_OUT        (0x08)
#define NACK      		(0x04)
#define MORE_DAT 	    (0x02)
#define TRANS_COMPLETE 	(0x01)

#define WRITE_INTR_MASK (ARB_LOST | TIME_OUT | RX_OVF | TX_OVF | NACK)
#define READ_INTR_MASK (ARB_LOST | TIME_OUT | RX_OVF | RX_UNF | NACK)

#define IO_CLK_CONTROL_REG_ADDR	(0xF800012C)
#define I2C0_CPU_1XCLKACT	(0x00040000)
#define I2C1_CPU_1XCLKACT	(0x00080000)

int XIicPs_MasterSendPolled_ours(XIicPs *InstancePtr, u8 *MsgPtr,
				 int ByteCount, u16 SlaveAddr);
int XIicPs_MasterRecvPolled_ours(XIicPs *InstancePtr, u8 *MsgPtr,
				int ByteCount, u16 SlaveAddr);
int XIicPs_SetupMaster(XIicPs *InstancePtr, int Role);

int zybo_i2c_reset(struct I2CDriver *self) {
  // ensure all required memory is allocated
  struct ZyboI2CState *state = self->state;
  if (state == NULL) return -1;
  if (state->inst == NULL) return -1;
  if (state->busId > 1) return -1;
  int i2cID = state->busId;
  XIicPs *inst = state->inst;

  //Make sure CPU_1x clk is enabled fostatusr I2C controller
  u16 *aper_ctrl = (u16 *) IO_CLK_CONTROL_REG_ADDR;
  u32 aper_mask = (i2cID == 0) ? (I2C0_CPU_1XCLKACT) : (I2C1_CPU_1XCLKACT);

  if (*aper_ctrl & aper_mask){
    xil_printf("CPU_1x is set to I2C0\r\n");
  }

  else {
    xil_printf("CPU_1x is not set to I2C..Setting now\r\n");
    *aper_ctrl |= aper_mask;
  }

  // Look up
  XIicPs_Config *i2c_config = XIicPs_LookupConfig(i2cID);
  XStatus status = XIicPs_CfgInitialize(inst, i2c_config, i2c_config->BaseAddress);

  // Check if initialization was successful
  if(status != XST_SUCCESS){
    return -1;
  }

  // Reset the controller and set the clock to 400kHz
  XIicPs_Reset(inst);
  XIicPs_SetSClk(inst, 400000);

  return 0;
}

int zybo_i2c_write(struct I2CDriver *self,
                   unsigned short device_addr,
                   unsigned char *data,
                   unsigned int length) {
  struct ZyboI2CState *state = self->state;
  XIicPs *inst = state->inst;
  if (device_addr == PX4FLOW_DEVICE_ADDR) {
	  // If we are sending a request to optical flow, drop down to 100kHz
	  XIicPs_SetSClk(inst, 100000);
  }
  int error = XIicPs_MasterSendPolled_ours(inst, data, length, device_addr);
  if (device_addr == PX4FLOW_DEVICE_ADDR) {
	  // Put it back to 400kHz
	  XIicPs_SetSClk(inst, 400000);
  }
  usleep(5);
  return error;
}

int zybo_i2c_read(struct I2CDriver *self,
                  unsigned short device_addr,
                  unsigned char *buff,
                  unsigned int length) {
  struct ZyboI2CState *state = self->state;
  XIicPs *inst = state->inst;
  if (device_addr == PX4FLOW_DEVICE_ADDR) {
	  // If we are sending a request to optical flow, drop down to 100kHz
	  XIicPs_SetSClk(inst, 100000);
  }
  int error = XIicPs_MasterRecvPolled_ours(inst, buff, length, device_addr);
  if (device_addr == PX4FLOW_DEVICE_ADDR) {
	  // Put it back to 400kHz
	  XIicPs_SetSClk(inst, 400000);
  }
  usleep(5);
  return error;
}

/*****************************************************************************/
/**
* NOTE to MicroCART: This function is originally from the Xilinx library,
* but we noticed that the original function didn't check for a NACK, which
* would cause the original polling function to enter an infinite loop in the
* event of a NACK. Notice that we have added that NACK check at the final
* while loop of this function.
*
*
* This function initiates a polled mode send in master mode.
*u16 i2cID
* It sends data to the FIFO and waits for the slave to pick them up.
* If slave fails to remove data from FIFO, the send fails with
* time out.
*
* @param	InstancePtr is a pointer to the XIicPs instance.
* @param	MsgPtr is the pointer to the send buffer.
* @param	ByteCount is the number of bytes to be sent.
* @param	SlaveAddr is the address of the slave we are sending to.
*
* @return
*		- XST_SUCCESS if everything went well.
*		- XST_FAILURE if timed out.
*
* @note		This send routine is for polled mode transfer only.
*
****************************************************************************/
int XIicPs_MasterSendPolled_ours(XIicPs *InstancePtr, u8 *MsgPtr,
		 int ByteCount, u16 SlaveAddr)
{
	u32 IntrStatusReg;
	u32 StatusReg;
	u32 BaseAddr;
	u32 Intrs;

	/*
	 * Assert validates the input arguments.
	 */
	Xil_AssertNonvoid(InstancePtr != NULL);
	Xil_AssertNonvoid(MsgPtr != NULL);
	Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);
	Xil_AssertNonvoid(XIICPS_ADDR_MASK >= SlaveAddr);

	BaseAddr = InstancePtr->Config.BaseAddress;
	InstancePtr->SendBufferPtr = MsgPtr;
	InstancePtr->SendByteCount = ByteCount;

	XIicPs_SetupMaster(InstancePtr, SENDING_ROLE);

	XIicPs_WriteReg(BaseAddr, XIICPS_ADDR_OFFSET, SlaveAddr);

	/*
	 * Intrs keeps all the error-related interrupts.
	 */
	Intrs = XIICPS_IXR_ARB_LOST_MASK | XIICPS_IXR_TX_OVR_MASK |
			XIICPS_IXR_TO_MASK | XIICPS_IXR_NACK_MASK;

	/*
	 * Clear the interrupt status register before use it to monitor.
	 */
	IntrStatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_ISR_OFFSET);
	XIicPs_WriteReg(BaseAddr, XIICPS_ISR_OFFSET, IntrStatusReg);

	/*
	 * Transmit first FIFO full of data.
	 */
	TransmitFifoFill(InstancePtr);

	IntrStatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_ISR_OFFSET);

	/*
	 * Continue sending as long as there is more data and
	 * there are no errors.
	 */
	while ((InstancePtr->SendByteCount > 0) &&
		((IntrStatusReg & Intrs) == 0)) {
		StatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_SR_OFFSET);

		/*
		 * Wait until transmit FIFO is empty.
		 */
		if ((StatusReg & XIICPS_SR_TXDV_MASK) != 0) {
			IntrStatusReg = XIicPs_ReadReg(BaseAddr,
					XIICPS_ISR_OFFSET);
			continue;
		}

		/*
		 * Send more data out through transmit FIFO.
		 */
		TransmitFifoFill(InstancePtr);
	}

	/*
	 * Check for completion of transfer.
	 */
	// NOTE for MicroCART: Corrected function. Original left for reference.
//	while ((XIicPs_ReadReg(BaseAddr, XIICPS_ISR_OFFSET) &
//		XIICPS_IXR_COMP_MASK) != XIICPS_IXR_COMP_MASK);
	while (!(IntrStatusReg & (Intrs | XIICPS_IXR_COMP_MASK))) {
		IntrStatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_ISR_OFFSET);
	}

	/*
	 * If there is an error, tell the caller.
	 */
	if (IntrStatusReg & Intrs) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}

static XTime get_time() {
    XTime time;
    XTime_GetTime(&time);
    u64 us = (u64)time * (1000000 / COUNTS_PER_SECOND); // (ticks)(1000000us/s)(s/ticks)
    return us;
}

/*****************************************************************************/
/**
* This function initiates a polled mode receive in master mode.
*
* It repeatedly sets the transfer size register so the slave can
* send data to us. It polls the data register for data to come in.
* If slave fails to send us data, it fails with time out.
*
* @param	InstancePtr is a pointer to the XIicPs instance.
* @param	MsgPtr is the pointer to the receive buffer.
* @param	ByteCount is the number of bytes to be received.
* @param	SlaveAddr is the address of the slave we are receiving from.
*
* @return
*		- XST_SUCCESS if everything went well.
*		- XST_FAILURE if timed out.
*
* @note		This receive routine is for polled mode transfer only.
*
****************************************************************************/
int XIicPs_MasterRecvPolled_ours(XIicPs *InstancePtr, u8 *MsgPtr,
				int ByteCount, u16 SlaveAddr)
{
	u32 IntrStatusReg;
	u32 Intrs;
	u32 StatusReg;
	u32 BaseAddr;
	int BytesToRecv;
	int BytesToRead;
	int TransSize;
	int Tmp;

	/*
	 * Assert validates the input arguments.
	 */
	Xil_AssertNonvoid(InstancePtr != NULL);
	Xil_AssertNonvoid(MsgPtr != NULL);
	Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);
	Xil_AssertNonvoid(XIICPS_ADDR_MASK >= SlaveAddr);

	BaseAddr = InstancePtr->Config.BaseAddress;
	InstancePtr->RecvBufferPtr = MsgPtr;
	InstancePtr->RecvByteCount = ByteCount;

	XIicPs_SetupMaster(InstancePtr, RECVING_ROLE);

	XIicPs_WriteReg(BaseAddr, XIICPS_ADDR_OFFSET, SlaveAddr);

	/*
	 * Intrs keeps all the error-related interrupts.
	 */
	Intrs = XIICPS_IXR_ARB_LOST_MASK | XIICPS_IXR_RX_OVR_MASK |
			XIICPS_IXR_RX_UNF_MASK | XIICPS_IXR_TO_MASK |
			XIICPS_IXR_NACK_MASK;

	/*
	 * Clear the interrupt status register before use it to monitor.
	 */
	IntrStatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_ISR_OFFSET);
	XIicPs_WriteReg(BaseAddr, XIICPS_ISR_OFFSET, IntrStatusReg);

	/*
	 * Set up the transfer size register so the slave knows how much
	 * to send to us.
	 */
	if (ByteCount > XIICPS_FIFO_DEPTH) {
		XIicPs_WriteReg(BaseAddr, XIICPS_TRANS_SIZE_OFFSET,
			 XIICPS_FIFO_DEPTH);
	}else {
		XIicPs_WriteReg(BaseAddr, XIICPS_TRANS_SIZE_OFFSET,
			 ByteCount);
	}

/* <--- MicroCART additions (iic watchdog timer hack) ---> */
	u32 iic_freq = XIicPs_GetSClk(InstancePtr);
	// (1000000 * 9 / iic_freq) is the number of microseconds required to send 1 byte of data
	// Using 5 times as an upper bound
	u32 max_usec_per_byte = 5 * 1000000 * 9 / iic_freq;
	u64 start_time = get_time();
/* <--- End hack ---> */

	/*
	 * Pull the interrupt status register to find the errors.
	 */
	IntrStatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_ISR_OFFSET);
	while ((InstancePtr->RecvByteCount > 0) &&
			((IntrStatusReg & Intrs) == 0) && !(IntrStatusReg & XIICPS_IXR_COMP_MASK)) {
/* <--- MicroCART additions (iic watchdog timer hack) ---> */
		u64 usec_passed = get_time() - start_time;
		// Add 1 so it has a chance to read
		if (usec_passed > max_usec_per_byte * (1 + ByteCount - InstancePtr->RecvByteCount)) {
			return IIC_RX_TIMEOUT_FAILURE;
		}
/* <--- End hack ---> */
		StatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_SR_OFFSET);

		/*
		 * If there is no data in the FIFO, check the interrupt
		 * status register for error, and continue.
		 */
		if ((StatusReg & XIICPS_SR_RXDV_MASK) == 0) {
			IntrStatusReg = XIicPs_ReadReg(BaseAddr,
					XIICPS_ISR_OFFSET);
			continue;
		}

		/*
		 * The transfer size register shows how much more data slave
		 * needs to send to us.
		 */
		TransSize = XIicPs_ReadReg(BaseAddr,
		XIICPS_TRANS_SIZE_OFFSET);

		BytesToRead = InstancePtr->RecvByteCount;

		/*
		 * If expected number of bytes is greater than FIFO size,
		 * the master needs to wait for data comes in and set the
		 * transfer size register for slave to send more.
		 */
		if (InstancePtr->RecvByteCount > XIICPS_FIFO_DEPTH) {
			/* wait slave to send data */
			while ((TransSize > 2) &&
				((IntrStatusReg & Intrs) == 0)) {
				TransSize = XIicPs_ReadReg(BaseAddr,
						XIICPS_TRANS_SIZE_OFFSET);
				IntrStatusReg = XIicPs_ReadReg(BaseAddr,
							XIICPS_ISR_OFFSET);
			}

			/*
			 * If timeout happened, it is an error.
			 */
			if (IntrStatusReg & XIICPS_IXR_TO_MASK) {
				return XST_FAILURE;
			}
			TransSize = XIicPs_ReadReg(BaseAddr,
						XIICPS_TRANS_SIZE_OFFSET);

			/*
			 * Take trans size into account of how many more should
			 * be received.
			 */
			BytesToRecv = InstancePtr->RecvByteCount -
					XIICPS_FIFO_DEPTH + TransSize;

			/* Tell slave to send more to us */
			if (BytesToRecv > XIICPS_FIFO_DEPTH) {
				XIicPs_WriteReg(BaseAddr,
					XIICPS_TRANS_SIZE_OFFSET,
					XIICPS_FIFO_DEPTH);
			} else{
				XIicPs_WriteReg(BaseAddr,
					XIICPS_TRANS_SIZE_OFFSET, BytesToRecv);
			}

			BytesToRead = XIICPS_FIFO_DEPTH - TransSize;
		}

		Tmp = 0;
		IntrStatusReg = XIicPs_ReadReg(BaseAddr, XIICPS_ISR_OFFSET);
		while ((Tmp < BytesToRead) &&
				((IntrStatusReg & Intrs) == 0)) {
			StatusReg = XIicPs_ReadReg(BaseAddr,
					XIICPS_SR_OFFSET);
			IntrStatusReg = XIicPs_ReadReg(BaseAddr,
					XIICPS_ISR_OFFSET);

			if ((StatusReg & XIICPS_SR_RXDV_MASK) == 0) {
				/* No data in fifo */
				continue;
			}
			XIicPs_RecvByte(InstancePtr);
			Tmp ++;
		}
	}

	if ((IntrStatusReg & Intrs) || InstancePtr->RecvByteCount > 0) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}


/*****************************************************************************/
/*
* NOTE to MicroCART: This function is required by the send polling method above,
* but it was originally static, so we had to copy it word-for-word here.
*
* This function prepares a device to transfers as a master.
*
* @param	InstancePtr is a pointer to the XIicPs instance.
*
* @param	Role specifies whether the device is sending or receiving.
*
* @return
*		- XST_SUCCESS if everything went well.
*		- XST_FAILURE if bus is busy.
*
* @note		Interrupts are always disabled, device which needs to use
*		interrupts needs to setup interrupts after this call.
*
****************************************************************************/
int XIicPs_SetupMaster(XIicPs *InstancePtr, int Role)
{
	u32 ControlReg;
	u32 BaseAddr;
	u32 EnabledIntr = 0x0;

	Xil_AssertNonvoid(InstancePtr != NULL);

	BaseAddr = InstancePtr->Config.BaseAddress;
	ControlReg = XIicPs_ReadReg(BaseAddr, XIICPS_CR_OFFSET);


	/*
	 * Only check if bus is busy when repeated start option is not set.
	 */
	if ((ControlReg & XIICPS_CR_HOLD_MASK) == 0) {
		if (XIicPs_BusIsBusy(InstancePtr)) {
			return XST_FAILURE;
		}
	}

	/*
	 * Set up master, AckEn, nea and also clear fifo.
	 */
	ControlReg |= XIICPS_CR_ACKEN_MASK | XIICPS_CR_CLR_FIFO_MASK |
		 	XIICPS_CR_NEA_MASK | XIICPS_CR_MS_MASK;

	if (Role == RECVING_ROLE) {
		ControlReg |= XIICPS_CR_RD_WR_MASK;
		EnabledIntr = XIICPS_IXR_DATA_MASK |XIICPS_IXR_RX_OVR_MASK;
	}else {
		ControlReg &= ~XIICPS_CR_RD_WR_MASK;
	}
	EnabledIntr |= XIICPS_IXR_COMP_MASK | XIICPS_IXR_ARB_LOST_MASK;

	XIicPs_WriteReg(BaseAddr, XIICPS_CR_OFFSET, ControlReg);

	XIicPs_DisableAllInterrupts(BaseAddr);

	return XST_SUCCESS;
}

#include "hw_iface.h"
#include "hw_impl_zybo.h"
#include "type_def.h"
#include <string.h>

#define LIDARLITE_DEVICE_ADDR		0x62
#define LIDAR_OFFSET (0.016666) // Distance from LiDAR sensor to ground, in meters

int lidarlite_write(struct I2CDriver *i2c, u8 register_addr, u8 data);
int lidarlite_read(struct I2CDriver *i2c, u8* recv_buffer, u8 register_addr, int size);

int zybo_lidar_reset(struct LidarDriver *self, lidar_t *lidar) {
  // initialize lidar
	memset(lidar, 0, sizeof(lidar_t));

  struct I2CDriver *i2c = self->i2c;

  int error = 0;
  //Device wakeup if asleep.
  lidarlite_write(i2c, 0x00, 0x00);
  usleep(15000);
  // Device Reset & Wake up with default settings
  error = lidarlite_write(i2c, 0x00, 0x00);
  if (error) return error;
  usleep(15000);

  // Enable Free Running Mode and distance measurements with correction
  error = lidarlite_write(i2c, 0x11, 0xff);
  if (error) return error;
  error = lidarlite_write(i2c, 0x00, 0x04);
  return error;
}

int zybo_lidar_read(struct LidarDriver *self, lidar_t *lidar) {
  struct I2CDriver *i2c = self->i2c;
  u8 buf[2];
  int error = 0;

  // Read the sensor value
  error = lidarlite_read(i2c, buf, 0x8f, 2);
  if (error) return error;
  float distance_cm = (float)(buf[0] << 8 | buf[1]);
  lidar->distance_m = (distance_cm * 0.01) + LIDAR_OFFSET;

  return error;
}

////////////////////
// Helper functions
////////////////////

int lidarlite_write(struct I2CDriver *i2c, u8 register_addr, u8 data) {
  u8 buf[] = {register_addr, data};
  return i2c->write(i2c, LIDARLITE_DEVICE_ADDR, buf, 2);
}

int lidarlite_read(struct I2CDriver *i2c, u8* recv_buffer, u8 register_addr, int size) {
  u8 buf[] = {register_addr};
  int error = 0;

  error = i2c->write(i2c, LIDARLITE_DEVICE_ADDR, buf, 1);
  if (error) return error;
  error = i2c->read(i2c, LIDARLITE_DEVICE_ADDR, recv_buffer, size);
  return error;
}

// Maybe this will be useful?
int lidarlite_sleep(struct I2CDriver *i2c) {
  return lidarlite_write(i2c, 0x65, 0x84);
}

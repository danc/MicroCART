#include "hw_impl_zybo.h"
#include "type_def.h"
#include <stdio.h>

/**
 * Test for the LEDDriver and SystemDriver.
 *
 * This is essentially a basic "blink" program, using the mio7 LED
 * on the Zybo board.
 *
 * Instructions:
 * 1) Connect Zybo board to computer by USB cable.
 * 2) Set the RUN_TESTS macro in main.c
 * 3) Uncomment only this test in main.c
 * 4) Run main.c
 * 5) Observe MIO7 LED on board blinking at 1 second intervals.
 */
int test_zybo_mio7_led_and_system() {
  struct LEDDriver mio7 = create_zybo_mio7_led();
  struct SystemDriver sys = create_zybo_system();
  mio7.reset(&mio7);
  sys.reset(&sys);

  while (1) {
    mio7.turn_on(&mio7);
    sys.sleep(&sys, 1000000);
    mio7.turn_off(&mio7);
    sys.sleep(&sys, 1000000);
  }

  return 0;
}

/**
 * Tests for the I2CDriver (one for each I2C device we use)
 *
 * Instructions:
 * 1) Connect Zybo Board to computer by USB cable.
 * 2) Prepare a breadboard to accomplish the following connections:
 *   - Zybo GND     <-> LIDAR GND
 *   - Zybo WALL    <-> LIDAR VCC
 *   - Zybo VV5V0   <-> LIDAR VCC
 *   - Zybo SDA/SCL <-> LIDAR SDA/SCL
 *     - The Zybo I2C SCL and SDA pins are on pins 1 and 2 of PORT JF, respectively
 * 3) Place breakpoint in this test function, somewhere in the while loop.
 * 4) Set the RUN_TESTS macro in main.c
 * 5) Uncomment only this test in main.c
 * 6) Debug main.c
 * 7) Step through the while loop, observing x change as you physically change the
 *    distance of the LIDAR sensor.
 */
int test_zybo_i2c() {
  struct I2CDriver i2c = create_zybo_i2c(1);
  struct LidarDriver ld = create_zybo_lidar(&i2c);
  i2c.reset(&i2c);

  lidar_t lidar = { };
  if (ld.reset(&ld, &lidar)) return 0;
  while (1) {
    ld.read(&ld, &lidar);
  }
  return 0;
}

int test_zybo_i2c_imu() {
  struct I2CDriver i2c = create_zybo_i2c(0);
  struct IMUDriver imu = create_zybo_imu(&i2c);
  char buf[100];
  gam_t gam;
  if (i2c.reset(&i2c)) return 0;
  if (imu.reset(&imu, &gam)) return 0;

  int status = 0;
  while (!status) {
    status = imu.read(&imu, &gam);
    usleep(5000);
  }
  return 0;
}

int test_zybo_i2c_px4flow() {
  struct I2CDriver i2c = create_zybo_i2c(0);
  struct OpticalFlowDriver ofd = create_zybo_optical_flow(&i2c);
  i2c.reset(&i2c);
  px4flow_t of;

  if (ofd.reset(&ofd, &of)) return 0;

  int status = 0;
  while(!status) {
    usleep(5000);
    status = ofd.read(&ofd, &of);
  }

  return 0;
}

int test_zybo_i2c_lidar() {
  struct I2CDriver i2c = create_zybo_i2c(1);
  struct LidarDriver lidarDriver = create_zybo_lidar(&i2c);
  i2c.reset(&i2c);
  lidar_t lidar;

  if (lidarDriver.reset(&lidarDriver, &lidar))
	  return 0;

  int status = 0;
  while(!status) {
    usleep(5000);
    status = lidarDriver.read(&lidarDriver, &lidar);
  }

  return 0;
}

int test_zybo_i2c_all() {
  struct I2CDriver i2c_0 = create_zybo_i2c(0);
  struct IMUDriver imu = create_zybo_imu(&i2c_0);
  struct I2CDriver i2c_1 = create_zybo_i2c(1);
  struct LidarDriver ld = create_zybo_lidar(&i2c_1);
  struct OpticalFlowDriver ofd = create_zybo_optical_flow(&i2c_0);
  i2c_0.reset(&i2c_0);
  i2c_1.reset(&i2c_1);

  lidar_t lidar;
  px4flow_t of;
  gam_t gam;

  if (ld.reset(&ld, &lidar)) return 0;
  if (imu.reset(&imu, &gam)) return 0;
  if (ofd.reset(&ofd, &of)) return 0;


  int lidarErrors = 0;
  int gamErrors = 0;
  int nLoops = 0;
  int of_errors = 0;

  for(;;) {
    ld.read(&ld, &lidar);
    imu.read(&imu, &gam);
    ofd.read(&ofd, &of);

    if (lidar.distance_m > 50) {
    	lidarErrors += 1;
    }
    if (gam.accel_z > -0.8) {
    	gamErrors += 1;
    }
    nLoops += 1;
  }
  return 0;
}

/**
 * Test for the PWMInputDriver.
 *
 * Instructions:
 * 1) Connect the quad Zybo board to computer using USB.
 * 2) Move jumper on Zybo board to use JTAG instead of SD.
 * 3) Turn on Zybo board and turn on Spektrum handheld controller.
 *   - Verify receiver on quad pairs with controller (orange LED should turn on)
 * 3) Place breakpoint somewhere in the while loop of this function.
 * 4) Set the RUN_TESTS macro in main.c
 * 5) Uncomment only this test in main.c
 * 6) Debug main.
 * 7) Observe the values of pwm_inputs in debugger changing as you use the
 *    Spektrum RC controller.
 */
int test_zybo_rc_receiver() {
  struct RCReceiverDriver rc_receiver = create_zybo_rc_receiver();
  rc_receiver.reset(&rc_receiver);

  float pwms[6];
  while (1) {
    int i;
    for (i = 0; i < 6; i += 1) {
      rc_receiver.read(&rc_receiver, i, &pwms[i]);
    }
    continue;
  }
}

/**
 * Test for the PWMOutputDriver.
 *
 * Instructions:
 * 1) Connect the quad Zybo board to computer using USB.
 * 2) Move jumper on Zybo board to use JTAG instead of SD.
 * 3) Get an oscilloscope and observe PMOD outputs JE7-JE10
 * 4) Set the RUN_TESTS macro in main.c
 * 5) Uncomment only this test in main.c
 * 6) Run main.
 * 7) Observe the PWM width of those PMOD pins changing with time
 */
int test_zybo_motors() {
  struct MotorDriver motors = create_zybo_motors();
  motors.reset(&motors);

  double j = 0;
  while (1) {
    for (j = 0; j < 1.0; j += 0.01) {
      int i = 0;
      for (i = 0; i < 4; i += 1) {
        motors.write(&motors, i, j);
        usleep(50000);
      }
    }
  }
  return 0;
}

/**
 * Test for the AXI timer, using LEDDriver.
 *
 * This is essentially a basic "blink" program, using the mio7 LED
 * on the Zybo board.
 *
 * Instructions:
 * 1) Connect Zybo board to computer by USB cable.
 * 2) Set the RUN_TESTS macro in main.c
 * 3) Uncomment only this test in main.c
 * 4) Run main.c
 * 5) Observe MIO7 LED on board blinking at 1 second intervals.
 */
int test_zybo_axi_timer() {
  struct TimerDriver axi = create_zybo_axi_timer();
  struct LEDDriver led = create_zybo_mio7_led();
  axi.reset(&axi);
  led.reset(&led);

  unsigned long time;

  while (1) {
    axi.restart(&axi);
    time = 0;
    while (time < 1000000) {
      axi.read(&axi, &time);
    }
    led.turn_off(&led);
    while (time < 2000000) {
      axi.read(&axi, &time);
    }
    led.turn_on(&led);
  }
}

/**
 * Test for the Global timer, using LEDDriver.
 *
 * This is essentially a basic "blink" program, using the mio7 LED
 * on the Zybo board.
 *
 * Instructions:
 * 1) Connect Zybo board to computer by USB cable.
 * 2) Set the RUN_TESTS macro in main.c
 * 3) Uncomment only this test in main.c
 * 4) Run main.c
 * 5) Observe MIO7 LED on board blinking at 1 second intervals.
 */
int test_zybo_global_timer() {
  struct TimerDriver global = create_zybo_global_timer();
  struct LEDDriver led = create_zybo_mio7_led();
  global.reset(&global);
  led.reset(&led);

  unsigned long time;

  while (1) {
    global.restart(&global);
    time = 0;
    while (time < 1000000) {
      global.read(&global, &time);
    }
    led.turn_off(&led);
    while (time < 2000000) {
      global.read(&global, &time);
    }
    led.turn_on(&led);
  }
}

/**
 * Communication tests using the UARTDriver.
 *
 * Instructions:
 * 1) Connect Zybo board to computer by USB cable.
 * 2) Get a FTDI Basic Sparkfun board in order to connect the UART pins
 *    on the Zybo to the computer by USB.
 *    - Zybo PMOD JC2 (TX) <-> Sparkfun Board RX
 *    - Zybo PMOD JC3 (RX) <-> sparkfun Board Tx
 *    - Zybo PMOD JC5 (GND) <-> Sparkfun Board GDN
 * 2) Set the RUN_TESTS macro in main.c
 * 3) Uncomment only this test in main.c
 * 4) Run main.c
 * 5) Execute quad/scripts/tests/test_zybo_uart.py
 *    - Observe test results on terminal
 *    - You might be able to see LED MIO7 blink when it receives bytes
 */
int test_zybo_uart_comm() {
  struct UARTDriver uart = create_zybo_uart(0);
  struct CommDriver comm = create_zybo_comm(&uart);
  struct LEDDriver led = create_zybo_mio7_led();
  uart.reset(&uart);
  led.reset(&led);

  unsigned char c;
  while (1) {
    if (comm.uart->read(comm.uart, &c)) {
    	uart.write(&uart, 0x55);
      // read failed
      led.turn_off(&led);
    } else {
      // read successful
      led.turn_on(&led);
      uart.write(&uart, c);
    }
  }

  return 0;
}


int test_zybo_pcb() {
  struct UARTDriver uart = create_zybo_uart(0);
  struct LEDDriver led = create_zybo_mio7_led();
  struct RCReceiverDriver rc = create_zybo_rc_receiver();
  struct MotorDriver motors = create_zybo_motors();
  uart.reset(&uart);
  led.reset(&led);
  rc.reset(&rc);
  motors.reset(&motors);

  char stateMsg[100];
  unsigned char c;
  unsigned char led_state = 0;
  float channels[6];
  float pwm[4];
  int i;
  while (1) {
	  if(uart.read(&uart, &c) >= 0) {
		  if(led_state) {
			  led.turn_off(&led);
			  led_state = 0;
		  } else {
			  led.turn_on(&led);
			  led_state = 1;
		  }
	  }
	  for (i = 0; i < 6; i++) {
		  rc.read(&rc, i, &channels[i]);
	  }
	  pwm[0] = channels[0]+channels[1]+channels[3];
	  pwm[1] = channels[0]+channels[1]-channels[3];
	  pwm[2] = channels[0]-channels[1]+channels[3];
	  pwm[3] = channels[0]-channels[1]-channels[3];
	  sprintf(stateMsg, "%5.2f %5.2f %5.2f %5.2f %5.2f %5.2f -> %5.2f %5.2f %5.2f %5.2f\r\n", channels[0], channels[1], channels[2], channels[3], channels[4], channels[5], pwm[0], pwm[1], pwm[2], pwm[2]);
	  char* it;
	  for (it = stateMsg; *it; it++) {
		  uart.write(&uart, *it);
	  }
	  for (i = 0; i < 4; i++) {
		  motors.write(&motors, i, pwm[i]);
	  }
  }

  return 0;
}

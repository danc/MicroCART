#ifndef HW_IMPL_ZYBO
#define HW_IMPL_ZYBO

#include "hw_iface.h"

#include <sleep.h>
#include <stdlib.h>
#include <xtmrctr.h>
#include <xgpiops.h>
#include "xiicps.h"
#include "xparameters.h"
#include "xgpiops.h"
#include "xil_types.h"
#include "xscugic.h"
#include "xtime_l.h"
#include "xuartps.h"
#include "queue.h"
#include "platform.h"
#include "sleep.h"
#include "type_def.h"

// Ideally, these defines would only be in the optical flow file, but
// i2c needs it for a certain hack
#define PX4FLOW_DEVICE_ADDR			0x42
#define PX4FLOW_QUAL_MIN			(100)

#define MAX_UART_BUFFER_SIZE 2048

#define QUAD_NUM 1

#if QUAD_NUM==0
#define IMU_ACCX_SIGN -1
#define IMU_ACCY_SIGN 1
#define IMU_ACCZ_SIGN 1

#define IMU_GYRX_SIGN -1
#define IMU_GYRY_SIGN 1
#define IMU_GYRZ_SIGN 1


#define MOTOR_FRONTLEFT_BASEADDR   XPAR_PWM_SIGNAL_OUT_3_S_AXI_BASEADDR
#define MOTOR_FRONTRIGHT_BASEADDR  XPAR_PWM_SIGNAL_OUT_1_S_AXI_BASEADDR
#define MOTOR_BACKLEFT_BASEADDR    XPAR_PWM_SIGNAL_OUT_2_S_AXI_BASEADDR
#define MOTOR_BACKRIGHT_BASEADDR   XPAR_PWM_SIGNAL_OUT_0_S_AXI_BASEADDR

#define ACCEL_X_BIAS    0.08f
#define ACCEL_Y_BIAS	-0.057f
#define ACCEL_Z_BIAS    0.0f

#define GYRO_X_BIAS	-0.006f
#define GYRO_Y_BIAS	 0.0545f
#define GYRO_Z_BIAS	 0.005//0.0541f

#endif

#if QUAD_NUM==1

#define IMU_ACCX_SIGN 1
#define IMU_ACCY_SIGN 1
#define IMU_ACCZ_SIGN 1

#define IMU_GYRX_SIGN 1
#define IMU_GYRY_SIGN 1
#define IMU_GYRZ_SIGN 1


#define MOTOR_FRONTLEFT_BASEADDR   XPAR_PWM_SIGNAL_OUT_3_S_AXI_BASEADDR
#define MOTOR_FRONTRIGHT_BASEADDR  XPAR_PWM_SIGNAL_OUT_1_S_AXI_BASEADDR
#define MOTOR_BACKLEFT_BASEADDR    XPAR_PWM_SIGNAL_OUT_2_S_AXI_BASEADDR
#define MOTOR_BACKRIGHT_BASEADDR   XPAR_PWM_SIGNAL_OUT_0_S_AXI_BASEADDR

#define ACCEL_X_BIAS    -0.01f
#define ACCEL_Y_BIAS	0.041f
#define ACCEL_Z_BIAS    0.0f

#define GYRO_X_BIAS	-0.024f
#define GYRO_Y_BIAS	-0.013f
#define GYRO_Z_BIAS	0.02//0.0541f

#endif

struct ZyboI2CState {
  XIicPs *inst;
  int busId;
};

struct ZyboUARTState {
  struct Queue *queue;
  XUartPs *inst;
  XScuGic xscugic;
  int devId;
};

int zybo_uart_reset(struct UARTDriver *self);
int zybo_uart_write(struct UARTDriver *self, unsigned char c);
int zybo_uart_read(struct UARTDriver *self, unsigned char *c);

int zybo_motor_reset(struct MotorDriver *self);
int zybo_motor_write(struct MotorDriver *self, unsigned int channel, float magnitude);

int zybo_rc_receiver_reset(struct RCReceiverDriver *self);
int zybo_rc_receiver_read(struct RCReceiverDriver *self, unsigned int channel, float *magnitude);

int zybo_i2c_reset(struct I2CDriver *self);
int zybo_i2c_write(struct I2CDriver *self,
                   unsigned short device_addr,
                   unsigned char *data,
                   unsigned int length);
int zybo_i2c_read(struct I2CDriver *self,
                  unsigned short device_addr,
                  unsigned char *buff,
                  unsigned int length);

int zybo_global_timer_reset(struct TimerDriver *self);
int zybo_global_timer_restart(struct TimerDriver *self);
int zybo_global_timer_read(struct TimerDriver *self, u64 *us);

int zybo_axi_timer_reset(struct TimerDriver *self);
int zybo_axi_timer_restart(struct TimerDriver *self);
int zybo_axi_timer_read(struct TimerDriver *self, u64 *us);

int zybo_mio7_led_reset(struct LEDDriver *self);
int zybo_mio7_led_turn_on(struct LEDDriver *self);
int zybo_mio7_led_turn_off(struct LEDDriver *self);

int zybo_system_reset(struct SystemDriver *self);
int zybo_system_sleep(struct SystemDriver *self, unsigned long us);

int zybo_imu_reset(struct IMUDriver *self, struct gam *gam);
int zybo_imu_read(struct IMUDriver *self, struct gam *gam);

int zybo_lidar_reset(struct LidarDriver *self, struct lidar *lidar);
int zybo_lidar_read(struct LidarDriver *self, struct lidar *lidar);

int zybo_optical_flow_reset(struct OpticalFlowDriver *self, struct px4flow *of);
int zybo_optical_flow_read(struct OpticalFlowDriver *self, struct px4flow *of);

int zybo_gps_reset(struct GPSDriver *self, gps_t *gps);
int zybo_gps_read(struct GPSDriver *self, gps_t *gps);

struct UARTDriver create_zybo_uart(int devId);
struct CommDriver create_zybo_comm(struct UARTDriver *uart);
struct GPSDriver create_zybo_gps(struct UARTDriver *uart);
struct MotorDriver create_zybo_motors();
struct RCReceiverDriver create_zybo_rc_receiver();
struct I2CDriver create_zybo_i2c(int busId);
struct TimerDriver create_zybo_global_timer();
struct TimerDriver create_zybo_axi_timer();
struct LEDDriver create_zybo_mio7_led();
struct SystemDriver create_zybo_system();
struct IMUDriver create_zybo_imu(struct I2CDriver *i2c);
struct LidarDriver create_zybo_lidar(struct I2CDriver *i2c);
struct OpticalFlowDriver create_zybo_optical_flow(struct I2CDriver *i2c);

int test_zybo_i2c();
int test_zybo_mio7_led_and_system();
int test_zybo_rc_receivers();

#endif

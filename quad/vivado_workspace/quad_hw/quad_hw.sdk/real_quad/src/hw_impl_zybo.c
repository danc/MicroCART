#include "hw_impl_zybo.h"

struct UARTDriver create_zybo_uart(int devId) {
  struct UARTDriver uart;
  uart.state = malloc(sizeof(struct ZyboUARTState));
  struct ZyboUARTState *state = uart.state;
  state->inst = malloc(sizeof(XUartPs));
  state->queue = queue_malloc(MAX_UART_BUFFER_SIZE);
  state->devId = devId;

  uart.reset = zybo_uart_reset;
  uart.write = zybo_uart_write;
  uart.read = zybo_uart_read;
  return uart;
}

struct GPSDriver create_zybo_gps(struct UARTDriver *uart) {
  struct GPSDriver gps;
  gps.uart = uart;
  gps.reset = zybo_gps_reset;
  gps.read = zybo_gps_read;
  return gps;
}

struct CommDriver create_zybo_comm(struct UARTDriver *uart) {
  struct CommDriver comm;
  comm.uart = uart;
  return comm;
}

struct MotorDriver create_zybo_motors() {
  struct MotorDriver motors;
  motors.state = NULL;
  motors.reset = zybo_motor_reset;
  motors.write = zybo_motor_write;
  return motors;
}

struct RCReceiverDriver create_zybo_rc_receiver() {
  struct RCReceiverDriver rc_receiver;
  rc_receiver.state = NULL;
  rc_receiver.reset = zybo_rc_receiver_reset;
  rc_receiver.read = zybo_rc_receiver_read;
  return rc_receiver;
}

struct I2CDriver create_zybo_i2c(int busId) {
  struct I2CDriver i2c;
  i2c.state = malloc(sizeof(struct ZyboI2CState));
  struct ZyboI2CState *state = i2c.state;
  state->inst = malloc(sizeof(XIicPs));
  state->busId = busId;
  i2c.reset = zybo_i2c_reset;
  i2c.write = zybo_i2c_write;
  i2c.read = zybo_i2c_read;
  return i2c;
}

struct TimerDriver create_zybo_global_timer() {
  struct TimerDriver global_timer;
  global_timer.state = NULL;
  global_timer.reset = zybo_global_timer_reset;
  global_timer.restart = zybo_global_timer_restart;
  global_timer.read = zybo_global_timer_read;
  return global_timer;
}

struct TimerDriver create_zybo_axi_timer() {
  struct TimerDriver axi_timer;
  axi_timer.state = NULL;
  axi_timer.reset = zybo_axi_timer_reset;
  axi_timer.restart = zybo_axi_timer_restart;
  axi_timer.read = zybo_axi_timer_read;
  return axi_timer;
}

struct LEDDriver create_zybo_mio7_led() {
  struct LEDDriver mio7_led;
  mio7_led.state = NULL;
  mio7_led.reset = zybo_mio7_led_reset;
  mio7_led.turn_on = zybo_mio7_led_turn_on;
  mio7_led.turn_off = zybo_mio7_led_turn_off;
  return mio7_led;
}

struct SystemDriver create_zybo_system() {
  struct SystemDriver sys;
  sys.state = NULL;
  sys.reset = zybo_system_reset;
  sys.sleep = zybo_system_sleep;
  return sys;
}

struct IMUDriver create_zybo_imu(struct I2CDriver *i2c) {
  struct IMUDriver imu;
  imu.i2c = i2c;
  imu.reset = zybo_imu_reset;
  imu.read = zybo_imu_read;
  return imu;
}

struct LidarDriver create_zybo_lidar(struct I2CDriver *i2c) {
  struct LidarDriver lidar;
  lidar.i2c = i2c;
  lidar.reset = zybo_lidar_reset;
  lidar.read = zybo_lidar_read;
  return lidar;
}

struct OpticalFlowDriver create_zybo_optical_flow(struct I2CDriver *i2c) {
  struct OpticalFlowDriver of;
  of.i2c = i2c;
  of.reset = zybo_optical_flow_reset;
  of.read = zybo_optical_flow_read;
  return of;
}

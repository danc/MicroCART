#include "hw_iface.h"
#include "hw_impl_zybo.h"
#include <stdint.h>
#include <string.h>
#include "type_def.h"

int px4flow_write(struct I2CDriver *i2c, u8 register_addr, u8 data);
int px4flow_read(struct I2CDriver *i2c, u8* recv_buffer, u8 register_addr, int size);

int zybo_optical_flow_reset(struct OpticalFlowDriver *self, px4flow_t *of) {
  memset(of, 0, sizeof(px4flow_t));

  return 0;
}

int zybo_optical_flow_read(struct OpticalFlowDriver *self, px4flow_t *of) {
  struct I2CDriver *i2c = self->i2c;
  int error = 0;

  // Note: Despite documentation, do not mark this as a "packed" struct. The actual code does not pack it.
  struct i2c_integral_frame
  {
      uint16_t frame_count_since_last_readout;//number of flow measurements since last I2C readout [#frames]
      int16_t pixel_flow_x_integral;//accumulated flow in radians*10000 around x axis since last I2C readout [rad*10000]
      int16_t pixel_flow_y_integral;//accumulated flow in radians*10000 around y axis since last I2C readout [rad*10000]
      int16_t gyro_x_rate_integral;//accumulated gyro x rates in radians*10000 since last I2C readout [rad*10000] 
      int16_t gyro_y_rate_integral;//accumulated gyro y rates in radians*10000 since last I2C readout [rad*10000] 
      int16_t gyro_z_rate_integral;//accumulated gyro z rates in radians*10000 since last I2C readout [rad*10000] 
      uint32_t integration_timespan;//accumulation timespan in microseconds since last I2C readout [microseconds]
      uint32_t sonar_timestamp;// time since last sonar update [microseconds]
      int16_t ground_distance;// Ground distance in meters*1000 [meters*1000]
      int16_t gyro_temperature;// Temperature * 100 in centi-degrees Celsius [degcelsius*100]
      uint8_t quality;// averaged quality of accumulated flow values [0:bad quality;255: max quality]
  } i2c_integral_frame;

  u8 buf[sizeof(i2c_integral_frame)];

  // Read the sensor value
  error = px4flow_read(i2c, buf, 0x16, 26);

  if(error == 0) {
    //Copy into struct
    memcpy(&i2c_integral_frame, buf, sizeof(i2c_integral_frame));

    of->flow_x_rad = 0.0001 * i2c_integral_frame.pixel_flow_x_integral;
    of->flow_y_rad = 0.0001 * i2c_integral_frame.pixel_flow_y_integral;
    of->quality = i2c_integral_frame.quality;
    of->dt = (double)i2c_integral_frame.integration_timespan / 1000000;
  }
  return error;
}

/////////////////////
// Helper functions
/////////////////////

int px4flow_write(struct I2CDriver *i2c, u8 register_addr, u8 data) {
	u8 buf[] = {register_addr, data};

	return i2c->write(i2c, PX4FLOW_DEVICE_ADDR, buf, 2);
}

int px4flow_read(struct I2CDriver *i2c, u8* recv_buffer, u8 register_addr, int size) {
	u8 buf[] = {register_addr};
	int error = 0;

	error = i2c->write(i2c, PX4FLOW_DEVICE_ADDR, buf, 1);
	if (error) {
		return error;
	}
	error = i2c->read(i2c, PX4FLOW_DEVICE_ADDR, recv_buffer, size);
	return error;
}


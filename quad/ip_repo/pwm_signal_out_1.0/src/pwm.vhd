LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY pwm IS
  PORT(
      clk       : IN  STD_LOGIC;        --system clock
      reset_n   : IN  STD_LOGIC;        --reset
      pwm_per   : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);    -- Number of clock counts
      pwm_puls  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);    -- Number of clock counts in puls
      pwm_out   : OUT STD_LOGIC := '0');        
END pwm;


ARCHITECTURE pwm_o OF pwm IS
  signal counter    : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000000"; -- Counter starting at 0
  signal pwm_puls_t : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000008"; 
  signal pwm_per_t  : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000010";
BEGIN
  PROCESS(clk, reset_n)
  begin
    
    if(rising_edge(clk)) then
	
		if(reset_n = '0') then
			counter <= X"00000000";
			pwm_out <= '0';
		elsif(unsigned(counter) < unsigned(pwm_puls_t)) then
			pwm_out <= '1';
			counter <= std_logic_vector(unsigned(counter) + 1);
        
		elsif(unsigned(counter) < unsigned(pwm_per_t)) then
			pwm_out <= '0';
			counter <= std_logic_vector(unsigned(counter) + 1);
		else
		    pwm_out <= '1';
			counter <= X"00000000";
		end if;
    end if;
  
  end process;
  
  -- Ensures pwm_puls_t and pwm_per_t only get updated when counter resets
  PROCESS(counter, reset_n, pwm_per_t, pwm_puls, pwm_per)
  begin
    if(unsigned(counter) = unsigned(pwm_per_t)) then
      pwm_puls_t <= pwm_puls;
      pwm_per_t  <= pwm_per; 

    end if;   
  end process;
  
END pwm_o;
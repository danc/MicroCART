----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/05/2018 02:27:32 PM
-- Design Name: 
-- Module Name: kernel_tester - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kernel_tester is
--  Port ( );
end kernel_tester;

architecture Behavioral of kernel_tester is
  component pwm IS
    PORT(clk       : IN  STD_LOGIC;        --system clock
         reset_n   : IN  STD_LOGIC;        --reset
         pwm_per   : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);    -- Number of clock counts
         pwm_puls  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);    -- Number of clock counts in puls
         pwm_out   : OUT STD_LOGIC := '0');        
  END component pwm;

  signal passing : std_logic;
  signal clk : std_logic;
  signal period : std_logic_vector(31 downto 0);
  signal pulse : std_logic_vector(31 downto 0);
  signal reset_n : std_logic;
  signal generated : std_logic;
  signal done : std_logic;
  
  constant CLK_HPER : time := 20 us;
begin
  UUT: pwm
    PORT map(clk => clk,
             reset_n => reset_n,
             pwm_per => period,
             pwm_puls => pulse,
             pwm_out => generated);
  
  testbench: process
    variable period_desired : integer;
    variable pulse_desired : integer;
    variable period_measured : time;
    variable pulse_measured : time;
    variable period_count : integer;
    variable pulse_count : integer;
  begin
    passing <= '1';
    
    reset_n <='0';
    wait for 2*CLK_HPER;
    reset_n <='1';
    
    for period_iter in 0 to 10 loop
      for duty in 1 to 9 loop
        period_desired := 100*(2**period_iter);
        pulse_desired := period_desired*duty/10;
        period <= std_logic_vector(to_unsigned(period_desired-1, 32));
        pulse <= std_logic_vector(to_unsigned(pulse_desired-1, 32));
        wait until generated = '1';
        wait until generated = '0';
        wait until generated = '1';
        period_measured := now;
        pulse_measured := now;
        wait until generated = '0';
        pulse_measured := now-pulse_measured;
        wait until generated = '1';
        period_measured := now-period_measured;
        period_count := (period_measured/CLK_HPER)/2;
        pulse_count := (pulse_measured/CLK_HPER)/2;
        if (period_count /= period_desired OR pulse_count /= pulse_desired) then
          passing <= '0';
        end if;
        ASSERT period_count = period_desired REPORT "Period was incorrect, expected: "&integer'image(period_desired)&" actual: "&integer'image(period_count) SEVERITY ERROR;
        ASSERT pulse_count = pulse_desired REPORT "Pulse was incorrect, expected: "&integer'image(pulse_desired)&" actual: "&integer'image(pulse_count) SEVERITY ERROR;
      end loop;
    end loop;
    
    
    done <= '1';
    wait;
  end process testbench;
  
  clock_gen: process
  begin
    if (done = '1') then
      wait;
    else
      clk <= '0';
      wait for CLK_HPER;
      clk <= '1';
      wait for CLK_HPER;
    end if;
  end process;
end Behavioral;

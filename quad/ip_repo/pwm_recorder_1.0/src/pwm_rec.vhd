LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;


entity pwm_rec is
	port(
		clk             : in std_logic;
		reset_n         : in std_logic;
		input_signal    : in std_logic;
		write_o         : out std_logic;
		count_out       : out std_logic_vector(31 downto 0));
end pwm_rec;

ARCHITECTURE behavioral OF pwm_rec IS
signal counter     : std_logic_vector(31 downto 0) := x"00000000";
--signal last_count  : std_logic_vector(31 downto 0) := x"00000000";
signal current     : std_logic := '0';
signal last_input  : std_logic := '0';
signal all_high    : std_logic := '0';
signal all_low     : std_logic := '0';
-- type shift_array is array(19 downto 0) of std_logic;
signal shift_array : std_logic_vector(19 downto 0) := x"00000";
begin	
	process(clk, reset_n)
	begin
		if(rising_edge(clk)) then
			count_out <= counter;
			-- Delay shift logic
			shift_array(19 downto 0) <= shift_array(18 downto 0) & input_signal;
			
			-- See if all the components of the shift array are high
			all_high <= (shift_array(19) and shift_array(18) and shift_array(17) and shift_array(16) and shift_array(15) and shift_array(14) and shift_array(13) and shift_array(12) and 
				shift_array(11) and shift_array(10) and shift_array(9) and shift_array(8) and shift_array(7) and shift_array(6) and shift_array(5) and shift_array(4) and 
				shift_array(3) and shift_array(2) and shift_array(1) and shift_array(0));
				
			-- See if all the components of the shift array are low
			all_low <= not(shift_array(19) or shift_array(18) or shift_array(17) or shift_array(16) or shift_array(15) or shift_array(14) or shift_array(13) or shift_array(12) or 
				shift_array(11) or shift_array(10) or shift_array(9) or shift_array(8) or shift_array(7) or shift_array(6) or shift_array(5) or shift_array(4) or 
				shift_array(3) or shift_array(2) or shift_array(1) or shift_array(0));
				
			if(reset_n = '0') then
				counter <= x"00000000";
				current <= '0';
				write_o <= '0';
				
			-- If they are all high the current state is high
			elsif(all_high = '1') then		
				current <= '1';		
				counter <= counter + 1;
				write_o <= '0';
				
			-- Check to see if all the bits are equal to 0
			elsif(all_low = '1' and current = '1') then
				write_o <= '1';
				-- If they are, the current state is low
				current <= '0';
				counter <= x"00000000";
				
			elsif(all_low = '1') then 
				counter <= x"00000000";
				current <= '0';
				write_o <= '0';
				
            -- Neither all high or all low
            elsif (current = '1') then
                current <= '1';        
                counter <= counter + 1;
                write_o <= '0';
		  
		    
			end if;
		end if;
	end process;	
end behavioral;
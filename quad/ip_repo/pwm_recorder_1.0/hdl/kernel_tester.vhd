----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/27/2018 12:05:17 PM
-- Design Name: 
-- Module Name: kernel_tester - testbench
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kernel_tester is
--  Port ( );
end kernel_tester;

architecture testbench of kernel_tester is
    component pwm_rec is
        port(
            clk             : in std_logic;
            reset_n         : in std_logic;
            input_signal    : in std_logic;
            write_o         : out std_logic;
            count_out       : out std_logic_vector(31 downto 0));
    end component;
    
    constant CLK_HPER : time := 20 ns;
    signal clk : std_logic;
    signal pwm : std_logic;
    signal reset_n : std_logic;
    signal count : std_logic_vector(31 downto 0);
    signal write : std_logic;
    signal done : std_logic := '0';
    signal passing : std_logic := '0';
    
begin
    UUT: pwm_rec
        port map(
            clk => clk,
            reset_n => reset_n,
            input_signal => pwm,
            write_o => write,
            count_out => count);
            
    test_process: process
        variable period : integer;
        
    begin
        passing <='1';
        
        reset_n <='0';
        wait for 2*CLK_HPER;
        reset_n <='1';
    
        period := 100;
        pwm <= '0';
        wait for CLK_HPER*2*20;
        pwm <= '1';
        wait for period*2*CLK_HPER;
        pwm <= '0';
        wait until write='1';
        if (count /= std_logic_vector(to_unsigned(period, 32))) then
          REPORT "Incorrect COUNT result" SEVERITY FAILURE;
          passing <='0';
        end if;
        
        period := 200;
        pwm <= '0';
        wait for CLK_HPER*2*20;
        pwm <= '1';
        wait for period*2*CLK_HPER;
        pwm <= '0';
        wait until write='1';
        if (count /= std_logic_vector(to_unsigned(period, 32))) then
          REPORT "Incorrect COUNT result" SEVERITY FAILURE;
          passing <='0';
        end if;
                
        period := 300;
        pwm <= '0';
        wait for CLK_HPER*2*20;
        pwm <= '1';
        wait for period*2*CLK_HPER;
        pwm <= '0';
        wait until write='1';
        if (count /= std_logic_vector(to_unsigned(period, 32))) then
          REPORT "Incorrect COUNT result" SEVERITY FAILURE;
          passing <='0';
        end if;
        
        period := 400;
        pwm <= '0';
        wait for CLK_HPER*2*20;
        pwm <= '1';
        wait for period*2*CLK_HPER;
        pwm <= '0';
        wait until write='1';
        if (count /= std_logic_vector(to_unsigned(period, 32))) then
          REPORT "Incorrect COUNT result" SEVERITY FAILURE;
          passing <='0';
        end if;
        
        period := 10;
        pwm <= '0';
        wait for CLK_HPER*2*20;
        pwm <= '1';
        wait for period*2*CLK_HPER;
        pwm <= '0';
        for t in 0 to 100 loop
            wait for 2*CLK_HPER;
            if (write /= '0') then
              REPORT "Glitch not Ignored" SEVERITY FAILURE;
              passing <='0';
            end if;
        end loop;
        
        -- Run for 45, glitch for 10, then run for 45, expect a result of 100
        period := 100;
        pwm <= '0';
        wait for CLK_HPER*2*20;
        pwm <= '1';
        wait for 45*2*CLK_HPER;
        pwm <= '0';
        wait for 10*2*CLK_HPER;
        pwm <= '1';
        wait for 45*2*CLK_HPER;
        pwm <= '0';
        wait until write='1';
        if (count /= std_logic_vector(to_unsigned(period, 32))) then
          REPORT "Incorrect COUNT result" SEVERITY FAILURE;
          passing <='0';
        end if;
        
        done <='1';
                        
        if (passing = '0') then
            -- output test faulure?
        end if;
        
        wait;
    end process;
        
    clock_proc: process
    begin
        clk <= '0';
        wait for CLK_HPER;
        clk <= '1';
        wait for CLK_HPER;
        if (done = '1') then wait; end if;
    end process;

end testbench;

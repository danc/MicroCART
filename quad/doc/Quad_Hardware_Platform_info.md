# Intro
This document is intended to provide you with a list of important things in the Vivado design.
It is not necessarrily equivalent to the full block diagram/vivado project, but it aims to document how those were built.

## Blocks
The design includes:
 - 1 Zynq Processing System block
 - 6 PWM Recorder Blocks
 - 4 PWM Generator Blocks
 - Any debugging GPIOs.

## Settings
[TODO: add project-wide settings like part number]

### Zynq Processing System
Within the PS, we use both I2C controllers, a UART for the wifi bridge, and other peripherials. This is where most of the settings are in this project.

#### I2C0
 - Enabled: true
 - Pin Assignment: MIO 10..11

#### I2C1
 - Enabled: true
 - Pin Assignment: MIO 12..13

#### UART0
 - Enabled: true
 - Pin Assignment: EMIO

#### UART1
 - Enabled: true
 - Pin Assignment: MIO 48..49

#### Clock Configuration
 - FCLK0
   - Enabled: true
   - Frequency: 100 MHZ

### AXI Bus Modules
The axi bus modules do not require configuration.
It is unlikely that the custom blocks will ever have useful configuration parameters as they are one-off and will be set for what the project needs.

## Unclarities
This section is for things that are in the design, but we don't know why (passed down over time and not yet investigated).

### PL Interrupts
The old [XPS] design had 1 interrupt enabled, that the PS configuration says is connected to the AXI Timer, but it is not connected in the graphical design view or the system.mhs file.
It is unclear if this is just a bug in XPS (seems likely) or if the interrupt is actually connected. Code analysis to see if th GIC is used has not been done.

### Clock Configuration
The clock configurations in the old XPS proejct are at slightly lower frequencies than the Vivado defaults.
This could mean that the old team has some problems running at max and backed it down some, or that Xilinx upped the default clock frequency between the Zybo part and the Zybo Z7020.
E.g. The CPU clock is at 650 MHz in the old project, but the defaults in Vivado are 667 MHz.

Dr. Jones agrees that it is likely a default settings change and can be ignored. 
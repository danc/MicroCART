# Zybo Pinout Assignments

The Zybo board has 6 PMOD Connectors. We use these for most quad I/O.

![image](/uploads/a3639b6fb0b2aba8ccdf1150d80fb21c/image.png)

## PMOD Assignment Table
```
Zybo PMOD Pin : Zync Pin : Xilinx Pin Name : Our Assignment

JF1  : MIO-13 : N/A : I2C1 SDA
JF2  : MIO-10 : N/A : I2C0 SCL
JF3  : MIO-11 : N/A : I2C0 SDA
JF4  : MIO-12 : N/A : I2C1 SCL
JF7  : MIO-0  : N/A : unused
JF8  : MIO-9  : N/A : unused
JF9  : MIO-14 : N/A : unused
JF10 : MIO-15 : N/A : unused

JE1  : V12 : IO_L4P_T0_34     : pwm_recorder_0
JE2  : W16 : IO_L18N_T2_34    : pwm_recorder_1
JE3  : J15 : IO_25_35         : pwm_recorder_2
JE4  : H15 : IO_L19P_T3_35    : pwm_recorder_3
JE7  : V13 : IO_L3N_T0_DQS_34 : pwm_signal_out_wkillswitch_0
JE8  : U17 : IO_L9N_T1_DQS_34 : pwm_signal_out_wkillswitch_1
JE9  : T17 : IO_L20P_T3_34    : pwm_signal_out_wkillswitch_2
JE10 : Y17 : IO_L7N_T1_34     : pwm_signal_out_wkillswitch_3

JD1  : T14 : IO_L5P_T0_34       : unused
JD2  : T15 : IO_L5N_T0_34       : unused
JD3  : P14 : IO_L6P_T0_34       : unused
JD4  : R14 : IO_L6N_T0_VREF_34  : unused
JD7  : U14 : IO_L11P_T1_SRCC_34 : pwm_recorder_4
JD8  : U15 : IO_L11N_T1_SRCC_34 : pwm_recorder_5
JD9  : V17 : IO_L21P_T3_DQS_34  : unused
JD10 : V18 : IO_L21N_T3_DQS_34  : unused

JC1  : V15 : IO_L10P_T1_34 : unused
JC2  : W15 : IO_L10N_T1_34 : processing_system7_0_UART0_TX_pin
JC3  : T11 : IO_L1P_T0_34  : processing_system7_0_UART0_RX_pin
JC4  : T10 : IO_L1N_T0_34  : unused
JC7  : W14 : IO_L8P_T1_34  : unused
JC8  : Y14 : IO_L8N_T1_34  : processing_system7_0_UART1_TX_pin
JC9  : T12 : IO_L2P_T0_34  : processing_system7_0_UART1_RX_pin
JC10 : U12 : IO_L2N_T0_34  : unused

JB1  : T20 : IO_L15P_T2_DQS_34 : unused
JB2  : U20 : IO_L15N_T2_DQS_34 : unused
JB3  : V20 : IO_L16P_T2_34     : unused
JB4  : W20 : IO_L16N_T2_34     : unused
JB7  : Y18 : IO_L17P_T2_34     : unused
JB8  : Y19 : IO_L17N_T2_34     : unused
JB9  : W18 : IO_L22P_T3_34     : unused
JB10 : W19 : IO_L22N_T3_34     : unused

JA1  : N15 : IO_L21P_T3_DQS_AD14P_35 : unused
JA2  : L14 : IO_L22P_T3_AD7P_35      : unused 
JA3  : K16 : IO_L24P_T3_AD15P_35     : unused
JA4  : K14 : IO_L20P_T3_AD6P_35      : unused
JA7  : N16 : IO_L21N_T3_DQS_AD14N_35 : unused
JA8  : L15 : IO_L22N_T3_AD7N_35      : unused
JA9  : L16 : IO_L16P_T2_34           : unused
JA10 : L14 : IO_L14P_T2_AD4P_SRCC_35 : unused

```
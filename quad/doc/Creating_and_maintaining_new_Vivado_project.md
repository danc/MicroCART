# Creating a new Zybo Z7 project in Vivado
First, use the `createNew.sh` script in `vivado_workspace/project_tcl`
you can run the script from wither the `project_tcl` folder, or the `vivado_workspace`.
This will generate a new project and it's raw project tcl.
Any tiem you add sources, change the block diagram, or otherwise change with settings, run `write_project_tcl <projname>.tcl` at the tcl console.
**YOU ARE NOT DONE YET**

# Necessarry Changes to tcl files
I feel that there should be a way around this, but I haven't found it yet.
- You need to change the project to auto-generate a new wrapper when re-created.
This is done by removeing the follwoing lines from the .tcl file

```
    # Import local files from the original project
    set files [list \
     [file normalize "${origin_dir}/<project_name>/<project_name>.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.vhd" ]\
    ]
    set imported_files [import_files -fileset sources_1 $files]

    # Set 'sources_1' fileset file properties for remote files
    # None

    # Set 'sources_1' fileset file properties for local files
    set file "hdl/design_1_wrapper.vhd"
    set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
    set_property -name "file_type" -value "VHDL" -objects $file_obj
```
and adding the following at the end of the file:
```
    make_wrapper -files [get_files ${origin_dir}/${_xil_proj_name_}/${_xil_proj_name_}.srcs/sources_1/bd/design_1/design_1.bd] -top
    add_files -norecurse ${origin_dir}/${_xil_proj_name_}/${_xil_proj_name_}.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.vhd
```
Additionally, you need to make the constraints file a remote source, not a local one.
Replace
````
set file "[file normalize "$origin_dir/<project_name>/src/constrs/Zybo-Z7-Master.xdc"]"
set file_imported [import_files -fileset constrs_1 [list $file]]
set file "constrs/Zybo-Z7-Master.xdc"
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property -name "file_type" -value "XDC" -objects $file_obj

````
with
````
add_files -fileset constrs_1 -norecurse ${origin_dir}/${_xil_proj_name_}/src/constrs/Zybo-Z7-Master.xdc
````

# Version Control
At the top of the tcl file for the project, there is a list of files that need to be in version control.
All of these should be added to the repo, as well as the tcl script itself, but not the `design_1_wrapper.vhd` file.
No other files for this project need be added to the repo. Any time you add a source file, please add it under src, not the auto-generated folder.
If you add it to the auto-generated folder, the scriopt will not be able to build the project as it will delete the file before building and then be unable to find it.

## I made a mistake and added a new file to the default folder
1. Don't do that again please
2. We can fix it:

1. Go in to the folder structure and copy the file from the bad place (see file properties in Vivado) to `<project>/src/...`.
2. Go into Vivado and delete the old file from the project.
3. In Vivado, add the new file to the project.
4. Update git to revision the new file.
5. Run `write_project_tcl <projname>.tcl` from the tcl console in vivado. and go up to [Necessarry Changes to tcl files](#necessarry-changes-to-tcl-files).
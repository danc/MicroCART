# Unity Notes

- When using Unity, be sure to define `UNITY_INCLUDE_CONFIG_H`
to use the [`unity_config.h`](/Unity/src/unity_config.h). (Details on why are in the
[Unity Configuration Guide](/Unity/docs/UnityConfigurationGuide.md).)
  - For the quad directory, this is done at compile-time of the Unity library [_here_](/Unity/src/Makefile#L4)
and at compile-time of the test runner [_here_](/quad/library.mk#L58). Both rely on the flag defined
[_here_](/quad/library.mk#L19).
- Follow the examples in the [`Unity/examples`](/Unity/examples) for a guide, as well
as the documentation in [`Unity/docs`](/Unity/docs)
  - The [Unity Getting Started guide](/Unity/docs/UnityGettingStartedGuide.md) and the
[Unity Assertions Cheat Sheet Suitable for Printing and Possibly Framing]
(/Unity/docs/UnityAssertionsCheatSheetSuitableforPrintingandPossiblyFraming.pdf)
are nice references
  - Generally, the format is to start `main()` with `UNITY_BEGIN()`, followed by calls to
`RUN_TEST(test_function)` where `test_function` is a test to run, ended with
`return UNITY_END()`. Additionally, each `test_function` should be a `void` and call the
[appropriate asserts](/Unity/docs/UnityAssertionsReference.md).

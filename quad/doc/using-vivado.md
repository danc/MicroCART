# Launching
To launch vivado from co3050-12, open a terminal and run

    $ source /remote/Xilinx/2018.2/Vivado/2018.2/settings64.sh
    $ vivado &

This will launch vivado in gui mode. See the vivado documentation for other modes.


# Startup Scripts
If you want to run any tcl scripts on startup, such as installing board backages
source them in `/local/ucart/.Xilinx/Vivado/Vivado_init.tcl` This will run on startup.

# Simulation
[This ug](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2018_1/ug937-vivado-design-suite-simulation-tutorial.pdf) has info on Vivado simulation (in batch mode too).
This should be usable for automated regression tests.

*This document will get updated as needed*
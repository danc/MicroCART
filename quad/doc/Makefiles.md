#  How to use the makefiles
## [library.mk](../library.mk)
This file contains many definitions of make targets for building libraries and building and running their corresponding tests.
A set of variable definitions are at the top of the file.
Within this file, the "User Targets" are defined for building a library (`default`),
building and running tests (`test`), and cleaning the library (`clean`).
The "Internal Targets" are used for building the library and test binary and rely on abstraction.

## [executable.mk](../executable.mk)
This file contains definitions of make targets for building executables.
Similar to `library.mk`, this file has a set of variable definitions at the top of the file.
The "User Targets" are defined for building the executable and cleaning.
The "Internal Targets" are used for building the executable and rely on abstraction.

## [quad/Makefile](../Makefile)
This Makefile contains targets for building libraries and binaries and their associated tests.
In this file, the specific libraries/executables and their locations are listed,
as opposed to the `.mk` files which have purely abstract definitions.
It also contains a `clean` target for removing compiled libraries and binaries.
A target for generating an image of the control network, called `diagram`, is also located here.

## Library Makefile
Each library needs its own Makefile.
This contains a set of definitions for variables, so that the abstract targets in `library.mk`
can work properly, and any additional targets you want to add that are specific to this library.

The following variable definitions should be included:
- `NAME`: The name of the library
- `TOP`: a relative path to the `quad/` directory
- `REQLIBS`: a list of the libraries necessary to compile this library, in the form of compiler flags, i.e. `-l<name>`

_Note: The `REQLIBS` list should include the name of this library at a minimum,
as any tests associated with it would need its own library to be linked in order to be useful._

Additionally, a line like the following should be included:

`include $(TOP)/library.mk`

This allows the library to use the targets in the `library.mk` file to be applied to this directory.
Any other make targets wanted for the library should also be included.

## How to add a library
To add a library:
1. Add your library to the `libs` and `clean` targets in [`quad/Makefile`](../Makefile) with a path to the directory containing the library
1. Add a Makefile to the new library with the variable definitions in it as described above
1. Update the `REQLIBS` variable for any library or executable that needs this new library to compile

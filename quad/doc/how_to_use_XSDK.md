# How to use XSDK

## Introduction

In this guide we will cover the following topics regarding XSDK:
- What Is XSDK
- How to open XSDK
- How to create a BSP
- How to create a new application project
- How to configure JTAG
- How to launch an Application Project
- How to boot using an SD card


## What is XSDK?

XSDK stands for Xilinx Software Development Kit and it does exactly that.
It builds the project C files and necessary packages into files that allow the
 developer to program the Zybo board to whatever he/she desires granted it meets
 the system constraints.

There are three main things required for a project:
  1. A system hardware platform (automatically exporting design from XPS)
  1. A Board Support Package (Contains software functions for interacting with
    the Processing System controllers i.e. UART and I2C or logic cores on the FPGA)
  1. An Application Project (A simple hello world, an NES emulator, or something else)

As mentioned above the system_hw_platform is imported for us by XPS when we
export our bitstream file to the XSDK. We’ll walkthrough the other two items
as well as programming the FPGA from within the XSDK.

## Opening XSDK

Setting up access to the Xilinx tools is fairly straight forward given the
machine the user is on. This guide
will cover three types of machines: Coover 3050-11 and -12 computers
(highly recommended), ISU’s
Remote Linux Servers, and a user’s own PC

1.  Coover 3050-11 and -12
Two machines in the Distributed Sensing and Decision Making Lab (Coover 3050)
come with the
tools already installed. However, the following steps need to be taken in
order to launch the
program
    - In a terminal, enter `source /opt/Xilinx/2018.2/Vivado/2018.2/settings64.sh`
    - In terminal type `xsdk &`
1. ISU Remote Linux Servers (linux-X, research-x.ece.iastate.edu)
    - `source Xilinx_Tools/setup_scripts/remote_servers/setup.sh`
    - In terminal type `xsdk &` (Note: these servers are not good for
      programming the Zybo board when it comes time to launch a program on the board)
1. User PC
    Some users may opt to download the Xilinx tools on their own PCs for
    development, but this is not recommended.
    - Download the Vivado Design Suite [here](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools.html) (~6GB)
    - Run XSDK


## Creating a new Board Support Package (bsp)

The BSP package is responsible for grabbing all necessary Xilinx library files
so they can be called by your program. In other words, it holds the drivers
necessary to interface with the hardware on the board.   
  1. Click File and select New->Board Support Package
  1. In the New Board Support Package Project, enter a project name
  (system_bsp in the example).  
  1. Ensure system_hw_platform and ps7_cortexa9_0 are selected in Target Hardware
  1. Ensure “standalone” is selected.
  1. Click Finish to create the BSP  

  __IMPORTANT:__ If UART0 is enabled, follow the steps below, otherwise ensure
   these are set properly anyway.   
  1. Click on “standalone” and change the
    value of stdin AND stdout to ps7_uart_1  
  1. Click OK when done, you should now see the BSP in the project explorer

##  Creating a new Application Project
Lastly, we have the software program that will run on the board. This is the
last piece to the Zybo puzzle, and we’re almost there. Projects can be written
in C or C++, and can use some standard libraries like stdio, stdlib, string,
and otherse. HOWEVER, some libraries (often from Linux) are not implemented
(like time.h), and alternate methods must be used for some things an OS would
normally handle. Hopefully you do not run into these instances.
(Off topic hint: If you need timing things, check out xtime_l.h)
1. Click File -> New -> Application Project to open the New Project box  
1. Enter a project name, and select the “Use existing” radio button for the
BSP. We want to use our newly created one rather than create one.   
1. Click next and select a template (Hello World is probably best) and hit Finish
1. The new project should appear on the Project Explorer. Expand the project,
src, and open the helloworld.c  

## Configuring JTAG

We now have all the components, we’re now ready to program the board, but wait,
 we haven’t setup how we’re going to communicate with the board.  You may now
 plug in the board to the development computer (yours, Coover machine, etc.)
 Flip the switch to power on the board.  

  1. In the menu bar, click Xilinx Tools -> Configure JTAG
  1. If the board is plugged into Coover 3050-11,-12 or a personal PC, then use
   Auto Detect (It is ill-advised to be programming the Zybo board from a remote
   linux machine at this point)  
  1. Click OK   

## Launching an Application Project
With all our pieces ready to go, we can finally program the board and launch
 our software program.  

_IMPORTANT:_ A COM terminal should be open after the board has been turned on
AND before launching the program. Either go through Putty on Windows (COMn
  Baud 115200) or if through a linux machine (sudo screen /dev/ttyUSB1 115200).

1. On the menu bar, click Xilinx Tools -> Program FPGA, and click Program to
start the process
1. If the JTAG configurations and setup were correct, the bitstream should
load onto the board and a blue LED should turn on indicating the board
is DONE programming.  
1. With the board program, we can now launch our software, click on the
Application Project we want to launch and click the Green Play Arrow button
 to start.  
1. View the data received from the board through the COM terminal.

## Booting on SD

The `boot.bin` file is the file that the ZYBO board used on the quad uses to
produce the hardware platform and run the software for our program to run.
In a standalone application(no operating system) this should be the only
file placed on the SD-card.
### What to Do Before

To correctly make a boot.bin file (using no operating system) you must have
the following:
- System hardware platform `.bit` file
  - Output of hardware toolset (Vivado)
  - Path: `<workspace>/sytem_hw_platform/system.bit`
- Application Project `.elf` file
  - Be sure to confirm project is built in XSDK
  - Path: `<workspace>/<app_project>/Realease/real_quad.elf`
- Board Support Package `.elf` file
  - The current boot loader is a default a standalone for the Zybo board
  - Be sure to confirm project is built in XSDK
  - Path: `<workspace>/zybo_fsbl/Realease/zybo_fsbl.elf`

NOTE: None of the `.elf` files are on GIT to reduce clutter
NOTE: `<workspace>` is the `MicroCART/quad/vivado_workspace/<viado_project>/<viado_project>.sdk` directory for the single-module HW test designs. 

### Instructions

In order to correctly create the boot file you must have all the above files
updated.

Within XSDK:
  1. Project -> Create Boot Image. Alternatively right-click on the project in
  the Project Explorer and -> Create Boot Image  
  1. Select `<workspace>/zybo_fsbl/Realease/` as the `.bif`
  1. Click add
  1. Select `<workspace>/zybo_fsbl/Realease/zybo_fsbl.elf`
  1. Confirm bootloader is checked and add the file to the Boot Image
  1. Click add
  1. Select `<workspace>/sytem_hw_platform/system.bit`
  1. Add the file to the Boot Image
  1. Click add
  1. Select `<workspace>/<app_project>/Realease/real_quad.elf`
  1. Add the file to the Boot Image
  1. Copy `Boot.bin` file at `<workspace>/zybo_fsbl/Realease/` to SD Card
  1. Plug SD card into quad and power on board

_NOTE:_ If anything here is unclear there is a deeper explanation in
[Xilinx: how to create a Boot Image](https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/SDK_Doc/tasks/sdk_t_create_zynq_boot_image.htm)

## XSDK Quirks

  The Xilinx SDK has a few quirks that are important to watch out for:
   - From the [documentation](https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/SDK_Doc/tasks/sdk_t_tcf_limitations_faq.htm)
   , if you abort program execution while at a breakpoint inside an interrupt
   handler, when re-running the program, interrupts don’t fire. You have to do
   a hard reset of the board (cycle power) to have interrupts work again.
   - After doing a `git pull` or `git checkout`, refresh the files by right-
   clicking on the project in the sidebar and clicking "Refresh"
   - The project does not detect changes in header files, so if you modify a
   `.h` file, you should do a clean before re-building, otherwise you may
   experience unexpected behavior. We got into the habit of always doing a
   clean before a build whenever creating code that will be put on the quadcopter.

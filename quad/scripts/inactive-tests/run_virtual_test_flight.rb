#!/usr/bin/env ruby

# Test Flight
#
# A simple virtual test flight (take off, hover, and set back down)
#

script_dir = File.expand_path(File.dirname(__FILE__))
require script_dir + "/testing_library"

bin_dir = script_dir + "/../../bin/"
Dir.chdir(bin_dir)

puts("Firing up the quad...")

# Start virtual quad
quad = Process.spawn("./virt-quad -q")

delay_spin_cursor(3)

##################
#  Begin Flight!
##################

begin
  puts("Starting flight...")

  # Set gravity
  File.write(I2C_MPU_ACCEL_Z, -1 * GRAVITY)

  puts("Turning on GEAR...")
  File.write(GEAR, GEAR_ON)
  sleep 0.015

  puts("Increasing Thrust to half maximum...")
  for i in (THROTTLE_MIN..THROTTLE_MID).step(1000)
    File.write(THROTTLE, i)
    sleep 0.005
  end

  puts("Hovering for 10 seconds")
  delay_spin_cursor(10)

  puts("Relaxing thrust to zero")
  i = THROTTLE_MID
  while i > THROTTLE_MIN
    i -= 1000
    File.write(THROTTLE, i)
    sleep 0.005
  end

  puts("Swiching off GEAR...")
  File.write(GEAR, GEAR_OFF)

  # puts("Collecting logs...")
  # msg = ""
  # misses = 0
  # fifo = File::open(UART_TX)
  # while misses < 10
  #   puts "trying..."
  #   if fifo.eof?
  #     misses += 1
  #     next
  #   end
  #   msg += fifo.read()
  # end
  
  # fifo.close()

# puts msg

  puts("Flight ended successfully.");
ensure

  Process.kill(9, quad)

end


#!/usr/bin/env ruby

# Logging test
#
# Let the quad fly for a bit, and then ensure we can
# get its logs

script_dir = File.expand_path(File.dirname(__FILE__))
require script_dir + "/testing_library"

bin_dir = script_dir + "/../../bin/"
Dir.chdir(bin_dir)


puts("Setting up...")

# Start virtual quad
quad_pid = Process.spawn("./virt-quad start -q",
                         { :rlimit_as => 536870912, # 512 MiB total RAM
                           :rlimit_stack => 1048576}) # 1 MiB stack

sleep 0.5

# Set RC switches
set_gear GEAR_OFF
set_flap FLAP_OFF

# Set initial quad orientation (flat on the ground, facing forward)
`./virt-quad set i2c_imu_x 0`
`./virt-quad set i2c_imu_y 0`
`./virt-quad set i2c_imu_z -1`
`./virt-quad set rc_roll 0.498`
`./virt-quad set rc_pitch 0.497`
`./virt-quad set rc_yaw 0.498`


#################
#  Begin Tests
#################

begin

  puts "------------------------------------------"
  puts "-- Beginning logging test..."
  puts "------------------------------------------"

  # Set initial quad orientation (flat on ground, facing forward)
  `./virt-quad set i2c_imu_x 0`
  `./virt-quad set i2c_imu_y 0`
  `./virt-quad set i2c_imu_z -1`
  `./virt-quad set rc_roll 0.498`
  `./virt-quad set rc_pitch 0.497`
  `./virt-quad set rc_yaw 0.498`

  puts("Turning on GEAR...")
  set_gear GEAR_ON
  set_flap FLAP_ON
  sleep 0.015

  puts("Increasing Thrust to half maximum...")
  for i in (THROTTLE_MIN..THROTTLE_MID).step(0.01)
    set_throttle(i)
    sleep 0.005
  end

  puts("Hovering for 1 seconds")
  sleep 1

  puts("Switching to autonomous and hovering for 1 seconds")
  set_flap FLAP_OFF
  sleep 1

  puts("Switch back to manual, relaxing thrust to zero")
  set_flap FLAP_ON
  i = THROTTLE_MID
  while i > THROTTLE_MIN
    i -= 0.01
    set_throttle(i)
    sleep 0.005
  end

  # Get logs

  Thread.new {
    sleep 1
    puts("Switching off GEAR...")
    set_gear GEAR_OFF
  }

  puts("Preparing to receive logs. This might take a while. You can ctrl-c to quit early.")
  logs = []

  begin
    while true
      logs.push(recv_packet)
    end
  rescue Exception
  end

  for data in logs
    puts data
  end

  puts "#{logs.length} total logs recieved"

  puts "------------------------------------------"

ensure

  Process.kill(9, quad_pid)
  Process.wait(quad_pid)

end

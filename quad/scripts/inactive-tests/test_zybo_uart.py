#!/usr/local/bin/python3.6

import sys
import time

import serial

def do_test():
    with serial.Serial('/dev/ttyUSB0', 921600, timeout=5) as ser:
        ser.reset_input_buffer()
        send_bytes = bytes(i % 256 for i in range(128))
        #send_bytes = b'sdjjjjjjjjjj9ssssssssssssnnnnnnnnnflouirgaorifa;eofija;ogijasfhluiasflawieufzxcvwe'
        ser.write(send_bytes)
        print("Sending {} bytes".format(len(send_bytes)))
        time.sleep(1)
        recv_bytes = bytes()
        while ser.in_waiting != 0:
            recv_bytes = ser.read(len(send_bytes))
        if recv_bytes == send_bytes:
            print(recv_bytes);
            print("Test Successful")
        else:
            print(recv_bytes);
            print("Test Failed.")
if __name__ == '__main__':
    for i in range(1):
        print("Test ", i)
        do_test()
        time.sleep(1)

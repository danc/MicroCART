#!/usr/bin/env ruby

# UART test
#
# This test is pretty simple, just a UART smoke test, using
# the debug callback on the quad

script_dir = File.expand_path(File.dirname(__FILE__))
require script_dir + "/testing_library"

bin_dir = script_dir + "/../../bin/"
Dir.chdir(bin_dir)

Timeout::timeout(30) {

  puts("Setting up...")

  # Start virtual quad
  quad_pid = Process.spawn("./virt-quad start -q",
                           { :rlimit_as => 536870912, # 512 MiB total RAM
                             :rlimit_stack => 1048576}) # 1 MiB stack

  sleep 0.5

  # Set RC switches
  set_gear GEAR_OFF
  set_flap FLAP_OFF

  # Set initial quad orientation (flat on the ground, facing forward)
  `./virt-quad set i2c_imu_x 0`
  `./virt-quad set i2c_imu_y 0`
  `./virt-quad set i2c_imu_z -1`
  `./virt-quad set rc_roll 0.498`
  `./virt-quad set rc_pitch 0.497`
  `./virt-quad set rc_yaw 0.498`


  #################
  #  Begin Tests
  #################

  begin

    puts "------------------------------------------"
    puts "-- Beginning basic communication test..."
    puts "------------------------------------------"

    for j in 1..10

      # Send a debug command
      Thread.new {
        sleep 0.5
        send_packet [0xBE, 1, 0, 0, 0, 0, 0, 0xBF]
      }

      fifo = File.open(UART_TX)

      # Receive the header
      msg = []
      for i in 1..7
        sleep 0.01
        c = fifo.read(1)
        msg.push(c)
      end

      # Receive the remaining data, according to the header specified length
      length = msg[5..7].join().unpack("S")[0]

      msg = []
      for i in 1..length
        sleep 0.01
        c = fifo.read(1)
        msg.push(c)
      end
      fifo.close

      msg = msg.join()
      puts msg
      assert_equal(msg.force_encoding("UTF-8"), "Packets received: #{j}")
    end

    puts "Basic communication test passed."
    puts "------------------------------------------"

  ensure

    Process.kill(9, quad_pid)
    Process.wait(quad_pid)

  end
}

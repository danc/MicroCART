#!/bin/bash

set -e

Xvfb :1 -ac -screen 0 1024x768x8 &
VIRT_SCREEN=$!
export DISPLAY=:1

source /remote/Xilinx/2015.4/SDK/2015.4/settings64.sh

echo "Building modular_quad_pid"

if [ ! -d ../.metadata ]; then
    # Haven't configured XSDK environment yet. Do that now

    ECLIPSE=/remote/Xilinx/2015.4/SDK/2015.4/eclipse/lnx64.o/eclipse
    VM=/remote/Xilinx/2015.4/SDK/2015.4/tps/lnx64/jre/bin
    WSPACE=$(dirname $0)/..
    BSP=$WSPACE/system_bsp
    FSBL=$WSPACE/zybo_fsbl
    FSBL_BSP=$WSPACE/zybo_fsbl_bsp
    HW=$WSPACE/system_hw_platform
    APP=$WSPACE/modular_quad_pid

    echo "Setting up dependencies for modular_quad_pid"

    # Import the system_hw_platform and app into the workspace
    $ECLIPSE -vm $VM -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild \
        -import $HW \
        -import $APP \
        -data $WSPACE \
        -vmargs -Dorg.eclipse.cdt.core.console=org.eclipse.cdt.core.systemConsole

    # Create the BSP
    xsct .create_bsp.tcl

    # Create the FSBL project
    xsct .create_fsbl.tcl

    $ECLIPSE -vm $VM -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild \
        -import $BSP \
        -import $FSBL \
        -import $FSBL_BSP \
        -data $WSPACE \
        -vmargs -Dorg.eclipse.cdt.core.console=org.eclipse.cdt.core.systemConsole

    # Build everything
    $ECLIPSE -vm $VM -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild \
        -build all \
        -data $WSPACE \
        -vmargs -Dorg.eclipse.cdt.core.console=org.eclipse.cdt.core.systemConsole

fi

xsct .build_app.tcl

kill $VIRT_SCREEN

#!/bin/bash

set -e

QUADDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
WS=$QUADDIR/xsdk_workspace

make -C $WS/modular_quad_pid/Debug clean
make -C $WS/modular_quad_pid/Release  clean
make -C $WS/system_bsp clean
make -C $WS/zybo_fsbl/Debug clean
make -C $WS/zybo_fsbl/Release clean
make -C $WS/zybo_fsbl_bsp clean

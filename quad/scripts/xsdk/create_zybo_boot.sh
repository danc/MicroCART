#!/bin/bash
set -e

QUADDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
WS=$QUADDIR/xsdk_workspace

PATH=$PATH:/remote/Xilinx/14.7:/opt/Xilinx/14.7/ISE_DS
source settings64.sh

# Working directory:
cd $QUADDIR/out

echo "the_ROM_image:" > zybo_fsbl.bif
echo "{" >> zybo_fsbl.bif
echo $WS/zybo_fsbl/Release/zybo_fsbl.elf >> zybo_fsbl.bif
echo $WS/system_hw_platform/system.bit >> zybo_fsbl.bif
echo $WS/modular_quad_pid/Release/modular_quad_pid.elf >> zybo_fsbl.bif
echo "}" >> zybo_fsbl.bif
bootgen -image zybo_fsbl.bif -o BOOT.bin
rm zybo_fsbl.bif
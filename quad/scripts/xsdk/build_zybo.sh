#!/bin/bash

set -e

QUADDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
WS=$QUADDIR/xsdk_workspace

PATH=$PATH:/remote/Xilinx/14.7:/opt/Xilinx/14.7/ISE_DS
source settings64.sh

make -C $WS/system_bsp all
make -C $WS/modular_quad_pid/Release all
make -C $WS/zybo_fsbl_bsp all
make -C $WS/zybo_fsbl/Release all

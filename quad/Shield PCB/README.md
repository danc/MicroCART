# Zybo PCB Shield
The creation of the custom PCB shield was inspired by a fateful crash during one of MicroCART's demos for the 2018 ECpE Scholar's Fair. Multiple connectors were disconnected by the impact of the crash and the MicroCART team (who had just begun the project and were not familiar with the hardware setup) needed to find the issue quickly and resume demos. The creation of the PCB is intended to spare future groups from this particular set of hardware problems.

The PCB is meant to act as a custom shield for the Zybo boards mounted on the quads. It serves 3 main functions:
1.  Simplify quad hardware assmebly and secure connections to external devices (sensors, wifi bridge, motors, etc) using latching connectors.
2.  Provide multiple test points where the user can probe for signals even when the quad is in operation.
3.  Incorporate a voltage regulator to step the battery voltage down while also saving power and maintaining consistent output voltages.


The layout and schematic of the board can be seen below. It has 4 different layers, each represented by a different color. These images are screenshots taken in MultiSim and UltiBoard - the software we used to design the PCB.
![image](/quad/doc/images/PCB_r2_layout.PNG)
![image](/quad/doc/images/PCB_r2_schematic.PNG)



# Future PCB Revisions
Because it's possible that future teams may wish to change the hardware used on the quad, we have included the MultiBoard and UltiSim design files and the Bill of Materials used for creating and assembling the PCB in Shield_PCB.zip in this folder.

For more information on how to design and build PCBs, check out the useful tutorials below from EE 333 (taught by Dr. Tuttle). They walk you through the basics of using MultiSim and UltiBoard, as well as provide some information about PCB components and design.
 * [Tutorial 1 - Circuit Design, Schematic Creation with MultiSim, Component Footprints](/quad/doc/pcb_layout_tutorial_1.pdf)  
 * [Tutorial 2 - Transferring Board Schematic to Layout in UltiBoard, Arranging and Connecting Parts](/quad/doc/pcb_layout_tutorial_2.pdf) 
 * [Tutorial 3 - Design Rule Check, Exporting Gerber Files for the Manufacturer](/quad/doc/pcb_layout_tutorial_3.pdf)  
 

# Errata/Easy Mistakes
## Version 2
 - UART0 lines (to WiFi) are flipped, can be fixed in constraints file.
 - RC controller PWM lines are reconfigurable in software and constraints,
 and depend on how the RC receiver is wired, so double check that you get good results if you change those.
 - Motor PWM lines are reconfigurable in software, and have to match the controller, so be careful with those as well.
 - The IMU has to be mounted with the label text down (the lables for the pins) and the mounting holes facing towards the PMOD test points
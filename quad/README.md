# Quadcopter

The `quad/` directory contains all code that programs the quadcopter. This
includes any C libraries we have written, any HDL to program the Zybo on
the quad, and the XSDK main project that runs on the Zybo.

## Intro

There are three main portions to the system that runs the quad

- Quad Hardware
- Quad Software Drivers
- Control Loop


#### Quad Hardware
The quad hardware is located at `xsdk_workspace/system_hw_platform`. This
consists of the vhdl and associated wrapper code that specifies the hardware on
the FPGA.

Below is a block diagram of the hardware/software implementation:

![image](doc/images/FPGA_Diagram.png)

#### Quad Software Drivers
The quad software drivers is all of the code that interacts with the sensors of
 the quad and collects the data for the control loop to use.
For example receiving coordinates over wifi and  reading measurements from the
 accelerometer.

To run this application on the Zybo, we need to implement the hardware drivers,
and provide a `main` function.
This is done in `xsdk_workspace/real_quad/src` To build the XSDK project,
refer to the [XSDK instructions](xsdk_workspace/README.md).

We can also run this application on our own laptops for testing.
We call this implementation the "virtual quad".
This is modified in `src/virt_quad`.

#### Control Loop
The control loop is the algorithm that runs within the quad software drivers.
This code determines what the code should do to keep the quad flying and as
close to the waypoint
 as possible.

The main quad control application is located at `src/quad_app/`.
This entails a group of C and header files that work as drivers/interfaces to
the various sensors and communications that are on the quad.

## Building

#### Primarily Libraries and Executables

To build the libraries and executables:
```
make
```

You can also build each library individually inside their respective project
directories:
```
cd src/<project> && make
```

**NOTE**: All build artifacts will be placed in `lib` or `bin` (depending on
whether it is a library or executable, respectively)

To learn more about the makefiles, see [Makefiles.md](doc/Makefiles.md)

## Testing

#### Automated Tests
_Write tests! It makes automating things so much easier._

Run the unit and functional tests:
```
make test
```

And glad to hear you are interested in writing your own unit tests! Look at the
[README](/Unity/README.md) and examples in the Unity testing library at
[`Unity/`](/Unity/). A few notes about using Unity are [here](doc/Unity_notes.md).

#### Manually testing the hardware interface

Of course, we cannot run automated tests on code that needs the Zybo. But
we have manual tests that you can use to test each specific driver in the
hardware interface.

Look in `xsdk_workspace/real_quad/src/hw_impl_zybo_tests.c` for instructions.
Ideally, you would run these tests from the XSDK IDE.

## Sections
* [The Quad Application](src/quad_app/README.md)  
* [Quad App on the Zybo (Real Quad)](xsdk_workspace/real_quad/README.md)  
* [Quad App on Unix (Virtual Quad)](src/virt_quad/README.md)  
* [XSDK Instructions](xsdk_workspace/README.md)

## Other Documents
* [Hardware Block Diagram](doc/images/FPGA_Diagram.png)
* [Zybo Pinout Assignments](doc/zybo_pinout_assignments.md)
* [How to use Xilinx software tools](doc/how_to_use_XSDK.md)  
* [How to use the makefile structure](doc/Makefiles.md)
* [Unity notes](doc/Unity_notes.md)

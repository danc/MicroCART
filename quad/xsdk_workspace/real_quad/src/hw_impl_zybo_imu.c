#include "hw_iface.h"
#include "hw_impl_zybo.h"
#include "type_def.h"

#include <string.h>

#define MPU9150_DEVICE_ADDR 		0b01101000
#define MPU9150_COMPASS_ADDR 		0x0C

#define ACCEL_GYRO_READ_SIZE 		14		//Bytes
#define ACCEL_GYRO_BASE_ADDR		0x3B	//Starting register address

#define MAG_READ_SIZE 				6
#define MAG_BASE_ADDR 				0x03

#define MAG_DRDY_TIMEOUT			(10)

#define RAD_TO_DEG 57.29578
#define DEG_TO_RAD 0.0174533

// Array indicies when reading from ACCEL_GYRO_BASE_ADDR
#define ACC_X_H 0
#define ACC_X_L 1
#define ACC_Y_H 2
#define ACC_Y_L 3
#define ACC_Z_H 4
#define ACC_Z_L 5

#define GYR_X_H 8
#define GYR_X_L 9
#define GYR_Y_H 10
#define GYR_Y_L 11
#define GYR_Z_H 12
#define GYR_Z_L 13

#define MAG_X_L 0
#define MAG_X_H 1
#define MAG_Y_L 2
#define MAG_Y_H 3
#define MAG_Z_L 4
#define MAG_Z_H 5

// Gyro is configured for +/-2000dps
// Sensitivity gain is based off MPU9150 datasheet (pg. 11)
#define GYRO_SENS 16.4

int mpu9150_write(struct I2CDriver *i2c, u8 register_addr, u8 data);
int mpu9150_read(struct I2CDriver *i2c, u8* recv_buffer, u8 register_addr, int size);

int mpu9150_calc_mag_sensitivity(struct IMUDriver *self, gam_t *gam);
int mpu9150_read_mag(struct IMUDriver *self, gam_t* gam);
int mpu9150_read_gyro_accel(gam_t* gam);

int zybo_imu_reset(struct IMUDriver *self, gam_t *gam) {
  memset(gam, 0, sizeof(gam_t));

  struct I2CDriver *i2c = self->i2c;

  // Device Reset & Wake up
  mpu9150_write(i2c, 0x6B, 0x80);
  usleep(5000);

  // Set clock reference to Z Gyro
  mpu9150_write(i2c, 0x6B, 0x03);
  // Configure Digital Low/High Pass filter
  mpu9150_write(i2c, 0x1A,0x03); // Level 3 low pass on gyroscope

  // Configure Gyro to 2000dps, Accel. to +/-8G
  mpu9150_write(i2c, 0x1B, 0x18);
  mpu9150_write(i2c, 0x1C, 0x10);

  // Enable I2C bypass for AUX I2C (Magnetometer)
  mpu9150_write(i2c, 0x37, 0x02);

  usleep(100000);

  //Calculate magnetometer sensitivities
  mpu9150_calc_mag_sensitivity(self, gam);

  usleep(10000);

  //Enable single measurement mode
  mpu9150_write(i2c, 0x0A, 0x00);
  mpu9150_write(i2c, 0x0A, 0x01);

  int i;

  // Do about 20 reads to warm up the device
  for(i=0; i < 20; ++i){
    self->read(self, gam);
    usleep(1000);
  }

  return 0;
}

int mpu9150_calc_mag_sensitivity(struct IMUDriver *self, gam_t *gam) {
	u8 buf[3];
	u8 ASAX, ASAY, ASAZ;

	// Quickly read from the factory ROM to get correction coefficients
	int status = mpu9150_write(self->i2c, 0x0A, 0x1F);
	if(status != 0) {
		return status;
	}

	usleep(10000);

	// Read raw adjustment values
	status = mpu9150_read(self->i2c, buf, 0x10,3);
	if(status != 0) {
		return status;
	}
	ASAX = buf[0];
	ASAY = buf[1];
	ASAZ = buf[2];

	// Set the correction coefficients
	gam->magX_correction = (ASAX-128)*0.5/128 + 1;
	gam->magY_correction = (ASAY-128)*0.5/128 + 1;
	gam->magZ_correction = (ASAZ-128)*0.5/128 + 1;

	return 0;
}

int zybo_imu_read(struct IMUDriver *self, gam_t *gam) {
  struct I2CDriver *i2c = self->i2c;
  i16 raw_accel_x, raw_accel_y, raw_accel_z;
  i16 sensor_raw_accel_x, sensor_raw_accel_y, sensor_raw_accel_z;
  i16 gyro_x, gyro_y, gyro_z;
  i16 sensor_gyro_x, sensor_gyro_y, sensor_gyro_z;

  u8 sensor_data[ACCEL_GYRO_READ_SIZE] = {};

  int error = mpu9150_read(i2c, sensor_data, ACCEL_GYRO_BASE_ADDR, ACCEL_GYRO_READ_SIZE);
  if (error) return error;

  //Calculate accelerometer data
  sensor_raw_accel_x = sensor_data[ACC_X_H] << 8 | sensor_data[ACC_X_L];
  sensor_raw_accel_y = sensor_data[ACC_Y_H] << 8 | sensor_data[ACC_Y_L];
  sensor_raw_accel_z = sensor_data[ACC_Z_H] << 8 | sensor_data[ACC_Z_L];

  // TODO: Update Matlab model with correct directions.
  raw_accel_x =  sensor_raw_accel_y;
  raw_accel_y = -sensor_raw_accel_x;
  raw_accel_z =  sensor_raw_accel_z;
//  raw_accel_x = sensor_raw_accel_y;
//  raw_accel_y = sensor_raw_accel_x;
//  raw_accel_z = sensor_raw_accel_z;

  // put in G's
  gam->accel_x = IMU_ACCX_SIGN * (raw_accel_x / 4096.0) + ACCEL_X_BIAS; // 4,096 is the gain per LSB of the measurement reading based on a configuration range of +-8g
  gam->accel_y = IMU_ACCY_SIGN * (raw_accel_y / 4096.0) + ACCEL_Y_BIAS;
  gam->accel_z = IMU_ACCZ_SIGN * (raw_accel_z / 4096.0) + ACCEL_Z_BIAS;

  //Convert gyro data to rate (we're only using the most 12 significant bits)
  sensor_gyro_x = (sensor_data[GYR_X_H] << 8) | (sensor_data[GYR_X_L]); //* G_GAIN;
  sensor_gyro_y = (sensor_data[GYR_Y_H] << 8 | sensor_data[GYR_Y_L]);// * G_GAIN;
  sensor_gyro_z = (sensor_data[GYR_Z_H] << 8 | sensor_data[GYR_Z_L]);// * G_GAIN;

  //remap
  gyro_x =  sensor_gyro_y;
  gyro_y = -sensor_gyro_x;
  gyro_z =  sensor_gyro_z;
//	gyro_x = sensor_gyro_y;
//	gyro_y = sensor_gyro_x;
//	gyro_z = sensor_gyro_z;

  //Get the number of degrees
  //javey: converted to radians to following SI units
  gam->gyro_xVel_p = IMU_GYRX_SIGN * ((gyro_x / GYRO_SENS) * DEG_TO_RAD) + GYRO_X_BIAS;
  gam->gyro_yVel_q = IMU_GYRY_SIGN * ((gyro_y / GYRO_SENS) * DEG_TO_RAD) + GYRO_Y_BIAS;
  gam->gyro_zVel_r = IMU_GYRZ_SIGN * ((gyro_z / GYRO_SENS) * DEG_TO_RAD) + GYRO_Z_BIAS;

  // Magnometer
  mpu9150_read_mag(self, gam);

  return error;
}

//////////////////////
// Helper functions
/////////////////////

int mpu9150_write(struct I2CDriver *i2c, u8 register_addr, u8 data){

  u16 device_addr = MPU9150_DEVICE_ADDR;
  u8 buf[] = {register_addr, data};

  // Check if within register range
  if(register_addr < 0 || register_addr > 0x75){
    return -1;
  }

  if(register_addr <= 0x12){
    device_addr = MPU9150_COMPASS_ADDR;
  }

  return i2c->write(i2c, device_addr, buf, 2);
}

int mpu9150_read(struct I2CDriver *i2c, u8* recv_buffer, u8 register_addr, int size){

  u16 device_addr = MPU9150_DEVICE_ADDR;
  u8 buf[] = {register_addr};

  // Check if within register range
  if(register_addr < 0 || register_addr > 0x75){
  }

  // Set device address to the if 0x00 <= register address <= 0x12
  if(register_addr <= 0x12){
    device_addr = MPU9150_COMPASS_ADDR;
  }


  int error = i2c->write(i2c, device_addr, buf, 1);
  if (error) return error;
  return i2c->read(i2c, device_addr, recv_buffer, size);
}


int mpu9150_read_mag(struct IMUDriver *self, gam_t* gam){
	u8 mag_data[6];
	u8 mag_status;
	i16 raw_magX, raw_magY, raw_magZ;
	i16 sensor_raw_magX, sensor_raw_magY, sensor_raw_magZ;

	int trigger = 0;

	int status = mpu9150_read(self->i2c, mag_data, 0x02, 1);
	if(status != 0) {
		return status;
	}

	if(mag_data[0] & 0x01) {
		// Get mag data
		status = mpu9150_read(self->i2c, mag_data, 0x03, 6);
		if(status != 0) {
			return status;
		}

		status = mpu9150_read(self->i2c, &mag_status, 0x09, 1);
		if(status != 0) {
			return status;
		}

		sensor_raw_magX = (mag_data[1] << 8) | mag_data[0];
		sensor_raw_magY = (mag_data[3] << 8) | mag_data[2];
		sensor_raw_magZ = (mag_data[5] << 8) | mag_data[4];

		//remap
		raw_magX = -sensor_raw_magY;
		raw_magY =  sensor_raw_magX;
		raw_magZ =  sensor_raw_magZ;

		// Set magnetometer data to output
		gam->mag_x = raw_magX * gam->magX_correction;
		gam->mag_y = raw_magY * gam->magY_correction;
		gam->mag_z = raw_magZ * gam->magZ_correction;

		trigger = 1;
	}
	else {
		gam->magDRDYCount++;

		if(gam->magDRDYCount > MAG_DRDY_TIMEOUT) {
			gam->magDRDYCount = 0;

			trigger = 1;
		}
	}

	if(trigger) {
		//Start next reading
		mpu9150_write(self->i2c, 0x0A, 0x00);
		mpu9150_write(self->i2c, 0x0A, 0x01);
	}

	return 0;
}

#include "hw_impl_zybo.h"

#define SYS_CLOCK_RATE 100000000 // ticks per second
#define FREQUENCY 450
#define PERIOD_WIDTH SYS_CLOCK_RATE/FREQUENCY
#define PULSE_WIDTH_LOW SYS_CLOCK_RATE/1000 // 1 ms
#define PULSE_WIDTH_HIGH SYS_CLOCK_RATE/500 // 2 ms
#define PULSE_WIDTH_ADDR_OFFSET 1

struct RCReceiverDriverState {
  int *channels[6];
};

int zybo_rc_receiver_reset(struct RCReceiverDriver *self) {
  if (self->state == NULL) {
    self->state = malloc(sizeof(struct RCReceiverDriverState));
    if (self->state == NULL) {
      return -1;
    }
  }

  // Save the addresses of the input PWM recorders
  struct RCReceiverDriverState *state = self->state;
  state->channels[0] = (int *) XPAR_PWM_RECORDER_0_S_AXI_BASEADDR;
  state->channels[1] = (int *) XPAR_PWM_RECORDER_1_S_AXI_BASEADDR;
  state->channels[2] = (int *) XPAR_PWM_RECORDER_2_S_AXI_BASEADDR;
  state->channels[3] = (int *) XPAR_PWM_RECORDER_3_S_AXI_BASEADDR;
  state->channels[4] = (int *) XPAR_PWM_RECORDER_4_S_AXI_BASEADDR;
  state->channels[5] = (int *) XPAR_PWM_RECORDER_5_S_AXI_BASEADDR;
  return 0;
}

int zybo_rc_receiver_read(struct RCReceiverDriver *self,
                        unsigned int channel,
                        float *magnitude) {
  struct RCReceiverDriverState *state = self->state;
  unsigned long pulse_width_ticks = (long) *((int *) state->channels[channel]);
  *magnitude = (float) (pulse_width_ticks - PULSE_WIDTH_LOW) / (float) (PULSE_WIDTH_HIGH - PULSE_WIDTH_LOW);
  return 0;
}

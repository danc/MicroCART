#include "hw_impl_zybo.h"

#define SYS_CLOCK_RATE 100000000 // ticks per second
#define FREQUENCY 450
#define PERIOD_WIDTH SYS_CLOCK_RATE/FREQUENCY
#define PULSE_WIDTH_LOW SYS_CLOCK_RATE/1000 // 1 ms
#define PULSE_WIDTH_HIGH SYS_CLOCK_RATE/500 // 2 ms
#define PULSE_WIDTH_ADDR_OFFSET 1

struct MotorDriverState {
  int *outputs[4];
};

int zybo_motor_reset(struct MotorDriver *self) {
  if (self->state == NULL) {
    self->state = malloc(sizeof(struct MotorDriverState));
    if (self->state == NULL) {
      return -1;
    }
  }
  struct MotorDriverState *state = self->state;

  state->outputs[0] = (int *) MOTOR_BACKRIGHT_BASEADDR;
  state->outputs[1] = (int *) MOTOR_FRONTRIGHT_BASEADDR;
  state->outputs[2] = (int *) MOTOR_BACKLEFT_BASEADDR;
  state->outputs[3] = (int *) MOTOR_FRONTLEFT_BASEADDR;

  // Set period width of PWM pulse
  *(state->outputs[0]) = PERIOD_WIDTH;
  *(state->outputs[1]) = PERIOD_WIDTH;
  *(state->outputs[2]) = PERIOD_WIDTH;
  *(state->outputs[3]) = PERIOD_WIDTH;

  // Set a low pulse (1 ms) so that outputs are off
  *(state->outputs[0] + PULSE_WIDTH_ADDR_OFFSET) = PULSE_WIDTH_LOW;
  *(state->outputs[1] + PULSE_WIDTH_ADDR_OFFSET) = PULSE_WIDTH_LOW;
  *(state->outputs[2] + PULSE_WIDTH_ADDR_OFFSET) = PULSE_WIDTH_LOW;
  *(state->outputs[3] + PULSE_WIDTH_ADDR_OFFSET) = PULSE_WIDTH_LOW;

  usleep(1000000);
  return 0;
}

int zybo_motor_write(struct MotorDriver *self,
                          unsigned int channel,
                          float magnitude) {
  if (magnitude > 1)
    magnitude = 1;
  if (magnitude < 0)
    magnitude = 0;
  struct MotorDriverState *state = self->state;
  unsigned long pulse_width_ticks = (unsigned long) (magnitude * (float) (PULSE_WIDTH_HIGH - PULSE_WIDTH_LOW)) + PULSE_WIDTH_LOW;
  *(state->outputs[channel] + PULSE_WIDTH_ADDR_OFFSET) = pulse_width_ticks;
  return 0;
}

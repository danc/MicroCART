#include "hw_impl_zybo.h"

#define BAUD_RATE 921600


int XUartPs_SetBaudRate_ours(XUartPs *InstancePtr, u32 BaudRate);
int SetupInterruptSystem(struct ZyboUARTState *state, u16 UartIntrId, Xil_ExceptionHandler handler);
void uart_interrupt_handler(struct ZyboUARTState *state);

int zybo_uart_reset(struct UARTDriver *self) {
  // Ensure all required memory is allocated
  struct ZyboUARTState *state = self->state;
  if (state == NULL) return -1;
  if (state->inst == NULL) return -1;
  if (state->queue == NULL) return -1;

  XUartPs *inst = state->inst;

  // Configure XUartPs instance
  XUartPs_Config* config = XUartPs_LookupConfig(XPAR_PS7_UART_0_DEVICE_ID);
  if (XUartPs_CfgInitialize(inst, config, config->BaseAddress) != XST_SUCCESS) {
    return -1;
  }

  // Set UART Baudrate
  if(XUartPs_SetBaudRate_ours(inst, BAUD_RATE) != XST_SUCCESS) {
    return -1;
  }

  // Clear Tx/Rx FIFOs
  int* uart_ctrl_reg = (int*) inst->Config.BaseAddress;
  *uart_ctrl_reg |= 0x00000003; // clear TX & RX

  // Setup Interrupts
  if (SetupInterruptSystem(state, XPAR_PS7_UART_0_INTR, (Xil_ExceptionHandler) uart_interrupt_handler) != XST_SUCCESS) {
    return -1;
  }

  // Interrupts selected: Rx FIFO threashold reached, RX overflow, timeout
  u32 IntrMask = XUARTPS_IXR_RXFULL | XUARTPS_IXR_RXOVR | XUARTPS_IXR_TOUT;
  XUartPs_SetInterruptMask(inst, IntrMask);

  /*
   * Set the receiver timeout. If it is not set, and the last few bytes
   * of data do not trigger the over-water or full interrupt, the bytes
   * will not be received. By default it is disabled.
   * Timeout duration = RecvTimeout x 4 x Bit Period. 0 disables the
   * timeout function.
   *
   * The setting of 8 will timeout after 8 x 4 = 32 character times.
   * Increase the time out value if baud rate is high, decrease it if
   * baud rate is low.
   */
  XUartPs_SetRecvTimeout(inst, 8);

  // Second argument is the number of bytes to trigger an interrupt at
  XUartPs_SetFifoThreshold(inst, 48);

  return 0;
}

int zybo_uart_write(struct UARTDriver *self, unsigned char c) {
  struct ZyboUARTState *state = self->state;
  XUartPs *inst = state->inst;
  XUartPs_SendByte(inst->Config.BaseAddress, c);
  return 0;
}

int zybo_uart_read(struct UARTDriver *self, unsigned char *c) {
  struct ZyboUARTState *state = self->state;
  struct Queue *queue = state->queue;
  if (queue_remove(queue, c)) return -1;
  else return 0;
}

//This is copied from xuart driver
/***************************************************/

#define XUARTPS_MAX_BAUD_ERROR_RATE              3      /* max % error allowed */
int XUartPs_SetBaudRate_ours(XUartPs *InstancePtr, u32 BaudRate)
{
  u8 IterBAUDDIV;               /* Iterator for available baud divisor values */
  u32 BRGR_Value;               /* Calculated value for baud rate generator */
  u32 CalcBaudRate;     /* Calculated baud rate */
  u32 BaudError;                /* Diff between calculated and requested baud rate */
  u32 Best_BRGR = 0;    /* Best value for baud rate generator */
  u8 Best_BAUDDIV = 0;  /* Best value for baud divisor */
  u32 Best_Error = 0xFFFFFFFF;
  u32 PercentError;
  u32 ModeReg;
  u32 InputClk;

  /*
   * Asserts validate the input arguments
   */
  Xil_AssertNonvoid(InstancePtr != NULL);
  Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);
  //Xil_AssertNonvoid(BaudRate <= XUARTPS_MAX_RATE);
  Xil_AssertNonvoid(BaudRate >= XUARTPS_MIN_RATE);

  /*
   * Make sure the baud rate is not impossilby large.
   * Fastest possible baud rate is Input Clock / 2.
   */
  if ((BaudRate * 2) > InstancePtr->Config.InputClockHz) {
    return XST_UART_BAUD_ERROR;
  }
  /*
   * Check whether the input clock is divided by 8
   */
  ModeReg = XUartPs_ReadReg( InstancePtr->Config.BaseAddress,
                             XUARTPS_MR_OFFSET);

  InputClk = InstancePtr->Config.InputClockHz;
  if(ModeReg & XUARTPS_MR_CLKSEL) {
    InputClk = InstancePtr->Config.InputClockHz / 8;
  }

  /*
   * Determine the Baud divider. It can be 4to 254.
   * Loop through all possible combinations
   */
  for (IterBAUDDIV = 4; IterBAUDDIV < 255; IterBAUDDIV++) {

    /*
     * Calculate the value for BRGR register
     */
    BRGR_Value = InputClk / (BaudRate * (IterBAUDDIV + 1));

    /*
     * Calculate the baud rate from the BRGR value
     */
    CalcBaudRate = InputClk/ (BRGR_Value * (IterBAUDDIV + 1));

    /*
     * Avoid unsigned integer underflow
     */
    if (BaudRate > CalcBaudRate) {
      BaudError = BaudRate - CalcBaudRate;
    }
    else {
      BaudError = CalcBaudRate - BaudRate;
    }

    /*
     * Find the calculated baud rate closest to requested baud rate.
     */
    if (Best_Error > BaudError) {

      Best_BRGR = BRGR_Value;
      Best_BAUDDIV = IterBAUDDIV;
      Best_Error = BaudError;
    }
  }

  /*
   * Make sure the best error is not too large.
   */
  PercentError = (Best_Error * 100) / BaudRate;
  if (XUARTPS_MAX_BAUD_ERROR_RATE < PercentError) {
    return XST_UART_BAUD_ERROR;
  }

  /*
   * Disable TX and RX to avoid glitches when setting the baud rate.
   */
  XUartPs_DisableUart(InstancePtr);

  XUartPs_WriteReg(InstancePtr->Config.BaseAddress,
                   XUARTPS_BAUDGEN_OFFSET, Best_BRGR);
  XUartPs_WriteReg(InstancePtr->Config.BaseAddress,
                   XUARTPS_BAUDDIV_OFFSET, Best_BAUDDIV);

  /*
   * Enable device
   */
  XUartPs_EnableUart(InstancePtr);

  InstancePtr->BaudRate = BaudRate;

  return XST_SUCCESS;

}

void uart_interrupt_handler(struct ZyboUARTState *state) {
  u32 IsrStatus;
  XUartPs *InstancePtr = state->inst;
  struct Queue *queue = state->queue;

  /*
   * Read the interrupt ID register to determine which
   * interrupt is active
   */
  IsrStatus = XUartPs_ReadReg(InstancePtr->Config.BaseAddress,
                              XUARTPS_IMR_OFFSET);

  IsrStatus &= XUartPs_ReadReg(InstancePtr->Config.BaseAddress,
                               XUARTPS_ISR_OFFSET);

  /*
   * Read the Channel Status Register to determine if there is any data in
   * the RX FIFO
   */

  u32 CsrRegister = XUartPs_ReadReg(InstancePtr->Config.BaseAddress,
                                    XUARTPS_SR_OFFSET);

  while (0 == (CsrRegister & XUARTPS_SR_RXEMPTY)) {
    u8 byte = XUartPs_ReadReg(InstancePtr->Config.BaseAddress, XUARTPS_FIFO_OFFSET);
    queue_add(queue, byte);
    CsrRegister = XUartPs_ReadReg(InstancePtr->Config.BaseAddress, XUARTPS_SR_OFFSET);
  }

  // Clear the interrupt status.
  XUartPs_WriteReg(InstancePtr->Config.BaseAddress, XUARTPS_ISR_OFFSET,
                   IsrStatus);
}

/*****************************************************************************/
/**
*
* This function sets up the interrupt system so interrupts can occur for the
* Uart. This function is application-specific.
*
* @param	IntcInstancePtr is a pointer to the instance of the INTC.
* @param	UartInstancePtr contains a pointer to the instance of the UART
*		driver which is going to be connected to the interrupt
*		controller.
* @param	UartIntrId is the interrupt Id and is typically
*		XPAR_<UARTPS_instance>_INTR value from xparameters.h.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note		None.
*
****************************************************************************/
int SetupInterruptSystem(struct ZyboUARTState *state, u16 UartIntrId, Xil_ExceptionHandler handler)
{
	int Status;
	XScuGic_Config *IntcConfig; /* Config for interrupt controller */

	/* Initialize the interrupt controller driver */
	IntcConfig = XScuGic_LookupConfig(XPAR_SCUGIC_SINGLE_DEVICE_ID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

    memset(&state->xscugic, 0, sizeof(XScuGic));
	Status = XScuGic_CfgInitialize(&state->xscugic, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the interrupt controller interrupt handler to the
	 * hardware interrupt handling logic in the processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				(Xil_ExceptionHandler) XScuGic_InterruptHandler,
				&state->xscugic);

	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler
	 * performs the specific interrupt processing for the device
	 */
	Status = XScuGic_Connect(&state->xscugic, UartIntrId,
				  handler,
				  (void *) state);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* Enable the interrupt for the device */
	XScuGic_Enable(&state->xscugic, UartIntrId);

	/* Enable interrupts */
	 Xil_ExceptionEnable();

	return XST_SUCCESS;
}

#include "hw_impl_zybo.h"

#define PL_CLK_CNTS_PER_USEC 100

int zybo_axi_timer_reset(struct TimerDriver *self) {
  if (self->state == NULL) {
    self->state = malloc(sizeof(XTmrCtr));
  }
  return XTmrCtr_Initialize(self->state, XPAR_AXI_TIMER_0_DEVICE_ID);
}

int zybo_axi_timer_restart(struct TimerDriver *self) {
  XTmrCtr_Reset(self->state, 0);
  XTmrCtr_Start(self->state, 0);
  return 0;
}

int zybo_axi_timer_read(struct TimerDriver *self, u64 *us) {
  // Returns the number of useconds
  *us = XTmrCtr_GetValue(self->state, 0) / PL_CLK_CNTS_PER_USEC;
  return 0;
}

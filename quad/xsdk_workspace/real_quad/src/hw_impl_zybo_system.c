#include "hw_impl_zybo.h"

int zybo_system_reset(struct SystemDriver *sys) {
  return 0;
}

int zybo_system_sleep(struct SystemDriver *sys, unsigned long us) {
  usleep(us);
  return 0;
}


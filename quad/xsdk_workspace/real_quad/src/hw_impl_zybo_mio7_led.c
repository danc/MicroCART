#include "hw_impl_zybo.h"

int zybo_mio7_led_reset(struct LEDDriver *self) {
  if (self->state == NULL) {
    self->state = malloc(sizeof(XGpioPs));
  }

  XGpioPs_Config *ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
  XGpioPs_CfgInitialize(self->state, ConfigPtr, ConfigPtr->BaseAddr);
  XGpioPs_SetDirectionPin(self->state, 7, 1);
  return 0;
}

int zybo_mio7_led_turn_on(struct LEDDriver *self) {
  XGpioPs_WritePin(self->state, 7, 0x01);
  return 0;
}

int zybo_mio7_led_turn_off(struct LEDDriver *self) {
  XGpioPs_WritePin(self->state, 7, 0x00);
  return 0;
}

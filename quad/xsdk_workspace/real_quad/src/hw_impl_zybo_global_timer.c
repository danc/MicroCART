#include "hw_impl_zybo.h"

int zybo_global_timer_reset(struct TimerDriver *self) {
  // Nothing to initialize
  return 0;
}

int zybo_global_timer_restart(struct TimerDriver *self) {
  XTime_SetTime(0);
  return 0;
}

int zybo_global_timer_read(struct TimerDriver *self, u64 *us) {
  XTime time;
  XTime_GetTime(&time);
  *us = ((u64)time * 1000000) / COUNTS_PER_SECOND; // (ticks)(1000000us/s)(s/ticks)
  return 0;
}

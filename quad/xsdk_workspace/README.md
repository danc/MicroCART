# XSDK Workspace

This directory is reserved for Xilinx XSDK projects and their respective hardware platforms.  

## What is XSDK

XSDK is a development tool made by Xilinx to create the files necessary to boot
 the hardware and software on the FPGA. It includes a test editor based on
 eclipse so the tools should feel vaguely familiar.
 Use our [how to use XSDK document](../doc/how_to_use_XSDK.md) or 
 [XSDK documentation webpage](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2015_1/SDK_Doc/index.html)


## Setup
XSDK, being based on Eclipse, is rather fragile, so do yourself a favor
and read this section so things get setup correctly.

1. When you first open eclipse, select this directory, `xsdk_workspace`, as
   your workspace (see what we did there?).

2. When you get to your workbench, your project pane should be empty. To
   add these projects, right-click on the project pane, and click on something
   like "import projects".

  1. Select "Import Existing Projects" (wherever that is)
  2. And then select the xsdk_workspace as the folder where you want to import
     projects. Add them all.

3. If things are going swimmingly, then you should be able to build everything
   and be off on your merry embedded endeavors.

#ifndef QUEUE_H
#define QUEUE_H

#include <stdlib.h>
#include <stdio.h>

struct Queue {
  volatile unsigned char *buff;
  unsigned int max_size_plus1;
  unsigned int start;
  unsigned int end;
};


int queue_init(struct Queue *q, volatile unsigned char *buff, unsigned int max_size_plus1);
struct Queue * queue_malloc(unsigned int max_size);
void queue_free(struct Queue *q);
int queue_add(struct Queue *q, unsigned char c);
int queue_remove(struct Queue *q, unsigned char *c);
int queue_size(struct Queue *q);
int queue_full(struct Queue *q);
int queue_empty(struct Queue *q);
void queue_print(struct Queue *q);

#endif

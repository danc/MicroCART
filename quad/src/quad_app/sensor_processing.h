/*
 * sensor_processing.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef SENSOR_PROCESSING_H_
#define SENSOR_PROCESSING_H_
#include "type_def.h"
/*
 * Aero channel declaration
 */

#define THROTTLE 0
#define ROLL     1
#define PITCH    2
#define YAW      3
#define GEAR 	 4
#define FLAP 	 5

/**
 * @brief 
 *      Processes the data from the sensors.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param raw_sensor_struct
 *      structure of the raw data from the sensors
 *
 * @param sensor_struct
 *      structure of the processed data from the sensors
 *
 * @return 
 *      error message
 *
 */

int sensor_processing(log_t* log_struct, user_input_t *user_input_struct, raw_sensor_t * raw_sensor_struct, sensor_t* sensor_struct);
int sensor_processing_init(sensor_t* sensor_struct);
void deep_copy_Qpos(quadPosition_t * dest, quadPosition_t * src);
void set_pitch_angle_filtered(sensor_t * sensor_struct, float accel_roll);
void set_roll_angle_filtered(sensor_t * sensor_struct, float accel_roll);

#endif /* SENSOR_PROCESSING_H_ */

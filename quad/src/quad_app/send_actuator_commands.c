/*
 * send_actuator_commands.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */
 
#include "send_actuator_commands.h"
#include "util.h"
 
int send_actuator_commands(struct MotorDriver *motors, log_t* log_struct, actuator_command_t* actuator_command_struct) {
  int i;
  // write the PWMs to the motors

  for (i = 0; i < 4; i++) {
    motors->write(motors, i, actuator_command_struct->motor_magnitudes[i]);
  }

  return 0;
}

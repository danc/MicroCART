/*
 * mio7_led.c
 *
 *  Created on: Feb 20, 2016
 *      Author: Amy Seibert
 */
 
#include "mio7_led.h"

struct LEDDriver *mio7_led;
struct SystemDriver *sys;

void mio7_init_globals(struct LEDDriver *given_mio7_led, struct SystemDriver *given_system) {
  mio7_led = given_mio7_led;
  sys = given_system;
}

void flash_MIO_7_led(int how_many_times, int ms_between_flashes)
{
  int i;
  for (i = 0; i < how_many_times; i += 1) {
    mio7_led->turn_on(mio7_led);
    sys->sleep(sys, ms_between_flashes * 500);
    mio7_led->turn_off(mio7_led);
    sys->sleep(sys, ms_between_flashes * 500);
  }
}


void MIO7_led_on() {
  mio7_led->turn_on(mio7_led);
}
void MIO7_led_off() {
  mio7_led->turn_off(mio7_led);
}

#ifndef HW_IFACE_H
#define HW_IFACE_H

/**
 * Hardware Interfaces
 *
 * These interfaces are used to accomplish separation between the application
 * layer and the hardware layer in the program that runs on the quadcopter.
 *
 * NOTE:
 * If you wound up here after following some IDE function declaration trail,
 * you've hit the end of the application layer. Go to the location of the
 * hardware layer appropriate for your circumstance:
 * ../../xsdk_worksapce/real_quad -> running quad_app on the Zybo
 * ../virt_quad -> running quad_app in a Unix environment
 *
 * Function Pointer Return Values:
 * - All driver functions return the error code
 * - 0 for success
 * - nonzero otherwise
 */

// Forward declared types
struct gam;
struct lidar;
struct px4flow;
struct gps;

struct RCReceiverDriver {
  void *state;
  int (*reset)(struct RCReceiverDriver *self);
  int (*read)(struct RCReceiverDriver *self, unsigned int channel, float *magnitude);
};

struct MotorDriver {
  void *state;
  int (*reset)(struct MotorDriver *self);
  int (*write)(struct MotorDriver *self, unsigned int channel, float magnitude);
};

struct UARTDriver {
  void *state;
  int (*reset)(struct UARTDriver *self);
  int (*write)(struct UARTDriver *self, unsigned char c);
  int (*read)(struct UARTDriver *self, unsigned char *c);
};

struct TimerDriver {
  void *state;
  int (*reset)(struct TimerDriver *self);
  int (*restart)(struct TimerDriver *self);
  int (*read)(struct TimerDriver *self, unsigned long long *us);
};

struct LEDDriver {
  void *state;
  int (*reset)(struct LEDDriver *self);
  int (*turn_on)(struct LEDDriver *self);
  int (*turn_off)(struct LEDDriver *self);
};

struct SystemDriver {
  void *state;
  int (*reset)(struct SystemDriver *self);
  int (*sleep)(struct SystemDriver *self, unsigned long us);
};

struct I2CDriver {
  void *state;
  int (*reset)(struct I2CDriver *self);
  int (*write)(struct I2CDriver *self,
               unsigned short device_addr,
               unsigned char *data,
               unsigned int length);
  int (*read)(struct I2CDriver *self,
              unsigned short device_addr,
              unsigned char *buff,
              unsigned int length);
};

struct IMUDriver {
  struct I2CDriver *i2c;
  int (*reset)(struct IMUDriver *self, struct gam *gam);
  int (*read)(struct IMUDriver *self, struct gam *gam);
};

struct LidarDriver {
  struct I2CDriver *i2c;
  int (*reset)(struct LidarDriver *self, struct lidar *lidar);
  int (*read)(struct LidarDriver *self, struct lidar *lidar);
};

struct OpticalFlowDriver {
  struct I2CDriver *i2c;
  int (*reset)(struct OpticalFlowDriver *self, struct px4flow *of);
  int (*read)(struct OpticalFlowDriver *self, struct px4flow *of);
};

struct GPSDriver {
  struct UARTDriver *uart;
  int (*reset)(struct GPSDriver *self, struct gps *gps);
  int (*read)(struct GPSDriver *self, struct gps *gps);
};

struct CommDriver {
  struct UARTDriver *uart;
};

#endif

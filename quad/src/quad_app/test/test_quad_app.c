#include "communication.h"
#include <string.h>
#include "queue.h"
#include "unity.h"

struct Queue *queue;
struct UARTDriver *uart;

int mock_uart_read(struct UARTDriver *self, unsigned char *c) {
  return queue_remove(queue, c);
}

struct UARTDriver * mock_uart_malloc() {
  struct UARTDriver *uart = malloc(sizeof(struct UARTDriver));
  uart->read = mock_uart_read;
  return uart;
}

void queue_add_corruption(struct Queue *q, int num) {
  int val = 0xAA;
  int i;
  for (i = 0; i < num; i += 1) {
    val = (val << 1) ^ i;
    if (val == 0xBE) queue_add(q, 0);
    else queue_add(q, val);
  }
}

void queue_add_short(struct Queue *q, short val) {
  queue_add(q, val & 0x00FF);
  queue_add(q, val >> 8);
}

// Return checksum
unsigned char queue_add_packet(struct Queue *q,
		unsigned short type,
		unsigned short id,
		unsigned short length,
		unsigned char *data) {
  queue_add(q, 0xBE);
  queue_add(q, type & 0x00FF);
  queue_add(q, type >> 8);
  queue_add(q, id & 0x00FF);
  queue_add(q, id >> 8);
  queue_add(q, length & 0x00FF);
  queue_add(q, length >> 8);

  unsigned char checksum = 0;
  checksum ^= 0xBE;
  checksum ^= type & 0x00FF;
  checksum ^= type >> 8;
  checksum ^= id & 0x00FF;
  checksum ^= id >> 8;
  checksum ^= length & 0x00FF;
  checksum ^= length >> 8;
  int i;
  for (i = 0; i < length; i += 1) {
    queue_add(q, data[i]);
    checksum ^= data[i];
  }
  queue_add(q, checksum);
  return checksum;
}

// Spying into communication.c's global variables
extern u8 packet[MAX_PACKET_SIZE];
extern int bytes_recv;
extern unsigned char packet_checksum;


/*--------- TESTS ---------*/

// Test fails when no BE and run out
void test_try_receive_packet_fails_when_no_BE_and_run_out() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  queue = queue_malloc(5);
  queue_add(queue, 0);
  queue_add(queue, 0);
  queue_add(queue, 1);

  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(0, bytes_recv);


  // Try again to verify that we actually ran out
  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(0, bytes_recv);
}

// Test fails when no BE and too much (check resume)
void test_try_receive_packet_fails_when_no_BE_and_too_much() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  int size = 255;
  queue = queue_malloc(size);
  queue_add_corruption(queue, size);

  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(0, bytes_recv);

  // Ensure that we quit trying
  TEST_ASSERT_TRUE(size - queue_size(queue) <= MAX_PACKET_SIZE);
}

// Test fails when BE and run out (check resume)
void test_try_receive_packet_fails_when_BE_and_run_out() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  queue = queue_malloc(100);
  queue_add_corruption(queue, 10);
  queue_add(queue, 0xBE);

  queue_add(queue, 1);
  queue_add(queue, 2);
  queue_add(queue, 3);

  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(4, bytes_recv);
  u8 expected[] = {0xBE, 1, 2, 3};
  TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, packet, 4);

  // Try again to verify that we actually ran out
  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(4, bytes_recv);
}

// Test fails when BE and nonsensical length (check resume)
void test_try_receive_packet_fails_when_BE_and_big_length() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  queue = queue_malloc(500);
  int i;
  queue_add_corruption(queue, 10);
  queue_add(queue, 0xBE);
  queue_add_short(queue, 0);
  queue_add_short(queue, 0);
  queue_add_short(queue, 500);
  for (i = 0; i < 10; i += 1) queue_add_corruption(queue, i);

  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(0, bytes_recv);

  // Try again to verify that we actually ran out
  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(0, bytes_recv);
}

// Test fails when BE and length and run out (check resume)
void test_try_receive_packet_fails_when_BE_length_and_run_out() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  queue = queue_malloc(500);
  int i;
  queue_add_corruption(queue, 20);
  queue_add(queue, 0xBE);
  queue_add_short(queue, 0);
  queue_add_short(queue, 0);
  queue_add_short(queue, 4);
  unsigned char data[4] = {1, 2, 3, 4};
  for (i = 0; i < 2; i += 1) queue_add(queue, data[i]);

  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(9, bytes_recv);
  u8 expected[] = {0xBE, 0, 0, 0, 0, 4, 0, 1, 2};
  TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, packet, 9);

  // Try again to verify that we actually ran out
  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(9, bytes_recv);
}

// Test fails when BE and length and data and run out (check resume)
void test_try_receive_packet_fails_when_BE_length_data_and_run_out() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  queue = queue_malloc(500);
  int i;
  queue_add_corruption(queue, 10);
  queue_add(queue, 0xBE);
  queue_add_short(queue, 0);
  queue_add_short(queue, 0);
  queue_add_short(queue, 4);
  unsigned char data[4] = {1, 2, 3, 4};
  for (i = 0; i < 4; i += 1) queue_add(queue, data[i]);

  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(11, bytes_recv);
  u8 expected[] = {0xBE, 0, 0, 0, 0, 4, 0, 1, 2, 3, 4};
  TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, packet, 11);

  // Try again to verify that we actually ran out
  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(11, bytes_recv);
}

// Test fails when BE, length, data, and checksum fails
void test_try_receive_packet_fails_when_BE_length_data_and_bad_checksum() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  queue = queue_malloc(500);
  int i;
  queue_add_corruption(queue, 10);
  queue_add(queue, 0xBE);
  queue_add_short(queue, 0);
  queue_add_short(queue, 0);
  queue_add_short(queue, 4);
  unsigned char data[4] = {1, 2, 3, 4};
  for (i = 0; i < 4; i += 1) queue_add(queue, data[i]);
  queue_add(queue, 0xFF); // bad checksum
  queue_add(queue, 0xBE); // next start

  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(1, bytes_recv);
  TEST_ASSERT_EQUAL(0xBE, packet[0]);

  // Try again to verify that we actually ran out
  TEST_ASSERT_EQUAL(-1, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(1, bytes_recv);
}

// Test succeeds when BE, length, data, checksum
void test_try_receive_packet_succeeds() {
  bytes_recv = 0;
  uart = mock_uart_malloc();
  queue = queue_malloc(500);
  int i;
  queue_add_corruption(queue, 10);
  unsigned char data[4] = {1, 2, 3, 4};
  unsigned char checksum = queue_add_packet(queue, 0, 0, 4, data);

  TEST_ASSERT_EQUAL(0, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(12, bytes_recv);
  u8 expected[] = {0xBE, 0, 0, 0, 0, 4, 0, 1, 2, 3, 4, checksum};
  TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, packet, 12);

  // Try again to verify that we don't lose the ready packet
  TEST_ASSERT_EQUAL(0, try_receive_packet(uart));
  TEST_ASSERT_EQUAL(12, bytes_recv);
}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_try_receive_packet_fails_when_no_BE_and_run_out);
  RUN_TEST(test_try_receive_packet_fails_when_no_BE_and_too_much);
  RUN_TEST(test_try_receive_packet_fails_when_BE_and_run_out);
  RUN_TEST(test_try_receive_packet_fails_when_BE_and_big_length);
  RUN_TEST(test_try_receive_packet_fails_when_BE_length_and_run_out);
  RUN_TEST(test_try_receive_packet_fails_when_BE_length_data_and_run_out);
  RUN_TEST(test_try_receive_packet_fails_when_BE_length_data_and_bad_checksum);
  RUN_TEST(test_try_receive_packet_succeeds);
  return UNITY_END();
}

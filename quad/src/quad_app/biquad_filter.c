#include "biquad_filter.h"

struct biquadState filter_make_state(float a0, float a1, float a2,
                              float b1, float b2) {
    struct biquadState state = {
        .delayed = {0,0},
        .a0 = a0,
        .a1 = a1,
        .a2 = a2,
        .b1 = b1,
        .b2 = b2
    };
    return state;
}

// http://www.earlevel.com/main/2003/02/28/biquads/
// Direct form II
float biquad_execute(struct biquadState* state, float new_input) {
    float left_sum = new_input -
                     (state->delayed[0] * state->b1) -
                     (state->delayed[1] * state->b2);
    float output = (left_sum * state->a0) +
                   (state->delayed[0] * state->a1) +
                   (state->delayed[1] * state->a2);

    state->delayed[1] = state->delayed[0];
    state->delayed[0] = left_sum;
    return output;
}
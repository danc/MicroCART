/*
 * send_actuator_commands.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef DO_OUTPUT_OVERRIDE_H_
#define DO_OUTPUT_OVERRIDE_H_
 
#include <stdio.h>

#include "log_data.h"
#include "type_def.h"

/**
 * @brief 
 *      Sends commands to the actuators.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param actuator_command_struct
 *      structure of the commmands to go to the actuators
 *
 * @param actuator_command_struct
 *      structure of the commmands to go to the actuators
 *
 * @return 
 *      error message
 *
 */
int do_output_override(log_t* log_struct, override_t* override_struct, actuator_command_t* actuator_command_struct);

#endif /* DO_OUTPUT_OVERRIDE_H_ */

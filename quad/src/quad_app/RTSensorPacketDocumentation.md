#Sensor Data - Real Time 

### Sends data in "raw sensor struct"

**NOTE**
It is important to keep the payload as small as possible. So if the code seems strange, keep in mind NOT to change the payload size to be fixed, even if it would be cleaner or easier to understand

* In the main quad loop, a function is called that is "get sensors". It places stuff in the hardware struct into the raw sensor struct. 
* The hardware struct includes all the drivers for the various sensors on the quadcopter. The get sensors function essentially calls "read" on the drivers and places that info into the raw sensor struct
* Each driver has these things:
	* reset function pointer
	* read function pointer
	* state void pointer
	
* The raw sensor struct holds these values
	* optical flow sensor data (px4flow)
	* lidar data
	* imu data (gam struct)
	* UNUSED gps struct
	* All these structs have an error variable
		* called SensorError_t struct
		* has an error count variable

### Structs for sensors
* lidar struct
	* distance the quad is from the ground in meters
		* float (32 bits)
	

* optical flow struct
	* All the below are doubles (64 bits)
	* xy flow
	* xy filter
	* xy velocity

* imu data
	* All the below values are floats (32 bits)
	* gyroscope data
		* xyz velocity (angular)
	* accelerometer data
		* xyz acceleration
	* magnetometer data
		* xyz direction

* actuator data
	*

* SensorError struct (all the above have this)
	* Errors
	* Consecutive Errors
	* all the above is 32 bits wide
	


### Packet structure

#### Groundstation to Quad: Sensor RT Data Configuration Packet

| index  | Description  | Size  |
|---|---|---|
|  0 |  start char | 1  |
| 1 + 2  |  message type - 20 |  2 |
|  3 + 4 | message id - 0  |  2 |
|  5 + 6 |  # bytes - 14 |  2 |
|  7 | first 8 variables (see table)  |  1 |
|  8 | second 8 varibales (see table) |  1 |
|  9 |  last 6 variables (see table) |  1 |
|  10 | reserved for future use  | 1  |
| end|  checksum |  1 |

Bit map:

Payload of configuration packet

MSB (Bit 31) --------  LSB (bit 0)
|Byte 4| Byte 3| Byte 2| Byte 1|

** Byte 4 **

| bit | Description  | 
|---|---|
| 31 | lidar - distance to ground |
| 30 | optical flow - x flow|
| 29 | optical flow - y flow|
| 28 | optical flow - x filter|
| 27 | optical flow - y filter|
| 26 | optical flow - x velocity |
| 25 | optical flow - y velocity |
| 24 | gyroscope - x velocity|



** byte 3 **

| bit | Description  | 
|---|---|
| 23 | gyroscope - y velocity|
| 22 | gyroscope - z velocity|
| 21 | accelerometer - x acceleration|
| 20 | accelerometer - y acceleration|
| 19 | accelerometer - z acceleration|
| 18 | magnetometer - x |
| 17 | magnetometer - y |
| 16 | magnetometer - z |


** byte 2**

| bit       |     Description     | 
|---|---|
| 15 | lidar err total|
| 14 | consecutive lidar err|
| 13 | opt flow error total|
| 12 | consecutive opt flow error|
| 11 | imu total errors|
| 10 | consecutive imu errors|
| 9  | EMPTY |
| 8  | EMPTY |


** byte 1**

| bit       |     Description     | 
|---|---|
|  7  | EMPTY |
|  6  | EMPTY | 
|  5  | EMPTY |
|  4  | EMPTY |
|  3  | EMPTY |
|  2  | EMPTY |
|  1  | EMPTY |
|  0  | EMPTY |


#### Quad to Groundstation Packet - Contains payload data

| index  | 0  | 1 + 2  | 3+4  | 5&6  |  ? | end  |
|---|---|---|---|---|---|---|
|  description | start char  | msg type - SEND_RT_ID  | msg id - 0  | # bytes| payload  | checksum  |
| bytes  | 1  | 2  | 2  | 2  | variable  | 1  |


* The packet has a msg type of send_rt_id, which has a value of 20
* The structure of the payload depends on what variables are selected
	* See "Checklist" below
	* There are 22 available things to send, but depending on how much data the user wants sent, the payload will vary
	* They will be sent in the order listed by the configuration packet





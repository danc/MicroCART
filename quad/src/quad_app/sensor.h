/*
 * sensor.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef SENSOR_H_
#define SENSOR_H_
 
#include <stdio.h>

#include "log_data.h"
#include "user_input.h"
#include "hw_iface.h"

/**
 * @brief
 *      Initializes the sensors.
 *
 * @return
 *      error message
 *
 */
int sensor_init(hardware_t *hardware_struct, raw_sensor_t * raw_sensor_struct, sensor_t * sensor_struct);

/**
 * @brief
 *      Recieves data from the sensors.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param user_input_struct
 *      structure of the input from the user
 *
 * @param raw_sensor_struct
 *      structure of the raw values from the sensors
 *
 * @return 
 *      error message
 *
 */
int get_sensors(hardware_t *hardware_struct, log_t* log_struct, user_input_t* user_input_struct, raw_sensor_t* raw_sensor_struct);

#endif /* SENSOR_TEMPLATE_H_ */

/*
 * log_data.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef LOG_DATA_H_
#define LOG_DATA_H_
#include "type_def.h"

#define LOG_STARTING_SIZE 60000 //262144 // 2^18      32768  2^15
// Maximum number of block outputs you can record, and maximum number of block parameters to record
#define MAX_LOG_NUM 50

void initialize_logging(log_t* log_struct, parameter_t* ps, SensorRTFlags_t * flags);

void initializeFlags(SensorRTFlags_t * flags); 
/**
 * Adds the given block output to the data to be logged
 */
void addOutputToLog(log_t* log_struct, int controller_id, int output_id, char* units);

/**
 * Adds the given block parameter to the data to be logged
 */
void addParamToLog(log_t* log_struct, int controller_id, int param_id, char* units);


/**
 * @brief
 *      Logs the data obtained throughout the controller loop.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @return
 *      error message
 *
 */
 int log_data(log_t* log_struct, parameter_t* ps, SensorRTFlags_t * flags);

 /**
  * Fills up an xbox hueg amount of memory with log data
  */
 void updateLog(log_t log_struct);

/**
 * Prints the latest log entry. Used for Real-Time Data Transfer
 */
void RTprintLogging(struct CommDriver *comm, log_t* log_struct, parameter_t* ps, raw_sensor_t* raw_sensors, SensorRTFlags_t * flags);
/**
 *  Prints the beginning header for a flight data file. Used for Real-Time Data Transfer
 * */
void RTprintheader(struct CommDriver *comm, log_t* log_struct, parameter_t* ps, raw_sensor_t* raw_sensors, SensorRTFlags_t * flags);

/**
 * Sends the required ending message to close a log file following the end of a flight. Used for 
 * Real-Time Data Transfer
 * */
void RTprintend(struct CommDriver *comm);

 /**
  * Prints all the log information.
  */
 void printLogging(struct CommDriver *comm, log_t* log_struct, parameter_t* ps, raw_sensor_t* raw_sensors);

 /**
  * Resets and clears logged data
  */
 void resetLogging();


#endif /* LOG_DATA_H_ */

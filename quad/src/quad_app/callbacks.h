#ifndef __callbacks_h
#define __callbacks_h

#include "commands.h"
/* Grab some stupid stuff from legacy code */
struct modular_structs;
typedef int (command_cb)(struct modular_structs *, struct metadata *, unsigned char *, unsigned short);

#endif

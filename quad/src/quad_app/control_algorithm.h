/*
 * control_algorithm.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef CONTROL_ALGORITHM_H_
#define CONTROL_ALGORITHM_H_
 
#include <stdio.h>

#include "log_data.h"
#include "sensor_processing.h"
#include "type_def.h"

/**
 * @brief 
 *      Initializes everything used in the control algorithm.
 *
 * @return
 *      error message
 *
 */
int control_algorithm_init(parameter_t * parameter_struct);

/**
 * @brief 
 *      Runs the control algorithm on the data and outputs a command for actuators.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param sensor_struct
 *      structure of the processed data from the sensors
 *
 * @param setpoint_struct
 *      structure of the setpoints used in the controller
 *
 * @param parameter_struct
 *      structure of the parameters used in the controller
 *
 * @param user_defined_struct
 *      structure of the user defined variables
 *
 * @param raw_actuator_struct
 *      structure of the commmands outputted to go to the actuators
 *
 * @return 
 *      error message
 *
 */
int control_algorithm(log_t* log_struct,
					  user_input_t * user_input_struct,
					  sensor_t* sensor_struct,
                      setpoint_t* setpoint_struct, 
                      parameter_t* parameter_struct, 
                      user_defined_t* user_defined_struct, 
                      actuator_command_t* actuator_struct,
                      modular_structs_t* structs);

/**
 * @brief
 *      Internally used functions
 *
 */
void setPIDCoeff(PID_t* p, float pValue, float iValue, float dValue);

#endif /* CONTROL_ALGORITHM_H_ */

#ifndef QUAD_APP_H
#define QUAD_APP_H

#include "type_def.h"

int quad_main(int (*setup_hardware)(hardware_t *hardware_struct));

#endif

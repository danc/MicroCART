#include "util.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "PID.h"
#include "log_data.h"
#include "controllers.h"

extern int motor0_bias, motor1_bias, motor2_bias, motor3_bias;

/**
 * Reads all 6 receiver channels at once
 */
void read_rec_all(struct RCReceiverDriver *rc_receiver, float *mixer){
  int i;
  for(i = 0; i < 6; i++){
    rc_receiver->read(rc_receiver, i, &mixer[i]);
  }
  return;
}

int hexStrToInt(char *buf, int startIdx, int endIdx) {
	int result = 0;
	int i;
	int power = 0;
	for (i=endIdx; i >= startIdx; i--) {
		int value = buf[i];
		if ('0' <= value && value <= '9') {
			value -= '0';
		} else if ('a' <= value && value <= 'f') {
			value -= 'a';
			value += 10;
		} else if ('A' <= value && value <= 'F') {
			value -= 'A';
			value += 10;
		}

		result += (2 << (4 * power)) * value;
		power++;
	}

	return result;
}

/**
 * Argument is the reading from the pwm_recorder4 which is connected to the gear pwm
 * If the message from the receiver is 0 - gear, kill the system by sending a 1
 * Otherwise, do nothing
 */
int read_kill(float gear) {
  if (gear_is_engaged(gear)) return 0;
  else return 1;
}

int gear_is_engaged(float gear) {
  if (gear > GEAR_MID) return 1;
  else return 0;
}

int read_flap(float flap) {
  if (flap_is_engaged(flap)) return 1;
  else return 0;
}

int flap_is_engaged(float flap) {
  if (flap > FLAP_MID) return 1;
  else return 0;
}

/**
 * Return true if the transmitter is on. This is mostly a failsafe to be sure we don't
 * drop out of the air if the receiver cuts out momentarily. If the receiver
 * cuts out, we expect the rc values will flatline at 0. So if we get a float zero,
 * then assume the transmitter is disconnected.
 */
int is_transmitter_on(float gear) {
	if (gear < 0.00001) return 0;
	else return 1;
}

/**
 * Turns off the motors
 */
void kill_motors(struct MotorDriver *motors) {
  motors->write(motors, 0, MOTOR_MIN);
  motors->write(motors, 1, MOTOR_MIN);
  motors->write(motors, 2, MOTOR_MIN);
  motors->write(motors, 3, MOTOR_MIN);
}

int build_int(u8 *buff) {
  return  buff[3] << 24
    | buff[2] << 16
    | buff[1] << 8
    | buff[0];
}

float build_float(u8 *buff) {
  union {
    float f;
    int i;
  } x;

  x.i =  buff[3] << 24
    | buff[2] << 16
    | buff[1] << 8
    | buff[0];
  return x.f;
}

int16_t build_short(u8* buff) {
	return (buff[0] & 0xFF) | ((buff[1] & 0xFF) << 8);
}

void pack_short(int16_t val, u8* buff) {
	buff[0] = val & 0xFF;
	buff[1] = (val >> 8) & 0xFF;
}

void pack_float(float val, u8* buff) {
	memcpy(buff, &val, sizeof(val));
}

/*
    Helper to return single bit of u32 data. This returns the "position"'th bit of the given u32,
    assuming it is Zero indexed.
*/
u32 read_bit(u32 data, int position) {
    return (data >> position) & 1;
}


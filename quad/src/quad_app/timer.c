#include "timer.h"

u64 before = 0, after = 0;
float LOOP_TIME; // microseconds
float time_stamp = 0;

struct TimerDriver *global_timer;
struct TimerDriver *axi_timer;

void timer_init_globals(struct TimerDriver *given_global_timer, struct TimerDriver *given_axi_timer) {
  global_timer = given_global_timer;
  axi_timer = given_axi_timer;
}

void timer_axi_reset() {
	axi_timer->restart(axi_timer);
}

int timer_start_loop()
{
  //timing code
  LOOP_TIME = ((float)(after - before));
  global_timer->read(global_timer, &before);
  axi_timer->restart(axi_timer);
  return 0;
}

int timer_end_loop(log_t *log_struct)
{
  // get number of useconds its taken to run the loop thus far
  u64 usec_loop;
  axi_timer->read(axi_timer, &usec_loop);

  // attempt to make each loop run for the same amount of time
  while(usec_loop < DESIRED_USEC_PER_LOOP) {
    axi_timer->read(axi_timer, &usec_loop);
  }

  //timing code
  global_timer->read(global_timer, &after);
  time_stamp += get_last_loop_time();

  // Log the timing information
  log_struct->time_stamp = time_stamp;
  log_struct->time_slice = get_last_loop_time();

  return 0;
}

float get_last_loop_time() {
  return (float)LOOP_TIME / 1000000;
}

u64 timer_get_count() {
  u64 time;
  return axi_timer->read(axi_timer, &time);
  return time;
}

/*
 * log_data.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "PID.h"
#include "type_def.h"
#include "log_data.h"
#include "communication.h"
#include "computation_graph.h"
#include "graph_blocks.h"
#include "timer.h"

// Current index of the log array
static int arrayIndex = 0;
// Size of the array
static int arraySize = 0;

struct graph_tuple { // Tuple for
    int block_id;
    int sub_id;
};

// Holds a statically allocated string, as well as info about its size and capacity
// Used by safe_sprintf_cat
struct str {
	char* str;
	size_t size;
	size_t capacity;
};


 /**
  * Forward declarations
  */
 void format_log(int idx, log_t* log_struct, struct str* buf);

struct graph_tuple log_outputs[MAX_LOG_NUM];
struct graph_tuple log_params[MAX_LOG_NUM];
static size_t n_outputs;
static size_t n_params;

static float* logArray = NULL;
static int row_size;

static char units_output_str[512] = {};
static char units_param_str[512] = {};
static struct str units_output = { .str = units_output_str, .size = 0, .capacity = sizeof(units_output_str)};
static struct str units_param = { .str = units_param_str, .size = 0, .capacity = sizeof(units_output_str)};

/*
 * Does an sprintf and concatenation. Used just like sprintf, but pass in a pointer to a struct str instead.
 * Returns the number of bytes that would have been written (just like snprintf)
 * Will not write more than is available in the given string
*/
int safe_sprintf_cat(struct str *str, const char *fmt, ...) {
	size_t available = str->capacity - str->size;
	va_list args;
	va_start(args, fmt);
	// Print to offset not more than remaining capacity characters
	size_t full_size = vsnprintf(str->str + str->size, available, fmt, args);
	va_end(args);
	if (full_size >= available) { // Check for truncation
		str->size = str->capacity;
	} else {
		str->size = str->size + full_size;
	}
	return full_size;
}

void addOutputToLog(log_t* log_struct, int controller_id, int output_id, char* units) {
	if (n_outputs < MAX_LOG_NUM) {
		log_outputs[n_outputs].block_id = controller_id;
		log_outputs[n_outputs].sub_id = output_id;
		n_outputs++;
		safe_sprintf_cat(&units_output, "\t%s", units);
	}
}
void addParamToLog(log_t* log_struct, int controller_id, int param_id, char* units) {
	if (n_params < MAX_LOG_NUM) {
		log_params[n_params].block_id = controller_id;
		log_params[n_params].sub_id = param_id;
		n_params++;
		safe_sprintf_cat(&units_param, "\t%s", units);
	}
}

void initialize_logging(log_t* log_struct, parameter_t* ps, SensorRTFlags_t * flags) {
	char* rad = "rad";
	char* rad_s = "rad/s";
	char* pwm_val = "10ns_dutycycle";
	char* m = "m";
	char* m_s = "m/s";

	addOutputToLog(log_struct, ps->alt_pid, PID_CORRECTION, pwm_val);
	addOutputToLog(log_struct, ps->x_pos_pid, PID_CORRECTION, rad);
	addOutputToLog(log_struct, ps->y_pos_pid, PID_CORRECTION, rad);
	addOutputToLog(log_struct, ps->pitch_pid, PID_CORRECTION, rad_s);
	addOutputToLog(log_struct, ps->roll_pid, PID_CORRECTION, rad_s);
	addOutputToLog(log_struct, ps->yaw_pid, PID_CORRECTION, rad_s);
	addOutputToLog(log_struct, ps->pitch_r_pid, PID_CORRECTION, pwm_val);
	addOutputToLog(log_struct, ps->roll_r_pid, PID_CORRECTION, pwm_val);
	addOutputToLog(log_struct, ps->yaw_r_pid, PID_CORRECTION, pwm_val);
	addOutputToLog(log_struct, ps->pitch_trim_add, CONST_VAL, rad);
	addOutputToLog(log_struct, ps->mixer, MIXER_MOTOR0, pwm_val);
	addOutputToLog(log_struct, ps->mixer, MIXER_MOTOR1, pwm_val);
	addOutputToLog(log_struct, ps->mixer, MIXER_MOTOR2, pwm_val);
	addOutputToLog(log_struct, ps->mixer, MIXER_MOTOR3, pwm_val);
	addOutputToLog(log_struct, ps->x_vel_pid, PID_CORRECTION, rad);
	addOutputToLog(log_struct, ps->y_vel_pid, PID_CORRECTION, rad);
	addOutputToLog(log_struct, ps->x_vel, PID_CORRECTION, m_s);
	addOutputToLog(log_struct, ps->y_vel, PID_CORRECTION, m_s);
	addOutputToLog(log_struct, ps->of_angle_corr, ROT_OUT_X, m_s);
	addOutputToLog(log_struct, ps->of_angle_corr, ROT_OUT_Y, m_s);
	addOutputToLog(log_struct, ps->of_integ_x, INTEGRATED, m);
	addOutputToLog(log_struct, ps->of_integ_y, INTEGRATED, m);
	addOutputToLog(log_struct, ps->psi_sum, ADD_SUM, rad);
	addOutputToLog(log_struct, ps->mag_yaw, CONST_VAL, rad);

	addParamToLog(log_struct, ps->cur_roll, CONST_VAL, rad);
	addParamToLog(log_struct, ps->cur_yaw, CONST_VAL, rad);
	addParamToLog(log_struct, ps->x_set, CONST_VAL, m);
	addParamToLog(log_struct, ps->y_set, CONST_VAL, m);
	addParamToLog(log_struct, ps->alt_set, CONST_VAL, m);
	addParamToLog(log_struct, ps->yaw_set, CONST_VAL, m);
	addParamToLog(log_struct, ps->lidar, CONST_VAL, m);
	addParamToLog(log_struct, ps->vrpn_x, CONST_VAL, m);
	addParamToLog(log_struct, ps->vrpn_y, CONST_VAL, m);
	addParamToLog(log_struct, ps->vrpn_alt, CONST_VAL, m);
	addParamToLog(log_struct, ps->flow_vel_x, CONST_VAL, m_s);
	addParamToLog(log_struct, ps->flow_vel_y, CONST_VAL, m_s);
	addParamToLog(log_struct, ps->flow_quality, CONST_VAL, "none");

	int num_outputs = 0;
	if (flags->imuflags.acc_x)
	{
		num_outputs++;
	}
	if (flags->imuflags.acc_y)
	{
		num_outputs++;
	}
	if (flags->imuflags.acc_z)
	{
		num_outputs++;
	}
	if (flags->imuflags.gyro_x)
	{
		num_outputs++;
	}
	if (flags->imuflags.gyro_y)
	{
		num_outputs++;
	}
	if (flags->imuflags.gyro_z)
	{
		num_outputs++;
	}
	if (flags->imuflags.mag_x)
	{
		num_outputs++;
	}
	if (flags->imuflags.mag_y)
	{
		num_outputs++;
	}
	if (flags->imuflags.mag_z)
	{
		num_outputs++;
	}
	//Track number of outputs desired for log utilizing flags struct. Add 1 for timestamp as
	//time stamp will always be required for any logging.
	row_size = n_outputs + n_params + num_outputs + 1;
	size_t needed_memory = sizeof(float) * row_size * LOG_STARTING_SIZE;
	logArray = malloc(needed_memory);
	if (!logArray) { // malloc failed
		arraySize = 0;
	} else {
		arraySize = LOG_STARTING_SIZE;
	}
}

int log_data(log_t* log_struct, parameter_t* ps, SensorRTFlags_t * flags)
{
	if(arrayIndex >= arraySize)
	{
		return 1;
	}
	float* thisRow = &logArray[arrayIndex * row_size];
	int offset = 0;

	//TODO make this not stupid
	thisRow[offset++] = log_struct->time_stamp;
	if (flags->imuflags.acc_x)
	{
		thisRow[offset++] = log_struct->gam.accel_x;
	}
	if (flags->imuflags.acc_y)
	{
		thisRow[offset++] = log_struct->gam.accel_y;
	}
	if (flags->imuflags.acc_z)
	{
		thisRow[offset++] = log_struct->gam.accel_z;
	}
	if (flags->imuflags.gyro_x)
	{
		thisRow[offset++] = log_struct->gam.gyro_xVel_p;
	}
	if (flags->imuflags.gyro_y)
	{
		thisRow[offset++] = log_struct->gam.gyro_yVel_q;
	}
	if (flags->imuflags.gyro_z)
	{
		thisRow[offset++] = log_struct->gam.gyro_zVel_r;
	}
	if (flags->imuflags.mag_x)
	{
		thisRow[offset++] = log_struct->gam.mag_x;
	}
	if (flags->imuflags.mag_y)
	{
		thisRow[offset++] = log_struct->gam.mag_y;
	}
	if (flags->imuflags.mag_z)
	{
		thisRow[offset++] = log_struct->gam.mag_z;
	}
	int i;
	for (i = 0; i < n_params; i++) {
		thisRow[offset++] = graph_get_param_val(ps->graph, log_params[i].block_id, log_params[i].sub_id);
	}
	for (i = 0; i < n_outputs; i++) {
		thisRow[offset++] = graph_get_output(ps->graph, log_outputs[i].block_id, log_outputs[i].sub_id);
	}
	arrayIndex++;
	return 0;
}

void RTprintheader(struct CommDriver *comm, log_t* log_struct, parameter_t* ps, raw_sensor_t* raw_sensors, SensorRTFlags_t * flags) {
	if (arrayIndex == 0) {
		// Don't send any logs if nothing was logged
		return;
	}
	else if (flags->flag_count == 0)
	{
		//Don't send anything if flags has not been configured or if no data has been requested to be sent.
		return;
	}
	char buf_arr[2560] = {};
	struct str buf = {.str = buf_arr, .size = 0, .capacity = sizeof(buf_arr)};

	// Let user know that allocation failed
	if (arraySize != LOG_STARTING_SIZE) {
		safe_sprintf_cat(&buf, "Failed to allocate enough log memory\n");
		send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);
		return;
	}

	int i;
	// Comment header
	safe_sprintf_cat(&buf, "# MicroCART On-board Quad Log\n# Sample number: %d\n", arrayIndex);
	safe_sprintf_cat(&buf, "# IMU IIC failures: %d\n", raw_sensors->gam.error.consErrorCount);
	safe_sprintf_cat(&buf, "# LiDAR IIC failures: %d\n", raw_sensors->lidar.error.consErrorCount);
	safe_sprintf_cat(&buf, "# Optical Flow IIC failures: %d\n", raw_sensors->optical_flow.error.consErrorCount);

	// List Pid Constants
	for (i = 0; i < ps->graph->n_nodes; ++i) {
		struct graph_node* node = &ps->graph->nodes[i];
		if (node->type->type_id == BLOCK_PID) {
			double kp, ki, kd, alpha;
			kp = graph_get_param_val(ps->graph, i, 0);
			ki = graph_get_param_val(ps->graph, i, 1);
			kd = graph_get_param_val(ps->graph, i, 2);
			alpha = graph_get_param_val(ps->graph, i, 3);
			safe_sprintf_cat(&buf, "# %s :\tKp = %lf Ki = %lf Kd = %lf Alpha = %lf\n", node->name, kp, ki, kd, alpha);
		}
	}

	// TODO make this not stupid
	safe_sprintf_cat(&buf, "%%Time");
	if (flags->imuflags.acc_x)
	{
		safe_sprintf_cat(&buf, "\taccel_x");
	}
	if (flags->imuflags.acc_y)
	{
		safe_sprintf_cat(&buf, "\taccel_y");
	}
	if (flags->imuflags.acc_z)
	{
		safe_sprintf_cat(&buf, "\taccel_z");
	}
	if (flags->imuflags.gyro_x)
	{
		safe_sprintf_cat(&buf, "\tgyro_x");
	}
	if (flags->imuflags.gyro_y)
	{
		safe_sprintf_cat(&buf, "\tgyro_y");
	}
	if (flags->imuflags.gyro_z)
	{
		safe_sprintf_cat(&buf, "\tgyro_z");
	}
	if (flags->imuflags.mag_x)
	{
		safe_sprintf_cat(&buf, "\tmag_x");
	}
	if (flags->imuflags.mag_y)
	{
		safe_sprintf_cat(&buf, "\tmag_y_x");
	}
	if (flags->imuflags.mag_z)
	{
		safe_sprintf_cat(&buf, "\tmag_z");
	}

	// Print all the recorded block parameters
	for (i = 0; i < n_params; i++) {
		const char* block_name = ps->graph->nodes[log_params[i].block_id].name;
		const char* output_name = ps->graph->nodes[log_params[i].block_id].type->param_names[log_params[i].sub_id];
		safe_sprintf_cat(&buf, "\t%s_%s", block_name, output_name);
	}
	// Print all the recorded block outputs
	for (i = 0; i < n_outputs; i++) {
		const char* block_name = ps->graph->nodes[log_outputs[i].block_id].name;
		const char* param_name = ps->graph->nodes[log_outputs[i].block_id].type->output_names[log_outputs[i].sub_id];
		safe_sprintf_cat(&buf, "\t%s_%s", block_name, param_name);
	}
	safe_sprintf_cat(&buf, "\n");

	// Send header names
	send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);

	// Send units header
	buf.size = 0;

	safe_sprintf_cat(&buf, "&s"); // The pre-defined ones
	if (flags->imuflags.acc_x)
	{
		safe_sprintf_cat(&buf, "\tG");
	}
	if (flags->imuflags.acc_y)
	{
		safe_sprintf_cat(&buf, "\tG");
	}
	if (flags->imuflags.acc_z)
	{
		safe_sprintf_cat(&buf, "\tG");
	}
	if (flags->imuflags.gyro_x)
	{
		safe_sprintf_cat(&buf, "\trad/s");
	}
	if (flags->imuflags.gyro_y)
	{
		safe_sprintf_cat(&buf, "\trad/s");
	}
	if (flags->imuflags.gyro_z)
	{
		safe_sprintf_cat(&buf, "\trad/s");
	}
	if (flags->imuflags.mag_x)
	{
		safe_sprintf_cat(&buf, "\tuT");
	}
	if (flags->imuflags.mag_y)
	{
		safe_sprintf_cat(&buf, "\tuT");
	}
	if (flags->imuflags.mag_z)
	{
		safe_sprintf_cat(&buf, "\tuT");
	}

	safe_sprintf_cat(&buf, units_output.str);
	safe_sprintf_cat(&buf, units_param.str);
	safe_sprintf_cat(&buf, "\n");
	send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);
}

/**
 * Print current sensor info to log file. This data is held at the current ArrayIndex entry of the log_struct.
 */
void RTprintLogging(struct CommDriver *comm, log_t* log_struct, parameter_t* ps, raw_sensor_t* raw_sensors, SensorRTFlags_t * flags){
	if (arrayIndex == 0)
	{
		return;
	}
	else if (flags->flag_count == 0)
	{
		//Don't send anything if flags has not been configured or if no data has been requested to be sent.
		return;
	}
	char buf_arr[2560] = {};
	struct str buf = {.str = buf_arr, .size = 0, .capacity = sizeof(buf_arr)};
	//Send only the most previous log data.
	format_log(arrayIndex - 1, log_struct, &buf);
	send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);
	// This is a stupid hack because if the axi timer is not reset in awhile, it always returns 0, and the timer_end_loop() call hangs forever after a long log
	// Not 100% certain this works
	timer_axi_reset();
}

void RTprintend(struct CommDriver *comm)
{
	if (arrayIndex == 0)
	{
		return;
	}
	char buf_arr[2560] = {};
	struct str buf = {.str = buf_arr, .size = 0, .capacity = sizeof(buf_arr)};
	// Empty message of type LOG_END to indicate end of log
	send_data(comm->uart, LOG_END_ID, 0, (u8*)buf.str, 0);
}

/**
 * Prints all the log information. Currently replaced by the RTprintglogging method. 
 *
 * TODO: This should probably be transmitting in binary instead of ascii
 */

void printLogging(struct CommDriver *comm, log_t* log_struct, parameter_t* ps, raw_sensor_t* raw_sensors){
	if (arrayIndex == 0) {
		// Don't send any logs if nothing was logged
		return;
	}
	char buf_arr[2560] = {};
	struct str buf = {.str = buf_arr, .size = 0, .capacity = sizeof(buf_arr)};

	// Let user know that allocation failed
	if (arraySize != LOG_STARTING_SIZE) {
		safe_sprintf_cat(&buf, "Failed to allocate enough log memory\n");
		send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);
		return;
	}

	int i;
	// Comment header
	safe_sprintf_cat(&buf, "# MicroCART On-board Quad Log\n# Sample number: %d\n", arrayIndex);
	safe_sprintf_cat(&buf, "# IMU IIC failures: %d\n", raw_sensors->gam.error.consErrorCount);
	safe_sprintf_cat(&buf, "# LiDAR IIC failures: %d\n", raw_sensors->lidar.error.consErrorCount);
	safe_sprintf_cat(&buf, "# Optical Flow IIC failures: %d\n", raw_sensors->optical_flow.error.consErrorCount);

	// List Pid Constants
	for (i = 0; i < ps->graph->n_nodes; ++i) {
		struct graph_node* node = &ps->graph->nodes[i];
		if (node->type->type_id == BLOCK_PID) {
			double kp, ki, kd, alpha;
			kp = graph_get_param_val(ps->graph, i, 0);
			ki = graph_get_param_val(ps->graph, i, 1);
			kd = graph_get_param_val(ps->graph, i, 2);
			alpha = graph_get_param_val(ps->graph, i, 3);
			safe_sprintf_cat(&buf, "# %s :\tKp = %lf Ki = %lf Kd = %lf Alpha = %lf\n", node->name, kp, ki, kd, alpha);
		}
	}

	// Header names for the pre-defined values
	safe_sprintf_cat(&buf, "%%Time\taccel_x\taccel_y\taccel_z\tgyro_x\tgyro_y\tgyro_z\tmag_x\tmag_y\tmag_z");

	// Print all the recorded block parameters
	for (i = 0; i < n_params; i++) {
		const char* block_name = ps->graph->nodes[log_params[i].block_id].name;
		const char* output_name = ps->graph->nodes[log_params[i].block_id].type->param_names[log_params[i].sub_id];
		safe_sprintf_cat(&buf, "\t%s_%s", block_name, output_name);
	}
	// Print all the recorded block outputs
	for (i = 0; i < n_outputs; i++) {
		const char* block_name = ps->graph->nodes[log_outputs[i].block_id].name;
		const char* param_name = ps->graph->nodes[log_outputs[i].block_id].type->output_names[log_outputs[i].sub_id];
		safe_sprintf_cat(&buf, "\t%s_%s", block_name, param_name);
	}
	safe_sprintf_cat(&buf, "\n");

	// Send header names
	send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);

	// Send units header
	buf.size = 0;
	safe_sprintf_cat(&buf, "&s\tG\tG\tG\trad/s\trad/s\trad/s\tuT\tuT\tuT"); // The pre-defined ones
	safe_sprintf_cat(&buf, units_output.str);
	safe_sprintf_cat(&buf, units_param.str);
	safe_sprintf_cat(&buf, "\n");
	send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);

	/*************************/
	/* print & send log data */
	for(i = 0; i < arrayIndex; i++){
		format_log(i, log_struct, &buf);
		send_data(comm->uart, LOG_ID, 0, (u8*)buf.str, buf.size);
		// This is a stupid hack because if the axi timer is not reset in awhile, it always returns 0, and the timer_end_loop() call hangs forever after a long log
		// Not 100% certain this works
		timer_axi_reset();
	}

	// Empty message of type LOG_END to indicate end of log
	send_data(comm->uart, LOG_END_ID, 0, (u8*)buf.str, 0);
}

void resetLogging() {
	arrayIndex = 0;
}

/*
  * Fills the given buffer as ASCII for the recorded index, and returns the length of the string created
*/
void format_log(int idx, log_t* log_struct, struct str* buf) {
	int i;
	buf->size = 0;
	
	float* row = &logArray[idx * row_size];\

	safe_sprintf_cat(buf, "%f", row[0]);
	for (i = 1; i < row_size; i++) {
		safe_sprintf_cat(buf, "\t%f", row[i]);
	}
	safe_sprintf_cat(buf, "\n");
}

/*
    Initially sets it so that all debug values are zero
*/
void initializeFlags(SensorRTFlags_t * flags) {
  IMUFlags_t imu = {0,0,0, 0,0,0, 0,0,0};
  flags->imuflags = imu;
  OptFlowFlags_t optf = {0,0, 0,0, 0,0};
  flags->optflowflags = optf;
  lidarFlags_t lidar = {0};
  flags->lidarflags = lidar;
  SensorErrorFlags_t se = {0,0,0, 0,0,0};
  flags->errorflags = se;
  flags->flag_count = 0;
}

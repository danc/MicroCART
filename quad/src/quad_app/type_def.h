/*
 * struct_def.h
 *
 *  Created on: Mar 2, 2016
 *      Author: ucart
 */

#ifndef TYPE_DEF_H_
#define TYPE_DEF_H_

#include <stdint.h>
#include "commands.h"
#include "computation_graph.h"
#include "hw_iface.h"
#include "biquad_filter.h"

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef unsigned long long u64;
typedef signed char i8;
typedef signed short i16;
typedef signed long i32;

/**
 * @brief
 *      The modes for autonomous and manual flight.
 *
 */
enum flight_mode{
	AUTO_FLIGHT_MODE,
	MANUAL_FLIGHT_MODE
};

//----------------------------------------------------------------------------------------------
//     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
//---------------------------------------------------------------------------------------------|
// msg param|| beg char |       msg type         | msg id | data len (bytes) | data | checksum |
//-------------------------------------------------------------------------------------------- |
//     bytes||     1    |          2             |    2   |        2         | var  |    1     |
//----------------------------------------------------------------------------------------------

typedef struct {
	char** tokens;
	int numTokens;
} tokenList_t;

typedef struct commands{
	int pitch, roll, yaw, throttle;
}commands;

typedef struct raw{
	int x,y,z;
}raw;

typedef struct PID_Consts{
	float P, I, D;
}PID_Consts;

//Camera system info
typedef struct {
	int packetId;

	double y_pos;
	double x_pos;
	double alt_pos;

	double yaw;
	double yawOffset;
	double roll;
	double pitch;
} quadPosition_t;

typedef struct {
	float yaw;
	float roll;
	float pitch;
	float throttle;
} quadTrims_t;

typedef struct SensorError {
	uint32_t errorCount;
	uint32_t consErrorCount;
} SensorError_t;

//Gyro, accelerometer, and magnetometer data structure
//Used for reading an instance of the sensor data
typedef struct gam {

	// GYRO
	//Xint16 raw_gyro_x, raw_gyro_y, raw_gyro_z;

	float gyro_xVel_p; // In degrees per second
	float gyro_yVel_q;
	float gyro_zVel_r;

	// ACCELEROMETER
	//Xint16 raw_accel_x, raw_accel_y, raw_accel_z;

	float accel_x; //In g
	float accel_y;
	float accel_z;

	float accel_roll;
	float accel_pitch;

	//float heading; // In degrees

	float mag_x; //Magnetic north: ~50 uT
	float mag_y;
	float mag_z;

	float magX_correction;
	float magY_correction;
	float magZ_correction;

	int magDRDYCount;

	SensorError_t error;
} gam_t;

typedef struct lidar {
	float distance_m; // distance in meters

	SensorError_t error;
} lidar_t;

typedef struct px4flow {
	double xVel, yVel;

	// Flow around the x and y axes in radians
	double flow_x_rad, flow_y_rad;

	// Time since last readout in seconds
	double dt;

	double xVelFilt, yVelFilt;

	int16_t quality;

	SensorError_t error;
} px4flow_t;

/** is this used??? */
typedef struct gps {
	double lat, lon;
	double vel, course;

	int fixed;
	unsigned int numSats;
} gps_t;

typedef struct PID_t {
	float current_point;	// Current value of the system
	float setpoint;		// Desired value of the system
	float Kp;				// Proportional constant
	float Ki;				// Integral constant
	float Kd;				// Derivative constant
	float prev_error;		// Previous error
	float acc_error;		// Accumulated error
	float pid_correction;	// Correction factor computed by the PID
	float dt; 				// sample period
} PID_t;

typedef struct PID_values{
	float P;	// The P component contribution to the correction output
	float I;	// The I component contribution to the correction output
	float D;	// The D component contribution to the correction output
	float error; // the error of this PID calculation
	float change_in_error; // error change from the previous calc. to this one
	float pid_correction; // the correction output (P + I + D)
} PID_values;

///////// MAIN MODULAR STRUCTS
/**
 * @brief
 *      Holds the data inputed by the user
 *
 */
typedef struct user_input_t {
	float rc_commands[6]; 	// Commands from the RC transmitter


//	float cam_x_pos;	// Current x position from the camera system
//	float cam_y_pos;	// Current y position from the camera system
//	float cam_z_pos;	// Current z position from the camera system
//	float cam_roll;		// Current roll angle from the camera system
//	float cam_pitch;	// Current pitch angle from the camera system
//	float cam_yaw;		// Current yaw angle from the camera system

	float yaw_manual_setpoint;
	float roll_angle_manual_setpoint;
	float pitch_angle_manual_setpoint;

	int locationFresh;
	int receivedBeginUpdate;
} user_input_t;

/**
 * @brief
 *      Holds the log data to be sent to the ground station. It may hold the
 * timestamp of when a sensor's data was obtained.
 *
 */
typedef struct log_t {
	// Time
	float time_stamp;
	float time_slice;

	// Id
	int packetId;

	gam_t gam; 	// Raw and calculated gyro, accel, and mag values are all in gam_t

	/*
	float phi_dot, theta_dot, psi_dot; // gimbal equation values

	quadPosition_t currentQuadPosition;

	float roll_angle_filtered, pitch_angle_filtered;
	float lidar_altitude;

	float pid_P_component, pid_I_component, pid_D_component; // use these generically for any PID that you are testing

	// PID coefficients and errors
	PID_t local_x_PID, local_y_PID, altitude_PID;
	PID_t angle_yaw_PID, angle_roll_PID, angle_pitch_PID;
	PID_t ang_vel_yaw_PID, ang_vel_roll_PID, ang_vel_pitch_PID;

	PID_values local_x_PID_values, local_y_PID_values, altitude_PID_values;
	PID_values angle_yaw_PID_values, angle_roll_PID_values, angle_pitch_PID_values;
	PID_values ang_vel_yaw_PID_values, ang_vel_roll_PID_values, ang_vel_pitch_PID_values;

	// RC commands
	commands commands;

	//trimmed values
	quadTrims_t trims;

	int motors[4];
	*/
} log_t;

/**
 * @brief
 *      Holds the raw data from the sensors and the timestamp if available
 *
 */
typedef struct raw_sensor {

	gam_t gam;
	lidar_t lidar;
    	px4flow_t optical_flow;
	// Structures to hold the current quad position & orientation
	// This is mostly unused?
	quadPosition_t currentQuadPosition;

	

} raw_sensor_t;

/**
 * @brief
 *      Holds the processed data from the sensors and the timestamp if available
 *
 */
typedef struct sensor {
	int acc_x;		// accelerometer x data
	int acc_x_t;	// time of accelerometer x data

	int acc_y;		// accelerometer y data
	int acc_y_t;	// time of accelerometer y data

	int acc_z;		// accelerometer z data
	int acc_z_t;	// time of accelerometer z data


	float gyr_x;		// gyroscope x data
	int gyr_x_t;	// time of gyroscope x data

	float gyr_y;		// gyroscope y data
	int gyr_y_t;	// time of gyroscope y data

	float gyr_z;		// gyroscope z data
	int gyr_z_t;	// time of gyroscope z data


	// Complementary filter outputs
	float pitch_angle_filtered;
	float roll_angle_filtered;
	float yaw_angle_filtered;

	// Z-axis value obtained from LiDAR
	// Note that this is not distance, as our Z-axis points upwards.
	float lidar_altitude;

	float phi_dot, theta_dot, psi_dot;

	// Structures to hold the current quad position & orientation
	quadPosition_t currentQuadPosition;
	quadTrims_t trims;

	struct biquadState accel_x_filt;
	struct biquadState accel_y_filt;
	struct biquadState accel_z_filt;
	struct biquadState flow_x_filt;
	struct biquadState flow_y_filt;
	struct biquadState phi_dot_filt;
	struct biquadState psi_dot_filt;
	struct biquadState theta_dot_filt;
	struct biquadState mag_x_filt;
	struct biquadState mag_y_filt;

	// Information obtained from optical flow sensor 
	px4flow_t optical_flow;
} sensor_t;

/**
 * @brief
 *      Holds the setpoints to be used in the controller
 *
 */
typedef struct setpoint_t {
	quadPosition_t desiredQuadPosition;
} setpoint_t;

/**
 * @brief
 *      Holds the parameters that are specific to whatever type of
 *      control algorithm is being used
 *
 */
typedef struct parameter_t {
	struct computation_graph* graph;
	// PID blocks
	int roll_pid;
	int pitch_pid;
	int yaw_pid;
	int roll_r_pid;
	int pitch_r_pid;
	int yaw_r_pid;
	int x_pos_pid;
	int y_pos_pid;
	int alt_pid;
	// Sensor blocks
	int cur_pitch;
	int cur_roll;
	int cur_yaw;
	int gyro_y;
	int gyro_x;
	int gyro_z;
	int lidar;
	int flow_vel_x; // optical flow
	int flow_vel_y;
	int flow_vel_x_filt;
	int flow_vel_y_filt;
	int flow_quality; // Quality value returned by optical flow sensor
	int flow_distance;
	// VRPN blocks
	int vrpn_x;
	int vrpn_y;
	int vrpn_alt;
	int vrpn_pitch, vrpn_roll;
	// RC blocks
	int rc_pitch;
	int rc_roll;
	int rc_yaw;
	int rc_throttle;
	// Desired positions
	int x_set;
	int y_set;
	int alt_set;
	int yaw_set;
	// Clamps
	int clamp_d_pwmP;
	int clamp_d_pwmR;
	int clamp_d_pwmY;
	int yaw_clamp;
	// Loop times
	int angle_time;
	int pos_time;
	// Signal mixer
	int mixer;
	// "trim" for autonomous
	int throttle_trim;
	int throttle_trim_add;
	int pitch_trim;
	int pitch_trim_add;
	int yaw_trim;
	int yaw_trim_add;
	// Velocity nodes
	int x_vel_pid;
	int y_vel_pid;
	int x_vel;
	int y_vel;
	int x_vel_clamp;
	int y_vel_clamp;
	int vel_x_gain;
	int vel_y_gain;
	// Sensor processing
	int yaw_correction;
	int of_angle_corr; // Corrects for the optical flow mounting angle
	int of_integ_x; // Integrates the optical flow data
	int of_integ_y;
	int of_trim_x; // Trim value for optical flow integrated value
	int of_trim_y;
	int of_trimmed_x; // Trimmed optical flow integrated value (of_integ_x + of_trim_x)
	int of_trimmed_y;
	//psi dot integration chain
	int psi_dot;
	int psi_dot_offset;
	int psi_dot_sum;
	int psi;
	int psi_offset;
	int psi_sum;
	int mag_yaw; //Complementary filtered magnetometer/gyro yaw
} parameter_t;

/**
 * @brief
 *      Holds user defined data for the controller
 *
 */
typedef struct user_defined_t {
	int flight_mode;
	int engaging_auto;
} user_defined_t;

/**
 * @brief
 *      Holds the raw actuator values before processing
 *
 */
typedef struct raw_actuator_t {

	float controller_corrected_motor_commands[6];

} raw_actuator_t;

/**
 * @brief
 *      Holds processed commands to go to the actuators
 *
 */
typedef struct actuator_command_t {
	float motor_magnitudes[4];
} actuator_command_t;

enum PWMChannels {
  MOTOR_0,
  MOTOR_1,
  MOTOR_2,
  MOTOR_3,
  RC_INPUT_0,
  RC_INPUT_1,
  RC_INPUT_2,
  RC_INPUT_3,
  RC_INPUT_4,
  RC_INPUT_5,
};

typedef struct hardware_t {
  struct I2CDriver i2c_0;
  struct I2CDriver i2c_1;
  struct RCReceiverDriver rc_receiver;
  struct MotorDriver motors;
  struct UARTDriver uart_0;
  struct UARTDriver uart_1;
  struct GPSDriver gps;
  struct CommDriver comm;
  struct TimerDriver global_timer;
  struct TimerDriver axi_timer;
  struct LEDDriver mio7_led;
  struct SystemDriver sys;
  struct IMUDriver imu;
  struct LidarDriver lidar;
  struct OpticalFlowDriver of;
} hardware_t;

typedef enum {
	OVERRIDE_NONE,  // 0 Use controller output
	OVERRIDE_ALL    // 1 Use override values
} override_mode_t;

typedef struct override_t {
	u8 mode;
	float values[4];
} override_t;

/* BEGIN FLAG STRUCTS CONTAINING ENABLED/DISABLED RT DATA TRANSFERS */
typedef struct lidarFlags
{
    int quadHeight;
    
} lidarFlags_t;

typedef struct IMUFlags
{
    int gyro_x;
    int gyro_y;
    int gyro_z;
    int acc_x;
    int acc_y;
    int acc_z;
    int mag_x;
    int mag_y;
    int mag_z;
    
} IMUFlags_t;

typedef struct OptFlowFlags
{
    int x_flow;
    int y_flow;
    int x_filter;
    int y_filter;
    int x_velocity;
    int y_velocity;
    
} OptFlowFlags_t;

typedef struct SensorErrorFlags
{
    int consec_lidar;
    int consec_imu;
    int consec_optFlow;
    int lidar;
    int imu;
    int optFlow; 
    
} SensorErrorFlags_t;

/*
	This struct contains all of the above pre-defined flag structs. For use in RT data logging configuration
*/
typedef struct SensorRTFlags
{
    IMUFlags_t imuflags;
    OptFlowFlags_t optflowflags;
    lidarFlags_t lidarflags;
    SensorErrorFlags_t errorflags;
    int flag_count;
    
} SensorRTFlags_t;

/* END FLAG STRUCTS */

/**
 * @brief
 * 		Structures to be used throughout
 */
typedef struct modular_structs {
	user_input_t user_input_struct;
	log_t log_struct;
	raw_sensor_t raw_sensor_struct;
	sensor_t sensor_struct;
	setpoint_t setpoint_struct;
	parameter_t parameter_struct;
	user_defined_t user_defined_struct;
	raw_actuator_t raw_actuator_struct;
	actuator_command_t actuator_command_struct;
	hardware_t hardware_struct;
	override_t override_struct;
	SensorRTFlags_t flags_struct;
} modular_structs_t;


//////// END MAIN MODULAR STRUCTS

#endif /* TYPE_DEF_H_ */

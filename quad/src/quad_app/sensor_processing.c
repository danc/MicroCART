/*
 * sensor_processing.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */
 
#include <stdio.h>
#include "log_data.h"
#include "sensor.h"
#include "conversion.h"
#include "sensor_processing.h"
#include "timer.h"
#include <math.h>

#define ALPHA 0.99
#define YAW_ALPHA	(0.995)
#define PX4FLOW_QUAL_MIN			(100)
#define PX4FLOW_VEL_DECAY					(0.99)

#define MAG_OFFSET_X				(-33.9844)
#define MAG_OFFSET_Y				(40.4922)

#define GYRO_Z_OFFSET				(0.0073)

#define MAX_VALID_LIDAR (10.0) // Maximum valid distance to read from LiDAR to update

int sensor_processing_init(sensor_t* sensor_struct) {
	// 10Hz cutoff at 200hz sampling
	float a0 = 0.0200833310260;
	float a1 = 0.0401666620520;
	float a2 = 0.0200833310260;
	float b1 = -1.561015391253;
	float b2 = 0.6413487153577;
	sensor_struct->accel_x_filt = filter_make_state(a0, a1, a2, b1, b2);
	sensor_struct->accel_y_filt = filter_make_state(a0, a1, a2, b1, b2);
	sensor_struct->accel_z_filt = filter_make_state(a0, a1, a2, b1, b2);

	// 10Hz filters for bias-corrected euler rates
	sensor_struct->phi_dot_filt = filter_make_state(a0, a1, a2, b1, b2);
	sensor_struct->theta_dot_filt = filter_make_state(a0, a1, a2, b1, b2);
	sensor_struct->psi_dot_filt = filter_make_state(a0, a1, a2, b1, b2);

	//1 Hert filter
	float vel_a0 = 2.3921e-4;
	float vel_a1 = 4.7841e-4;
	float vel_a2 = 2.3921e-4;
	float vel_b1 = -1.9381;
	float vel_b2 = 0.9391;
	sensor_struct->flow_x_filt = filter_make_state(vel_a0, vel_a1, vel_a2, vel_b1, vel_b2);
	sensor_struct->flow_y_filt = filter_make_state(vel_a0, vel_a1, vel_a2, vel_b1, vel_b2);

	float mag_a0 = 2.3921e-4;
	float mag_a1 = 4.7841e-4;
	float mag_a2 = 2.3921e-4;
	float mag_b1 = -1.9281;
	float mag_b2 = 0.9391;
	sensor_struct->mag_x_filt = filter_make_state(mag_a0, mag_a1, mag_a2, mag_b1, mag_b2);
	sensor_struct->mag_y_filt = filter_make_state(mag_a0, mag_a1, mag_a2, mag_b1, mag_b2);

	return 0;
}


// Focal length in mm = 16, in pixels is below
// static float focal_length_px = 16.0 / (4.0f * 6.0f) * 1000.0f; //original focal lenght: 12mm pixelsize: 6um, binning 4 enabled
/*
 * Convert integral frame flow in radians to velocity in m/s
 * Theta = pitch, phi = roll
 * Note that we pass phi and theta as angles, but psi dot, because we don't have a complementary-filtered psi (yaw)
 */
 void flow_gyro_compensation(sensor_t* sensor_struct, double distance,
		                     double phi, double theta, double psi_d_new) {
	 //------ Gyro compensation stuff. It seems to make the quadcopter unstable, although all the math checks out and data seems better
	/*
	// The reason for converting back to euler rates instead of using raw gyroscope data is so that we can use
	// The complementary-filtered angles, which will prevent gyroscope drift from creating position drift
	static double last_phi = 0;
	static double last_theta = 0;

	// Calculate difference in angles
	double phi_d_new, theta_d_new;
	float loop_dt = get_last_loop_time();
	if (loop_dt != 0) { // divide by zero check
		phi_d_new = (phi - last_phi) / loop_dt;
		theta_d_new = (theta - last_theta) / loop_dt;
	} else {phi_d_new = theta_d_new = 0;}

	// Run low-pass filters on euler angle rates
	float phi_d = biquad_execute(&sensor_struct->phi_dot_filt, phi_d_new);
	float theta_d = biquad_execute(&sensor_struct->theta_dot_filt, theta_d_new);
	float psi_d = biquad_execute(&sensor_struct->psi_dot_filt, psi_d_new);

	// Convert angles to body rotations (gyroscope equivalents)
	///////////////////-------  Inverse of AEB matrix  -------//////////////////
	// | p |    |  1  0           -sin(Phi)           |  | Phi_d   |
	// | q |  = |  0  cos(Phi)    cos(Theta)*sin(Phi) |  | theta_d |
	// | r |    |  0  -sin(Phi)   cos(Phi)*cos(Theta) |  | Psi_d   |

	double sin_phi = sin(phi);
	double cos_theta = cos(theta);
	double cos_phi = cos(phi);

	// We re-calculate p, q, r instead of using the gyroscope values because these are calculated using
	// the complementary filter pitch and roll, which eliminates drift over time
	double p = phi_d - sin_phi*psi_d;
	double q = cos_phi*theta_d + cos_theta*sin_phi*psi_d;
	double r = -sin_phi*theta_d + cos_phi*cos_theta*psi_d;
	*/

	// Convert rotations to rotation rates
	double flow_x_rad_rate = sensor_struct->optical_flow.flow_x_rad / sensor_struct->optical_flow.dt;
	double flow_y_rad_rate = sensor_struct->optical_flow.flow_y_rad / sensor_struct->optical_flow.dt;

	// Add p to flow_x_rad_rate to add gyro compensation (Currently disabled)
	// Add q to flow_y_rad_rate
	double x_rad_rate_corr = flow_x_rad_rate;// + p;
	double y_rad_rate_corr = flow_y_rad_rate;// + q;
	

	// Only accumulate if the quality is good
	if (sensor_struct->optical_flow.quality > PX4FLOW_QUAL_MIN) {
		// Swap x and y to switch from rotation around an axis to movement along an axis
		// Y is negative because some reason?
		// We simply multiply by distance, because for small angles tan(theta) = theta.
		//     Also, the internal PX4Flow code works under the small angle assumption,
		//     so not doing trig here makes it more accurate than doing trig
		sensor_struct->optical_flow.xVel = -y_rad_rate_corr * distance;
		sensor_struct->optical_flow.yVel = x_rad_rate_corr * distance;
	}
	// Gradually decay towards 0 if quality is bad
	else {
		sensor_struct->optical_flow.xVel *= PX4FLOW_VEL_DECAY;
		sensor_struct->optical_flow.yVel *= PX4FLOW_VEL_DECAY;
	}

	// Un-comment if using gyroscope compensation
	/*
	// Store angles for next time
	last_phi = phi;
	last_theta = theta;
	*/
}


/*
 * Populates the xVel and yVel fields of flow_data,
 * using the flowX and flowY, and the given distance
 */
// void flow_to_vel(px4flow_t* flow_data, double distance) {
// 	double loop_time = get_last_loop_time();
// 	if (loop_time != 0) {
// 		if(flow_data->quality > PX4FLOW_QUAL_MIN) {
// 			flow_data->xVel = distance * flow_data->flowX / focal_length_px / loop_time;
// 			flow_data->yVel = distance * flow_data->flowY / focal_length_px / loop_time;
// 		}
// 		else {
// 			flow_data->xVel *= PX4FLOW_VEL_DECAY;
// 			flow_data->yVel *= PX4FLOW_VEL_DECAY;
// 		}
// 	}
// }

int sensor_processing(log_t* log_struct, user_input_t *user_input_struct, raw_sensor_t* raw_sensor_struct, sensor_t* sensor_struct)
{
	// Filter accelerometer values
	gam_t* gam = &(raw_sensor_struct->gam);
	float accel_x = biquad_execute(&sensor_struct->accel_x_filt, gam->accel_x);
	float accel_y = biquad_execute(&sensor_struct->accel_y_filt, gam->accel_y);
	float accel_z = biquad_execute(&sensor_struct->accel_z_filt, gam->accel_z);
	//Get X and Y angles
	// javey: this assigns accel_(pitch/roll) in units of radians
	float accel_pitch = atan(accel_x / sqrt(accel_y*accel_y + accel_z*accel_z));
	float accel_roll = -atan(accel_y / sqrt(accel_x*accel_x + accel_z*accel_z)); // negative because sensor board is upside down


	// copy currentQuadPosition and trimmedRCValues from raw_sensor_struct to sensor_struct
	deep_copy_Qpos(&(sensor_struct->currentQuadPosition), &(raw_sensor_struct->currentQuadPosition));

	// Calculate Euler angles and velocities using Gimbal Equations below
	/////////////////////////////////////////////////////////////////////////
	// | Phi_d   |   |  1  sin(Phi)tan(theta)    cos(Phi)tan(theta) |  | p |
	// | theta_d | = |  0  cos(Phi)              -sin(Phi)		    |  | q |
	// | Psi_d   |   |  0  sin(Phi)sec(theta)    cos(Phi)sec(theat) |  | r |
	//
	// Phi_dot = p + q sin(Phi) tan(theta) + r cos(Phi) tan(theta)
	// theta_dot = q cos(Phi) - r sin(Phi)
	// Psi_dot = q sin(Phi) sec(theta) + r cos(Phi) sec(theta)
	///////////////////////////////////////////////////////////////////////////

	// javey:
	//
	// The gimbal equations are defined in the book "Flight Simulation" by Rolfe and Staples.
	// Find on page 46, equation 3.6

	// these are calculated to be used in the gimbal equations below
	// the variable roll(pitch)_angle_filtered is phi(theta)
	double sin_phi = sin(sensor_struct->roll_angle_filtered);
	double cos_phi = cos(sensor_struct->roll_angle_filtered);
	double tan_theta = tan(sensor_struct->pitch_angle_filtered);
	double sec_theta = 1/cos(sensor_struct->pitch_angle_filtered);

//	Gryo "p" is the angular velocity rotation about the x-axis (defined as var gyro_xVel_p in gam struct)
//	Gyro "q" is the angular velocity rotation about the y-axis (defined as var gyro_xVel_q in gam struct)
//	Gyro "r" is the angular velocity rotation about the z-axis (defined as var gyro_xVel_r in gam struct)

	// phi is the conventional symbol used for roll angle, so phi_dot is the roll velocity
	sensor_struct->phi_dot = raw_sensor_struct->gam.gyro_xVel_p + (raw_sensor_struct->gam.gyro_yVel_q*sin_phi*tan_theta)
			+ (raw_sensor_struct->gam.gyro_zVel_r*cos_phi*tan_theta);

	// theta is the conventional symbol used for pitch angle, so theta_dot is the pitch velocity
	sensor_struct->theta_dot = (raw_sensor_struct->gam.gyro_yVel_q*cos_phi)
			- (raw_sensor_struct->gam.gyro_zVel_r*sin_phi);

	// psi is the conventional symbol used for yaw angle, so psi_dot is the yaw velocity
	sensor_struct->psi_dot = (raw_sensor_struct->gam.gyro_yVel_q*sin_phi*sec_theta)
			+ (raw_sensor_struct->gam.gyro_zVel_r*cos_phi*sec_theta);

	// Copy in raw gyroscope values
	sensor_struct->gyr_x = raw_sensor_struct->gam.gyro_xVel_p;
	sensor_struct->gyr_y = raw_sensor_struct->gam.gyro_yVel_q;
	sensor_struct->gyr_z = raw_sensor_struct->gam.gyro_zVel_r;

	double loop_dt = get_last_loop_time();
	// Complementary Filter Calculations
	sensor_struct->pitch_angle_filtered = ALPHA * (sensor_struct->pitch_angle_filtered + sensor_struct->theta_dot * loop_dt)
			+ (1. - ALPHA) * accel_pitch;

	sensor_struct->roll_angle_filtered = ALPHA * (sensor_struct->roll_angle_filtered + sensor_struct->phi_dot* loop_dt)
			+ (1. - ALPHA) * accel_roll;

	// Z-axis points upward, so negate distance
	//sensor_struct->lidar_altitude = -raw_sensor_struct->lidar->distance_m;


	//-------- Optical flow -----------//
	// Copy over optical flow data
	sensor_struct->optical_flow = raw_sensor_struct->optical_flow;

	flow_gyro_compensation(sensor_struct,
						   raw_sensor_struct->lidar.distance_m,
						   sensor_struct->roll_angle_filtered,
						   sensor_struct->pitch_angle_filtered,
						   sensor_struct->psi_dot);


	//Filter OF velocities
	sensor_struct->optical_flow.xVelFilt = biquad_execute(&sensor_struct->flow_x_filt, sensor_struct->optical_flow.xVel);
	sensor_struct->optical_flow.yVelFilt = biquad_execute(&sensor_struct->flow_y_filt, sensor_struct->optical_flow.yVel);



	/*
	 * Altitude double complementary filter
	 */
	static float alt_alpha = 0.98;
	static float filtered_vel = 0;
	static float filtered_alt = 0;
	static float last_lidar = 0;
	
	float this_lidar = -raw_sensor_struct->lidar.distance_m;
	if(this_lidar < (-MAX_VALID_LIDAR)) {
		this_lidar = filtered_alt;
	}

	// Acceleration in m/s without gravity
	float quad_z_accel = 9.8 * (accel_z + 1);
	filtered_vel = alt_alpha*(filtered_vel + quad_z_accel*get_last_loop_time()) +
	              (1 - alt_alpha)*(this_lidar - last_lidar);
	filtered_alt = alt_alpha*(filtered_alt + filtered_vel*get_last_loop_time()) +
	              (1 - alt_alpha)*(this_lidar);

	last_lidar = this_lidar;
	sensor_struct->lidar_altitude = filtered_alt;

	//Magnetometer filter
	float magX_filt = biquad_execute(&sensor_struct->mag_x_filt, gam->mag_x - MAG_OFFSET_X);
	float magY_filt = biquad_execute(&sensor_struct->mag_y_filt, gam->mag_y - MAG_OFFSET_Y);
	float mag_yaw = atan2(-magY_filt, -magX_filt);

	//Heading complementary filter
	sensor_struct->yaw_angle_filtered = YAW_ALPHA * (sensor_struct->yaw_angle_filtered +
				(sensor_struct->psi_dot)*get_last_loop_time()) + (1. - YAW_ALPHA) * mag_yaw;

	return 0;
}

void set_pitch_angle_filtered(sensor_t * sensor_struct, float accel_roll)
{
	sensor_struct->pitch_angle_filtered = accel_roll;
}
void set_roll_angle_filtered(sensor_t * sensor_struct, float accel_pitch)
{
	sensor_struct->roll_angle_filtered = accel_pitch;
}

void deep_copy_Qpos(quadPosition_t * dest, quadPosition_t * src)
{
	dest->packetId = src->packetId;
	dest->y_pos = src->y_pos;
	dest->x_pos = src->x_pos;
	dest->alt_pos = src->alt_pos;
	dest->roll = src->roll;
	dest->pitch = src->pitch;
	dest->yaw = src->yaw;

}

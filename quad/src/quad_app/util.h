#ifndef _UTIL_H
#define _UTIL_H

#include "type_def.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "PID.h"
#include "log_data.h"
#include "controllers.h"
#include "hw_iface.h"

void read_rec_all(struct RCReceiverDriver *rc_receiver, float *mixer);
int read_kill(float gear);
int read_flap(float flap);
int gear_is_engaged(float gear);
int flap_is_engaged(float flap);
void kill_motors(struct MotorDriver *motors);

int build_int(u8 *buff);
float build_float(u8 *buff);
int16_t build_short(u8* buff);

void pack_short(int16_t val, u8* buff);
void pack_float(float val, u8* buff);
u32 read_bit(u32 data, int position);

#endif //_UTIL_H

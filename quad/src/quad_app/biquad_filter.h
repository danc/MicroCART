#ifndef BIQUAD_FILTER_H
#define BIQUAD_FILTER_H

struct biquadState {
	float delayed[2];
    float a0, a1, a2, b1, b2;
};

float biquad_execute(struct biquadState* state, float new_input);


struct biquadState filter_make_state(float a0, float a1, float a2,
                              float b1, float b2);
#endif // BIQUAD_FILTER_H
/*
 * send_actuator_commands.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef SEND_ACTUATOR_COMMANDS_H_
#define SEND_ACTUATOR_COMMANDS_H_
 
#include <stdio.h>

#include "log_data.h"

/**
 * @brief 
 *      Sends commands to the actuators.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param actuator_command_struct
 *      structure of the commmands to go to the actuators
 *
 * @return 
 *      error message
 *
 */
int send_actuator_commands(struct MotorDriver *motors, log_t* log_struct, actuator_command_t* actuator_command_struct);

#endif /* SEND_ACTUATOR_COMMANDS_H_ */

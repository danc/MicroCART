/*
 * initialize_components.h
 *
 *  Created on: Nov 12, 2015
 *      Author: ucart
 */

#ifndef INITALIZE_COMPONENTS_H_
#define INITALIZE_COMPONENTS_H_

#include "timer.h"
#include "control_algorithm.h"
#include "util.h"
#include "type_def.h"
#include "controllers.h"
#include "mio7_led.h"

/**
 * @brief 
 *      Runs loops to make sure the quad is responding and in the correct state before starting.
 *
 * @return
 *      error message
 *
 */
int protection_loops(modular_structs_t *structs);

/**
 * @brief
 *      Initializes the sensors, communication, and anything else that needs
 * initialization.
 *
 * @return 
 *      error message
 *
 */
int init_structs(modular_structs_t *structs);

#endif /* INITALIZE_COMPONENTS_H_ */


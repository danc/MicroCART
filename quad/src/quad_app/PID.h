/*
 * PID.h
 *
 *  Created on: Nov 10, 2014
 *      Author: ucart
 */

#ifndef PID_H_
#define PID_H_

#include "type_def.h"

#define VEL_CLAMP 2.0
#define PITCH_TRIM 0.045

// Yaw constants
// when using units of radians
#define YAW_ANGULAR_VELOCITY_KP 0.297
#define YAW_ANGULAR_VELOCITY_KI 0.0f
#define YAW_ANGULAR_VELOCITY_KD 0.0f
#define YAW_ANGLE_KP 2.6f
#define YAW_ANGLE_KI 0.0f
#define YAW_ANGLE_KD 0.0f



// when using units of radians
#define ROLL_ANGULAR_VELOCITY_KP 0.03
#define ROLL_ANGULAR_VELOCITY_KI 0.0f
#define ROLL_ANGULAR_VELOCITY_KD 0.005
#define ROLL_ANGULAR_VELOCITY_ALPHA 0.88
#define ROLL_ANGLE_KP 35
#define ROLL_ANGLE_KI 0.0f
#define ROLL_ANGLE_KD 1.0
#define ROLL_ANGLE_ALPHA 0.88
#define YVEL_KP 0.1
#define YVEL_KD 0.02
#define YVEL_ALPHA 0.88
#define YPOS_KP 0.55
#define YPOS_KI 0.0075
#define YPOS_KD 0.0
#define YPOS_ALPHA 0


//Pitch constants
// when using units of radians
#define PITCH_ANGULAR_VELOCITY_KP 0.03
#define PITCH_ANGULAR_VELOCITY_KI 0.0f
#define PITCH_ANGULAR_VELOCITY_KD 0.005
#define PITCH_ANGULAR_VELOCITY_ALPHA 0.88
#define PITCH_ANGLE_KP 35
#define PITCH_ANGLE_KI 0.0f
#define PITCH_ANGLE_KD 1.0
#define PITCH_ANGLE_ALPHA 0.88
#define XVEL_KP -0.1
#define XVEL_KD -0.02
#define XVEL_ALPHA 0.88
#define XPOS_KP 0.55
#define XPOS_KI 0.0075
#define XPOS_KD 0.0
#define XPOS_ALPHA 0


//Throttle constants
#define ALT_ZPOS_KP -0.09804f
#define ALT_ZPOS_KI -0.00817f
#define ALT_ZPOS_KD -0.07353f
#define ALT_ZPOS_ALPHA 0.88

#endif /* PID_H_ */

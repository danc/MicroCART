/*
 * send_actuator_commands.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */
 
#include "do_output_override.h"
#include "util.h"

#define THROTTLE 0
#define ROLL     1
#define PITCH    2
#define YAW      3

static int motor_min = 0.00000;
static int motor_max = 1.00000;

static double motor_clamp(double val) {
        if (isnan(val)) { val = motor_min; }
	else if (val < motor_min) {val = motor_min;}
	else if (val > motor_max) {val = motor_max;}
	return val;
}

int do_output_override(log_t* log_struct, override_t* override_struct, actuator_command_t* actuator_command_struct) {
  // write the PWMs to the motors
  if (override_struct->mode == OVERRIDE_ALL) {
	  float *inputs = override_struct->values;
	  float motor0 = inputs[THROTTLE] - inputs[PITCH] - inputs[ROLL] - inputs[YAW];
	  float motor1 = inputs[THROTTLE] + inputs[PITCH] - inputs[ROLL] + inputs[YAW];
	  float motor2 = inputs[THROTTLE] - inputs[PITCH] + inputs[ROLL] + inputs[YAW];
	  float motor3 = inputs[THROTTLE] + inputs[PITCH] + inputs[ROLL] - inputs[YAW];
	  actuator_command_struct->motor_magnitudes[0] = motor_clamp(motor0);
	  actuator_command_struct->motor_magnitudes[1] = motor_clamp(motor1);
	  actuator_command_struct->motor_magnitudes[2] = motor_clamp(motor2);
	  actuator_command_struct->motor_magnitudes[3] = motor_clamp(motor3);
  }

  return 0;
}

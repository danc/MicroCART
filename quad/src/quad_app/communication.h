#ifndef _COMMUNICATION_H
#define _COMMUNICATION_H

#include "type_def.h"
#include "timer.h"
#include "commands.h"
#include "hw_iface.h"

#define MAX_PACKET_SIZE 256

int initUartComms();
void process_received(modular_structs_t *structs);
int send_data(struct UARTDriver *uart, u16 type_id, u16 msg_id, u8* data, size_t size);

#endif

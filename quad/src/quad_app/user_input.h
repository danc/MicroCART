/*
 * user_input.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef USER_INPUT_H_
#define USER_INPUT_H_
 
#include <stdio.h>
#include "type_def.h"
#include "log_data.h"
#include "util.h"

/*
 * Aero channel declaration
 */

#define THROTTLE 0
#define ROLL     1
#define PITCH    2
#define YAW      3
#define GEAR 	 4
#define FLAP 	 5

//////TARGETS

#define YAW_DEG_TARGET 60.0f
#define YAW_RAD_TARGET ((float) ((YAW_DEG_TARGET * 3.141592) / ((float) 180)))

#define ROLL_DEG_TARGET 10.0f
#define ROLL_RAD_TARGET ((float) ((ROLL_DEG_TARGET * 3.141592) / ((float) 180)))

#define PITCH_DEG_TARGET 12.0f
#define PITCH_RAD_TARGET ((float) ((PITCH_DEG_TARGET * 3.141592) / ((float) 180)))

/**
 * @brief 
 *      Receives user input to the system.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param user_input_struct
 *      structure of the data inputed by the user
 *
 * @return 
 *      error message
 *
 */
int get_user_input(hardware_t *hardware_struct, log_t* log_struct,  user_input_t* user_input_struct);
int kill_condition(user_input_t* user_input_struct);
float convert_from_receiver_cmd(float receiver_cmd, float max_receiver_cmd, float center_receiver_cmd, float min_receiver_cmd, float max_target, float min_target);


#endif /* USER_INPUT_H_ */

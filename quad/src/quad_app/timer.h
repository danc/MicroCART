/*
 * timer.h
 *
 *  Created on: Feb 24, 2016
 *      Author: ucart
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "log_data.h"

// desired loop time is not guaranteed (its possible that the loop may take longer than desired)
#define DESIRED_USEC_PER_LOOP 5000 // gives 5ms loops

/**
 * @brief
 *      Does processing of the loop timer at the beginning of the control loop.
 *
 * @return
 *      error message
 *
 */
int timer_start_loop();

/**
 * @brief
 *      Does processing of the loop timer at the end of the control loop.
 *
 * @return
 *      error message
 *
 */
int timer_end_loop(log_t *log_struct);

// Returns the number of seconds the last loop took
float get_last_loop_time();

u64 timer_get_count();

// Resets the axi timer
void timer_axi_reset();

void timer_init_globals(struct TimerDriver *global_timer, struct TimerDriver *axi_timer);
#endif /* TIMER_H_ */

#include "hw_iface.h"
#include "hw_impl_unix.h"

extern struct VirtQuadIO *virt_quad_io;

int unix_imu_reset(struct IMUDriver *self, gam_t *gam) {
  // Sensible defaults
  virt_quad_io->gam.gyro_xVel_p = 0;
  virt_quad_io->gam.gyro_yVel_q = 0;
  virt_quad_io->gam.gyro_zVel_r = 0;
  virt_quad_io->gam.accel_x = 0;
  virt_quad_io->gam.accel_y = 0;
  virt_quad_io->gam.accel_z = -1;
  virt_quad_io->gam.mag_x = 0;
  virt_quad_io->gam.mag_y = 0;
  virt_quad_io->gam.mag_z = 0;
  virt_quad_io->gam.accel_roll = 0;
  virt_quad_io->gam.accel_pitch = 0;
  return 0;
}

int unix_imu_read(struct IMUDriver *self, gam_t *gam) {
  *gam = virt_quad_io->gam;
  return 0;
}

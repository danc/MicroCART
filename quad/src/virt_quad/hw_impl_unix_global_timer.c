#include "hw_impl_unix.h"

int unix_global_timer_reset(struct TimerDriver *self) {
  if (self->state == NULL) {
    self->state = malloc(sizeof(struct timeval));
  }
  struct timezone tz;
  struct timeval *start = self->state;
  gettimeofday(start, &tz);
  return 0;
}

int unix_global_timer_restart(struct TimerDriver *self) {
  struct timeval *start = self->state;
  struct timezone tz;
  gettimeofday(start, &tz);
  return 0;
}

int unix_global_timer_read(struct TimerDriver *self, u64 *us) {
  struct timeval *start = self->state;
  struct timeval end;
  struct timezone tz;
  gettimeofday(&end, &tz);
  *us = end.tv_usec - start->tv_usec;
  return 0;
}

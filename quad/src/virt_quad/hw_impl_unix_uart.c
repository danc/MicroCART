#include "hw_impl_unix.h"
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

static char *fifo_full_name_rx;
static char *fifo_full_name_tx;
static int fifo_rx;

int unix_uart_reset(struct UARTDriver *self) {
  fifo_full_name_rx = VIRT_QUAD_FIFOS_DIR "/uart-rx";
  fifo_full_name_tx = VIRT_QUAD_FIFOS_DIR "/uart-tx";

  char fifoname[64];
  mkdir(VIRT_QUAD_FIFOS_DIR, 0777);

  unlink(fifo_full_name_rx);
  mkfifo(fifo_full_name_rx, 0666);
  fifo_rx = open(fifo_full_name_rx, O_RDONLY | O_NONBLOCK);

  unlink(fifo_full_name_tx);
  mkfifo(fifo_full_name_tx, 0666);

  return 0;
}

int unix_uart_write(struct UARTDriver *self, unsigned char c) {
  int fifo = open(fifo_full_name_tx, O_WRONLY | O_NONBLOCK);
  if (fifo >= 0) {
    printf("%s: %x\n", "uart-tx", c);
    write(fifo, &c, 1);
    close(fifo);
  }
  return 0;
}

int unix_uart_read(struct UARTDriver *self, unsigned char *c) {
  int err = read(fifo_rx, c, 1);
  if (err > 0) {
    printf("%s: %x\n", "uart-rx", *c);
  }
  return err <= 0;
}

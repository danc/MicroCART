#include "hw_impl_unix.h"

struct UARTDriver create_unix_uart() {
  struct UARTDriver uart;
  uart.state = NULL;
  uart.reset = unix_uart_reset;
  uart.write = unix_uart_write;
  uart.read = unix_uart_read;
  return uart;
}

struct CommDriver create_unix_comm(struct UARTDriver *uart) {
  struct CommDriver comm;
  comm.uart = uart;
  return comm;
}

struct MotorDriver create_unix_motors() {
  struct MotorDriver motors;
  motors.state = NULL;
  motors.reset = unix_motor_reset;
  motors.write = unix_motor_write;
  return motors;
}

struct RCReceiverDriver create_unix_rc_receiver() {
  struct RCReceiverDriver rc_receivers;
  rc_receivers.state = NULL;
  rc_receivers.reset = unix_rc_receiver_reset;
  rc_receivers.read = unix_rc_receiver_read;
  return rc_receivers;
}

struct I2CDriver create_unix_i2c() {
  struct I2CDriver i2c;
  i2c.state = NULL;
  i2c.reset = unix_i2c_reset;
  i2c.write = unix_i2c_write;
  i2c.read = unix_i2c_read;
  return i2c;
}

struct TimerDriver create_unix_global_timer() {
  struct TimerDriver global_timer;
  global_timer.state = NULL;
  global_timer.reset = unix_global_timer_reset;
  global_timer.restart = unix_global_timer_restart;
  global_timer.read = unix_global_timer_read;
  return global_timer;
}

struct TimerDriver create_unix_axi_timer() {
  struct TimerDriver axi_timer;
  axi_timer.state = NULL;
  axi_timer.reset = unix_axi_timer_reset;
  axi_timer.restart = unix_axi_timer_restart;
  axi_timer.read = unix_axi_timer_read;
  return axi_timer;
}

struct LEDDriver create_unix_mio7_led() {
  struct LEDDriver mio7_led;
  mio7_led.state = NULL;
  mio7_led.reset = unix_mio7_led_reset;
  mio7_led.turn_on = unix_mio7_led_turn_on;
  mio7_led.turn_off = unix_mio7_led_turn_off;
  return mio7_led;
}

struct SystemDriver create_unix_system() {
  struct SystemDriver sys;
  sys.state = NULL;
  sys.reset = unix_system_reset;
  sys.sleep = unix_system_sleep;
  return sys;
}

struct IMUDriver create_unix_imu(struct I2CDriver *i2c) {
  struct IMUDriver imu;
  imu.i2c = i2c;
  imu.reset = unix_imu_reset;
  imu.read = unix_imu_read;
  return imu;
}

struct LidarDriver create_unix_lidar(struct I2CDriver *i2c) {
  struct LidarDriver lidar;
  lidar.i2c = i2c;
  lidar.reset = unix_lidar_reset;
  lidar.read = unix_lidar_read;
  return lidar;
}

struct OpticalFlowDriver create_unix_optical_flow(struct I2CDriver *i2c) {
  struct OpticalFlowDriver of;
  of.i2c = i2c;
  of.reset = unix_optical_flow_reset;
  of.read = unix_optical_flow_read;
  return of;
}

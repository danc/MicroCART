#include "hw_impl_unix.h"

int unix_system_reset(struct SystemDriver *sys) {
  return 0;
}

int unix_system_sleep(struct SystemDriver *sys, unsigned long us) {
  struct timespec time;
  struct timespec time2;
  time.tv_sec = 0;
  time.tv_nsec = us * 1000;
  nanosleep(&time, &time2);
  return 0;
}

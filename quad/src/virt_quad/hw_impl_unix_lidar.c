#include "hw_iface.h"
#include "hw_impl_unix.h"

extern struct VirtQuadIO *virt_quad_io;

int unix_lidar_reset(struct LidarDriver *self, lidar_t *lidar) {
  virt_quad_io->lidar.distance_m = 0;
  return 0;
}

int unix_lidar_read(struct LidarDriver *self, lidar_t *lidar) {
  *lidar = virt_quad_io->lidar;
  return 0;
}

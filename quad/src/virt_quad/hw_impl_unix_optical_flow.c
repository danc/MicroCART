#include "hw_iface.h"
#include "hw_impl_unix.h"

extern struct VirtQuadIO *virt_quad_io;

int unix_optical_flow_reset(struct OpticalFlowDriver *self, px4flow_t *of) {
  virt_quad_io->of.xVel = 0;
  virt_quad_io->of.yVel = 0;
  return 0;
}

int unix_optical_flow_read(struct OpticalFlowDriver *self, px4flow_t *of) {
  *of = virt_quad_io->of;
  return 0;
}

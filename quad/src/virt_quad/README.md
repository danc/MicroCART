# The Virtual Quadcopter
Look at how modular our quad is ... it can even run in a Unix environment!

But really, this isn't just some token project, this is pretty useful for
debugging routine things in the quad_app without having to fire up the
plethoria of things required for flight in Coover 3050. In fact, you don't 
even have to be in Coover 3050...

## Building
To build just the virtual quad, in this folder:
```
make
```

## Using the Virtual Quad
The virt-quad has help output. Get started with:
```
./virt-quad
```

There are some good example of using this CLI in the 
[functional tests](https://git.ece.iastate.edu/danc/MicroCART/blob/master/quad/scripts/tests/test_safety_checks.rb#L31).

### Using the UART Driver

The UART interface is implemented with unix FIFOs. You can treat these FIFOs
as regular unix files. Read from uart-tx to hear with the quad is saying. Write
to uart-rx to tell the quad something.

```
echo "hello world" > virt-quad-fifos/uart-rx
cat virt-quad-fifos/uart-tx
```

#include "hw_impl_unix.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

extern struct VirtQuadIO *virt_quad_io;

int unix_motor_reset(struct MotorDriver *self) {
  return 0;
}

int unix_motor_write(struct MotorDriver *self,
                          unsigned int channel,
                          float magnitude) {
  struct VirtQuadIO *io = virt_quad_io;
  pthread_mutex_lock(&io->motors_lock);
  io->motors[channel] = magnitude;
  pthread_mutex_unlock(&io->motors_lock);
  return 0;
}

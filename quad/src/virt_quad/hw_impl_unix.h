#ifndef HW_IMPL_UNIX
#define HW_IMPL_UNIX

#include "hw_iface.h"
#include "type_def.h"

#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "controllers.h"
#include <pthread.h>
#include <errno.h>

#define VIRT_QUAD_FIFOS_DIR "virt-quad-fifos"

struct VirtQuadIO {
  pthread_mutex_t led_lock;
  int led;
  pthread_mutex_t motors_lock;
  float motors[4];
  pthread_mutex_t rc_lock;
  float rc_receiver[6];
  pthread_mutex_t i2c_lock;
  gam_t gam;
  lidar_t lidar;
  px4flow_t of;
};

#define VIRT_QUAD_SHARED_MEMORY "/virt-quad-io"

int unix_uart_reset(struct UARTDriver *self);
int unix_uart_write(struct UARTDriver *self, unsigned char c);
int unix_uart_read(struct UARTDriver *self, unsigned char *c);

int unix_motor_reset(struct MotorDriver *self);
int unix_motor_write(struct MotorDriver *self, unsigned int channel, float magnitude);

int unix_rc_receiver_reset(struct RCReceiverDriver *self);
int unix_rc_receiver_read(struct RCReceiverDriver *self, unsigned int channel, float *magnitude);

int unix_i2c_reset(struct I2CDriver *self);
int unix_i2c_write(struct I2CDriver *self,
                   unsigned short device_addr,
                   unsigned char *data,
                   unsigned int length);
int unix_i2c_read(struct I2CDriver *self,
                  unsigned short device_addr,
                  unsigned char *buff,
                  unsigned int length);

int unix_global_timer_reset(struct TimerDriver *self);
int unix_global_timer_restart(struct TimerDriver *self);
int unix_global_timer_read(struct TimerDriver *self, u64 *us);

int unix_axi_timer_reset(struct TimerDriver *self);
int unix_axi_timer_restart(struct TimerDriver *self);
int unix_axi_timer_read(struct TimerDriver *self, u64 *us);

int unix_mio7_led_reset(struct LEDDriver *self);
int unix_mio7_led_turn_on(struct LEDDriver *self);
int unix_mio7_led_turn_off(struct LEDDriver *self);

int unix_system_reset(struct SystemDriver *self);
int unix_system_sleep(struct SystemDriver *self, unsigned long us);

int unix_imu_reset(struct IMUDriver *self, struct gam *gam);
int unix_imu_read(struct IMUDriver *self, struct gam *gam);

int unix_lidar_reset(struct LidarDriver *self, struct lidar *lidar);
int unix_lidar_read(struct LidarDriver *self, struct lidar *lidar);

int unix_optical_flow_reset(struct OpticalFlowDriver *self, struct px4flow *of);
int unix_optical_flow_read(struct OpticalFlowDriver *self, struct px4flow *of);

struct UARTDriver create_unix_uart();
struct CommDriver create_unix_comm(struct UARTDriver *uart);
struct MotorDriver create_unix_motors();
struct RCReceiverDriver create_unix_rc_receiver();
struct I2CDriver create_unix_i2c();
struct TimerDriver create_unix_global_timer();
struct TimerDriver create_unix_axi_timer();
struct LEDDriver create_unix_mio7_led();
struct SystemDriver create_unix_system();
struct IMUDriver create_unix_imu(struct I2CDriver *i2c);
struct LidarDriver create_unix_lidar(struct I2CDriver *i2c);
struct OpticalFlowDriver create_unix_optical_flow(struct I2CDriver *i2c);

int test_unix_i2c();
int test_unix_mio7_led_and_system();
int test_unix_rc_receivers();

#endif

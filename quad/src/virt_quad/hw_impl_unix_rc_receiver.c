#include "hw_impl_unix.h"
#include "controllers.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

extern struct VirtQuadIO *virt_quad_io;

int unix_rc_receiver_reset(struct RCReceiverDriver *self) {
  // sane defaults for the RC receiver
  virt_quad_io->rc_receiver[0] = 0;
  virt_quad_io->rc_receiver[1] = 0.5;
  virt_quad_io->rc_receiver[2] = 0.5;
  virt_quad_io->rc_receiver[3] = 0.5;
  virt_quad_io->rc_receiver[4] = 0;
  virt_quad_io->rc_receiver[5] = 0;
  return 0;
}

int unix_rc_receiver_read(struct RCReceiverDriver *self,
                        unsigned int channel,
                        float *magnitude) {
  struct VirtQuadIO *io = virt_quad_io;
  pthread_mutex_lock(&io->rc_lock);
  *magnitude = io->rc_receiver[channel];
  pthread_mutex_unlock(&io->rc_lock);
  return 0;
}

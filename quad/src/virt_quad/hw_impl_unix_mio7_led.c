#include "hw_impl_unix.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <pthread.h>

extern struct VirtQuadIO *virt_quad_io;
static int on;

int unix_mio7_led_reset(struct LEDDriver *self) {
  return 0;
}

int unix_mio7_led_turn_on(struct LEDDriver *self) {
  if (!on) {
    puts("LED ON");
    on = 1;

    struct VirtQuadIO *io = virt_quad_io;
    pthread_mutex_lock(&io->led_lock);
    io->led = on;
    pthread_mutex_unlock(&io->led_lock);
  }
  return 0;
}

int unix_mio7_led_turn_off(struct LEDDriver *self) {
  if (on) {
    puts("LED OFF");
    on = 0;

    struct VirtQuadIO *io = virt_quad_io;
    pthread_mutex_lock(&io->led_lock);
    io->led = on;
    pthread_mutex_unlock(&io->led_lock);
}
  return 0;
}

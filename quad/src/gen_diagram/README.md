The gen_diagram project can create an image of the controller network.

To generate an image, you need the graphviz application installed. You can get this from your package manager (Tested so far on apt and yum). After installing, you should be have access to the `dot` command from your terminal.

To make a new diagram, from the top-level quad directory (`MicroCART/quad`), run `make diagram`.
The new network.png file show up in this folder.
#include <stdio.h>
#include "computation_graph.h"
#include "control_algorithm.h"

int main() {
    parameter_t ps;
    control_algorithm_init(&ps);
    FILE* dot_fp;
    dot_fp = fopen("network.dot", "w");
    export_dot(ps.graph, dot_fp, 0);
    fclose(dot_fp);
}

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "computation_graph.h"

#define GRAPH_MAX_DEPTH 20
#define GRAPH_MAX_INPUTS 20

// Array to store input values for passing to the execute function of each node
static double exec_input_vals[GRAPH_MAX_INPUTS];

// Macro functions for setting and clearing single bits in int array
// From http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/1-C-intro/bit-array.html
#define setBit(A,k)     ( A[(k / (8*sizeof(*A)))] |=  (1 << (k % (8*sizeof(*A)))) )
#define clearBit(A,k)   ( A[(k / (8*sizeof(*A)))] &= ~(1 << (k % (8*sizeof(*A)))) )
#define testBit(A,k)    ( A[(k / (8*sizeof(*A)))] &   (1 << (k % (8*sizeof(*A)))) )

struct computation_graph *create_graph() {
    struct computation_graph *the_graph = malloc(sizeof(struct computation_graph));
    if (!the_graph) {return NULL;}
    // Allocate space for a single node in the graph
    the_graph->nodes = malloc(sizeof(struct graph_node));
    the_graph->node_existence = malloc(sizeof(int));
    if (!the_graph->nodes || !the_graph->node_existence) { return NULL; }
    the_graph->n_nodes = 0;
    the_graph->size = 1;
    return the_graph;
}

struct computation_graph free_graph(struct computation_graph *graph) {
    if (graph == NULL) return; // Do nothing if memory has already been cleared

    for (int i = 0; i < graph->size; i++) {
        if (graph_node_exists(graph, i)) {
            struct graph_node *node = &graph->nodes[i];

            // THESE ARE SEPERATED TO TEST FOR NOW -> There's gotta be a more concise way to do this.
            if (node->name != NULL) free(node->name);
            if (node->state != NULL) free(node->state);
            if (node->output_values != NULL) free(node->output_values);
            if (node->param_values != NULL) free(node->param_values);
            if (node->input_srcs != NULL) free(node->input_srcs);


        }
    }
    // These need to be tested as well... should just be able to be grouped together... right?
    if (graph->nodes != NULL) free(graph->nodes);
    if (graph->node_existence != NULL) free(graph->node_existence);

    // Finally...
    free(graph);
}

static void reset_node_rec(struct computation_graph* graph, int node_id, int depth) {
    if (depth > GRAPH_MAX_DEPTH) {
        return;
    }
    struct graph_node* node = &graph->nodes[node_id];
    // Don't reset nodes that have already been reset or discovered
    if (node->processed_state != UNPROCESSED) {
        return;
    }
    node->processed_state = DISCOVERED;
    int input_id;
    for (input_id = 0; input_id < node->type->n_inputs; input_id++) {
        int src_node_id = node->input_srcs[input_id].controller_id;
        if (src_node_id != -1) {
            reset_node_rec(graph, src_node_id, depth + 1);
        }
    }
    // Reset this node
    if (node->type->reset != NULL) {
        node->type->reset(node->state);
    }
    node->updated = 1;
    node->processed_state = PROCESSED;
}

int reset_node(struct computation_graph* graph, int node_id) {
    if (!graph_node_exists(graph, node_id)) {
        return -1;
    }
    int i;
    for (i = 0; i < graph->size; i++) {
        if (graph_node_exists(graph, i)) {
            graph->nodes[i].processed_state = UNPROCESSED;
        }
    }
    reset_node_rec(graph, node_id, 0);
    return 0;
}

int graph_set_source(struct computation_graph *graph,
                     int dest_node_id, int dest_input, int src_node_id, int src_output) {
    if (!graph_node_exists(graph, dest_node_id) || !graph_node_exists(graph, src_node_id)) {
        return -1;
    }
    struct graph_node *dest_node = &graph->nodes[dest_node_id];
    struct graph_node *src_node = &graph->nodes[src_node_id];
    if (dest_input >= dest_node->type->n_inputs || src_output >= src_node->type->n_outputs ||
        dest_input < 0 || src_output < 0) {
        return -1;
    }

    // If a previous source exists, remove one from its children count
    int prev_src_id = dest_node->input_srcs[dest_input].controller_id;
    if (prev_src_id != -1) {
        graph->nodes[prev_src_id].n_children -= 1;
    }
    src_node->n_children += 1; // Count destination node as a child
    dest_node->input_srcs[dest_input].controller_id = src_node_id;
    dest_node->input_srcs[dest_input].controller_output = src_output;
    dest_node->updated = 1;
    reset_node(graph, src_node_id);
    return 0;
}

struct node_src graph_get_source(struct computation_graph *graph, int node_id, int input_id) {
    if (!graph_node_exists(graph, node_id) ||
        input_id >= graph->nodes[node_id].type->n_inputs || input_id < 0) {
        return (struct node_src) {.controller_id = -1, .controller_output = -1};
    }
    return graph->nodes[node_id].input_srcs[input_id];
}

int graph_add_node(struct computation_graph *graph,
                   const char* name,
                   const struct graph_node_type *type) {
    assert(type->n_inputs <= GRAPH_MAX_INPUTS);
    int new_id = graph->n_nodes;
    return graph_add_node_id(graph, new_id, name, type);
}

int graph_add_node_id(struct computation_graph *graph,
                   int id,
                   const char *name,
                   const struct graph_node_type *type) {
    if (id >= graph->size) {
        size_t old_size = graph->size;
        size_t new_size = old_size == 0 ? 8 : id * 2; // Hold twice the given ID
        struct graph_node *node_arr = realloc(graph->nodes, sizeof(struct graph_node) * new_size);
        if (!node_arr) { return -1; }
        // Number of integers needed to hold new_size bits
        size_t new_exist_size = ceil((float)new_size / (8 * sizeof(int))); // ceil(new_size / (bits per int))
        // Set the newly allocated memory to 0
        size_t old_exist_size = ceil((float)old_size / (8 * sizeof(int)));
        if (old_exist_size != new_exist_size) {
            int* exist_arr = realloc(graph->node_existence, sizeof(int) * new_exist_size);
            if (!exist_arr) {return -1;}
            memset(exist_arr + old_exist_size, 0, (new_exist_size - old_exist_size) * sizeof(int));
            graph->node_existence = exist_arr;
        }
        graph->size = new_size;
        graph->nodes = node_arr;
    }
    struct graph_node *new_node = &graph->nodes[id];
    new_node->name = strdup(name);
    new_node->type = type;
    new_node->state = malloc(type->state_size);
    new_node->n_children = 0;
    new_node->updated = 1;
    new_node->output_values = calloc(type->n_outputs, sizeof(double));
    new_node->param_values = calloc(type->n_params, sizeof(double));
    new_node->input_srcs = malloc(type->n_inputs * sizeof(struct node_src));
    // Check that malloc succeeded in every case which memory was requested
    if ((type->n_outputs && !new_node->output_values) ||
        (type->n_params && !new_node->param_values) ||
        (type->n_inputs && !new_node->input_srcs) ||
        (type->state_size && !new_node->state)) {
        return -1;
    }
    int i;
    for (i = 0; i < type->n_inputs; i++) {
        new_node->input_srcs[i].controller_id = -1;
    }
    graph->n_nodes += 1;
    setBit(graph->node_existence, id);
    // Reset block upon creation
    if (new_node->type->reset != NULL) {
        new_node->type->reset(new_node->state);
    }
    return id;
}

int graph_set_param_val(struct computation_graph *graph, int node_id, int param_id, double value) {
    if (!graph_node_exists(graph, node_id) || param_id >= graph->nodes[node_id].type->n_params) {
        return -1;
    }
    graph->nodes[node_id].param_values[param_id] = value;
    graph->nodes[node_id].updated = 1;
    return 0;
}

double graph_get_param_val(const struct computation_graph *graph, int node_id, int param_id) {
    if (!graph_node_exists(graph, node_id) || param_id >= graph->nodes[node_id].type->n_params) {
        return NAN;
    }
	return graph->nodes[node_id].param_values[param_id];
}

double graph_get_output(const struct computation_graph *graph, int node_id, int output_id) {
    if (!graph_node_exists(graph, node_id) || output_id >= graph->nodes[node_id].type->n_outputs) {
        return NAN;
    }
    return graph->nodes[node_id].output_values[output_id];
}

/*
 * Assumptions: The node passed in is a valid ID (should be checked before passing)
 * and all node sources are either valid node-output pairs, or the source node ID == -1
 * These constraints should be satisfied by using the graph_set_source function, so long as
 * a valid node ID is passed in to the first call of this function
*/
void graph_compute_node_rec(struct computation_graph *graph, int node_id, int depth) {
    if (depth >= GRAPH_MAX_DEPTH) {
        assert(1 == 0);
        return;
    }
    // if (!graph_node_exists(graph, node_id)) {
    //     return;
    // }
    struct graph_node *node = &graph->nodes[node_id];
    if (node->processed_state != UNPROCESSED) {
        return;
    }
    node->processed_state = DISCOVERED;
    int input_id;
    for (input_id = 0; input_id < node->type->n_inputs; input_id++) {
        int src_cntl_id = node->input_srcs[input_id].controller_id;
        if (src_cntl_id != -1) {
            graph_compute_node_rec(graph, src_cntl_id, depth + 1);
            node->updated |= graph->nodes[src_cntl_id].updated;
        }
    }
    if (node->updated) {
        // Populate the exec_input_vals array for computation
        for (input_id = 0; input_id < node->type->n_inputs; input_id++) {
            int src_cntl_id = node->input_srcs[input_id].controller_id;
            int src_output_id = node->input_srcs[input_id].controller_output;
            if (src_cntl_id != -1) {
                exec_input_vals[input_id] = graph->nodes[src_cntl_id].output_values[src_output_id];
            }
            else {
                // Set input value to 0 if not connected
                exec_input_vals[input_id] = 0;
            }
        }
        if (node->type->execute != NULL) {
            (*node->type->execute)(node->state, node->param_values, exec_input_vals, node->output_values);
        }
    }
    node->processed_state = PROCESSED;
}

void graph_compute_nodes(struct computation_graph *graph, int* node_ids, int n_nodes) {
    int i;
    for (i = 0; i < graph->size; i++) {
        // Note: Do not access malloc'd members in here without first checking if node is valid
        graph->nodes[i].processed_state = UNPROCESSED;
    }
    for (i = 0; i < n_nodes; i++) {
    	int node_id = node_ids[i];
    	if (graph_node_exists(graph, node_id)) {
    	    graph_compute_node_rec(graph, node_id, 0);
    	}
    }
    // Clear all the updated flags for nodes that were actually executed
    for (i = 0; i < graph->size; i++) {
        // Note: Do not access malloc'd members in here without first checking if node is valid
        struct graph_node* node = &graph->nodes[i];
        if (node->processed_state == PROCESSED) {
            node->updated = 0;
        }
    }
}

int export_dot(const struct computation_graph* graph, FILE* of, int print_outputs) {
    fprintf(of, "digraph G {\n"); // Header
    fprintf(of, "rankdir=\"LR\"\n"); // Horizontal layout

    // Draw all the nodes and their inputs
    int i;
    for (i = 0; i < graph->size; i++) {
        if (!graph_node_exists(graph, i)) {continue;}
        struct graph_node *node = &graph->nodes[i];
        // Create node
        fprintf(of, "\"%s\"[shape=record\nlabel=\"", node->name);
        fprintf(of, "<f0>%s ", node->name); // Node name is port 0
        int j;
        // Create ports for inputs
        for (j = 0; j < node->type->n_inputs; j++) {
            fprintf(of, " |<f%d> --\\>%s", j+1, node->type->input_names[j]);
        }
        // Create ports for parameters
        for (j = 0; j < node->type->n_params; j++) {
            fprintf(of, " |<f%d> [%s=%.3f]", j+1+node->type->n_inputs, node->type->param_names[j],node->param_values[j]);
        }
        fprintf(of, "\"]\n"); // Close label bracket
        // Make connections from
        for (j = 0; j < node->type->n_inputs; j++) {
        	int input_id = node->input_srcs[j].controller_id;
        	if (input_id != -1) {
                struct graph_node* src_node = &graph->nodes[input_id];
                int output_id = node->input_srcs[j].controller_output;
                const char* output_name = src_node->type->output_names[output_id];
                fprintf(of, "\"%s\" -> \"%s\":f%d [label=\"%s", src_node->name, node->name, j+1, output_name);
                if (print_outputs) {
                    fprintf(of, "=%.3f", src_node->output_values[output_id]);
                }
                fprintf(of, "\"]\n");
        	}
        }
    }
    fprintf(of, "}"); // Close graph
    return 0;
}

int graph_node_exists(const struct computation_graph *graph, int node_id) {
    if (node_id < 0 || node_id >= graph->size || !testBit(graph->node_existence, node_id)) {
        return 0;
    }
    else {return 1;}
}

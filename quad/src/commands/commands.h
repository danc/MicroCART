#ifndef __COMMANDS_H
#define __COMMANDS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define MAX_CMD_TEXT_LENGTH 100

enum Message{
	BEGIN_CHAR = 0xBE,
	END_CHAR   = 0xED
};

/*
 * Enumeration of the data types that a callback function may use
 * doubleType should get added here at some point
 */
enum DataType
{
	floatType,            // 00
	intType,              // 01
	stringType            // 02
};

/*
 * Message type IDs used to know what kind of message we are dealing with
 * Enumeration used to index the MessageTypes array in commands.c
 *
 * DO NOT change items in the middle of this enum or you will break backwards compatibility.
 *     - Add new message types in the slot between MAX_TYPE_ID and the one before it
 * DO NOT change this enum without also updating the "MessageTypes" array
 * in commands.c to match.
 */
enum MessageTypeID{
	DEBUG_ID,             // 00
	PACKETLOG_ID,         // 01
	GETPACKETLOGS_ID,     // 02
	UPDATE_ID,            // 03
	BEGINUPDATE_ID,       // 04
	LOG_ID,               // 05
	LOG_END_ID,           // 06 - Responding with controller parameters. Example: PID constants
	SETPARAM_ID,          // 07 - Setting node parameters.
	GETPARAM_ID,          // 08 - Getting node parameters.
	RESPPARAM_ID,         // 09 - Responding with node parameters.
	SETSOURCE_ID,         // 10 - Setting node input source block_id & output_id
	GETSOURCE_ID,         // 11 - Getting node input source block_id & output_id
	RESPSOURCE_ID,        // 12 - Responding with node input source block_id & output_id
	GETOUTPUT_ID,         // 13 - Getting node output
	RESPOUTPUT_ID,        // 14 - Responding with node output
	GETNODES_ID,          // 15 - Getting node IDs from current comp_graph
	RESPNODES_ID,         // 16 - Responding with node IDs from current comp_graph
	ADDNODE_ID,			  // 17 - Add a node of specified type_id
	RESPADDNODE_ID,		  // 18 - Responding with the block_id of the newly added node
	OUTPUT_OVERRIDE_ID,   // 19 - Override the outputs from the controller and use provided values instead
	SEND_RT_ID,	    	  // 20 - Real Time sensor data sent to groundstation
	MAX_TYPE_ID      	  // 21 - Just used to keep track of the size. Must remain at the end
};

struct modular_structs;

struct metadata {
	char begin_char;
	uint16_t msg_type;
	uint16_t msg_id;
	uint16_t data_len;
};

typedef int (command_cb)(struct modular_structs *, struct metadata *, unsigned char *, unsigned short);

/*
 * Message type struct used to keep track of the callback function
 * pointers located in commands.c
 */
struct MessageType{
	char cmdText[MAX_CMD_TEXT_LENGTH];
	char cmdDataType;
	command_cb * functionPtr;
};

/* Defined in commands.c */
extern struct MessageType MessageTypes[MAX_TYPE_ID+3];
int findCommand(char * cmdStr);

#endif /* __COMMANDS_H */

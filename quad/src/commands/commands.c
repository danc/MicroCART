#include "commands.h"

/* This file defines the commands structure.
 * This is the canonical reference for all commands
 * used. This file can be used - unchanged - on both the quad side
 * and on the ground station side. The commands.h file is also entirely
 * portable and may be used unchanged.
 *
 * This file (commands.c) and the matching header (commands.h)
 * are fully portable (quad + groundStation).
 * 
 * To use this file, three non-portable files must also exist:
 *  - callbacks.h. Typedef for command_cb
 *  - cb_default.h. Implementation of cb_default.
 *  - callbacks.c file. Contains implemented callbacks.
 *
 * There are two mandatory things that must be implemented in
 * other files for this to work: First, in callbacks.h, create a typedef
 * for command_cb. See the callbacks.h of the ground station for an
 * example.
 *
 * Second, in cb_default.h, implement the function
 * cb_default. This function should do nothing; it will be the 
 * default action for an unimplemented callback. Note that because
 * the function is implemented in the .h file, cb_default.h MUST NOT
 * be included in any other file!
 *
 * To implement callbacks, simply define them in callbacks.c.
 *
 *
 * EXTENDING COMMANDS.C
 *
 * To extend this file, simply add the new type (typically
 * a Setter, Getter, and Response) and create weak aliases below.
 *
 * Ensure the Quad and GroundStation always maintain this file in sync!
 *
 */

/*
 * List of callbacks. DO NOT MODIFY THESE IN THIS FILE -
 * Simply implement a function with the same name
 * in a different file (callbacks.c) and these will
 * be overridden.
 */

/* Grab the default callback implementation */
#include "cb_default.h"

/* Misc. callbacks */
command_cb cb_debug __attribute__((weak, alias("cb_default")));
command_cb cb_packetlog __attribute__((weak, alias("cb_default")));
command_cb cb_getpacketlogs __attribute__((weak, alias("cb_default")));
command_cb cb_update __attribute__((weak, alias("cb_default")));
command_cb cb_beginupdate __attribute__((weak, alias("cb_default")));
command_cb cb_log __attribute__((weak, alias("cb_default")));
command_cb cb_response __attribute__((weak, alias("cb_default")));
command_cb cb_logend __attribute__((weak, alias("cb_default")));

/* Callbacks for configuration */
command_cb cb_setparam __attribute__((weak, alias("cb_default")));
command_cb cb_getparam __attribute__((weak, alias("cb_default")));
command_cb cb_respparam __attribute__((weak, alias("cb_default")));

command_cb cb_setsource __attribute__((weak, alias("cb_default")));
command_cb cb_getsource __attribute__((weak, alias("cb_default")));
command_cb cb_respsource __attribute__((weak, alias("cb_default")));

command_cb cb_getoutput __attribute__((weak, alias("cb_default")));
command_cb cb_respoutput __attribute__((weak, alias("cb_default")));

command_cb cb_getnodes __attribute__((weak, alias("cb_default")));
command_cb cb_respnodes __attribute__((weak, alias("cb_default")));

command_cb cb_addnode __attribute__((weak, alias("cb_default")));
command_cb cb_respaddnode __attribute__((weak, alias("cb_default")));
command_cb cb_overrideoutput __attribute__((weak, alias("cb_default")));

command_cb cb_sendrtdata __attribute__((weak, alias("cb_default")));
command_cb cb_getlogfile __attribute__((weak, alias("cb_default")));
command_cb cb_resplogfile __attribute__((weak, alias("cb_default")));
command_cb cb_logblockcommand __attribute__((weak, alias("cb_default")));

/*
 * Command structure.
 * This array is used to keep track of the callback functions
 * for commands between the quad and the ground station.
 *
 * There is one callback function pointer associated with each
 * element in this struct array.
 *
 * DO NOT change this struct without updating the
 * "MessageTypeID" struct in commands.h as well
 */
struct MessageType MessageTypes[MAX_TYPE_ID+3] =
{
	// DEBUG
	{
		// Command text
		"debug",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_debug
	},
	// PACKETLOG
	{
		// Command text
		"packetlog",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_packetlog
	},
	// GETPACKETLOGS
	{
		// Command text
		"getpacketlogs",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_getpacketlogs
	},
	// UPDATE
	{
		// Command text
		"update",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_update
	},
	// BEGINUPDATE
	{
		// Command text
		"beginupdate",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_beginupdate
	},
	// LOG
	{
		// Command text
		"log",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_log
	},
	// LOG_END
	{
		// Command text
		"logend",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_logend
	},
	// SETPARAM
	{
		// Command text
		"setparam",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_setparam
	},
	// GETPARAM
	{
		// Command text
		"getparam",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_getparam
	},
	// RESPPARAM
	{
		// Command text
		"respparam",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_respparam
	},
	// SETSOURCE
	{
		// Command text
		"setsource",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_setsource
	},
	// GETSOURCE
	{
		// Command text
		"getsource",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_getsource
	},
	// RESPSOURCE
	{
		// Command text
		"respsource",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_respsource
	},
	// GETOUTPUT
	{
		// Command text
		"getoutput",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_getoutput
	},
	// RESPOUTPUT
	{
		// Command text
		"respoutput",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_respoutput
	},
	// GETNODES
	{
		// Command text
		"getnodes",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_getnodes
	},
	// RESPNODES
	{
		// Command text
		"respnodes",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_respnodes
	},
	// ADDNODE
	{
		// Command text
		"addnode",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_addnode
	},
	// RESPADDNODE
	{
		// Command text
		"respaddnode",
		// Type of the command data
		floatType,
		// Function pointer
		&cb_respaddnode
	},
	// OVERRIDE_OUTPUT
	{
		// Command text
		"outputoverride",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_overrideoutput
	},
	// SENDRTDATA
	{
		// Command text
		"sendrtdata",
		// Type of the command data
		stringType,
		// Function pointer
		&cb_sendrtdata
	},
	{ "getlogfile", 
		stringType, 
		&cb_getlogfile
	},
	{ "respgetlogfile", 
		stringType,
		&cb_resplogfile
	},
	{ "logblockcommand", 
		intType, 
		&cb_logblockcommand
	}
};

int findCommand(char * str)
{
	int i;
	for (i = 0; i < MAX_TYPE_ID; i++) {
		if (strncmp(str, MessageTypes[i].cmdText, strlen(str)) == 0) {
			return i;
		}
	}
	return -1;
}

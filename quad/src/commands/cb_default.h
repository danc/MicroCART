#include "commands.h"
/* The cb_default used on the groundStation. This file MUST NOT BE INCLUDED
 * by anything except for commands.c */

/* cb_default used by portable commands.c */
int cb_default(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length)
{
	return 0;
}

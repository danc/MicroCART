#ifndef __NODE_MULT_H__
#define __NODE_MULT_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_mult_type;

enum graph_node_mult_inputs {
    MULT_MULTIPLICAND1,
    MULT_MULTIPLICAND2,
};

enum graph_node_mult_outputs {
    MULT_PRODUCT
};

#endif  // __NODE_MULT_H__

#ifndef __GRAPH_BLOCKS_H__
#define __GRAPH_BLOCKS_H__

#include "computation_graph.h"
#include "node_constant.h"
#include "node_add.h"
#include "node_mult.h"
#include "node_gain.h"
#include "node_accumulator.h"
#include "node_bounds.h"
#include "node_mixer.h"
#include "node_pid.h"
#include "node_yaw_rot.h"
#include "node_integrator.h"

/*
 * ---------- How-To ------------
 * To add a new block type, put the implementation (.c and .h) files
 * in the same directory as this file, and include the header file above
 * Add a new entry to this enum right before MAX_BLOCK_TYPES
 * 
 * In graph_blocks.c, add a new entry at the end of the array with
 * your graph_node_type struct.
 */


/*
 * Enumerates all the types of different block types.
 * Must match blockDefs
 */
enum BlockTypes {
    BLOCK_CONSTANT,      // 00
    BLOCK_ADD,           // 01
    BLOCK_MULT,          // 02
    BLOCK_GAIN,          // 03
    BLOCK_ACCUMULATE,    // 04
    BLOCK_BOUNDS,        // 05
    BLOCK_MIXER,         // 06
    BLOCK_PID,           // 07
    BLOCK_YAW_ROT,       // 08
    BLOCK_INTEGRATOR,    // 09
    //                        <-- Insert new block type here
    MAX_BLOCK_TYPES
};

/*
 * Array corresponding to the different block type structs
 * Mustm match the nums in BlockTypes
 */
extern const struct graph_node_type* blockDefs[MAX_BLOCK_TYPES];


/*
 * Creates a new node and adds it to the graph with the given type ID and name
 * Returns the id of the new node upon success, -1 upon failure
 */
int graph_add_defined_block(struct computation_graph* graph, int type_id, const char* name);
#endif // __GRAPH_BLOCKS_H__

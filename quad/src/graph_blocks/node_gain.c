#include "node_gain.h"
#include <stdlib.h>

static void scale_nodes(void *state, const double* params, const double *inputs, double *outputs) {
    outputs[GAIN_RESULT] = inputs[GAIN_INPUT] * params[GAIN_GAIN];
}
static void reset(void *state) {}

static const char* const in_names[1] = {"Input"};
static const char* const out_names[1] = {"Amplified"};
static const char* const param_names[1] = {"Gain"};
const struct graph_node_type node_gain_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 1,
        .n_outputs = 1,
        .n_params = 1,
        .execute = scale_nodes,
        .reset = reset,
        .state_size = 0,
        .type_id = BLOCK_GAIN
};

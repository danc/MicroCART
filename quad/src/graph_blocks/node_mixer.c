#include "node_mixer.h"
#include <stdlib.h>
#include <math.h>

static int motor_min = 0.00000;
static int motor_max = 1.00000;

static double motor_clamp(double val) {
        if (isnan(val)) { val = motor_min; }
	else if (val < motor_min) {val = motor_min;}
	else if (val > motor_max) {val = motor_max;}
	return val;
}

static void mixer_computation(void *state, const double* params, const double *inputs, double *outputs) {
	double motor0 = inputs[MIXER_THROTTLE] - inputs[MIXER_PITCH] - inputs[MIXER_ROLL] - inputs[MIXER_YAW];
	double motor1 = inputs[MIXER_THROTTLE] + inputs[MIXER_PITCH] - inputs[MIXER_ROLL] + inputs[MIXER_YAW];
	double motor2 = inputs[MIXER_THROTTLE] - inputs[MIXER_PITCH] + inputs[MIXER_ROLL] + inputs[MIXER_YAW];
	double motor3 = inputs[MIXER_THROTTLE] + inputs[MIXER_PITCH] + inputs[MIXER_ROLL] - inputs[MIXER_YAW];
	outputs[MIXER_MOTOR0] = motor_clamp(motor0);
	outputs[MIXER_MOTOR1] = motor_clamp(motor1);
	outputs[MIXER_MOTOR2] = motor_clamp(motor2);
	outputs[MIXER_MOTOR3] = motor_clamp(motor3);
}

static void reset_mixer(void *state) {}

static const char* const in_names[4] = {"Throttle", "Pitch", "Roll", "Yaw"};
static const char* const out_names[4] = {"MOTOR 0", "MOTOR 1", "MOTOR 2", "MOTOR 3"};
static const char* const param_names[1] = {"You shouldnt see this"};
const struct graph_node_type node_mixer_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 4,
        .n_outputs = 4,
        .n_params = 0,
        .execute = mixer_computation,
        .reset = reset_mixer,
		.state_size = 0,
		.type_id = BLOCK_MIXER
};

#include "graph_blocks.h"
#include <stdlib.h>

// See graph_blocks.h
const struct graph_node_type* blockDefs[MAX_BLOCK_TYPES] = 
{
    &node_const_type,
    &node_add_type,
    &node_mult_type,
    &node_gain_type,
    &node_accum_type,
    &node_bounds_type,
    &node_mixer_type,
    &node_pid_type,
    &node_yaw_rot_type,
    &node_integrator_type
};


int graph_add_defined_block(struct computation_graph* graph, int type_id, const char* name) {
    // Verify block type is valid
    if (type_id >= MAX_BLOCK_TYPES) {
        return -1;
    }
    const struct graph_node_type *block_type = blockDefs[type_id];

    // Use the computation graph implementation's add node function
    return graph_add_node(graph, name, block_type);
}
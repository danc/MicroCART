#include "node_add.h"
#include <stdlib.h>

static void add_nodes(void *state, const double* params, const double *inputs, double *outputs) {
    outputs[ADD_SUM] = inputs[ADD_SUMMAND1] + inputs[ADD_SUMMAND2];
}
static void reset(void *state) {}

static const char* const in_names[2] = {"Summand 1", "Summand 2"};
static const char* const out_names[1] = {"Sum"};
static const char* const param_names[1] = {"Error if you see this"};
const struct graph_node_type node_add_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 2,
        .n_outputs = 1,
        .n_params = 0,
        .execute = add_nodes,
        .reset = reset,
        .state_size = 0,
        .type_id = BLOCK_ADD
};


#ifndef __NODE_INTEGRATOR_H__
#define __NODE_INTEGRATOR_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_integrator_type;

enum graph_node_integrate_inputs {
    INTEGRATE_IN,
    INTEGRATE_DT
};

enum graph_node_integrate_outputs {
    INTEGRATED
};
#endif // __NODE_INTEGRATOR_H__

#ifndef __NODE_GAIN_H__
#define __NODE_GAIN_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_gain_type;

enum graph_node_pow_inputs {
    GAIN_INPUT
};

enum graph_node_pow_params {
    GAIN_GAIN
};

enum graph_node_gain_outputs {
    GAIN_RESULT
};
#endif // __NODE_GAIN_H__

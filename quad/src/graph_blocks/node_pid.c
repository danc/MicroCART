#include "node_pid.h"
#include <stdlib.h>
#include <math.h>

static double FLT_EPSILON = 0.0001;


struct pid_node_state {
    double prev_val; // Previous input value
    double acc_error; // Accumulated error
    double last_filtered; // Last output from the filtered derivative
    int just_reset; // Set to 1 after a call to reset
};

// The generic PID diagram. This function takes in pid inputs (CUR_POINT and SETOINT) and calculates the output "pid_correction"
// part based on those parameters.
//
//              +  ---    error    ------------------		P		+  ---			  ----------------------------
// setpoint ---> / sum \ --------->| Kp * error	    |--------------->/ sum \ -------->| output: "pid_correction" |
//				 \     /	|	   ------------------				 \	   /		  ----------------------------
//				   ---		|										   ---					||
//                -	^       |	   									+ ^  ^ +				||
//					|		|	   -------------------------------	  |	 |			------- \/------------
//					|		|----->| Ki * accumulated error * dt |----+	 |			|					 |
//					|		|	   -------------------------------	I	 |			|		SYSTEM		 |
//					|		|											 |			|					 |
//					|		|											 |			--------||------------
//					|		|											 |					||
//					|		|      ----------------------------------	 |					||
//					|		|----->| Kd * (error - last error) / dt |----+					||
//				    |			   ----------------------------------  D					||
//					|																		||
//					|															 -----------\/-----------
//					|____________________________________________________________| Sensor measurements: |
//																				 |	 "current point"	|
//																				 ------------------------
//
static void pid_computation(void *state, const double* params, const double *inputs, double *outputs) {
    struct pid_node_state* pid_state = (struct pid_node_state*)state;

    double P = 0.0, I = 0.0, D = 0.0;

    // calculate the current error
    double error = inputs[PID_SETPOINT] - inputs[PID_CUR_POINT];

    // Accumulate the error (if Ki is less than epsilon, rougly 0,
    // then reset the accumulated error for safety)
    if (fabs(params[PID_KI]) <= FLT_EPSILON) {
      pid_state->acc_error = 0;
    } else {
      pid_state->acc_error += (error * inputs[PID_DT]);
    }

    if (pid_state->just_reset) {
        // On first call after a reset, set the previous value to
        // the current value to prevent a spike in derivative
        pid_state->prev_val = inputs[PID_CUR_POINT];
        pid_state->just_reset = 0;
    }

    // Compute each term's contribution
    P = params[PID_KP] * error;
    I = params[PID_KI] * pid_state->acc_error;
    // Low-pass filter on derivative
    double change_in_value = inputs[PID_CUR_POINT] - pid_state->prev_val;
    double term1 = params[PID_ALPHA] * pid_state->last_filtered;
    double derivative = change_in_value / inputs[PID_DT];
    if (inputs[PID_DT] == 0) { // Divide by zero check
        derivative = 0;
    }
    double term2 = params[PID_KD] * (1.0f - params[PID_ALPHA]) * derivative;
    D = term1 + term2;
    pid_state->last_filtered = D; // Store filtered value for next filter iteration
    pid_state->prev_val = inputs[PID_CUR_POINT]; // Store the current error into the state

    outputs[PID_CORRECTION] = P + I - D; // Store the computed correction
}

// This function sets the accumulated error and previous error to 0
// to prevent previous errors from affecting output after a reset
static void reset_pid(void *state) {
    struct pid_node_state* pid_state = (struct pid_node_state*)state;
    pid_state->acc_error = 0;
    pid_state->last_filtered = 0;
    pid_state->just_reset = 1;
}


static const char* const in_names[3] = {"Cur point", "Setpoint", "dt"};
static const char* const out_names[1] = {"Correction"};
static const char* const param_names[4] = {"Kp", "Ki", "Kd", "alpha"};
const struct graph_node_type node_pid_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 3,
        .n_outputs = 1,
        .n_params = 4,
        .execute = pid_computation,
        .reset = reset_pid,
        .state_size = sizeof(struct pid_node_state),
        .type_id = BLOCK_PID
};

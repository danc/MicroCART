#ifndef __NODE_MIXER_H__
#define __NODE_MIXER_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_mixer_type;

enum graph_node_mixer_inputs {
    MIXER_THROTTLE,
    MIXER_PITCH,
    MIXER_ROLL,
    MIXER_YAW,
};

enum graph_node_mixer_outputs {
    MIXER_MOTOR0,
    MIXER_MOTOR1,
    MIXER_MOTOR2,
    MIXER_MOTOR3,
};

#endif // __NODE_MIXER_H__

#ifndef __NODE_BOUNDS_H__
#define __NODE_BOUNDS_H__
#include "computation_graph.h"
#include "graph_blocks.h"


extern const struct graph_node_type node_bounds_type;

enum graph_node_bounds_inputs {
    BOUNDS_IN, // Input to be bounded
};

enum graph_node_bounds_outputs {
    BOUNDS_OUT // Bounded output
};

enum graph_node_bounds_params {
    BOUNDS_MIN,
    BOUNDS_MAX
};

#endif // __NODE_BOUNDS_H__

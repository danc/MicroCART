#include "node_pow.h"
#include <stdlib.h>
#include <math.h>

static void pow_nodes(void *state, const double* params, const double *inputs, double *outputs) {
    outputs[POW_RESULT] = pow(inputs[POW_BASE], params[POW_EXP]);
}
static void reset(void *state) {}

static const char* const in_names[1] = {"Base"};
static const char* const out_names[1] = {"Result"};
static const char* const param_names[1] = {"Exponent"};
const struct graph_node_type node_pow_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 1,
        .n_outputs = 1,
        .n_params = 1,
        .execute = pow_nodes,
        .reset = reset,
        .state_size = 0,
};

#ifndef __NODE_ROTATE_H__
#define __NODE_ROTATE_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_yaw_rot_type;

enum graph_node_yaw_rot_inputs {
    ROT_YAW, // Amount of current yaw
    ROT_CUR_X, // Input X position
    ROT_CUR_Y // Input Y position
};

enum graph_node_yaw_rot_outputs {
    ROT_OUT_X, // Rotated X position
    ROT_OUT_Y // Rotated Y position
};
#endif // __NODE_ROTATE_H__

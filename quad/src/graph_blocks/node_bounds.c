#include "node_bounds.h"
#include <stdlib.h>

static void bounds_computation(void *state, const double* params, const double *inputs, double *outputs) {
    double to_be_bounded = inputs[BOUNDS_IN];
    if (to_be_bounded < params[BOUNDS_MIN]) {
        to_be_bounded = params[BOUNDS_MIN];
    }
    if (to_be_bounded > params[BOUNDS_MAX]) {
        to_be_bounded = params[BOUNDS_MAX];
    }
    outputs[BOUNDS_OUT] = to_be_bounded;
}

static void reset_bounds(void *state) {}

static const char* const in_names[1] = {"Bounds in"};
static const char* const out_names[1] = {"Bounded"};
static const char* const param_names[2] = {"Min", "Max"};
const struct graph_node_type node_bounds_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 1,
        .n_outputs = 1,
        .n_params = 2,
        .execute = bounds_computation,
        .reset = reset_bounds,
        .state_size = 0,
        .type_id = BLOCK_BOUNDS
};

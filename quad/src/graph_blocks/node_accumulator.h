#ifndef __NODE_ACCUMULATOR_H__
#define __NODE_ACCUMULATOR_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_accum_type;

enum graph_node_accum_inputs {
    ACCUM_IN,
};

enum graph_node_accum_outputs {
    ACCUMULATED
};
#endif // __NODE_ACCUMULATOR_H__

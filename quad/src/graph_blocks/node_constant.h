#ifndef __NODE_CONSTANT_H__
#define __NODE_CONSTANT_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_const_type;

enum graph_node_const_params {
    CONST_SET
};

enum graph_node_const_outputs {
    CONST_VAL
};
#endif //__NODE_CONSTANT_H__

#include "node_mult.h"
#include <stdlib.h>

static void mult_nodes(void *state, const double* params, const double *inputs, double *outputs) {
    outputs[MULT_PRODUCT] = inputs[MULT_MULTIPLICAND1] * inputs[MULT_MULTIPLICAND2];
}
static void reset(void *state) {}

static const char* const in_names[2] = {"Multiplicand 1", "Multiplicand 2"};
static const char* const out_names[1] = {"Product"};
static const char* const param_names[0] = {};
const struct graph_node_type node_mult_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 2,
        .n_outputs = 1,
        .n_params = 0,
        .execute = mult_nodes,
        .reset = reset,
        .state_size = 0,
        .type_id = BLOCK_MULT
};

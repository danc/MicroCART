#include "node_constant.h"
#include <stdlib.h>

static void output_const(void *state, const double *params, const double *inputs, double *outputs) {
    outputs[CONST_VAL] = params[CONST_SET];
}
static void reset(void *state) {}

static const char* const in_names[1] = {"You shouldn't see this"};
static const char* const out_names[1] = {"Constant"};
static const char* const param_names[1] = {"Constant"};
const struct graph_node_type node_const_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 0,
        .n_outputs = 1,
        .n_params = 1,
        .execute = output_const,
        .reset = reset,
        .state_size = 0,
        .type_id = BLOCK_CONSTANT
};

#include "node_integrator.h"
#include <stdlib.h>

struct integrate_state {
    double integrated;
};

static void integrate(void *state, const double* params, const double *inputs, double *outputs) {
    struct integrate_state* my_state = (struct integrate_state*)state;
    my_state->integrated += inputs[INTEGRATE_IN] * inputs[INTEGRATE_DT];
    outputs[INTEGRATED] = my_state->integrated;
}

static void reset(void *state) {
    ((struct integrate_state*)state)->integrated = 0;
}


static const char* const in_names[2] = {"Integrator In", "Integrator dt"};
static const char* const out_names[1] = {"Integrated"};
static const char* const param_names[0] = {};
const struct graph_node_type node_integrator_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 2,
        .n_outputs = 1,
        .n_params = 0,
        .execute = integrate,
        .reset = reset,
        .state_size = sizeof(struct integrate_state),
        .type_id = BLOCK_INTEGRATOR
};


#include "node_accumulator.h"
#include <stdlib.h>

struct accum_state {
    double accumulated;
};

static void accum_nodes(void *state, const double* params, const double *inputs, double *outputs) {
    struct accum_state* my_state = (struct accum_state*)state;
    my_state->accumulated += inputs[ACCUM_IN];
    outputs[ACCUMULATED] = my_state->accumulated;
}

static void reset(void *state) {
    ((struct accum_state*)state)->accumulated = 0;
}


static const char* const in_names[2] = {"Accumulator in"};
static const char* const out_names[1] = {"Accumulated"};
static const char* const param_names[0] = {};
const struct graph_node_type node_accum_type = {
        .input_names = in_names,
        .output_names = out_names,
        .param_names = param_names,
        .n_inputs = 1,
        .n_outputs = 1,
        .n_params = 0,
        .execute = accum_nodes,
        .reset = reset,
        .state_size = sizeof(struct accum_state),
        .type_id = BLOCK_ACCUMULATE
};


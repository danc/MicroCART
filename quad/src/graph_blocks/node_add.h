#ifndef __NODE_ADD_H__
#define __NODE_ADD_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_add_type;

enum graph_node_add_inputs {
    ADD_SUMMAND1,
    ADD_SUMMAND2,
};

enum graph_node_add_outputs {
    ADD_SUM
};
#endif // __NODE_ADD_H__

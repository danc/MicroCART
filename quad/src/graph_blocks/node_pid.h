#ifndef __NODE_PID_H__
#define __NODE_PID_H__
#include "computation_graph.h"
#include "graph_blocks.h"

extern const struct graph_node_type node_pid_type;

enum graph_node_pid_inputs {
    PID_CUR_POINT, // Current value of the system
    PID_SETPOINT,	// Desired value of the system
    PID_DT // sample period
};

enum graph_node_pid_outputs {
    PID_CORRECTION // Correction factor computed by the PID
};

enum graph_node_pid_params {
    PID_KP, // Proportional constant
    PID_KI, // Integral constant
    PID_KD, // Derivative constant
    PID_ALPHA // alpha = (1 - N*T_s); High values mean more filtering
};

#endif // __NODE_PID_H__

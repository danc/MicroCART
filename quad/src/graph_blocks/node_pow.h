#ifndef __NODE_POW_H__
#define __NODE_POW_H__
#include "computation_graph.h"

extern const struct graph_node_type node_pow_type;

enum graph_node_pow_inputs {
    POW_BASE
};

enum graph_node_pow_params {
    POW_EXP
};

enum graph_node_pow_outputs {
    POW_RESULT
};
#endif // __NODE_POW_H__

GCC = gcc
AR = ar

INCDIR = $(TOP)/inc
OBJDIR = obj
LIBDIR = $(TOP)/lib

SOURCES = $(wildcard *.c)
TESTSOURCES = $(wildcard test/*.c)
HEADERS = $(wildcard *.h)
INCLUDES = $(addprefix $(INCDIR)/, $(HEADERS))
OBJECTS = $(patsubst %.c, $(OBJDIR)/%.o, $(SOURCES))
TESTOBJECTS = $(patsubst $.c, %.o, $(TESTSOURCES))

TARGET = $(LIBDIR)/lib$(NAME).a
TESTBIN = run_tests

# For Unity; allows custom configuration
UNITYDEFINES = -DUNITY_INCLUDE_CONFIG_H

.PHONY: default test clean

################
## User Targets
################

default: $(TARGET) $(INCLUDES)

test: $(TESTBIN)
	./$(TESTBIN)

clean:
	rm -rf $(TARGET) $(INCLUDES) $(OBJDIR)

####################
## Internal Targets
####################

$(TARGET): $(OBJECTS) | $(LIBDIR)
	$(AR) rcs $@ $^

$(OBJDIR)/%.o : %.c | $(OBJDIR) $(INCDIR)
	$(GCC) -c -g -o $@ $< -I$(INCDIR) -Wall $(DEFINES)

$(INCDIR)/%.h : %.h | $(INCDIR)
	cp $^ $(INCDIR)

$(INCDIR):
	mkdir $(INCDIR)

$(OBJDIR):
	mkdir $(OBJDIR)

$(LIBDIR):
	mkdir $(LIBDIR)

$(TESTBIN): $(TESTOBJECTS) $(OBJECTS) | default
	$(GCC) -g -o $(TESTBIN) $^ -I$(INCDIR) -L$(LIBDIR) $(REQLIBS) $(UNITYDEFINES)

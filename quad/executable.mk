GCC = gcc

INCDIR = $(TOP)/inc
OBJDIR = obj
EXEDIR = $(TOP)/bin
LIBDIR = $(TOP)/lib

SOURCES = $(wildcard *.c)
HEADERS = $(wildcard *.h)
INCLUDES = $(addprefix $(INCDIR)/, $(HEADERS))
OBJECTS = $(patsubst %.c, $(OBJDIR)/%.o, $(SOURCES))

TARGET = $(EXEDIR)/$(NAME)

CLEANUP = $(TARGET) $(OBJDIR) $(NAME)

.PHONY: default run clean

################
## User Targets
################

default: $(TARGET)

clean:
	rm -rf $(CLEANUP)

####################
## Internal Targets
####################

$(TARGET): $(OBJECTS) | $(EXEDIR)
	$(GCC) -g -o $(TARGET) $^ -I$(INCDIR) -L$(LIBDIR) $(REQLIBS)
	cp $(TARGET) $(NAME)

$(OBJDIR)/%.o : %.c | $(OBJDIR) $(INCDIR)
	$(GCC) -c -g -o $@ $< -I$(INCDIR) -Wall

$(OBJDIR):
	mkdir $(OBJDIR)

$(EXEDIR):
	mkdir $(EXEDIR)

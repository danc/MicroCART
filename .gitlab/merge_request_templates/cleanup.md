## Problem Description
*Describe the problem you addressed with the clean up*

## Refactor Description
*Describe your clean up efforts*

## Clean up areas
*Remove inapplicable items*
- Code refactor
- Code comments added
- Documentation added

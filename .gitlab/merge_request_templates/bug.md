## Bug Description
*Describe the bug*

## Fix Description
*Describe your implemented fix*

## Impacted Areas
*Remove inapplicable items*
- Quad Software
- Controls
- Ground Station
- Testing


#include "stabilizer.h"
#include "stabilizer_types.h"

#include "student_attitude_controller.h"
#include "sensfusion6.h"
#include "controller_student.h"

#include "log.h"
#include "debug.h"

#include "param.h"
#include "math3d.h"

#define STUDENT_UPDATE_DT    (float)(1.0f/ATTITUDE_RATE)

static bool tiltCompensationEnabled = false;
static bool tuning_mode = false;

//desired vehicle state as calculated by the PID controllers
static attitude_t attitudeDesired;
static attitude_t rateDesired;
static float thrustDesired;

//variables used only for logging PID command outputs
static float cmd_thrust;
static float cmd_roll;
static float cmd_pitch;
static float cmd_yaw;
static float measured_roll_rate;
static float measured_pitch_rate;
static float measured_yaw_rate;
static float measured_z_accel;

void controllerStudentInit(void)
{
  studentAttitudeControllerInit(STUDENT_UPDATE_DT);
}

bool controllerStudentTest(void)
{
  bool pass = true;
  //controller passes check if attitude controller passes
  pass &= studentAttitudeControllerTest();

  return pass;
}


/**
 * Limit the input angle between -180 and 180
 * 
 * @param angle 
 * @return float 
 *
static float capAngle(float angle) {
  //TODO MICROCART: remove
  
  float result = angle;

  while (result > 180.0f) {
    result -= 360.0f;
  }

  while (result < -180.0f) {
    result += 360.0f;
  }

  return result;
}
*/

/**
 * This function is called periodically to update the PID loop,
 * Reads state estimate and setpoint values and passes them
 * to the functions that preform PID calculations, postion PID (including Velocity PID),
 * and attitude PID (including attitude rate)
 * 
 * @param control Output, struct is modified as the ouput of the control loop, holds the commanded outputs
 * @param setpoint Input, setpoints for thrust, position, velocity, and acceleration of quad
 * @param sensors Input, Raw sensor values (typically want to use the state estimator instead) includes gyro, 
 * accelerometer, barometer, magnatometer 
 * @param state Input, A robust way to measure the current state of the quad using multiple internal sensors, allows for direct
 * measurements of the orientation of the quad. Includes attitude, position, velocity, and acceleration. Does not include
 * attitude rate, use sensors->gyro directly instead
 * @param tick Input, system clock
 */
void controllerStudent(control_t *control, setpoint_t *setpoint, const sensorData_t *sensors, const state_t *state, const uint32_t tick)
{

  //TODO MICROCART: student written

  // check if time to update the attutide controller
  if (RATE_DO_EXECUTE(ATTITUDE_RATE, tick)) {

    //only support attitude and attitude rate control
    if(setpoint->mode.x != modeDisable || setpoint->mode.y != modeDisable || setpoint->mode.z != modeDisable){
      DEBUG_PRINT("Student controller does not support vehicle position or velocity mode. Check flight mode.");
      control->thrust = 0;
      control->roll = 0;
      control->pitch = 0;
      control->yaw = 0;
      return;
    }

    //set desired roll and pitch and yaw angles
    attitudeDesired.roll = setpoint->attitude.roll;
    attitudeDesired.pitch = setpoint->attitude.pitch;

    //only do yaw hold if not in tuning mode
    if(tuning_mode == false){
      // Rate-controlled YAW is moving YAW angle setpoint
      if (setpoint->mode.yaw == modeVelocity) {
        attitudeDesired.yaw += setpoint->attitudeRate.yaw * STUDENT_UPDATE_DT;
      } else {
        attitudeDesired.yaw = setpoint->attitude.yaw;
      }
    }

    //set desired thrust
    thrustDesired = setpoint->thrust;


    // Run the attitude controller with the measured attitude and desired attitude
    // outputs the desired attitude rates
    studentAttitudeControllerCorrectAttitudePID(state->attitude.roll, state->attitude.pitch, state->attitude.yaw,
                                attitudeDesired.roll, attitudeDesired.pitch, attitudeDesired.yaw,
                                &rateDesired.roll, &rateDesired.pitch, &rateDesired.yaw);

    // if velocity mode, overwrite rateDesired output 
    // from the attitude controller with the setpoint value
    // Also reset the PID error values to avoid error buildup, which can lead to unstable
    // behavior if level mode is engaged later
    if (setpoint->mode.roll == modeVelocity) {
      rateDesired.roll = setpoint->attitudeRate.roll;
      studentAttitudeControllerResetRollAttitudePID();
    }
    if (setpoint->mode.pitch == modeVelocity) {
      rateDesired.pitch = setpoint->attitudeRate.pitch;
      studentAttitudeControllerResetPitchAttitudePID();
    }

    // allow for direct yaw rate control if in tuning mode
    if(tuning_mode == true){
      if (setpoint->mode.yaw == modeVelocity) {
        rateDesired.yaw = setpoint->attitudeRate.yaw;
        attitudeDesired.roll = 3;
        studentAttitudeControllerResetYawAttitudePID();
      }
    }

    //update the attitude rate PID, given the current angular rate 
    //read by the gyro and the desired rate 
    studentAttitudeControllerCorrectRatePID(sensors->gyro.x, -sensors->gyro.y, sensors->gyro.z,
                             rateDesired.roll, rateDesired.pitch, rateDesired.yaw,
                             &(control->roll), &(control->pitch), &(control->yaw));
    rateDesired.yaw = setpoint->attitudeRate.yaw;
    //invert yaw control
    control->yaw = -control->yaw;
  }

  //tilt compensation, increases thrust when tilting more to maintain height
  //copy working value of actuator thrust to control->thrust for output
  if (tiltCompensationEnabled)
  {
    control->thrust = thrustDesired / sensfusion6GetInvThrustCompensationForTilt();
  }
  else
  {
    control->thrust = thrustDesired;
  }


  //if no thrust active, set all other outputs to 0 and reset PID variables
  if (control->thrust == 0)
  {
    control->thrust = 0;
    control->roll = 0;
    control->pitch = 0;
    control->yaw = 0;

    //reset all PID variables
    studentAttitudeControllerResetAllPID();

    // Reset the YAW angle desired 
    attitudeDesired.yaw = state->attitude.yaw;
  }

  //copy values for logging
  cmd_thrust = control->thrust;
  cmd_roll = control->roll;
  cmd_pitch = control->pitch;
  cmd_yaw = control->yaw;
  measured_roll_rate = radians(sensors->gyro.x);
  measured_pitch_rate = -radians(sensors->gyro.y);
  measured_yaw_rate = radians(sensors->gyro.z);
  measured_z_accel = sensors->acc.z;

}

/**
 * Logging variables for the command and reference signals for the
 * student PID controller
 */
LOG_GROUP_START(ctrlStdnt)
/**
 * @brief Thrust command
 */
LOG_ADD(LOG_FLOAT, cmd_thrust, &cmd_thrust)
/**
 * @brief Roll command
 */
LOG_ADD(LOG_FLOAT, cmd_roll, &cmd_roll)
/**
 * @brief Pitch command
 */
LOG_ADD(LOG_FLOAT, cmd_pitch, &cmd_pitch)
/**
 * @brief yaw command
 */
LOG_ADD(LOG_FLOAT, cmd_yaw, &cmd_yaw)
/**
 * @brief Gyro roll measurement in radians
 */
LOG_ADD(LOG_FLOAT, msrdRollRate, &measured_roll_rate)
/**
 * @brief Gyro pitch measurement in radians
 */
LOG_ADD(LOG_FLOAT, msrdPitchRate, &measured_pitch_rate)
/**
 * @brief Yaw  measurement in radians
 */
LOG_ADD(LOG_FLOAT, msrdYawRate, &measured_yaw_rate)
/**
 * @brief Acceleration in the zaxis in G-force
 */
LOG_ADD(LOG_FLOAT, msrdZAccel, &measured_z_accel)
/**
 * @brief Thrust command without (tilt)compensation
 */
LOG_ADD(LOG_FLOAT, thrustDesired, &thrustDesired)
/**
 * @brief Desired roll setpoint
 */
LOG_ADD(LOG_FLOAT, desiredRoll,      &attitudeDesired.roll)
/**
 * @brief Desired pitch setpoint
 */
LOG_ADD(LOG_FLOAT, desiredPitch,     &attitudeDesired.pitch)
/**
 * @brief Desired yaw setpoint
 */
LOG_ADD(LOG_FLOAT, desiredYaw,       &attitudeDesired.yaw)
/**
 * @brief Desired roll rate setpoint
 */
LOG_ADD(LOG_FLOAT, dsirdRollRate,  &rateDesired.roll)
/**
 * @brief Desired pitch rate setpoint
 */
LOG_ADD(LOG_FLOAT, dsirdPitchRate, &rateDesired.pitch)
/**
 * @brief Desired yaw rate setpoint
 */
LOG_ADD(LOG_FLOAT, dsirdYawRate,   &rateDesired.yaw)

LOG_GROUP_STOP(ctrlStdnt)


/**
 * Controller parameters
 */
PARAM_GROUP_START(ctrlStdnt)
/**
 * @brief Nonzero for tilt compensation enabled (default: 0)
 */
PARAM_ADD(PARAM_UINT8, tiltComp, &tiltCompensationEnabled)

/**
 * @brief Changes how the yaw setpoint behaves to allow for tuning of 
 * yaw rate and yaw angle PIDs
 */
PARAM_ADD(PARAM_UINT8, tuning_mode, &tuning_mode)

PARAM_GROUP_STOP(ctrlStdnt)
// Copyright (c) 2013, Jan Winkler <winkler@cs.uni-bremen.de>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of Universität Bremen nor the names of its
//       contributors may be used to endorse or promote products derived from
//       this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "CTOC.h"
#include "CCrazyflie.h"

#include <limits>

static const char *param_header = "Param ID\tType\tGroup\t"
						   "Identifier Name";

static const char *log_header = "Log ID\tType\tGroup\t"
						   "Identifier Name";

CTOC::CTOC(CCrazyRadio *crRadio, CCrazyflie *crazyflie, int radioChannel, int nPort) {
	m_crRadio = crRadio;
	m_crazyflie = crazyflie;
	m_nRadioChannel = radioChannel;
	m_nPort = nPort;
	m_nItemCount = 0;
	char fileName[255];
	if(nPort == 0x02) {
		sprintf(fileName, "logs/cflie%d_param_toc", crazyflie->getQuadcopterNumber()+1);
		this->openParamTOCFile(fileName);
	}
	else if(nPort == 0x05) {
		sprintf(fileName, "logs/cflie%d_log_toc", crazyflie->getQuadcopterNumber()+1);
		this->openLogTOCFile(fileName);
	}
}

CTOC::~CTOC() {
}

bool CTOC::sendTOCPointerReset() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__);
	CCRTPPacket* crtpPacket = new CCRTPPacket(0, 0);
	crtpPacket->setPort(m_nPort);
	crtpPacket->setChannel(0);
	CCRTPPacket* crtpReceived = m_crRadio->sendPacket(m_nRadioChannel,
			crtpPacket, m_crazyflie);

	if (crtpReceived) {
		delete crtpReceived;
		return true;
	}
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__);
	return false;
}

bool CTOC::requestMetaData() {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bReturnvalue = false;

	if( ALL_THE_DEBUG ) printf( "new CCRTP Packet\n" );
	CCRTPPacket* crtpPacket = new CCRTPPacket(0x03, 0);
	if( ALL_THE_DEBUG ) printf( "set port\n" );
	crtpPacket->setPort(m_nPort);
	//crtpPacket->setChannel(0);
	if( ALL_THE_DEBUG ) printf( "..sendAndReceive\n" );
	CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpPacket,
			m_nRadioChannel, m_crazyflie);

	// if (crtpReceived->data()[0] == 0x03) {
	// 	m_nItemCount = (uint8_t) crtpReceived->data()[1];
	// 	bReturnvalue = true;
	// }
	if(crtpReceived->data()[0] == 0x03) {
		char amount[2];
		amount[0] = crtpReceived->data()[1];
		amount[1] = crtpReceived->data()[2];
		memcpy(&m_nItemCount, amount, sizeof(uint16_t));
		bReturnvalue = true;
	}

	delete crtpReceived;
	delete crtpPacket;
	return bReturnvalue;
}

bool CTOC::requestInitialItem() {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	return this->requestItem(0, true);
}

bool CTOC::requestItem(int nID) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	return this->requestItem(nID, false);
}

bool CTOC::requestItem(int nID, bool bInitial) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bReturnvalue = false;
	uint16_t rID = (uint16_t) nID;
	char cRequest[3];
	cRequest[0] = 0x02;
	memcpy(&cRequest[1], &rID, sizeof(uint16_t));

	CCRTPPacket* crtpPacket = new CCRTPPacket(cRequest, (bInitial ? 1 : 3), 0);
	crtpPacket->setPort(m_nPort);
	//crtpPacket->setChannel(0x0);
	CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, m_crazyflie);

	bReturnvalue = this->processItem(crtpReceived);

	delete crtpReceived;
	delete crtpPacket;
	return bReturnvalue;
}

bool CTOC::requestNextItem() {
	if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bReturnvalue = false;

	if( ALL_THE_DEBUG ) printf( "new CCRTP Packet\n" );
	CCRTPPacket* crtpPacket = new CCRTPPacket(1, 0);
	if( ALL_THE_DEBUG ) printf( "set port\n" );
	crtpPacket->setPort(m_nPort);
	crtpPacket->setChannel(0);
	if( ALL_THE_DEBUG ) printf( "..sendAndReceive\n" );
	CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpPacket,
			m_nRadioChannel, m_crazyflie);

	if( ALL_THE_DEBUG ) printf( "retrieved item\n" );
	bReturnvalue = this->processItem(crtpReceived);
	if(!bReturnvalue) {
		if(ALL_THE_DEBUG) printf("failure processing item\n");
		return bReturnvalue;
	}
	if( ALL_THE_DEBUG ) printf( "item saved\n" );

	return bReturnvalue;
}

//probably works but super inefficient
bool CTOC::requestItems() {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	for (int nI = 0; nI < m_nItemCount; nI++) {
		this->requestItem(nI);
	}

	return true;
}

bool CTOC::downloadTOC() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__);
	if(m_nPort == 2) {
		this->requestMetaData();
		//this->sendTOCPointerReset();
		for (int nI = 0; nI < m_nItemCount; nI++) {
			printf("Parameter Progress: %i / %i\n", nI+1, m_nItemCount);
			this->requestItem(nI);
		}
		param_toc_file.close();
	}
	else {
		this->requestMetaData();
		for (int nI = 0; nI < m_nItemCount; nI++) {
			printf("Logging Progress: %i / %i\n", nI+1, m_nItemCount);
			this->requestItem(nI);
		}
		log_toc_file.close();
	}
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__);
	return true;
}

bool CTOC::processItem(CCRTPPacket* crtpItem) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	if (crtpItem->port() == m_nPort) {
		if (crtpItem->channel() == 0) {
			char* cData = crtpItem->data();
			//int nLength = crtpItem->dataLength(); -- unused variable

			if (cData[0] == 0x2 || cData[0] == 0x3) { // Command identification ok?
				uint16_t nID;
				memcpy(&nID, &cData[1], sizeof(uint16_t));
				uint8_t nType = cData[3] & 0x0F;

				std::string strGroup;
				int nI;
				for (nI = 4; cData[nI] != '\0'; nI++) {
					strGroup += cData[nI];
				}

				nI++;
				std::string strIdentifier;
				for (; cData[nI] != '\0'; nI++) {
					strIdentifier += cData[nI];
				}

				struct TOCElement teNew;
				teNew.strIdentifier = strIdentifier;
				teNew.strGroup = strGroup;
				teNew.nID = nID;
				teNew.nType = nType;
				teNew.bIsLogging = false;
				teNew.dValue = 0;
				teNew.timeLastUpdated = 0;

				if(ALL_THE_DEBUG) printf("Group.Name: %s.%s ID: %i Type: %i ", strGroup.c_str(), strIdentifier.c_str(), nID, nType);

				if(m_nPort == 0x02) {
					this->param_toc_file << teNew.nID << "\t" << teNew.nType << "\t" << teNew.strGroup << "\t" << teNew.strIdentifier << std::endl;
				}
				else if(m_nPort = 0x05) {
					this->log_toc_file << teNew.nID << "\t" << teNew.nType << "\t" << teNew.strGroup << "\t" << teNew.strIdentifier << std::endl;
				}

				m_lstTOCElements.push_back(teNew);

				if(m_nPort == 0x02) {
					crtpItem->setPort(m_nPort);
					crtpItem->setChannel(1); 
					crtpItem->clearData();
					char cBuffer[1];
					cBuffer[0] = nID;
					crtpItem->setData(cBuffer, 1);

					CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpItem,
						m_nRadioChannel, m_crazyflie);

					//float fValue = 0;
					int nByteLength = 0;

					
					//teNew.dValue = crtpReceived->data()[1];
					switch(teNew.nType) {
						//int16
						case 1:
							nByteLength = 2;
							int16_t int16Value;
							memcpy(&int16Value, &crtpReceived->data()[1],
									nByteLength);
							teNew.dValue = (double) int16Value;
						break;
						case 2:
							nByteLength = 4;
							int32_t int32Value;
							memcpy(&int32Value, &crtpReceived->data()[1],
									nByteLength);
							teNew.dValue = (double) int32Value;
						break;
						case 3:
							nByteLength = 8;
							int64_t int64Value;
							memcpy(&int64Value, &crtpReceived->data()[1],
									nByteLength);
							teNew.dValue = (double) int64Value;
						break;
						case 5:
							// NOTE(winkler): This is untested code (as no FP16
							// variable gets advertised yet). This has to be tested
							// and is to be used carefully. I will do that as soon
							// as I find time for it.
							/*nByteLength = 2;
							char cBuffer1[nByteLength];
							char cBuffer2[4];
							memcpy(cBuffer1, &crtpReceived->data()[1], nByteLength);
							cBuffer2[0] = cBuffer1[0] & 0b10000000; // Get the sign bit
							cBuffer2[1] = 0;
							cBuffer2[2] = cBuffer1[0] & 0b01111111; // Get the magnitude
							cBuffer2[3] = cBuffer1[1];
							memcpy(teNew.dValue, cBuffer2, 4); // Put it into the float variable
							*/
						break;
						case 6:
							nByteLength = 4;
							float fValue;
							memcpy(&fValue, &crtpReceived->data()[1], nByteLength);
							teNew.dValue = (double) fValue;
						break;
						case 7:
							/*nByteLength = 8;
							double doubleValue;
							memcpy(&doubleValue, &cLogdata[nOffset],
									nByteLength);
							fValue = uint32Value;
							*/
						break;
						case 8:
							nByteLength = 1;
							uint8_t uint8Value;
							memcpy(&uint8Value, &crtpReceived->data()[1],
									nByteLength);
							teNew.dValue = (double) uint8Value;
						break;
						case 9:
						nByteLength = 2;
							uint16_t uint16Value;
							memcpy(&uint16Value, &crtpReceived->data()[1],
									nByteLength);
							teNew.dValue = (double) uint16Value;
						break;
						case 10:
							nByteLength = 4;
							uint32_t uint32Value;
							memcpy(&uint32Value, &crtpReceived->data()[1],
									nByteLength);
							teNew.dValue = (double) uint32Value;
						break;
						case 11:
							nByteLength = 8;
							uint64_t uint64Value;
							memcpy(&uint64Value, &crtpReceived->data()[1],
									nByteLength);
							teNew.dValue = (double) uint64Value;
						break;
						default:
						break;
					}
					// NOTE(winkler): For debug purposes only.
		//				std::cout << strGroup << "." << strIdentifier << std::endl;

					if(ALL_THE_DEBUG) printf("Value: %f\n", teNew.dValue);
				}
				else {
					if(ALL_THE_DEBUG) printf("\n");
				}
				

				return true;
			}
		}
	}

	return false;
}

bool CTOC::processNextItem(CCRTPPacket* crtpItem) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	if (crtpItem->port() == m_nPort) {
		if (crtpItem->channel() == 0) {
			char* cData = crtpItem->data();
			//int nLength = crtpItem->dataLength(); -- unused variable

			if (cData[0] == 0x1) { // Command identification ok?
				uint8_t nID = cData[1];
				uint8_t nType = cData[2];

				std::string strGroup;
				int nI;
				for (nI = 3; cData[nI] != '\0'; nI++) {
					strGroup += (char) cData[nI];
				}

				nI++;
				std::string strIdentifier;
				for (; cData[nI] != '\0'; nI++) {
					strIdentifier += (char) cData[nI];
				}

				struct TOCElement teNew;
				teNew.strIdentifier = strIdentifier;
				teNew.strGroup = strGroup;
				teNew.nID = nID;
				teNew.nType = nType;
				teNew.bIsLogging = false;
				teNew.dValue = 0;
				teNew.timeLastUpdated = 0;

				m_lstTOCElements.push_back(teNew);

				// NOTE(winkler): For debug purposes only.
//				std::cout << strGroup << "." << strIdentifier << std::endl;

				return true;
			}
		}
	}

	return false;
}

struct TOCElement CTOC::elementForName(std::string strName, bool& bFound) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	for (std::list<struct TOCElement>::iterator itElement =
			m_lstTOCElements.begin(); itElement != m_lstTOCElements.end();
			itElement++) {
		struct TOCElement teCurrent = *itElement;

		std::string strTempFullname = teCurrent.strGroup + "."
				+ teCurrent.strIdentifier;
		if (strName == strTempFullname) {
			bFound = true;
			return teCurrent;
		}
	}

	bFound = false;
	struct TOCElement teEmpty;

	return teEmpty;
}

struct TOCElement CTOC::elementForID(int nID, bool& bFound) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	for (std::list<struct TOCElement>::iterator itElement =
			m_lstTOCElements.begin(); itElement != m_lstTOCElements.end();
			itElement++) {
		struct TOCElement teCurrent = *itElement;

		if (nID == teCurrent.nID) {
			bFound = true;
			return teCurrent;
		}
	}

	bFound = false;
	struct TOCElement teEmpty;

	return teEmpty;
}

int CTOC::idForName(std::string strName) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;

	struct TOCElement teResult = this->elementForName(strName, bFound);

	if (bFound) {
		return teResult.nID;
	}

	return -1;
}

int CTOC::typeForName(std::string strName) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;

	struct TOCElement teResult = this->elementForName(strName, bFound);

	if (bFound) {
		return teResult.nType;
	}

	return -1;
}

bool CTOC::startLogging(std::string strName, std::string strBlockName) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;
	struct LoggingBlock lbCurrent = this->loggingBlockForName(strBlockName,
			bFound);

	if (bFound) {
		struct TOCElement teCurrent = this->elementForName(strName, bFound);
		if (bFound) {
			char cID[2];
			uint16_t idOS = (uint16_t) teCurrent.nID;
			memcpy(cID, &idOS, sizeof(uint16_t));
			char cPayload[5] = { 0x07, (char)lbCurrent.nID, (char)teCurrent.nType,
					cID[0], cID[1] };
			CCRTPPacket* crtpLogVariable = new CCRTPPacket(cPayload, 5, 1);
			crtpLogVariable->setPort(m_nPort);
			crtpLogVariable->setChannel(1);
			CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(
					crtpLogVariable, m_nRadioChannel, m_crazyflie);

			delete crtpLogVariable;

			char* cData = crtpReceived->data();
			bool bCreateOK = false;
			if (cData[0] == 0x07 && cData[1] == lbCurrent.nID
					&& cData[2] == 0x00) {
				bCreateOK = true;
			} else {
				printf("0x%x %x %x\n", cData[1], cData[2], cData[3]);
			}

			if (crtpReceived) {
				delete crtpReceived;
			}
			this->addElementToBlock(lbCurrent.nID, teCurrent.nID);

			if (bCreateOK) {
				return true;
			} else {
				std::cout << "Unable to add element " << strName << " to " << strBlockName << std::endl;
			}
		} else {
			std::cout << "Unable to find element " << strName << " in block table" << std::endl;
		}
	} else {
		std::cout << "Unable to find logging block " << strBlockName << " in the active list" << std::endl;
	}

	return false;
}

bool CTOC::addElementToBlock(int nBlockID, int nElementID) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	for (std::list<struct LoggingBlock>::iterator itBlock =
			m_lstLoggingBlocks.begin(); itBlock != m_lstLoggingBlocks.end();
			itBlock++) {
		struct LoggingBlock lbCurrent = *itBlock;

		if (lbCurrent.nID == nBlockID) {
			(*itBlock).lstElementIDs.push_back(nElementID);

			return true;
		}
	}

	return false;
}

bool CTOC::stopLogging(std::string strName) {
	// TODO: Implement me.
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	return false;
}

bool CTOC::isLogging(std::string strName) {
	// TODO: Implement me.
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	return false;
}

double CTOC::doubleValue(std::string strName) {
	bool bFound;

	struct TOCElement teResult = this->elementForName(strName, bFound);

	if (bFound) {
		return teResult.dValue;
	}

	return std::numeric_limits<double>::quiet_NaN();
}

void CTOC::printLoggingBlocksInitialized() {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);

    std::cout << m_lstLoggingBlocks.size() << " logging blocks found on quadcopter:" << std::endl;
	for (std::list<struct LoggingBlock>::iterator itBlock = m_lstLoggingBlocks.begin();
		 itBlock != m_lstLoggingBlocks.end();
		 itBlock++) {
		struct LoggingBlock lbCurrent = *itBlock;

		std::cout << "\t" << lbCurrent.strName << std::endl;			
	}
}

bool compare_id(const struct LoggingBlock& first, const struct LoggingBlock& second) {
	return (first.nID < second.nID);
}

std::string CTOC::createActiveHeader() {
	std::string header = "#" + std::to_string(updateAmount) + ":" + "\t";
	header = header + "time" + "\t";
	std::string element = "";
	m_lstActiveLogging.clear();
	m_lstLoggingBlocks.sort(compare_id);
	bool bFound = false;
	for (std::list<struct LoggingBlock>::iterator itBlock = m_lstLoggingBlocks.begin();
		 itBlock != m_lstLoggingBlocks.end();
		 itBlock++) {
		struct LoggingBlock lbCurrent = *itBlock;

		if(lbCurrent.isActive) {
			for(int i = 0; i < lbCurrent.lstElementIDs.size(); i++) {
				int elementID = this->elementIDinBlock(lbCurrent.nID, i);
				struct TOCElement teCurrent = this->elementForID(elementID,
					bFound);
				element = teCurrent.strGroup + "." + teCurrent.strIdentifier;
				header = header + element + "\t";
				m_lstActiveLogging.push_back(element);
			}
		}			
	}
	header = header + "MicroCARTTest.stand" + "\t";
	updateAmount++;
	return header;
}

std::string CTOC::activeLogName(int index) {
	return m_lstActiveLogging.at(index);
}

int CTOC::sizeOfActiveList() {
	return m_lstActiveLogging.size();
}

struct LoggingBlock CTOC::loggingBlockForName(std::string strName,
		bool& bFound) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	for (std::list<struct LoggingBlock>::iterator itBlock =
			m_lstLoggingBlocks.begin(); itBlock != m_lstLoggingBlocks.end();
			itBlock++) {
		struct LoggingBlock lbCurrent = *itBlock;

		if (strName == lbCurrent.strName) {
			bFound = true;
			return lbCurrent;
		}
	}

	bFound = false;
	struct LoggingBlock lbEmpty;

	return lbEmpty;
}

struct LoggingBlock CTOC::loggingBlockForID(int nID, bool& bFound) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__);
	for(std::list<struct LoggingBlock>::iterator itBlock = m_lstLoggingBlocks.begin(); itBlock != m_lstLoggingBlocks.end(); itBlock++) {
		struct LoggingBlock lbCurrent = *itBlock;

		if (nID == lbCurrent.nID) {
			bFound = true;
			return lbCurrent;
		}
		
	}

	bFound = false;
	struct LoggingBlock lbEmpty;
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__);
	return lbEmpty;
}

bool CTOC::registerLoggingBlock(std::string strName, double dFrequency, uint16_t id) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	int nID = id;
	bool bFound;

	if (dFrequency > 0) { // Only do it if a valid frequency > 0 is given
		this->loggingBlockForName(strName, bFound);
		if (bFound) {
			this->unregisterLoggingBlock(strName);
		}

		// do {
		// 	this->loggingBlockForID(nID, bFound);

		// 	if (bFound) {
		// 		nID++;
		// 	}
		// } while (bFound);

		this->unregisterLoggingBlockID(nID);

		uint8_t logPeriod= (uint8_t) ( (1 / dFrequency) * 1000 );
		char cPayload[4] = { 0x06, (char)nID }; // JRB: A little concerned about this cast from double to char
		CCRTPPacket* crtpRegisterBlock = new CCRTPPacket(cPayload, 2, 1);
		crtpRegisterBlock->setPort(m_nPort);
		crtpRegisterBlock->setChannel(1);

		CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpRegisterBlock, m_nRadioChannel, m_crazyflie);

		delete crtpRegisterBlock;

		char* cData = crtpReceived->data();
		bool bCreateOK = false;
		if (cData[0] == 0x06 && cData[1] == nID && cData[2] == 0x00) {
			bCreateOK = true;
			std::cout << "Registered logging block `" << strName << "'"
					<< std::endl;
		} else {
			std::cout << "Unable to register block " << strName << " on the quadcopter" << std::endl;
			printf("0x%x %x %x\n", cData[0], cData[1], cData[2] );
		}

		if (crtpReceived) {
			delete crtpReceived;
		}

		if (bCreateOK) {
			struct LoggingBlock lbNew;
			lbNew.strName = strName;
			lbNew.nID = nID;
			lbNew.dFrequency = dFrequency;

			m_lstLoggingBlocks.push_back(lbNew);

			return true;
		}
	}

	return false;
}

bool CTOC::enableLogging(std::string strBlockName) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;

	struct LoggingBlock lbCurrent = this->loggingBlockForName(strBlockName,
			bFound);
	if (bFound) {
		this->setBlockActive(lbCurrent.nID, true);
		uint8_t logPeriod= (uint8_t) ( (1 / lbCurrent.dFrequency) * 1000 );
		char cPayload[3] = { 0x03, (char)lbCurrent.nID, logPeriod }; // JRB: A little concerned about this cast from double to char

		CCRTPPacket* crtpEnable = new CCRTPPacket(cPayload, 3, 1);
		crtpEnable->setPort(m_nPort);
		crtpEnable->setChannel(1);

		CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpEnable, m_nRadioChannel, m_crazyflie);
		delete crtpEnable;
		delete crtpReceived;

		return true;
	}

	return false;
}

bool CTOC::disableLogging(std::string strBlockName) {
	if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;

	struct LoggingBlock lbCurrent = this->loggingBlockForName(strBlockName,
			bFound);

	if (bFound) {
		this->setBlockActive(lbCurrent.nID, false);
		char cPayload[2] = { 0x04, (char)lbCurrent.nID }; // JRB: A little concerned about this cast from double to char

		CCRTPPacket* crtpEnable = new CCRTPPacket(cPayload, 2, 1);
		crtpEnable->setPort(m_nPort);
		crtpEnable->setChannel(1);

		CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpEnable, m_nRadioChannel, m_crazyflie);

		delete crtpEnable;
		delete crtpReceived;

		return true;
	}

	return false;
}

bool CTOC::unregisterLoggingBlock(std::string strName) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;

	struct LoggingBlock lbCurrent = this->loggingBlockForName(strName, bFound);
	if (bFound) {
		return this->unregisterLoggingBlockID(lbCurrent.nID);
	}

	return false;
}

bool CTOC::unregisterLoggingBlocks() {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;

	char cPayload[1] = { 0x05 };

	CCRTPPacket* resetBlock = new CCRTPPacket(cPayload, 1, 1);
	resetBlock->setPort(0x05);
	resetBlock->setChannel(1);

	CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(resetBlock, m_nRadioChannel, m_crazyflie);

	delete resetBlock;

	if (crtpReceived) {
		delete crtpReceived;
		m_lstLoggingBlocks.clear();
		return true;
	}

	return false;
}

bool CTOC::unregisterLoggingBlockID(int nID) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	char cPayload[2] = { 0x02, (char)nID };

	CCRTPPacket* crtpUnregisterBlock = new CCRTPPacket(cPayload, 2, 1);
	crtpUnregisterBlock->setPort(m_nPort);
	crtpUnregisterBlock->setChannel(1);

	CCRTPPacket* crtpReceived = m_crRadio->sendAndReceive(crtpUnregisterBlock, m_nRadioChannel, m_crazyflie);

	delete crtpUnregisterBlock;

	if (crtpReceived) {
		delete crtpReceived;
		for(std::list<struct LoggingBlock>::iterator itBlock = m_lstLoggingBlocks.begin(); itBlock != m_lstLoggingBlocks.end(); itBlock++) {
			struct LoggingBlock lbCur = *itBlock;
			if (lbCur.nID == nID) {
				itBlock = m_lstLoggingBlocks.erase(itBlock);
			}
		}
		return true;
	}

	return false;
}

//void CTOC::processPackets(std::list<CCRTPPacket*> lstPackets) {
void CTOC::processPackets(CCRTPPacket *crtpPacket) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__ );
//	if (lstPackets.size() > 0) {
//		for (std::list<CCRTPPacket*>::iterator itPacket = lstPackets.begin();
//				itPacket != lstPackets.end(); itPacket++) {
//			CCRTPPacket* crtpPacket = *itPacket;

	char* cData = crtpPacket->data();
	float fValue;
	memcpy(&fValue, &cData[4], 4);

	char* cLogdata = &cData[4];
	int nOffset = 0;
	int nIndex = 0;
	//int nAvailableLogBytes = crtpPacket->dataLength() - 5;  -- unused variable, commented out

	int nBlockID = cData[0];
	bool bFound;

	struct LoggingBlock lbCurrent = this->loggingBlockForID(nBlockID,
			bFound);

	if (bFound) {
		while (nIndex < (int)lbCurrent.lstElementIDs.size()) {
			int nElementID = this->elementIDinBlock(nBlockID, nIndex);
			bool bFound;
			struct TOCElement teCurrent = this->elementForID(nElementID,
					bFound);

			if (bFound) {
				int nByteLength = 0;

				// NOTE(winkler): We just copy over the incoming bytes in
				// their according data structures and afterwards assign
				// the value to fValue. This way, we let the compiler to
				// the magic of conversion.
				float fValue = 0;

				switch (teCurrent.nType) {
				case 1: { // UINT8
					nByteLength = 1;
					uint8_t uint8Value;
					memcpy(&uint8Value, &cLogdata[nOffset],
							nByteLength);
					fValue = uint8Value;
				}
					break;

				case 2: { // UINT16
					nByteLength = 2;
					uint16_t uint16Value;
					memcpy(&uint16Value, &cLogdata[nOffset],
							nByteLength);
					fValue = uint16Value;
				}
					break;

				case 3: { // UINT32
					nByteLength = 4;
					uint32_t uint32Value;
					memcpy(&uint32Value, &cLogdata[nOffset],
							nByteLength);
					fValue = uint32Value;
				}
					break;

				case 4: { // INT8
					nByteLength = 1;
					int8_t int8Value;
					memcpy(&int8Value, &cLogdata[nOffset], nByteLength);
					fValue = int8Value;
				}
					break;

				case 5: { // INT16
					nByteLength = 2;
					int16_t int16Value;
					memcpy(&int16Value, &cLogdata[nOffset],
							nByteLength);
					fValue = int16Value;
				}
					break;

				case 6: { // INT32
					nByteLength = 4;
					int32_t int32Value;
					memcpy(&int32Value, &cLogdata[nOffset],
							nByteLength);
					fValue = int32Value;
				}
					break;

				case 7: { // FLOAT
					nByteLength = 4;
					memcpy(&fValue, &cLogdata[nOffset], nByteLength);
				}
					break;

				case 8: { // FP16
					// NOTE(winkler): This is untested code (as no FP16
					// variable gets advertised yet). This has to be tested
					// and is to be used carefully. I will do that as soon
					// as I find time for it.
					nByteLength = 2;
					char cBuffer1[nByteLength];
					char cBuffer2[4];
					memcpy(cBuffer1, &cLogdata[nOffset], nByteLength);
					cBuffer2[0] = cBuffer1[0] & 0b10000000; // Get the sign bit
					cBuffer2[1] = 0;
					cBuffer2[2] = cBuffer1[0] & 0b01111111; // Get the magnitude
					cBuffer2[3] = cBuffer1[1];
					memcpy(&fValue, cBuffer2, 4); // Put it into the float variable
				}
					break;

				default: { // Unknown. This hopefully never happens.
				}
					break;
				}

				this->setFloatValueForElementID(nElementID, fValue);
				this->setLastUpdateForElementID(nElementID);
				nOffset += nByteLength;
				nIndex++;
			} else {
				std::cerr << "Didn't find element ID " << nElementID
						<< " in block ID " << nBlockID
						<< " while parsing incoming logging data."
						<< std::endl;
				std::cerr << "This REALLY shouldn't be happening!"
						<< std::endl;
				std::exit(-1);
			}
		}
	}

//			delete crtpPacket;
//		}
//	}
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__ );
}

int CTOC::elementIDinBlock(int nBlockID, int nElementIndex) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool bFound;

	struct LoggingBlock lbCurrent = this->loggingBlockForID(nBlockID, bFound);
	if (bFound) {
		if (nElementIndex < (int) lbCurrent.lstElementIDs.size()) {
			std::list<int>::iterator itID = lbCurrent.lstElementIDs.begin();
			advance(itID, nElementIndex);
			return *itID;
		}
	}

	return -1;
}

bool CTOC::setFloatValueForElementID(int nElementID, float fValue) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	int nIndex = 0;
	for (std::list<struct TOCElement>::iterator itElement =
			m_lstTOCElements.begin(); itElement != m_lstTOCElements.end();
			itElement++, nIndex++) {
		struct TOCElement teCurrent = *itElement;

		if (teCurrent.nID == nElementID) {
			teCurrent.dValue = fValue; // We store floats as doubles
			(*itElement) = teCurrent;
			// std::cout << fValue << std::endl;
			return true;
		}
	}

	return false;
}

bool CTOC::setLastUpdateForElementID(int nElementID) {
    if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	int nIndex = 0;
	for (std::list<struct TOCElement>::iterator itElement =
			m_lstTOCElements.begin(); itElement != m_lstTOCElements.end();
			itElement++, nIndex++) {
		struct TOCElement teCurrent = *itElement;

		if (teCurrent.nID == nElementID) {
			teCurrent.timeLastUpdated = m_crazyflie->currentTime(); // We store floats as doubles
			(*itElement) = teCurrent;
			// std::cout << fValue << std::endl;
			return true;
		}
	}

	return false;
}

/*
 * Open a log file for the quadcopter
 *
 * @param baseFileName The base filename for the logfile
 */
void CTOC::openParamTOCFile(char *baseFileName) {
	char logFileName[40];

	char timeString[40];
	time_t rawtime;
	time( &rawtime );
	struct tm *timeinfo = localtime( &rawtime );
	strftime(timeString, 40, "%Y_%m_%d_%H:%M:%S", timeinfo);

	// Create the log file name (including date)
	sprintf(logFileName, "%s_%s.txt", baseFileName, timeString);
	this->paramfilename = logFileName;

	printf("Opening param toc file %s  for quadcopter %d...", logFileName, (this->m_crazyflie->getQuadcopterNumber() + 1));

	try {
		char dir[256];
		dir[0] = '/';
		getcwd(&dir[1], 255);
		std::string fullFilePath(dir);
		fullFilePath += "/";
		fullFilePath += logFileName;
		this->paramfilename = fullFilePath;
		param_toc_file.open(logFileName, std::ios_base::out);
	} catch (...) {
		std::cout << "Error opening logfile for quadcopter " << this->m_crazyflie->getQuadcopterNumber();
		exit(-1);
	}

	// Place the header information into the log file
	param_toc_file << "#Crazyflie" << std::endl;

	// Add the controller type to the logfile
	param_toc_file << param_header << std::endl;
	std::cout << " Complete" << std::endl;
}

/*
 * Open a log file for the quadcopter
 *
 * @param baseFileName The base filename for the logfile
 */
void CTOC::openLogTOCFile(char *baseFileName) {
	char logFileName[40];

	char timeString[40];
	time_t rawtime;
	time( &rawtime );
	struct tm *timeinfo = localtime( &rawtime );
	strftime(timeString, 40, "%Y_%m_%d_%H:%M:%S", timeinfo);

	// Create the log file name (including date)
	sprintf(logFileName, "%s_%s.txt", baseFileName, timeString);

	printf("Opening param toc file %s  for quadcopter %d...", logFileName, (this->m_crazyflie->getQuadcopterNumber() + 1));

	try {
		char dir[256];
		dir[0] = '/';
		getcwd(&dir[1], 255);
		std::string fullFilePath(dir);
		fullFilePath += "/";
		fullFilePath += logFileName;
		this->logidfilename = fullFilePath;
		log_toc_file.open(logFileName, std::ios_base::out);
	} catch (...) {
		std::cout << "Error opening logfile for quadcopter " << this->m_crazyflie->getQuadcopterNumber();
		exit(-1);
	}

	// Place the header information into the log file
	log_toc_file << "#Crazyflie" << std::endl;

	// Add the controller type to the logfile
	log_toc_file << log_header << std::endl;
	std::cout << " Complete" << std::endl;
}

std::string CTOC::getLogTOCFile() {
	if(m_nPort == 0x02) {
		return "";
	}
	else {
		return this->logidfilename;
	}
}

std::string CTOC::getParamTOCFile() {
	if(m_nPort == 0x05) {
		return "";
	}
	else {
		return this->paramfilename;
	}
}

void CTOC::setBlockActive(int nID, bool activity) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__);
	for(std::list<struct LoggingBlock>::iterator itBlock = m_lstLoggingBlocks.begin(); itBlock != m_lstLoggingBlocks.end(); itBlock++) {
		struct LoggingBlock &lbCurrent = *itBlock;
		if (nID == lbCurrent.nID) {
			lbCurrent.isActive = activity;
			return;
		}
	}
}
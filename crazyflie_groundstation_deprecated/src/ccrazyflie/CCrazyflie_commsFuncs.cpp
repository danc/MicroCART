#include "CCrazyflie.h"

/**
 * Query the type of controller that the Crazyflie is using
 *
 * @return Controller type
 */
int CCrazyflie::queryControllerType() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	// Create just random data to send (Unsure how the packet behaves if there is no data)
	int nSize = 1;
	char cBuffer[1];
	int controllerId = this->m_tocParameters->idForName("CONTROLLER");
	cBuffer[0] = controllerId;

	// Controller update is on port 8
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_PARAM);
	crtpPacket->setChannel(0x01);		// Query is on channel 0

	// Send the packet and wait for a response
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	// Parse the response
	int dataLength = crtpReceived->dataLength();
	
	int retVal = -1;
	if (dataLength >= 1) {
		// Parse the one data byte to get controller type
		char *cData = crtpReceived->data();
		controllerType = cData[0];
		retVal = controllerType;
	} else {
		// This isn't good...
		retVal = -1;
	}

	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return( retVal );
}


bool CCrazyflie::sendSGSF_Gains( CONTROLLER_SGSF_GAINS gains) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	if (this->getControllerType() != 4) {
		std::cout << "SGSF not enabled, ignoring constants" << std::endl;
		return false;
	}

	int nSize = 1 + sizeof(CONTROLLER_SGSF_GAINS);
	char cBuffer[nSize];
	cBuffer[0] = CONT_SGSF_UPDATE_GAINS;
	memcpy(&(cBuffer[1]), &gains, sizeof(CONTROLLER_SGSF_GAINS) );

	printf( "sending SGSF Gains (%d, %5.3f, %5.3f, %5.3f, %4.1f)\n",
			gains.state, gains.ut, gains.ua, gains.ue, gains.ur);

	// The PID update is on port 8
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_PARAM);
	crtpPacket->setChannel(0x02);		// The PID update is on channel 1
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	bool retVal = false;
	if (crtpReceived != NULL) {
		retVal = true;
	} else {
		retVal = false;
	}


	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return( retVal );
}

bool CCrazyflie::sendPID_Constants( PID_ControllerParams params, std::string str ) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	if (this->getControllerType() != 1) {
		std::cout << "PID not enabled, ignoring constants" << std::endl;
		return false;
	}

	int nSize = sizeof(float) + 1;
	char cBuffer[nSize];
	/*
	memcpy(&cBuffer[0 * sizeof(float)], &params.Kp, sizeof(float));
	memcpy(&cBuffer[1 * sizeof(float)], &params.Ki, sizeof(float));
	memcpy(&cBuffer[2 * sizeof(float)], &params.Kd, sizeof(float));
	memcpy(&cBuffer[3 * sizeof(float)], &params.iLimit, sizeof(float));
	memcpy(&cBuffer[4 * sizeof(float)], &params.controllerID, sizeof(uint8_t));
	*/

	printf( "sending PID Values (%d, %5.3f, %5.3f, %5.3f, %4.1f)\n",
			params.controllerID, params.Kp, params.Ki, params.Kd, params.iLimit);

	// The PID update is on port 8
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_PARAM);
	crtpPacket->setChannel(0x02);		// The PID update is on channel 1
	CCRTPPacket *crtpReceived;

	std::string name = "";
	int id = -1;

	for(int i = 0; i < 3; i++) {
		name = str;
		switch(i) {
			case 0:
				if(str.compare("X") == 0 || str.compare("Y") == 0 || str.compare("Z") == 0) {
					name = str.append("p");
				}
				else {
					name = str.append("P");
				}
				id = this->m_tocParameters->idForName(name);
				cBuffer[0] = id;
				memcpy(&cBuffer[1 * sizeof(float)], &params.Kp, sizeof(float));
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			case 1:
				if(str.compare("X") == 0 || str.compare("Y") == 0 || str.compare("Z") == 0) {
					name = str.append("i");
				}
				else {
					name = str.append("I");
				}
				id = this->m_tocParameters->idForName(name);
				cBuffer[0] = id;
				memcpy(&cBuffer[1 * sizeof(float)], &params.Kd, sizeof(float));
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			case 2:
				if(str.compare("X") == 0 || str.compare("Y") == 0 || str.compare("Z") == 0) {
					name = str.append("d");
				}
				else {
					name = str.append("D");
				}
				id = this->m_tocParameters->idForName(name);
				cBuffer[0] = id;
				memcpy(&cBuffer[1 * sizeof(float)], &params.Ki, sizeof(float));
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			case 3:
				name = str.append("iLimit");
				id = this->m_tocParameters->idForName(name);
				cBuffer[0] = id;
				memcpy(&cBuffer[1 * sizeof(float)], &params.iLimit, sizeof(float));
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			default:
				break;
		}
	}

	bool retVal = false;
	if (crtpReceived != NULL) {
		retVal = true;
	} else {
		retVal = false;
	}


	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return( retVal );
}

/**
 * Modify if the thrust controller is active or not.
 *
 * @param enabled True to enable controller, false to disable
 * @return True if packet received successfully
 */
bool CCrazyflie::modifyThrustController(bool enabled) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	// Create just random data to send (Unsure how the packet behaves if there is no data)
	int nSize = 1;
	char cBuffer[1];
	if (enabled) {
		cBuffer[0] = 2;
	} else {
		cBuffer[0] = 1;
	}

	// Controller update is on port 8
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_CONTROLLER_UPDATE);
	crtpPacket->setChannel(CONTROLLER_SWITCH_THROTTLE_CTRL);		// Query is on channel 0

	// Send the packet and wait for a response
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	// See if something was received
	bool retVal = false;
	if (crtpReceived != NULL) {
		retVal = true;
	} else {
		retVal = false;
	}

	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return( retVal );
}

bool CCrazyflie::sendCameraData() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	// Prepare the buffer for data
	int nSize = 4 * sizeof(float);
	char cBuffer[nSize];

	float xPosition = this->getPositionX(this->m_systemToUse);
	float yPosition = this->getPositionY(this->m_systemToUse);
	float zPosition = this->getPositionZ(this->m_systemToUse);

	// Copy the data into a send buffer
	memcpy(&cBuffer[0 * sizeof(float)], &(xPosition), sizeof(float));
	memcpy(&cBuffer[1 * sizeof(float)], &(yPosition), sizeof(float));
	memcpy(&cBuffer[2 * sizeof(float)], &(zPosition), sizeof(float));
	memcpy(&cBuffer[3 * sizeof(float)], &(this->m_currentCameraAngle.yaw), sizeof(float));

	if( ALL_THE_DEBUG ) printf( "sending position (%5.3f, %5.3f, %5.3f, %4.1f)\n",
								xPosition, yPosition, zPosition, this->m_currentCameraAngle.yaw);


	// The XYZ current position is on port 7
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_XYZ_POS);
	CCRTPPacket *crtpReceived = m_crRadio->sendPacket(m_nRadioChannel, crtpPacket, this);

	delete crtpPacket;
	if (crtpReceived != NULL) {
		delete crtpReceived;
		if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
		return true;
	} else {
		if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
		return false;
	}
}


bool CCrazyflie::sendPositionSetpoint() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	uint8_t resetValue;
	if (this->m_resetController) {
		resetValue = 1;
	} else {
		resetValue = 0;
	}

	float xSetpoint = this->getSetpointX(this->m_systemToUse);
	float ySetpoint = this->getSetpointY(this->m_systemToUse);
	float zSetpoint = this->getSetpointZ(this->m_systemToUse);

	int nSize = 4 * sizeof(float) + 1*sizeof(uint16_t) + 1*sizeof(uint8_t);
	char cBuffer[nSize];
		
	memcpy(&cBuffer[0 * sizeof(float)], &(xSetpoint), sizeof(float));
	memcpy(&cBuffer[1 * sizeof(float)], &(ySetpoint), sizeof(float));
	memcpy(&cBuffer[2 * sizeof(float)], &(zSetpoint), sizeof(float));
	memcpy(&cBuffer[3 * sizeof(float)], &(this->m_desiredYaw), sizeof(float));
	memcpy(&cBuffer[4 * sizeof(float)], &(this->m_baseThrust), sizeof(uint16_t));
	memcpy(&cBuffer[4 * sizeof(float) + sizeof(uint16_t)], &resetValue, sizeof(uint8_t));

	if( ALL_THE_DEBUG ) printf( "sending setpoint (%5.3f, %5.3f, %5.3f, %4.1f)\n",
								xSetpoint, ySetpoint, zSetpoint, this->m_desiredYaw);



	// The XYZ setpoint is on port 6
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_XYZ_SET);
	CCRTPPacket *crtpReceived = m_crRadio->sendPacket(m_nRadioChannel, crtpPacket, this);

	delete crtpPacket;
	if (crtpReceived != NULL) {
		delete crtpReceived;
		if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
		return true;
	} else {
		if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
		return false;
	}
}
/*
 * This file contains functions for interacting with the controller
 * on the Crazyflie quadcopter. These are part of the CCrazyflie class.
 */
#include "CCrazyflie.h"
#include <ostream>


/*
 * This function will cycle through the takeoff profile for the Crazyflie.
 */
void CCrazyflie::takeoffCycle() {
	// The current time
	double time = this->currentTime();

	// Make sure the takeoff profile is initialized
	if (this->takeoffProfile == NULL) {
		// The takeoff profile hasn't been initialized, return and don't do anything
		return;
	}

	// Check if the next step uses time or altitude
	switch ( this->takeoffProfile[this->currentStep].type ) {
		case step_time:
			// The next step is time based
			if ( abs( time - this->lastStepTime ) > this->takeoffProfile[this->currentStep].condition ) {
				// The step has ended, move to the next one
				this->currentStep++;
				if (this->currentStep > this->numTakeoffSteps) {
					this->currentStep = this->numTakeoffSteps;
				}
			}
			break;
		case step_height:
			// The next step is height based
			if ( this->getPositionZ( coor_global ) < this->takeoffProfile[this->currentStep].condition ) {
				// The step has ended, move to the next one
				this->currentStep++;
				if (this->currentStep > this->numTakeoffSteps) {
					this->currentStep = this->numTakeoffSteps;
				}
			}
			break;
		case step_final:
		default:
		// Renable the Z controller
		this->enableThrustController();

		// If it is the final step, then put it into hover mode
		this->setFlightMode(HOVER_MODE);

		std::cout << "Takeoff complete" << std::endl;
	}

	// Set the base thrust
	this->setBaseThrust( this->takeoffProfile[this->currentStep].thrust );
}

/**
 * Enable the thrust controller.
 */
bool CCrazyflie::enableThrustController() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	
	std::cout << "Enabling thrust controller" << std::endl;
	bool retVal =  this->modifyThrustController(true);

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return( retVal );
}

/**
 * Reset the controller on the quadcopter
 */
void CCrazyflie::resetController() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	this->m_resetController = true;

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

/**
 * Disable the thrust controller.
 */
bool CCrazyflie::disableThrustController() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	
	std::cout << "Disabling thrust controller" << std::endl;
	bool retVal =  this->modifyThrustController(false);

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return( retVal );
}


void CCrazyflie::setPID_Constants( PID_ControllerParams params ) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	// Add the PID parameters to the buffer to send
	pidToSend.push(params);

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

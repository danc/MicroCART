/**
 * This file contains the functions associated with the logging and parameter tables
 * of contents for the CCrazyflie class.
 *
 */
#include "CCrazyflie.h"
#include "CTOC.h"

#include <time.h>
#include <iostream>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <limits>
#include <filesystem>
#include <stdexcept>

// Various headers for the files
static const char *variables = "%Time\t\tMarker\t\tMotor_1\t\t"
							   "Motor_2\t\tMotor_3\t\t"
							   "Motor_4\t\tPitch\t\t"
							   "Pitch_err\t\tPitch_pid_p\t\t"
							   "Pitch_pid_d\t\t"
							   "Roll\t\tRoll_err\t\t"
							   "Roll_pid_p\t\tRoll_pid_d\t\t";

static const char *units = "&sec\t\tMarker\t\t%thrust\t\t"
						   "%thrust\t\t%thrust\t\t"
						   "%thrust\t\tdegrees\t\t"
						   "degrees\t\tdegrees\t\t"
						   "degrees\t\t"
						   "degrees\t\tdegrees\t\t"
						   "degrees\t\tdegrees\t\t";

/*
 * Open a log file for the quadcopter
 *
 * @param baseFileName The base filename for the logfile
 */
void CCrazyflie::openLogFile(char *baseFileName) {
	char logFileName[40];

	char timeString[40];
	time_t rawtime;
	time( &rawtime );
	struct tm *timeinfo = localtime( &rawtime );
	strftime(timeString, 40, "%Y_%m_%d_%H:%M:%S", timeinfo);

	// Create the log file name (including date)
	sprintf(logFileName, "%s_%s.txt", baseFileName, timeString);

	printf("Opening log file %s  for quadcopter %d...", logFileName, (m_quadNum + 1));

	try {
		char dir[256];
		dir[0] = '/';
		getcwd(&dir[1], 255);
		std::string fullFilePath(dir);
		fullFilePath += "/";
		fullFilePath += logFileName;
		this->logfilename = fullFilePath;
		file_log.open(logFileName, std::ios_base::out);
	} catch (...) {
		std::cout << "Error opening logfile for quadcopter " << m_quadNum;
		exit(-1);
	}

	// Place the header information into the log file
	file_log << "#Crazyflie" << std::endl;

	// Add the controller type to the logfile
	file_log << "#" << this->getControllerTypeString();
	//file_log << variables << std::endl;
	//file_log << units << std::endl;

	std::cout << " Complete" << std::endl;
}

/*
 * Write data to the log file
 */
void CCrazyflie::writeLogData() {
	/* Make a new line
	file_log << std::endl;

	// Print the time
	file_log << this->currentTime();

	// Print the marker and motors
	file_log << "\t\t" << 0;

	file_log << "\t\t" << this->sensorDoubleValue("pwm.m1_pwm");
	file_log << "\t\t" << this->sensorDoubleValue("pwm.m2_pwm");
	file_log << "\t\t" << this->sensorDoubleValue("pwm.m3_pwm");
	file_log << "\t\t" << this->sensorDoubleValue("pwm.m4_pwm");

	// Print pitch and roll data
	float pitch = this->getQuadPitch();
	file_log << "\t\t" << pitch;
	file_log << "\t\t" << -pitch;

	file_log << "\t\t" << this->sensorDoubleValue("pid_attitude.pitch_outP");
	file_log << "\t\t" << this->sensorDoubleValue("pid_attitude.pitch_outD");

	float roll = this->getQuadRoll();
	file_log << "\t\t" << roll;
	file_log << "\t\t" << -roll;

	file_log << "\t\t" << this->sensorDoubleValue("pid_attitude.roll_outP");
	file_log << "\t\t" << this->sensorDoubleValue("pid_attitude.roll_outD");
	file_log << "\t\t" << this->sensorDoubleValue("ctrlStdnt.rollRate");

	file_log << "\t\t" << this->sensorDoubleValue("testStand");
	*/
	bool bFound;
	TOCElement response;
	file_log << std::endl;
	double currentTime = this->currentTime();
	file_log << currentTime;
	for(int i = 0; i < m_tocLogs->sizeOfActiveList(); i++) {
		response = m_tocLogs->elementForName(m_tocLogs->activeLogName(i), bFound);
		if(response.timeLastUpdated > currentTime - 0.01) {
			file_log << "\t" << this->sensorDoubleValue(m_tocLogs->activeLogName(i));
		}
		else {
			file_log << "\t" << std::numeric_limits<double>::quiet_NaN();
		}
	}
	file_log << "\t" << this->sensorDoubleValue("testStand");
}


bool CCrazyflie::readTOCParameters() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	printf( "      readTocParameters requestMetaData\n" );
	if (m_tocParameters->requestMetaData()) {
		printf( "      requestItems\n" );
		if (m_tocParameters->requestItems()) {
			printf( "      return true\n" );
			return true;
		}
	}

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return false;
}

bool CCrazyflie::readTOCLogs() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	if (m_tocLogs->requestMetaData()) {
		if (m_tocLogs->requestItems()) {
			return true;
		}
	}

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return false;
}

void CCrazyflie::displayLoggingBlocksInitialized() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	m_tocLogs->printLoggingBlocksInitialized();
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

double CCrazyflie::sensorDoubleValue(std::string strName) {
	if(!strName.compare("testStand")) {
		if(this->test_stand != NULL) {
			return this->testStandData;
		}
		else {
			return std::numeric_limits<double>::quiet_NaN();
		}
	}
	return m_tocLogs->doubleValue(strName);
}

bool CCrazyflie::addLoggingBlock(const char *name, uint16_t frequency, uint16_t id) {
	bool retval = m_tocLogs->registerLoggingBlock(name, frequency, id);
	return(retval);
}

bool CCrazyflie::resetLoggingBlocks() {
	if( ALL_THE_DEBUG ) printf( "%s\n", __FUNCTION__);
	bool result = m_tocLogs->unregisterLoggingBlocks();
	return result;
}

void CCrazyflie::removeLoggingBlock(const char *name) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	m_tocLogs->unregisterLoggingBlock(name);

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

bool CCrazyflie::addLoggingEntry(const char *blockName, const char *entryName) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	bool retval = m_tocLogs->startLogging(entryName, blockName);

	if (!retval) {
		std::cout << "Error starting logging entry " << entryName << " in block " << blockName << std::endl;
	}

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return ( retval );
}

bool CCrazyflie::setTestStand(serial::Serial * ptr) {
	this->test_stand = ptr;
	return true;
}

double CCrazyflie::readTestStand() {
	if(this->test_stand == NULL) {
		return std::numeric_limits<double>::quiet_NaN();
	}
	/*
	uint8_t buffer[8];
	int nlNum = 0;
	std::string result = "";
	this->test_stand->read(buffer, 8);
	for(int i = 0; i < 8; i++) {
		if(buffer[i] != '\n' && buffer[i] != ',') {
			result += buffer[i];
		}
		else if(buffer[i] == '\n') {
			nlNum++;
		}
	}
	if(nlNum  == 8) {
		return -1;
	}
	int find = result.find(".");
	int maxOut = 0;
	if(find == std::string::npos || find > result.length() - 3) {
		while(find == std::string::npos || find > result.length() - 3) {
			this->test_stand->read(buffer, 1);
			if(buffer[0] != ',' && buffer[0] != '\n') {
				result += buffer[0];
			}
			else {
				maxOut++;
			}
			if(maxOut == 8) {
				return -2;
			}
		}
	}
	*/
	std::string result = "";
	
	
	//usleep(5);
	// this->test_stand->readline();
	// result = this->test_stand->readline();
	// this->test_stand->flushInput();
	result = this->testStand.getCurrentValue();

	double value;
	try {
		value = std::stod(result);
	}
	catch(std::invalid_argument&) {
		return -3;
	}
	catch(std::out_of_range&) {
		return -3;
	}
	this->testStandData = value;
	return value;
}

std::string CCrazyflie::getlogfile() {
	return this->logfilename;
}

bool CCrazyflie::enableLogging(std::string strBlockName) {
	return m_tocLogs->enableLogging(strBlockName);
}

bool CCrazyflie::disableLogging(std::string strBlockName) {
	return m_tocLogs->disableLogging(strBlockName);
}

/*
START BLOCK
ID
Name
Frequency
Entries
END BLOCK
*/
bool CCrazyflie::loadLoggingBlocksFromFile(std::string blockFileName) {
	std::ifstream blockFile;
	blockFile.open(blockFileName);
	std::string line = "";
	//0 if not readingBlock 1 is START BLOCK and so on
	int blockLinesRead = 0;
	int curId = -1;
	uint16_t curFreq;
	std::string curName;
	std::string curEntryName;

	while(getline(blockFile, line)) {
		if(!line.compare("START BLOCK")) {
			blockLinesRead = 1;
			continue;
		}
		else if(!line.compare("END BLOCK")) {
			enableLogging(curName);
			blockLinesRead = 0;
			curId = -1;
			curFreq = 0;
			curName = "";
			curEntryName = "";
			continue;
		}
		else if(blockLinesRead == 1) {
			try {
				curId = std::stoi(line);
				bool found = false;
				m_tocLogs->loggingBlockForID(curId, found);
				if(found) {
					blockLinesRead = 0;
					curId = -1;
					curFreq = 0;
					curName = "";
					curEntryName = "";
					continue;
				}
			}
			catch(std::invalid_argument& e) {
				std::cout << "Error: invalid ID. Skipping Block" << std::endl;
				blockLinesRead = 0;
				continue;
			}
			blockLinesRead++;
			continue;
		}
		else if(blockLinesRead == 2) {
			curName = line;
			blockLinesRead++;
			continue;
		}
		else if(blockLinesRead == 3) {
			try {
				curFreq = std::stoi(line);
			}
			catch(std::invalid_argument& e) {
				std::cout << "Error: invalid frequency. Skipping Block" << std::endl;
				blockLinesRead = 0;
				continue;
			}
			blockLinesRead++;
			addLoggingBlock(curName.c_str(), curFreq, curId);
			continue;
		}
		else if(blockLinesRead > 3) {
			curEntryName = line;
			addLoggingEntry(curName.c_str(), curEntryName.c_str());
			blockLinesRead++;
			continue;
		}
		else {
			continue;
		}
	}
	blockFile.close();
	return true;
}

void CCrazyflie::printUpdatedHeader() {
	file_log << std::endl;
	this->activeHeader =  m_tocLogs->createActiveHeader(); 
	file_log << this->activeHeader;
	
}
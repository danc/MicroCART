#include <chrono>

#include "CCrazyflie.h"
#include "structures.h"
#include "userInput.h"

#include "quadcopterData.h"
#include "crazyflieGroundStation.h"

float build_float(uint8_t* buf);

/**
 * Pass a command from the user into this Crazyflie
 *
 * @param command The command to pass in
 */
void CCrazyflie::passUserCommand(userCommands_t command) {
	pthread_mutex_lock( &(this->userCommandsMutex) );

	userCommands.push(command);

	pthread_mutex_unlock( &(this->userCommandsMutex) );
}

/**
 * Parse the commands that are sitting in the queue.
 * Each call will only parse one command.
 *
 * @return True if command parsed, false otherwise
 */
bool CCrazyflie::parseReceivedUserCommand() {
	// Get the mutex
	pthread_mutex_lock( &(userCommandsMutex) );

	// If there are no commands, then return
	if (userCommands.size() == 0) {
		pthread_mutex_unlock( &(userCommandsMutex) );
		return (false);
	}

	// Get the next command from the queue and release the mutex
	userCommands_t command = userCommands.front();
	userCommands.pop();
	pthread_mutex_unlock( &(userCommandsMutex) );


	// Parse the command
	switch ( command.type ) {
	case CMD_KILL:
		this->setFlightMode( GROUNDED_MODE );

		std::cout << "Crazyflie " << (m_quadNum+1) << " Killed" << std::endl;
		break;

	case CMD_TAKEOFF:
		this->setCoordinateSystem( coor_global );

		// Configure the setpoints
		this->setSetpointX( this->getPositionX( coor_global ), coor_global );
		this->setSetpointY( this->getPositionY( coor_global ), coor_global );
		this->setSetpointZ( defaultSetpoints[m_quadNum-1][2], coor_global );

		// Get the quad ready to takeoff
		this->setTakeoffProfile( quad1_takeoffSteps, quad1_numTakeoffSteps );
		this->disableThrustController();
		this->resetController();

		// Actually takeoff
		this->setFlightMode( TAKEOFF_MODE );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Taking off" << std::endl;
		break;

	case CMD_GOTO_DEFAULT:
		this->setSetpointX( defaultSetpoints[m_quadNum-1][0], coor_global );
		this->setSetpointY( defaultSetpoints[m_quadNum-1][1], coor_global );
		this->setSetpointZ( defaultSetpoints[m_quadNum-1][2], coor_global );

		std::cout << "Crazyflie " << (m_quadNum+1) << " Going to default coordinates" << std::endl;
		break;

	case CMD_LAND:
		this->setFlightMode( LANDING_MODE );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Landing" << std::endl;
		break;

	case CMD_SET_X:
		//this->setSetpointX( command.payload, this->m_systemToUse );
		std::cout << "Crazyflie " << (m_quadNum+1) << " X Setpoint = " << command.payload << std::endl;
		break;

	case CMD_SET_Y:
		//this->setSetpointY( command.payload, this->m_systemToUse );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Y Setpoint = " << command.payload << std::endl;
		break;

	case CMD_SET_Z:
		//this->setSetpointZ( command.payload, this->m_systemToUse );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Z Setpoint = " << command.payload << std::endl;
		break;

	case CMD_SET_YAW:
		//this->setSetpointYaw( command.payload );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Yaw Setpoint = " << command.payload << std::endl;
		break;

	case CMD_ALL_MOTORS:
		/*this->setSetpointX( command.payload, coor_global );
		this->setSetpointY( command.payload, coor_global );
		this->setSetpointZ( command.payload, coor_global );
		this->setSetpointYaw( command.payload );
		*/
		std::cout << "Crazyflie " << (m_quadNum+1) << " Setting motors to = " << command.payload << std::endl;
		break;

	case CMD_BRIDGE_HOVER:
		// Determine where on the bridge to land
		lateralPosition bridgePos;
		/*
		if ( bridge->getInterpolatedPosition(&bridgePos, command.payload) ) {
			// The position is valid
			this->setSetpointX( bridgePos.x, coor_global);
			this->setSetpointY( bridgePos.y, coor_global);
			this->setSetpointZ( bridgePos.z - 0.5, coor_global);
			this->setFlightMode( BRIDGE_HOVER_MODE );
			std::cout << "Crazyflie " << (m_quadNum+1) << " Hover over bridge" << std::endl;
		} else {
			std::cout << "Bridge is not valid" << std::endl;
		}
		*/
		break;

	case CMD_BRIDGE_LAND:
		// Land on the bridge by just changing the Z setpoint
		this->setSetpointZ( this->getSetpointZ(this->m_systemToUse) + 0.7, this->m_systemToUse );
		this->setFlightMode(  BRIDGE_LANDING_MODE );

		std::cout << "Crazyflie " << (m_quadNum+1) << " Land on bridge" << std::endl;
		break;

	case CMD_BRIDGE_TAKEOFF:
		this->setFlightMode(  BRIDGE_TAKEOFF_MODE );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Take off from bridge" << std::endl;
		break;
	
	case CMD_TOGGLE_COOR:
		// Toggle the coordinate system
		if (this->m_systemToUse == coor_local) {
			this->m_systemToUse = coor_global;

			// Remap the setpoints so there is no jump
			this->setSetpointX( this->getSetpointX(coor_local) + this->m_localOrigin.x , coor_global);
			this->setSetpointY( this->getSetpointY(coor_local) + this->m_localOrigin.y , coor_global);
			this->setSetpointZ( this->getSetpointZ(coor_local) + this->m_localOrigin.z , coor_global);

			std::cout << "Crazyflie " << (m_quadNum+1) << " Using global system" << std::endl;
		} else {
			this->m_systemToUse = coor_local;

			// Remap the setpoints so there is no jump
			this->setSetpointX( this->getSetpointX(coor_global) - this->m_localOrigin.x , coor_local);
			this->setSetpointY( this->getSetpointY(coor_global) - this->m_localOrigin.y , coor_local);
			this->setSetpointZ( this->getSetpointZ(coor_global) - this->m_localOrigin.z , coor_local);


			std::cout << "Crazyflie " << (m_quadNum+1) << " Using local system" << std::endl;
		}
		break;

	case CMD_TOGGLE_COMP:
		// Toggle the computation on and off
		if (this->computationStarted) {
			this->disableComputation();
			std::cout << "Crazyflie " << (m_quadNum+1) << " Disabling computation" << std::endl;	
		} else {
			this->enableComputation();
			std::cout << "Crazyflie " << (m_quadNum+1) << " Enabling computation" << std::endl;
		}
		break;
	
	case ACMD_GETPARAM:
		{
			int nSize = 2;
			char cBuffer[nSize];

			CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_PARAM);
			crtpPacket->setChannel(0x01);		// The PID update is on channel 1
			CCRTPPacket *crtpReceived;

			//int id = (char) command.payload;
			//char id = (char) command.payload[2];
			uint16_t id;
			memcpy(&id, &command.payload[2], sizeof(uint16_t));
			memcpy(cBuffer, &command.payload[2], sizeof(uint16_t));
			//float fvalue = build_float(&command.payload[4]);
			//uint16_t pvalue = (uint16_t) fvalue;
			bool bFound = false;
			struct TOCElement element = this->m_tocParameters->elementForID(id, bFound);

			//memcpy(&cBuffer[1 * sizeof(uint16_t)], &pvalue, sizeof(uint16_t));
			crtpPacket->setData(cBuffer, nSize);
			crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
			char sending[crtpReceived->sendableDataLength() + 2];
			memcpy(sending, &command.msg_id, sizeof(uint16_t));
			char * sendData = crtpReceived->sendableData();
			char type = (char) element.nType;
			sendData[0] = type;
			memcpy(&sending[2], sendData, crtpReceived->sendableDataLength());
			send(command.fd, sending, crtpReceived->sendableDataLength() + 2, 0);

			break;
		}
		

	case ACMD_SETPARAM:
		{
		int nSize = sizeof(double) + 2;
		char cBuffer[nSize];

		CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_PARAM);
		crtpPacket->setChannel(0x02);		// The PID update is on channel 1
		CCRTPPacket *crtpReceived;

		//int id = (char) command.payload;
		//char pid = (char) command.payload[2];
		uint16_t pid;
		memcpy(&pid, &command.payload[2], 2);
		memcpy(cBuffer, &command.payload[2], 2);
		bool bFound = false;
		struct TOCElement element = this->m_tocParameters->elementForID(pid, bFound);
		uint8_t pType = element.nType;
		float fvalue = build_float(&command.payload[4]);
		switch(pType) {
			case 8:
			{
				uint8_t pvalue = (uint8_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(uint8_t));
				nSize = 2 + sizeof(uint8_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 9:
			{
				uint16_t pvalue = (uint16_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(uint16_t));
				nSize = 2 + sizeof(uint16_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 10:
			{
				uint32_t pvalue = (uint32_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(uint32_t));
				nSize = 2 + sizeof(uint32_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 11:
			{
				uint64_t pvalue = (uint64_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(uint64_t));
				nSize = 2 + sizeof(uint64_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 0:
			{
				int8_t pvalue = (int8_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(int8_t));
				nSize = 2 + sizeof(int8_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 1:
			{
				int16_t pvalue = (int16_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(int16_t));
				nSize = 2 + sizeof(int16_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 2:
			{
				int32_t pvalue = (uint32_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(int32_t));
				nSize = 2 + sizeof(int32_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 3:
			{
				int64_t pvalue = (int64_t) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(int64_t));
				nSize = 2 + sizeof(int64_t);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 6:
			{
				float pvalue = fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(float));
				nSize = 2 + sizeof(float);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
			case 7:
			{
				double pvalue = (double) fvalue;

				memcpy(&cBuffer[2], &pvalue, sizeof(double));
				nSize = 2 + sizeof(double);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);
				break;
			}
		}
		break;
		}

	case ACMD_SETPOINT:
		{
			float roll = build_float(&command.payload[13]);
			float pitch = build_float(&command.payload[9]);
			float yaw = build_float(&command.payload[17]);
			float fthrust = build_float(&command.payload[5]);
			float ftime = build_float(&command.payload[1]);
			int nSize = 4 * sizeof(float) + sizeof(char);
			char cBuffer[nSize];
			if(command.payload[0] == 1) {
				cBuffer[0] = 9;
			}
			else if(command.payload[0] == 2) {
				cBuffer[0] = 8;
			}
			else if(command.payload[0] == 3) {
				cBuffer[0] = 10;
			}
			else if(command.payload[0] == 4) {
				cBuffer[0] = 7;
			}
			else {
				break;
			}
			memcpy(&cBuffer[0 * sizeof(float) + 1], &roll, sizeof(float));
			memcpy(&cBuffer[1 * sizeof(float) + 1], &pitch, sizeof(float));
			memcpy(&cBuffer[2 * sizeof(float) + 1], &yaw, sizeof(float));
			memcpy(&cBuffer[3 * sizeof(float) + 1], &fthrust, sizeof(float));

			CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, 7);
			crtpPacket->setChannel(0x00);
			int milliseconds = ftime*1000;
			CCRTPPacket *crtpReceived = NULL;
			if(ftime == 0) {
				crtpReceived = m_crRadio->sendPacket(m_nRadioChannel, crtpPacket, this);
			}
			else {
				auto start = std::chrono::high_resolution_clock::now();
				auto timeNow = std::chrono::high_resolution_clock::now();
				auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow-start);
				while(duration.count() < milliseconds) {
					usleep(10);
					crtpReceived = m_crRadio->sendPacket(m_nRadioChannel, crtpPacket, this);
					timeNow = std::chrono::high_resolution_clock::now();
					duration = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow-start);
				}
				cBuffer[0] = 0;
				nSize = sizeof(char);
				crtpPacket->setData(cBuffer, nSize);
				crtpReceived = m_crRadio->sendPacket(m_nRadioChannel, crtpPacket, this);
			}
			delete crtpPacket;
			break;
		}
	case ACMD_GETLOGFILE:
		{
			std::string filename;
			switch(command.payload[0]) {
				case 0: 
				{
					filename = this->getlogfile();
					break;
				}
				case 1:
				{
					filename = this->m_tocParameters->getParamTOCFile();
					break;
				}
				case 2:
				{
					filename = this->m_tocLogs->getLogTOCFile();
					break;
				}
				case 3:
				{
					filename = this->activeHeader;
					break;
				}
				case 4: 
				{
					if(this->test_stand != NULL) {
						filename = " true";
					}
					else {
						filename = " false";
					}
					break;
				}
				default:
				{
					filename = "error: invalid command id\n";
					break;
				}

			}
			uint16_t nSize = strlen(filename.c_str()) + 4;
			char cBuffer[nSize];
			memcpy(cBuffer, &command.msg_id, sizeof(uint16_t));
			uint16_t size = nSize - 4;
			memcpy(&cBuffer[2], &size, sizeof(uint16_t));
			memcpy(&cBuffer[4], filename.c_str(), strlen(filename.c_str()));
			send(command.fd, cBuffer, nSize, 0);

			break;
		}
	case ACMD_LOGBLOCKCOMMAND:
		{
			char cmd = (char) command.payload[0];
			char id = (char) command.payload[1];
			bool found = false;
			switch(cmd) {
				case 0:
				{
					this->resetLoggingBlocks();
					this->printUpdatedHeader();
					break;
				}
				case 1:
				{
					this->resetLoggingBlocks();
					this->loadLoggingBlocksFromFile("loggingBlocks.txt");
					this->printUpdatedHeader();
					break;
				}
				case 2:
				{
					this->loadLoggingBlocksFromFile("loggingBlocks.txt");
					this->printUpdatedHeader();
					break;
				}
				case 3:
				{
					this->m_tocLogs->unregisterLoggingBlockID(id);
					this->printUpdatedHeader();
					break;
				}
				case 4:
				{
					struct LoggingBlock block = this->m_tocLogs->loggingBlockForID(id, found);
					this->enableLogging(block.strName);
					this->printUpdatedHeader();
					break;
				}
				case 5:
				{
					struct LoggingBlock block = this->m_tocLogs->loggingBlockForID(id, found);
					this->disableLogging(block.strName);
					this->printUpdatedHeader();
					break;
				}
				case 6:
				{
					if(this->test_stand == NULL) {
						std::vector<serial::PortInfo> devices_found = serial::list_ports();

						std::vector<serial::PortInfo>::iterator iter = devices_found.begin();

						std::string selectedPort = "n/a";

						while( iter != devices_found.end() )
						{
							serial::PortInfo device = *iter++;

							if(!device.port.compare("/dev/ttyUSB0")) {
								selectedPort = device.port;
								break;
							}
						}

						if(selectedPort.compare("n/a")) {
							serial::Serial * test_stand_ptr = new serial::Serial(selectedPort, 9600);
							test_stand_ptr->setTimeout(0, 200, 0, 200, 0);
							this->setTestStand(test_stand_ptr);
						}
						else {
							this->setTestStand(NULL);
						}
					}
					break;
				}
				case 7:
				{
					if(this->test_stand != NULL) {
						delete this->test_stand;
						this->test_stand = NULL;
					}
					break;
				}
				case 8: 
				{
					this->allowLogging = true;
					break;
				}
				case 9:
				{
					this->allowLogging = false;
					break;
				}
			}
			break;
		}

	}
	// A command was parsed
	return( true );
}

float build_float(uint8_t* buf) {
	union {
		float f;
		int i;
	} x;

	x.i = buf[3] << 24
	| buf[2] << 16
	| buf[1] << 8
	| buf[0];

	return x.f;
}
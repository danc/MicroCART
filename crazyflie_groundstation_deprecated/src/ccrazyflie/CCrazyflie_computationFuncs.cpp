#include "CCrazyflie.h"
#include "CCRTPPacket.h"



int CCrazyflie::queryComputationType() {
	// Create just random data to send (Unsure how the packet behaves if there is no data)
	int nSize = 1;
	char cBuffer[1];
	int computationId = this->m_tocParameters->idForName("mode");
	cBuffer[0] = computationId;

	// Create the packet to send
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_PARAM);
	crtpPacket->setChannel(0x01);
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	// Parse the response
	int dataLength = crtpReceived->dataLength();
	
	int retVal = -1;
	if (dataLength >= 1) {
		// Parse the one data byte to get controller type
		char *cData = crtpReceived->data();
		this->computationType = cData[0];
		retVal = this->computationType;
	} else {
		// This isn't good...
		retVal = -1;
	}

	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;

	return( retVal );
}

void CCrazyflie::enableComputation() {
	// Populate the packet
	int nSize = 2;
	char cBuffer[2];
	cBuffer[0] = COMP_CONFIG_ENABLE;
	cBuffer[1] = 1;

	// Create the packet to send
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_COMPUTATION);
	crtpPacket->setChannel(COMP_CHAN_CONFIG);
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;

	this->computationStarted = true;
}

void CCrazyflie::disableComputation() {
	// Populate the packet
	int nSize = 2;
	char cBuffer[2];
	cBuffer[0] = COMP_CONFIG_ENABLE;
	cBuffer[1] = 0;

	// Create the packet to send
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_COMPUTATION);
	crtpPacket->setChannel(COMP_CHAN_CONFIG);
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;

	this->computationStarted = false;
}

/**
 * Set the computation data rate in Hz.
 * Note: Maximum rate of 1000 Hz.
 *
 * @param rate The computation rate in Hz
 */
void CCrazyflie::setComputationRate(uint16_t rate) {
	// Populate the packet
	int nSize = 3;
	char cBuffer[3];
	cBuffer[0] = COMP_CONFIG_RATE;
	
	memcpy( &(cBuffer[1]), &rate, sizeof(uint16_t) );

	// Create the packet to send
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_COMPUTATION);
	crtpPacket->setChannel(COMP_CHAN_CONFIG);
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;
}

void CCrazyflie::configureComputation( void *config, uint8_t length) {
	// Populate the packet
	int nSize = length+1;
	char *cBuffer;
	cBuffer = (char*) malloc(nSize);
	cBuffer[0] = COMP_CONFIG_FUNC;			// Add the appropriate opcode
	memcpy(&cBuffer[1], config, length);

	// Create the packet to send
	// OUTDATED
	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, CRTP_PORT_COMPUTATION);
	crtpPacket->setChannel(COMP_CHAN_CONFIG);
	CCRTPPacket *crtpReceived = m_crRadio->sendAndReceive(crtpPacket, m_nRadioChannel, this);

	// Clear variables and return
	delete crtpPacket;
	delete crtpReceived;
}
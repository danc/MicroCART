// Copyright (c) 2013, Jan Winkler <winkler@cs.uni-bremen.de>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of Universität Bremen nor the names of its
//       contributors may be used to endorse or promote products derived from
//       this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <time.h>
#include <stdbool.h>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include "CCrazyflie.h"
#include "CCrazyRadio.h"
 
CCrazyflie::CCrazyflie(CCrazyRadio *crRadio, int nRadioChannel, XferRate dataRate, int quadNum, double startTime) {
	m_quadNum = quadNum;
	m_crRadio = crRadio;
	m_nRadioChannel = nRadioChannel;
	m_radioDataRate = dataRate;

	m_network = NULL;
	computationStarted = false;

	// Review these values
	m_fMaxAbsRoll = 45.0f;
	m_fMaxAbsPitch = m_fMaxAbsRoll;
	m_fMaxYaw = 180.0f;
	m_nMaxThrust = 60000;
	m_nMinThrust = 0; //15000;

	m_fRoll = 0;
	m_fPitch = 0;
	m_fYaw = 0;
	m_nThrust = 0;

	// Initialize the sending stuff
	m_sendSetpoints = false;
	m_sendPosition = false;

	//this->resetLoggingBlocks();

	// Initialize the log and parameters
	m_tocParameters = new CTOC(m_crRadio, this, nRadioChannel, 2);
	m_tocParameters->downloadTOC();
	m_tocLogs = new CTOC(m_crRadio, this, nRadioChannel, 5);
	m_tocLogs->downloadTOC();

	// Initialize the ACK miss stuff
	m_nAckMissCounter = 0;
	m_nAckMissTolerance = 0;

	// Initalize the cycle state
	m_enumState = STATE_ZERO;

	m_dSendSetpointPeriod = 0.01; // Seconds
	m_dSetpointLastSent = 0;

	// Initialize the positions
	m_currentCameraPosition.x = 0;
	m_currentCameraPosition.y = 0;
	m_currentCameraPosition.z = 0;
	m_currentCameraAngle.roll = 0;
	m_currentCameraAngle.pitch = 0;
	m_currentCameraAngle.yaw = 0;
	m_currentCameraQuat.q0 = 0;
	m_currentCameraQuat.q1 = 0;
	m_currentCameraQuat.q2 = 0;
	m_currentCameraQuat.q3 = 1;
	m_currentLocalPosition.x = 0;
	m_currentLocalPosition.y = 0;
	m_currentLocalPosition.z = 0;

	// Initialize the setpoints
	m_desiredPositionGlobal.x = 0;
	m_desiredPositionGlobal.y = 0;
	m_desiredPositionGlobal.z = 0;
	m_desiredYaw = 0;
	m_baseThrust = 0;
	m_desiredPositionLocal.x = 0;
	m_desiredPositionLocal.y = 0;
	m_desiredPositionLocal.z = 0;


	// Default to the global coordinate system
	m_systemToUse = coor_global;

	// Default the local to be the same as the global, and some default extents
	m_localOrigin.x = 0;
	m_localOrigin.y = 0;
	m_localOrigin.z = 0;

	m_localXlim.min = -2;
	m_localXlim.min = 2;

	m_localYlim.min = -2;
	m_localYlim.min = 2;

	m_localZlim.min = -2.5;
	m_localZlim.min = 0.5;

	// Set the default flight mode to grounded
	m_enumFlightMode = GROUNDED_MODE;

	// Initialize the takeoff stuff
	takeoffProfile = NULL;
  	numTakeoffSteps = 0;
	currentStep = 0;
  	lastStepTime = 0;

	m_startTime = startTime;

	// Variables used to compute the sampling time of position information
	lastPositionTime = 0;
	positionTimeDelta = 0;

	// Initialize the mutexes
	pthread_mutex_init( &(this->userCommandsMutex), NULL);
	pthread_mutex_init( &(this->cameraDataMutex), NULL);

	// Open the file 
	try {
		char fileName[255];
		sprintf(fileName, "console_cflie%d", m_quadNum+1);
		this->file_console.open(fileName, std::ios_base::out);
	} catch (...) {
		std::cout << "Error opening console file for " << +(m_quadNum+1);
		exit(-1);
	}

	this->queryControllerType();
	this->queryComputationType();

	// Open the log file
	char fileName[255];
	sprintf(fileName, "logs/cflie%d", m_quadNum+1);
	this->openLogFile(fileName);
}

CCrazyflie::~CCrazyflie() {
	if (file_log) {
		file_log.close();
	}

	if (file_console) {
		file_console.close();
	}

	delete m_tocParameters;
	delete m_tocLogs;
}

bool CCrazyflie::sendSetpoint(float fRoll, float fPitch, float fYaw,
		short sThrust) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	fPitch = -fPitch;

	int nSize = 3 * sizeof(float) + sizeof(short);
	char cBuffer[nSize];
	memcpy(&cBuffer[0 * sizeof(float)], &fRoll, sizeof(float));
	memcpy(&cBuffer[1 * sizeof(float)], &fPitch, sizeof(float));
	memcpy(&cBuffer[2 * sizeof(float)], &fYaw, sizeof(float));
	memcpy(&cBuffer[3 * sizeof(float)], &sThrust, sizeof(short));

	if( ALL_THE_DEBUG ) printf( "sending setpoint, thrust %d\n", (int)sThrust );

	CCRTPPacket *crtpPacket = new CCRTPPacket(cBuffer, nSize, 3);
	CCRTPPacket *crtpReceived = m_crRadio->sendPacket(m_nRadioChannel, crtpPacket, this);

	delete crtpPacket;
	if (crtpReceived != NULL) {
		delete crtpReceived;
		if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
		return true;
	} else {
		if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
		return false;
	}
}

bool CCrazyflie::cycle() {
//	double dTimeNow = this->currentTime();
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	bool usbStat;

	if( ALL_THE_DEBUG ) printf( "cycle: \n" );
	switch (m_enumState) {
	case STATE_ZERO: {
		if( ALL_THE_DEBUG ) printf( "    STATE_ZERO\n" );
		m_enumState = STATE_READ_PARAMETERS_TOC;
	}
		break;

	case STATE_READ_PARAMETERS_TOC: {
		if( ALL_THE_DEBUG ) printf( "    STATE_READ_PARAMETERS_TOC\n" );
		//if (this->readTOCParameters()) {
			m_enumState = STATE_READ_LOGS_TOC;
		//}
	}
		break;

	case STATE_READ_LOGS_TOC: {
		if( ALL_THE_DEBUG ) printf( "    STATE_READ_LOGS_TOC\n" );
		//if (this->readTOCLogs()) {
			m_enumState = STATE_START_LOGGING;
		//}
	}
		break;

	case STATE_START_LOGGING: {
		if( ALL_THE_DEBUG ) printf( "    STATE_START_LOGGING\n" );
		//if (this->startLogging()) {
			m_enumState = STATE_ZERO_MEASUREMENTS;
		//}
	}
		break;

	case STATE_ZERO_MEASUREMENTS: {
		if( ALL_THE_DEBUG ) printf( "    STATE_ZERO_MEASUREMENTS\n" );
//		m_tocLogs->processPackets( m_lstLoggingPackets );

		// NOTE(winkler): Here, we can do measurement zero'ing. This is
		// not done at the moment, though. Reason: No readings to zero at
		// the moment. This might change when altitude becomes available.

		m_enumState = STATE_NORMAL_OPERATION;
	}
		break;

	case STATE_NORMAL_OPERATION: {
		if( ALL_THE_DEBUG ) printf( "    STATE_NORMAL_OPERATION\n" );

		switch (this->m_enumFlightMode) {
			case TAKEOFF_MODE:
				// If the quad is taking off, run the takeoff routine
				this->takeoffCycle();
				break;
			case LANDING_MODE:
				// If the quad is landing, check for ground contact
				if ( fabs(this->getPositionZ( coor_global )) < 0.05) {
					std::cout << "Low enough at: " << fabs(this->getPositionZ( coor_global )) << std::endl;
					this->setFlightMode(GROUNDED_MODE);
				}
				break;
			default:
				break;
		}

		
		//int port = -1;
		//int channel = -1;
		/*while(port != 5 && channel != 2) {
			CCRTPPacket * logData = this->m_crRadio->waitForPacket(m_nRadioChannel, this);
			port = logData->port();
			channel = logData->channel();
			if(logData->port() == 5 && logData->channel() == 2) {
				this->m_tocLogs->processPackets(logData);
			}
		}*/
		
		

		// If there is a packet from the computation network to send, send it
		// Place a timeout of 5 packets on the sending
		int count = 0;
		CCRTPPacket *pk = new CCRTPPacket(0);
		while( m_network->retrievePacket(m_quadNum, pk) && (count < 5) ) {
			this->m_crRadio->sendPacket( m_nRadioChannel, pk, this);
			count++;
		}
		delete pk;
		
		// If there are commands, process them
		count = 0;
		while ( this->parseReceivedUserCommand() && (count < 5) ) {
			count++;
		}

		// If there are PID parameters, send them
		if ( this->pidToSend.size() > 0 ) {
			PID_ControllerParams param = this->pidToSend.front();
			std::string pidStr = "";
			switch(param.controllerID) {
				case PID_X:
					pidStr = "X";
					break;
				case PID_Y:
					pidStr = "Y";
					break;
				case PID_Z:
					pidStr = "Z";
					break;
				case PID_ROLL:
					pidStr = "ROLL";
					break;
				case PID_PITCH:
					pidStr = "PITCH";
					break;
				case PID_YAW:
					pidStr = "YAW";
					break;
				case PID_ROLL_RATE:
					pidStr = "ROLLRATE";
					break;
				case PID_PITCH_RATE:
					pidStr = "PITCHRATE";
					break;
				case PID_YAW_RATE:
					pidStr = "YAWRATE";
					break;
				default:
					break;
			}
			this->pidToSend.pop();
			this->sendPID_Constants(param, "");
		}

		// If the setpoints are new, send them
		if (this->m_sendSetpoints) {
			this->sendPositionSetpoint();
			this->m_sendSetpoints = false;

			// Reset the flag to reset the controllers
			this->m_resetController = false;
		}

		// If the position is new, send it
		if (this->m_sendPosition) {
			this->sendCameraData();
			this->m_sendPosition = false;
		} else {
			// Send a dummy packet for keepalive otherwise
			m_crRadio->sendDummyPacket(m_nRadioChannel, this);
			
		}

		//read test_stand data
		// int speed = 0;
		// if(this->testStandError <= 0) {
		// 	this->testStandError = 0;
		// 	speed = this->readTestStand();
		// }
		// else {
		// 	this->testStandError--;
		// }

		// if(speed < 0) {
		// 	this->testStandError = 1;
		// }

		this->testStandFlag = true;

		// CCRTPPacket *logD = m_crRadio->readACK();
		// if(logD != NULL) {
		// 	if(logD->dataLength() > 0) {
		// 		this->callback_Packet(logD);
		// 	}	
		// }
		
	}
		break;

	default: {
	}
		break;
	}

	if (m_crRadio->ackReceived()) {
		if( ALL_THE_DEBUG ) printf( "    received ACK\n" );
		m_nAckMissCounter = 0;
	} else {
		m_nAckMissCounter++;
		if( ALL_THE_DEBUG ) printf( "    no ACK, counter now %d\n", m_nAckMissCounter );
	}

	usbStat = m_crRadio->usbOK();

	if( ALL_THE_DEBUG ) printf( "    usbStat: %d\n", usbStat );

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );

	return usbStat;
}

bool CCrazyflie::copterInRange() {
	return m_nAckMissCounter < m_nAckMissTolerance;
}

double CCrazyflie::currentTime() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	struct timespec tsTime;
	clock_gettime(CLOCK_MONOTONIC, &tsTime);

	return( (tsTime.tv_sec + double(tsTime.tv_nsec) / NSEC_PER_SEC) - m_startTime);
}

bool CCrazyflie::isInitialized() {
	return m_enumState == STATE_NORMAL_OPERATION;
}

enum State CCrazyflie::getCurrentState() {
	return this->m_enumState;
}

void* CCrazyflie::startTestStand(void* args) {
	CCrazyflie *quad = (CCrazyflie*) args;
	quad->testStand.setupTestStand(0);
	quad->testStandLoop();

	return(NULL);
}

void CCrazyflie::testStandLoop() {
	std::cout << "Spawning thread to read test stand" << std::endl;
	std::string result = "";
	int speed = 0;
	while(1) {
		if(testStandFlag) {
			//read test_stand data
			speed = 0;
			if(this->testStandError <= 0) {
				this->testStandError = 0;
				speed = this->readTestStand();
			}
			else {
				this->testStandError--;
			}

			if(speed < 0) {
				this->testStandError = 1;
			}			
			this->testStandFlag = false;
		}
		usleep(500);

		// Check if the thread has been told to terminate
		if (this->m_exitThread) {
			std::cout << "Stopping test stand thread for crazyflie " << this->m_quadNum+1 << std::endl;
			return;
		}
	}
}

void CCrazyflie::stopThread() {
	this->m_exitThread = true;
}

void CCrazyflie::radioLogging(bool logging) {
	this->m_crRadio->isLogging = logging;
}
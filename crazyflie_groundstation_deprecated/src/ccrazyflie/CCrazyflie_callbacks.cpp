/**
 * This file contains the callback functions for the CCrazyflie class.
 *
 */
#include "CCrazyflie.h"
#include <quat.h>

/*
 * Callback called whenever there is a new VRPN position packet.
 * Note: This function is static in the class.
 *
 * @param crazyflie Pointer to the crazyflie object (cast as a void *)
 * @param t The tracker results
 */
void VRPN_CALLBACK CCrazyflie::vrpnTrackerCallback(void *crazyflie, const vrpn_TRACKERCB t) {
	// Make a new variable so the methods can be called
	CCrazyflie *crazy = (CCrazyflie *) crazyflie;

	// Temp structs to pass inside the function
	angularPosition angles;
	lateralPosition lat;
	quaternionPosition quat;

	// Pull out the lateral position into the struct
	lat.x = t.pos[0];
	lat.y = t.pos[1];
	lat.z = t.pos[2];

	// Pull out the angular position
	q_vec_type euler;
	q_to_euler(euler, t.quat);
	angles.yaw = Q_RAD_TO_DEG(euler[0]);
	angles.pitch = Q_RAD_TO_DEG(euler[1]);
	angles.roll = Q_RAD_TO_DEG(euler[2]);

	// Pull out the quaternion position
	quat.q0 = t.quat[0];
	quat.q1 = t.quat[1];
	quat.q2 = t.quat[2];
	quat.q3 = t.quat[3];

	// Actually call the set routines
	crazy->setCameraQuat(quat);
	crazy->setCameraAngle(angles);
	crazy->setCameraPosition(lat);
}

/**
 * This callback is called by the radio whenever a packet is received
 * from the Crazyflie.
 *
 * @param crtpPacket The packet received
 */
void CCrazyflie::callback_Packet(CCRTPPacket *crtpPacket) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	char *cData = crtpPacket->data();
	int nLength = crtpPacket->dataLength();

	

	switch ( crtpPacket->port() ) {
	case CRTP_PORT_CONSOLE:
		{
			// Console
			if ( nLength < 1) {
				// See if the length is 0, if it is leave
				break;
			}

			// Allocate space for the text array and a null terminator
			char *cText;
			cText = (char*) std::malloc(nLength + 1);
			if (cText == NULL) {
				// Memory allocation failed
				break;
			}

			// Copy the data and add a null terminator
			std::memcpy(cText, &cData[0], nLength);
			cText[nLength] = '\0';

			// Print it and flush the console
			this->file_console << cText;
			this->file_console.flush();

			std::free(cText);
		}
		break;

	case CRTP_PORT_LOG:
		// Logging
		if (crtpPacket->channel() == 2 && this->m_crRadio->isLogging) {
			CCRTPPacket *crtpLog = new CCRTPPacket(cData, nLength,
			                                       crtpPacket->channel());
			crtpLog->setChannel(crtpPacket->channel());
			crtpLog->setPort(crtpPacket->port());
			//printf("Packet Process");
			m_tocLogs->processPackets(crtpLog);

			delete crtpLog;
		}
		break;

	// OUTDATED
	case CRTP_PORT_COMPUTATION:
		// The computation port
		{
			if (crtpPacket->channel() == COMP_CHAN_COMPUTE_DATA) {
				// This is the channel that contains data to pass along the network
				if (m_network == NULL) {
					// The network hasn't been enabled, so just ignore the packet
					break;
				}

				if ( nLength < 1) {
					// See if the length is 0, if it is leave
					break;
				}

				// Pass the packet into the network so it can be processed
				CCRTPPacket *crtpComp = new CCRTPPacket(cData, nLength,
				                                       crtpPacket->channel());
				crtpComp->setChannel(crtpPacket->channel());
				crtpComp->setPort(crtpPacket->port());

				m_network->packetReceived(m_quadNum, crtpComp);

				delete crtpComp;
			}
		}
		break;
	}

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}
#include "crazyflieGroundStation.h"
#include "CCrazyflie.h"
#include "CCrazyRadio.h"
#include "vrpn.h"
#include "errno.h"
#include "userInput.h"
#include "userOutput.h"
#include "Bridge.h"
#include "computations/NetworkForwarding.h"
#include "computations/LocalizationLogger.h"
#include "serial/serial.h"

#include "quadcopterData.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <vector>

#include <limits>

uint8_t exitProgram = 0;

const std::string erisIP = "192.168.0.196"; //TODO Change to IP of Other Linux Computer***
const int port = 60551;
const int buffLen = 8;

// The bridge object
Bridge *bridge;

// The computation graph
NetworkForwarding *network;

// The logger for the localization system
LocalizationLogger *compLogger;

using namespace std;

void initMulticast();
void sendMulticast();
void readMulticast();

void closeoutProgram();

double startTime = 0;

float cfliePitch = 0;
float cflieRoll = 0;
float cflieYaw = 0;
float cflieThrust = 0;
double lastTime = 0;
double curTime = 0;
double initTimeVrpn = 0;

int frameCount = 0;
double takeOffTime1 = 0;
double takeOffTime2 = 0;
double takeOffTime3 = 0;
double takeOffTime4 = 0;
double endTakeOffTime = 0;
float takeOffSetpoint = 0.75;

char landingMode = 0;
char mirrorMode = 0;
char takeOff1 = 0;
char takeOff2 = 0;
char takeOff3 = 0;
char takeOff4 = 0;
char handMode = 0;
char trackHand = 0;
char tcp_server_ON = 0;
char tcp_client_ON = 0;
char flap = 0;

//=========Testing Variables===============
int j = 0; //Testing Index Variable****
int dongleNum = 0;
bool toggle = 0;

double loopTimeTotalStart = 0;
double loopTimeTotalEnd = 0;
double loopTimeTotal = 0;		//Sum of Callback Time Totals
double loopTimeTotalPrev = 0;
double loopTimeTotalDelta = 0;

//========End Testing Variables============

// For keeping track of the threads that are spawned
pthread_t threads[3+NUM_RADIOS];

#define PI 3.14159265

int main(int argc, char** argv) {
	//sleep(1); //***FOR TESTING PURPOSES*** (REMOVE)

	signal(SIGINT, &ctrlc_handler);

	unlink("./crazyflie_groundstation.socket");

	// Initialize the computation logger
	char compFileName[] = "logs/comp_localization";
	compLogger = new LocalizationLogger(networkNumNodes, compFileName, 1.0/100.0);

	// Initialize the computation graph
	network = new NetworkForwarding(networkNumNodes, networkEdges, networkNumEdges);
/*	network->addEdge({1, 2});
	network->addEdge({1, 4});
	network->addEdge({2, 1});
	network->addEdge({2, 3});
	network->addEdge({3, 2});
	network->addEdge({3, 4});
	network->addEdge({4, 1});
	network->addEdge({4, 3});
*/	
	network->setNetworkLogger(compLogger);
	network->displayAdjacency();
	
	// Initialize the crazyflie radios
	for (int i = 0; i < NUM_RADIOS; i++) {
		cout << "Initializing Radio " << i << endl;

		// Create the radio object
		radios[i].radio = new CCrazyRadio(i);

		// Start the radio
		if (radios[i].radio->startRadio() == 0) {
			// The radio failed to initialize, error out
			std::cerr << "Error: Radio " << i << " did not initialize." << std::endl;
			exit(1);
		}
		radios[i].radio->setARDTime( radios[i].ARDtime );
		radios[i].radio->setARC( radios[i].ARC );
		radios[i].radio->setPower( radios[i].powerLevel );
		radios[i].radio->setDataRate( radios[i].dataRate );
	}


	struct timespec tsTime;
	clock_gettime(CLOCK_MONOTONIC, &tsTime);

	startTime = tsTime.tv_sec + double(tsTime.tv_nsec) / NSEC_PER_SEC;

	// Initialize the CrazyFlie quadcopters
	for (int i = 0; i < NUM_QUADS; i++) {
		cout << "Initializing Crazyflie " << i+1 << endl;

		// Init the radio
		if(argc == 2 && i == 0) {
			uint8_t channelNum = (uint8_t) std::strtol(argv[1], (char**) NULL, 10);
			radios[crazyflie_info[i].radioNumber].radio->setChannel( channelNum );
		}
		else {
			radios[crazyflie_info[i].radioNumber].radio->setChannel( crazyflie_info[i].channelNumber );
		}
		

		// Clear the packets on the network destined for this quadcopter
		network->clearNetworkQueue(i);

		// Create the crazyflie object
		if(argc == 2 && i == 0) {
			uint8_t channelNo = (uint8_t) std::strtol(argv[1], (char**) NULL, 10);
			crazyflie_info[i].cflieCopter = new CCrazyflie(radios[crazyflie_info[i].radioNumber].radio,
		 											   channelNo,
		 											   crazyflie_info[i].dataRate,
		 											   i,
		 											   startTime);
		}
		else {
			crazyflie_info[i].cflieCopter = new CCrazyflie(radios[crazyflie_info[i].radioNumber].radio,
		 											   crazyflie_info[i].channelNumber,
		 											   crazyflie_info[i].dataRate,
		 											   i,
		 											   startTime);
		}
		crazyflie_info[i].cflieCopter->setNetworkForwarding(network);
		crazyflie_info[i].initTime = crazyflie_info[i].cflieCopter->currentTime();

		// Add the Crazyflie to the radio
		radios[crazyflie_info[i].radioNumber].radio->addCrazyflie( crazyflie_info[i].cflieCopter );

		// Disable the computation (just in case)
		//crazyflie_info[i].cflieCopter->disableComputation();

		std::vector<serial::PortInfo> devices_found = serial::list_ports();

		std::vector<serial::PortInfo>::iterator iter = devices_found.begin();

		std::string selectedPort = "n/a";

		while( iter != devices_found.end() )
		{
			serial::PortInfo device = *iter++;

			if(!device.port.compare("/dev/ttyUSB0")) {
				selectedPort = device.port;
				break;
			}
		}

		if(selectedPort.compare("n/a")) {
			serial::Serial * test_stand_ptr = new serial::Serial(selectedPort, 9600);
			test_stand_ptr->setTimeout(0, 200, 0, 200, 0);
			crazyflie_info[i].cflieCopter->testStand.setSerialInterface(test_stand_ptr);
		}
		else {
			crazyflie_info[i].cflieCopter->testStand.setSerialInterface(NULL);
		}

		// Check the controller type
		cout << crazyflie_info[i].cflieCopter->getControllerTypeString() << endl;

		// Check the computation type
		cout << crazyflie_info[i].cflieCopter->getComputationTypeString() << endl;

		lateralPosition localOrigin;
		localOrigin.x = -0.5;
		localOrigin.y = 0.5;
		localOrigin.z = -1;
		crazyflie_info[i].cflieCopter->setLocalOrigin( localOrigin);

		cout << "Configuring the computation subsystem" << endl;
		computation_localization_config_packet config;
		config.nodeNumber = i;
		config.numTotalNodes = NUM_QUADS;
  		config.stepSize = 0.01;
  		config.anchor = 0;
  		config.k1 = 1;
  		config.k2 = 1;
  		config.k3 = 1;

  		//crazyflie_info[i].cflieCopter->configureComputation( (void*) &config, sizeof(computation_localization_config_packet));


  		switch (crazyflie_info[i].cflieCopter->getControllerType()) {
		case 1:
			// Change the PID controller values
			cout << "Sending PID Constants" << endl;
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_X], "X" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_Y], "Y" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_Z], "Z" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_ROLL], "roll_out" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_PITCH], "pitch_out" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_YAW], "yaw_out" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_ROLL_RATE], "roll_out" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_PITCH_RATE], "pitch_out" );
			usleep(1000);
			crazyflie_info[i].cflieCopter->sendPID_Constants( crazyflie_info[i].pidParams[PID_YAW_RATE], "yaw_out" );
			break;
		case 4:
			// Change the state space controller values
			cout << "Sending state space gains" << endl;
			for (int j=0; j < STATE_END; j++) {
				usleep(1000);
				crazyflie_info[i].cflieCopter->sendSGSF_Gains( ssGains[j] );
			}
			break;
		}


		usleep(1000);
		// Only do this for the RADA motors
//		struct PID_ControllerParams armMotors = {0.0, 0.0, 0.0, 0.0, 9};
//		crazyflie_info[i].cflieCopter->sendPID_Constants( armMotors );

		while( crazyflie_info[i].cflieCopter->getCurrentState() != STATE_NORMAL_OPERATION ) {
			crazyflie_info[i].cflieCopter->cycle();
		}

		//usleep(100);

		crazyflie_info[i].cflieCopter->radioLogging(true);
		crazyflie_info[i].cflieCopter->resetLoggingBlocks();

		// Log the battery information
		// if ( crazyflie_info[i].cflieCopter->addLoggingBlock("battery", 20) ) {
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("battery", "pm.vbat");
		// }

		//usleep(100);

		// The current information about the quad

		// Log the controller loop time
		/*
		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("contStats", 200) ) {
			crazyflie_info[i].cflieCopter->addLoggingEntry("contStats", "controlStats.estimDur");
			crazyflie_info[i].cflieCopter->addLoggingEntry("contStats", "controlStats.contDur");
		}*/

		
		// usleep(1000);
		// if ( crazyflie_info[i].cflieCopter->addLoggingBlock("pwm", 100) ) {
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("pwm", "pwm.m1_pwm");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("pwm", "pwm.m2_pwm");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("pwm", "pwm.m3_pwm");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("pwm", "pwm.m4_pwm");
		// }

		/*
		usleep(1000);
		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("pitch", 100) ) {
			crazyflie_info[i].cflieCopter->addLoggingEntry("pitch", "pid_attitude.pitch_outP");
			crazyflie_info[i].cflieCopter->addLoggingEntry("pitch", "pid_attitude.pitch_outD");
			crazyflie_info[i].cflieCopter->addLoggingEntry("pitch", "pid_attitude.pitch_outI");
		}

		usleep(1000);
		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("roll", 100) ) {
			crazyflie_info[i].cflieCopter->addLoggingEntry("roll", "pid_attitude.roll_outP");
			crazyflie_info[i].cflieCopter->addLoggingEntry("roll", "pid_attitude.roll_outD");
			crazyflie_info[i].cflieCopter->addLoggingEntry("roll", "pid_attitude.roll_outI");
		}
		*/

/*		usleep(1000);
		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("mixer", 100) ) {
			crazyflie_info[i].cflieCopter->addLoggingEntry("mixer", "mixer.ctr_thrust");
			crazyflie_info[i].cflieCopter->addLoggingEntry("mixer", "mixer.ctr_roll");
			crazyflie_info[i].cflieCopter->addLoggingEntry("mixer", "mixer.ctr_pitch");
			crazyflie_info[i].cflieCopter->addLoggingEntry("mixer", "mixer.ctr_yaw");
		}
*/
		
		// if ( crazyflie_info[i].cflieCopter->addLoggingBlock("stabilizer", 50) ) {
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("stabilizer", "stabilizer.roll");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("stabilizer", "stabilizer.pitch");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("stabilizer", "stabilizer.yaw");
    	// 	//crazyflie_info[i].cflieCopter->addLoggingEntry("stabilizer", "stabilizer.thrust");
		// 	crazyflie_info[i].cflieCopter->enableLogging("stabilizer");
		// }
		

		// if ( crazyflie_info[i].cflieCopter->addLoggingBlock("gyro", 50) ) {
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("gyro", "gyro.x");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("gyro", "gyro.y");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("gyro", "gyro.z");
		// 	crazyflie_info[i].cflieCopter->enableLogging("gyro");
		// }
		

/*		usleep(1000);
		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("positionPID", 100) ) {
			crazyflie_info[i].cflieCopter->addLoggingEntry("positionPID", "posCtlAlt.outx");
			crazyflie_info[i].cflieCopter->addLoggingEntry("positionPID", "posCtlAlt.outy");
			crazyflie_info[i].cflieCopter->addLoggingEntry("positionPID", "posCtlAlt.outz");
		}
*/

//		usleep(1000);
//		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("attitudePID", 100) ) {
//			crazyflie_info[i].cflieCopter->addLoggingEntry("attitudePID", "pitchPID.output");
//			crazyflie_info[i].cflieCopter->addLoggingEntry("attitudePID", "rollPID.output");
//			crazyflie_info[i].cflieCopter->addLoggingEntry("attitudePID", "yawPID.output");
//		}

//		usleep(1000);
//		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("ratePID", 100) ) {
//			crazyflie_info[i].cflieCopter->addLoggingEntry("ratePID", "pitchRatePID.output");
//			crazyflie_info[i].cflieCopter->addLoggingEntry("ratePID", "rollRatePID.output");
//			crazyflie_info[i].cflieCopter->addLoggingEntry("ratePID", "yawRatePID.output");
//		}

		// usleep(1000);
		// if ( crazyflie_info[i].cflieCopter->addLoggingBlock("acc", 100) ) {
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("acc", "acc.x");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("acc", "acc.y");
		// 	crazyflie_info[i].cflieCopter->addLoggingEntry("acc", "acc.z");
		// }

		/*usleep(1000);
		if ( crazyflie_info[i].cflieCopter->addLoggingBlock("ctrlStdnt", 100) ) {
			crazyflie_info[i].cflieCopter->addLoggingEntry("ctrlStdnt", "ctrlStdnt.rollRate");
		}
		*/

		crazyflie_info[i].cflieCopter->loadLoggingBlocksFromFile("loggingBlocks.txt");
		crazyflie_info[i].cflieCopter->printUpdatedHeader();

		crazyflie_info[i].cflieCopter->displayLoggingBlocksInitialized();

		/* The current outputs from the position PIDs
		if(crazyflie_info[i].cflieCopter->addLoggingBlock("position", 100)) {
			crazyflie_info[i].cflieCopter->addLoggingEntry("position", "posCtlAlt.outx");
			crazyflie_info[i].cflieCopter->addLoggingEntry("position", "posCtlAlt.outy");
			crazyflie_info[i].cflieCopter->addLoggingEntry("position", "posCtlAlt.outz");
		}*/

		
		
	}

	

	bridge = new Bridge();

	// Initialize the VRPN connections
#if 1
	//vrpn_init("192.168.0.120:3883", handle_hand);
#else
	vrpn_init("192.168.0.120:3883", NULL);
//	vrpn_init("localhost:3883", NULL);
#endif // END USE_HAND

	usleep(10000);

#if USE_LOGGING
//===============Initialize the Logging Files=====================
	//const char * cols = "Timestamp, Roll, Pitch, Yaw, X, Y, Z";
#if USE_BASIC_LOGGING
	const char * logHeaderHand = "#Hand Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tLoopTime\t\tLoopTimeDelta\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tseconds\t\tseconds\n";

#else
	const char * logHeaderHand = "#Hand Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tLoopTime\t\tLoopTimeDelta\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tseconds\t\tseconds\n";

#endif // END: USE_BASIC_LOGGING

#if USE_HAND
	hand.logfile = fopen(hand.logfileName, "w");
		
			if(hand.logfile == NULL)
			{
				printf("Could not open %s: errno %d\n", hand.logfileName, errno);
				exit(-1);
			}
		
	fprintf(hand.logfile, "%s\n", logHeaderHand);
#endif 	// END USE_HAND
#endif	// END USE_LOGGING

//===========End Logfile Init==============		

	// Clear all the queues for good measure
	network->clearAllNetworkQueues();


	cout << "The Init has completed" << endl;
	
	void* status;

	int i=0;
    pthread_create(&threads[i++], NULL, UIThread, (void*)1);
    //pthread_create(&threads[i++], NULL, vrpn_go, (void*)2);

    pthread_create(&threads[i++], NULL, displayData, (void*)3);
    for (int j=0; j<NUM_RADIOS; j++) {
    	pthread_create(&threads[i+j], NULL, CCrazyRadio::startThread, (void*) radios[j].radio );
    }
	for (int z=0; z< NUM_QUADS; z++) {
    	pthread_create(&threads[i+j+z], NULL, CCrazyflie::startTestStand, (void*) crazyflie_info[z].cflieCopter);
    }
    for (int j=0; j < (i+NUM_RADIOS+NUM_QUADS); j++) {
    	pthread_join(threads[j], &status);
    }

	// End the program
	cout << "Closing program" << endl;
	closeoutProgram();
	return 0;
}


/**
 * This function is registered as the handler for when Ctrl-C is pressed
 */
void ctrlc_handler(int sig) {
	// Send up a flag for the threads to end
	for (int i = 0; i < NUM_RADIOS; i++) {
		radios[i].radio->stopThread();
	}
	for(int i = 0; i < NUM_QUADS; i++) {
		crazyflie_info[i].cflieCopter->stopThread();
	}
	unlink("./crazyflie_groundstation.socket");
	closeoutProgram();
	exit(0);
}


/**
 * Function that closes out the program properly and deletes all objects
 */
void closeoutProgram() {
	for (int i = 0; i < NUM_QUADS; i++) {
		// Turn off the crazyflies
		cout << "Stopping crazyflie " << (i+1) << endl;
		crazyflie_info[i].cflieCopter->setBaseThrust( 0 );
		crazyflie_info[i].cflieCopter->cycle();

		cout << "\tBattery" << ": " << crazyflie_info[i].cflieCopter->batteryLevel() << endl;
		crazyflie_info[i].cflieCopter->cycle();
		//crazyflie_info[i].cflieCopter->resetLoggingBlocks();
		if(crazyflie_info[i].cflieCopter->test_stand != NULL) {
			crazyflie_info[i].cflieCopter->test_stand->close();
		}
		delete crazyflie_info[i].cflieCopter;
	}

	for (int i = 0; i < NUM_RADIOS; i++) {
		delete radios[i].radio;
	}

	delete network;
	delete compLogger;

	// Close out of the log files
#if USE_LOGGING
	#if USE_HAND
	fclose(outHand);
	#endif
#endif
}

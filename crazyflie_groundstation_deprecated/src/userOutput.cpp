#include "userOutput.h"
#include "userInput.h"
#include "quadcopterData.h"
#include "callbacks.h"
#include "crazyflieGroundStation.h"
#include "Bridge.h"
#include <iostream>

using namespace std;


// When sent to cout, this clears the terminal screen
const string CLEAR = "\033[H\033[J";

/**
 * Display the data from the quadcopters
 */
void* displayData(void* id) {

    printf("Spawning User Output Thread\n");

	lateralPosition bridgePos;
	targetDistance pos1, pos2, pos3, pos4;

	while (1) {

		for (int i = 0; i < NUM_QUADS; i++) {
			bool check = false;
			crazyflie_info[i].cflieCopter->m_currentLocalPosition.x = (float) crazyflie_info[i].cflieCopter->m_tocParameters->elementForName("X", check).dValue;
		}

		// Clear the display
		cout << CLEAR;

		// Print out the commands
		cout << "Command List" << endl;
		cout << "t = Takeoff" << endl;
		cout << "l = Land" << endl;
		cout << "x# = Change X Setpoint to # meters" << endl;
		cout << "y# = Change Y Setpoint to # meters" << endl;
		cout << "z# = Change Z Setpoint to # meters" << endl;
		cout << "w# = Change Yaw setpoint to # degrees" << endl;
		cout << "r# = Hover over bridge at # between targets" << endl;
		cout << "h = Land on bridge" << endl;
		cout << "b = Takeoff from bridge" << endl;
		cout << "k = Kill all motors" << endl;
		cout << "m# = Send # to all motors (requires special firmware)" << endl;
		cout << "d = Goto default setpoint" << endl;
		cout << endl;

		// Print the headers
		cout << "\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			cout << "Quad " << i+1;
			cout << "\t\t";
		}
		cout << endl;

		// Print out the current mode number
		cout << "Mode\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			switch (crazyflie_info[i].cflieCopter->m_enumFlightMode) {
			case LANDING_MODE:
				cout << "Landing";
				break;
			case TAKEOFF_MODE:
				cout << "Takeoff";
				break;
			case HOVER_MODE:
				cout << "Hover";
				break;
			case MIRROR_MODE:
				cout << "Mirror";
				break;
			case HAND_MODE:
				cout << "Hand";
				break;
			case GROUNDED_MODE:
				cout << "Grounded";
				break;
			case STEP_MODE:
				cout << "Step";
				break;
			case KILL_MODE:
				cout << "Kill";
				break;
/*
			case BRIDGE_HOVER_MODE:
				cout << "Bridge H";
				break;
			case BRIDGE_LANDING_MODE:
				cout << "Bridge L";
				break;
			case BRIDGE_TAKEOFF_MODE:
				cout << "Bridge T";
				break;
*/
			case IDLE_MODE:
				cout << "Idle";
				break;
			default:
				cout << "?";
			}
			cout << "\t\t";
		}
		cout << endl << endl;

		// Print the current X position
		cout << "Coor\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			if ( crazyflie_info[i].cflieCopter->getCoordinateSystem() == coor_local ) {
				cout << "Local";
			} else {
				cout << "Global";
			}
			cout << "\t\t";
		}
		cout << endl << endl;

		// Print the current X position
		cout << "Cur X\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			coordinateSystem coor = crazyflie_info[i].cflieCopter->getCoordinateSystem();
			printf("%1.3f", crazyflie_info[i].cflieCopter->getPositionX( coor ) );
			cout << "\t\t";
			}
		cout << endl;

		// Print the X setpoint
		cout << "Set X\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			coordinateSystem coor = crazyflie_info[i].cflieCopter->getCoordinateSystem();
			printf("%1.3f", crazyflie_info[i].cflieCopter->getSetpointX( coor ) );
			cout << "\t\t";
		}
		cout << endl << endl;

		// Print the current Y position
		cout << "Cur Y\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			coordinateSystem coor = crazyflie_info[i].cflieCopter->getCoordinateSystem();
			printf("%1.3f", crazyflie_info[i].cflieCopter->getPositionY( coor ) );
			cout << "\t\t";
		}
		cout << endl;

		// Print the Y setpoint
		cout << "Set Y\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			coordinateSystem coor = crazyflie_info[i].cflieCopter->getCoordinateSystem();
			printf("%1.3f", crazyflie_info[i].cflieCopter->getSetpointY( coor ) );
			cout << "\t\t";
		}
		cout << endl << endl;

		// Print the current Z position
		cout << "Cur Z\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			coordinateSystem coor = crazyflie_info[i].cflieCopter->getCoordinateSystem();
			printf("%1.3f", crazyflie_info[i].cflieCopter->getPositionZ( coor ) );
			cout << "\t\t";
		}
		cout << endl;

		// Print the Z setpoint
		cout << "Set Z\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			coordinateSystem coor = crazyflie_info[i].cflieCopter->getCoordinateSystem();
			printf("%1.3f", crazyflie_info[i].cflieCopter->getSetpointZ( coor ) );
			cout << "\t\t";
		}
		cout << endl << endl;

		// Print the quad attitude
		cout << "C Roll\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->getCameraRoll() );
			cout << "\t\t";
		}
		cout << endl;

		cout << "Q Roll\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->getQuadRoll() );
			cout << "\t\t";
		}
		cout << endl;

		cout << "C Pitch\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->getCameraPitch() );
			cout << "\t\t";
		}
		cout << endl;

		cout << "Q Pitch\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->getQuadPitch() );
			cout << "\t\t";
		}
		cout << endl;

		cout << "C Yaw\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->getCameraYaw() );
			cout << "\t\t";
		}
		cout << endl;

		cout << "Q Yaw\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->getQuadYaw() );
			cout << "\t\t";
		}
		cout << endl << endl;

#ifdef USE_BRIDGE
	if(bridge->endpoint1Valid){
		if(NUM_QUADS == 1){	
			bridge->getTargetDistance(&bridgePos, &pos1); 
			cout << setprecision(3) <<  "D T1 X: " << bridge->disToQuad1.x << endl;
			cout << setprecision(3) <<  "D T1 Y: " << bridge->disToQuad1.y << endl;
			cout << setprecision(3) <<  "D T1 Z: " << bridge->disToQuad1.z << endl;
			cout << setprecision(3) <<  "D T1: " << bridge->disToQuad1.dis << endl;
		}
			if(NUM_QUADS == 2){	
			bridge->getTargetDistance(&bridgePos, &pos1, &pos2); 
			cout << setprecision(3) <<  "D T1 X: " << bridge->disToQuad1.x << "\t\t" << bridge->disToQuad2.x << endl;
			cout << setprecision(3) <<  "D T1 Y: " << bridge->disToQuad1.y << "\t\t" << bridge->disToQuad2.y << endl;
			cout << setprecision(3) <<  "D T1 Z: " << bridge->disToQuad1.z << "\t\t" << bridge->disToQuad2.z << endl;
			cout << setprecision(3) <<  "D T1: " << bridge->disToQuad1.dis << "\t\t" << bridge->disToQuad2.dis << endl;
		}
		if(NUM_QUADS == 3){	
			bridge->getTargetDistance(&bridgePos, &pos1, &pos2, &pos3); 
			cout << setprecision(3) <<  "D T1 X: " << bridge->disToQuad1.x << "\t\t" << bridge->disToQuad2.x << "\t\t" << bridge->disToQuad3.x << endl;
			cout << setprecision(3) <<  "D T1 Y: " << bridge->disToQuad1.y << "\t\t" << bridge->disToQuad2.y << "\t\t" << bridge->disToQuad3.y << endl;
			cout << setprecision(3) <<  "D T1 Z: " << bridge->disToQuad1.z << "\t\t" << bridge->disToQuad2.z << "\t\t" << bridge->disToQuad3.z << endl;
			cout << setprecision(3) <<  "D T1: " << bridge->disToQuad1.dis << "\t\t" << bridge->disToQuad2.dis << "\t\t" << bridge->disToQuad3.dis << endl;
		}
		if(NUM_QUADS == 4){	
			bridge->getTargetDistance(&bridgePos, &pos1, &pos2, &pos3, &pos4); 
			cout << setprecision(3) << "D T1 X: " << bridge->disToQuad1.x << "\t\t" << bridge->disToQuad2.x << "\t\t" << bridge->disToQuad3.x << "\t\t" << bridge->disToQuad4.x <<  endl;
			cout << setprecision(3) << "D T1 Y: " << bridge->disToQuad1.y << "\t\t" << bridge->disToQuad2.y << "\t\t" << bridge->disToQuad3.y << "\t\t" << bridge->disToQuad4.y <<  endl;
			cout << setprecision(3) << "D T1 Z: " << bridge->disToQuad1.z << "\t\t" << bridge->disToQuad2.z << "\t\t" << bridge->disToQuad3.z << "\t\t" << bridge->disToQuad4.z <<  endl;
			cout << setprecision(3) <<  "D T1: " << bridge->disToQuad1.dis << "\t\t" << bridge->disToQuad2.dis << "\t\t" << bridge->disToQuad3.dis << "\t\t" << bridge->disToQuad4.dis << endl;
		}
	}
		cout << endl;
		
#endif

		cout << "Gyro X\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->sensorDoubleValue("gyro.x"));
			cout << "\t\t";
		}
		cout << endl;

		cout << "Gyro Y\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->sensorDoubleValue("gyro.y"));
			cout << "\t\t";
		}
		cout << endl;

		cout << "Gyro Z\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->sensorDoubleValue("gyro.z"));
			cout << "\t\t";
		}
		cout << endl << endl;

		cout << "Accel X\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->sensorDoubleValue("acc.x"));
			cout << "\t\t";
		}
		cout << endl;

		cout << "Accel Y\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->sensorDoubleValue("acc.y"));
			cout << "\t\t";
		}
		cout << endl;

		cout << "Accel Z\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.3f", crazyflie_info[i].cflieCopter->sensorDoubleValue("acc.z"));
			cout << "\t\t";
		}
		cout << endl << endl;

		cout << "Base Thrust\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.2f", crazyflie_info[i].cflieCopter->sensorDoubleValue("mixer.ctr_thrust"));
			cout << "\t\t";
		}
		cout << endl << endl;

		// Print the battery level
		cout << "Test Stand\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.2f", crazyflie_info[i].cflieCopter->sensorDoubleValue("testStand"));
			cout << "\t\t";
		}
		cout << endl << endl;

		// Print the battery level
		cout << "Battery\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.2f", crazyflie_info[i].cflieCopter->sensorDoubleValue("pm.vbat"));
			cout << "\t\t";
		}
		cout << endl << endl;
		if(bridge->endpoint1Valid){
			cout << "Target 1 X: " << bridge->endpoint1.x << "\tY: " << bridge->endpoint1.y << "\tZ: " << bridge->endpoint1.z;
			cout << endl; 
		}
		cout << endl << endl;		

		// Print the loop time
		cout << "Loop T\t";
		for (int i = 0; i < NUM_QUADS; i++) {
			printf("%1.5f", crazyflie_info[i].cflieCopter->getPositionTimeDelta());
			cout << "\t\t";
		}
		cout << endl << endl;

		// Check if the program should terminate
		if (exitProgram) {
			std::cout << "Stopping thread for user output" << endl;
			return(NULL);
		}

		usleep(200000);
	}

	return(NULL);
}

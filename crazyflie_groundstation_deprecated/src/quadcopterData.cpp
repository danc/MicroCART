#include "structures.h"
#include "CCrazyflie.h"
#include "quadcopterData.h"
//#include "eris_vrpn.h"
#include "callbacks.h"
#include "computations/NetworkForwarding.h"

// State Space controller gains
CONTROLLER_SGSF_GAINS ssGains[STATE_END+1] = {
	{state_u,                0.000,     0.000, -1500.000,     0.000},
	{state_v,                0.000,  1500.037,     0.000,     0.000},
	{state_w,            -8345.105,     0.000,     0.000,     0.000},
	{state_p,                0.000,  2400.728,     0.000,     0.000},
	{state_q,                0.000,     0.000,  2340.517,     0.000},
	{state_r,                0.000,     0.000,     0.000,  5952.081},
	{state_x,                0.000,     0.000, -5000.000,     0.000},
	{state_y,                0.000,  5119.382,     0.000,     0.000},
	{state_z,           -18000.000,     0.000,     0.000,     0.000},
	{state_phi,              0.000,  8173.638,     0.000,     0.000},
	{state_theta,            0.000,     0.000,  6825.156,     0.000},
	{state_psi,              0.000,     0.000,     0.000, 40000.000},
	{state_u_integ,          0.000,     0.000,     0.000,     0.000},
	{state_v_integ,          0.000,     0.000,     0.000,     0.000},
	{state_w_integ,          0.000,     0.000,     0.000,     0.000},
	{state_p_integ,          0.000,     0.000,     0.000,     0.000},
	{state_q_integ,          0.000,     0.000,     0.000,     0.000},
	{state_x_integ,          0.000,     0.000,  1000.000,     0.000},
	{state_y_integ,          0.000, -1000.000,     0.000,     0.000},
	{state_z_integ,       5000.000,     0.000,     0.000,     0.000},
	{state_phi_integ,        0.000, -1000.000,     0.000,     0.000},
	{state_theta_integ,      0.000,     0.000, -1000.000,     0.000},
	{state_psi_integ,        0.000,     0.000,     0.000,  -500.000},
};

// Takeoff routine steps
// First column is the desired base thrust to use
// Second column is what type of end condition this is
// Third column is the condition to end the step
int quad1_numTakeoffSteps = 3;
takeoffProfileStep quad1_takeoffSteps[] = {
	{19000, step_time,	 	1},
//	{54000, step_height, -0.3},
	{52000, step_height, -0.3},
//	{50000, step_time, 5},
	{48739,	step_final, 	0}
//	{50000,	step_final, 	0}
};

int networkNumNodes = 4;
int networkNumEdges = 8;
edge_t networkEdges[] = {
	{1, 2},
	{1, 4},
	{2, 1},
	{2, 3},
	{3, 2},
	{3, 4},
	{4, 1},
	{4, 3}
};

// Offsets to be used during mirror flight
float mirrorOffsets[][2] = {
	{ 0.00,  0.00},		// Quadcopter 1 is the master
	{-0.50,  0.00},		// Quadcopter 2
	{ 0.45,  0.00},		// Quadcopter 3
	{ 0.00, -0.50}		// Quadcopter 4
};

// Offsets to be used during mirror flight
float handOffsets[][2] = {
	{-0.50,  0.50},		// Quadcopter 1
	{ 0.50,  0.50},		// Quadcopter 2
	{ 0.50, -0.50},		// Quadcopter 3
	{-0.50, -0.50}		// Quadcopter 4
};

float defaultSetpoints[][3] = {
	//{ 0.681,  0.360, -0.600},	// Quadcopter 1
	{ 0.681,  0.360, -0.600},	// Quadcopter 1
	{-0.475,  0.340, -0.750},	// Quadcopter 2
	{ 0.751,  0.450, -0.500},	// Quadcopter 3
	{-0.200, -0.600, -0.400}		// Quadcopter 4
};

// Structure for the radio
RADIOS_t radios[] = {
	// Radio 0
	{NULL,			// Radio object pointer (initialized by main)
	 2000,
	 0,
	 P_0DBM,
	 XFER_2M
	},

	// Radio 1
	{NULL,
	 2000,
	 0,
	 P_0DBM,
	 XFER_2M
	}
};

// Strucuture for the hand data
HAND_t hand = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "hand.txt", NULL
};

// Initialize the quadcopter data
QUADCOPTERS_t crazyflie_info[] = {
/*	{"UAV",
	 handle_pos,
	 NULL,
	 NULL,
	 100,//swapped 1,2
	 0,
	 0,
	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The time between loops
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  0,								// The default yaw offset
	  0.681,							// The default x setpoint
	  0.360,							// The default y setpoint
	  -0.600,							// The default z setpoint
  	  0,								// The default yaw setpoint
	  0,								// Initialize the x position
	  0,								// Initialize the y position
	  0,								// initialize the z position
	  0,								// Initialize the pitch position
	  0,								// Initialize the roll position
	  0,								// initialize the yaw position
	  0,								// The base thrust to use for the quadcopter
	  0,								// The number of frames received
	  0,								// Reset flag for the controllers
	  0
	 },
 	 "cflie1.txt",			// The name of the logfile
	 {						// The PID values {Kp ,Ki, Kd, iLimit, PID_ID}
	 	//{40.0, 2.0, 44.0, 40.0, PID_X},
	 	{1.0, 0.0, 0.0, 40.0, PID_X},
	 	{1.0, 0.0, 0.0, 40.0, PID_Y},
	 	{1.0, 0.0, 0.0, 40.0, PID_Z},
	 	//{40.0, 2.0, 44.0, 40.0, PID_Y},
	 	//{-10000.0, -2000.0, -15000.0, 10000.0, PID_Z},
	 	{251.0, 0.0, 0.0, 20.0, PID_ROLL},
	 	{251.0, 0.0, 0.0, 20.0, PID_PITCH},
	 	{  6.0, 0.0, 0.0, 360.0, PID_YAW},
	 	{0.0043, 0, 0, 33.3, PID_ROLL_RATE},
	 	{0.0043, 0, 0, 33.3, PID_PITCH_RATE},
	 	{0.0043, 0.00053, 0.0, 166.7, PID_YAW_RATE}
	 }
	},
*/
	// Quadcopter 1
	/*{"Crazyflie21",
	 NULL,
	 NULL,
 	 25,
	 0,
 	 XFER_2M,
	 0,
	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The time between loops
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  0,								// The default yaw offset
//	  0.681,							// The default x setpoint
	  0.0,
	  0.0,
	  -0.3,
//	  0.360,							// The default y setpoint
//	  -0.600,							// The default z setpoint
  	  0,								// The default yaw setpoint
	  0,								// Initialize the x position
	  0,								// Initialize the y position
	  0,								// initialize the z position
	  0,								// Initialize the pitch position
	  0,								// Initialize the roll position
	  0,								// initialize the yaw position
	  0,								// The base thrust to use for the quadcopter
	  0,								// The number of frames received
	  0,								// Reset flag for the controllers
	  0,
	  0
	 },
 	 "logs/cflie1",				// The base name of the logfile
	 {						// The PID values {Kp ,Ki, Kd, iLimit, PID_ID}
//	 	RADA PID Values
//		{0.0, 0.0, 0.0, 20.0, PID_X},
//	 	{0.0, 0.0, 0.0, 20.0, PID_Y},
//	 	{0.0, 0.0, 0.0, 20.0, PID_Z},
//	 	{Kp/Kd, Ki/Kd, 0.0, 20.0, PID_ROLL},	//(180/3.14)
//	 	{Kp/Kd, Ki/Kd, 0.0, 20.0, PID_PITCH},	//(6.0/7.0)
//	 	{0.0, 0.0, 0.0, 20.0, PID_YAW},
//	 	{-Kd, 0.0, 0.0, 20.0, PID_ROLL_RATE},
//	 	{Kd, 0.0, 0.0, 20.0, PID_PITCH_RATE},
//	 	{0.0, 0.0, 0.0, 20.0, PID_YAW_RATE}

	 	{-40.0/2, -2.0/2, -44.0/2, 40.0, PID_X},
	 	{40.0/2, 2.0/2, 44.0/2, 40.0, PID_Y},
	 	{-10000.0, -2000.0, -15000.0, 10000.0, PID_Z},
//	 	{0.0, 0.0, 0.0, 40.0, PID_X},
//	 	{0.0, 0.0, 0.0, 40.0, PID_Y},
//	 	{-10000.0, 0.0, 0.0, 10000.0, PID_Z},
	 	//{10000.0, 2000.0, 15000.0, 10000.0, PID_Z},
//	 	{0.0, 0.0, 0.0, 40.0, PID_X},
//	 	{0.0, 0.0, 0.0, 40.0, PID_Y},
//	 	{0.0, 0.0, 0.0, 40.0, PID_Z},
//	 	{6.0, 0.0, 0.0, 20.0, PID_ROLL},
//	 	{6.0, 0.0, 0.0, 20.0, PID_PITCH},
//	 	{6.0, 0.0, 0.35, 360.0, PID_YAW},
//	 	{250.0, 500.0, 2.5, 33.3, PID_ROLL_RATE},
//	 	{250.0, 500.0, 2.5, 33.3, PID_PITCH_RATE},
//	 	{70.0, 16.7, 0.0, 166.7, PID_YAW_RATE}
	 	{6.0, 1.0, 0.0, 20.0, PID_ROLL},
	 	{6.0, 1.0, 0.0, 20.0, PID_PITCH},
	 	{6.0, 2.0, 0.35, 360.0, PID_YAW},
	 	{250.0, 500.0, 2.5, 33.3, PID_ROLL_RATE},
	 	{250.0, 500.0, 2.5, 33.3, PID_PITCH_RATE},
	 	{70.0, 16.7, 0.0, 166.7, PID_YAW_RATE}
	 },
	},*/	

	// Quadcopter 2
	{"Crazyflie22",			// Trackable name
	 NULL,					// VRPN tracker object (initialized in VRPNinit)
	 NULL,					// CrazyFlie copter object (initialized in main)
	 85,					// The channel number of the crazyflie
	 0,						// The radio to use
	 XFER_2M,				// The datarate of the Crazyflie
	 0,						// The init time
	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The time between loops
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  0,								// The default yaw offset
	  -0,							// The default x setpoint
	  0,							// The default y setpoint
	  -0.750,							// The default z setpoint
  	  0,								// The default yaw setpoint
	  0,								// Initialize the x position
	  0,								// Initialize the y position
	  0,								// initialize the z position
	  0,								// Initialize the pitch position
	  0,								// Initialize the roll position
	  0,								// initialize the yaw position
	  0,								// The base thrust to use for the quadcopter
	  0,								// The number of frames received
	  0,								// Reset flag for the controllers
	  0,
	  0
	 },
	 "logs/cflie2",				// The base name of the logfile
 	 {						// The PID values {Kp ,Ki, Kd, iLimit, PID_ID}
	 	{-40.0/2, -2.0/2, -44.0/2, 40.0, PID_X},
	 	{40.0/2, 2.0/2, 44.0/2, 40.0, PID_Y},
	 	{-10000.0, -2000.0, -15000.0, 10000.0, PID_Z},
	 	{6.0, 1.0, 0.0, 20.0, PID_ROLL},
	 	{6.0, 1.0, 0.0, 20.0, PID_PITCH},
	 	{6.0, 2.0, 0.35, 360.0, PID_YAW},
	 	{250.0, 500.0, 2.5, 33.3, PID_ROLL_RATE},
	 	{250.0, 500.0, 2.5, 33.3, PID_PITCH_RATE},
	 	{70.0, 16.7, 0.0, 166.7, PID_YAW_RATE}
	 },
	},

	// Quadcopter 3
	/*{"Crazyflie23",
	 NULL,
	 NULL,
	 65, //swapped radio on 1 and 3
	 1,
	 XFER_2M,				// The datarate of the Crazyflie
	 0,
 	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The time between loops
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  0,								// The default yaw offset
	  0.751,							// The default x setpoint
	  -0.450,							// The default y setpoint
	  -0.500,							// The default z setpoint
	  0,								// The default yaw setpoint
	  0,								// Initialize the x position
	  0,								// Initialize the y position
	  0,								// initialize the z position
	  0,								// Initialize the pitch position
	  0,								// Initialize the roll position
	  0,								// initialize the yaw position
	  0,								// The base thrust to use for the quadcopter
	  0,								// The number of frames received
	  0,								// Reset flag for the controllers
	  0
	 },
	 "logs/cflie3",							// The base name of the logfile
 	 {						// The PID values {Kp ,Ki, Kd, iLimit, PID_ID}
	 	{-40.0/2, -2.0/2, -44.0/2, 40.0, PID_X},
	 	{40.0/2, 2.0/2, 44.0/2, 40.0, PID_Y},
	 	{-10000.0, -2000.0, -15000.0, 10000.0, PID_Z},
	 	{6.0, 1.0, 0.0, 20.0, PID_ROLL},
	 	{6.0, 1.0, 0.0, 20.0, PID_PITCH},
	 	{6.0, 2.0, 0.35, 360.0, PID_YAW},
	 	{250.0, 500.0, 2.5, 33.3, PID_ROLL_RATE},
	 	{250.0, 500.0, 2.5, 33.3, PID_PITCH_RATE},
	 	{70.0, 16.7, 0.0, 166.7, PID_YAW_RATE}
	 },
	},

	// Quadcopter 4
	{"Crazyflie24",
	 NULL,
	 NULL,
	 85,
	 1,
	 XFER_2M,				// The datarate of the Crazyflie
	 0,
 	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The time between loops
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  0,								// The default yaw offset
	  -0.200,							// The default x setpoint
	  -0.600,							// The default y setpoint
	  -0.400,							// The default z setpoint
	  0,								// The default yaw setpoint
	  0,								// Initialize the x position
	  0,								// Initialize the y position
	  0,								// Initialize the z position
	  0,								// Initialize the pitch position
	  0,								// Initialize the roll position
	  0,								// Initialize the yaw position
	  0,								// The base thrust to use for the quadcopter
  	  0,								// The number of frames received
	  0,								// Reset flag for the controllers
	  0,
	  0
	 },
 	 "logs/cflie4",				// The base name of the logfile
 	 {						// The PID values {Kp ,Ki, Kd, iLimit, PID_ID}
	 	{-40.0/2, -2.0/2, -44.0/2, 40.0, PID_X},
	 	{40.0/2, 2.0/2, 44.0/2, 40.0, PID_Y},
	 	{-10000.0, -2000.0, -15000.0, 10000.0, PID_Z},
	 	{6.0, 1.0, 0.0, 20.0, PID_ROLL},
	 	{6.0, 1.0, 0.0, 20.0, PID_PITCH},
	 	{6.0, 2.0, 0.35, 360.0, PID_YAW},
	 	{250.0, 500.0, 2.5, 33.3, PID_ROLL_RATE},
	 	{250.0, 500.0, 2.5, 33.3, PID_PITCH_RATE},
	 	{70.0, 16.7, 0.0, 166.7, PID_YAW_RATE}
	 },
	}*/
};

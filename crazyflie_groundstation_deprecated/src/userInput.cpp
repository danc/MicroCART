#include <pthread.h>
#include <stdio.h>
#include <curses.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <pty.h>
#include <stdint.h>
#include <err.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <assert.h>
#include <errno.h>
#include <sys/ioctl.h> 
#include <sys/types.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/stat.h>
//#include <cstring>

#include "CCrazyflie.h"
#include "quadcopterData.h"
#include "userInput.h"
#include "callbacks.h"
#include "crazyflieGroundStation.h"
#include "helperFunctions.h"

#define BytesTo16(lsb, msb) (((lsb) & 0xff) | (((msb) & 0xff) << 8))

using namespace std;

char * adapter_socket = "./crazyflie_groundstation.socket";
static int adapterSocket;
fd_set rfds_master;
int max_fd = 0;

//static int backendSocket;

#define MAX_CLIENTS 32
#define CLIENT_BUFFER_SIZE 64
#define CLIENT_MAX_PENDING_RESPONSES 64
static struct command* client_buffers[MAX_CLIENTS][CLIENT_BUFFER_SIZE];
static int client_fds[MAX_CLIENTS];
static int client_pending_responses[MAX_CLIENTS][CLIENT_MAX_PENDING_RESPONSES];

//static volatile int keepRunning = 1;

int safe_fd_set(int , fd_set* , int* );
int safe_fd_clr(int , fd_set* , int* );

ssize_t get_buffer_data_size(uint8_t * data);

static int new_client(int fd);
static ssize_t get_client_index(int fd);
static int wasDisconnected(int fd);
static int remove_client(int fd);
static struct command ** get_client_buffer(int fd);
static int * get_client_pend_responses(int fd);
int findNullPointer(struct command** buffer);
ssize_t DecodePacket(struct metadata * m, uint8_t * data, size_t data_size, const uint8_t * packet, size_t packet_size);

static int client_recv(int fd);

/**
 * This thread will get user input
 */
void* UIThread(void *threadID) {
	//char tempbuf[255];
	//uint8_t counter = 0;

	//struct termios ttystate;

	printf("Spawning User Input Thread\n");
	
  int activity, fd, i, j;
	FD_ZERO(&rfds_master);
	/* 
	 * Create listening socket
	 */
	/* Determine socket path */
	char * adapter_socket_path = adapter_socket;

	/* Unlink if it exists */
	//unlink(adapter_socket_path);

  /* Create socket */
  //mode_t old_umask = umask(0111);
	adapterSocket = socket(AF_UNIX, SOCK_STREAM, 0);
	if (adapterSocket < 0) {
	  err(-1, "socket");
	}

  printf("adapterSocket = %d\n", adapterSocket);

  /* Create sockaddr and bind */
  struct sockaddr_un sa;
  sa.sun_family = AF_UNIX;
  strcpy(sa.sun_path, adapter_socket_path);
  //sa.sun_path[107] = '\0';
  if (bind(adapterSocket, (struct sockaddr *) &sa, sizeof(sa))) {
    err(-1, "bind");
	}
	//umask(old_umask);

	/* Listen */
  if (listen(adapterSocket, 16) == -1) {
    err(-1, "listen");
	}

  /* Add to socket set */
	safe_fd_set(adapterSocket, &rfds_master, &max_fd);

  /* Initialize client buffers */
  for (i = 0; i < MAX_CLIENTS; i++) {
	  client_fds[i] = -1;
		client_buffers[i][0] = NULL;
		for(j = 0; j < CLIENT_MAX_PENDING_RESPONSES; j++) {
		  client_pending_responses[i][j] = -1;
		}
	}

	safe_fd_set(fileno(stdin), &rfds_master, &max_fd);

  struct timeval timeout = {
	  .tv_sec = 1,
		.tv_usec = 0
	};

	int exit_program = 0;
  while (1) {
		fd_set rfds;
		rfds = rfds_master;
		activity = select(max_fd+1, &rfds, NULL, NULL, NULL);
		if (activity == -1) {
			perror("select() ");
		} else if (activity) {
			for (fd = 0; fd <= max_fd; ++fd) {
			  if (FD_ISSET(fd, &rfds)) {
					if (wasDisconnected(fd)) {
						break;
					}
					if (fd == fileno(stdin)) {
						/**
						 * Ignore stdin from the backend
						 */
					} else if (fd == adapterSocket) {
						int new_fd = 0;
						new_fd = accept(adapterSocket, NULL, NULL);
						if (new_fd < 0) {
							warn("accept");
						} else {
							printf("Connection\n");
							if (new_client(new_fd)) {
								printf("Added client\n");
								safe_fd_set(new_fd, &rfds_master, &max_fd);
							}
						}
					} else if (get_client_index(fd) > -1) {
						//printf("Message Received\n");
						exit_program = client_recv(fd);
						//TODO
					}
				}
			}
		} else {
			timeout.tv_sec = 1;
			timeout.tv_usec = 0;
		}

		// Check if the program should terminate
		if (exit_program) {
			std::cout << "Stopping thread for user input" << endl;
			return(NULL);
		}

	}

  //get the terminal state
  //tcgetattr(STDIN_FILENO, &ttystate);

  //ttystate.c_lflag &= ~ICANON;		//turn off canonical mode
  //ttystate.c_cc[VMIN] = 1;		    //minimum of number input read

  //set the terminal attributes.
  //tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);

	// Loop forever while getting input
	/*while (1) {

		// Get a character from the terminal
		tempbuf[counter] = getchar();

		// If the character is a newline, the command is entered
		if (tempbuf[counter] == '\n') {
			// Add a null terminator to the buffer
			tempbuf[counter] = 0;

			// No command to parse
			if ( counter == 0 ) {
				continue;
			}

			uint8_t commandLength = strlen(tempbuf);

			if ( commandLength < 2) {
				if (tempbuf[0] == 'q') {
					// Exit the program
					ctrlc_handler(1);
					break;
				} else {
					// Invalid command
					counter = 0;
					continue;
				}
			}

			// Get the quadcopter number
			char temp[2];
			temp[0] = tempbuf[0];
			temp[1] = 0;
			uint8_t quadcopterNumber = atoi( temp );

			// Get the command type and the payload
			userCommands_t command;
			command.type = tempbuf[1];
			command.payload = atof(&tempbuf[2]);

			// Pass the command to the Crazyflie
			if (quadcopterNumber == 0) {
				// This command is for every quadcopter
				for (int i=0; i < NUM_QUADS; i++) {
					crazyflie_info[i].cflieCopter->passUserCommand(command);
				}
			} else {
				crazyflie_info[quadcopterNumber-1].cflieCopter->passUserCommand(command);
			}

			// Print it out and then reset the counter
			printf("\nReceived command: %s\n", tempbuf);
			counter = 0;

		} else {
			counter++;
		}

		// Check if the program should terminate
		if (exitProgram) {
			std::cout << "Stopping thread for user input" << endl;
			return(NULL);
		}
		usleep(1000);
	}*/

	std::cout << "Stopping thread for user input" << endl;
    return NULL;
}

int safe_fd_set(int fd, fd_set* fds, int* max_fd) {
	assert(max_fd != NULL);

	FD_SET(fd, fds);
	if (fd > *max_fd) {
	  *max_fd = fd;
	}
	return 0;
}

int safe_fd_clr(int fd, fd_set* fds, int* max_fd) {
  assert(max_fd != NULL);
		    
  FD_CLR(fd, fds);
  if (fd == *max_fd) {
		(*max_fd)--;
	}
	return 0;
}

static int new_client(int fd) {
	ssize_t new_slot = -1, i;
	for (i = 0; i < MAX_CLIENTS; i++) {
		if (client_fds[i] < 0) {
			new_slot = i;
			break;
		}
	}
	if (new_slot == -1) {
		warnx("Ran out of room! Consider increasing MAX_CLIENTS!");
		return 0;
	}

	client_fds[new_slot] = fd;
	client_buffers[new_slot][0] = NULL;

	return 1;
}

static ssize_t get_client_index(int fd) {
  ssize_t i;
	for (i = 0; i < MAX_CLIENTS; i++) {
		if (client_fds[i] == fd) {
		  return i;
		}
	}
																						 
	return -1;
}

static int wasDisconnected(int fd) {
	char buff;
	if (recv(fd, &buff, 1, MSG_PEEK | MSG_DONTWAIT) == 0) {
	  remove_client(fd);
	  safe_fd_clr(fd, &rfds_master, &max_fd);
	  printf("fd %d has disconneted and was removed\n", fd);
	  return 1;
	}
	return 0;
}

//TODO: update for command**
static int remove_client(int fd) {
	int i;
	ssize_t slot = get_client_index(fd);
	if (slot == -1) {
		return -1;
	}
	struct command ** clientBuffer = get_client_buffer(fd);
	if (clientBuffer == NULL) {
	  return -1;
	}
	clientBuffer[0] = NULL;
	int *pendingResponses = get_client_pend_responses(fd);
	if (pendingResponses == NULL) {
	  return -1;
	}
	for (i = 0; i < CLIENT_MAX_PENDING_RESPONSES; i++) {
	  pendingResponses[i] = -1;
	}
	client_fds[slot] = -1;
	return 0;
}

static struct command ** get_client_buffer(int fd) {
  ssize_t slot = get_client_index(fd);
	if (slot == -1) {
		return NULL;
	} else {
		return client_buffers[slot];
	}
}

static int * get_client_pend_responses(int fd) {
  ssize_t slot = get_client_index(fd);
	if (slot == -1) {
	  return NULL;
	} else {
		return client_pending_responses[slot];
	}
}

int findNullPointer(struct command** buffer) {
	int index = 0;
	for(int i = 0; i < CLIENT_BUFFER_SIZE; i++) {
		if(buffer[i] != NULL) {
			index++;
			continue;
		}
		break;
	}
	return index;
}

enum PacketHeader {
	BEGIN,
	MTYPE_L,
	MTYPE_H,
	ID_L,
	ID_H,
	DLEN_L,
	DLEN_H,
	HDR_SIZE
};

enum ChecksumFormat {
	CSUM_L,
	CSUM_SIZE
};

uint8_t PacketChecksum(const uint8_t * packet, size_t packet_size)
{
	uint8_t checkSum = 0;
	for(size_t i = 0; i < packet_size - CSUM_SIZE; i++){
		checkSum ^= packet[i];
	}	
	return checkSum;
}

ssize_t DecodePacket(
        struct metadata * m,        /* Decoded metadata (includes data_len)*/
        uint8_t * data,             /* Data is copied into this buffer */
        size_t data_size,           /* Max buffer size */
        const uint8_t * packet,     /* Packet to decode */
        size_t packet_size)         /* Size of packet to decode */
{
	uint8_t checkSum;
	if (packet[BEGIN] != 190) {
		return -1;
	}

	if (packet_size < ((uint8_t) HDR_SIZE + CSUM_SIZE)) {
		return -2;
	}

	m->msg_type = BytesTo16(packet[MTYPE_L], packet[MTYPE_H]);
	m->msg_id = BytesTo16(packet[ID_L], packet[ID_H]);
	m->data_len =  BytesTo16(packet[DLEN_L], packet[DLEN_H]);

	if (packet_size < (size_t)(HDR_SIZE + CSUM_SIZE + m->data_len)) {
		return -3;
	}

	if (data_size < m->data_len) {
		return -4;
	}

	checkSum = PacketChecksum(packet, HDR_SIZE + m->data_len + CSUM_SIZE);
	if (checkSum != packet[HDR_SIZE + m->data_len]) {
		return -5;
	}

	memcpy(data, &packet[HDR_SIZE], m->data_len);
	return m->data_len;
}

static int client_recv(int fd) {
	struct command ** buffer;
	uint8_t temp[100];
	ssize_t len_pre;
	//char cmdString[64];
	//char * cursor;
	ssize_t r;
	//int index = 0;

	buffer = get_client_buffer(fd);
	len_pre = findNullPointer(buffer);
	//cursor = buffer + len_pre;

	r = read(fd, temp, 100);
	if (r < 0) {
		warn("read (fd: %d)", fd);
	}
	//buffer[len_pre + r] = '\0';

	struct command newCmd;
	struct metadata m;
	newCmd.meta = m;
	DecodePacket(&newCmd.meta, newCmd.data, 100, temp, r);
	int cmType = (int) newCmd.meta.msg_type;
	newCmd.type = (commandType) cmType;
	buffer[len_pre] = &newCmd;

	int flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);

  /* Parse buffer and handle commands */
	while (1) {
		/* not using strtok because reasons */
		ssize_t len = (ssize_t) findNullPointer(buffer);
		//ssize_t newline = -1;
		

		/* No newline found. End parsing 
		if (newline == -1) {
			break;
		}
		*/
		//buffer[newline] = '\0';

		if ( len == 0 ) {
			/*
			if (buffer[0] == 'q') {
				// Exit the program
				ctrlc_handler(1);
		    char * rest = &buffer[newline] + 1;
		    size_t restLen = (strlen(rest) == 0) ? 1 : strlen(rest);
		     Delete parsed data and move the rest to the left 
		    memmove(buffer, rest, restLen +1);
				return 1; //break;
			} else {
				// Invalid command
				//counter = 0;
			
		    char * rest = &buffer[newline] + 1;
		    size_t restLen = (strlen(rest) == 0) ? 1 : strlen(rest);
		     Delete parsed data and move the rest to the left 
		    memmove(buffer, rest, restLen +1);
				continue;
			}
			*/
			len_pre = findNullPointer(buffer);
			//cursor = buffer + len_pre;

			r = read(fd, temp, 100);
			//buffer[len_pre + r] = '\0';

			if(r>0) {
				DecodePacket(&newCmd.meta, newCmd.data, 100, temp, r);
				cmType = (int) newCmd.meta.msg_type;
				newCmd.type = (commandType) cmType;
				buffer[len_pre] = &newCmd;
			}
			continue;
		}

		printf("Client(%d) : '%c'\n",fd, buffer[0]->type);

		/* Get the quadcopter number
		char temp[2];
		temp[0] = buffer[0];
		temp[1] = 0;
		*/
		uint8_t quadcopterNumber = 0;
		

		// Get the command type and the payload
		userCommands_t command;
		command.type = buffer[0]->type;
		command.payload = buffer[0]->data;
		command.fd = fd;
		command.msg_id = buffer[0]->meta.msg_id;

		// Pass the command to the Crazyflie
		if (quadcopterNumber == 0) {
			// This command is for every quadcopter
			for (int i=0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->passUserCommand(command);
			}
			//char temp[256];
			//snprintf(temp, 256, "Got command for quad %d that is %s", quadcopterNumber, buffer);
		} else {
			crazyflie_info[quadcopterNumber-1].cflieCopter->passUserCommand(command);
		}

		// Print it out and then reset the counter
		printf("\nReceived command: %c\n", buffer[0]->type);
		//counter = 0;
		struct command ** rest = &buffer[1];
		void * dest = buffer;
		ssize_t restLen = (ssize_t) findNullPointer(buffer);
		// if(restLen != 0) {
		// 	free(buffer[0]->meta);
		// 	free(buffer[0]->data);
		// 	free(buffer[0]);
		// }
		
		/* Delete parsed data and move the rest to the left */
		memmove(dest, rest, restLen*sizeof(struct command**));

		len_pre = findNullPointer(buffer);
		//cursor = buffer + len_pre;

		r = read(fd, temp, 100);
		//buffer[len_pre + r] = '\0';

		if(r>0) {
			DecodePacket(&newCmd.meta, newCmd.data, 100, temp, r);
			cmType = (int) newCmd.meta.msg_type;
			newCmd.type = (commandType) cmType;
			buffer[len_pre] = &newCmd;
		}
		
	}
	return 0;
}

/*	while (1) {

		// Get a character from the terminal
		tempbuf[counter] = getchar();

		// If the character is a newline, the command is entered
		if (tempbuf[counter] == '\n') {
			// Add a null terminator to the buffer
			tempbuf[counter] = 0;

			// No command to parse
			if ( counter == 0 ) {
				continue;
			}

			uint8_t commandLength = strlen(tempbuf);

			if ( commandLength < 2) {
				if (tempbuf[0] == 'q') {
					// Exit the program
					ctrlc_handler(1);
					break;
				} else {
					// Invalid command
					counter = 0;
					continue;
				}
			}

			// Get the quadcopter number
			char temp[2];
			temp[0] = tempbuf[0];
			temp[1] = 0;
			uint8_t quadcopterNumber = atoi( temp );

			// Get the command type and the payload
			userCommands_t command;
			command.type = tempbuf[1];
			command.payload = atof(&tempbuf[2]);

			// Pass the command to the Crazyflie
			if (quadcopterNumber == 0) {
				// This command is for every quadcopter
				for (int i=0; i < NUM_QUADS; i++) {
					crazyflie_info[i].cflieCopter->passUserCommand(command);
				}
			} else {
				crazyflie_info[quadcopterNumber-1].cflieCopter->passUserCommand(command);
			}

			// Print it out and then reset the counter
			printf("\nReceived command: %s\n", tempbuf);
			counter = 0;

		} else {
			counter++;
		}

		// Check if the program should terminate
		if (exitProgram) {
			std::cout << "Stopping thread for user input" << endl;
			return(NULL);
		}
		usleep(1000);
	} */

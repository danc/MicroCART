#include "CCrazyRadio.h"
#include "CCrazyflie.h"


CCRTPPacket *CCrazyRadio::sendAndReceive(CCRTPPacket *crtpSend, int nRadioChannel, CCrazyflie *crazyflie)
{
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	return this->sendAndReceive(crtpSend, nRadioChannel, crtpSend->port(), crtpSend->channel(), crazyflie);
}

CCRTPPacket *CCrazyRadio::sendAndReceive(CCRTPPacket *crtpSend,
		int nRadioChannel,
		int nPort,
		int nChannel, // the message itself has a separate, nested channel unrelated to the radio channel
		CCrazyflie *crazyflie,
		int nRetries,
		int nMicrosecondsWait)
{
    if( ALL_THE_DEBUG ) printf( "Enter: %s(crtpSend:0x%08lx,nRadioChannel:%d,nPort:%d,nChannel:%d,...)\n", __FUNCTION__, (unsigned long) crtpSend, nRadioChannel, nPort, nChannel  );
	bool bGoon = true;
	int nResendCounter = 0;
//	CCRTPPacket *crtpReturnvalue = NULL;
	CCRTPPacket *crtpReceived = NULL;

	if( ALL_THE_DEBUG ) printf( "sendAndReceive for port %d, channel %d\n", nPort, nChannel );
	while (bGoon) {
        if( ALL_THE_DEBUG ) printf( "iteration\n" );
		if (nResendCounter == 0) {
			if( ALL_THE_DEBUG ) printf( "sendPacket\n" );
			crtpReceived = this->sendPacket(nRadioChannel, crtpSend, crazyflie);
			nResendCounter = nRetries;
		} else {
			nResendCounter--;
		}

		if (crtpReceived) {
	        if( ALL_THE_DEBUG ) printf( "crtpReceived\n" );
			if (crtpReceived->port() == nPort
					&& crtpReceived->channel() == nChannel) {
		        if( ALL_THE_DEBUG ) printf( "right port and channel\n" );
//				crtpReturnvalue = crtpReceived;
				bGoon = false;
			}
			else
			{
		        if( ALL_THE_DEBUG ) printf( "wrong port and channel, expected %d,%d actual %d,%d\n",
		        		nPort, nChannel, crtpReceived->port(), crtpReceived->channel() );

			}
		}

		if (bGoon) {
			if (crtpReceived) {
				delete crtpReceived;
			}

			usleep(nMicrosecondsWait);
			if( ALL_THE_DEBUG ) printf( "waiting for packet\n" );
			crtpReceived = this->waitForPacket( nRadioChannel, crazyflie );
		}
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	//return crtpReturnvalue;
	return crtpReceived;
}

CCRTPPacket *CCrazyRadio::sendPacket(int nRadioChannel, CCRTPPacket *crtpSend, CCrazyflie *crazyflie) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(channelNumber:%d, crtpSend:0x%016lx, )\n", __FUNCTION__, nRadioChannel, (unsigned long)crtpSend );
	CCRTPPacket *crtpPacket = NULL;
#if 0		//TODO Old way of SWITCHING RADIO CHANNELS, NOW DONE IN SIMPLE.CPP
	// To support multiple crazyflies, we need to set the channel number before each
	// send, as each crazyflie will be on its own channel.
	if( ALL_THE_DEBUG ) printf( "switching to radio channel %d\n", nRadioChannel );
	this->setChannel( nRadioChannel );
#endif
	char *cSendable = crtpSend->sendableData();
	crtpPacket = this->writeData(cSendable, crtpSend->sendableDataLength());

	delete[] cSendable;

	if (crtpPacket) {
		if ( crtpPacket->dataLength() > 0 ) {
			crazyflie->callback_Packet(crtpPacket);
		}
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return crtpPacket;
}

CCRTPPacket *CCrazyRadio::waitForPacket(int nChannel, CCrazyflie *crazyflie) {		//Sends NULL Packet if ACK Wait and Packet Retries Fail
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	bool bGoon = true;
	CCRTPPacket *crtpReceived = NULL;
	CCRTPPacket *crtpDummy = new CCRTPPacket(0);
	crtpDummy->setIsPingPacket(true);

	while (bGoon) {
		crtpReceived = this->sendPacket(nChannel, crtpDummy, crazyflie);
		bGoon = (crtpReceived == NULL);
	}

	delete crtpDummy;

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return crtpReceived;
}

CCRTPPacket *CCrazyRadio::readACK() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	CCRTPPacket *crtpPacket = NULL;

	int nBufferSize = 64;
	char cBuffer[nBufferSize];
	int nBytesRead = nBufferSize;

	if (this->readData(cBuffer, nBytesRead)) {
		if (nBytesRead > 0) {
			// Analyse status byte
			m_bAckReceived = true; //cBuffer[0] & 0x1;
//			bool bPowerDetector = cBuffer[0] & 0x2;
//			int nRetransmissions = cBuffer[0] & 0xf0;

			// TODO(winkler): Do internal stuff with the data received here
			// (store current link quality, etc.). For now, ignore it.

			crtpPacket = new CCRTPPacket(0);

			/* Packet format for the CRTP packet received
				  7   6   5   4   3   2   1   0
				+---+---+---+---+---+---+---+---+
				|         Random bits           |
				+---+---+---+---+---+---+---+---+
				|      Port     |  Res  | Chan. |
				+---+---+---+---+---+---+---+---+
				|            DATA 0             |
				+---+---+---+---+---+---+---+---+
				:   :   :   :   :   :   :   :   :
				+---+---+---+---+---+---+---+---+
				|            Cksum              |
				+---+---+---+---+---+---+---+---+
			*/

			if (nBytesRead > 1) {
				// Parse the header into its fields
				char headerByte = cBuffer[1];

				short sPort = (headerByte & 0xf0) >> 4;
				crtpPacket->setPort(sPort);
				short sChannel = headerByte & 0b00000011;
				crtpPacket->setChannel(sChannel);  // JRB: Why does the packet contain the channel also??
				
				// There is no length field in the packet... grrr...
				// use the length read in instead (minus status and header)
				short nLength = nBytesRead - 2;

				// The data is the remainder of the fields
				if (nLength > 0) {
					crtpPacket->setData(&cBuffer[2], nLength);
				}
			}
		} else {
			m_bAckReceived = false;
		}
	}


    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return crtpPacket;
}

bool CCrazyRadio::sendDummyPacket(int nChannel, CCrazyflie *crazyflie) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	CCRTPPacket *crtpReceived = NULL;
	CCRTPPacket *crtpDummy = new CCRTPPacket(0);
	crtpDummy->setIsPingPacket(true);

	crtpReceived = this->sendPacket(nChannel, crtpDummy, crazyflie);
	if (crtpReceived) {
		delete crtpReceived;
		return true;
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return false;
}
#include "CCrazyRadio.h"
#include "CCrazyflie.h"


void CCrazyRadio::closeDevice() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	if (m_hndlDevice) {
		libusb_close(m_hndlDevice);
		libusb_unref_device(m_devDevice);

		m_hndlDevice = NULL;
		m_devDevice = NULL;
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

std::list<libusb_device*> CCrazyRadio::listDevices(int nVendorID,
		int nProductID) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	std::list<libusb_device*> lstDevices;
	ssize_t szCount;
	libusb_device **ptDevices;

	szCount = libusb_get_device_list(m_ctxContext, &ptDevices);
	for (unsigned int unI = 0; unI < szCount; unI++) {
		libusb_device *devCurrent = ptDevices[unI];
		libusb_device_descriptor ddDescriptor;

		libusb_get_device_descriptor(devCurrent, &ddDescriptor);

		if (ddDescriptor.idVendor == nVendorID
				&& ddDescriptor.idProduct == nProductID) {
			libusb_ref_device(devCurrent);
			lstDevices.push_back(devCurrent);
		}
	}

	if (szCount > 0) {
		libusb_free_device_list(ptDevices, 1);
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );

	return lstDevices;
}

bool CCrazyRadio::openUSBDongle() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	this->closeDevice();  // JRB: This seems dangerous if we haven't opened yet
	int popIndex = 0;
	libusb_device *devFirst;

	std::list<libusb_device*> lstDevices = this->listDevices(0x1915, 0x7777);

	if (lstDevices.size() > 0) {
		// For now, just take the first device. Give it a second to
		// initialize the system permissions.
		sleep(1.0);

		// Iterate through the list of dongles that were returned and save off
		// the one we're interested
		while (popIndex <= m_nDongleNumber ) {
			devFirst = lstDevices.front();  // this will keep getting overwritten until we reach the one we're interested in
			lstDevices.pop_front();
			popIndex++;
		}
		// devFirst now contains the one we're interested in
		int nError = libusb_open(devFirst, &m_hndlDevice);

		if (nError == 0) {
			// Opening device OK. Don't free the first device just yet.

			m_devDevice = devFirst;
		}

		for (std::list<libusb_device*>::iterator itDevice = lstDevices.begin();
				itDevice != lstDevices.end(); itDevice++) {
			libusb_device *devCurrent = *itDevice;

			libusb_unref_device(devCurrent);  // JRB: Not sure why we are unreferencing all devices... must be a reason?
		}

    	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
		return !nError;
	}
	std::cout << "No USB Devices Found" << std::endl;

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );

	return false;
}


CCRTPPacket *CCrazyRadio::writeData(void *vdData, int nLength) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(vdData:0x%016lx, nLength:%d)\n", __FUNCTION__, (unsigned long)vdData, nLength  );
	CCRTPPacket *crtpPacket = NULL;

	int nActuallyWritten;
	int nReturn = libusb_bulk_transfer(m_hndlDevice,
			(0x01 | LIBUSB_ENDPOINT_OUT), (unsigned char*) vdData, nLength,
			&nActuallyWritten, 1000);

	if (nReturn == 0 && nActuallyWritten == nLength) {
		crtpPacket = this->readACK();
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return crtpPacket;
}

bool CCrazyRadio::readData(void *vdData, int &nMaxLength) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(vdData:0x%016lx, nMaxLength:%d)\n", __FUNCTION__, (unsigned long)vdData, nMaxLength  );
	int nActuallyRead;
	int nReturn = libusb_bulk_transfer(m_hndlDevice,
			(0x81 | LIBUSB_ENDPOINT_IN), (unsigned char*) vdData, nMaxLength,
			&nActuallyRead, 50);

	if (nReturn == 0) {
		nMaxLength = nActuallyRead;

		return true;
	} else {
		switch (nReturn) {
		case LIBUSB_ERROR_TIMEOUT:
			std::cout << "USB timeout" << std::endl;
			break;

		default:
			break;
		}
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return false;
}

bool CCrazyRadio::writeControl(void *vdData, int nLength, uint8_t u8Request,
		uint16_t u16Value, uint16_t u16Index) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(vdData:0x%016lx, nLength:%d, u8Request:0x%02x, u16Value:0x%04x, u16Index:0x%04x)\n",
    		__FUNCTION__, (unsigned long)vdData, nLength, u8Request, u16Value, u16Index  );
	int nTimeout = 1000;

	/*int nReturn = */libusb_control_transfer(m_hndlDevice,
			LIBUSB_REQUEST_TYPE_VENDOR, u8Request, u16Value, u16Index,
			(unsigned char*) vdData, nLength, nTimeout);

	// Hack.  // JRB:  Why was this hacked in?
    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return true;
}

bool CCrazyRadio::claimInterface(int nInterface) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	return libusb_claim_interface(m_hndlDevice, nInterface) == 0;
}

bool CCrazyRadio::usbOK() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	libusb_device_descriptor ddDescriptor;
	return (libusb_get_device_descriptor(m_devDevice, &ddDescriptor) == 0);
}
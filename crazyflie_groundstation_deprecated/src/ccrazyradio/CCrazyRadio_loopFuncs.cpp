#include "CCrazyRadio.h"
#include "CCrazyflie.h"

#include <vector>

/**
 * Main loop for the thread running the radio
 */
void CCrazyRadio::mainLoop() {
	std::cout << "Spawning thread for radio " << this->m_nDongleNumber << std::endl;

	// Loop indefinitely until told to exit
	while(1) {
		for ( std::vector<CCrazyflie*>::iterator it = crazyflies.begin(); it != crazyflies.end(); ++it) {
			// Set the channel for the next Crazyflie
			this->setChannel( (*it)->getRadioChannel() );
			this->setDataRate( (*it)->getRadioDataRate() );

			// Run the mainloop for the crazyflie
			(*it)->cycle();

			if((*it)->allowLogging) {
				// Write the log information for the crazyflie
				(*it)->writeLogData();
			}	
		}

		usleep(500);

		// Check if the thread has been told to terminate
		if (this->m_exitThread) {
			std::cout << "Stopping thread for radio " << this->m_nDongleNumber << std::endl;
			return;
		}
	}

}

/**
 * Static function to use to start the thread for the radio
 */
void* CCrazyRadio::startThread(void* args) {
	CCrazyRadio *radio = (CCrazyRadio*) args;
	radio->mainLoop();

	return(NULL);
}

/**
 * Stop the thread from running
 */
void CCrazyRadio::stopThread() {
	this->m_exitThread = true;
}
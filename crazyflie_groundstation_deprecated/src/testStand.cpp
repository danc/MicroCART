#include "testStand.h"

/**
 * @brief Construct a new Test Stand:: Test Stand object
 *
 * @param ptr pointer to serial object for writing and reading data
 */
TestStand::TestStand(serial::Serial * ptr){
    setSerialInterface(ptr);
}

/**
 * @brief Helper function to set member variable for serial interface ptr
 *
 * @param ptr pointer to serial object for writing and reading data
 */
void TestStand::setSerialInterface(serial::Serial * ptr){
    this->serialInterface = ptr;
}

/**
 * @brief Zero the heading on the test stand
 *
 */
void TestStand::zeroHeading(){
    this->serialInterface->write(&ZERO_HEADING, 1);
}

/**
 * @brief Starts the streaming mode where the test stand write data nonstop
 *
 */
void TestStand::startStream(){
    this->serialInterface->write(&START_STREAM, 1);
}

/**
 * @brief Stops the streaming mode
 *
 */
void TestStand::stopStream(){
    this->serialInterface->write(&STOP_STREAM, 1);
}

/**
 * @brief Sets the time between samples for streaming mode
 *
 * @param period time between samples in microseconds
 */
void TestStand::setCycleDelay(int period){
    uint8_t delayString = ('@w',  std::to_string(period), '!');
    this->serialInterface->write(&delayString, 6);
}

/**
 * @brief Toggles between Position Mode and Rate Mode
 *
 */
void TestStand::toggleMode(){
    this->serialInterface->write(&MODE_TOGGLE, 1);
}

/**
 * @brief Sets the Test Stand to Position Mode
 *
 */
void TestStand::setModePosition(){
    this->serialInterface->write(&MODE_POSITION, 1);
}

/**
 * @brief Sets the Test Stand to Rate Mode
 *
 */
void TestStand::setModeRate(){
    this->serialInterface->write(&MODE_RATE, 1);
}

/**
 * @brief Gets the current value from the Test Stand
 *
 * @return std::string string containing sample from Test Stand
 */
std::string TestStand::getCurrentValue(){
    this->serialInterface->write(&GET_VALUE, 1);
    return this->serialInterface->readline();
}

/**
 * @brief Gets the current value from the Test Stand as a Position
 *
 * @return std::string string containing sample from Test Stand as Position
 */
std::string TestStand::getCurrentPosition(){
    this->serialInterface->write(&GET_POSITION, 1);
    return this->serialInterface->readline();
}

/**
 * @brief Gets the current value from the Test Stand as a Rate
 *
 * @return std::string string containing sample from Test Stand as Rate
 */
std::string TestStand::getCurrentRate(){
    this->serialInterface->write(&GET_RATE, 1);
    return this->serialInterface->readline();
}

/**
 * @brief Gets the current value from the ADC on the Test Stand
 *
 * @return std::string string containing raw sample from ADC
 */
std::string TestStand::getCurrentADCValue(){
    this->serialInterface->write(&GET_ADC, 1);
    return this->serialInterface->readline();
}

/**
 * @brief Sets up Test Stand for opertation mode
 *
 * @param deviceMode 0 for Rate Mode | 1 for Position Mode (default)
 */
void TestStand::setupTestStand(int deviceMode){
    if(deviceMode == 0){
        setModeRate();
    }else{
        zeroHeading();
        setModePosition();
    }
}

#ifndef TESTSTAND_H_
#define TESTSTAND_H_

#include "serial/serial.h"


class TestStand {

    public:
        TestStand(serial::Serial * ptr);

        void setSerialInterface(serial::Serial * ptr);
        void zeroHeading();
        void startStream();
        void stopStream();
        void setCycleDelay(int period);
        void toggleMode();
        void setModePosition();
        void setModeRate();
        void setupTestStand(int deviceMode);

        std::string getCurrentValue();
        std::string getCurrentPosition();
        std::string getCurrentRate();
        std::string getCurrentADCValue();

    private:
        serial::Serial *serialInterface;

        const uint8_t ZERO_HEADING = '@h';
        const uint8_t MODE_TOGGLE = '@t';
        const uint8_t GET_VALUE = '@v';
        const uint8_t START_STREAM = '@s';
        const uint8_t STOP_STREAM = '@d';
        const uint8_t MODE_POSITION = '@p';
        const uint8_t MODE_RATE = '@r';
        const uint8_t GET_POSITION = '@P';
        const uint8_t GET_RATE = '@R';
        const uint8_t GET_ADC = '@x';

};

#endif

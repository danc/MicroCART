#ifndef STRUCTURES_H_
#define STRUCTURES_H_

#include <stdint.h>

/*! \brief Power levels to configure the radio dongle with */
enum Power {
  /*! \brief Power at -18dbm */
  P_M18DBM = 0,
  /*! \brief Power at -12dbm */
  P_M12DBM = 1,
  /*! \brief Power at -6dbm */
  P_M6DBM = 2,
  /*! \brief Power at 0dbm */
  P_0DBM = 3
};

/*! \brief Channel speeds */
typedef enum XferRate {
  /*! \brief 250 kbps */
  XFER_250K = 0,
  /*! \brief 1 mbps */
  XFER_1M = 1,
  /*! \brief 2 mbps */
  XFER_2M = 2,
} XferRate;

typedef struct computation_localization_config_packet {
  uint8_t nodeNumber;
  uint8_t numTotalNodes;
  float stepSize;
  uint8_t anchor;
  float k1;
  float k2;
  float k3;
} __attribute__((packed)) computation_localization_config_packet;

// The ordering of the states for the K matrix
enum stateOrdering {
 state_u = 0,
 state_v,
 state_w,
 state_p,
 state_q,
 state_r,
 state_x,
 state_y,
 state_z,
 state_phi,
 state_theta,
 state_psi,

 // Lateral velocity integrator states
 state_u_integ,
 state_v_integ,
 state_w_integ,

 // Angular rate integrator states
 state_p_integ,
 state_q_integ,
 state_r_integ,

 // Position integrator states
 state_x_integ,
 state_y_integ,
 state_z_integ,

 // Euler angle integrator states
 state_phi_integ,
 state_theta_integ,
 state_psi_integ,

 // This is an item that will help to know how many states there are
 STATE_END = state_psi_integ,
};

enum CONTROLLER_SGSF_OPCODES {
  CONT_SGSF_UPDATE_GAINS = 0,
  CONT_SGSF_RETRIEVE_GAINS,
};

typedef struct CONTROLLER_SGSF_GAINS {
  uint8_t state;
  float ut;
  float ua;
  float ue;
  float ur;
} __attribute__((packed)) CONTROLLER_SGSF_GAINS;

typedef enum coordinateSystem {
  coor_global = 0,
  coor_local,
} coordinateSystem;

typedef struct userCommands {
  char type;
  uint8_t* payload;
  int fd;
  uint16_t msg_id;
} userCommands_t;

enum takeoffStepType {
  step_final = 0,
  step_time,
  step_height
};

struct PID_ControllerParams {
  float Kp;
  float Ki;
  float Kd;
  float iLimit;
  uint8_t controllerID;
};

typedef struct limits {
  float min;
  float max;
} limits;

typedef struct angularPosition {
  float roll;
  float pitch;
  float yaw;
} angularPosition;

typedef struct lateralPosition {
  float x;
  float y;
  float z;
} lateralPosition;

typedef struct targetDistance {
  float x;
  float y;
  float z;
  float dis;
} targetDistance;

typedef struct quaternionPosition {
  float q0;
  float q1;
  float q2;
  float q3;
} quaternionPosition;

typedef struct takeoffProfileStep {
  uint16_t thrust;
  enum takeoffStepType type;
  double condition;
} takeoffProfileStep;

#endif

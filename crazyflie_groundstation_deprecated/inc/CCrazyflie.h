// Copyright (c) 2013, Jan Winkler <winkler@cs.uni-bremen.de>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of Universität Bremen nor the names of its
//       contributors may be used to endorse or promote products derived from
//       this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


/* \author Jan Winkler */


#ifndef __C_CRAZYFLIE_H__
#define __C_CRAZYFLIE_H__


#define NSEC_PER_SEC 1000000000L


// System
#include <cmath>
#include <stdbool.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include <pthread.h>

// Private
#include "structures.h"
#include "CCrazyRadio.h"
#include "computations/NetworkForwarding.h"
#include "CTOC.h"
#include "serial/serial.h"
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"
#include "testStand.h"


// Ports defined for the CRTP protocol
enum CRTPPort {
  CRTP_PORT_CONSOLE           = 0x00,
  CRTP_PORT_PARAM             = 0x02,
  CRTP_PORT_COMMANDER         = 0x03,
  CRTP_PORT_MEM               = 0x04,
  CRTP_PORT_LOG               = 0x05,
  CRTP_PORT_XYZ_SET           = 0x06,
  CRTP_PORT_XYZ_POS           = 0x07,
  CRTP_PORT_CONTROLLER_UPDATE = 0x08,
  CRTP_PORT_COMPUTATION       = 0x09,
  CRTP_PORT_PLATFORM          = 0x0D,
  CRTP_PORT_LINK              = 0x0F,
};

// Channels in the CRTP packet describing the update requested
enum controllerUpdate_Channels {
  CONTROLLER_QUERY_TYPE = 0,
  CONTROLLER_CALL_UPDATE,
  CONTROLLER_SWITCH_THROTTLE_CTRL
};

// Channels in the CRTP packet for the computation framework
enum Computation_channels {
  COMP_CHAN_QUERY = 0,
  COMP_CHAN_COMPUTE_DATA,
  COMP_CHAN_CONFIG,
};

// These are various codes used to configure the computation framework in the config packet
enum comp_config_codes {
  COMP_CONFIG_ENABLE = 0,
  COMP_CONFIG_FUNC,
  COMP_CONFIG_RATE,
};

// Numbers specified here must match the quadcopter ID numbers
enum PID_Controller_IDs {
  PID_X = 0,
  PID_Y = 1,
  PID_Z = 2,
  PID_ROLL = 3,
  PID_PITCH = 4,
  PID_YAW = 5,
  PID_ROLL_RATE = 6,
  PID_PITCH_RATE = 7,
  PID_YAW_RATE = 8
};

enum State {
  STATE_ZERO = 0,
  STATE_READ_PARAMETERS_TOC = 1,
  STATE_READ_LOGS_TOC = 2,
  STATE_START_LOGGING = 3,
  STATE_ZERO_MEASUREMENTS = 4,
  STATE_NORMAL_OPERATION = 5
};

enum FlightMode{
	LANDING_MODE,
	TAKEOFF_MODE,
	HOVER_MODE,
	MIRROR_MODE,
	HAND_MODE,
	GROUNDED_MODE,
	STEP_MODE,
	KILL_MODE,
  BRIDGE_HOVER_MODE,
  BRIDGE_LANDING_MODE,
  BRIDGE_TAKEOFF_MODE,
  IDLE_MODE
};

// Backward declaration of the CCrazyflie class to make the compiler happy
class CCrazyRadio;
class CTOC;

/*! \brief Crazyflie Nano convenience controller class

  The class containing the mechanisms for starting sensor readings,
  ordering set point setting, selecting and running controllers and
  calculating information based on the current sensor readings. */
class CCrazyflie {
 private:
  // Variables for the specific quadcopter
  int m_quadNum;
  int m_nRadioChannel;
  XferRate m_radioDataRate;
  std::string logfilename;
  std::string activeHeader;

  int m_nAckMissTolerance;
  int m_nAckMissCounter;
  /*! \brief Internal pointer to the initialized CCrazyRadio radio
      interface instance. */
  CCrazyRadio *m_crRadio;
  /*! \brief The current thrust to send as a set point to the
      copter. */
  int m_nThrust;
  /*! \brief The current roll to send as a set point to the copter. */
  float m_fRoll;
  /*! \brief The current pitch to send as a set point to the
      copter. */
  float m_fPitch;
  /*! \brief The current yaw to send as a set point to the copter. */
  float m_fYaw;
  /*! \brief The current desired control set point (position/yaw to
      reach) */

  // Structures to hold the current position and orientation of the quad
  lateralPosition m_currentCameraPosition;
  angularPosition m_currentCameraAngle;
  quaternionPosition m_currentCameraQuat;
  

  // The origin and limits of the local coordinate system
  lateralPosition m_localOrigin;
  limits m_localXlim;
  limits m_localYlim;
  limits m_localZlim;
  coordinateSystem m_systemToUse;

  // Structures to hold the desired setpoints
  lateralPosition m_desiredPositionLocal;
  lateralPosition m_desiredPositionGlobal;
  float m_desiredYaw;
  uint16_t m_baseThrust;

  // The network structure for forwarding packets
  NetworkForwarding *m_network;

  // Stuff for the computation
  int computationType;
  bool computationStarted;

  // An array to hold the takeoff profile
  takeoffProfileStep *takeoffProfile;
  int numTakeoffSteps;

  // Variables needed for the takeoff routine
  int currentStep;
  double lastStepTime;

  float m_startTime;

  bool m_resetController;

  // Last time a position packet was received
  double lastPositionTime;
  double positionTimeDelta;
  double testStandData;

  // File stream for the console output and log file
  std::ofstream file_console;
  std::ofstream file_log;

  // Controller type
  int controllerType;

  

  // Various things for setting the PID parameters of the quadcopter
  std::queue<PID_ControllerParams> pidToSend;

  // Queue for the commands received from the user
  std::queue<userCommands_t> userCommands;
  pthread_mutex_t userCommandsMutex;

  // Mutex to protect the camera data
  pthread_mutex_t cameraDataMutex;

  // Control related parameters
  /*! \brief Maximum absolute value for the roll that will be sent to
      the copter. */
  float m_fMaxAbsRoll;
  /*! \brief Maximum absolute value for the pitch that will be sent to
      the copter. */
  float m_fMaxAbsPitch;
  /*! \brief Maximum absolute value for the yaw that will be sent to
      the copter. */
  float m_fMaxYaw;
  /*! \brief Maximum thrust that will be sent to the copter. */
  int m_nMaxThrust;
  /*! \brief Minimum thrust that will be sent to the copter. */
  int m_nMinThrust;
  double m_dSendSetpointPeriod;
  double m_dSetpointLastSent;
  
  enum State m_enumState;

  // Flags to say whether to send packets
  bool m_sendSetpoints;
  bool m_sendPosition;

  const uint8_t reqSer = 'A';

  // Functions
  bool readTOCParameters();
  bool readTOCLogs();

  /*! \brief Send a set point to the copter controller

    Send the set point for the internal copter controllers. The
    copter will then try to achieve the given roll, pitch, yaw and
    thrust. These values can be set manually but are managed by the
    herein available controller(s) if one is switched on to reach
    desired positions.

    \param fRoll The desired roll value.
    \param fPitch The desired pitch value.
    \param fYaw The desired yaw value.
    \param sThrust The desired thrust value.
    \return Boolean value denoting whether or not the command could be sent successfully. */

  /**
   * Modify if the thrust controller is active or not.
   *
   * @param enabled True to enable controller, false to disable
   * @return True if packet received successfully
   */
  bool modifyThrustController(bool enabled);

  /**
   * Parse the commands that are sitting in the queue.
   * Each call will only parse one command.
   *
   * @return True if command parsed, false otherwise
   */
  bool parseReceivedUserCommand();


 public:
  /*! \brief Constructor for the copter convenience class

    Constructor for the CCrazyflie class, taking a CCrazyRadio radio
    interface instance as a parameter.

    \param crRadio Initialized (and started) instance of the
    CCrazyRadio class, denoting the USB dongle to communicate
    with. */
    lateralPosition m_currentLocalPosition;
    CTOC *m_tocParameters;
    CTOC *m_tocLogs;
    int testStandError = 0;
    TestStand testStand;
    bool allowLogging = false;

  CCrazyflie(CCrazyRadio *crRadio, int radioChannel, XferRate dataRate, int quadNum, double startTime);
  /*! \brief Destructor for the copter convenience class

    Destructor, deleting all internal variables (except for the
    CCrazyRadio radio instance given in the constructor). */
  ~CCrazyflie();

  int getRadioChannel();
  XferRate getRadioDataRate();

  static void* startTestStand(void* args);

  void testStandLoop();

  void stopThread();

  bool m_exitThread = false;

  bool testStandFlag;

  /*
   * Open a log file for the quadcopter
   *
   * @param baseFileName The base filename for the logfile
   */
  void openLogFile(char *baseFileName);

  void printUpdatedHeader();

  bool loadLoggingBlocksFromFile(std::string blockFileName);

  bool resetLoggingBlocks();

  bool setTestStand(serial::Serial* ptr);

  bool enableLogging(std::string strBlockName);

  bool disableLogging(std::string strBlockName);
  
  /*
   * Write data to the log file
   */
  void writeLogData();

  /**
   * Pass a command from the user into this Crazyflie
   *
   * @param command The command to pass in
   */
  void passUserCommand(userCommands_t command);

  int getQuadcopterNumber();

  std::string getlogfile();

  /*
   * Callback called whenever there is a new VRPN position packet.
   * Note: This function is static in the class.
   *
   * @param crazyflie Pointer to the crazyflie object (cast as a void *)
   * @param t The tracker results
   */
  static void VRPN_CALLBACK vrpnTrackerCallback(void *crazyflie, const vrpn_TRACKERCB t);

  void setNetworkForwarding(NetworkForwarding *network);

  /**
   * Query the type of controller that the Crazyflie is using
   *
   * @return Controller type
   */
  int queryControllerType();
  int getControllerType();
  std::string getControllerTypeString();

  /**
   * Enable the thrust controller.
   */
  bool enableThrustController();
  bool disableThrustController();

  void setCameraPosition(lateralPosition cameraPosition);
  void setCameraAngle(angularPosition cameraAngle);
  void setCameraQuat(quaternionPosition cameraQuat);

  /*
   * These functions will return the current quadcopter
   * lateral position as reported by the camera system.
   */
  float getPositionX(coordinateSystem coor);
  float getPositionY(coordinateSystem coor);
  float getPositionZ(coordinateSystem coor);

  /*
   * These functions will return the current quadcopter
   * angular position as reported by the quadcopter sensors.
   */
  float getQuadRoll();
  float getQuadPitch();
  float getQuadYaw();

  /*
   * These functions will return the current quadcopter
   * angular position as reported by the camera system.
   */
  float getCameraRoll();
  float getCameraPitch();
  float getCameraYaw();

  /*
   * These functions will return the current controller setpoints.
   */
  float getSetpointX(coordinateSystem coor);
  float getSetpointY(coordinateSystem coor);
  float getSetpointZ(coordinateSystem coor);
  float getSetpointYaw();
  uint16_t getBaseThrust();

  // Return the time delta for the most recent position packet
  double getPositionTimeDelta();

  /**
   * These functions will set the setpoints of the quadcopter
   */
  void setSetpointX(float setpoint, coordinateSystem coor);
  void setSetpointY(float setpoint, coordinateSystem coor);
  void setSetpointZ(float setpoint, coordinateSystem coor);
  void setSetpointYaw(float setpoint);
  void setBaseThrust(uint16_t thrust);

  void setLocalOrigin(lateralPosition newOrigin);
  void setCoordinateSystem(coordinateSystem coor);
  coordinateSystem getCoordinateSystem();


  /*
   * Computation functions
   */
  int getComputationType();
  int queryComputationType();
  std::string getComputationTypeString();
  void enableComputation();
  void disableComputation();
  void configureComputation( void *config, uint8_t length);

  /**
   * Set the computation data rate in Hz.
   * Note: Maximum rate of 1000 Hz.
   *
   * @param rate The computation rate in Hz
   */
  void setComputationRate(uint16_t rate);

  /**
   * Reset the controller on the quadcopter
   */
  void resetController();

  /**
   * This function will set the flight mode of the Crazyflie
   */
  void setFlightMode( enum FlightMode mode);

  /*
   * This function will set the takeoff profile to use.
   *
   * @param profile Pointer to the takeoff profile to use
   * @param numSteps The number of steps in the takeoff profile
   * 
   */
  void setTakeoffProfile( takeoffProfileStep *profile, int numSteps );

  /*
   * This function will cycle through the takeoff profile for the Crazyflie.
   */
  void takeoffCycle();

  /*! \brief Manages internal calculation operations

    Should be called during every 'cycle' of the main program using
    this class. Things like sensor reading processing, integral
    calculation and controller signal application are performed
    here. This function also triggers communication with the
    copter. Not calling it for too long will cause a disconnect from
    the copter's radio.

    \return Returns a boolean value denoting the current status of the
    radio dongle. If it returns 'false', the dongle was most likely
    removed or somehow else disconnected from the host machine. If it
    returns 'true', the dongle connection works fine. */
  bool cycle();
  /*! \brief Signals whether the copter is in range or not

    Returns whether the radio connection to the copter is currently
    active.

    \return Returns 'true' is the copter is in range and radio
    communication works, and 'false' if the copter is either out of
    range or is switched off. */
  bool copterInRange();

  /*! \brief Whether or not the copter was initialized successfully.

    \returns Boolean value denoting the initialization status of the
    copter communication. */
  bool isInitialized();

  void setPID_Constants( PID_ControllerParams params );

  bool sendPID_Constants( PID_ControllerParams params, std::string str );

  bool sendSGSF_Gains( CONTROLLER_SGSF_GAINS gains);
/*
  void setXYZYawPosition(float xPosition, float yPosition, float zPosition,
                         float yawPosition);

  void setXYZYawSetpoint(float xSetpoint, float ySetpoint, float zSetpoint,
                         float yawSetpoint, uint16_t baseThrust, 
                         uint8_t resetController);
*/
  /*
   * Send setpoints for the quadcopter
   */
  bool sendPositionSetpoint();

  /*
   * Send the current position to the quad
   */
  bool sendCameraData();

  void radioLogging(bool logging);


  void displayLoggingBlocksInitialized();

  /**
   * Add a new logging block to the quadcopter
   * 
   * @param name The name of the logging block
   * @param frequency The frequency to log (in Hz)
   */
  bool addLoggingBlock(const char *name, uint16_t frequency, uint16_t id);

  /**
   * Remove a logging block (stop logging)
   *
   * @param name The name of the logging block
   */
  void removeLoggingBlock(const char *name);

  /**
   * Add a log entry to a logging block
   *
   * @param blockName The block to add the entry to
   * @param entryName The entry to add
   */
  bool addLoggingEntry(const char *blockName, const char *entryName);

  /*
   * Callback to pass a packet back to the quadcopter
   *
   * @param packet Pointer to the CRTP packet that was received
   */
  void callback_Packet(CCRTPPacket *crtpPacket);

//  void CCrazyflie::setFlightMode(enum FLIGHT_MODE);

  /*! \brief Read back a sensor value you subscribed to

    Possible sensor values might be:
    * stabilizer.yaw
    * stabilizer.roll
    * stabilizer.pitch
    * pm.vbat

    The possible key names strongly depend on your firmware. If you
    don't know what to do with this, just use the convience functions
    like roll(), pitch(), yaw(), and batteryLevel().

    \return Double value denoting the current value of the requested
    log variable. */
  double sensorDoubleValue(std::string strName);

  /*! \brief Report the current battery level

    \return Double value denoting the battery level as reported by the
    copter. */
  double batteryLevel();

  double readTestStand();

  float batteryState();

  enum State getCurrentState();

  double currentTime();

  int radioChannel() { return m_nRadioChannel; }		//Added for switching radio channels
//  void cheat_process_packets();

  bool sendSetpoint(float fRoll, float fPitch, float fYaw, short sThrust);		//Moved From Private

  enum FlightMode m_enumFlightMode;	//JN: ADDED enum for flight mode

};


#endif /* __C_CRAZYFLIE_H__ */

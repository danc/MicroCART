#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <err.h>
#include <unistd.h>

#define DEFAULT_ADAPTER_SOCKET "/home/bitcraze/projects/crazyflie_groundstation.socket"
//#define DEFAULT_ADAPTER_SOCKET "./crazyflie_groundstation.socket"
#define SOCKET_ENV "ADAPTER_SOCKET"

struct backend_conn {
        FILE * socket;
        size_t len;
        char * buf;
};

int adapterWrite(struct backend_conn *conn, const char * line) {
    return fputs(line, conn->socket);
}

int main() {
        int s;
        struct sockaddr_un remote;
        char str[100];

        if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
                perror("socket");
                exit(1);
        }

        struct backend_conn * conn = NULL;

        printf("Trying to connect...");

        remote.sun_family = AF_UNIX;
        char * sock_env = getenv(SOCKET_ENV);
        strcpy(remote.sun_path, sock_env ? sock_env : DEFAULT_ADAPTER_SOCKET);
        if (connect(s, (struct sockaddr *)&remote, sizeof(remote))  == -1) {
                perror("connect");
                goto fail_final;
        }

        conn = malloc(sizeof(struct backend_conn));
        if (conn == NULL) {
                perror("malloc");
                goto fail_sock;
        }

        conn->len = 0;
        conn->buf = NULL;
        conn->socket = fdopen(s, "r+");
        if (conn->socket == NULL) {
                perror("fdopen");
                goto fail_malloc_conn;
        }
        if (setvbuf(conn->socket, NULL, _IONBF, 0)) {
                warn("setvbuf");
                printf("bad");
        }

        printf("Success\n");
        /* success */
        goto fail_final;

fail_malloc_conn:
        free(conn);
        conn = NULL;
fail_sock:
        close(s);
        printf("Failure\n");
        return 1;
fail_final: ;
        //return conn;

        //User input to adapter
        int userExit;  
        userExit  = 0;
        char buffer[64];
        char msg[64];
        while (!userExit) {
                scanf("%s", buffer);
                printf("Input: %s\n", buffer);
                if (strcmp(buffer, "exit") == 0) {
                        userExit = 1;
                } else {
                    snprintf(msg, 64, "%s\n", buffer);
                    if (adapterWrite(conn, msg) < 0) {
                        userExit = 1;
                        printf("Error writing to adapter\n");
                    }        
                }
        }

        //Disconnect from socket
        fclose(conn->socket);
        if (conn->buf) {
                free(conn->buf);
        }
        free(conn);

        return 0;
}

This branch has an updated userInput thread that uses a socket for communication. To build the groundstation run `make`. To build the test connection run `make test` and then the `test_connection' can be run which will accept user
input.

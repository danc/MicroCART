//
// Created by dawehr on 10/24/2016.
//

#ifndef CIRC_BUFFER_H
#define CIRC_BUFFER_H

#define CIRC_BUFFER_SIZE 1024
#include <unistd.h>
#include <stdio.h>
#include <string.h>

struct data_chunk {
    size_t length;
    unsigned char* data;
};

/*
 * Returns the largest contiguous chunk from the buffer
 * Does not move the buffer forward. You must call markConsumed to
 * mark the area as free.
 */
struct data_chunk getChunk();

/*
 * Marks the n_consumed bytes as used, and that
 * area of the buffer can be used for new data now
 */
void markConsumed(size_t n_consumed);

/*
 * Places data into the circular buffer. Returns the number of bytes stored.
 * Will store the entire chunk, unless there is not enough remaining size in the buffer
 */
size_t putChunk(struct data_chunk);

/*
 * Returns the remaining size in the buffer
 */
size_t get_buffer_size();

#endif //CIRC_BUFFER_H


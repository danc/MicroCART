import socket
import serial
import time

TCP_IP = "192.168.1.1"
TCP_PORT = 8080

msg_size = 500
print_interval = 100
message = bytes(i % 256 for i in range(msg_size))
dropped = True
loop_time = 0.005

ser = serial.Serial('COM6', 921600, timeout=0.01)
ser.reset_input_buffer()

sent_cnt = 0
recvd_cnt = 0
outer_start_time = time.perf_counter()
received = []
while True:
    if dropped:
        attempts = 0
        while attempts < 5:
            print("Trying to connect")
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(2)
            try:
                sock.connect((TCP_IP, TCP_PORT))
                dropped = False
                break
            except:
                attempts += 1
        if dropped:
            print("Failed to connect")
            break
        print("connected")

    start_time = time.perf_counter()
    try:
        ser.write(message)
        sent_cnt += 1
    except Exception as e:
        print("Failed to send all data")
        continue

    # while len(received) < msg_size:
    try:
        amt_in_air = (sent_cnt - recvd_cnt) * msg_size - len(received)
        just_received = sock.recv(1024)
        received.extend(just_received)
        if len(just_received) == 0:
            print("Your socket broke")
            break
    except Exception as e:
        print("Exception when receiving: ", e)
        break
    if len(received) >= msg_size:
        recvd_packet = received[:msg_size]
        received = received[msg_size:]
        recvd_cnt += 1
        if bytes(recvd_packet) != message:
            print("Received data does not match")
    if sent_cnt % print_interval == 0:
        print("Sent {}, received {}".format(sent_cnt, recvd_cnt))
        print("Average send time was {}".format((time.perf_counter() - outer_start_time)/print_interval))
        outer_start_time = time.perf_counter()
        
    while time.perf_counter() - start_time <= loop_time:
        pass

    #debug_msg = ser.read(4096)
    #if len(debug_msg) != 0:
    #    print(debug_msg.decode())

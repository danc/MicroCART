import socket
import time
import csv

TCP_IP = "192.168.1.1"
# UDP_IP = "127.0.0.1"
TCP_PORT = 8080

# sock.bind(('', UDP_PORT))

message = bytes(range(36))
times_full = []
times_network = []
times = [0.0]*200
dropped = True
response = bytes("initial", 'ASCII')
addr = "initial"
recvd_data = []
for i in range(200):
    if dropped:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        sock.settimeout(2)
        sock.connect((TCP_IP, TCP_PORT))
        dropped = False
    send_msg = list(message)
    send_msg[0] = i
    send_msg = bytes(send_msg)

    send_time = time.perf_counter()
    times[i] = send_time
    sock.send(send_msg)
    try:
        response = sock.recv(1024)
        recvd_data.extend(response)
    except:
        print("timed out")
        dropped = True
    if len(recvd_data) >= 36:
        end_time = time.perf_counter()
        response = bytes(recvd_data[0:36])
        recvd_data = recvd_data[36:]
        msg_id = int(response[0])
        latency = end_time - times[msg_id]
        # serial_time = int.from_bytes(response[0:4], byteorder='big') / 1000
        serial_time = 0
        # times_full.append(1000 * (end_time - send_time) - 0)
        times_full.append(1000 * latency)
        times_network.append(1000 * (end_time - send_time) - serial_time)
        print("received " + str(response) + " in " + str(times_full[-1]) + " from " + str(addr))
    while time.perf_counter() - send_time < 0.01:
        pass

    with open("tcp_dist.csv", 'w', newline='') as f:
        writer = csv.writer(f)
        for t in times_network:
            writer.writerow([t])

for time in [times_full, times_network]:
    print("lowest: " + str(min(time)))
    print("highest: " + str(max(time)))
    print("median: " + str(sorted(time)[int(len(time) / 2)]))
    print("average; " + str(sum(time) / len(time)))


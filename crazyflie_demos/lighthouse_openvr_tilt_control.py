#!/usr/bin/env python3
# Demo that uses a VR controller's orientation as roll and pitch control 
# and trigger as throttle control
# clicking the touch pad switches to height hold mode.
# When in height hold mode, pulling the trigger switches to manual throttle mode
# click the left or right side of the touchpad to yaw left or right

from math import asin, atan2
import math
import sys
import time

import openvr

import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from cflib.crazyflie.syncLogger import SyncLogger
from sqlalchemy import Integer, null

# URI to the Crazyflie to connect to
uri = 'radio://0/40/2M'

print('Opening')
vr = openvr.init(openvr.VRApplication_Other)
print('Opened')

position = [0,0,0]

# Find first controller or tracker
controllerId = None
poses = vr.getDeviceToAbsoluteTrackingPose(openvr.TrackingUniverseStanding, 0,
                                           openvr.k_unMaxTrackedDeviceCount)
for i in range(openvr.k_unMaxTrackedDeviceCount):
    if poses[i].bPoseIsValid:
        device_class = vr.getTrackedDeviceClass(i)
        if device_class == openvr.TrackedDeviceClass_Controller or \
           device_class == openvr.TrackedDeviceClass_GenericTracker:
            controllerId = i
            break

if controllerId is None:
    print('Cannot find controller or tracker, exiting')
    sys.exit(1)


# calculates the vr controller's roll pitch and yaw from the pose translation matrix retrived from openvr
def getRPY(pose):
    roll = 0
    pitch = 0
    yaw = 0

    if(pose[0][0] == 1.0):
        yaw = atan2(pose[0][2], pose [2][3])
        pitch = 0
        roll = 0
    elif(pose[0][0] == -1.0):
        yaw = atan2(pose[0][2], pose[2][3])
        pitch = 0
        roll = 0
    else:
        yaw = atan2(-pose[2][0], pose[0][0])
        roll = asin(pose[1][0])
        pitch = atan2(-pose[1][2], pose[1][1])

    roll = math.degrees(roll)
    pitch = math.degrees(pitch)
    yaw = math.degrees(yaw)
    
    return roll, pitch, yaw


def wait_for_position_estimator(scf):
    print('Waiting for estimator to find position...')

    log_config = LogConfig(name='Kalman Variance', period_in_ms=500)
    log_config.add_variable('kalman.varPX', 'float')
    log_config.add_variable('kalman.varPY', 'float')
    log_config.add_variable('kalman.varPZ', 'float')

    var_y_history = [1000] * 10
    var_x_history = [1000] * 10
    var_z_history = [1000] * 10

    threshold = 0.001

    with SyncLogger(scf, log_config) as logger:
        for log_entry in logger:
            data = log_entry[1]

            var_x_history.append(data['kalman.varPX'])
            var_x_history.pop(0)
            var_y_history.append(data['kalman.varPY'])
            var_y_history.pop(0)
            var_z_history.append(data['kalman.varPZ'])
            var_z_history.pop(0)

            min_x = min(var_x_history)
            max_x = max(var_x_history)
            min_y = min(var_y_history)
            max_y = max(var_y_history)
            min_z = min(var_z_history)
            max_z = max(var_z_history)

            # print("{} {} {}".
            #       format(max_x - min_x, max_y - min_y, max_z - min_z))

            if (max_x - min_x) < threshold and (
                    max_y - min_y) < threshold and (
                    max_z - min_z) < threshold:
                break


def reset_estimator(scf):
    cf = scf.cf
    cf.param.set_value('kalman.resetEstimation', '1')
    time.sleep(0.1)
    cf.param.set_value('kalman.resetEstimation', '0')

    wait_for_position_estimator(cf)

#update data in the position array with the logging data from the crazyflie
def position_callback(timestamp, data, logconf):
    position[0] = data['kalman.stateX']
    position[1] = data['kalman.stateY']
    position[2] = data['kalman.stateZ']
    #print('pos: ({}, {}, {})'.format(position[0], position[1], position[2]))


def start_position_logging(scf):
    log_conf = LogConfig(name='Position', period_in_ms=100)
    log_conf.add_variable('kalman.stateX', 'float')
    log_conf.add_variable('kalman.stateY', 'float')
    log_conf.add_variable('kalman.stateZ', 'float')

    scf.cf.log.add_config(log_conf)
    log_conf.data_received_cb.add_callback(position_callback)
    log_conf.start()


def run_sequence(scf):
    cf = scf.cf

    heightHoldMode = False
    touchpadLeftHeld = False
    touchpadRightHeld = False
    heightHold = 0

    print("VR controller active")

    while True:

        #update controller roll, pitch, yaw and button inputs
        poses = vr.getDeviceToAbsoluteTrackingPose(
            openvr.TrackingUniverseStanding, 0,
            openvr.k_unMaxTrackedDeviceCount)
        controller_state = vr.getControllerState(controllerId)[1]
        controller_pose = poses[controllerId]
        pose = controller_pose.mDeviceToAbsoluteTracking
        roll, pitch, yaw = getRPY(pose)

        # trigger value
        trigger = (controller_state.rAxis[1].x)
        thrust = int(trigger * 60000)

        touchpadClicked = False
        triggerPressed = False

        #update buttons
        event = openvr.VREvent_t()
        vr.pollNextEvent(event)
        if(event.eventType == openvr.VREvent_ButtonPress or event.eventType == openvr.VREvent_ButtonUnpress):
            if(event.data.controller.button == openvr.k_EButton_SteamVR_Touchpad):
                if(controller_state.rAxis[0].x < -0.5):
                    if(event.eventType == openvr.VREvent_ButtonPress):
                        touchpadLeftHeld = True
                    elif(event.eventType == openvr.VREvent_ButtonUnpress):
                        touchpadLeftHeld = False
                elif(controller_state.rAxis[0].x > 0.5):
                    if(event.eventType == openvr.VREvent_ButtonPress):
                        touchpadRightHeld = True
                    elif(event.eventType == openvr.VREvent_ButtonUnpress):
                        touchpadRightHeld = False
                else:
                    touchpadClicked = True
            if(event.data.controller.button == openvr.k_EButton_SteamVR_Trigger):
                triggerPressed = True

        if(touchpadLeftHeld): 
            yawRate = -90
        elif(touchpadRightHeld): 
            yawRate = 90
        else:
            yawRate = 0

        # change what setpoint to send based on mode
        if(heightHoldMode):
            #height hold mode
            cf.commander.send_hover_setpoint(-pitch * 0.02, roll * 0.02, yawRate, heightHold)
            if(triggerPressed):
               heightHoldMode = False
        else:
            #manual throttle mode
            cf.commander.send_setpoint(-roll, -pitch, yawRate, thrust)
            if(touchpadClicked):
               heightHold = position[2]
               heightHoldMode = True
        

        time.sleep(0.02)

    cf.commander.send_setpoint
    (0, 0, 0, 0)
    # Make sure that the last packet leaves before the link is closed
    # since the message queue is not flushed before closing
    time.sleep(0.1)


if __name__ == '__main__':
    cflib.crtp.init_drivers()

    with SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache')) as scf:
        reset_estimator(scf)
        start_position_logging(scf)
        run_sequence(scf)

    openvr.shutdown()

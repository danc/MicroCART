EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x05_Male J0
U 1 1 61DA17BD
P 4000 1450
F 0 "J0" V 3850 1450 50  0000 L CNN
F 1 "Conn_01x05_Male" V 3950 1150 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Horizontal" H 4000 1450 50  0001 C CNN
F 3 "~" H 4000 1450 50  0001 C CNN
	1    4000 1450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 61DA7D2B
P 4450 3300
F 0 "R2" V 4350 3300 50  0000 C CNN
F 1 "100" V 4550 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4450 3300 50  0001 C CNN
F 3 "~" H 4450 3300 50  0001 C CNN
	1    4450 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 3300 4550 3300
$Comp
L Device:R_Small R0
U 1 1 61DB34DC
P 4000 2450
F 0 "R0" H 4059 2496 50  0000 L CNN
F 1 "1.5k or 3.3k" H 4059 2405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4000 2450 50  0001 C CNN
F 3 "~" H 4000 2450 50  0001 C CNN
	1    4000 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 61DB3D6C
P 4000 2850
F 0 "R1" H 4059 2896 50  0000 L CNN
F 1 "3.3k or 6.8k" H 4059 2805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4000 2850 50  0001 C CNN
F 3 "~" H 4000 2850 50  0001 C CNN
	1    4000 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3000 4000 2950
Wire Wire Line
	3650 3300 3750 3300
$Comp
L Device:R_Small R4
U 1 1 61E316B5
P 4000 2150
F 0 "R4" H 4059 2196 50  0000 L CNN
F 1 "100 or 0" H 4059 2105 50  0000 L CNN
F 2 "trinket_m0:DIN0207_L6.3mm_D2.5mm_P10.16mm_H_with_jumper" H 4000 2150 50  0001 C CNN
F 3 "~" H 4000 2150 50  0001 C CNN
	1    4000 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 61E32E5C
P 4000 1850
F 0 "R3" H 4059 1896 50  0000 L CNN
F 1 "100 or 0" H 4059 1805 50  0000 L CNN
F 2 "trinket_m0:DIN0207_L6.3mm_D2.5mm_P10.16mm_H_with_jumper" H 4000 1850 50  0001 C CNN
F 3 "~" H 4000 1850 50  0001 C CNN
	1    4000 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2350 4000 2250
Wire Wire Line
	4000 2050 4000 1950
Wire Wire Line
	4000 1750 4000 1650
Wire Wire Line
	4200 1650 4200 1700
NoConn ~ 4100 1650
NoConn ~ 3800 1650
Wire Wire Line
	3650 3150 3650 3300
$Comp
L power:GND #PWR0101
U 1 1 61EA4E2A
P 4700 3350
F 0 "#PWR0101" H 4700 3100 50  0001 C CNN
F 1 "GND" H 4705 3177 50  0000 C CNN
F 2 "" H 4700 3350 50  0001 C CNN
F 3 "" H 4700 3350 50  0001 C CNN
	1    4700 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3300 4700 3350
Wire Wire Line
	4700 3000 4700 3300
Wire Wire Line
	4000 3000 4700 3000
Connection ~ 4700 3300
$Comp
L power:GND #PWR0102
U 1 1 61EA61B8
P 4500 1700
F 0 "#PWR0102" H 4500 1450 50  0001 C CNN
F 1 "GND" H 4505 1527 50  0000 C CNN
F 2 "" H 4500 1700 50  0001 C CNN
F 3 "" H 4500 1700 50  0001 C CNN
	1    4500 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1700 4500 1700
$Comp
L Seeeduino-XIAO:SeeeduinoXIAO U1
U 1 1 61EF3051
P 5950 2850
F 0 "U1" H 5925 1911 50  0000 C CNN
F 1 "SeeeduinoXIAO" H 5925 1820 50  0000 C CNN
F 2 "Seeeduino_XIAO:Seeeduino XIAO-MOUDLE14P-2.54-21X17.8MM" H 5600 3050 50  0001 C CNN
F 3 "" H 5600 3050 50  0001 C CNN
	1    5950 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0105
U 1 1 61EF92FE
P 3650 3150
F 0 "#PWR0105" H 3650 3000 50  0001 C CNN
F 1 "+3V3" H 3665 3323 50  0000 C CNN
F 2 "" H 3650 3150 50  0001 C CNN
F 3 "" H 3650 3150 50  0001 C CNN
	1    3650 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2550 4000 2650
$Comp
L Switch:SW_Push SW0
U 1 1 61DA44FA
P 3950 3300
F 0 "SW0" H 3950 3250 50  0000 C CNN
F 1 "SW_Push" H 3950 3150 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3950 3500 50  0001 C CNN
F 3 "~" H 3950 3500 50  0001 C CNN
	1    3950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3300 4200 3300
Text GLabel 4100 2650 2    50   Input ~ 0
MA3_SCALED
Wire Wire Line
	4100 2650 4000 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 4000 2750
Text GLabel 4200 3550 2    50   Input ~ 0
SET_ZERO
Wire Wire Line
	4200 3550 4200 3300
Connection ~ 4200 3300
Wire Wire Line
	4200 3300 4350 3300
$Comp
L power:+5V #PWR?
U 1 1 61EFDB02
P 3550 1600
F 0 "#PWR?" H 3550 1450 50  0001 C CNN
F 1 "+5V" H 3565 1773 50  0000 C CNN
F 2 "" H 3550 1600 50  0001 C CNN
F 3 "" H 3550 1600 50  0001 C CNN
	1    3550 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1650 3900 1800
Wire Wire Line
	3900 1800 3550 1800
Wire Wire Line
	3550 1800 3550 1600
Text GLabel 6900 2850 2    50   Input ~ 0
MA3_SCALED
Text GLabel 6900 3000 2    50   Input ~ 0
SET_ZERO
Wire Wire Line
	6900 2850 6750 2850
Wire Wire Line
	6750 3000 6900 3000
$Comp
L power:GND #PWR?
U 1 1 61EFFE15
P 7150 2600
F 0 "#PWR?" H 7150 2350 50  0001 C CNN
F 1 "GND" H 7250 2500 50  0000 C CNN
F 2 "" H 7150 2600 50  0001 C CNN
F 3 "" H 7150 2600 50  0001 C CNN
	1    7150 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 61F02420
P 6800 2250
F 0 "#PWR?" H 6800 2100 50  0001 C CNN
F 1 "+5V" H 6815 2423 50  0000 C CNN
F 2 "" H 6800 2250 50  0001 C CNN
F 3 "" H 6800 2250 50  0001 C CNN
	1    6800 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 61F03587
P 7000 2250
F 0 "#PWR?" H 7000 2100 50  0001 C CNN
F 1 "+3V3" H 7015 2423 50  0000 C CNN
F 2 "" H 7000 2250 50  0001 C CNN
F 3 "" H 7000 2250 50  0001 C CNN
	1    7000 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2400 6800 2400
Wire Wire Line
	6800 2400 6800 2250
Wire Wire Line
	6750 2700 7000 2700
Wire Wire Line
	7000 2700 7000 2250
Wire Wire Line
	7150 2550 6750 2550
Wire Wire Line
	6200 1950 6200 1900
Wire Wire Line
	6200 1900 7150 1900
Wire Wire Line
	7150 1900 7150 2550
Connection ~ 7150 2550
Wire Wire Line
	7150 2550 7150 2600
NoConn ~ 6750 3150
NoConn ~ 6750 3300
NoConn ~ 6050 3700
NoConn ~ 5850 3700
NoConn ~ 5100 3300
NoConn ~ 5100 3150
NoConn ~ 5100 3000
NoConn ~ 5100 2850
NoConn ~ 5100 2700
NoConn ~ 5100 2550
NoConn ~ 5100 2400
NoConn ~ 5750 1950
NoConn ~ 5900 1950
NoConn ~ 6050 1950
$EndSCHEMATC

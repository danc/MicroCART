% Read in data 
data = readtable('C:\Users\Andy\Documents\School\MicroCART\GitRepo\MicroCART_17-18\controls\dataCollection\Thrust and Drag Constant Data v2.xlsx');

filePath_noLoadCurrentData = 'C:\Users\Andy\Documents\School\MicroCART\GitRepo\MicroCART_17-18\controls\dataCollection\noLoadCurrentv2.xlsx';

[I_0, I_1, I_2, motorCommand_error, error, residual_error, residual_error_ConstantIf] = zeroLoadCurrent(filePath_noLoadCurrentData);

Pmin = 1e5;
Pmax = 2e5;
Rm = 0.2308;
Kv = 96.3422;
Kq = 96.3422;
Kd = 1.0317e-7;

% Convert RPM to angular speed of each rotor.
rotor_speed_0 = data.(2) * (pi/30);
rotor_speed_1 = data.(3) * (pi/30);
rotor_speed_2 = data.(4) * (pi/30);
rotor_speed_3 = data.(5) * (pi/30);

motorCommand = data.(1);
Vb = data.(6);

u = ((data.(1)) - Pmin)/(Pmax - Pmin);

If = I_0 * sign(rotor_speed_0) + I_1 * rotor_speed_0 + I_2 * rotor_speed_0.^2;
w_num = -1 + sqrt( 1 - 4*Rm*Kv*Kq*Kd*(Kv*Rm*If - Kv*u.*Vb));
w_den = 2*Rm*Kv*Kq*Kd;
w = w_num / w_den;

figure()
hold on
plot(motorCommand, w); grid()
plot(motorCommand, rotor_speed_0);

xlabel('Motor Command');
ylabel('Rotor Speed (rad/s)')

residual_error

residual_error_ConstantIf
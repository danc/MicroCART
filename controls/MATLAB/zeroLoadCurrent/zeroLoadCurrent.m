function [I_0, I_1, I_2, motorCommand, error, residual_error, residual_error_constantIf] = zeroLoadCurrent(absoluteFilePath)
%This function takes in an absolute file path to a .csv or Excel file with 
%the following format:
%   Second column should be the motor speed in revolutions per minute
%   Third column should be the current (A)
%absoluteFilePath = 'C:\Users\Andy\Documents\MATLAB\MicroCART\Zero Load Current\No Load Friction Current.csv';

    %Get the data from the file
    data = readtable(absoluteFilePath);

    %Extract the motor speed column and convert it to radians per second
    motorCommand = data.(1);
    motorSpeed_rpm = data.(2);
    motorSpeed_radPerSec = motorSpeed_rpm * pi/30;

    %Extract the current column
    current = data.(3);

    %Define the three columns of the A matrix for the least squares 
    %approximation, using this equation:
    %   
    %   I_f = sgn(motorSpeed)*I_0 + motorSpeed*I_1 + (motorSpeed^2)*I_2
    %
    %   for this least squares equation: A*x = b (where A and b are matrices)
    %
    % where: sgn(motorSpeed) -- first column of the A matrix,
    %        motorSpeed      -- second column of the A matrix
    %        motorSpeed^2    -- third column of the A matrix
    %        I_f             -- b matrix
    %
    %  and: [I_0; I_1; I_2]  -- x vector that we are solving for
    A_col1 = sign(motorSpeed_radPerSec);
    A_col2 = motorSpeed_radPerSec;
    A_col3 = motorSpeed_radPerSec .^ 2;

    %Create the A matrix from its three columns
    A = [A_col1, A_col2, A_col3];

    %Define the b matrix
    b = current;

    %Least squares approximation -- solving for x, which is I_vector
    I_vector = (A' * A)^(-1) * A' * b;

    %Get the error vector
    error = A*I_vector - b;
    
    If_constant = 0.511;
    error_withIfConstant = A*[If_constant; 0; 0] - b;
    
    %Get the residual error
    residual_error = sum(error.^2);
    
    residual_error_constantIf = sum(error_withIfConstant.^2);

    %Extract the components of the vector
    I_0 = I_vector(1);
    I_1 = I_vector(2);
    I_2 = I_vector(3);

end %function

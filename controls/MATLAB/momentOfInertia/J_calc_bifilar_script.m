
% inches2meters = 0.0254 ; 

%%
% %Jxx
% m = 1.244*ones(5,1); 
% D = (12+3/8)*inches2meters*ones(5,1); 
% L = (81+3/4)*inches2meters*ones(5,1); 
% t = [ 94.58 ; 93.91 ; 93.81 ; 94.16 ; 94.04 ] ; 
% n = 50*ones(5,1); 

% %Jyy
% m = 1.244*ones(5,1); 
% D = (12+3/8)*inches2meters*ones(5,1); 
% L = (91+3/4)*inches2meters*ones(5,1); 
% t = [ 103.31 ; 103.15 ; 103.31 ; 103.47 ; 103.13 ] ; 
% n = 50*ones(5,1); 


% %Jzz 
% m = 1.244*ones(5,1); 
% D = (19+3/16)*inches2meters*ones(5,1); 
% L = (96+1/4)*inches2meters*ones(5,1); 
% t = [ 96.64 ; 97.47 ; 97.75 ; 97.35 ; 97.78 ] ; 
% n = 50*ones(5,1); 

load .\git_repos\microCART_17-18\controls\dataCollection\bifilar_inertia_2_18_2017\MicroCART_bifilar_Jxx_2_18_2017 ; 
Jxx_a = J_calc_bifilar( m , g, D, L, t, n , 'average')
Jxx_ls = J_calc_bifilar( m , g, D, L, t, n , 'least squares')

clear D L m n t 
load .\git_repos\microCART_17-18\controls\dataCollection\bifilar_inertia_2_18_2017\MicroCART_bifilar_Jyy_2_18_2017 ; 
Jyy_a = J_calc_bifilar( m , g, D, L, t, n , 'average')
Jyy_ls = J_calc_bifilar( m , g, D, L, t, n , 'least squares')

clear D L m n t 
load .\git_repos\microCART_17-18\controls\dataCollection\bifilar_inertia_2_18_2017\MicroCART_bifilar_Jzz_2_18_2017 ; 
Jzz_a = J_calc_bifilar( m , g, D, L, t, n , 'average')
Jzz_ls = J_calc_bifilar( m , g, D, L, t, n , 'least squares')

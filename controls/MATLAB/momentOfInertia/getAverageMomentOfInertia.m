function averageMOI = ...
    getAverageMomentOfInertia( directoryLocation, massUsed_grams_array )
    %Given a directory of data labeled "Test_1", "Test_2", etc, will find
    %the moment of inertia for each data set, using the mass used in grams
    %at the corresponding index of the massUsed_grams_array. And from
    %finding all of the moments of inertia, in this way, this function will
    %return the average.
    
    %First find the number of tests we need to run (will be equal to the
    %length of the massUsed_grams_array)
    numRuns = length(massUsed_grams_array);
    
    %Create a sum variable, to which we will add our calculated MOI values:
    sumOfMOIs = 0;
    
    %For each of the runs, calculate the moment of inertia
    for i = 1:numRuns
    
        %Create the file name for the current run, in string:
        currFileName = ['Test_', num2str(i), '.csv'];
        
        %Create the full file path for the current run:
        currFullFilePath = [directoryLocation, '\', currFileName];
        
        %Get the current mass used in grams
        massUsed_grams = massUsed_grams_array(i);
        
        %Get the moment of inertia for this run
        curr_MOI = getMomentOfInertia(currFullFilePath, massUsed_grams);
        
        %Add this run's moment of inertia value calculated to the sum
        sumOfMOIs = sumOfMOIs + curr_MOI;
        
    end %for i = 1:numRuns
    
    %Get the average moment of inertia value, by dividing by the total
    %number of runs
    averageMOI = sumOfMOIs / numRuns;

end

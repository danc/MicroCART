%Create the massesUsed_grams_array, which represents the masses we used for
%each run, which we keep the same for the runs done for each of the
%testing (yaw, pitch, roll, and their calibration tests as well)
massesUsed_grams = [200.1, 300.1, 100.1];
numTrialsPerMass = 5;
massUsed_grams_array = [ ones(1, numTrialsPerMass) * massesUsed_grams(1), ...
                         ones(1, numTrialsPerMass) * massesUsed_grams(2), ...
                         ones(1, numTrialsPerMass) * massesUsed_grams(3)];

%Set the directory locations for the pitch, roll, and yaw data and their
%calibration runs
topLevelDir = 'C:\Users\Tara\Desktop\Project Documents\Current Project Documents\EE 491\Data\Physics Department Measurements\Moment of Inertia Data';
directoryLocation_yawCalib = [topLevelDir , '\Calibration Testing Yaw'];
directoryLocation_pitchCalib = [topLevelDir, '\Calibration Testing Pitch and Roll'];
directoryLocation_rollCalib = [topLevelDir, '\Calibration Testing Pitch and Roll'];
directoryLocation_yawData = [topLevelDir, '\Yaw Data'];
directoryLocation_pitchData = [topLevelDir, '\Pitch Data'];
directoryLocation_rollData = [topLevelDir, '\Roll Data'];
                     
%Get the average moment of inertia values for the pitch, roll, and yaw data
%as well as their calibration funs
averageMOI_yawCalib = getAverageMomentOfInertia( ...
    directoryLocation_yawCalib, massUsed_grams_array );

averageMOI_pitchCalib = getAverageMomentOfInertia( ...
    directoryLocation_pitchCalib, massUsed_grams_array );

averageMOI_rollCalib = getAverageMomentOfInertia( ...
    directoryLocation_rollCalib, massUsed_grams_array );

averageMOI_yawData = getAverageMomentOfInertia( ...
    directoryLocation_yawData, massUsed_grams_array );

averageMOI_pitchData = getAverageMomentOfInertia( ...
    directoryLocation_pitchData, massUsed_grams_array );

averageMOI_rollData = getAverageMomentOfInertia( ...
    directoryLocation_rollData, massUsed_grams_array );

%Now determine the quadcopter's average moment of inertia about the pitch,
%roll, and yaw axes of rotation
quadYaw = averageMOI_yawData - averageMOI_yawCalib
quadPitch = averageMOI_pitchData - averageMOI_pitchCalib
quadRoll = averageMOI_rollData - averageMOI_rollCalib
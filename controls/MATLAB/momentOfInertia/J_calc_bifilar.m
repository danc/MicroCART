function  J  = J_calc_bifilar( m , g, D, L, t, n , varargin)
% J  = J_calc_bifilar( m , g, D, L, t, n)
% J  = J_calc_bifilar( m , g, D, L, t, n , mode_option)
% 
% m , D , L , t , and n are vectors containing the mass, distance between
% strings, legnth of strings, time, and cycles of a set of tests. g is the
% acceleration of gravity you want to use. 
% 
% mode_option is an optional input which will specify how to calculate J: 
% mode_option = 0 will use averageing of the trials (default) 
% mode_option = 1 will use least squares of the whole data set 

average_strings = {'average','mean','ave'}; 
least_squares_strings = {'least squares','ls'}; 

mode = 0; 
if ~isempty(varargin)
    for k = 1:length(varargin)
        if any(strcmpi(varargin{k},average_strings))
            mode = 0; 
        end
        if any(strcmpi(varargin{k},least_squares_strings))
            mode = 1; 
        end
    end
end

if mode == 0 %averge
        N = length(t); 
        Js = zeros(1,N); 
        for k = 1:N
            Js(k) = m(k)*g*D(k)^2/(16*pi^2*L(k))*(t(k)/n(k))^2; 
        end        
        J = mean(Js); 
end

if mode == 1 %least squares
    J = (16*pi^2*L./(m*g.*D.^2))\(t./n).^2 ; 
end

end


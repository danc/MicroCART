function data = load_IMU_data_from_csv( filename )
% load_IMU_data_from_csv load data from csv file into workspace 
%
%SYNTAX
% data = load_IMU_data_from_csv( filename )
% 
%DESCRIPTION
% ...to be written...
%
%
%Inputs:
% filename - string specifying the file 
%
%Options:
% none 
%
%Outputs:
% data - IMU data loaded into a structure (specify more later)  
%
%EXAMPLES
%
%NOTES
% This function currently assumes the csv format that was used as of the 
% date: 11/15/2016 (m/d/y) 
%
%AUTHORS
% Matt Rich - m87rich@iastate.edu
%
%DEVELOPERS
%

%
%DEVELOPMENT NOTES
%
% dates are in m-d-y format 
%
% Initial bare bones function. 
% - Matt Rich 11-15-2016
%
delimiter = ',';
formatSpec = '%f%f%f%f%f%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);
data.t = dataArray{:, 1};
data.ax = dataArray{:, 2};
data.ay = dataArray{:, 3};
data.az = dataArray{:, 4};
data.gx = dataArray{:, 5};
data.gy = dataArray{:, 6};
data.gz = dataArray{:, 7};


end


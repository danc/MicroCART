%Run through IMU data for all filters (levels 0 to 6):

% for i = 0:1:6
%     
%     fileName = [ 'level' , num2str(i) , '.csv' ];
%     analyzeIMU_data( fileName );
%     
% end

fileName = 'level5.csv';
analyzeIMU_data( fileName );
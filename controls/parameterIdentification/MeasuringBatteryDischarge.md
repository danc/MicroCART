# Measuring Battery Discharge Rate[^1]

## Average Battery Discharge Rate

When flying the quadcopter, the voltage of the attached battery decreases throughout flight,
which has nonnegligible effects on the motor output. Because of this, we want to be able
to form a model for how the battery discharges. The actual battery discharge pattern is nonlinear,
but a simple linearization puts our voltage approximation significantly closer than simply assuming
a constant voltage, so that is the approach that is currently used.

## Variables

- $`V_0`$ - Modeled initial battery voltage
- $`\delta_V`$ - Average battery discharge rate
- $`t_i`$ - Time of $`i^{\text{th}}`$ voltage measurement
- $`V_i`$ - Voltage value of $`i^{\text{th}}`$ measurement
- $`{\mathbf t}`$ - Column vector of all $`t_i`$
- $`{\mathbf V}`$ - Column vector of all $`V_i`$


## Measurement Process

1. Attach a multimeter to measure voltage in parallel with the quad's battery.
1. Set the throttle to a value where the quad is nearly (or barely) hovering (because of the multimeter wires, this is as close as we can safely get to approximating flight).
1. Regularly record time and voltage measurement pairs $`t_i`$ and $`V_i`$ (ideally this step is automated).
1. Use MATLAB to solve the overdetermined system $`\begin{pmatrix}{\mathbf 1} & {\mathbf t}\end{pmatrix}\begin{pmatrix}V_0 \\ \delta_V\end{pmatrix} = {\mathbf V}`$ with the script given below.

## MATLAB Script

The below script assumes that you have your time (as seconds) and voltage (as volts) samples ($`t`$ and $`V`$) as column vectors (if you don't, transpose them before continuing).

```matlab
% A is matrix [1 t];
A = [ones(size(t)), t];

% Least squares to solve system
x = A \ V;

% Extract results for V_0 and delta_V
V_0     = x(1)
delta_V = x(2)

% Plot the samples and linearization
close all; figure; hold on;
plot(t, V, 'bo');
plot(t, V_0 + delta_V*V);
xlabel('Time (s)'); ylabel('Batt. Voltage (V)');
hold off;
```

This should output the proper values for $`V_0`$ and $`\delta_V`$, as well as plot the samples against
the linearization so you can visualize the precision of the approximation.

[^1]: Adapted from Section 5.4 of Matthew Rich's graduate  thesis "Model development, system identification, and control of a quadrotor helicopter"

# Modeling Parameters

This table was moved from a Word document that was last pushed to Git on Feb 5, 2017.
If the date listed for "Last Updated" is this, then the actual update date may be before that.

If the same procedure or document is used for multiple consecutive parameters,
it is given for the first and listed as "same" for subsequent rows. 

| Symbol                   | Nominal Value           | Units                     | Brief Description                                             | Last Updated | Measurement Procedure |
|:------------------------:|:-----------------------:|:-------------------------:|:--------------------------------------------------------------|:-------------|:----------------------|
| $`m_q`$                  | $`0.986`$               | $`kg`$                    | Quadcopter mass                                               | Feb 5, 2017  | Measured with a scale |
| $`m_b`$                  | $`0.204`$               | $`kg`$                    | Battery mass                                                  | Feb 5, 2017  | same                  |
| $`m`$                    | $`1.19`$                | $`kg`$                    | Quadcopter + battery mass                                     | Feb 5, 2017  | same                  |
| $`g`$                    | $`9.81`$                | $`\frac{m}{s^2}`$         | Acceleration of gravity                                       | [constant]   | [constant]            |
| $`J_{xx}`$               | $`0.0218`$              | $`kg\,m^2`$               | Quadrotor + battery moment of inertia around $`b_x`$          | Feb 5, 2017  | [Measuring Moment of Inertia][3] |
| $`J_{yy}`$               | $`0.0277`$              | $`kg\,m^2`$               | Quadrotor + battery moment of inertia around $`b_y`$          | Feb 5, 2017  | same                  |
| $`J_{zz}`$               | $`0.0332`$              | $`kg\,m^2`$               | Quadrotor + battery moment of inertia around $`b_z`$          | Feb 5, 2017  | same                  |
| $`J_{\text{req}}`$       | $`4.2012\times10^{-5}`$ | $`kg\,m^2`$               | Rotor + motor moment of inertia around motor axis of rotation | Feb 5, 2017  | [Measuring Equivalent Moment of Inertia][5] |
| $`K_T`$                  | $`8.6519\times10^{-6}`$ | $`\frac{kg\,m}{rad^2}`$   | Rotor thrust constant                                         | Feb 5, 2017  | [Measuring Thrust and Drag Constants][1] |
| $`K_d`$                  | $`1.0317\times10^{-7}`$ | $`\frac{kg\,m^2}{rad^2}`$ | Rotor drag constant                                           | Feb 5, 2017  | same                  |
| $`K_H`$                  |                         | $`\frac{kg}{rad}`$        | Rotor in-plane drag constant                                  | Feb 5, 2017  | _Matt's thesis 5.5.3, needs doc_ |
| $`\delta_{T_z}`$         |                         | $`\frac{kg}{rad}`$        | Rotor velocity thrust adjustment factor                       | Feb 5, 2017  | _Matt's thesis 5.5.2, needs doc_ |
| $`\lvert r_{h_x}\rvert`$ | $`0.16`$                | $`m`$                     | $`x`$-axis distance from center of mass to a rotor hub        | Feb 5, 2017  | Measure by hand       |
| $`\lvert r_{h_y}\rvert`$ | $`0.16`$                | $`m`$                     | $`y`$-axis distance from center of mass to a rotor hub        | Feb 5, 2017  | same                  |
| $`\lvert r_{h_z}\rvert`$ | $`0.03`$                | $`m`$                     | $`z`$-axis distance from center of mass to a rotor hub        | Feb 5, 2017  | same                  |
| $`R_m`$                  | $`0.2308`$              | $`\Omega`$                | Motor resistance                                              | Feb 5, 2017  | [Measuring Motor Resistance][2] |
| $`K_Q`$                  | $`96.3422`$             | $`\frac{A}{N\,m}`$        | Motor torque constant                                         | Feb 5, 2017  | Specified by motor    |
| $`K_V`$                  | $`96.3422`$             | $`\frac{rad}{V\,s}`$      | Motor back-emf constant                                       | Feb 5, 2017  | Specified by motor    |
| $`i_f`$                  | $`0.511`$               | $`A`$                     | Motor internal friction (no-load) current                     | Feb 5, 2017  | Remove rotor and measure with power supply and ammeter |
| $`P_\bot`$               | $`117000`$              | [none]                    | ESC turn-on duty cycle command                                | Feb 5, 2017  | [Measuring Thrust and Drag Constants][1] |
| $`P_\bot`$               | $`100000`$              | [none]                    | Minimum Zybo output duty cycle command                        | Feb 5, 2017  |                       |
| $`P_\top`$               | $`100000`$              | [none]                    | Maximum Zybo output duty cycle command                        | Feb 5, 2017  |                       |
| $`\delta_V`$             |                         | $`\frac{V}{s}`$           | Approximate constant battery discharge rate                   | Feb 5, 2017  | [Measuring Battery Discharge Rate][4]  |
| $`T_C`$                  | $`0.01`$                | $`s`$                     | Camera system sampling period                                 | Feb 5, 2017  |                       |
| $`\tau_C`$               |                         | $`s`$                     | Camera system total latency                                   | Feb 5, 2017  |                       |

* 0.175 ms single trip latency between camera system and ground station (ping)

[1]: MeasuringThrustAndDragConstants.md
[2]: ../controls/documentation/MeasuringMotorResistance.pdf
[3]: MeasuringMomentOfInertia.md
[4]: MeasuringBatteryDischarge.md
[5]: MeasuringEquivMoment.md

# Measuring Moment of Inertia[^1]

## Bifilar Pendulum

A bifilar pendulum consists of a mass affixed to a pair of vertical wires.
An angular displacement in the plane perpendicular to the wires creates an oscillation that is characterized by the moment of inertia of the mass.
We can measure this oscillation to determine the moments of intertia of the quadcoopter
about all three axes.

## Variables

- $`\theta`$ - Angular position relative to equilibrium
- $`J`$ - Moment of inertia
- $`D`$ - Distance between the wires
- $`m`$ - Mass of the quadcopter
- $`h`$ - Height of wire attachment from quadcopter center of mass
- $`\omega_n`$ - Frequency of oscillations

## Computing the Moment of Inertia

1. Suspend the quadcopter from a pair of wires to form a bifilar pendulum such that the $`z`$-axis of the quad is vertical
1. Introduce and angular displacement (around the vertical axis) to the quadcopter
1. Repeatedly measure the angular position with the VRPN camera system over a number of oscillations
1. Use MATLAB to compute the average frequency of the oscillations, $`\omega_n`$, and use it to compute the moment of inertia from the equation[^2] $`J_{zz} = \frac{mgD^2}{4h\omega_n^2}`$
1. Repeat for the $`x`$- and $`y`$-axes to compute $`J_{xx}`$ and $`J_{yy}`$

## Notes on Setup

- The wires should set up to be parallel (i.e. vertical, so the spacing between them on the quad matches the space where they are affixed above)
- The center of mass should be positioned halfway in between the two wires
- The value of $`D`$ can affect the precision and stability of the measurement, so try spacing the wires differently if the measurements seem inaccurate
- The value of $`h`$ should be maximized for best results, so it is recommend to affix the top of the wires as close to the ceiling as possible

[^1]: Adapted from Section 6.3 of Ian McInerney's graduate thesis "Development of a multi-agent quadrotor research platform with distributed computational capabilities"

[^2]: The derivation of this is shown in part in [this][matlab] article about computing moment of inertia measurements with MATLAB, which also contains some details regarding implementation

[matlab]: https://www.mathworks.com/company/newsletters/articles/improving-mass-moment-of-inertia-measurements.html


# Measuring Equivalent Moment of Inertia[^1]

## Variables

- $`K_V`$ - Motor back-emf constant $`\left(\frac{\text{rad}}{Vs}\right)`$
- $`R_m`$ - Motor resistance $`(\Omega)`$
- $`K_Q`$ - Motor torque constant $`\left(\frac{Nm}{A}\right)`$
- $`\tilde J_r`$ - Equivalent moment of inertia $`(kg\;m^2)`$
- $`\tau`$ - Time constant of transient rotor response $`(s)`$

## Equivalent Moment of Inertia

In order to determine the response to a motor speed command, we need the combined moment of inertia for the rotor and the outer motor housing (the parts that spin).
Note that this value is for a single motor its axis of rotation, not for all four combined.
A larger moment of inertia corresponds to the rotor taking more time to reach its target angular velocity.
The transient response of a rotor to a step change in input command should be approximately a negative exponential decay, specifically one where
```math
\tau = R_mK_VK_Q\tilde J_r
```

## Measurement Options

The big-picture goal here is to measure the time constant of the rotor speed transition after a step change in input command
(which when plotted with angular velocity as function of time should look like a negative exponential decay).
The time constant is the time it takes the angular velocity to reach $`63.2\%`$ of its final value from its initial value.
This can then be plugged into the above equation and rearranged such that $`\tilde J_r = \frac{\tau}{R_mK_VK_Q}`$.  

There are two[^1][^2] approaches that have been used in the lab in the past. The first is to record the sound of rotors spinning,
view the spectrogram of the sound with MATLAB (the sound should transition to a higher pitch at higher rotor speeds), and measure the time constant.
This is done by the approximation that the time that the rotor takes to reach $`95\%`$ of its final velocity (pitch) is equal to $`3\tau`$.  

The other option is to continuously measure angular velocity with a photointerrupter or optical tachometer, from which data can be used to determine the
actual time when the velocity crosses the $`63.2\%`$ threshold. Note that past time constants have been on the order of $`10^{-2}`$s, so if this approach is used,
the setup has to be able to take sufficiently fast samples.

## Procedure

1. Disable gyroscope feedback and secure or arrange the quadcopter so that it will not move when thrust is applied
1. Set up the chosen measurement device to be recording data
1. Apply an initial (non-zero) throttle command to the quadcopter
1. After a few seconds, step the throttle input to a larger value (there should be a fairly significant difference in the two command values)
1. After a few more seconds, return the throttle to 0 and stop recording data
1. Using the *Measurement Options* Section above, compute the time constant and moment of inertia

[^1]: Adapted from subsection 5.5.5 of Matthew Rich's graduate  thesis "Model development, system identification, and control of a quadrotor helicopter"
[^2]: The photointerrupter approach can be found in subsection 6.4.4 of Ian McInerney's graduate thesis "Development of a multi-agent quadrotor research platform with distributed computational capabilities"
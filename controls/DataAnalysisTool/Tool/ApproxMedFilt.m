function [ y ] = ApproxMedFilt( x, eta)
%APPROXMEDFILT Summary of this function goes here
%   Detailed explanation goes here
mean = x(1);
med = x(1);
y = zeros(size(x));
for i = 1:length(x)
    mean = mean + (eta * (x(i) - mean));
    med = med + (eta * sign(x(i) - med));
    y(i) = med;
end


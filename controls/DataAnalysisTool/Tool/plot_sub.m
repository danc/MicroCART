function plot_sub(expData, useMarker, varargin)
%PLOT_SUB This function make sub 2x1 subplots of data headers mentioned
%   expData     - structure that holds all of the data
%   useMarker   - flag to indicated whether or not to plot vertical lines
%                 at the marker locations
%   varargin    - the data headers to be plotted along with optional
%                 formatting parameters
%
%   Example of varargin: 'Pitch','r-','Roll','Yaw','go' 
%   This means that Pitch will be plotted in red solids, Roll with no
%   special formatting and Yaw in green circles.

% extracting the Time structure
time = expData.Time;
currHeaderIndex = 0;

% extract markerlocations, if any
markerLocations = [];
if(useMarker)
    if(~isfield(expData,'Marker'))
        error('Error! Attempting to plot markers without Marker field');
    else
        markerLocations = find(expData.Marker.data);
    end
end

% calculating number of headers
numOfHeaders = 0;
for i = 1:length(varargin)
    if(~isPlotCharString(varargin{i}))
        numOfHeaders = numOfHeaders + 1;
    end
end

% building the plot statement to be executed
for i = 1:length(varargin)
    
    % continue to next argument if the current argument is a plot
    % formatting string
    if(isPlotCharString(varargin{i}))
        continue;
    end
    
    varargin{i} = strrep(varargin{i},' ', '_');
    
    % keeping track of the number of headers
    currHeaderIndex = currHeaderIndex + 1;
    
    % adding header name to the plot statment
    plotString = strcat('plot(time.data,expData.',varargin{i},'.data');
    
    % adding the plot formatting string if it exists
    if(i ~= length(varargin))
        if(isPlotCharString(varargin{i+1}))
            plotString = strcat(plotString,',''',varargin{i+1},''');');
        else
            % use the plotting style parameters set for this header
            plotCharString = buildPlotCharString(eval(['expData.' varargin{i} '.params']));
            plotString = strcat(plotString,',''', plotCharString, ''');');
        end
    else
        % use the plotting parameters set for this header
        plotCharString = buildPlotCharString(eval(['expData.' varargin{i} '.params']));
        plotString = strcat(plotString,',''', plotCharString, ''');');
    end
    
    
    % checking to see if a new figure needs to be created
    if(mod(currHeaderIndex,2) == 1)
        figure;
        if(currHeaderIndex ~= numOfHeaders)
            subplot(2,1,1);
        end
    else
        subplot(2,1,2);
    end
    
    %% plotting data and making it look pretty
    eval(plotString);
    title(varargin{i},'Interpreter','none');
    xlabel(['Time (' time.unit ')']);
    if(eval(['isempty(expData.' varargin{i} '.unit)']))
        yAxisLabel = varargin{i};
    else
        yAxisLabel = [varargin{i} ' (' eval(['expData.' varargin{i} '.unit']) ')'];
    end
    ylabel(yAxisLabel,'Interpreter','none');
    xlim([0,time.data(end)]);
    grid ON;
    set(gca,'Color',eval(['expData.' varargin{i} '.params.backgnd']));
    % plotting markers, if any
    for i = 1:numel(markerLocations)
        %hx = graph2d.constantline(time.data(markerLocations(i)),'Linestyle', '--','Color', [0.7 0.7 0.7]);
        %changedependvar(hx,'x');
        text(time.data(markerLocations(i)),(min(ylim)+max(ylim))/2, num2str(i));
    end
    
end

end


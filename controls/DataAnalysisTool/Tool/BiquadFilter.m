function out = BiquadFilter(input, Fs, Fc)
    K = tan(pi * Fc / Fs);
    Q = 0.5;
    
    norm = 1.0 / (1.0 + K/Q + K*K);
    a0 = K*K*norm;
    a1 = 2.0*a0;
    a2 = a0;
    b1 = 2.0*(K*K -1) * norm;
    b2 = (1.0 - K/Q + K*K) * norm;
    
    prev = [0, 0];
    out = zeros(length(input), 1);
    for n = 1:length(input)
        leftSum = input(n) - prev(1)*b1 - prev(2)*b2;
        out(n) = leftSum*a0 + prev(1)*a1 + prev(2)*a2;
        
        prev(2) = prev(1);
        prev(1) = leftSum;
    end
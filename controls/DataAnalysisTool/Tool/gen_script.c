/*
 * This basic script will populate a .txt file (gui_testLogFile) with junk
 * (but viable) data. This is for the purpose of testing the real time
 * data logging feature on the groundstation side.
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int NUM_OPERATIONS = 50;

int main(){

  FILE * f = fopen("gui_testLogFile.txt", "a");
  FILE * to_write = fopen("junk_data.txt", "r");
  char buffer[1048];

  int i = 0;

  if (f == NULL){
    perror("Error opening file");
    }
  if (to_write == NULL){
    perror("Error opening junk file");
    }
  while(i < NUM_OPERATIONS){
    FILE * f = fopen("gui_testLogFile.txt", "a");
    fgets(buffer, 746, to_write);
    fprintf(f, buffer);
    fclose(f);
    i++;
    sleep(1);
  }
  fclose(to_write);
  return 0;
}

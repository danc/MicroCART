% Runs the simulation once, optionally plotting the setpoint errors
function [sp_err] = run_once(show_plot)
    output = sim('Quadcopter_Model', 'ReturnWorkspaceOutputs','on');
    % Use position errors but not velocity
    sp_err    = output.yout{1}.Values.Data(:, (7:12));
    setpoints = output.yout{2}.Values.Data;
    position  = [output.yout{3}.Values.Data output.yout{4}.Values.Data(:,3)];
    
    % Plot position and angle error
    if show_plot
        close all;
        % Separate position and angle errors
        position_err = sp_err(:, 1:3);
        angle_err = sp_err(:,4:6);
        time = output.tout;
        
        % Draw plots of errors 
        figure; hold on; 
        plot(time, position_err); plot(time, angle_err, '-.');
        title('Position and Euler Angle Error');
        xlabel('Time (s)'); ylabel('Error (m or rad)');
        legend({'x', 'y', 'z', '\phi', '\theta', '\psi'});
        hold off;
        
        % Plot position and setpoints
        figure; hold on; 
        plot(time, position); plot(time, setpoints, '-.');
        title('Setpoints and Actual Position');
        xlabel('Time (s)'); ylabel('Value (m or rad)');
        legend({'x', 'y', 'z', '\psi'});
        hold off;
    end
end
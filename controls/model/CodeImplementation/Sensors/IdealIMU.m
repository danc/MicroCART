function [accelReading, gyroReading] = idealIMU(B_vo_dot, B_vo, B_Omega, B_g, g, r_oc)
%#codegen

a = B_vo_dot + cross(B_Omega,B_vo); % body frame acceleration 

accelReading = (a - B_g)/g ; % accelerometer reading (ideal)

gyroReading = B_Omega ; % gyroscope reading (ideal) 

end


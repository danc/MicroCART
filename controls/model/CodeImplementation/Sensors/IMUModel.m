% 
%
% Author: Austin Beinder
%

classdef IMUModel
    properties
        accelerometer_sample_time
        accelerometer_transfer_function
        accelerometer_noise
        gyroscope_sample_time
        gyroscope_noise
        gyroscope_transfer_function
        accel_sampler_out
        gyro_sampler_out
        time
        accelerometer_tf_state
        gyroscope_tf_state
        accel_tf_b
        accel_tf_a
        gyro_tf_b
        gyro_tf_a

    end
    methods 
        function obj = IMUModel(accelerometer_sample_time, ...
                accelerometer_transfer_function, ...
                accelerometer_noise, ...
                gyroscope_sample_time, ...
                gyroscope_noise, ...
                gyroscope_transfer_function)
            
            obj.accelerometer_sample_time = accelerometer_sample_time;
            obj.accelerometer_transfer_function = accelerometer_transfer_function;
            obj.accelerometer_noise = accelerometer_noise;
            obj.gyroscope_sample_time = gyroscope_sample_time;
            obj.gyroscope_noise = gyroscope_noise;
            obj.gyroscope_transfer_function = gyroscope_transfer_function;
            obj.time = 0;

            obj.accel_sampler_out = zeros(1, 3);
            obj.gyro_sampler_out = zeros(1, 3);

            obj.accelerometer_tf_state = 0;
            obj.gyroscope_tf_state = 0;

            obj.accel_tf_b = [0.749 0];
            obj.accel_tf_a = [-0.251 1];
            obj.gyro_tf_b = [0.7327 0];
            obj.gyro_tf_a = [-0.2673 1];

        end

        function [accelerometer, gyroscope, imu] = nextOutput(imu, B_vo_dot, B_vo, B_Omega, B_g, g, r_oc, delta_t)
            
            [accelReading, gyroReading] = IdealIMU(B_vo_dot, B_vo, B_Omega, B_g, g, r_oc);
            
            [imu.accel_sampler_out, imu] = imu.sampler(imu.accelerometer_sample_time, accelReading, imu.accel_sampler_out, delta_t);
            [imu.gyro_sampler_out, imu] = imu.sampler(imu.gyroscope_sample_time, gyroReading, imu.accel_sampler_out, delta_t);
            
            noisy_accel = imu.accel_sampler_out + mvnrnd([0,0,0],imu.accelerometer_noise);
            noisy_gyro = imu.gyro_sampler_out + mvnrnd([0,0,0],imu.gyroscope_noise);

            [filtered_accel, imu.accelerometer_tf_state] = filter(imu.accel_tf_b, imu.accel_tf_a, noisy_accel, imu.accelerometer_tf_state);
            [filtered_gyro, imu.gyroscope_tf_state] = filter(imu.gyro_tf_b, imu.gyro_tf_a, noisy_gyro, imu.gyroscope_tf_state);
            
            accelerometer = quantization(2.4400e-04, filtered_accel);
            gyroscope = quantization(1.1e-3, filtered_gyro);

        end

        function [output, imu] = sampler(imu, sample_rate, input_vector, current_output, delta_t)
        
            new_time = imu.time + delta_t;

            if (new_time > sample_rate)
                imu.time = new_time - sample_rate;
                output = input_vector;
            else
                imu.time = new_time;
                output = current_output;
            end

        end
    end
end

function output = quantization(quantization_rate, input_vector)
            
    output = zeros(1, length(input_vector));

    for i = 1:length(input_vector)
        
        output(i) = quantization_rate * ...
            round(input_vector(i) / quantization_rate);

    end

end
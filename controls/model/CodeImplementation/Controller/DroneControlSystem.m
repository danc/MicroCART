% Implements the top level control block for the drone control system.
% Implements a switch between a PID controller and the LQR controller.
%
% Author: Austin Beinder
%

classdef DroneControlSystem
    properties
        drone_pid_controller
        signal_mixer
        pmax
        pmin
        lqr_k

    end
    methods 
        function obj = DroneControlSystem(drone_pid_controller, signal_mixer, pmax, pmin, lqr_k)
           
            obj.drone_pid_controller = drone_pid_controller;
            obj.signal_mixer = signal_mixer;
            obj.pmax = pmax;
            obj.pmin = pmin;
            obj.lqr_k = lqr_k;

            return;
        end
        
        % Equivalent to nextOutput in RealTimePIDController
        function [P, controller] = nextOutputMainController(controller, setpoints, euler_angles_filtered, euler_rates, current_position, delta_t)
            
            x_setpoint = setpoints(1);
            y_setpoint = setpoints(2);
            z_setpoint = setpoints(3);
            yaw_setpoint = setpoints(4);

            x_position = current_position(1);
            y_position = current_position(2);
            z_position = current_position(3);

            roll = euler_angles_filtered(1);
            pitch = euler_angles_filtered(2); % not super sure im getting these right
            yaw = euler_angles_filtered(3);

            roll_rate = euler_rates(1);
            pitch_rate = euler_rates(2);
            yaw_rate = euler_rates(3);

            [heightControlled, y_controlled, x_controlled, yaw_controlled, controller.drone_pid_controller] = controller.drone_pid_controller.nextOutput(z_setpoint, z_position, y_setpoint, y_position, roll, roll_rate, ...
                x_setpoint, x_position, pitch, pitch_rate, yaw_setpoint, yaw, yaw_rate, delta_t);
            
            P = [heightControlled, y_controlled, x_controlled, yaw_controlled];
            
        end

        function [P, setpoint_error] = nextOutputErrorController(controller, setpoints, euler_angles_filtered, euler_rates, current_position, velocity)
            
            vector1 = [velocity, euler_rates, euler_angles_filtered, current_position];
            vector2 = [0, 0, 0, 0, 0, 0, setpoints(1), setpoints(2), setpoints(3), 0, 0, setpoints(4)];
            
            setpoint_error = vector1 - vector2;
            p1 = setpoint_error .* controller.lqr_k;
            p2 = controller.saturation(p1, [20000, 20000, 20000, 20000], [-20000, -20000, -20000, -20000]);
            
            P = -p2;


        end
        
        % Top level control system function, muxes between LQR controller
        % and PID controller.
        function [P, setpoint_error, controller] = nextOutput(controller, setpoints, euler_angles_filtered, euler_rates, current_position, velocity, throttle_command, y_controlled, x_controlled, yaw_controlled, lqr_switch, delta_t)
            
            [p11, controller] = controller.nextOutputMainController(setpoints, euler_angles_filtered, euler_rates, current_position, delta_t); 
            [p12, setpoint_error] = controller.nextOutputErrorController(setpoints, euler_angles_filtered, euler_rates, current_position, velocity);

            if (lqr_switch == 1)
                p2 = p11;
            else
                p2 = p12;
            end

            p3 = p2 * controller.signal_mixer;
            
            external_inputs = [throttle_command, y_controlled, x_controlled, yaw_controlled];

            p4 = p3 + external_inputs;

            P = controller.saturation(p4, controller.pmax, controller.pmin);

        end

        % Mimics the saturation block in simulink
        function output = saturation(~, input, upper, lower)
            
            l = length(input);
            output = zeros(1,l);
            for i = 1:length(input)
                
                if input(i) > upper
                    output(i) = upper;
                elseif input(i) < lower
                    output(i) = lower;
                else
                    output(i) = input(i);
                end    
            end
        end

    end

end
% This implements a D controller in the time domain. 
%
% Author: Austin Beinder
%

classdef RealTimeDerivativeController
    properties
        D {mustBeNumeric}
        input_last {mustBeNumeric}
    end
    methods 
        function obj = RealTimeDerivativeController(d)
            
            obj.D = d;
            obj.input_last = 0;

        end

        function [output, controller] = nextOutput(controller, input, delta_t)
            
            difference = input - controller.input_last;
            slope = difference / delta_t;

            output = controller.D * slope;
            controller.input_last = input;


        end


    end
end
% This implements a PID controller in the time domain. All equations are 
% taken from the wikipedia "Discrete implementation" section.
%
% https://en.wikipedia.org/wiki/PID_controller#Discrete_implementation
%
% Author: Austin Beinder
%

classdef RealTimePIDController
    properties
        P {mustBeNumeric}
        I {mustBeNumeric}
        D {mustBeNumeric}
        input_last {mustBeNumeric}
        input_second_2last {mustBeNumeric}
        output_last {mustBeNumeric}
    end
    methods 
        function obj = RealTimePIDController(p, i, d)
            
            obj.P = p;
            obj.I = i;
            obj.D = d;
            obj.input_last = 0;
            obj.input_second_2last = 0;
            obj.output_last = 0;

        end

        function [output, pid] = nextOutput(pid, input, delta_t)
            
            Ti = pid.P / pid.I;
            Td = pid.D / pid.P;

            term1 = (1 + (delta_t/Ti) + (Td/delta_t)) * input;
            term2 = (-1 - (2*Td/delta_t)) * pid.input_last;
            term3 = ((Td/delta_t)) * pid.input_second_2last;

            output = pid.output_last + pid.P * (term1 + term2 + term3);

            pid.input_last = input;
            pid.input_second_2last = pid.input_last;
            pid.output_last = output;


        end


    end
end
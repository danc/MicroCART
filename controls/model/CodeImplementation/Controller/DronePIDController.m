% This class represents a code implementation of the previous block diagram
% of the quadcopter PID controller. It uses several nested PID controllers
% to achieve control of x, y, z and yaw.
%
% Author: Austin Beinder
%

classdef DronePIDController
    properties
        z_pid_controller

        z_max
        z_min

        y_pid 
        y_pid1
        y_vel_pid
        roll_pid
        roll_rate_pid

        y_max
        y_min

        x_pid
        y_pid2
        x_vel_pid
        pitch_pid
        pitch_rate_pid

        x_max
        x_min


        yaw_pid
        yaw_rate_pid
        yaw_max
        yaw_min

    end
    methods 
        function obj = DronePIDController(z_pid, z_max, z_min, y_pid, y_pid1, y_vel_pid, roll_pid, roll_rate_pid, y_max, y_min, ...
                x_pid, y_pid2, x_vel_pid, pitch_pid, pitch_rate_pid, x_max, x_min, yaw_pid, yaw_rate_pid, yaw_max, yaw_min)
           
            obj.z_pid_controller = z_pid;
            obj.z_max = z_max;
            obj.z_min = z_min;

            obj.y_pid = y_pid;
            obj.y_pid1 = y_pid1;
            obj.y_vel_pid = y_vel_pid;
            obj.roll_pid = roll_pid;
            obj.roll_rate_pid = roll_rate_pid;
            obj.y_max = y_max;
            obj.y_min = y_min;


            obj.x_pid = x_pid;
            obj.y_pid2 = y_pid2;
            obj.x_vel_pid = x_vel_pid;
            obj.pitch_pid = pitch_pid;
            obj.pitch_rate_pid = pitch_rate_pid;
            obj.x_max = x_max;
            obj.x_min = x_min;

            obj.yaw_pid = yaw_pid;
            obj.yaw_rate_pid = yaw_rate_pid;
            obj.yaw_max = yaw_max;
            obj.yaw_min = yaw_min;

            return;
        end
        
        % Equivalent to nextOutput in RealTimePIDController
        function [heightControlled, y_controlled, x_controlled, yaw_controlled, controller] = nextOutput(controller, z_setpoint, z_position, y_setpoint, y_position, roll, roll_rate, ...
                x_setpoint, x_position, pitch, pitch_rate, yaw_setpoint, yaw, yaw_rate, delta_t)
            
            % ------------- Height Controller -----------
            z_error = z_setpoint - z_position;
            [z_output, controller.z_pid_controller] = controller.z_pid_controller.pi_minus_d(z_error, z_position, delta_t);
            heightControlled = controller.saturation(z_output, controller.z_max, controller.z_min);
            
            % -------------- Y Controller ---------
            y_error = y_setpoint - y_position;
            [y_output1, controller.y_pid] = controller.y_pid.pi_minus_d(y_error, y_position, delta_t);
            [y_output2, controller.y_pid1] = controller.y_pid1.pi_minus_d(y_error, y_position, delta_t);

            y_vel_error = y_output1 - y_output2;
            [roll_setpoint, controller.y_vel_pid] = controller.y_vel_pid.pi_minus_d(y_vel_error, y_output2, delta_t);
            
            roll_error = roll_setpoint - roll;
            [roll_rate_setpoint, controller.roll_pid] = controller.roll_pid.pi_minus_d(roll_error, roll_setpoint, delta_t);

            roll_rate_error = roll_rate_setpoint - roll_rate;
            [y_motor_command, controller.roll_rate_pid] = controller.roll_rate_pid.pi_minus_d(roll_rate_error, roll_rate, delta_t);
            y_controlled = controller.saturation(y_motor_command, controller.y_max, controller.y_min);
            
            % --------------- X Controller ---------
            x_error = x_setpoint - x_position;
            [x_output1, controller.x_pid] = controller.x_pid.pi_minus_d(x_error, x_position, delta_t);
            [x_output2, controller.y_pid2] = controller.y_pid2.pi_minus_d(x_error, x_position, delta_t);

            x_vel_error = x_output1 - x_output2;
            [pitch_setpoint, controller.x_vel_pid] = controller.x_vel_pid.pi_minus_d(x_vel_error, x_output2, delta_t);
            
            pitch_error = pitch_setpoint - pitch;
            [pitch_rate_setpoint, controller.pitch_pid] = controller.pitch_pid.pi_minus_d(pitch_error, pitch, delta_t);

            pitch_rate_error = pitch_rate_setpoint - pitch_rate;
            [x_motor_command, controller.pitch_rate_pid] = controller.pitch_rate_pid.pi_minus_d(pitch_rate_error, pitch_rate, delta_t);

            x_controlled = controller.saturation(x_motor_command, controller.x_max, controller.y_max);

            % --------------- Yaw Controller -------
            yaw_error = yaw_setpoint - yaw;
            [yaw_setpoint, controller.yaw_pid] = controller.yaw_pid.pi_minus_d(yaw_error, yaw, delta_t);

            yaw_rate_error = yaw_setpoint - yaw_rate;
            [yaw_motor_command, controller.yaw_rate_pid] = controller.yaw_rate_pid.pi_minus_d(yaw_rate_error, yaw_rate, delta_t);

            yaw_controlled = controller.saturation(yaw_motor_command, controller.yaw_max, controller.yaw_min);

        end
        
        % Mimics the saturation block in simulink
        function output = saturation(~, input, upper, lower)
            
            if input > upper
                output = upper;
            elseif input < lower
                output = lower;
            else
                output = input;
            end
        end

    end

end
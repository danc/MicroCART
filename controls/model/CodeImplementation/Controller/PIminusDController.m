classdef PIminusDController
    properties
        pi_controller
        d_controller
    end
    methods 
        function obj = PIminusDController(P, I, D, ts)
           

            pi_c = RealTimePIDController(P, I, 0);
            d_c = RealTimeDerivativeController(D);

            obj.pi_controller = pi_c;
            obj.d_controller = d_c;

            return;
        end
        
        % Equivalent to nextOutput in RealTimePIDController
        function [setpoint, controller] = pi_minus_d(controller, error, value, delta_t)
            [output1, controller.pi_controller] = controller.pi_controller.nextOutput(error, delta_t);

            [output2, controller.d_controller] = controller.d_controller.nextOutput(value, delta_t);

            setpoint = output1 - output2;
        end

    end

end
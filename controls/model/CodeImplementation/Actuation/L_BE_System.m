% This class represents the previous Simulink Block 
% linear_earth_body_conversion and gravity block subsystem 
%
% Author Gautham Ajith

classdef L_BE_System
    
    properties
        E_Fg
        B_vo
        euler_angles
    end
    
    methods
        function obj = L_BE_System(g, m, euler_angles, B_vo)
            if nargin < 3
                obj.E_Fg = [0; 0; m*g];
            else
                obj.B_vo = B_vo;        
            end
            obj.euler_angles = euler_angles;
        end
        
        function [B_Fg, B_g] = L_BE(obj)
            phi = obj.euler_angles(1);
            theta = obj.euler_angles(2);
            psi = obj.euler_angles(3);
            
            Lbe = [             cos(theta)*cos(psi)              ,          cos(theta)*sin(psi)                  ,     -sin(theta)    ; ...
                   sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi), sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi), sin(phi)*cos(theta); ...
                   cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi), cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi), cos(phi)*cos(theta)];
            
            B_Fg = Lbe * obj.E_Fg;
            
            B_g = B_Fg/m;
        end

        function E_ro  = L_BE_2(obj, B_vo)
            euler_rates = zeros(3,1);
            E_ro = zeros(3,1);
            
            phi = obj.euler_angles(1);
            theta = obj.euler_angles(2);
            psi = obj.euler_angles(3);
            
            Leb = [cos(theta)*cos(psi), sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi), cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi); ...
                   cos(theta)*sin(psi), sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi), cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi); ...
                       -sin(theta)    ,                sin(phi)*cos(theta)            ,                 cos(phi)*cos(theta)           ];
            
            E_ro = Leb * B_vo;
        end
    end
end


% This class represents the previous Simulink Block Rotor_System which is
% in the Actuation block
%
% Author Gautham Ajith
classdef Rotor_System
    
    properties
        w_dot
        w
        B_Fg
        B_omega
        B_vo
    end
    
    methods
        function obj = Rotor_System(w_dot, w, B_Fg, B_omega, B_vo)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            obj.w_dot = w_dot;
            obj.w = w;
            obj.B_Fg = B_Fg;
            obj.B_omega = B_omega;
            obj.B_vo = B_vo;
        end
        
        function [B_omega_dot, B_vo_dot] = rotor(obj, m, Kt, Kd, rhx, rhy, rhz, Jreq, Jxx, Jyy, Jzz, r_oc, Kh, delta_T)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            
            % Create J vector
            J = [Jxx,  0 ,  0 ; ...
                  0 , Jyy,  0 ; ...
                  0 ,  0 , Jzz;];
              
            r_oc_X = [0, -r_oc(3), r_oc(2); r_oc(3), 0, -r_oc(1); -r_oc(2), r_oc(1), 0];
            
            % Create r_hi vector
            rh_0 = [-rhx; rhy; -rhz];
            rh_1 = [rhx; rhy; -rhz];
            rh_2 = [-rhx; -rhy; -rhz];
            rh_3 = [rhx; -rhy; -rhz];
            
            % Calculate rotor hub velocity
            vh_0 = obj.B_vo + cross(obj.B_omega,rh_0);
            vh_1 = obj.B_vo  + cross(obj.B_omega,rh_1); 
            vh_2 = obj.B_vo  + cross(obj.B_omega,rh_2); 
            vh_3 = obj.B_vo  + cross(obj.B_omega,rh_3); 
            
            % Define 3x3 Identity Matrix
            I = eye(3);
            
            % Create gamma vectors
            gamma_Ti = [0; 0; -1];
            gamma_omega_03 = [0; 0; 1];  %Rotors 0 and 3 use this gamma_omega vector
            gamma_omega_12 = [0; 0; -1]; %Rotors 1 and 2 use this gamma_omega vector
            gamma_Hi = [ 1 0 0 ; 0 1 0 ; 0 0 0 ];
            
            % Define angular velocities for each rotor
            w_0 = obj.w(1);
            w_1 = obj.w(2);
            w_2 = obj.w(3);
            w_3 = obj.w(4);
            
            % Define angular acceleration for each rotor
            w_0_dot = obj.w_dot(1);
            w_1_dot = obj.w_dot(2);
            w_2_dot = obj.w_dot(3);
            w_3_dot = obj.w_dot(4);
            
            % Define the thrust force
            T_0 = (Kt * w_0 * w_0 + delta_T * vh_0 * w_0) * gamma_Ti; 
            T_1 = (Kt * w_1 * w_1 + delta_T * vh_1 * w_1) * gamma_Ti;
            T_2 = (Kt * w_2 * w_2 + delta_T * vh_2 * w_2) * gamma_Ti;
            T_3 = (Kt * w_3 * w_3 + delta_T * vh_3 * w_3) * gamma_Ti;
            
            % Define the in plane drag force
            H_0 = -Kh * w_0 * gamma_Hi * vh_0;
            H_1 = -Kh * w_1 * gamma_Hi * vh_1;
            H_2 = -Kh * w_2 * gamma_Hi * vh_2;
            H_3 = -Kh * w_3 * gamma_Hi * vh_3;
            
            % Define the rotor force in the z-direction from each rotor
            B_Fr_0 = T_0 + H_0;
            B_Fr_1 = T_1 + H_1;
            B_Fr_2 = T_2 + H_2;
            B_Fr_3 = T_3 + H_3;
            
            % Sum up the rotor forces in the z-direction from each vector to get the
            % total body force in the z-direction
            B_Fr = B_Fr_0 + B_Fr_1 + B_Fr_2 + B_Fr_3;
            
            % Define the in-plane drag and induced torque produced by each rotor
             B_Q_d0 = -1 * Kd * w_0 * w_0 * gamma_omega_03;
             B_Q_d1 = -1 * Kd * w_1 * w_1 * gamma_omega_12;
             B_Q_d2 = -1 * Kd * w_2 * w_2 * gamma_omega_12;
             B_Q_d3 = -1 * Kd * w_3 * w_3 * gamma_omega_03;
            
            % Sum up the total in-plane drag and induced torque to get the total
            % in-plane drag and induced torque on the body
            B_Q_d = B_Q_d0 + B_Q_d1 + B_Q_d2 + B_Q_d3;
            
            % Define the force lever arm torque created from the force produced by each
            % rotor in the z-direction
            B_Q_F0 = cross( rh_0, B_Fr_0 );
            B_Q_F1 = cross( rh_1, B_Fr_1 );
            B_Q_F2 = cross( rh_2, B_Fr_2 );
            B_Q_F3 = cross( rh_3, B_Fr_3 );
            
            B_Q_F = B_Q_F0 + B_Q_F1 + B_Q_F2 + B_Q_F3;
            
            % Define the change in angular momentum torque produced by each rotor 
            B_Q_L0 = -1 * Jreq * ( cross(obj.B_omega, w_0 * gamma_omega_03) + w_0_dot * gamma_omega_03 );
            B_Q_L1 = -1 * Jreq * ( cross(obj.B_omega, w_1 * gamma_omega_12) + w_1_dot * gamma_omega_12 ); 
            B_Q_L2 = -1 * Jreq * ( cross(obj.B_omega, w_2 * gamma_omega_12) + w_2_dot * gamma_omega_12 ); 
            B_Q_L3 = -1 * Jreq * ( cross(obj.B_omega, w_3 * gamma_omega_03) + w_3_dot * gamma_omega_03 );
            
            % Sum up the total change in angular momentum torque produced by each rotor
            B_Q_L = B_Q_L0 + B_Q_L1 + B_Q_L2 + B_Q_L3;
                
            % Define the total rotor system torque as the sum of the in-plane drag and
            % induced torque, force lever arm torque, and change in angular momentum
            % torques
            B_Q = B_Q_d + B_Q_F + B_Q_L;
            
            % Define the body forces in the z-direction from each vector to get the
            % total body force in the z-direction
            B_F = B_Fr + obj.B_Fg; 
            
            % Determine the dynamics of the system
            M = [m * I , -m * r_oc_X; 
                zeros(3,3), J];
            
            dynamics = M^-1*[B_F - m*cross(obj.B_omega,obj.B_vo) - m*cross(obj.B_omega,cross(obj.B_omega,r_oc)) ; ... 
                      B_Q - cross(obj.B_omega,J*obj.B_omega) - cross(r_oc,B_F) ];
            
            % Define the body frame angular velocities
            B_vo_dot = dynamics(1:3);
            
            % Define the body frame linear velocities
            B_omega_dot = dynamics(4:6);
        end
    end
end


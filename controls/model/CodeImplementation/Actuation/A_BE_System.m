% 

classdef A_BE_System
    
    properties
        B_omega
        euler_angles
    end
    
    methods
        function obj = A_BE_System(B_omega,euler_angles)
            obj.B_omega = B_omega;
            obj.euler_angles = euler_angles;
        end
        
        function euler_rates = A_BE(obj)
            
            phi = obj.euler_angles(1);
            theta = obj.euler_angles(2);
            
            Aeb = [1,  sin(phi)*tan(theta), cos(phi)*tan(theta); ...
                   0,      cos(phi)       ,        -sin(phi)   ; ...
                   0,  sin(phi)/cos(theta), cos(phi)/cos(theta)];
            
               
            euler_rates = Aeb * obj.B_omega;
        end
    end
end


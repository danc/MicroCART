function [B_omega, euler_angles, B_vo, E_ro, B_vo_dot, B_g]= Actuation_Mat(P)

    function Vb_eff   = ESC(P, Pmin, Pmax, Vb)
        P1 = P(1);
        P2 = P(2);
        P3 = P(3);
        P4 = P(4);
        
        % Define u_Pi for each of the rotors, limiting it to be greater than 0
        u_P0 = (P1 - Pmin) / (Pmax - Pmin);
        u_P1 = (P2 - Pmin) / (Pmax - Pmin);
        u_P2 = (P3 - Pmin) / (Pmax - Pmin);
        u_P3 = (P4 - Pmin) / (Pmax - Pmin);
        
        
        % Determine the effective battery voltage from each ESC
        Vb_eff_0 = u_P0 * Vb;
        Vb_eff_1 = u_P1 * Vb;
        Vb_eff_2 = u_P2 * Vb;
        Vb_eff_3 = u_P3 * Vb;
            
        Vb_eff = [Vb_eff_0, Vb_eff_1, Vb_eff_2, Vb_eff_3];
    
    function w_dot = motor(Vb_eff, w, Rm, Kv, Kq, Kd, If, Jreq)

        % Define each motors effective battery voltage
        Vb_eff_0 = Vb_eff(1);
        Vb_eff_1 = Vb_eff(2);
        Vb_eff_2 = Vb_eff(3);
        Vb_eff_3 = Vb_eff(4);
        
        % Determine the angular velocity of each rotor from feedback
        w_0 = w(1);
        w_1 = w(2);
        w_2 = w(3);
        w_3 = w(4);
        
        % Determine angular acceleration of each rotor
        w_0_dot = 1/(Jreq*Rm*Kq) * Vb_eff_0 - 1/(Jreq*Rm*Kq*Kv) * w_0 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_0^2;
        w_1_dot = 1/(Jreq*Rm*Kq) * Vb_eff_1 - 1/(Jreq*Rm*Kq*Kv) * w_1 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_1^2;
        w_2_dot = 1/(Jreq*Rm*Kq) * Vb_eff_2 - 1/(Jreq*Rm*Kq*Kv) * w_2 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_2^2;
        w_3_dot = 1/(Jreq*Rm*Kq) * Vb_eff_3 - 1/(Jreq*Rm*Kq*Kv) * w_3 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_3^2;
        
        w_dot = [w_0_dot, w_1_dot, w_2_dot, w_3_dot];  

    function [B_omega_dot, B_vo_dot]= rotor(w_dot, w, B_Fg, B_omega, B_vo, m, Kt, Kd, rhx, rhy, rhz, Jreq, Jxx, Jyy, Jzz, r_oc, Kh, delta_T)

        % Create J vector
        J = [Jxx,  0 ,  0 ; ...
              0 , Jyy,  0 ; ...
              0 ,  0 , Jzz;];
          
        r_oc_X = [0, -r_oc(3), r_oc(2); r_oc(3), 0, -r_oc(1); -r_oc(2), r_oc(1), 0];
        
        % Create r_hi vector
        rh_0 = [-rhx; rhy; -rhz];
        rh_1 = [rhx; rhy; -rhz];
        rh_2 = [-rhx; -rhy; -rhz];
        rh_3 = [rhx; -rhy; -rhz];
        
        % Calculate rotor hub velocity
        vh_0 = B_vo + cross(B_omega,rh_0);
        vh_1 = B_vo + cross(B_omega,rh_1); 
        vh_2 = B_vo + cross(B_omega,rh_2); 
        vh_3 = B_vo + cross(B_omega,rh_3); 
        
        % Define 3x3 Identity Matrix
        I = eye(3);
        
        % Create gamma vectors
        gamma_Ti = [0; 0; -1];
        gamma_omega_03 = [0; 0; 1];  %Rotors 0 and 3 use this gamma_omega vector
        gamma_omega_12 = [0; 0; -1]; %Rotors 1 and 2 use this gamma_omega vector
        gamma_Hi = [ 1 0 0 ; 0 1 0 ; 0 0 0 ];
        
        % Define angular velocities for each rotor
        w_0 = w(1);
        w_1 = w(2);
        w_2 = w(3);
        w_3 = w(4);
        
        % Define angular acceleration for each rotor
        w_0_dot = w_dot(1);
        w_1_dot = w_dot(2);
        w_2_dot = w_dot(3);
        w_3_dot = w_dot(4);
        
        % Define the thrust force
        T_0 = (Kt * w_0 * w_0 + delta_T * vh_0 * w_0) * gamma_Ti; 
        T_1 = (Kt * w_1 * w_1 + delta_T * vh_1 * w_1) * gamma_Ti;
        T_2 = (Kt * w_2 * w_2 + delta_T * vh_2 * w_2) * gamma_Ti;
        T_3 = (Kt * w_3 * w_3 + delta_T * vh_3 * w_3) * gamma_Ti;
        
        % Define the in plane drag force
        H_0 = -Kh * w_0 * gamma_Hi * vh_0;
        H_1 = -Kh * w_1 * gamma_Hi * vh_1;
        H_2 = -Kh * w_2 * gamma_Hi * vh_2;
        H_3 = -Kh * w_3 * gamma_Hi * vh_3;
        
        % Define the rotor force in the z-direction from each rotor
        B_Fr_0 = T_0 + H_0;
        B_Fr_1 = T_1 + H_1;
        B_Fr_2 = T_2 + H_2;
        B_Fr_3 = T_3 + H_3;
        
        % Sum up the rotor forces in the z-direction from each vector to get the
        % total body force in the z-direction
        B_Fr = B_Fr_0 + B_Fr_1 + B_Fr_2 + B_Fr_3;
        
        % Define the in-plane drag and induced torque produced by each rotor
         B_Q_d0 = -1 * Kd * w_0 * w_0 * gamma_omega_03;
         B_Q_d1 = -1 * Kd * w_1 * w_1 * gamma_omega_12;
         B_Q_d2 = -1 * Kd * w_2 * w_2 * gamma_omega_12;
         B_Q_d3 = -1 * Kd * w_3 * w_3 * gamma_omega_03;
        
        % Sum up the total in-plane drag and induced torque to get the total
        % in-plane drag and induced torque on the body
        B_Q_d = B_Q_d0 + B_Q_d1 + B_Q_d2 + B_Q_d3;
        
        % Define the force lever arm torque created from the force produced by each
        % rotor in the z-direction
        B_Q_F0 = cross( rh_0, B_Fr_0 );
        B_Q_F1 = cross( rh_1, B_Fr_1 );
        B_Q_F2 = cross( rh_2, B_Fr_2 );
        B_Q_F3 = cross( rh_3, B_Fr_3 );
        
        B_Q_F = B_Q_F0 + B_Q_F1 + B_Q_F2 + B_Q_F3;
        
        % Define the change in angular momentum torque produced by each rotor 
        B_Q_L0 = -1 * Jreq * ( cross(B_omega, w_0 * gamma_omega_03) + w_0_dot * gamma_omega_03 );
        B_Q_L1 = -1 * Jreq * ( cross(B_omega, w_1 * gamma_omega_12) + w_1_dot * gamma_omega_12 ); 
        B_Q_L2 = -1 * Jreq * ( cross(B_omega, w_2 * gamma_omega_12) + w_2_dot * gamma_omega_12 ); 
        B_Q_L3 = -1 * Jreq * ( cross(B_omega, w_3 * gamma_omega_03) + w_3_dot * gamma_omega_03 );
        
        % Sum up the total change in angular momentum torque produced by each rotor
        B_Q_L = B_Q_L0 + B_Q_L1 + B_Q_L2 + B_Q_L3;
            
        % Define the total rotor system torque as the sum of the in-plane drag and
        % induced torque, force lever arm torque, and change in angular momentum
        % torques
        B_Q = B_Q_d + B_Q_F + B_Q_L;
        
        % Define the body forces in the z-direction from each vector to get the
        % total body force in the z-direction
        B_F = B_Fr + B_Fg; 
        
        % Determine the dynamics of the system
        M = [m * I , -m * r_oc_X; 
            zeros(3,3), J];
        
        dynamics = M^-1*[B_F - m*cross(B_omega,B_vo) - m*cross(B_omega,cross(B_omega,r_oc)) ; ... 
                  B_Q - cross(B_omega,J*B_omega) - cross(r_oc,B_F) ];
        
        % Define the body frame angular velocities
        B_vo_dot = dynamics(1:3);
        
        % Define the body frame linear velocities
        B_omega_dot = dynamics(4:6);

    function E_Fg = gravity(m, g)

        E_Fg = [0; 0; m*g];
    
    function [B_Fg, B_g]  = linear_earth_body_conversion(E_Fg, euler_angles, m)

        phi = euler_angles(1);
        theta = euler_angles(2);
        psi = euler_angles(3);
        
        Lbe = [             cos(theta)*cos(psi)              ,          cos(theta)*sin(psi)                  ,     -sin(theta)    ; ...
               sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi), sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi), sin(phi)*cos(theta); ...
               cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi), cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi), cos(phi)*cos(theta)];
        
        B_Fg = Lbe * E_Fg;
        
        B_g = B_Fg/m;

    function euler_rates = angular_body_earth_conversion(B_omega, euler_angles)

        phi = euler_angles(1);
        theta = euler_angles(2);
        
        Aeb = [1,  sin(phi)*tan(theta), cos(phi)*tan(theta); ...
               0,      cos(phi)       ,        -sin(phi)   ; ...
               0,  sin(phi)/cos(theta), cos(phi)/cos(theta)];
        
           
        euler_rates = Aeb * B_omega;
  
    function E_ro  = linear_body_earth_conversion(B_vo, euler_angles)

        euler_rates = zeros(3,1);
        E_ro = zeros(3,1);
        
        phi = euler_angles(1);
        theta = euler_angles(2);
        psi = euler_angles(3);
        
        Leb = [cos(theta)*cos(psi), sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi), cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi); ...
               cos(theta)*sin(psi), sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi), cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi); ...
                   -sin(theta)    ,                sin(phi)*cos(theta)            ,                 cos(phi)*cos(theta)           ];
        
        E_ro = Leb * B_vo;
    

    
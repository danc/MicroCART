% This class represents the code implementation of the previous simulink
% block of the Motor_System in the Actuator Block.
% 
% Author Gautham Ajith

classdef Motor_System
    
    properties
        Vb_eff
        w
    end
    
    methods
        function obj = Motor_System(Vb_eff)
            obj.Vb_eff = Vb_eff;
        end
        
        function w_dot = motor(obj, Rm, Kv, Kq, Kd, If, Jreq, w)
    
            % Define each motors effective battery voltage
            Vb_eff_0 = obj.Vb_eff(1);
            Vb_eff_1 = obj.Vb_eff(2);
            Vb_eff_2 = obj.Vb_eff(3);
            Vb_eff_3 = obj.Vb_eff(4);
            
            % Determine the angular velocity of each rotor from feedback
            w_0 = w(1);
            w_1 = w(2);
            w_2 = w(3);
            w_3 = w(4);
            
            % Determine angular acceleration of each rotor
            w_0_dot = 1/(Jreq*Rm*Kq) * Vb_eff_0 - 1/(Jreq*Rm*Kq*Kv) * w_0 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_0^2;
            w_1_dot = 1/(Jreq*Rm*Kq) * Vb_eff_1 - 1/(Jreq*Rm*Kq*Kv) * w_1 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_1^2;
            w_2_dot = 1/(Jreq*Rm*Kq) * Vb_eff_2 - 1/(Jreq*Rm*Kq*Kv) * w_2 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_2^2;
            w_3_dot = 1/(Jreq*Rm*Kq) * Vb_eff_3 - 1/(Jreq*Rm*Kq*Kv) * w_3 - 1/(Jreq*Kq)*If - (Kd/Jreq) * w_3^2;
            
            w_dot = [w_0_dot, w_1_dot, w_2_dot, w_3_dot];
        end
    end
end


% This class is the code representation of the previous ESC_System Simulink
% Block which was in the Actuator Block
%
% Author Gautham Ajith
classdef ESC_System
   
    properties
        P
    end
    
    methods
        function obj = ESC_System(P)
            obj.P = P;
        end
        
        function Vb_eff   = ESC(obj, Pmin, Pmax, Vb)
            P1 = obj.P(1);
            P2 = obj.P(2);
            P3 = obj.P(3);
            P4 = obj.P(4);
            
            % Define u_Pi for each of the rotors, limiting it to be greater than 0
            u_P0 = (P1 - Pmin) / (Pmax - Pmin);
            u_P1 = (P2 - Pmin) / (Pmax - Pmin);
            u_P2 = (P3 - Pmin) / (Pmax - Pmin);
            u_P3 = (P4 - Pmin) / (Pmax - Pmin);
            
            
            % Determine the effective battery voltage from each ESC
            Vb_eff_0 = u_P0 * Vb;
            Vb_eff_1 = u_P1 * Vb;
            Vb_eff_2 = u_P2 * Vb;
            Vb_eff_3 = u_P3 * Vb;
                
            Vb_eff = [Vb_eff_0, Vb_eff_1, Vb_eff_2, Vb_eff_3];            
        end
    end
end


% Implements the top level actuation block for the drone actuation system.
%
% Author: Gautham Ajith
%
classdef Actuation   
    properties
        A_BE_System
        ESC_System
        L_BE_System
        L_BE_2
        Motor_System
        Rotor_System
        Pmax
        Pmin
        Vb
        w
        dt
        eu_angle
        B_omega
        B_Vo
    end
    
    methods
        function obj = Actuation(A_BE_System, ESC_System, L_BE_System, L_BE_2, Motor_System, Rotor_System, Pmax, Pmin, Vb)
            obj.A_BE_System = A_BE_System;
            obj.ESC_System = ESC_System();
            obj.L_BE_System = L_BE_System;
            obj.L_BE_2 = L_BE_2;
            obj.Motor_System = Motor_System;
            obj.Rotor_System = Rotor_System;
            obj.Pmax = Pmax;
            obj.Pmin = Pmin;
            obj.Vb = Vb;
            obj.w = 0;
            obj.dt = 0;
            obj.B_omega = 0;
            obj.B_Vo = 0;
        end
        
        function [B_omega, euler_angles, B_Vo, E_ro, B_vo_dot, B_g] = NextOutput(obj, m, Rm, Kv, Kq, Kd, If, Jreq, delta_t)
            obj.dt = delta_t;
            obj.Motor_System.Vb_Eff = obj.ESC_System.ESC(obj.ESC_System, obj.Pmax, obj.Pmin, obj.Vb);
            w_dot = obj.Motor_System.Motor(obj.Motor_System, Rm, Kv, Kq, Kd, If, Jreq, obj.w);
            obj.w = obj.integrator(w_dot, obj.dt, omega0_o, omega1_o, omega2_o, omega3_o);
            B_Fg, B_g = obj.L_BE_System.L_BE(obj.L_BE_System);
            B_omega_dot, B_vo_dot = obj.Rotor_System.rotor(obj, m, Kt, Kd, rhx, rhy, rhz, Jreq, Jxx, Jyy, Jzz, r_oc, Kh, delta_T);
            obj.B_Vo = integrator(B_vo_dot, delta_t, x_vel_o, y_vel_o, z_vel_o);
            obj.B_omega = integrator(B_omega_dot, delta_t, rollrate_o, pitchrate_o, yawrate_o);
            B_omega = obj.B_omega;
            B_Vo = obj.B_Vo;
            euler_rates = obj.A_BE_System.A_BE(obj.A_BE_System);
            euler_angles = integrator(euler_rates, delta_t, roll_o, pitch_o, yaw_o);
            E_ro_dot = obj.L_BE_2_System.L_BE_2(obj.L_BE_2, obj.B_Vo);
            E_ro = integrator(E_ro_dot, delta_t, x_o, y_o, z_o);




            
            



        end

        function output = integrator(input, delta_t, init_cond_1, init_cond_2, init_cond_3, init_cond_4)
            if nargin < 5
                x = input(1);
                y = input(2);
                z = input(3);
                x_prime = int(x, 0, delta_t) +init_cond_1;
                y_prime = int(y, 0, delta_t) + init_cond_2;
                z_prime = int(z, 0, delta_t) + init_cond_3;
                output = [x_prime, y_prime, z_prime];
            else
                o1 = input(1);
                o2 = input(2);
                o3 = input(3);
                o4 = input(4);
                o1_prime = int(o1, 0, delta_t) + init_cond_1;
                o2_prime = int(o2, 0, delta_t) + init_cond_2;
                o3_prime = int(o3, 0, delta_t) + init_cond_3;
                o4_prime = int(o4, 0, delta_t) + init_cond_4;
                output = [o1_prime, o2_prime, o3_prime, o4_prime];
            end
        end
    end
end


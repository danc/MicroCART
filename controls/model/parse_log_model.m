function [loggedData, headers] = parse_log_model(filename)
%parse_log This independent function parses the data stored in the file and
%returns a structure containing the data
%   filename    - this is the complete path of the file with the filename
%   params      - this is the params structure which holds the analysis
%                 configuration options

% Check if file exists
if (~exist(filename,'file'))
    error(strcat(filename, ' does not exist'));
end

% Open file
FileID = fopen(filename, 'r');

% Gets the first line of the file
string = fgetl(FileID);

% Test first line, if not formatted correctly, reject
if(size(regexp(string, '^#')) == 0)
    error(strcat(filename, ' is not properly formatted, and does not contain "#" headers'));
end

% Loop through header lines
while( regexp(string, '^#') == 1 )
    
    %Print out string and move on
    disp(string)
    old = string;
    string = fgetl(FileID);
    
end

% Two possibilities for the next two lines:
%   1) line of headers
%   2) line of units
foundHeaders = 0;
foundUnits = 0;

% Checking current line's type:
identifier = string(1);

if (regexp(identifier,'%'))
    foundHeaders = 1;
    % this is a line of headers; extract headers:
    string = strrep(string,' ', '_');
    headers = strsplit(string);
    headers{1} = strrep(headers{1},'%', '');
    numOfHeaders = length(headers);
else
    if (regexp(identifier,'&'))
        foundUnits = 1;
        % this is a line of units; extract units:
        units = strsplit(string);
        units{1} = strrep(units{1},'&','');
    else
         error(strcat(filename, ' is not properly formatted, contains undefined line identifier.'));
    end
end

% Obtaining the next line
string = fgetl(FileID); 
identifier = string(1);

if(foundHeaders)
    if(regexp(identifier,'&'))
        foundUnits = 1;
        % this is a line of units; extract units:
        units = strsplit(string);
        units{1} = strrep(units{1},'&','');
    else
        error(strcat(filename, ' is not properly formatted, contains or undefined/excessive line identifiers.'));
    end
else
    if(foundUnits)
        if(regexp(identifier,'%'))
            % this is a line of headers; extract headers:
            headers = strsplit(string);
            headers{1} = strrep(headers{1},'%', '');
            numOfHeaders = length(headers);
        end
    else
        error('Should never be able to get here');
    end
end

% sanity check and clean up
if(numOfHeaders ~= length(units))
    error(strcat(filename, ' is not properly formatted, contains unmatched number of units and headers'));
end
clear foundHeaders foundUnits;

% Get all data into a single matrix called "log"
log = [];
line = zeros(1,numOfHeaders);
while ~feof(FileID)
    line = textscan(FileID, '%f', numOfHeaders);
    line = transpose(cell2mat(line));
    log = [log;line];
end

% Converting the log matrix into a expData structure.
for i = 1:numOfHeaders
    
    eval(['loggedData.' headers{i} '.data = log(:,i);']);           % adding data
    eval(['loggedData.' headers{i} '.unit = cell2mat(units(i));']); % adding unit
        
end

end


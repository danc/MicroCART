% Given a set of weights and the state-space matrices, lqr_err() constructs
% the LQR controller, runs the Simulink simulation, and computes the sum of
% the norms of the errors of each setpoint position dimension (x, y, z,
% roll, pitch, and yaw)

% Note weights is [uv, w, pq, r, xy, z, phth, ps, uT, uAuE, uR]
% All analogous state variables for x and y are shared to reduce dimension
function err = lqr_err(A, B, weights, T)
    w = weights;
    % This re-expands the weights to length 12 and 4 vectors, respectively
    state_weights = [w(1) w(1:3) w(3:5) w(5:7) w(7:8)];
    command_weights = [w(9:10) w(10:11)];
    [K, ~, ~] = initial_lqr(A, B, state_weights, command_weights, T);
    % Set the LQR K matrix in the simulator to the newly calculated one
    assignin('base', 'lqr_K', K);
    % Run Simulink simulation and return norms of the errors (no plot)
    sp_err = run_once(false);
    err = sum(vecnorm(sp_err));
end
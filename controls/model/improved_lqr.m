% Given the state-space linearization, diagonal weighting matrices, and
% update period, compute the discrete LQR model for the system, described
% in Matt Rich's thesis section 7.4. Unlike the "initial LQR" design, this
% version defines its cost function with block-diagonal weighting matrices
% (instead of purely diagonal matrices)

% c_alt, c_lat, and c_dir are 3x2, 1x4, and 3x2, respectively
function [lqr_K, lqr_S, lqr_E] = improved_lqr(A, B, c_alt, c_lat, c_dir, d_alt, d_lat, d_dir, T)
    % Given the block-diagonal weighting matrix, index ordering permutation
    % to get the controller in our previous state ordering
    perm = [7 3 1 5 9 11 8 4 2 6 10 12];
    % Longitudinal is the same as latidude, but the angular weights need to
    % be flipped
    d_long = -d_lat;
    c_long = [c_lat(1:2), -c_lat(3:4)];
    % Q, R, and N matrices from weighting, given in subsytem components
    Q_sub = blkdiag(c_alt'*c_alt, c_lat'*c_lat, c_long'*c_long, c_dir'*c_dir);
    R_sub = blkdiag(d_alt'*d_alt, d_lat'*d_lat, d_long'*d_long, d_dir'*d_dir);
    N_sub = blkdiag(c_alt'*d_alt, c_lat'*d_lat, c_long'*d_long, c_dir'*d_dir);
    % Permute subsystem-ordered matrices to get compatible controller
    [lqr_K, lqr_S, lqr_E] = lqrd(A, B, Q_sub(perm,perm), R_sub, N_sub(perm,:), T);
end
% A test script intended to demo the functionality of DroneControlSystem.m
% I don't have a good drone plant model yet so all this does is check to
% see if everything compiles correctly.
%
%
% Author: Austin Beinder
%

% ------------------------------------------------------------------------
z_controller = PIminusDController(-0.098040, -0.00817, -0.07353, 0.1); % currently not using alpha
z_max = 20000;
z_min = -20000;

y_pid = PIminusDController(0.55, 0.0075, 0, 0.1);
y_pid1 = PIminusDController(0, 0, -1, 0.1);
y_vel_pid = PIminusDController(0.1, 0, 0.02, 0.1);
roll_pid = PIminusDController(35, 0, 1, 0.1);
roll_rate_pid = PIminusDController(0.03, 0, 0.005, 0.1);

y_max = 20000;
y_min = -20000;

x_pid = PIminusDController(0.55, 0.0075, 0, 0.1);
y_pid2 = PIminusDController(0, 0, -1, 0.1);
x_vel_pid = PIminusDController(-0.1, 0, -0.02, 0.1);
pitch_pid = PIminusDController(35, 0, 1, 0.1);
pitch_rate_pid = PIminusDController(0.03, 0, 0.005, 0.1);

x_max = 20000;
x_min = -20000;

yaw_pid = PIminusDController(2.6, 0, 0, 0.1);
yaw_rate_pid = PIminusDController(0.297, 0, 0, 0.1);

yaw_max = 20000;
yaw_min = -20000;

drone_controller = DronePIDController(z_controller, z_max, z_min, y_pid, y_pid1, y_vel_pid, roll_pid, roll_rate_pid, y_max, y_min, ...
                x_pid, y_pid2, x_vel_pid, pitch_pid, pitch_rate_pid, x_max, x_min, yaw_pid, yaw_rate_pid, yaw_max, yaw_min);

% -------------------------------------------------------------------------
% Model Parameters
    m = 1.216;                          % Quadrotor + battery mass
    g = 9.81;                           % Acceleration of gravity
    Jxx = 0.0110;%0.0130;               % Quadrotor and battery motor of inertia around bx (pitch)
    Jyy = 0.0116;%0.0140;               % Quadrotor and battery motor of inertia around by (roll)
    Jzz = 0.0223;%0.0285;               % Quadrotor and battery motor of inertia around bz (yaw)
    J = diag([ Jxx Jyy Jzz ]);
    Jreq = 4.2012e-05;                  % Rotor and motor moment of inertia around axis of rotation 
    Kt = 1.2007e-05;                    % Rotor thrust constant
    delta_T = [ 0,  0,  9.404e-04 ];    % Thrust constant adjustment factor
    Kh = 3.4574e-04;                    % Rotor in-plane drag constant
    Kd = 1.4852e-07;                    % Rotor drag constant
    rhx = 0.16;                         % X-axis distance from center of mass to a rotor hub
    rhy = 0.16;                         % Y-axis distance from center of mass to a rotor hub
    rhz = 0.03;                         % Z-axis distance from center of mass to a rotor hub
    r_oc = [0; 0; 0];                   % Vector from origin to center of mass
    Rm = 0.235;                         % Motor resistance
    Kq = 96.3422;                       % Motor torque constant
    Kv = 96.3422;                       % Motor back emf constant
    If = 0.3836;                        % Motor internal friction current
    Pmin = 0;                           % Minimum zybo output duty cycle command
    Pmax = 1;                           % Maximum zybo output duty cycle command
    Tc = 0.01;%0.04;                          % Camera system sampling period
    Tq = 0.005;                         % Quad sampling period
    tau_c = 0;                          % Camera system total latency
    Vb = 12.3;                          % Nominal battery voltage (V)


% Signal mixer using rotor layout
% 1 2
% 3 4
signal_mixer = [ 1, -1, -1, -1 
                 1, -1,  1,  1
                 1,  1, -1,  1
                 1,  1,  1, -1 ];

% Linearize model, load preferred weight set, and build LQR model
[ssA, ssB] = linearization(m, g, Kt, Kh, Kd, delta_T(3), rhx, rhy, rhz, J, Jreq, Vb, Rm, Kv, Kq, If, signal_mixer);
load('weights_modern.mat');
% Set control period to equal period of camera system
T_LQR = Tc;
[lqr_K, lqr_S, lqr_E] = initial_lqr(ssA, ssB, state_weights, command_weights, T_LQR);

% -------------------------------------------------------------------------

controller = DroneControlSystem(drone_controller, signal_mixer, 1, 0, lqr_K);

% -------------------------------------------------------------------------
err = 0;
desired = 10;
value = 0;
value2 = 0;
value3 = 0;

y_setpoint = 0; 
y_position = 0;  
roll = 0; 
roll_rate = 0; 
x_setpoint  = 0; 
x_position  = 0; 
pitch = 0; 
pitch_rate = 0; 
yaw_setpoint  = 0; 
yaw = 0; 
yaw_rate = 0; 

arr = zeros(100);
arr2 = zeros(100);
arr3 = zeros(100);
delta_t = 0.2;

euler_angles_filtered = zeros(1,3);
euler_rates = zeros(1,3);
current_position = [0, 0, 0];
velocity = zeros(1,3);
throttle_command = 0; 
y_controlled = 0;
x_controlled = 0;
yaw_controlled = 0; 
lqr_switch = 1;
setpoints = [0, 0, 0, 0];
delta_t = 1;


for a = 1:100
    % todo honestly you need something to pretend to be a drone here
    [P, setpoint_error, controller] = controller.nextOutput(setpoints, euler_angles_filtered, euler_rates, current_position, velocity, throttle_command, y_controlled, x_controlled, yaw_controlled, lqr_switch, delta_t);
end



% A test script intended to demo the functionality of RealTimePIDController.m
%
% Author: Austin Beinder
%

clf("reset");
accelerometer_sample_time = 0.1
accelerometer_transfer_function = 0;
accelerometer_noise = [0.01, 0.01, 0.01];
gyroscope_sample_time = 0.1;
gyroscope_noise = [0.01, 0.01, 0.01];
gyroscope_transfer_function = 0;
imu_model = IMUModel(accelerometer_sample_time, ...
                accelerometer_transfer_function, ...
                accelerometer_noise, ...
                gyroscope_sample_time, ...
                gyroscope_noise, ...
                gyroscope_transfer_function);

B_vo_dot = 0.1;
B_vo = [0.1, 0.1, 0.1];
B_Omega = [0.1, 0.1, 0.1];
B_g = 0.1;
g = 0.1;
r_oc = 0.1;

arr = zeros(3, 1000);
arr2 = zeros(3, 1000);
arr3 = zeros(3, 1000);
delta_t = 0.1;

for a = 1:1000
    [value, value2, pid1] = imu_model.nextOutput(B_vo_dot, B_vo, B_Omega, B_g, g, r_oc, delta_t);

    arr(:,a) = value;
    arr3(:,a) = value2;

end

plot(arr(1,1,:))
hold on
plot(arr2(1,1,:))
plot(arr3(1,1,:))
hold off

legend('actuator','error', 'output')

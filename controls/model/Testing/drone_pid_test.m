% A test script intended to demo the functionality of DronePIDController.m
%
% Author: Austin Beinder
%

z_controller = PIminusDController(-0.098040, -0.00817, -0.07353, 0.1); % currently not using alpha
z_max = 20000;
z_min = -20000;

y_pid = PIminusDController(0.55, 0.0075, 0, 0.1);
y_pid1 = PIminusDController(0, 0, -1, 0.1);
y_vel_pid = PIminusDController(0.1, 0, 0.02, 0.1);
roll_pid = PIminusDController(35, 0, 1, 0.1);
roll_rate_pid = PIminusDController(0.03, 0, 0.005, 0.1);

y_max = 20000;
y_min = -20000;

x_pid = PIminusDController(0.55, 0.0075, 0, 0.1);
y_pid2 = PIminusDController(0, 0, -1, 0.1);
x_vel_pid = PIminusDController(-0.1, 0, -0.02, 0.1);
pitch_pid = PIminusDController(35, 0, 1, 0.1);
pitch_rate_pid = PIminusDController(0.03, 0, 0.005, 0.1);

x_max = 20000;
x_min = -20000;

yaw_pid = PIminusDController(2.6, 0, 0, 0.1);
yaw_rate_pid = PIminusDController(0.297, 0, 0, 0.1);

yaw_max = 20000;
yaw_min = -20000;

drone_controller = DronePIDController(z_controller, z_max, z_min, y_pid, y_pid1, y_vel_pid, roll_pid, roll_rate_pid, y_max, y_min, ...
                x_pid, y_pid2, x_vel_pid, pitch_pid, pitch_rate_pid, x_max, x_min, yaw_pid, yaw_rate_pid, yaw_max, yaw_min);
           
err = 0;
desired = 10;
value = 0;
value2 = 0;
value3 = 0;

y_setpoint = 0; 
y_position = 0;  
roll = 0; 
roll_rate = 0; 
x_setpoint  = 0; 
x_position  = 0; 
pitch = 0; 
pitch_rate = 0; 
yaw_setpoint  = 0; 
yaw = 0; 
yaw_rate = 0; 

arr = zeros(100);
arr2 = zeros(100);
arr3 = zeros(100);
delta_t = 0.2;

for a = 1:100
    err = desired - value;
    value3 = value2;
    [value2, y_controlled, x_controlled, yaw_controlled, drone_controller] = drone_controller.nextOutput(desired, value, ...
        y_setpoint, y_position, roll, roll_rate, ...
                x_setpoint, x_position, pitch, pitch_rate, yaw_setpoint, yaw, yaw_rate, delta_t);

    value = value + value2*delta_t;
    
    arr(a) = value;
    arr2(a) = err;
    arr3(a) = value2;
end

plot(arr)
hold on
plot(arr2)
%plot(arr3)
hold off

legend('output', 'error')

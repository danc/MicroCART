% This script offers a demo of how to use the drone_gamepad_input 
% function and plots the yaw, elevation, x and y position.
% Essentially, it just integrates the x and y joystick positions.
% Intended for the Logitech F310 controllers found in the MicroCart and
% 476 room.

e = 0;
yaw = 0;
xa = 0;
y = 0;
gamepad = vrjoystick(1);


T = 500;
passo = 1;
t=1;
x1=0;
x2=0;
x3=0;
x4=0;
while 1

    position = drone_gamepad_input(yaw, e, y, xa, gamepad);

    yaw = position(1)
    e = position(2)
    y = position(3)
    xa = position(4)

    x1=[x1,yaw];
    plot(x1);
    hold on
    x2=[x2,e];
    plot(x2);
    x3=[x3,y];
    plot(x3);
    x4=[x4,xa];
    plot(x4);
    hold off
    
    %pay attention to this command %
    axis([T*fix(t/T),T+T*fix(t/T),-15,15]); 
    grid
    t=t+passo;
    drawnow;
    tic(); pause(0.1); toc()
end
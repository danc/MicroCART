% A test script intended to demo the functionality of PIminusDController.m
%
% Author: Austin Beinder
%


piminusd = PIminusDController(2, 0.5, 0.1, 0.1);
%pi_c = pid(1, 1, 0, 0, 0.1)

err = 10;
desired = 10;
value = 3;
value2 = 0;
value3 = 0;

arr = zeros(100);
arr2 = zeros(100);
arr3 = zeros(100);
delta_t = 0.2;

for a = 1:100
    err = desired - value;

    [value2, piminusd] = piminusd.pi_minus_d(err, value, delta_t);

    value = value + value2*delta_t;
    
    arr(a) = value2;
    arr2(a) = err;
    arr3(a) = value;
end

plot(arr)
hold on
plot(arr2)
plot(arr3)
hold off

legend('actuator','error', 'output')

% A test script intended to demo the functionality of RealTimeDerivativeController.m
%
% Author: Austin Beinder
%


clf("reset");

pid1 = RealTimeDerivativeController(0.1);

err = 10;
desired = 10;
value = 3;
value2 = 0;

arr = zeros(100);
arr2 = zeros(100);
arr3 = zeros(100);
delta_t = 0.01;

for a = 1:100
    err = desired - value2;
    [value, pid1] = pid1.nextOutput(err, delta_t);
    value2 = value2 + value*delta_t;

    arr(a) = value;
    arr2(a) = err;
    arr3(a) = value2;

end

plot(arr)
hold on
plot(arr2)
plot(arr3)
hold off

legend('actuator','error', 'output')

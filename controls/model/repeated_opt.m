% Change the size of the max value in the for loop to change the number of
% optimizations run. Each run (except the first) picks a random set of LQR
% weights and uses matlab's fmincon() to repeatedly run the simulation and
% minimize the value of lqr_err(). The weights resulting weights are stored
% as .mat files that can be loaded in modelParameters.m.

% Start with weights that look like Matt Rich's values
rw = matfile('weights_rich.mat');
sw = double(rw.state_weights); cw = rw.command_weights;
x0 = [sw([1,3,4,6,7,9,10,12]) cw([1,3,4])];
for i = 1:30
    % For other iterations, create random initial weights
    if (i > 1)
        x0 = 100*rand(1, 11);
    end
    % Run Matlab optimization on weights to minimize simulated error
    options = optimoptions('fmincon','Display','iter');
    [X,FVAL,EXITFLAG,OUTPUT] = fmincon(...
        @(weights) lqr_err(ssA, ssB, weights, T_LQR),...
        x0, -eye(11), zeros(11, 1), ...
        [], [], [], [], [], options);

    % Store result of optimization in file
    state_weights   = X([1, 1:3, 3:5, 5:7, 7:8]);
    command_weights = X([9:10 10:11]);
    error_norm = FVAL;
    fprintf("At timestamp %s, error was %f", datestr(now, 'mmddHHMM'), error_norm);
    save(['opt_weights_', datestr(now, 'mmddHHMM'), '.mat'], 'state_weights', 'command_weights', 'error_norm');
end

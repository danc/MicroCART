#ifndef C_CONTROLLER_H
#define C_CONTROLLER_H
#include "computation_graph.h"
#include "node_pid.h"
#include "node_bounds.h"
#include "node_constant.h"
#include "node_mixer.h"
#include "PID.h"
#include "control_algorithm.h"

double c_controller(int vrpn_id, double vrpn_ts, double set_x, double set_y, double set_z, double set_yaw,
        double cur_x, double cur_y, double cur_z,
        double cur_phi, double cur_theta, double cur_psi,
        double cur_phi_d, double cur_theta_d, double cur_psi_d,
        double *z_out, double *y_out, double *x_out, double *yaw_out,
        double* pid_y_out, double* pid_roll_out);

#endif // C_CONTROLLER_H
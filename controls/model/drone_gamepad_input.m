function setpoint = drone_gamepad_input(yaw_i, elevation_i, y_i, x_i, gamepad)

input = read(gamepad);

yaw_add = 0;
elevation_add = 0;
y_add = 0;
x_add = 0;

if abs(input(1)) > 0.1
    yaw_add = input(1);
end
if abs(input(2)) > 0.1
    elevation_add = input(2);
end
if abs(input(4)) > 0.1
    y_add = input(4);
end
if abs(input(5)) > 0.1
    x_add = input(5);
end


yaw = yaw_add + yaw_i;
elevation = elevation_add + elevation_i;
y = y_add + y_i;
x = x_add + x_i;

setpoint = [yaw, elevation, y, x];  

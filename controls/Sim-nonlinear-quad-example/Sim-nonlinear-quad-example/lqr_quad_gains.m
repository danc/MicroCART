function [U] = lqr_quad_gains(X)
% Name: lqr_gains
% Author: Phillip Jones.
% Description: Compute control command for CrazyFlie quad (-KX), 
%              based on LQR control law
%
%
%   X_dot = A*X + B*U;
%
%   Where, A is linerized quadcopter body dynmaics
%          B is the linearized actuator dynamics
%
%   Non-linear
%
% Dynamic equation for a quadcopter (Bolandil-ICA-2013)
%         x1  = x
%         x2  = y
%         x3  = z
%         x4  = phi
%         x5  = theta
%   X  =  x6  = psi
%         x7  = x_dot
%         x8  = y_dot
%         x9  = z_dot
%         x10 = phi_dot
%         x11 = theta_dot
%         x12 = psi_dot
%
%
%             x1_dot  = x7
%             x2_dot  = x8
%             x3_dot  = x9
%             x4_dot  = x10
%             x5_dot  = x11
%   X_dot  =  x6_dot  = x12
%             x7_dot  = x_ddot     = -(sin(theta)*cos(phi))*(u1/m) 
%                                  = -(sin(x5)*cos(x4))*(u1/m)
%
%             x8_dot  = y_ddot     = sin(phi)*(u1/m)
%                                  = sin(x4)*(u1/m)
%
%             x9_dot  = z_ddot     = -(cos(theta)*cos(phi))*(u1/m) + g
%                                  = -(cos(x5)*cos(x4))*(u1/m) + g
%
%             x10_dot = phi_ddot   =   -psi_dot*theta_dot*cos(phi)
%                                    + [len*cos(psi)*u2]/I_xx
%                                    - [len*sin(psi)*u3]/I_yy
%                                    + [(I_yy - I_zz)/I_xx]*(psi_dot - theta_dot*sin(phi))*(theta_dot*cos(phi))
%
%                                  =   -x12*x11*cos(x4)
%                                    + [len*cos(x6)*u2]/I_xx
%                                    - [len*sin(x6)*u3]/I_yy
%                                    + [(I_yy - I_zz)/I_xx]*(x12 - x11*sin(x4))*(x11*cos(x4))
%
%             x11_dot = theta_ddot =   (psi_dot*phi_dot)/cos(phi) + phi_dot*theta_dot*tan(phi) 
%                                    + [len*(sin(psi)/cos(phi))*u2]/I_xx 
%                                    + [len*(cos(psi)/cos(phi))*u3]/I_yy 
%                                    - [(I_yy - I_zz)/I_xx]*(psi_dot - theta_dot*sin(phi))*(phi_dot/cos(phi))
%
%                                  =   (x12*x10)/cos(x4) + x10*x11*tan(x4)
%                                    + [len*(sin(x6)/cos(x4))*u2]/I_xx 
%                                    + [len*(cos(x6)/cos(x4))*u3]/I_yy
%                                    - [(I_yy - I_zz)/I_xx]*(x12 - x11*sin(x4))*(x10/cos(x4))
%
%             x12_dot = psi_ddot   =   phi_dot*psi_dot*tan(phi) + (phi_dot*theta_dot)/cos(phi) 
%                                    + [len*sin(psi)*tan(phi)*u2]/I_xx
%                                    + [len*cos(psi)*tan(phi)*u3]/I_yy 
%                                    + [len*u4]/I_zz 
%                                    - [(I_yy - I_zz)/I_xx]*(psi_dot - theta_dot*sin(phi))*(phi_dot*tan(phi))
%
%                                  =   x10*x12*tan(x4) + (x10*x11)/cos(x4) 
%                                    + [len*sin(x6)*tan(x4)*u2]/I_xx 
%                                    + [len*cos(x6)*tan(x4)*u3]/I_yy 
%                                    + [len*u4]/I_zz 
%                                    - [(I_yy - I_zz)/I_xx]*(x12 - x11*sin(x4))*(x10*tan(x4))
%
%
%
% Linear (assume operating all states at or near 0)
%
%             x1_dot  = x7
%             x2_dot  = x8
%             x3_dot  = x9
%             x4_dot  = x10
%             x5_dot  = x11
%   X_dot  =  x6_dot  = x12
%             x7_dot  = x_ddot     = -(theta)*(u1/m) = -(theta)*(g)  (at equil u1 ~ g*m)
%                                  = -(x5)*(u1/m)    = -(x5)*(g)
%
%             x8_dot  = y_ddot     = (phi)*(u1/m) = (phi)*(u1)  (at equil u1 ~ g*m)
%                                  = (x4)*(u1/m)  = (x4)*(g)
%
%             x9_dot  = z_ddot     = -(u1/m) + g  = -u1/m  (at equilibrium u1 is u1+g*m)
%                                  = -(u1/m) + g  = -u1/m
%
%             x10_dot = phi_ddot   =   [len*u2]/I_xx
%                                  =   [len*u2]/I_xx
%
%             x11_dot = theta_ddot =   [len*u3]/I_yy 
%                                  =   [len*u3]/I_yy
%
%             x12_dot = psi_ddot   =   [len*u4]/I_zz 
%                                  =   [len*u4]/I_zz 
%
%     Matrix A :
%       x   y   z   phi theta psi x_dot y_dot z_dot phi_dot theta_dot psi_dot    
%       x1  x2  x3  x4   x5   x6   x7    x8    x9     x10      x11    x12
% A = [ 0   0   0   0    0    0    1     0     0      0        0      0; ...
%       0   0   0   0    0    0    0     1     0      0        0      0; ...
%       0   0   0   0    0    0    0     0     1      0        0      0; ...
%       0   0   0   0    0    0    0     0     0      1        0      0; ...
%       0   0   0   0    0    0    0     0     0      0        1      0; ...
%       0   0   0   0    0    0    0     0     0      0        0      1; ...
%       0   0   0   0   -g    0    0     0     0      0        0      0; ...
%       0   0   0   g    0    0    0     0     0      0        0      0; ...
%       0   0   0   0    0    0    0     0     0      0        0      0; ...
%       0   0   0   0    0    0    0     0     0      0        0      0; ...
%       0   0   0   0    0    0    0     0     0      0        0      0; ...
%       0   0   0   0    0    0    0     0     0      0        0      0]
%
%   Matrix B :
%       u1       u2        u3         u4
% B = [ 0        0         0          0; ...    % x1_dot  (x_dot) 
%       0        0         0          0; ...    % x2_dot  (y_dot)
%       0        0         0          0; ...    % x3_dot  (z_dot)
%       0        0         0          0; ...    % x4_dot  (phi_dot)
%       0        0         0          0; ...    % x5_dot  (theta_dot)
%       0        0         0          0; ...    % x6_dot  (psi_dot)
%       0        0         0          0; ...    % x7_dot  (x_ddot)
%       0        0         0          0; ...    % x8_dot  (y_ddot)
%      -1/m      0         0          0; ...    % x9_dot  (z_ddot)
%       0   len_xy/I_xx    0          0; ...    % x10_dot (phi_ddot)
%       0        0     len_xy/I_yy    0; ...    % x11_dot (theta_ddot)
%       0        0         0       len/I_zz ]   % x12_dot (psi_ddot)
%
%  Where CrazyFlie constants are:
%  g = 9.8;        % (m/s^2) Gravity
%  m = 28/1000;    % (Kg) Mass of quad (Crazyflie max take-off weight  ~42g) (Foster-2015)
%  len = (.092/2); % (m) Length of quad lever arm for yaw force (92mm diameter of CrazyFlie)
%  len_xy = (.092/2)*sin(pi/4); % (m) Length of quad lever arm for pitch/roll force: Quad arm length * projection onto pitch/roll axis
%  I_xx = 1.33e-5; % (Kg*m^2) Moment of inertia about X-axis (McInerney-MS-Thesis-2017)
%  I_yy = 1.33e-5; % (Kg*m^2) Moment of inertia about Y-axis (McInerney-MS-Thesis-2017)
%  I_zz = 2.64e-5; % (Kg*m^2) Moment of inertia about Z-axis (McInerney-MS-Thesis-2017)
%
%  LQR: Q and R matrix (Note the Q and R matrix is differnt from the Q and
%       R in LQR.
%
%  Q = [1  0  0  0  0  0  0  0  0  0  0  0; ...
%       0  1  0  0  0  0  0  0  0  0  0  0; ...
%       0  0  1  0  0  0  0  0  0  0  0  0; ...
%       0  0  0  1  0  0  0  0  0  0  0  0; ...
%       0  0  0  0  1  0  0  0  0  0  0  0; ...
%       0  0  0  0  0  1  0  0  0  0  0  0; ...
%       0  0  0  0  0  0  1  0  0  0  0  0; ...
%       0  0  0  0  0  0  0  1  0  0  0  0; ...
%       0  0  0  0  0  0  0  0  1  0  0  0; ...
%       0  0  0  0  0  0  0  0  0  1  0  0; ...
%       0  0  0  0  0  0  0  0  0  0  1  0; ...
%       0  0  0  0  0  0  0  0  0  0  0  1]
%
%  R = [1  0  0  0; ...
%       0  1  0  0; ...
%       0  0  1  0; ...
%       0  0  0  1]

% K from matlab lqr function: [K, S, E] = lqr(A, B, Q, R)  (Continuous time)

K = [ 0.0000    0.0000   -1.0000    0.0000   -0.0000    0.0000    0.0000    0.0000   -1.0276    0.0000   -0.0000    0.0000; ...
      0.0000    1.0000   -0.0000    5.4330   -0.0000    0.0000    0.0000    1.4522    0.0000    1.0022    0.0000    0.0000; ...
     -1.0000    0.0000   -0.0000    0.0000    5.4330   -0.0000   -1.4522    0.0000   -0.0000    0.0000    1.0022   -0.0000; ...
      0.0000   -0.0000   -0.0000   -0.0000   -0.0000    1.0000    0.0000   -0.0000   -0.0000   -0.0000   -0.0000    1.0006];


% K from matlab lqrd function: [K, S, E] = lqr(A, B, Q, R,Ts)  (Discrete time)
% where Ts = .001 seconds
dK_u_1111 = [ 0.0000   -0.0000   -0.9819    0.0000   -0.0000   -0.0000    0.0000    0.0000   -1.0095    0.0000    0.0000   -0.0000; ...
             -0.0000    0.4076   -0.0000    2.2178    0.0000   -0.0000   -0.0000    0.5921   -0.0000    0.4098    0.0000   -0.0000; ...
             -0.4076   -0.0000    0.0000   -0.0000    2.2178   -0.0000   -0.5921   -0.0000    0.0000   -0.0000    0.4098   -0.0000; ...
             -0.0000   -0.0000    0.0000   -0.0000    0.0000    0.5021   -0.0000   -0.0000    0.0000   -0.0000    0.0000    0.5027];

dK_u_2111 = [-0.0000   -0.0000   -0.6979   -0.0000   -0.0000    0.0000    0.0000   -0.0000   -0.7254    0.0000   -0.0000    0.0000; ...
             -0.0000    0.4076    0.0000    2.2178    0.0000    0.0000   -0.0000    0.5921   -0.0000    0.4098   -0.0000    0.0000; ...
             -0.4076   -0.0000    0.0000   -0.0000    2.2178    0.0000   -0.5921   -0.0000    0.0000    0.0000    0.4098    0.0000; ...
              0.0000    0.0000   -0.0000    0.0000   -0.0000    0.5021    0.0000    0.0000    0.0000    0.0000   -0.0000    0.5027];


dK_u_10_1_1_1 = [ 0.0000    0.0000   -0.3143    0.0000    0.0000   -0.0000   -0.0000   -0.0000   -0.3412    0.0000    0.0000   -0.0000; ...
                  0.0000    0.4076   -0.0000    2.2178   -0.0000   -0.0000    0.0000    0.5921   -0.0000    0.4098   -0.0000   -0.0000; ...
                 -0.4076   -0.0000   -0.0000   -0.0000    2.2178   -0.0000   -0.5921    0.0000   -0.0000    0.0000    0.4098   -0.0000; ...
                 -0.0000   -0.0000    0.0000   -0.0000    0.0000    0.5021   -0.0000   -0.0000    0.0000   -0.0000    0.0000    0.5027];


dK_u_10_10_10_1 = [-0.0000    0.0000   -0.3143    0.0000    0.0000   -0.0000   -0.0000    0.0000   -0.3412    0.0000    0.0000   -0.0000; ...
                    0.0000    0.2235   -0.0000    1.2186   -0.0000    0.0000    0.0000    0.3248   -0.0000    0.2257    0.0000    0.0000; ...
                   -0.2235    0.0000    0.0000   -0.0000    1.2186   -0.0000   -0.3248    0.0000   -0.0000   -0.0000    0.2257   -0.0000; ...
                    0.0000    0.0000    0.0000    0.0000   -0.0000    0.5021    0.0000    0.0000    0.0000    0.0000    0.0000    0.5027];


dK_u_10_10_10_xang_01_01_1 = [-0.0000    0.0000   -0.3143   -0.0000    0.0000    0.0000   -0.0000    0.0000   -0.3412   -0.0000    0.0000    0.0000; ...
                               0.0000    0.2235    0.0000    1.1971    0.0000   -0.0000   -0.0000    0.3233   -0.0000    0.2256   -0.0000   -0.0000; ...
                              -0.2235    0.0000   -0.0000    0.0000    1.1971   -0.0000   -0.3233    0.0000   -0.0000   -0.0000    0.2256   -0.0000; ...
                               0.0000   -0.0000   -0.0000   -0.0000   -0.0000    0.5021    0.0000   -0.0000   -0.0000   -0.0000   -0.0000    0.5027];

dK_u_10_10_10_xang_001_001_1 = [-0.0000   -0.0000   -0.3143   -0.0000    0.0000    0.0000   -0.0000   -0.0000   -0.3412   -0.0000    0.0000   -0.0000; ...
                                -0.0000    0.2235    0.0000    1.1950    0.0000   -0.0000   -0.0000    0.3232    0.0000    0.2256    0.0000   -0.0000; ...
                                -0.2235   -0.0000   -0.0000    0.0000    1.1950    0.0000   -0.3232   -0.0000   -0.0000    0.0000    0.2256   -0.0000; ...
                                -0.0000   -0.0000   -0.0000   -0.0000    0.0000    0.5021   -0.0000   -0.0000   -0.0000   -0.0000    0.0000    0.5027];


dK_u_10_10_10_xang_001_001_1_vang_01_01_1 = [ 0.0000    0.0000   -0.3143   -0.0000    0.0000   -0.0000   -0.0000    0.0000   -0.3412   -0.0000    0.0000   -0.0000; ...
                                             -0.0000    0.2799    0.0000    0.7962    0.0000   -0.0000   -0.0000    0.3519   -0.0000    0.0921    0.0000   -0.0000; ...
                                             -0.2799   -0.0000    0.0000   -0.0000    0.7962   -0.0000   -0.3519   -0.0000   -0.0000    0.0000    0.0921    0.0000; ...
                                              0.0000    0.0000    0.0000   -0.0000   -0.0000    0.5021    0.0000    0.0000    0.0000   -0.0000   -0.0000    0.5027];


dK_u_10_10_10_xang_0001_0001_1_vang_005_005_1 = [ 0.0000    0.0000   -0.3143   -0.0000   -0.0000    0.0000    0.0000    0.0000   -0.3412   -0.0000   -0.0000    0.0000; ...
                                                  0.0000    0.2892    0.0000    0.6883    0.0000    0.0000    0.0000    0.3525    0.0000    0.0689    0.0000    0.0000; ...
                                                 -0.2892    0.0000    0.0000    0.0000    0.6883    0.0000   -0.3525   -0.0000    0.0000    0.0000    0.0689    0.0000; ...
                                                 -0.0000    0.0000   -0.0000    0.0000    0.0000    0.5021   -0.0000    0.0000   -0.0000    0.0000    0.0000    0.5027];

dK_u_10_10_10_xang_0001_0001_1_vang_005_005_1_v_01_01_1 = [-0.0000   -0.0000   -0.3143   -0.0000    0.0000    0.0000   -0.0000   -0.0000   -0.3412   -0.0000    0.0000   -0.0000; ...
                                                           -0.0000    0.2896   -0.0000    0.5086    0.0000   -0.0000   -0.0000    0.1961    0.0000    0.0679    0.0000   -0.0000; ...
                                                           -0.2896    0.0000   -0.0000   -0.0000    0.5086   -0.0000   -0.1961    0.0000   -0.0000    0.0000    0.0679    0.0000; ...
                                                           -0.0000    0.0000    0.0000   -0.0000   -0.0000    0.5021   -0.0000   -0.0000   -0.0000   -0.0000    0.0000    0.5027];

dK_u_10_10_10_xang_0001_0001_1_vang_005_005_1_v_01_01_01 = [ 0.0000   -0.0000   -0.3153    0.0000   -0.0000    0.0000    0.0000    0.0000   -0.1661    0.0000   -0.0000    0.0000; ...
                                                            -0.0000    0.2896   -0.0000    0.5086   -0.0000   -0.0000    0.0000    0.1961   -0.0000    0.0679   -0.0000   -0.0000; ...
                                                            -0.2896   -0.0000    0.0000   -0.0000    0.5086   -0.0000   -0.1961   -0.0000    0.0000   -0.0000    0.0679   -0.0000; ...
                                                             0.0000   -0.0000   -0.0000   -0.0000   -0.0000    0.5021    0.0000    0.0000   -0.0000   -0.0000   -0.0000    0.5027];

dK_u_10_50_50_xang_0001_0001_1_vang_005_005_1_v_01_01_01 = [-0.0000   -0.0000   -0.3153   -0.0000    0.0000   -0.0000   -0.0000   -0.0000   -0.1661   -0.0000    0.0000    0.0000; ...
                                                            -0.0000    0.1356    0.0000    0.2444    0.0000   -0.0000   -0.0000    0.0928    0.0000    0.0335    0.0000   -0.0000; ...
                                                            -0.1356   -0.0000   -0.0000   -0.0000    0.2444    0.0000   -0.0928   -0.0000   -0.0000   -0.0000    0.0335    0.0000; ...
                                                            -0.0000   -0.0000    0.0000   -0.0000    0.0000    0.5021   -0.0000   -0.0000    0.0000   -0.0000    0.0000    0.5027];

dK_u_10_100_100_xang_0001_0001_1_vang_005_005_1_v_01_01_01 = [ 0.0000   -0.0000   -0.3153   -0.0000   -0.0000   -0.0000   -0.0000   -0.0000   -0.1661    0.0000   -0.0000   -0.0000; ...
                                                               0.0000    0.0970    0.0000    0.1781   -0.0000    0.0000   -0.0000    0.0668   -0.0000    0.0248   -0.0000   -0.0000; ...
                                                              -0.0970   -0.0000    0.0000    0.0000    0.1781   -0.0000   -0.0668   -0.0000    0.0000   -0.0000    0.0248    0.0000; ...
                                                              -0.0000    0.0000    0.0000    0.0000   -0.0000    0.5021    0.0000    0.0000    0.0000   -0.0000    0.0000    0.5027];

dK_u_10_500_500_xang_0001_0001_1_vang_005_005_1_v_01_01_01 = [-0.0000   -0.0000   -0.3153    0.0000   -0.0000    0.0000    0.0000    0.0000   -0.1661    0.0000   -0.0000    0.0000; ...
                                                              -0.0000    0.0440   -0.0000    0.0871    0.0000   -0.0000    0.0000    0.0312   -0.0000    0.0130    0.0000   -0.0000; ...
                                                              -0.0440   -0.0000    0.0000   -0.0000    0.0871   -0.0000   -0.0312   -0.0000    0.0000    0.0000    0.0130    0.0000; ...
                                                               0.0000   -0.0000    0.0000    0.0000   -0.0000    0.5021    0.0000   -0.0000   -0.0000   -0.0000   -0.0000    0.5027];


dK_u_50_500_500_xang_0001_0001_1_vang_005_005_1_v_01_01_01 = [ 0.0000   -0.0000   -0.1412   -0.0000   -0.0000    0.0000    0.0000   -0.0000   -0.0995   -0.0000   -0.0000    0.0000; ...
                                                              -0.0000    0.0440    0.0000    0.0871    0.0000   -0.0000   -0.0000    0.0312    0.0000    0.0130    0.0000   -0.0000; ...
                                                              -0.0440    0.0000    0.0000    0.0000    0.0871    0.0000   -0.0312    0.0000    0.0000    0.0000    0.0130    0.0000; ...
                                                              -0.0000    0.0000    0.0000    0.0000    0.0000    0.5021   -0.0000   -0.0000   -0.0000   -0.0000   -0.0000    0.5027];


dK_u_50_500_500_xang_0001_0001_1_vang_005_005_1_v_01_01_005 = [ 0.0000    0.0000   -0.1412    0.0000    0.0000   -0.0000    0.0000    0.0000   -0.0944    0.0000    0.0000   -0.0000; ...
                                                               -0.0000    0.0440   -0.0000    0.0871    0.0000    0.0000   -0.0000    0.0312   -0.0000    0.0130    0.0000    0.0000; ...
                                                               -0.0440    0.0000   -0.0000    0.0000    0.0871   -0.0000   -0.0312    0.0000   -0.0000    0.0000    0.0130   -0.0000; ...
                                                               -0.0000    0.0000    0.0000    0.0000    0.0000    0.5021   -0.0000    0.0000    0.0000    0.0000   -0.0000    0.5027];


dK_u_50_500_500_xang_0001_0001_1_vang_005_005_1_v_01_01_001 = [-0.0000   -0.0000   -0.1412    0.0000   -0.0000   -0.0000    0.0000   -0.0000   -0.0900    0.0000   -0.0000   -0.0000; ...
                                                               -0.0000    0.0440   -0.0000    0.0871    0.0000   -0.0000   -0.0000    0.0312   -0.0000    0.0130   -0.0000   -0.0000; ...
                                                               -0.0440    0.0000    0.0000   -0.0000    0.0871    0.0000   -0.0312   -0.0000    0.0000    0.0000    0.0130   -0.0000; ...
                                                               -0.0000   -0.0000    0.0000    0.0000    0.0000    0.5021   -0.0000   -0.0000    0.0000   -0.0000   -0.0000    0.5027];


dK_u_100_500_500_xang_0001_0001_1_vang_005_005_1_v_01_01_001 = [ 0.0000    0.0000   -0.0999    0.0000   -0.0000    0.0000    0.0000    0.0000   -0.0754    0.0000   -0.0000    0.0000; ...
                                                                 0.0000    0.0440    0.0000    0.0871   -0.0000   -0.0000    0.0000    0.0312   -0.0000    0.0130   -0.0000   -0.0000; ...
                                                                -0.0440    0.0000    0.0000    0.0000    0.0871   -0.0000   -0.0312    0.0000    0.0000   -0.0000    0.0130   -0.0000; ...
                                                                -0.0000   -0.0000   -0.0000    0.0000   -0.0000    0.5021   -0.0000   -0.0000   -0.0000   -0.0000   -0.0000    0.5027];


dK_488_matrix = [
        -0.0   0.0    -0.9698 -0.0   0.0     -0.0 0.0   0.0   -1.3878 -0.0   0.0     -0.0;
        0.0    0.9678 0.0     0 0.0     -0.0 0.0   1.429 0.0     0 0.0     -0.0;
        0.9678 -0.0   0.0     0.0    -0 -0.0 1.429 -0.0  0.0     0.0    -0 -0.0;
        -0.0   -0.0   0.0     -0.0   0.0     0.0  -0.0  -0.0  0.0     -0.0   0.0     0
];


U_col = -dK_u_100_500_500_xang_0001_0001_1_vang_005_005_1_v_01_01_001*X.';

U = U_col.';

end
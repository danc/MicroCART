function [y, X, U, tspan] = compute_quad_wrapper(tstep, X, U, X_ref,F_hover, tspan)

% tspan=[0 tstep]; 
X_init = X(end,:); % Initialize state from where last ode left off
tspan=[tspan(2) tspan(2)+tstep];  % Advance to next time interval

[~, X] = ode45(@(t,X) compute_quad(t,X,U), tspan, X_init); % Simulate

% Xplot = [Xplot; [t, X]]; % log data for plotting later (this is the main thing that contributes to time

% Compute feedback (i.e. Force inputs (U) in terms of state (X) )
%Get current state;
X_c = X(end,:);

% LQR control law
U = lqr_quad_gains(X_c-X_ref); % LQR controller gains applied to error 
                             % between current state (X_c), and
                             % refernce setpoint (X_ref).

U(1) = U(1) + F_hover;  % LQR controlling quad at hoover equilibrium 




euler_angles = [X_c(6) X_c(5) X_c(4)];
displacement = [X_c(1) X_c(2) X_c(3)];

y = [euler_angles displacement]; 

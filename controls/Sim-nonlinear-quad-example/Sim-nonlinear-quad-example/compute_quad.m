function [X_dot] = compute_quad(t, X, U)
% Dynamic equation for a quadcopter (Bolandil-ICA-2013)
%         x1  = x
%         x2  = y
%         x3  = z
%         x4  = phi
%         x5  = theta
%   X  =  x6  = psi
%         x7  = x_dot
%         x8  = y_dot
%         x9  = z_dot
%         x10 = phi_dot
%         x11 = theta_dot
%         x12 = psi_dot
%
%
%             x1_dot  = x7
%             x2_dot  = x8
%             x3_dot  = x9
%             x4_dot  = x10
%             x5_dot  = x11
%   X_dot  =  x6_dot  = x12
%             x7_dot  = x_ddot     = -(sin(theta)*cos(phi))*(u1/m) 
%                                  = -(sin(x5)*cos(x4))*(u1/m)
%
%             x8_dot  = y_ddot     = sin(phi)*(u1/m)
%                                  = sin(x4)*(u1/m)
%
%             x9_dot  = z_ddot     = -(cos(theta)*cos(phi))*(u1/m) + g
%                                  = -(cos(x5)*cos(x4))*(u1/m) + g
%
%             x10_dot = phi_ddot   =   -psi_dot*theta_dot*cos(phi)
%                                    + [len*cos(psi)*u2]/I_xx
%                                    - [len*sin(psi)*u3]/I_yy
%                                    + [(I_yy - I_zz)/I_xx]*(psi_dot - theta_dot*sin(phi))*(theta_dot*cos(phi))
%
%                                  =   -x12*x11*cos(x4)
%                                    + [len*cos(x6)*u2]/I_xx
%                                    - [len*sin(x6)*u3]/I_yy
%                                    + [(I_yy - I_zz)/I_xx]*(x12 - x11*sin(x4))*(x11*cos(x4))
%
%             x11_dot = theta_ddot =   (psi_dot*phi_dot)/cos(phi) + phi_dot*theta_dot*tan(phi) 
%                                    + [len*(sin(psi)/cos(phi))*u2]/I_xx 
%                                    + [len*(cos(psi)/cos(phi))*u3]/I_yy 
%                                    - [(I_yy - I_zz)/I_xx]*(psi_dot - theta_dot*sin(phi))*(phi_dot/cos(phi))
%
%                                  =   (x12*x10)/cos(x4) + x10*x11*tan(x4)
%                                    + [len*(sin(x6)/cos(x4))*u2]/I_xx 
%                                    + [len*(cos(x6)/cos(x4))*u3]/I_yy
%                                    - [(I_yy - I_zz)/I_xx]*(x12 - x11*sin(x4))*(x10/cos(x4))
%
%             x12_dot = psi_ddot   =   phi_dot*psi_dot*tan(phi) + (phi_dot*theta_dot)/cos(phi) 
%                                    + [len*sin(psi)*tan(phi)*u2]/I_xx
%                                    + [len*cos(psi)*tan(phi)*u3]/I_yy 
%                                    + [len*u4]/I_zz 
%                                    - [(I_yy - I_zz)/I_xx]*(psi_dot - theta_dot*sin(phi))*(phi_dot*tan(phi))
%
%                                  =   x10*x12*tan(x4) + (x10*x11)/cos(x4) 
%                                    + [len*sin(x6)*tan(x4)*u2]/I_xx 
%                                    + [len*cos(x6)*tan(x4)*u3]/I_yy 
%                                    + [len*u4]/I_zz 
%                                    - [(I_yy - I_zz)/I_xx]*(x12 - x11*sin(x4))*(x10*tan(x4))
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % Constants
  g = 9.8;        % (m/s^2) Gravity
  m = 28/1000;    % (Kg) Mass of quad (Crazyflie max take-off weight  ~42g) (Foster-2015)
  len = (.092/2); % (m) Length of quad lever arm for yaw force (92mm diameter of CrazyFlie)
  len_xy = (.092/2)*sin(pi/4); % (m) Length of quad lever arm for pitch/roll force: Quad arm length * projection onto pitch/roll axis
  I_xx = 1.33e-5; % (Kg*m^2) Moment of inertia about X-axis (McInerney-MS-Thesis-2017)
  I_yy = 1.33e-5; % (Kg*m^2) Moment of inertia about Y-axis (McInerney-MS-Thesis-2017)
  I_zz = 2.64e-5; % (Kg*m^2) Moment of inertia about Z-axis (McInerney-MS-Thesis-2017)

  % To reduce indexing syntax, reassign input vector
  % to individule state varibles
  x1  = X(1);  % x
  x2  = X(2);  % y
  x3  = X(3);  % z
  x4  = X(4);  % phi
  x5  = X(5);  % theta
  x6  = X(6);  % psi
  x7  = X(7);  % x_dot
  x8  = X(8);  % y_dot
  x9  = X(9);  % z_dot
  x10 = X(10); % phi_dot
  x11 = X(11); % theta_dot
  x12 = X(12); % psi_dot

  % Input forces
  u1 = U(1); % Total force (Note: always normal to yaw motion): Crazy Fly max ~.6 N (Foster-2015)
  u2 = U(2); % Roll force
  u3 = U(3); % Pitch force
  u4 = U(4); % Yaw force  

  % Compute Quad dynamics
  x1_dot  = x7;   % x_dot
  x2_dot  = x8;   % y_dot
  x3_dot  = x9;   % z_dot
  x4_dot  = x10;  % phi_dot
  x5_dot  = x11;  % theta_dot
  x6_dot  = x12;  % psi_dot
  x7_dot  =   -(sin(x5)*cos(x4))*(u1/m);      % x_ddot
  x8_dot  =   sin(x4)*(u1/m);                 % y_ddot
  x9_dot  =   -(cos(x5)*cos(x4))*(u1/m) + g;  % z_ddot

  x10_dot =   -x12*x11*cos(x4) ...
            + (len_xy*cos(x6)*u2)/I_xx ...
            - (len_xy*sin(x6)*u3)/I_yy ...
            + ((I_yy - I_zz)/I_xx)*(x12 - x11*sin(x4))*(x11*cos(x4)); % phi_ddot

  x11_dot =   (x12*x10)/cos(x4) + x10*x11*tan(x4) ...
            + (len_xy*(sin(x6)/cos(x4))*u2)/I_xx ... 
            + (len_xy*(cos(x6)/cos(x4))*u3)/I_yy ... 
            - ((I_yy - I_zz)/I_xx)*(x12 - x11*sin(x4))*(x10/cos(x4)); % theta_ddot

  x12_dot =   x10*x12*tan(x4) + (x10*x11)/cos(x4) ... 
            + (len_xy*sin(x6)*tan(x4)*u2)/I_xx ... 
            + (len_xy*cos(x6)*tan(x4)*u3)/I_yy ... 
            + (len*u4)/I_zz ... 
            - ((I_yy - I_zz)/I_xx)*(x12 - x11*sin(x4))*(x10*tan(x4)); % psi_ddot

  % X_dot : State variable direvative output
  X_dot = [x1_dot x2_dot x3_dot x4_dot x5_dot x6_dot ... 
           x7_dot x8_dot x9_dot x10_dot x11_dot x12_dot]';
end
% Plot Quadcopter motion

% Constants
g = 9.8;      % (m/s^2) Gravity
m = 23.1/1000;  % (Kg) Mass of quad (Crazy Fly flying weight (Foster-2015)
F_hover = m*g; % Force required for Crazy Flie to hover
%gamepad = vrjoystick(1);

% Initial states
x1  = 0;  % x
x2  = 0;  % y
x3  = 0;  % z
x4  = 0;  % phi
x5  = 0;  % theta
x6  = 0;  % psi
x7  = 0;  % x_dot
x8  = 0;  % y_dot
x9  = 0;  % z_dot
x10 = 0;  % phi_dot
x11 = 0;  % theta_dot
x12 = 0;  % psi_dot

X_init=[x1  x2  x3  x4  x5  x6 ... 
        x7  x8  x9  x10 x11 x12];
Xsize = 11;

% Initialize forces
u1 = F_hover;  % Total force (Note: always normal to yaw motion)
u2 = 0;   % Roll force
u3 = 0;   % Pitch force
u4 = 0;   % Yaw force
U = [u1 u2 u3 u4];    

% Simulation time
tstep = .001; % Time step in seconds
tspan=[0 tstep]; 

runtime=15;

% Initilze loop body varibles

% Setpoint (i.e refernce)
alt_setpoint = 0;  % Altitude (-Z axis position) 
y_setpoint = 0;  % Y axis position
x_setpoint = 0;  % X axis position

% X_ref : Set point refernce state for quad to go to
%         In this case for initial state, to X/Y/Z setpoint
%         with 0 velocities, and level
X_ref = [x_setpoint y_setpoint -alt_setpoint 0 0 0 ...
         0 0 0 0 0 0];

% % Simulate model for 1st timestep (tstep)
% % PHJ: error bound setting, options = odeset('AbsTol',1e-9,'RelTol',1e-9);
[t, X] = ode45(@(t,X) compute_quad(t,X,U), tspan, X_init); % Simulate
 Xplot = [t, X]; % log time and state for plotting later
 U_plot = [tspan(1), U];

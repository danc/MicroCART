function [y, X, U, tspan, pid_states, pid_gains] = compute_quad_wrapper_pid(tstep, X, U, X_ref,F_hover, tspan, pid_states, pid_gains)

% tspan=[0 tstep]; 
X_init = X(end,:); % Initialize state from where last ode left off
tspan=[tspan(2) tspan(2)+tstep];  % Advance to next time interval

alt_error_sum = pid_states(1);
alt_error_prev = pid_states(2); 
y_error_sum = pid_states(3);
y_error_prev = pid_states(4);
phi_error_sum = pid_states(5);
phi_error_prev = pid_states(6);
x_error_sum = pid_states(7);
x_error_prev = pid_states(8);
theta_error_sum = pid_states(9);
theta_error_prev = pid_states(10);
phi_setpoint = pid_states(11);
theta_setpoint = pid_states(12);

Kp_alt = pid_gains(1); 
Ki_alt = pid_gains(2); 
Kd_alt  = pid_gains(3);
Kp_y  = pid_gains(4);
Ki_y  = pid_gains(5);
Kd_y = pid_gains(6);
Kp_phi  = pid_gains(7);
Ki_phi  = pid_gains(8);
Kd_phi  = pid_gains(9);
Kp_x  = pid_gains(10);
Ki_x  = pid_gains(11);
Kd_x = pid_gains(12);
Kp_theta  = pid_gains(13);
Ki_theta = pid_gains(14);
Kd_theta = pid_gains(15);

[~, X] = ode45(@(t,X) compute_quad(t,X,U), tspan, X_init); % Simulate

U = [0 0 0 0];
% Xplot = [Xplot; [t, X]]; % log data for plotting later (this is the main thing that contributes to time

% Compute feedback (i.e. Force inputs (U) in terms of state (X) )
%Get current state;
X_c = X(end,:);

  % Altitude PID control
alt_c = -X_c(3); % Current Altitude is -Z, as Z-axis points down

[alt_cmd, alt_error_sum, alt_error_prev] = pid_ctrl(X_ref(3),alt_c, ...
    Kp_alt, Ki_alt, Kd_alt, alt_error_sum, alt_error_prev);
U(1) = alt_cmd;


  % Y position (controlled by phi angle)
y_c = X_c(2); % Current Y position
[y_cmd, y_error_sum, y_error_prev] = pid_ctrl(X_ref(2),y_c, ...
    Kp_y, Ki_y, Kd_y, y_error_sum, y_error_prev);
phi_setpoint = y_cmd; % Use Y correction command to drive phi setpoint

  % Roll (phi) angle
phi_c = X_c(4); % Current angle phi
[phi_cmd, phi_error_sum, phi_error_prev] = pid_ctrl(phi_setpoint,phi_c, ...
    Kp_phi, Ki_phi, Kd_phi, phi_error_sum, phi_error_prev);
U(2) = phi_cmd; %phi rotation (roll), impacts Y-position @ Yaw = 0

  % X position (controlled by theta angle)
x_c = X_c(1); % Current X position
[x_cmd, x_error_sum, x_error_prev] = pid_ctrl(X_ref(1),x_c, ...
    Kp_x, Ki_x, Kd_x, x_error_sum, x_error_prev);
theta_setpoint = -x_cmd; % Use X correction command to drive -theta setpoint

  % Pitch (theta) angle
theta_c = X_c(5); % Current angle theta
[theta_cmd, theta_error_sum, theta_error_prev] = pid_ctrl(theta_setpoint,theta_c, ...
    Kp_theta, Ki_theta, Kd_theta, theta_error_sum, theta_error_prev);
U(3) = theta_cmd; %theta rotation (pitch), impacts X-position @ Yaw = 0

pid_states = [alt_error_sum alt_error_prev y_error_sum y_error_prev phi_error_sum phi_error_prev x_error_sum x_error_prev theta_error_sum theta_error_prev phi_setpoint theta_setpoint];
pid_gains = [Kp_alt Ki_alt Kd_alt Kp_y Ki_y Kd_y Kp_phi Ki_phi Kd_phi Kp_x Ki_x Kd_x Kp_theta Ki_theta Kd_theta];

euler_angles = [X_c(6) X_c(5) X_c(4)];
displacement = [X_c(1) X_c(2) X_c(3)];

y = [euler_angles displacement]; 

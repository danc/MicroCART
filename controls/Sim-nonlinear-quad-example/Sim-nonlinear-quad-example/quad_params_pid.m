% Plot Quadcopter motion

% Constants
g = 9.8;      % (m/s^2) Gravity
m = 28/1000;  % (Kg) Mass of quad (Crazy Fly flying weight (Foster-2015)
F_hover = m*g; % Force required for Crazy Flie to hover

% Initial states
x1  = 0;  % x
x2  = 0;  % y
x3  = 0;  % z
x4  = 0;  % phi
x5  = 0;  % theta
x6  = 0;  % psi
x7  = 0;  % x_dot
x8  = 0;  % y_dot
x9  = 0;  % z_dot
x10 = 0;  % phi_dot
x11 = 0;  % theta_dot
x12 = 0;  % psi_dot

X_init=[x1  x2  x3  x4  x5  x6 ... 
        x7  x8  x9  x10 x11 x12];


% Initialize forces
u1 = F_hover;  % Total force (Note: always normal to yaw motion)
u2 = 0;   % Roll force
u3 = 0;   % Pitch force
u4 = 0;   % Yaw force
U = [u1 u2 u3 u4];   


% Simulation time
tstep = .001; % Time step
tspan=[0 tstep]; 

% Initilze loop body varibles

% Altitude PID Control
alt_setpoint = 0;  % Altitude (-Z axis)
Kp_alt = 0.500;
Ki_alt =  0.001;
Kd_alt = 500;
alt_error_sum = 0;    % Sum of error
alt_error_prev = 0.;   % Previous error

% Y position PID Controll (assuming 0 yaw, i.e., psi=0)
y_setpoint = 0;  % Controlled by phi (roll) angle
Kp_y = .105;
Ki_y =  0;
Kd_y = 140;
y_error_sum = 0;    % Sum of error
y_error_prev = 0;   % Previous error
y_cmd = 0;          % correction force

% phi (roll) angle PID Controll (assuming 0 yaw, i.e., psi=0)
phi_setpoint = Kp_y*y_setpoint;  % phi  (Set by y_cmd)
Kp_phi = .001;
Ki_phi =  0;
Kd_phi = 100;
phi_error_sum = 0;    % Sum of error
phi_error_prev = 0;   % Previous error
phi_cmd = 0;          % correction force

% X position PID Controll (assuming 0 yaw, i.e., psi=0)
x_setpoint = 0;  % Controlled by -theta (-pitch) angle
Kp_x = .105;
Ki_x =  0;
Kd_x = 140;
x_error_sum = 0;    % Sum of error
x_error_prev = 0;   % Previous error
x_cmd = 0;          % correction force

% theta (pitch) angle PID Controll (assuming 0 yaw, i.e., psi=0)
theta_setpoint = -Kp_x*x_setpoint;  % theta  (Set by -x_cmd)
Kp_theta = .001;
Ki_theta =  0;
Kd_theta = 100;
theta_error_sum = 0;    % Sum of error
theta_error_prev = 0;   % Previous error
theta_cmd = 0;          % correction force

% X_ref : Set points for quad to follow
X_ref = [0 y_setpoint 0 0 0 0 ...
         0 0 0 0 0 0];
Xsize = 11;
pid_states = [alt_error_sum alt_error_prev y_error_sum y_error_prev phi_error_sum phi_error_prev x_error_sum x_error_prev theta_error_sum theta_error_prev phi_setpoint theta_setpoint];
pid_gains = [Kp_alt Ki_alt Kd_alt Kp_y Ki_y Kd_y Kp_phi Ki_phi Kd_phi Kp_x Ki_x Kd_x Kp_theta Ki_theta Kd_theta];

 

% Simulate model for 1st timestep (tstep)
% PHJ: error bound setting, options = odeset('AbsTol',1e-9,'RelTol',1e-9);
[t, X] = ode45(@(t,X) compute_quad(t,X,U), tspan, X_init); % Sim
Xplot = [t, X]; % log time and state for plotting later
U_plot = [tspan(1), U];

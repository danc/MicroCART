function [cmd, error_sum, error_prev] = pid_ctrl(setpoint,state, Kp, Ki, Kd, goal_error_sum, goal_error_prev)
% Name: pid_ctrl
% Authour: Phillip Jones (07/20/2022)
% Description: Compute a PID control law
% PID correction cmd = Kp*error + Ki*error_sum + Kd*error_diff
%

coder.extrinsic('evalin', 'assignin')

% Compute PID error terms
goal_error = setpoint - state;  % Current error
goal_error_sum = goal_error_sum + goal_error; % Error sum
goal_error_diff = goal_error - goal_error_prev; % Error difference

% Compute PID correction command
pid_cmd = Kp*goal_error + Ki*goal_error_sum + Kd*goal_error_diff;

% Return results
cmd = pid_cmd;
error_sum = goal_error_sum;  % Use as input on next pid function call
error_prev = goal_error;     % Use as input on next pid function call 
end
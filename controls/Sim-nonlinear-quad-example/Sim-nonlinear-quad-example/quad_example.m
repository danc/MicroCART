% Plot Quadcopter motion

% Constants
g = 9.8;      % (m/s^2) Gravity
m = 28/1000;  % (Kg) Mass of quad (Crazy Fly flying weight (Foster-2015)
F_hover = m*g; % Force required for Crazy Flie to hover

% Initial states
x1  = 0;  % x
x2  = 0;  % y
x3  = 0;  % z
x4  = 0;  % phi
x5  = 0;  % theta
x6  = 0;  % psi
x7  = 0;  % x_dot
x8  = 0;  % y_dot
x9  = 0;  % z_dot
x10 = 0;  % phi_dot
x11 = 0;  % theta_dot
x12 = 0;  % psi_dot

X_init=[x1  x2  x3  x4  x5  x6 ... 
        x7  x8  x9  x10 x11 x12];

% Initialize forces
u1 = F_hover;  % Total force (Note: always normal to yaw motion)
u2 = 0;   % Roll force
u3 = 0;   % Pitch force
u4 = 0;   % Yaw force
U = [u1 u2 u3 u4];    

% Simulation time
tstep = .001; % Time step in seconds
tspan=[0 tstep]; 

% Initilze loop body varibles

% Setpoint (i.e refernce)
alt_setpoint = 1;  % Altitude (-Z axis position) 
y_setpoint = 1;  % Y axis position
x_setpoint = 1;  % X axis position

% X_ref : Set point refernce state for quad to go to
%         In this case for initial state, to X/Y/Z setpoint
%         with 0 velocities, and level
X_ref = [x_setpoint y_setpoint -alt_setpoint 0 0 0 ...
         0 0 0 0 0 0];


% Simulate model for 1st timestep (tstep)
% PHJ: error bound setting, options = odeset('AbsTol',1e-9,'RelTol',1e-9);
[t, X] = ode113(@(t,X) compute_quad(t,X,U), tspan, X_init); % Simulate
Xplot = [t, X]; % log time and state for plotting later
U_plot = [tspan(1), U];

% Simulate for discrete intervals for n more time steps
n = 20000;  
for i=1:(n-1)
  [t, y, X, U, tspan] = compute_quad_wrapper(t, tstep, X, U, X_ref,F_hover, tspan);

% Log control forces
%U_plot = [U_plot; [tspan(1), U]];

end


% Reuse t and X for ploting
t = Xplot(:,1); 
X = Xplot(:,2:13);

td = U_plot(:,1);
U  = U_plot(:,2:5);

% Convert from radians to degrees
X(:,4) = X(:,4)*(180/pi);  % phi
X(:,5) = X(:,5)*(180/pi);  % theta
X(:,6) = X(:,6)*(180/pi);  % psi
X(:,10) = X(:,10)*(180/pi);  % phi_dot
X(:,11) = X(:,11)*(180/pi);  % theta_dot
X(:,12) = X(:,12)*(180/pi);  % psi_dot

[t, X]; % Print state over entire simulation time.

% Plot results of simulation
% https://en.wikipedia.org/wiki/List_of_Unicode_characters
plot_fontsize = 12;
plot_linewidth = 1;
plot_rows = 3;
plot_cols = 6;

subplot(plot_rows,plot_cols,1)
plot(t,X(:,1), 'green', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('X (m)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,2)
plot(t,X(:,2), 'blue', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('Y (m)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,3)
plot(t,-X(:,3), 'Color', '#D95319', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('Z (m)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,4)
plot(t,X(:,4), 'magenta', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel(['\phi (' char(176) ')'])
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,5)
plot(t,X(:,5), 'cyan', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel(['\theta (' char(176) ')'])
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,6)
plot(t,X(:,6), 'black', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel(['\psi (' char(176) ')'])
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,7)
plot(t,X(:,7), 'green', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('X^\prime (m/s)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,8)
plot(t,X(:,8), 'blue', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('Y^\prime (m/s)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,9)
plot(t,-X(:,9), 'Color', '#D95319', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('Z^\prime (m/s)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,10)
plot(t,X(:,10), 'magenta', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel(['\phi^\prime (' char(176) '/s)'])
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,11)
plot(t,X(:,11), 'cyan', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel(['\theta^\prime (' char(176) '/s)'])
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,12)
plot(t,X(:,12), 'black', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel(['\psi^\prime (' char(176) '/s)'])
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,13)
plot(td,U(:,1), 'Color', '#D95319', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('Thrust (N)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,14)
plot(td,U(:,2), 'magenta', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('\phi Force (N)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,15)
plot(td,U(:,3), 'cyan', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('\theta Force (N)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')

subplot(plot_rows,plot_cols,16)
plot(td,U(:,4), 'black', 'linewidth', plot_linewidth)
grid; xlabel('t (s)'); ylabel('\psi Force (N)')
set(gca,'FontSize',plot_fontsize)
title('Title: ')


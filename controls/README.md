# Controls

This folder contains the files used in developing the model of the quadcopter.

### **1. Simulink Flight Simulator**

**How to Run the Simulator**
1. In the project folder, open “MicroCART/controls/model”. The “model” folder contains the files needed to run the Simulink simulator. Make sure this folder is the current workspace directory.
2. Open “modelParameters.m” in MATLAB and run the script. This will create/update parameters in the MATLAB workspace which are used by the simulator. At this point you should also check that the contents of the “model” folder are displayed in the “Current Folder” window in MATLAB.
3. Open “Quadcopter_Model.slx”
4. To view the 3D simulator, navigate within the model by clicking on the following blocks: Sensors -> 3D Graphical Simulation -> VR Sink
5. Press “Run” in Simulink


**Changing Setpoints**
1. To change the simulation setpoints, return to the top level of the simulink model and then enter the “Setpoints” block.
2. The default version of the simulator has five step functions which individually instruct the quad to move 1 unit in the x-direction, 1 unit in the y-direction, 1 unit in the z-direction, turn clockwise by 1 unit along the yaw angle, and then return to its starting values. Each step function can be thought of as a setpoint.
3. Open a step function block by double clicking on it. Altering the step time will change when the quad begins to move towards a setpoint. Initial and final value have the format [x-position, y-position, z-position, yaw-angle], and any alterations to these fields will change where the quad starts and where it goes to respectively. 
4. Setpoints can be added or removed by adding or removing step functions from the Simulink model diagram.

    *Note:
    For this model, negative z values correspond to a position above the starting point [0,0,0,0] and positive z values correspond to a position beneath the starting point. (Example: If the quad is instructed to move from [0,0,-2,0] to [0,0,-1,0], it will be moving from a height of 2 units above the starting point down to a height of 1 unit above the starting point.) This means we will usually only work with negative z-values since our starting point is usually the ground and we cannot fly beneath it :)*


**Changing Control Models**
1. To select which controller is used to simulate quad flight, navigate to the top level of the Simulink model and toggle the controller switch to either LQR or PID




## Additional Documents
[Measuring Motor Resistance](documentation/MeasuringMotorResistance.pdf)  
[Simulink Model](documentation/SimulinkModel.md)  
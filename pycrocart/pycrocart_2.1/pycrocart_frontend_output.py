import socket
from dataclasses import dataclass as dc
from turtle import pen
from crazyflieConncetToBackend import CrazyflieConnectToBackend
import os

@dc
class backend_conn:
    socket:socket
    size:int
    buf:str

@dc
class frontend_output_data:
    block:int
    output:int
    value:float

class pycrocart_frontend_output:
    def __init__(self):
        self.backend_conn = backend_conn(None, 0, "")
        self.fe_output_data = frontend_output_data(0, 0, 0.0)

    def frontend_getoutput(self, CtB: CrazyflieConnectToBackend):
        msg = ''
        msg = f"getoutput {self.fe_output_data.block} {self.fe_output_data.output}\n"
        msg = msg[:64]
        written = 0
        try:
            written = CrazyflieConnectToBackend.backend_writeLine(self, msg)
        except socket.error as e:
            print(f"Output msg was not recieved: {e}")
        pendingResonses = 1
        response = None

        while pendingResonses > 0:
            response = CrazyflieConnectToBackend.backend_readLine(self)
            if response == None:
                


        




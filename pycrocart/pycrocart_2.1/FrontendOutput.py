import socket
from dataclasses import dataclass as dc
from turtle import pen
from FrontendCommon import CrazyflieConnectToBackend
import os

@dc
class backend_conn:
    socket:socket
    size:int
    buf:str

@dc
class frontend_output_data:
    block:int
    output:int
    value:float



def frontend_getoutput(CtB: CrazyflieConnectToBackend, output: frontend_output_data):
    msg = ''
    msg = f"getoutput {output.block} {output.output}\n"
    msg = msg[:64]
    written = 0
    try:
        written = CtB.backend_writeLine(msg)
    except socket.error as e:
        print(f"Output msg was not recieved: {e}")

    pendingResonses = 1
    response = None

    while pendingResonses > 0:
        response = CtB.backend_readLine()
        if response == None:
            print("Failed to readLine from backend(getoutput)")
            #add exit

        if response.startswith("getoutput"):
            ##not sure if this is correct???
            output.block = int(response[len("getoutput "):].strip())
            output.output = int(response[len("getoutput "):].strip())
            output.value = float(output.block[len("getoutput "):].strip())
            pendingResonses -= 1
 


        
        




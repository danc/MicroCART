import re
from socket import socket
import FrontendLogfile
import FrontendParams
import FrontendCommon
import FrontendOutput
from queue import Queue





class FrontendMain():

    def __init__(self):

        self.msg_id = 0
        self.in_q = Queue()
        self.out_q = Queue()
        self.toc = {}
        
        self.conn = FrontendCommon.CrazyflieConnectToBackend()

    #Called whenever the pycrocart front end needs to be running

    def connect(self):

        self.conn.connect_to_backend()
        ## Send message to backend to Synch Crazyflie

        if self.conn.s == None:
            print("An error occured during connection of frontend->backend")

        return self.conn

    #called to disconnect the backend from the frontend.
    def disconnect(self):
        self.conn.disconnect_from_backend()

        if self.conn.s == None:
            print("An error occured during disconnection of frontend->backend")

        return self.conn

    
    """def check_link_is_alive_callback(self):
        #"" Periodically investigate if the link is alive ""
        if self.scf:
            if not self.scf.is_link_open():
                self.is_connected = False"""

    def param_set_value(self, group: str, name: str, value: float):
        """ Set a craxyflie parameter value """

        # Get the param id from the toc
        group_dict = dict(self.toc.get(group))
        param_id = group_dict.get(name)
        #test
        print(f"param id: {param_id}")
        print(f"value: {value}")
        param_data = FrontendParams.frontend_param_data(param_id, value)

        """
        TODO implement a queue
        for now just set param right away.
        once we implement a queue its gonna be GREAT
        """
        # self.out_q.put(param_data)
        FrontendParams.frontend_setparam(self.conn, param_data)

    def get_param_value(self, group: str, name: str):
        """ Retrieve parameter value from crazyflie adapter """
        """ TODO """
        group_dict = dict(self.toc.get(group))
        param_id = group_dict.get(name)

        

        #Default, should know if error has happend
        param_value = -1.23456

        param_value = FrontendParams.frontend_getparams(self.conn, param_id)

        #test
        print(f"param_value: {param_value}")
        return param_value
        # self.out_q.put(FrontendParams.frontend_getparams(self.conn, group, name))
        # self.msg_id += 1



    #def done_setting_param_value(self, *_args):

    def get_logging_toc(self):
        """ Retrieve entire logging table of contents. Used in order to
        display list in logging tab. """
        logfile = FrontendLogfile.frontend_getlogfile_data(2, None)
        toc_path = FrontendLogfile.frontend_getlogfile(self.conn, logfile)
        logging_toc_file = open(f"{toc_path}", "r+")


        logging_toc_file.close()
    def get_param_toc(self):
        """ Get the names of all groups available for parameters on the
        crazyflie. Used to populate parameter group list on parameter tab. """

        logfile = FrontendLogfile.frontend_getlogfile_data(1, None)
        toc_path = FrontendLogfile.frontend_getlogfile(self.conn, logfile)

        print(f"file path returned from backend {toc_path}")

        # read from file
        with open(f"{toc_path}") as f:

            for line in f:
                inner_dict = {}
                if line.startswith("#Crazyflie"):
                    continue
                if line.startswith("Param"):
                    continue
                if line.startswith("Log"):
                    continue
                else:
                    words = line.split()
                    print(words)
                    group = words[2]
                    name = words[3]
                    id = words[0]
                    for keys in self.toc.keys():
                        if (group == keys):
                            inner_dict = dict(self.toc[group])
                            inner_dict.update({name : id})
                        else:
                            inner_dict.update({name : id})
                    self.toc.update({group : inner_dict})

            
            # for testing
            print(f"toc list: {self.toc}")
            group_test = {}
            group_test.update(self.toc[group])   # should provide inner dictionary
            name_test = group_test.keys()
            id_test = group_test[name]    #should provide value of id
            print(f"group test: {group_test}")
            print(f"name test: {name_test}")
            print(f"id test: {id_test}")

        f.close()
        print(f"toc list: {self.toc}")
        return self.toc

    #def add_logging_config(self, name: str, period_in_ms: int):

    #def start_logging(self):

    #def stop_logging(self):

    """
        @staticmethod
    def list_available_crazyflies():
         ### Lists crazyflies that are on and within range.
        cflib.crtp.init_drivers()  # run this again just in case you plug the
        # dongle in
        return cflib.crtp.scan_interfaces()
    """


    
        




"""
TODO
Eventually we will need to implementing threading and a two queue structures.
We will need two additional threads, one to handle the outgoing queue, another to
handle the incoming queue, while the main thread will add to the queues

"""
        
        

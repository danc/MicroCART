import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QGridLayout, \
    QWidget, QVBoxLayout, QComboBox
from PyQt5.QtCore import Qt, QTimer
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from time import time
import pyqtgraph as pg


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # Create the plot widget
        self.canvas = PlotCanvas()
        win = QWidget()
        win2 = QWidget()
        self.setCentralWidget(win)

        layout = QGridLayout()
        left_side_vertical_layout = QVBoxLayout()

        self.graph_data = True

        self.print_button = QPushButton("Start")
        self.print_button.clicked.connect(self.print_entry_boxes)
        left_side_vertical_layout.addWidget(self.print_button, 1)
        win2.setLayout(left_side_vertical_layout)
        layout.addWidget(win2, 1, 1)
        layout.addWidget(self.canvas, 1, 2)

        self.cb1 = QComboBox()
        self.line1 = "Sine"
        self.cb1.addItems(["Sine", "2Sine", "Ramp", "2Ramp", "Square"])
        left_side_vertical_layout.addWidget(self.cb1, 2)
        self.cb1.currentIndexChanged.connect(self.selectionchange)

        self.cb2 = QComboBox()
        self.line2 = "Sine"
        self.cb2.addItems(["Sine", "2Sine", "Ramp", "2Ramp", "Square"])
        left_side_vertical_layout.addWidget(self.cb2, 3)

        self.cb3 = QComboBox()
        self.line3 = "Sine"
        self.cb3.addItems(["Sine", "2Sine", "Ramp", "2Ramp", "Square"])
        left_side_vertical_layout.addWidget(self.cb3, 4)

        self.cb4 = QComboBox()
        self.line4 = "Sine"
        self.cb4.addItems(["Sine", "2Sine", "Ramp", "2Ramp", "Square"])
        left_side_vertical_layout.addWidget(self.cb4, 5)

        self.cb5 = QComboBox()
        self.line5 = "Sine"
        self.cb5.addItems(["Sine", "2Sine", "Ramp", "2Ramp", "Square"])
        left_side_vertical_layout.addWidget(self.cb5, 6)

        # Set up the timer to update the plot
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_plot_outer)
        self.timer.start(50)

        # Set the window properties
        win.setLayout(layout)
        self.setWindowTitle("Sine Wave Plot")
        self.setGeometry(100, 100, 800, 600)
        self.show()

    def update_plot_outer(self):
        if self.graph_data:

            if self.cb1.currentText() == "Sine":
                data1 = np.sin(2 * np.pi * time() / 10)
            elif self.cb1.currentText() == "2Sine":
                data1 = 2 * np.sin(2 * np.pi * time() / 10)
            elif self.cb1.currentText() == "Ramp":
                data1 = (time()) % 10
            elif self.cb1.currentText() == "2Ramp":
                data1 = 2 * (time()) % 10
            elif self.cb1.currentText() == "Square":
                data1 = (time()) % 2

                if data1 > 1:
                    data1 = 1
                else:
                    data1 = 0



            data2 = np.sin(3 * np.pi * time() / 10)
            data3 = np.sin(4 * np.pi * time() / 10)
            data4 = np.sin(5 * np.pi * time() / 10)
            data5 = np.sin(6 * np.pi * time() / 10)

            data = [data1, data2, data3, data4, data5]

            self.canvas.update_plot(data)

    def selectionchange(self):
        print("Change")


    def print_entry_boxes(self):
        print("Hello world!")

        if self.graph_data:
            self.print_button.setText("Play")
            self.graph_data = False
        else:
            self.print_button.setText("Pause")
            self.graph_data = True


class PlotCanvas(pg.PlotWidget):
    def __init__(self, parent=None, num_axes=5, width=5, height=4, dpi=100):
        super().__init__(parent=parent, width=width, height=height, dpi=dpi)
        self.setLabel('bottom', 'Time (s)')
        self.setLabel('left', 'Amplitude')
        self.setLabel('top', 'Sine Wave Plot')
        self._num_axes = num_axes

        self.current_iter = 0

        # Set the background color of the plot widget to white
        self.setBackground('w')

        if num_axes > 0:
            # Set up the sine wave data and plot
            self.plot_data = np.zeros((0, num_axes))
            self.plot_curves = \
                [self.plot(self.plot_data[:, i],
                           pen=pg.mkPen(pg.intColor(i), width=2), symbol='o',
                           symbolSize=5)
                 for i
                                in range(num_axes)]

    def update_num_axes(self):
        num_axes = 0
        if num_axes < 0:
            raise ValueError("num_axes must be greater than 0!")

        if num_axes != self._num_axes:
            if num_axes == 0:  # turn it all off
                self.clear()
            else:
                pass

    def update_plot(self, input_data: list):
        # Shift the data and add a new point
        # self.plot_data[:-1] = self.plot_data[1:]
        self.plot_data = np.append(self.plot_data, [input_data], axis=0)

        # Update the plot
        for i in range(5):
            self.plot_curves[i].setData(self.plot_data[:, i])

        self.current_iter += 1

        if self.current_iter < 100:
            self.setXRange(0, 100, padding=0)
        else:
            self.setXRange(self.current_iter-100, self.current_iter, padding=0)



# Start the application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())

import logging
import time
import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie

# Only output errors from the logging framework
logging.basicConfig(level=logging.ERROR)


def simple_connect():

    print("Yeah, I'm connected! :D")
    time.sleep(3)
    print("Now I will disconnect :'(")


def log_stab_callback(timestamp, data, logconf):
    print('[%d][%s]: %s' % (timestamp, logconf.name, data))


def simple_log_async(scf1, logconf):
    cf = scf1.cf
    cf.log.add_config(logconf)
    logconf.data_received_cb.add_callback(log_stab_callback)
    logconf.start()
    time.sleep(50)
    logconf.stop()


def get_param_toc(scf1):

    toc = scf1.cf.param.values
    print(len(toc))

    return toc


def get_logging_toc(scf1):

    toc = scf1.cf.log.toc.toc
    print(len(toc))

    return toc


def set_param(scf1, group: str, name: str, value: float):

    full_name = group + '.' + name
    scf1.cf.param.add_update_callback(group=group, name=name, cb=done_set_param)
    time.sleep(1)
    scf1.cf.param.set_value(full_name, value)
    time.sleep(1)


def done_set_param(*_args):
    print("Done setting param")


def find_available_crazyflies():
    cflib.crtp.init_drivers()

    print("Scanning interfaces for Crazyflies...")
    available = cflib.crtp.scan_interfaces()
    print("Crazyflies found:")
    for i in available:
        print(i[0])


if __name__ == '__main__':
    # Initialize the low-level drivers
    cflib.crtp.init_drivers()
    uri = 'radio://0/75/2M/E7E7E7E7E7'
    scf = SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache'))
    scf.open_link()
    scf.wait_for_params()

    print("Done")

import sys
import numpy as np
from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QLabel, \
    QLineEdit, QSlider, QPushButton
from PyQt5.QtGui import QPainter, QPen, QColor
from PyQt5.QtCore import Qt, QTimer
from time import time
import pyqtgraph as pg


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        # Set up the layout
        layout = QGridLayout()

        # Add the entry boxes
        yaw_label = QLabel("Yaw:")
        pitch_label = QLabel("Pitch:")
        roll_label = QLabel("Roll:")
        self.yaw_box = QLineEdit()
        self.pitch_box = QLineEdit()
        self.roll_box = QLineEdit()

        layout.addWidget(yaw_label, 0, 0)
        layout.addWidget(self.yaw_box, 0, 1)
        layout.addWidget(pitch_label, 1, 0)
        layout.addWidget(self.pitch_box, 1, 1)
        layout.addWidget(roll_label, 2, 0)
        layout.addWidget(self.roll_box, 2, 1)

        # Add the sliders and thrust label
        thrust_label = QLabel("Thrust:")
        self.thrust_slider = QSlider(Qt.Horizontal)
        self.thrust_slider.setMinimum(0)
        self.thrust_slider.setMaximum(100)

        layout.addWidget(thrust_label, 3, 0)
        layout.addWidget(self.thrust_slider, 3, 1)

        # Add the print button
        self.print_button = QPushButton("Print")
        self.print_button.clicked.connect(self.print_entry_boxes)

        layout.addWidget(self.print_button, 4, 0)

        # Add the plot window
        self.plot_widget = pg.PlotWidget()
        layout.addWidget(self.plot_widget, 0, 2, 5, 1)

        # Set the background color of the plot widget to white
        self.plot_widget.setBackground('w')

        # Set up the sine wave data and plot
        self.plot_data = np.zeros((100, 5))
        self.plot_curves = [self.plot_widget.plot(self.plot_data[:, i],
                                                  pen=pg.mkPen(pg.intColor(i),
                                                               width=2)) for i
                            in range(5)]

        # Set up the timer to update the plot
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_plot)
        self.timer.start(50)

        # Set the layout
        self.setLayout(layout)

    def update_plot(self):
        # Shift the data and add a new point
        self.plot_data[:-1] = self.plot_data[1:]
        self.plot_data[-1] = np.sin(
            2 * np.pi * np.array([1, 2, 3, 4, 5]) * time() / 10)

        # Update the plot
        for i in range(5):
            self.plot_curves[i].setData(self.plot_data[:, i])

    def print_entry_boxes(self):
        # Get the contents of the entry boxes
        yaw_text = self.yaw_box.text()
        pitch_text = self.pitch_box.text()
        roll_text = self.roll_box.text()

        # Attempt to parse the contents as floats
        try:
            yaw = float(yaw_text)
            pitch = float(pitch_text)
            roll = float(roll_text)

            # If parsing was successful, print the values to the terminal
            print("Yaw:", yaw)
            print("Pitch:", pitch)
            print("Roll:", roll)
        except ValueError:
            # If parsing failed, highlight the boxes in red
            if not yaw_text.isnumeric():
                self.yaw_box.setStyleSheet("background-color: red;")
            if not pitch_text.isnumeric():
                self.pitch_box.setStyleSheet("background-color: red;")
            if not roll_text.isnumeric():
                self.roll_box.setStyleSheet("background-color: red;")
        else:
            # If parsing was successful, reset the box styles and print the
            # values
            self.yaw_box.setStyleSheet("")
            self.pitch_box.setStyleSheet("")
            self.roll_box.setStyleSheet("")
            print("Yaw:", yaw)
            print("Pitch:", pitch)
            print("Roll:", roll)


# Start the application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())

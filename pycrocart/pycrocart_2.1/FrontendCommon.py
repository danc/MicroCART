import imp
import socket
from dataclasses import dataclass as dc
import os
import struct
from enum import Enum
from typing import List, Tuple
from urllib import response
from enum import IntEnum



@dc
class backend_conn:
    socket:socket.socket
    size:int
    buf:str


@dc
class frontend_param_data:
    param_id:int
    value:float



class CrazyflieConnectToBackend:
    
    def __init__(self):
        self.socket_path = '/home/bitcraze/Desktop/groundstation/groundStation/ucart.socket'
        #TODO: comeback here next
        self.backend_connection = backend_conn(None, 256, "x"*256)
        self.fe_param_data = frontend_param_data(0, 0.0)

        
    def connect_to_backend(self):

        print("Creating socket object")
        self.s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)


        print("Trying to connect... ")
        print(f"Socket path: {self.socket_path}")
        try:
            self.s.connect(self.socket_path)
        except socket.error as e:
            print(f"Socket error: {e}")

        print(f"socket {self.s}")

        self.backend_connection.socket = self.s

    def disconnect_from_backend(self):
        
        self.backend_connection.socket.close()
        if self.backend_connection.buf:
            del self.backend_connection.buf
        del self.backend_connection
    
    def backend_readLine(self):
        
        # Should maybe be changed in the future

        self.backend_connection.buf = self.backend_connection.socket.recv(self.backend_connection.size)

        data = self.backend_connection.buf.decode("utf-8")
        return data


    def backend_writeLine(self, line: str):
        line = line
        self.backend_connection.socket.sendall(line.encode())




#Enums necessary for packet decoding/encoding
class PacketHeader(IntEnum):
    BEGIN = 0
    MTYPE_L = 1
    MTYPE_H = 2
    ID_L = 3
    ID_H = 4
    DLEN_L = 5
    DLEN_H = 6
    HDR_SIZE = 7

class ChecksumFormat(IntEnum):
    CSUM_L = 0
    CSUM_SIZE = 1

class Message(Enum):
    BEGIN_CHAR = 0xBE
    END_CHAR = 0xED

# Enumeration of the data types that a callback function may use
# doubleType should get added here at some point
class DataType(IntEnum):
    floatType = 0
    intType = 0
    stringType = 2
"""
  Message type IDs used to know what kind omessagedata enum or you will break backwards compatibility.
  Add new message types in the slot between MAX_TYPE_ID and the one before it
  DO NOT change this enum without also updating the "MessageTypes" array
  in commands.c to match.
 """
class MessageTypeID(Enum):
    GETPACKETLOGS_ID = 2
    UPDATE_ID = 3
    BEGINUPDATE_ID = 4
    LOG_ID = 5
    LOG_END_ID = 6
    SETPARAM_ID = 7
    GETPARAM_ID = 8
    RESPPARAM_ID = 9
    SETSOURCE_ID = 10
    GETSOURCE_ID = 11
    RESPSOURCE_ID = 12
    GETOUTPUT_ID = 13
    RESPOUTPUT_ID = 14
    GETNODES_ID = 15
    RESPNODES_ID = 16
    ADDNODE_ID = 17
    RESPADDNODE_ID = 18
    OUTPUT_OVERRIDE_ID = 19
    SEND_RT_ID = 20
    #MAX_TYPE_ID = 21
    GETLOGFILE_ID = 21
    RESPLOGFILE_ID = 22
    LOGBLOCKCOMMAND_ID = 23

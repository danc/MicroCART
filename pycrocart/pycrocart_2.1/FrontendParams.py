import socket
from dataclasses import dataclass as dc
from tkinter import N
from urllib import response
from FrontendCommon import CrazyflieConnectToBackend, MessageTypeID, Message

@dc
class backend_conn:
    socket:socket
    size:int
    buf:str


@dc
class frontend_param_data:
    param_id:int
    value:float


@dc
class frontend_getloggile_data:
    command:int
    name:str


def frontend_getparams(conn: CrazyflieConnectToBackend, param_id:int):

    #TODO: change msg format
    #test
    print(f"param id: {param_id}")

    msg = f"getparam 0 {param_id}\n"[:64]
    param_data = frontend_param_data(param_id, 0.0)
    try:
        conn.backend_writeLine(msg)
    except socket.error as e:
        print("Error happend while writing: {e}")

    pendingResponses = 1

    while pendingResponses:
        response = conn.backend_readLine()
        if(response == None):
            print("Failed to readLine from backend(getparam)")
            
        if response.startswith("getparam"):
            param_data.value = response[len("getparam 0 0 8 "):].strip()
            pendingResponses-=1

    return param_data.value

def frontend_setparam(conn: CrazyflieConnectToBackend, param: frontend_param_data):

    param_data = frontend_param_data(param.param_id, 0.0)

    msg = f"setparam 0 {param.param_id} {param.value}\n"[:64]

    try:
        #test
        print("MSG: " + msg)
        conn.backend_writeLine(msg)
    except socket.error as e:
        print("Error happend while writing: {e}")

    msg = f"getparam 0 {param.param_id}\n"[:64]

    try:
        conn.backend_writeLine(msg)
    except socket.error as e:
        print("Error happend while writing: {e}")


    pendingResponses = 1

    while pendingResponses:
        response = conn.backend_readLine()
        if(response == None):
            print("Failed to readLine from backend(setparam)")
            break
        if response.startswith("getparam"):
            param_data.value = response[len("getparam 0 0 8 "):].strip()
            print(f"value: {param_data.value}")
            pendingResponses-=1

    return param_data.value
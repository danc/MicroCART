import imp
import socket
from dataclasses import dataclass as dc
import os
from tkinter import N
from urllib import response
from FrontendCommon import CrazyflieConnectToBackend


@dc
class backend_conn:
    socket:socket
    size:int
    buf:str


@dc
class frontend_param_data:
    param_id:int
    value:float


@dc
class frontend_getlogfile_data:
    command:int
    name:str

@dc
class frontend_logblockcommand_data:
    command:int
    id:int


def frontend_getlogfile(conn: CrazyflieConnectToBackend, logfile: frontend_getlogfile_data):

    msg = f"getlogfile {logfile.command} \n"[:64]

    try:
        conn.backend_writeLine(msg)
    except socket.error as e:
        print("Error happend while writing: {e}")

    pendingResponses = 1

    while pendingResponses:
        response = conn.backend_readLine()
        if(response == None):
            print("Failed to readLine from backend(getlogfile)")
            #add exit
        if(logfile.command == 3):
            for element in range(0, len(response)):
                if response[element] == '/t':
                    response[element] = ','
        
        if response.startswith("getlogfile"):
            logfile.name = response[len("getlogfile "):].strip()
            pendingResponses-=1

    return logfile.name

def frontend_logblock_command(conn: CrazyflieConnectToBackend, values: frontend_logblockcommand_data):

    msg =f"logblockcommand {values.command} {values.id}\n"
    msg = msg[:64]

    try:
        conn.backend_readLine(msg)
    except socket.error as e:
        print("Error happened while writing: {e}")
        #add exit
    
    if values.command == 10:
        pendingResponses = 1

    while pendingResponses:
        response = conn.backend_readLine()
        if(response == None):
            print("failed to readLine from backend (logblock_command)")
            #add exit
    

    return 0






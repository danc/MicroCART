from PyQt5.QtWidgets import QWidget, QVBoxLayout, QComboBox, QLabel
from PyQt5.QtCore import Qt


class LoggingSelectionMenu(QWidget):
    """
    Logging selection menu sits within the controls tab, handles selecting
    valid logging signals through a series of combo boxes.
    """

    def __init__(self, config_changed_callback):
        super().__init__()

        layout = QVBoxLayout()
        self.setLayout(layout)
        self.config_changed_callback = config_changed_callback  # whenever
        # the config changes, send a signal to the controls tab to press the
        # pause button

        self.available_logging_variables = []

        self.cbs = [QComboBox(), QComboBox(), QComboBox(), QComboBox(),
                    QComboBox()]

        # Default logging variables to Logging Variable 1,2,3,4,5
        colors = ['red', 'orange', 'purple', 'green', 'blue']

        for i in range(0, len(self.cbs)):
            lst = ["Logging Variable " + str(i+1)]
            lst.extend(self.available_logging_variables)
            self.cbs[i].addItems(lst)
            layout.addWidget(self.cbs[i], 1+2*i)

            color_label = QLabel("Color Label")
            color_label.setStyleSheet("QLabel {color: " + colors[i] +
                                      ";background : " + colors[i] + "}")
            color_label.setMaximumHeight(5)
            layout.addWidget(color_label, 1+2*i+1, alignment=Qt.AlignHCenter)

            # add callback for when new selection happens
            self.cbs[i].currentIndexChanged.connect(
                self.selection_change_callback)
        # todo add coloring to each combo box text that matches the plotting
        #  window line color associated with that logging variable

    def get_axis_of_signal(self, signal_name: str):
        """ Used for determining which signal is which in plotting window. """
        for i in range(0, len(self.cbs)):
            if signal_name == self.cbs[i].currentText():
                return i

    def update_available_logging_variables(self, variables_list: list):
        """ This function is called by the logging config tab upon refreshing
        the logging variables. If anything changes, the variables list will
        be updated. """
        self.available_logging_variables = []
        for variable in variables_list:
            for sub_variable in variable:
                self.available_logging_variables.append(sub_variable)
        self.config_changed_callback()
        self.selection_change_callback()

        # todo handle variables that used to be in the list and no longer are
        # todo always pause or set back to Logging Variable # whenever the
        #  old variable selected is no longer valid

    def selection_change_callback(self):
        """ Called whenever a signal selection is changed. """

        selected_signals = [
            self.cbs[0].currentText(), self.cbs[1].currentText(),
            self.cbs[2].currentText(), self.cbs[3].currentText(),
            self.cbs[4].currentText()
        ]
        modified_selection_signals = []

        # find any signals that are different from before.
        for i in range(0, len(self.available_logging_variables)):
            if not (self.available_logging_variables[i] in selected_signals):
                modified_selection_signals.append(
                    self.available_logging_variables[i])

        # update logging variable options in each menu, don't show options
        # that are selected elsewhere.
        for i in range(0, len(self.cbs)):

            self.cbs[i].currentIndexChanged.disconnect(
                self.selection_change_callback)

            signal = self.cbs[i].currentText()

            items = ["Logging Variable " + str(i+1)]
            items.extend(modified_selection_signals)
            if signal != ("Logging Variable " + str(i+1)):
                items.append(signal)

            self.cbs[i].clear()
            self.cbs[i].addItems(items)
            self.cbs[i].setCurrentText(signal)

            self.cbs[i].currentIndexChanged.connect(
                self.selection_change_callback)

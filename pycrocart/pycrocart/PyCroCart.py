"""
PyCroCart is the main script of the pycrocart gui. This includes a master class
PyCroCart that holds the connections from the gui to the crazyflie proto
connection, the setpoint handler, and the joystick reader.

The __main__ function of this script initializes all components of this
groundstation and launches the gui.
"""

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QTabWidget, \
    QHBoxLayout, QVBoxLayout, QComboBox, QPushButton, QWidget, QLabel, \
    QCheckBox, QButtonGroup
import uCartCommander
from CrazyflieProtoConnection import CrazyflieProtoConnection
from ControlTab import ControlTab
from SetpointHandler import SetpointHandler
from GamepadWizardTab import InputConfigDialogue
from cfclient.utils.input import JoystickReader
from ParameterTab import ParameterTab
from LoggingConfigTab import LoggingConfigTab
import time
import os


class PyCroCart(QMainWindow):
    """
    PyCroCart holds the connections from the gui to the crazyflie proto
    connection, the setpoint handler, and the joystick reader. It also holds
    all of the tabs to display in the GUI. PyCroCart IS the gui, but as the
    top level of the gui it also is responsible for handling the external
    connections like the joystick reader, and the crazyflie.
    """

    def __init__(self, cf: CrazyflieProtoConnection,
                 setpoint_handler: SetpointHandler):
        super().__init__()

        self.cf = cf
        self.crazyflie_selection_box = QComboBox(self)
        self.crazyflie_selection_box.setFixedWidth(150)
        
        self.connect = QPushButton("Connect", self)
        self.connect.clicked.connect(self.on_connect)
        self.connect.setFixedWidth(100)

        self.scan = QPushButton("Scan", self)
        self.scan.clicked.connect(self.on_scan)
        self.scan.setFixedWidth(100)

        # Some of the gamepad logic needs to different between crazyflie and
        # flypi drones because otherwise it's very hard to control the flypi,
        # plus whenever we use a TCP connection for the flypi,
        # that connection logic will need to be different as well
        self.crazyflie_checkbutton = QCheckBox("Crazyflie")
        self.flypi_checkbutton = QCheckBox("FlyPi")
        self.crazyflie_checkbutton.setChecked(True)  # Default to crazyflie
        self.drone_selection_group = QButtonGroup()
        self.drone_selection_group.addButton(self.crazyflie_checkbutton, 1)
        self.drone_selection_group.addButton(self.flypi_checkbutton, 2)
        self.drone_selection_group.setExclusive(True)

        self.error_label = QLabel("")
        
        button_layout = QHBoxLayout()
        button_layout.addWidget(self.crazyflie_selection_box)
        button_layout.addWidget(self.connect)
        button_layout.addWidget(self.scan)
        button_layout.addWidget(self.crazyflie_checkbutton)
        button_layout.addWidget(self.flypi_checkbutton)
        button_layout.addWidget(self.error_label)
        button_layout.addStretch(1)

        self.joystick_reader = JoystickReader()
        self.setpoint_handler = setpoint_handler

        self.tabs = QTabWidget()
        self.control_tab = ControlTab(cf.logging_queue, self.setpoint_handler,
                                      self.joystick_reader, cf)
        self.gamepad_tab = InputConfigDialogue(
            self.joystick_reader, self.control_tab.setpoint_menu.enableGamepad)
        self.parameter_tab = ParameterTab(cf)
        self.logging_config_tab = LoggingConfigTab(
            cf, self.control_tab.logging_menu.update_available_logging_variables)

        self.setGeometry(100, 200, 600, 600)

        self.tabs.addTab(self.control_tab, "Controls Window")
        self.tabs.addTab(self.gamepad_tab, "Gamepad Configuration")
        self.tabs.addTab(self.parameter_tab, "Parameter Window")
        self.tabs.addTab(self.logging_config_tab, "Logging Window")

        main_layout = QVBoxLayout()
        main_layout.addLayout(button_layout)
        main_layout.addWidget(self.tabs)

        widget = QWidget()
        widget.setLayout(main_layout)

        self.setCentralWidget(widget)

    def on_scan(self):
        """ When pressing the scan button, connect to the crazyradio,
        and scan for crazyflies. Populate the crazyflie selection box. """

        cfs = self.cf.list_available_crazyflies()
        if cfs:
            error_text = ""
            self.error_label.setText(
                "<span style='color: red;'>" + error_text + "</span>")

            # add crazyflie connections to selection box
            self.crazyflie_selection_box.clear()
            self.crazyflie_selection_box.addItems(cfs[0][:len(cfs[0])-1])
        else:
            error_text = "Either no crazyradio plugged in, or no crazyflies " \
                         "detected."
            self.error_label.setText(
                "<span style='color: red;'>" + error_text + "</span>")

            # clear out selection box
            self.crazyflie_selection_box.clear()

    def on_connect(self):

        # check that a crazyflie is selected
        self.crazyflie_checkbutton.setEnabled(False)
        self.flypi_checkbutton.setEnabled(False)

        uri = self.crazyflie_selection_box.currentText() + "/E7E7E7E7E7"

        # return an error to the user if otherwise
        if uri == "/E7E7E7E7E7":
            error_text = "No crazyflie selected."
            self.error_label.setText(
                "<span style='color: red;'>" + error_text + "</span>")

        # attempt to connect to the crazyflie
        self.cf.connect(uri)
        self.cf.scf.cf.commander = uCartCommander.Commander(cf1.scf.cf)
        self.cf.scf.wait_for_params()

        # offer the user a green error label saying connected when connected
        error_text = "Connected to drone"
        self.error_label.setText(
            "<span style='color: green;'>" + error_text + "</span>")

        # change the connect button to a disconnect button, and disconnect this
        # function from it and connect to the on_disconnect function
        self.connect.clicked.disconnect(self.on_connect)
        self.connect.clicked.connect(self.on_disconnect)
        self.connect.setText("Disconnect")

        # connect the crazyflie commander to the setpoint handler
        # refresh the logging page so that it displays the toc
        # refresh the parameter page so that it displays the correct information
        self.setpoint_handler.setCommander(self.cf.scf.cf.commander)

        # enable restricting the gamepad input for the flypi to be more
        # stable or not
        # TODO enable more granular gamepad configuration in gamepad config menu
        if self.flypi_checkbutton.isChecked():
            self.control_tab.setpoint_menu.enable_flypi_mode()
        else:
            self.control_tab.setpoint_menu.disable_flypi_mode()

        self.logging_config_tab.on_connect()
        self.parameter_tab.on_connect()

    def on_disconnect(self):

        # terminate the link in crazyflieprotoconnection
        self.cf.disconnect()

        self.crazyflie_checkbutton.setEnabled(True)
        self.flypi_checkbutton.setEnabled(True)

        error_text = ""
        self.error_label.setText(
            "<span style='color: green;'>" + error_text + "</span>")

        # change the disconnect button to a connect button, and disconnect this
        # function from it and connect the on_connect function
        self.connect.clicked.disconnect(self.on_disconnect)
        self.connect.clicked.connect(self.on_connect)
        self.connect.setText("Connect")

        # disconnect the commander from setpointhandler
        self.setpoint_handler.disconnectCommander()
        self.logging_config_tab.on_disconnect()
        self.parameter_tab.on_disconnect()




# Start the application
if __name__ == '__main__':

    app = QApplication(sys.argv)

    cf1 = CrazyflieProtoConnection()
    setpoint_handler1 = SetpointHandler()

    window = PyCroCart(cf1, setpoint_handler1)
    window.show()

    # Janky but it does work
    os._exit(app.exec_())

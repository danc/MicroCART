import math
import time
import matplotlib
from matplotlib import pyplot as plt
# matplotlib.use('module://backend_interagg')

if __name__ == '__main__':


    print("Hello world!")

    xs = []
    for i in range(0, 100):
        xs.append(i)
    ys = [0.0] * 100
    y2s = [0.0] * 100

    plt.ion()
    figure, ax = plt.subplots(figsize=(10, 8))
    line1, = ax.plot(xs, ys, '-o')
    line2, = ax.plot(xs, y2s, '-o')
    # plt.show()
    #plt.title("Distance vs. time", fontsize=20)
    #plt.xlabel("time")
    #plt.ylabel("distance (inches)")

    for i in range(0, 1000):
        ys[0:len(ys) - 1] = ys[1:len(ys)]
        ys[int(len(ys) - 1)] = i*i*math.sin(i/8)

        y2s[0:len(y2s)-1] = y2s[1:len(y2s)]
        y2s[int(len(y2s)-1)] = 0.5*i*i*math.sin(i/4)

        line1.set_xdata(xs)
        line1.set_ydata(ys)
        line2.set_ydata(y2s)
        figure.plotting_window.draw()
        figure.plotting_window.flush_events()
        if min(ys) != max(ys):
            plt.ylim(1.5*min(ys), 1.5*max(ys))
        # time.sleep(0.1)




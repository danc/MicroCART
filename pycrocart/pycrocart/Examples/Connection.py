from PyQt5.QtWidgets import QPushButton, QGridLayout, QWidget, QHBoxLayout, QComboBox


class Connection(QWidget):

    def __init__(self):
    
        super().__init__()
        
        self.dropdown = QComboBox(self)
        self.dropdown.addItem("radio://45/")
        self.dropdown.addItem("radio://100/")
        self.dropdown.setFixedWidth(150)
        
        self.connect = QPushButton("Connect", self)
        self.connect.setFixedWidth(100)

        self.scan = QPushButton("Scan", self)
        self.scan.setFixedWidth(100)
        
        button_layout = QHBoxLayout()
        button_layout.addWidget(self.dropdown)
        button_layout.addWidget(self.connect)
        button_layout.addWidget(self.scan)
        button_layout.addStretch(1)
        
        self.show()


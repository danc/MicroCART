from PyQt5.QtWidgets import QPushButton, QGridLayout, QWidget, QVBoxLayout, \
    QLabel, QHBoxLayout, QListWidget
from PyQt5.QtCore import Qt
import os
import json
from CrazyflieProtoConnection import CrazyflieProtoConnection


class LoggingConfigTab(QWidget):
    """
    LoggingConfigTab is the class that configures the logging config tab
    inside the gui. Holds the logging table of contents, and a couple buttons to
    configure the logging blocks, refresh the logging, and stop it completely.
    """

    def __init__(self, cf: CrazyflieProtoConnection,
                 logging_selection_menu_update_function):
        """
        Initialize all widgets in the logging config tab window.

        :param cf: CrazyflieProtoConnection in order to interface with
        drone.
        :param logging_selection_menu_update_function: This function is
        called whenever the logging blocks are refreshed. The purpose is to
        allow the logging selection menu to know that the logging config has
        been refreshed and to look for new signals.
        """
        super().__init__()

        layout = QHBoxLayout()
        self.setLayout(layout)
        self.cf = cf
        self.logging_selection_menu_update_function = \
            logging_selection_menu_update_function
        self.items = []
        self.items_text = []

        left_widget = QWidget()
        right_widget = QWidget()

        left_layout = QVBoxLayout()
        right_layout = QGridLayout()

        left_widget.setLayout(left_layout)
        right_widget.setLayout(right_layout)
        layout.addWidget(left_widget, 1)
        layout.addWidget(right_widget, 2)

        # ------------ Left ----------------------------------------------------

        logging_vars = QLabel("Logging Variables")
        logging_vars.setMaximumHeight(20)
        left_layout.addWidget(logging_vars, 1)

        self.list_widget = QListWidget()
        left_layout.addWidget(self.list_widget, 2)

        # ------------ Right ---------------------------------------------------

        log_block_commands = QLabel("Log Block Commands")
        log_block_commands.setMaximumHeight(50)
        right_layout.addWidget(log_block_commands, 1, 1,
                               alignment=Qt.AlignHCenter)

        self.error_label = QLabel()
        self.error_label.setMaximumHeight(50)
        right_layout.addWidget(self.error_label, 2, 1,
                               alignment=Qt.AlignHCenter)

        explanation = """
        The logging blocks file is configured to work in logging groups. The 
        name of the groups does not matter, but what does matter is the 
        "update_frequency_ms" value as well as the values inside the "vars" 
        array. Each vars array can be at most 5 values long, otherwise an 
        error will be thrown. They also must contain a logging variable found in 
        the valid logging variables, which can be seen in the menu to the left.
        The update_frequency_ms can be at most 500ms and at minimum 20 ms.
        """

        log_block_comments = QLabel(explanation)
        right_layout.addWidget(log_block_comments, 3, 1,
                               alignment=Qt.AlignCenter)

        open_log_file_button = QPushButton("Open Logging Block Setup File")
        open_log_file_button.clicked.connect(self.open_log_file)
        open_log_file_button.setMaximumWidth(500)
        right_layout.addWidget(open_log_file_button, 4, 1,
                               alignment=Qt.AlignCenter)

        horizontal_widget = QWidget()
        horizontal_layout = QHBoxLayout()
        horizontal_widget.setLayout(horizontal_layout)
        right_layout.addWidget(horizontal_widget, 5, 1)

        refresh_logs_button = QPushButton("Refresh Logging Blocks")
        refresh_logs_button.clicked.connect(self.on_refresh)
        refresh_logs_button.setMaximumWidth(200)
        horizontal_layout.addWidget(refresh_logs_button, 1)

        stop_logging_button = QPushButton("Stop Logging")
        stop_logging_button.clicked.connect(self.on_stop)
        stop_logging_button.setMaximumWidth(200)
        horizontal_layout.addWidget(stop_logging_button, 2)

    @staticmethod
    def open_log_file(_self):
        """ Open logging file using default json file editor, probably
        notepad. """
        os.startfile('logging_variables.json')

    def on_refresh(self):
        """ Whenever the refresh button is clicked, call this function. """

        self.cf.clear_logging_configs()
        self.set_logging_block_from_file()

    def on_connect(self):
        self.update_logging_values()
        self.on_refresh()

    def on_disconnect(self):
        self.update_logging_values()

    def on_stop(self):
        """ Whenever the stop logging button is clicked, call this function.
        Arguably unneccesary given the pause button on the controls page. """

        self.cf.stop_logging()

    def update_logging_values(self):
        """ Update the logging parameter values available in the left-hand
        menu. """

        # Clear away the current values
        for item in self.items:
            self.list_widget.takeItem(self.list_widget.row(item))

        # Get table of contents.
        logging_vars_toc = self.cf.get_logging_toc()
        self.list_widget.addItems(logging_vars_toc)

        # Update items list so, we can remove items next time we try.
        self.items = []
        self.items_text = []
        for x in range(self.list_widget.count()):
            self.items.append(self.list_widget.item(x))
            self.items_text.append(self.list_widget.item(x).text())

    def set_logging_block_from_file(self):
        """ Read logging variables json. Offer feedback to the user if
        formatting is incorrect. """

        with open('./logging_variables.json', 'r') as f:
            contents = json.load(f)
        # todo offer feedback if file is missing

        blocks_valid = True
        blocks = []
        all_vars_logged = []

        if not contents:
            print("No logging blocks")
            blocks_valid = False

        for key in contents:
            name = key

            try:
                period = int(contents[key]['update_frequency_ms'])

                if period <= 20:
                    error_text = "update_frequency_ms must be greater than 20"
                    self.error_label.setText(
                        "<span style='color: red;'>" + error_text + "</span>")
                    blocks_valid = False
                    break
                elif period >= 500:
                    error_text = "update_frequency_ms must be less than 500"
                    self.error_label.setText(
                        "<span style='color: red;'>" + error_text + "</span>")
                    blocks_valid = False
                    break
            except ValueError:
                error_text = "update_frequency_ms must be an int"
                self.error_label.setText("<span style='color: red;'>" +
                                         error_text + "</span>")
                blocks_valid = False
                break
            except KeyError:
                error_text = "need an update_frequency_ms key"
                self.error_label.setText(
                    "<span style='color: red;'>" + error_text + "</span>")
                blocks_valid = False
                break

            bad_var = ""
            try:
                variables = contents[key]['vars']

                if len(variables) > 5:
                    error_text = "logging block " + key + " longer than 5 " \
                                                          "variables"
                    self.error_label.setText(
                        "<span style='color: red;'>" + error_text + "</span>")

                    blocks_valid = False
                    break

                # This is where good variables are actually handled, but will
                # raise errors if they are bad.
                for var in variables:
                    if var not in self.items_text:
                        bad_var = var
                        raise ValueError
                    if var not in all_vars_logged:
                        all_vars_logged.append(var)
                    else:
                        bad_var = var
                        raise AssertionError
            except KeyError:
                error_text = "need a vars key"
                self.error_label.setText(
                    "<span style='color: red;'>" + error_text + "</span>")

                blocks_valid = False
                break
            except ValueError:
                error_text = "logging variable " + str(bad_var) + " not found"
                self.error_label.setText(
                    "<span style='color: red;'>" + error_text + "</span>")
                blocks_valid = False
                break
            except AssertionError:
                error_text = "logging variable " + str(bad_var) + \
                             " found repeated"
                self.error_label.setText(
                    "<span style='color: red;'>" + error_text + "</span>")
                blocks_valid = False
                break
            # todo add error for if there are over 4 logging groups

            # current logging blocks
            blocks.append({'name': name, 'period': period, 'vars': variables})

        if blocks_valid:
            self.error_label.setText("")
            variables = []
            for block in blocks:
                # add logging block to crazyflie proto connection
                self.cf.add_log_config(block['name'], block['period'],
                                       block['vars'])
                variables.append(block['vars'])
            # call function for menu update on controls page
            self.logging_selection_menu_update_function(variables)

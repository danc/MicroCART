from PyQt5.QtWidgets import QPushButton, QGridLayout, QWidget, QComboBox, \
    QLabel, QLineEdit, QProgressBar
from PyQt5.QtCore import QTimer
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyLine import QHSeparationLine
import os
import json
from queue import Queue
import queue
from CrazyflieProtoConnection import CrazyflieProtoConnection


class ParameterTab(QWidget):
    """
    The parameter tab is a page that handles sending or getting parameters
    from the drone.
    """

    def __init__(self, cf: CrazyflieProtoConnection):
        super().__init__()

        layout = QGridLayout()
        self.setLayout(layout)
        self.cf = cf  # Crazyflie proto connection

        self.sending = False
        self.sending_queue = Queue()
        self.num_to_send = 0
        self.num_sent = 0

        self.toc = {}  # Parameter table of contents
        self.timer = QTimer()

        # Add white space around the parameter menus.
        horizontal_spacer = QtWidgets.QSpacerItem(
            1, 1, QtWidgets.QSizePolicy.Expanding,
            QtWidgets.QSizePolicy.Minimum)
        layout.addItem(horizontal_spacer, 1, 2, Qt.AlignRight)

        horizontal_spacer2 = QtWidgets.QSpacerItem(
            1, 1, QtWidgets.QSizePolicy.Expanding,
            QtWidgets.QSizePolicy.Minimum)
        layout.addItem(horizontal_spacer2, 1, 0, Qt.AlignRight)

        horizontal_spacer3 = QtWidgets.QSpacerItem(
            1, 1, QtWidgets.QSizePolicy.Minimum,
            QtWidgets.QSizePolicy.Expanding)
        layout.addItem(horizontal_spacer3, 0, 1, Qt.AlignTop)

        # --------------------- Get Param --------------------------------------
        get_paraml = QLabel("Get Parameter")
        layout.addWidget(get_paraml, 1, 1)

        get_param_widget = QWidget()
        get_param_widget.setMinimumHeight(100)
        get_param_widget.setMinimumHeight(100)
        layout.addWidget(get_param_widget, 2, 1)

        get_param_layout = QGridLayout()
        get_param_widget.setLayout(get_param_layout)

        get_param_group = QLabel("Group")
        get_param_layout.addWidget(get_param_group, 1, 1)

        self.get_param_group_cbox = QComboBox()
        self.get_param_group_cbox.currentTextChanged.connect(
            self.on_get_param_group_changed)
        get_param_layout.addWidget(self.get_param_group_cbox, 1, 2)

        get_param = QLabel("Entry")
        get_param_layout.addWidget(get_param, 2, 1)

        self.get_param_cbox = QComboBox()
        self.get_param_cbox.setMinimumWidth(150)
        self.get_param_cbox.setMaximumWidth(150)
        get_param_layout.addWidget(self.get_param_cbox, 2, 2)

        value_label = QLabel("Value")
        get_param_layout.addWidget(value_label, 3, 1)

        self.value_value_label = QLabel()
        self.value_value_label.setMaximumWidth(150)
        get_param_layout.addWidget(self.value_value_label, 3, 2)

        self.get_param_button = QPushButton("Get Param")
        self.get_param_button.clicked.connect(self.get_param)
        layout.addWidget(self.get_param_button, 3, 1, alignment=Qt.AlignHCenter)
        self.get_param_button.setMinimumWidth(150)
        self.get_param_button.setMaximumWidth(150)

        # ---------------- Line ------------------------------------------------
        line1 = QHSeparationLine()
        line2 = QHSeparationLine()
        line3 = QHSeparationLine()
        layout.addWidget(line1, 4, 1)
        layout.addWidget(line2, 4, 2)
        layout.addWidget(line3, 4, 0)

        # --------------------- Set Param --------------------------------------
        set_paraml = QLabel("Set Parameter")
        layout.addWidget(set_paraml, 6, 1)

        set_param_widget = QWidget()
        set_param_widget.setMinimumHeight(100)
        set_param_widget.setMinimumHeight(100)
        layout.addWidget(set_param_widget, 7, 1)

        set_param_layout = QGridLayout()
        set_param_widget.setLayout(set_param_layout)

        set_param_group = QLabel("Group")
        set_param_layout.addWidget(set_param_group, 1, 1)

        self.set_param_group_cbox = QComboBox()
        self.set_param_group_cbox.currentTextChanged.connect(
            self.on_set_param_group_changed)
        set_param_layout.addWidget(self.set_param_group_cbox, 1, 2)

        set_param = QLabel("Entry")
        set_param_layout.addWidget(set_param, 2, 1)

        self.set_param_cbox = QComboBox()
        set_param_layout.addWidget(self.set_param_cbox, 2, 2)

        self.set_value_label = QLabel("Value")
        set_param_layout.addWidget(self.set_value_label, 3, 1)

        self.set_param_value = QLineEdit()
        self.set_param_value.setMaximumWidth(150)
        set_param_layout.addWidget(self.set_param_value, 3, 2)

        self.set_param_button = QPushButton("Set Param")
        self.set_param_button.setMinimumWidth(150)
        self.set_param_button.setMaximumWidth(150)
        self.set_param_button.clicked.connect(self.set_param)
        layout.addWidget(self.set_param_button, 8, 1, alignment=Qt.AlignHCenter)

        # -------------------- File interaction --------------------------------

        file_interaction_widget = QWidget()
        file_interaction_layout = QGridLayout()
        file_interaction_widget.setLayout(file_interaction_layout)
        layout.addWidget(file_interaction_widget, 9, 1)

        self.set_from_file_button = QPushButton("Set Params from Json file")
        self.set_from_file_button.setMinimumWidth(200)
        self.set_from_file_button.setMaximumWidth(200)
        file_interaction_layout.addWidget(self.set_from_file_button, 1, 1)
        self.set_from_file_button.clicked.connect(self.on_send)

        self.edit_file_button = QPushButton("Edit")
        self.edit_file_button.setMinimumWidth(20)
        self.edit_file_button.setMaximumWidth(40)
        file_interaction_layout.addWidget(self.edit_file_button, 1, 2)
        self.edit_file_button.clicked.connect(self.on_edit)

        file_note = QLabel("Note: This may take a few seconds")
        layout.addWidget(file_note, 10, 1, alignment=Qt.AlignHCenter)

        self.complete_label = QLabel("Complete ✓")
        layout.addWidget(self.complete_label, 11, 1, alignment=Qt.AlignHCenter)

        self.progress_bar = QProgressBar()
        self.progress_bar.setMinimumWidth(100)
        self.progress_bar.setMaximumWidth(200)
        layout.addWidget(self.progress_bar, 12, 1, alignment=Qt.AlignHCenter)

    def on_connect(self):
        """ Whenever connecting to the crazyflie, grab the parameter table of
        contents. This is how we actually get what parameters and groups are
        available. """
        self.toc = self.cf.get_param_toc()
        self.populate_group_menu_options()

    def on_disconnect(self):
        self.toc = {}
        self.populate_group_menu_options()

    def populate_group_menu_options(self):
        """ Remove any current entries from parameter boxes and add the
        options for the parameter groups. """

        self.set_param_group_cbox.clear()
        self.get_param_group_cbox.clear()

        self.set_param_group_cbox.addItems(self.toc.keys())
        self.get_param_group_cbox.addItems(self.toc.keys())

    def on_get_param_group_changed(self):
        """ Whenever a parameter group is selected, populate the options for
        the specific entries available in each group. """

        self.get_param_cbox.clear()
        group = self.get_param_group_cbox.currentText()
        if group != "":  # nothing there when cf is disconnected
            self.get_param_cbox.addItems(self.toc[group])

    def on_set_param_group_changed(self):
        """ Whenever a parameter group is selected, populate the options for
        the specific entries available in each group. """

        self.set_param_cbox.clear()
        group = self.set_param_group_cbox.currentText()
        if group != "":  # nothing there when cf is disconnected
            self.set_param_cbox.addItems(self.toc[group])

    def get_param(self):
        """ Retrieve parameter value from crazyflie proto connection. All
        parameter values are always available from it, so this is actually
        just retrieving data from the object not from the physical crazyflie
        itself. """

        group = self.get_param_group_cbox.currentText()
        entry = self.get_param_cbox.currentText()
        value = self.cf.param_get_value(group, entry)
        self.value_value_label.setText(value)

    def set_param(self):
        """ Set parameter value. If the value isn't a number offer the user
        feedback that it is an unacceptable value. Otherwise, set the
        parameter utilizing the crazyflie proto connection. """

        group = self.set_param_group_cbox.currentText()
        entry = self.set_param_cbox.currentText()
        value = self.set_param_value.text()

        try:
            value = float(value)
            self.cf.param_set_value(group, entry, value)

            if self.set_value_label.text() != "Value":
                self.set_value_label.setText("Value")

        except ValueError:
            error_text = "Unaccepted value"
            self.set_value_label.setText(
                "<span style='color: red;'>" + error_text + "</span>")

    @staticmethod
    def on_edit(_self):
        """ When editing the mp4params file, launch it in notepad or default
        text editor. """
        os.startfile('mp4params.json')

    def on_send(self):
        """ Send mp4params file to CrazyflieProtoConnection. """

        # Don't send if you are already sending
        if not self.sending:

            try:
                with open('./mp4params.json', 'r') as f:
                    contents = json.load(f)

                self.sending = True
                self.complete_label.setEnabled(False)
                self.complete_label.setText("Complete ✓")

                # add all the parameters in the file to a sending queue
                for key in contents:
                    for sub_key in contents[key]:
                        self.num_to_send += 1

                        self.sending_queue.put(
                            {'key': key, 'sub_key': sub_key,
                             'value': contents[key][sub_key]})

                # every 300 ms, send the next parameter. Uses a timer to
                # retain reactivity of UI. Uses 300ms because it was more
                # stable than 100ms. Relatively often at 100ms parameters
                # would not get set correctly
                self.timer.timeout.connect(self.send_callback)
                self.timer.start(300)
            except json.decoder.JSONDecodeError:
                error_text = "Malformed Json"
                # If your json loading fails for some reason this is the
                # error to be provided to the user.
                self.complete_label.setText(
                    "<span style='color: red;'>" + error_text + "</span>")
            # todo add error for if file isn't present

    def send_callback(self):
        """ This function get called whenever a parameter is ready to be
        sent from the file, and once all parameters are set it will
        disconnect itself from being called. Also updates the progress bar. """
        try:
            vals = self.sending_queue.get(block=False)
            key = vals['key']
            sub_key = vals['sub_key']
            value = vals['value']
            self.cf.param_set_value(key, sub_key, value)
            self.num_sent += 1
            self.progress_bar.setValue(
                int((self.num_sent / self.num_to_send) * 100))

        except queue.Empty:
            # stop sending once the queue is empty
            self.sending = False
            self.timer.timeout.disconnect(self.send_callback)
            self.timer.stop()
            self.complete_label.setEnabled(True)
            self.num_to_send = 0
            self.num_sent = 0
            self.progress_bar.setValue(int(0))

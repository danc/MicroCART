"""
Crazyflie Proto Connection handles the actual interaction with the crazyflie.
The only reason it exists is to serve as an intermediary between the frontend
and the crazyflie itself so that it can handle all interactions with the
cflib library and that the GUI doesn't have to.

If one wanted to slot this GUI into the existing microcart infrastructure,
CrazyflieProtoConnection could be rewritten to interface to frontend
commands, and to connect to the backend, and the crazyflie adapter,
and crazyflie groundstation.

"""


from time import time
from typing import List
import time

import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from queue import Queue

from cflib.crazyflie.log import LogConfig


class CrazyflieProtoConnection:
    """
    Handles all interactions with cflib.
    """

    def __init__(self):
        """
        Initialize the start time, the logging queue which is given to the
        plotting window, and set the synchronous crazyflie connection by
        default to None. This should be set to the crazyflie when connecting
        to one.
        """

        # Get the start time, so that the timestamp returned to the user will
        # be in seconds since startup.
        self.start_time = time.time()

        self.logging_queue = Queue()

        self.scf = None
        self.is_connected = False
        self.param_callback_count = 0
        self.logging_configs = []

        # self.timer = QTimer()
        # self.timer.timeout.connect(self.update_plot)
        # self.timer.start(50)

    def check_link_is_alive_callback(self):
        """ Periodically investigate if the link is alive """
        if self.scf:
            if not self.scf.is_link_open():
                self.is_connected = False

    def logging_callback(self, _timestamp, data, _logconf):
        """ Whenever data comes in from the logging, it is sent here,
        which routes it into our specific format for the logging queue. """

        timestamp1 = time.time() - self.start_time

        for key in data.keys():
            value_pair = {'timestamp': timestamp1, 'data': data[key],
                          'signal': key}
            print(value_pair)
            self.logging_queue.put(value_pair)

    def param_set_value(self, group: str, name: str, value: float):
        """ Set a crazyflie parameter value. """

        try:
            if self.scf.is_link_open():
                full_name = group + "." + name
                cf = self.scf.cf
                cf.param.add_update_callback(group=group, name=name,
                                             cb=self.done_setting_param_value)

                cf.param.set_value(full_name, value)

                # Don't return until the parameter is done getting set
                while self.param_callback_count < 1:
                    time.sleep(0.01)

                self.param_callback_count = 0
        except AttributeError:
            print("Nothing connected")

    def done_setting_param_value(self, *_args):
        """ Callback when done setting a parameter value. """
        print("Done setting param")
        self.param_callback_count += 1

    def param_get_value(self, group: str, name: str):
        """ Retrieve parameter value from crazyflie toc. """
        try:
            if self.scf.is_link_open():
                return self.scf.cf.param.values[group][name]

        except AttributeError:
            pass
        return -1.234567890  # 1234567890 should be pretty obvious that
        # something has gone wrong.

    def get_logging_toc(self):
        """ Retrieve entire logging table of contents. Used in order to
        display list in logging tab. """

        try:
            if self.scf.is_link_open():
                tocFull = self.scf.cf.log.toc.toc
                toc = []
                for key in tocFull.keys():
                    for inner_key in tocFull[key].keys():
                        # concatenate group name with parameter name.
                        full_name = key + "." + inner_key
                        toc.append(full_name)

                return toc
            else:
                return []
        except AttributeError:
            pass
        return []

    def get_param_toc(self):
        """ Get the names of all groups available for parameters on the
        crazyflie. Used to populate parameter group list on parameter tab. """

        try:
            if self.scf.is_link_open():
                toc = self.scf.cf.param.values

                return toc
        except AttributeError:
            pass
        return {}

    def add_log_config(self, name: str, period: int, variables: List[str]):
        """ Add a logging config. Used from logging tab when refreshing
        logging variables. Add callback to route logged data to logging
        queue. """

        print("Name: " + name + ", period: " + str(period) + ", variables: "
              + str(variables))
        logging_group = LogConfig(name=name, period_in_ms=period)

        for variable in variables:
            logging_group.add_variable(variable, 'float')

        self.logging_configs.append(logging_group)
        self.logging_configs[-1].data_received_cb.add_callback(
            self.logging_callback)
        self.scf.cf.log.add_config(self.logging_configs[-1])

    def clear_logging_configs(self):
        """ Stop logging and clear configuration. Used when refreshing
        logging to stop anything that has configured to be logged from
        logging. """

        self.stop_logging()
        self.logging_configs = []
        # done refreshing toc is a callback function that is triggered when
        # refresh toc is done executing.
        self.scf.cf.log.refresh_toc(
            self.done_refreshing_toc, self.scf.cf.log._toc_cache)
        # Blocks until toc is done refreshing.
        while self.param_callback_count < 1:
            time.sleep(0.01)
        self.param_callback_count = 0

        # grabs new toc values
        self.scf.wait_for_params()

    def done_refreshing_toc(self, *_args):
        """ Callback for flow control, increments param callback count to
        allow exit of while loop. """
        self.param_callback_count = 1

    def start_logging(self):
        """ Begins logging all configured logging blocks. This is used from
        the controls tab when hitting begin logging. """
        for i in range(0, len(self.logging_configs)):
            self.logging_configs[i].start()

    def stop_logging(self):
        """ Stops logging all configured logging blocks. This is used from
        the controls tab when hitting pause logging. """
        for i in range(0, len(self.logging_configs)):
            self.logging_configs[i].stop()

    def connect(self, uri: str):
        """
        Handles connecting to a crazyflie. Bitcraze has excellent
        documentation on how to use the synchronous crazyflie object in order
        to send setpoints, set parameters or retrieve logging.
        :param uri: Radio channel
        """
        cflib.crtp.init_drivers()
        self.scf = SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache'))
        self.scf.open_link()
        self.scf.wait_for_params()
        self.is_connected = True

    def disconnect(self):
        """ Disconnect from crazyflie. """
        print("Disconnect quad")
        if self.is_connected:
            self.scf.close_link()
            self.scf = None
            self.is_connected = False

    @staticmethod
    def list_available_crazyflies():
        """ Lists crazyflies that are on and within range. """
        cflib.crtp.init_drivers()  # run this again just in case you plug the
        # dongle in
        return cflib.crtp.scan_interfaces()

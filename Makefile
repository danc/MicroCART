all:
	cd ./crazyflie_groundstation && $(MAKE)
	cd ./groundStation && $(MAKE)
	cd ./groundStation/adapters/crazyflie && $(MAKE) clean && $(MAKE)
	cd ./groundStation/gui/MicroCART && $(MAKE)

clean:
	cd ./crazyflie_groundstation && $(MAKE) clean
	cd ./groundStation && $(MAKE) clean
	cd ./groundStation/adapters/crazyflie && $(MAKE) clean
	cd ./groundStation/gui/MicroCART && $(MAKE) clean

cf_gs:
	cd ./crazyflie_groundstation && $(MAKE)

mc_gs:
	cd ./groundStation && $(MAKE)

adap:
	cd ./groundStation/adapters/crazyflie && $(MAKE)

gui:
	cd ./groundStation/gui/MicroCART && $(MAKE)

clean_cf_gs:
	cd ./crazyflie_groundstation && $(MAKE) clean

clean_mc_gs:
	cd ./groundStation && $(MAKE) clean

clean_adap:
	cd ./groundStation/adapters/crazyflie && $(MAKE) clean

clean_gui:
	cd ./groundStation/gui/MicroCART && $(MAKE) clean

#ifndef SETPOINT_H
#define SETPOINT_H


class Setpoint
{
public:
    float roll = 0;
    float pitch = 0;
    float yaw = 0;
    float throttle = 0;
    Setpoint();
};

#endif // SETPOINT_H

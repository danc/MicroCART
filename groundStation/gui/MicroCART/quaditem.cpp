#include "quaditem.h"
#include <QPixmap>
#include <QLabel>
#include <QtMath>

QuadItem::QuadItem()
{
   setFlag(ItemIsMovable);
}

QRectF QuadItem::boundingRect() const
{
   return QRectF(0,0,50,50);
}

void QuadItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
   QRectF rec = boundingRect();
   painter->setRenderHint(QPainter::Antialiasing);
   painter->drawImage(rec,QImage(":/images/quad/quad.png"));
}

void QuadItem::updatePos(float longitudinal, float lateral, float height, float pitch, float roll, float yaw)
{
      QPointF center = boundingRect().center();
      this->setTransform(QTransform().translate(center.x(), center.y()).rotate(qRadiansToDegrees(yaw)).translate(-center.x(), -center.y()));
      this->setPos(lateral*100, -longitudinal*100); // These are flipped due to the camera system's +x and +y.
//    QTransform trans = this->transform();
//    trans.translate(center.x(), center.y());
//    trans.translate(-center.x(), -center.y());
//    this->setTransform(trans);
    update();
}

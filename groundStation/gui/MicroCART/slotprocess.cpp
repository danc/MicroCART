#include "slotprocess.h"
#include <QDebug>

void SlotProcess::startProcess()
{
    qInfo() << "Started process " + program();
    setProcessChannelMode(QProcess::ForwardedChannels);
    connect(this, SIGNAL (finished(int, QProcess::ExitStatus)), this, SLOT (notifyFinished(int, QProcess::ExitStatus)));
    connect(this, SIGNAL (errorOccurred(QProcess::ProcessError)), this, SLOT (notifyError(QProcess::ProcessError)));
    start();
}

void SlotProcess::notifyFinished(int status, QProcess::ExitStatus exitStatus)
{
    qInfo() << "Finished process " + program();
    if (exitStatus != QProcess::NormalExit) {
        qInfo() << "Did not exit normally!" + QString::number(status);
    }
}

void SlotProcess::notifyError(QProcess::ProcessError error)
{
    qInfo() << "Error with process " + program() + " " + errorString();
}

SlotProcess::SlotProcess(QObject * parent) :
    QProcess(parent)
{

}

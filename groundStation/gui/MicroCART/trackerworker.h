#ifndef TRACKERWORKER_H
#define TRACKERWORKER_H

#include <QObject>

#include "frontend_common.h"
#include "frontend_tracker.h"

class TrackerWorker : public QObject
{
    Q_OBJECT
public:
    TrackerWorker();
    ~TrackerWorker();

signals:
    void finished(float, float, float, float, float, float);
    void connected();
    void disconnected();

public slots:
    void process();
    void connectBackend();
    void disconnectBackend();

private:
    struct backend_conn * conn;

};
#endif // TRACKERWORKER_H

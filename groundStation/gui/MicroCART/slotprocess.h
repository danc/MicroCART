#ifndef SLOTPROCESS_H
#define SLOTPROCESS_H

#include <QObject>
#include <QProcess>

class SlotProcess : public QProcess
{
    Q_OBJECT
public:
   SlotProcess(QObject *);

public slots:
    void startProcess();
    void notifyFinished(int status, QProcess::ExitStatus);
    void notifyError(QProcess::ProcessError error);
};

#endif // SLOTPROCESS_H

#include "logworker.h"
#include <iostream>
#include <unistd.h>
#include <sys/stat.h>

LogWorker::LogWorker(QObject *parent) : QObject(parent), conn(NULL)
{
}

LogWorker::~LogWorker()
{
    disconnectBackend();
}

/**
 * @brief CrazyflieWorker::connectBackend
 * Command the worker to connect to the backend
 * will set the private class struct, conn, once done.
 * The connected() signal is emitted once complete.
 */
void LogWorker::connectBackend()
{
    if (conn == NULL) {
        conn = ucart_backendConnect();
        emit (connected());
    } else {
        qInfo() << "Attempted to connect crazyflieworker when already connected!";
    }
}

/**
 * @brief CrazyflieWorker::disconnectBackend
 * Command the worker to disconnect from the backend,
 * sets the conn struct to null and emits the
 * disconnected() signal once complete
 */
void LogWorker::disconnectBackend()
{
    if (conn) {
         ucart_backendDisconnect(conn);
         conn = NULL;
         emit (disconnected());
    }
}

void LogWorker::openLogFile(QString filename){
    logFileName = filename.toStdString();
}


void LogWorker::startLogging() {

    keepLogging = true;

    logFile.open(logFileName.c_str(), std::fstream::ate);

    std::string line;
    std::vector<std::string> tokens;
    std::string temp;
    std::stringstream stream;

    QVector<QVector<double>> x;
    QVector<QVector<double>> y;
    for(int i=0; i<5; i++){
        x.append(QVector<double>());
        y.append(QVector<double>());
    }

    int valCnt = 0;
    uint8_t nanCnt = 0;

    while(keepLogging) {
        while(std::getline(logFile, line) && keepLogging) {

            if(line.at(0) == '#') {
                continue;
            }
            stream.str(line);
            while(getline(stream, temp, '\t')) {
                tokens.push_back(temp);
            }

            for(int i = 0; i < 5; i++) {
                if(logColumns.at(i) == -1) {
                    //do not want this column value
                }
                else {
                    //do want this column value
                    if(logColumns.at(i) >= tokens.size()){
                        emit(logBlockOutOfSync());
                        stopLogging();
                        return;
                    }
                    if(!(tokens.at(logColumns.at(i)).compare("nan"))) {
                        //nan, dont plot
                        nanCnt++;
                    }else{
                        //contains value that we want to plot
                        double xVal = std::stod(tokens.at(0));
                        double yVal = std::stod(tokens.at(logColumns.at(i)));

                        //check if y value (variable val) has changed since last value, if so plot it
                        //also check if last plot time was a while ago
                        if(y[i].empty() || x[i].empty()){
                            y[i].append(yVal);
                            //also capture time x
                            x[i].append(xVal);
                            valCnt++;
                        }else if(y[i].last() != yVal || xVal - x[i].last() > 0.2 ){
                            y[i].append(yVal);
                            //also capture time x
                            x[i].append(xVal);
                            valCnt++;
                        }
                    }
                }
            }
            if(nanCnt >= 2) {
                nanCnt = 0;
                tokens.clear();
                stream.str("");
                stream.clear();
                continue;
            }
            tokens.clear();
            stream.str("");
            stream.clear();
            nanCnt = 0;

            //if new values to plot, send to main window
            if(valCnt > 2) {

                emit(gotLogData(x, y, false));

                //clear old values
                for(int i=0; i<5; i++){
                    x[i].clear();
                    y[i].clear();
                }
                valCnt = 0;
            }

        }
        if(!logFile.eof()) {
            break;
        }
        logFile.clear();
        usleep(150000);
    }

    logFile.close();
}

void LogWorker::stopLogging(){
    this->keepLogging = false;
    logFile.clear();
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QStandardItemModel>
#include <QGraphicsScene>
#include "quaditem.h"
#include "gamepadmonitor.h"
#include "crazyflieworker.h"
#include "logworker.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void connectWorkers();
    void disconnectWorkers();
    void getParamValue(QString node, QString param);
    void getParamValue(QString key);
    void getNodeOutput(QString node);
    void setParamValue(QString node, QString param, float value);
    void setParamValue(QString key, double value);
    void getPosAttFromBackend();
    void rateSetpointSignal(float roll, float pitch, float yaw, float throttle);
    void angleSetpointSignal(float roll, float pitch, float yaw, float throttle);
    void mixedSetpointSignal(float roll, float pitch, float yaw, float throttle);
    void getGroupEntries(int box, QString gName);
    void triggerAttSetpointSend();
    void triggerAttRateSetpointSend();
    void triggerMixedAttSetpointSend();
    void getLogValues(QString log1, QString log2, QString log3, QString log4, QString log5);
    void sendLogBlockCommand(int8_t command, int8_t id);
    void requestLogBlockID(QString name, int8_t command);
    void beginReadingLogFile(QString name);
    void sendStartLogging();
    void sendStopLogging();

private slots:
    void on_pbStart_clicked();

    void on_pbConnect_clicked();

    void on_pbStop_clicked();

    void on_chooseBackend_clicked();

    void newGroupEntries(int box, QStringList entries);

    void on_pb_getParam_click();

    void on_pb_setParam_click();

    void on_pb_setParamJSON_click();

    void openParamsJsonFile();

    void updateJSONParams();

    void backendStarted();

    void backendFinished(int exitCode, QProcess::ExitStatus exitStatus);

    void backendError(QProcess::ProcessError error);

    void backendRead();

    void workerConnected();

    void workerDisconnected();

    void on_applySetpointButton_clicked();

    void on_stopSetpointButton_clicked();

    void setParamGroups(QStringList groupNames);

    void getGroupBoxChange(int index);

    void getGroupBox2Change(int index);

    void setLoggingVariableBox(QStringList variableNames);

    void displayParamValue(QString param, float value);

    void getEntryBoxChange(int index);

    void getEntryBoxChange2(int index);

    void onGamepadDisconnect();

    void onGamepadConnect();

    void on_pb_configRoll_clicked();

    void updateGamepad();

    void updateGraph();

    void on_pb_resetConfig_clicked();

    void on_pb_configYaw_clicked();

    void on_pb_configThrust_clicked();

    void on_pb_configPitch_clicked();

    void on_tActual_sliderMoved(int position);

    void on_rbGamepadControl_toggled(bool checked);

    void on_rbManualSetpoint_toggled(bool checked);

    void trigger_send_setpoint();

    void on_gamepadYawScale_valueChanged(int arg1);

    void on_gamepadThrustScale_valueChanged(int arg1);

    void on_gamepadRollScale_valueChanged(int arg1);

    void on_gamepadPitchScale_valueChanged(int arg1);

    void graphLogs(QVector<QVector<double>> x, QVector<QVector<double>> y, bool lastSet);

    void openLogBlockFile();

    void gotLogBlockNames(QStringList logBlockNames);

    void on_pb_lbRefresh();

    void on_pb_lbStop();

    void on_pb_lbResume();

    void on_pb_lbPause();

    void on_pb_lbDelete();

    void useLogBlockID(int id, int8_t command);

    void setLogButtons(bool isEnabled);

    void fillLogVariablesBox(QStringList activeVars);

    void getLogEntryBoxChange1(QString text);

    void getLogEntryBoxChange2(QString text);

    void getLogEntryBoxChange3(QString text);

    void getLogEntryBoxChange4(QString text);

    void getLogEntryBoxChange5(QString text);

    void receiveLogFile(QString filename);

    //void on_pb_stopLog_clicked();
    //void updateProgressBar();

    void on_pb_startLog_clicked();
    void on_pb_stopLog_clicked();
    void logBlockOutOfSyncPopup();
    void on_pb_disconnectTS();
    void on_pb_connectTS();

    void on_cb_autoscale_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    float sp_roll;
    float sp_pitch;
    float sp_yaw;
    float sp_thrust;
    QString logVar1, logVar2, logVar3, logVar4, logVar5;
    QTimer * workerStartTimer;
    QTimer * crazyflieTimer;
    QTimer *pollGamepadTimer;
    QTimer *logTimer;
    QProcess * backendProcess;
    int connectedWorkers;
    GamepadMonitor *gamepadMonitor;
    CrazyflieWorker *crazyflieWorker;
    LogWorker *logWorker;
    void setTestStandButtons();
public:
    bool firstSet = true;
};

#endif // MAINWINDOW_H

#include "trackerworker.h"
#include <QThread>
#include "frontend_common.h"
#include "frontend_tracker.h"
#include "quaditem.h"
#include <QDebug>

TrackerWorker::TrackerWorker() :
    QObject(), conn(NULL)
{
}

TrackerWorker::~TrackerWorker()
{
    disconnectBackend();
}
/**
 * @brief TrackerWorker::connectBackend
 * Command the worker to connect to the backend
 * will set the private class struct, conn, once done.
 * The connected() signal is emitted once complete.
 */
void TrackerWorker::connectBackend()
{
    if (conn == NULL) {
        conn = ucart_backendConnect();
        emit (connected());
    } else {
        qInfo() << "Attempted to connect trackerworker when already connected!";
    }
}

/**
 * @brief TrackerWorker::disconnectBackend
 * Command the worker to disconnect from the backend,
 * sets the conn struct to null and emits the
 * disconnected() signal once complete
 */
void TrackerWorker::disconnectBackend()
{
    if (conn) {
         ucart_backendDisconnect(conn);
         conn = NULL;
         emit (disconnected());
    }
}

/**
 * @brief TrackerWorker::process
 * Recieve tracking data from the backend from the VRPN system
 */
void TrackerWorker::process()
{
    if (conn) {
        struct frontend_tracker_data td;
        frontend_track(conn, &td);

        emit finished(td.longitudinal, td.lateral, td.height, td.pitch, td.roll, td.yaw);
    }
}

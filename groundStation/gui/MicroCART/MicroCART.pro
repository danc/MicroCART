#-------------------------------------------------
#
# Project created by QtCreator 2017-02-23T15:03:45
#
#-------------------------------------------------

DEFINES += QCUSTOMPLOT_USE_OPENGL


QT       += core gui gamepad

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += debug


TARGET = MicroCART
TEMPLATE = app

LIBS += ../../frontend.a
INCLUDEPATH += ../../src/frontend/

SOURCES += main.cpp\
        mainwindow.cpp \
    trackerworker.cpp \
    controlworker.cpp \
    quaditem.cpp \
    slotprocess.cpp \
    qFlightInstruments.cpp\
    qcustomplot.cpp\
    crazyflieworker.cpp\
    setpoint.cpp\
    gamepadmonitor.cpp\
    logworker.cpp

HEADERS  += mainwindow.h \
    trackerworker.h \
    controlworker.h \
    quaditem.h \
    slotprocess.h \
	graph_blocks.h \
    qFlightInstruments.h\
    qcustomplot.h\
    crazyflieworker.h\
    logworker.h\
    setpoint.h\
    gamepadmonitor.h

FORMS    += mainwindow.ui

INCLUDEPATH += $$PWD/../../../quad/inc
DEPENDPATH += $$PWD/../../../quad/inc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../quad/lib/release/ -lgraph_blocks
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../quad/lib/debug/ -lgraph_blocks
else:unix: LIBS += -L$$PWD/../../../quad/lib/ -lgraph_blocks

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/release/libgraph_blocks.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/debug/libgraph_blocks.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/release/graph_blocks.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/debug/graph_blocks.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../quad/lib/libgraph_blocks.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../quad/lib/release/ -lcomputation_graph
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../quad/lib/debug/ -lcomputation_graph
else:unix: LIBS += -L$$PWD/../../../quad/lib/ -lcomputation_graph

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/release/libcomputation_graph.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/debug/libcomputation_graph.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/release/computation_graph.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../quad/lib/debug/computation_graph.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../quad/lib/libcomputation_graph.a

RESOURCES += \
    resources.qrc

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <bits/stdc++.h>
#include <math.h>

#include <QFileDialog>
#include <QDesktopServices>
#include <QMessageBox>
#include <QThread>
#include <QTimer>
#include <QRegExp>
#include <QProcessEnvironment>
#include <QPixmap>
#include <QProcess>
#include <QDebug>
#include <algorithm>
#include <QVector>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <unistd.h>

//Debugging
#include <fstream>
using namespace std;
ofstream dbugfile;

#include "controlworker.h"
#include "crazyflieworker.h"
#include "quaditem.h"
#include "slotprocess.h"
#include "qFlightInstruments.h"
#include "graph_blocks.h"

#include <iostream>

#define DEBUG 0

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    sp_roll(0.0f),
    sp_pitch(0.0f),
    sp_yaw(0.0f),
    sp_thrust(0.0f),
    workerStartTimer(new QTimer(this)),
    crazyflieTimer(new QTimer(this)),
    backendProcess(new QProcess(this)),
    pollGamepadTimer(new QTimer(this)),
    logTimer(new QTimer(this)),
    connectedWorkers(0)
{

    ui->setupUi(this);
    qRegisterMetaType<int8_t>("int8_t");
    qRegisterMetaType<QVector<QVector<double>>>("QVector<QVector<double>>");

    QGraphicsScene *posScene = new QGraphicsScene(this);
    
    QuadItem * posIndicator = new QuadItem();
    posScene->addItem(posIndicator);

    QGraphicsScene *attScene = new QGraphicsScene(this);
    
    QADI * attIndicator = new QADI();
    attScene->addWidget(attIndicator);

    if(DEBUG){
        dbugfile.open("debug.txt");
        dbugfile << "starting debug mode\n";
    }

    workerStartTimer->setSingleShot(true);

    backendProcess->setProcessChannelMode(QProcess::MergedChannels);

    /* Create another worker for the control graph */
    QThread * cwThread = new QThread(this);
    ControlWorker * controlWorker = new ControlWorker();
    controlWorker->moveToThread(cwThread);

    /* Connect signals from control worker */
    connect(controlWorker, SIGNAL (paramSet(QString, QString)), controlWorker, SLOT (getParamValue(QString, QString)));

    /* Signals to control worker */
    connect(this, SIGNAL (getParamValue(QString, QString)), controlWorker, SLOT (getParamValue(QString, QString)));
    connect(this, SIGNAL (setParamValue(QString, QString, float)), controlWorker, SLOT (setParamValue(QString, QString, float)));
    connect(this, SIGNAL (getNodeOutput(QString)), controlWorker, SLOT (getNodeOutput(QString)));

    /* Create another work for the crazyflie communication */
    QThread * cfThread = new QThread(this);
    crazyflieWorker = new CrazyflieWorker();
    crazyflieWorker->moveToThread(cfThread);

    QThread * logThread = new QThread(this);
    logWorker = new LogWorker();
    logWorker->moveToThread(logThread);

    /* connect signals for crazyflie worker */
    connect(ui->logEntriesComboBox1, SIGNAL (currentTextChanged(QString)), this, SLOT (getLogEntryBoxChange1(QString)));
    connect(ui->logEntriesComboBox2, SIGNAL (currentTextChanged(QString)), this, SLOT (getLogEntryBoxChange2(QString)));
    connect(ui->logEntriesComboBox3, SIGNAL (currentTextChanged(QString)), this, SLOT (getLogEntryBoxChange3(QString)));
    connect(ui->logEntriesComboBox4, SIGNAL (currentTextChanged(QString)), this, SLOT (getLogEntryBoxChange4(QString)));
    connect(ui->logEntriesComboBox5, SIGNAL (currentTextChanged(QString)), this, SLOT (getLogEntryBoxChange5(QString)));
    connect(ui->pb_logBlockFile, SIGNAL (clicked()), this, SLOT(openLogBlockFile()));
    connect(ui->pb_logBlockRefresh, SIGNAL (clicked()), this, SLOT(on_pb_lbRefresh()));
    connect(ui->pb_logBlockStop, SIGNAL (clicked()), this, SLOT(on_pb_lbStop()));
    connect(ui->pb_logBlockResume, SIGNAL (clicked()), this, SLOT(on_pb_lbResume()));
    connect(ui->pb_logBlockPause, SIGNAL (clicked()), this, SLOT(on_pb_lbPause()));
    connect(ui->comboBox_logBlocks, SIGNAL (currentTextChanged(QString)), crazyflieWorker, SLOT (logBlockBoxChanged(QString)));
    connect(crazyflieWorker, SIGNAL (gotBlockisEnabled(bool)), this, SLOT(setLogButtons(bool)));
    connect(logWorker, SIGNAL (gotLogData(QVector<QVector<double>>, QVector<QVector<double>>, bool)), this, SLOT(graphLogs(QVector<QVector<double>>, QVector<QVector<double>>, bool)));
    //connect(ui->pb_logBlockDelete, SIGNAL (clicked()), this, SLOT(on_pb_lbDelete()));
    connect(this, SIGNAL(sendLogBlockCommand(int8_t, int8_t)), crazyflieWorker, SLOT(frontendLogBlockCommand(int8_t, int8_t)));
    connect(this, SIGNAL (requestLogBlockID(QString, int8_t)), crazyflieWorker, SLOT(findLogBlockID(QString, int8_t)));
    connect(crazyflieWorker, SIGNAL (gotLogBlockID(int, int8_t)), this, SLOT(useLogBlockID(int, int8_t)));
    connect(crazyflieWorker, SIGNAL (changeLogBlock_comboBox(QStringList)), SLOT(gotLogBlockNames(QStringList)));
    connect(crazyflieWorker, SIGNAL (gotActiveLogVariables(QStringList)), this, SLOT(fillLogVariablesBox(QStringList)));
    connect(crazyflieWorker, SIGNAL (gotLogFile(QString)), this, SLOT (receiveLogFile(QString)));
    connect(this, SIGNAL (beginReadingLogFile(QString)), logWorker, SLOT(openLogFile(QString)));
    connect(this, SIGNAL(sendStartLogging()), logWorker, SLOT(startLogging()));
    connect(logWorker, SIGNAL(logBlockOutOfSync()), this, SLOT(logBlockOutOfSyncPopup()));

    connect(this, SIGNAL (rateSetpointSignal(float, float, float, float)), crazyflieWorker, SLOT (setCurrAttRateSetpoint(float, float, float, float)));
    connect(this, SIGNAL (angleSetpointSignal(float, float, float, float)), crazyflieWorker, SLOT (setCurrAttSetpoint(float, float, float, float)));
    connect(this, SIGNAL (mixedSetpointSignal(float, float, float, float)), crazyflieWorker, SLOT (setCurrMixedSetpoint(float, float, float, float)));
    connect(crazyflieTimer, SIGNAL(timeout()), this, SLOT(trigger_send_setpoint()));
    connect(this, SIGNAL(triggerAttSetpointSend()), crazyflieWorker, SLOT(sendAttSetpoint()));
    connect(this, SIGNAL(triggerAttRateSetpointSend()), crazyflieWorker, SLOT(sendAttRateSetpoint()));
    connect(this, SIGNAL(triggerMixedAttSetpointSend()), crazyflieWorker, SLOT(sendMixedAttSetpoint()));
    connect(crazyflieWorker, SIGNAL (gotParamGroups(QStringList)), this, SLOT (setParamGroups(QStringList)));
    connect(crazyflieWorker, SIGNAL (gotLogVariables(QStringList)), this, SLOT (setLoggingVariableBox(QStringList)));

    connect(ui->paramGroupComboBox, SIGNAL (currentIndexChanged(int)), this, SLOT (getGroupBoxChange(int)));
    connect(ui->paramGroupComboBox_2, SIGNAL (currentIndexChanged(int)), this, SLOT (getGroupBox2Change(int)));
    connect(ui->paramEntriesComboBox, SIGNAL (currentIndexChanged(int)), this, SLOT (getEntryBoxChange(int)));
    connect(ui->paramEntriesComboBox_2, SIGNAL (currentIndexChanged(int)), this, SLOT (getEntryBoxChange2(int)));
    connect(ui->pb_getParam, SIGNAL (clicked()), this, SLOT (on_pb_getParam_click()));
    connect(ui->pb_setParam, SIGNAL (clicked()), this, SLOT (on_pb_setParam_click()));
    connect(ui->pb_setParam_JSON, SIGNAL(clicked()), this, SLOT (on_pb_setParamJSON_click()));
    //connect(ui->pb_setParam_JSON, SIGNAL(clicked()), this, SLOT (updateProgressBar()));
    connect(ui->pb_editJSONFile, SIGNAL (clicked()), this, SLOT (openParamsJsonFile()));
    connect(ui->pb_disconnectTS, SIGNAL (clicked()), this, SLOT (on_pb_disconnectTS()));
    connect(ui->pb_connectTS, SIGNAL (clicked()), this, SLOT (on_pb_connectTS()));

    connect(crazyflieWorker, SIGNAL (gotGroupEntries(int, QStringList)), this, SLOT (newGroupEntries(int, QStringList)));
    connect(this, SIGNAL (getGroupEntries(int, QString)), crazyflieWorker, SLOT (getGroupEntries(int, QString)));
    connect(this, SIGNAL (getParamValue(QString)), crazyflieWorker, SLOT (getParamValue(QString)));
    connect(this, SIGNAL (setParamValue(QString, double)), crazyflieWorker, SLOT (setParamValue(QString, double)));
    connect(crazyflieWorker, SIGNAL (gotParamValue(QString, float)), this, SLOT (displayParamValue(QString, float)));

    /* Connect and disconnect from backend when signals emitted */
    connect(workerStartTimer, SIGNAL (timeout()), logWorker, SLOT (connectBackend()));
    connect(workerStartTimer, SIGNAL (timeout()), controlWorker, SLOT (connectBackend()));
    connect(workerStartTimer, SIGNAL (timeout()), crazyflieWorker, SLOT (connectBackend()));

    connect(this, SIGNAL (connectWorkers()), logWorker, SLOT (connectBackend()));
    connect(this, SIGNAL (connectWorkers()), controlWorker, SLOT (connectBackend()));
    connect(this, SIGNAL (connectWorkers()), crazyflieWorker, SLOT (connectBackend()));

    connect(this, SIGNAL (disconnectWorkers()), logWorker, SLOT (disconnectBackend()));
    connect(this, SIGNAL (disconnectWorkers()), controlWorker, SLOT (disconnectBackend()));
    connect(this, SIGNAL (disconnectWorkers()), crazyflieWorker, SLOT (disconnectBackend()));

    connect(backendProcess, SIGNAL (started()), this, SLOT (backendStarted()));
    connect(backendProcess, SIGNAL (errorOccurred(QProcess::ProcessError)), this, SLOT (backendError(QProcess::ProcessError)));
    connect(backendProcess, SIGNAL (finished(int, QProcess::ExitStatus)), this, SLOT (backendFinished(int, QProcess::ExitStatus)));
    connect(backendProcess, SIGNAL (readyReadStandardOutput()), this, SLOT (backendRead()));
    connect(backendProcess, SIGNAL (readyReadStandardError()), this, SLOT (backendRead()));

    connect(controlWorker, SIGNAL (connected()), this, SLOT (workerConnected()));
    connect(controlWorker, SIGNAL (disconnected()), this, SLOT (workerDisconnected()));

    connect(logWorker, SIGNAL (connected()), this, SLOT (workerConnected()));
    connect(logWorker, SIGNAL (disconnected()), this, SLOT (workerDisconnected()));

    connect(crazyflieWorker, SIGNAL (connected()), this, SLOT (workerConnected()));
    connect(crazyflieWorker, SIGNAL (disconnected()), this, SLOT (workerDisconnected()));

    /* Start the things */
    crazyflieTimer->start(300);
    cwThread->start();
    cfThread->start();
    logThread->start();
    emit(connectWorkers());
    ui->progressBar->setMaximum(19);
    ui->progressBar->setMinimum(0);


    /* setup logging ui */
    ui->pb_getParam->setEnabled(false);
    ui->pb_setParam->setEnabled(false);
    ui->pb_logBlockRefresh->setEnabled(false);
    ui->pb_logBlockStop->setEnabled(false);
    ui->pb_logBlockResume->setEnabled(false);
    ui->pb_logBlockPause->setEnabled(false);
    ui->setValueSpin->setMaximum(10000.00);
    ui->setValueSpin->setMinimum(-10000.00);
    ui->setValueSpin->setSingleStep(0.01);
    ui->setValueSpin->setValue(0);
    logVar1 = "Logging Variable 1";
    logVar2 = "Logging Variable 2";
    logVar3 = "Logging Variable 3";
    logVar4 = "Logging Variable 4";
    logVar5 = "Logging Variable 5";

    ui->dataPlot->setOpenGl(true);

    //qInfo() << ui->dataPlot->openGl();

    ui->dataPlot->addGraph();
    ui->dataPlot->addGraph();
    ui->dataPlot->addGraph();
    ui->dataPlot->addGraph();
    ui->dataPlot->addGraph();

    //set colors and dots
    ui->dataPlot->graph(0)->setPen(QPen(Qt::blue, 1));
    ui->dataPlot->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->dataPlot->graph(1)->setPen(QPen(Qt::red, 1));
    ui->dataPlot->graph(1)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->dataPlot->graph(2)->setPen(QPen(Qt::green, 1));
    ui->dataPlot->graph(2)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->dataPlot->graph(3)->setPen(QPen(Qt::black, 1));
    ui->dataPlot->graph(3)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->dataPlot->graph(4)->setPen(QPen(Qt::darkMagenta, 1));
    ui->dataPlot->graph(4)->setScatterStyle(QCPScatterStyle::ssDisc);

    ui->dataPlot->xAxis->setLabel("Time (s)");
    ui->dataPlot->yAxis->setLabel("Value");
    ui->dataPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    ui->logEntriesComboBox1->setStyleSheet("QComboBox { color: blue; }");
    ui->logEntriesComboBox2->setStyleSheet("QComboBox { color: red; }");
    ui->logEntriesComboBox3->setStyleSheet("QComboBox { color: green; }");
    ui->logEntriesComboBox4->setStyleSheet("QComboBox { color: black; }");
    ui->logEntriesComboBox5->setStyleSheet("QComboBox { color: darkMagenta; }");



    /* start gamepad monitor */
    gamepadMonitor = new GamepadMonitor();
    if(gamepadMonitor->isGamepadConnected()){
        onGamepadConnect();
    }else{
        onGamepadDisconnect();
    }

    connect(gamepadMonitor, SIGNAL(gamepadDisconnected()), this, SLOT(onGamepadDisconnect()));
    connect(gamepadMonitor, SIGNAL(gamepadConnected()), this, SLOT(onGamepadConnect()));
    connect(ui->pb_configRoll, SIGNAL(clicked()), this, SLOT(on_pb_configRoll_clicked()));
    connect(pollGamepadTimer, SIGNAL(timeout()), this, SLOT(updateGamepad()));
    connect(logTimer, SIGNAL(timeout()), this, SLOT(updateGraph()));
    pollGamepadTimer->start(50);

    //gamepad configure tab setup
    ui->gamepadRollScale->setValue((int)gamepadMonitor->getRollScale());
    ui->gamepadPitchScale->setValue((int)gamepadMonitor->getPitchScale());
    ui->gamepadYawScale->setValue((int)gamepadMonitor->getYawScale());
    ui->gamepadThrustScale->setValue((int)gamepadMonitor->getThrustScale());

    /* Populate scripts list */
    QDir scriptsDir("scripts/");
    QStringList scripts = scriptsDir.entryList();
    for (int i = 0; i < scripts.size(); i++) {
        QAction * action = ui->menuScripts->addAction(scripts[i]);
        SlotProcess * scriptProcess = new SlotProcess(action);
        scriptProcess->setProgram(scriptsDir.filePath(scripts[i]));
        connect(action, SIGNAL (triggered()), scriptProcess, SLOT (startProcess()));
    }
    this->setWindowTitle("CrazyCART");
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pbStart_clicked()
{
    backendProcess->setProgram(ui->backendPath->text());
    backendProcess->start();
}

void MainWindow::backendStarted()
{
    //ui->pbStart->setEnabled(false);
    ui->pbStop->setEnabled(true);
    workerStartTimer->start(750);
}

void MainWindow::backendError(QProcess::ProcessError error)
{
    ui->vConsole->append(backendProcess->errorString());
}

void MainWindow::backendRead()
{
    ui->vConsole->append(backendProcess->readAll());
}

void MainWindow::workerConnected()
{
    connectedWorkers++;
     if (connectedWorkers == 2) {
        //ui->pbStart->setEnabled(false);
        ui->pbConnect->setEnabled(false);
        ui->pbStop->setEnabled(true);
        ui->pb_logBlockRefresh->setEnabled(true);
        ui->pb_logBlockStop->setEnabled(true);
        this->setTestStandButtons();
    }
    
}

void MainWindow::workerDisconnected()
{
    connectedWorkers--;
    if (( connectedWorkers == 0) && (backendProcess->state() == QProcess::Running)) {
        backendProcess->terminate();
    } else if (connectedWorkers == 0) {
        //ui->pbStart->setEnabled(true);
        ui->pbConnect->setEnabled(true);
        ui->pbStop->setEnabled(false);
        ui->pb_getParam->setEnabled(false);
        ui->pb_setParam->setEnabled(false);
        ui->pb_logBlockRefresh->setEnabled(false);
        ui->pb_logBlockStop->setEnabled(false);
    }
}

void MainWindow::backendFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    //ui->pbStart->setEnabled(true);
    ui->pbConnect->setEnabled(true);
    ui->pbStop->setEnabled(false);
}

void MainWindow::on_pbConnect_clicked()
{
    emit(connectWorkers());
}

void MainWindow::on_pbStop_clicked()
{
    emit(disconnectWorkers());
}

void MainWindow::on_chooseBackend_clicked()
{
    QString backendPath = QFileDialog::getOpenFileName(this,
         tr("Path to Backend Executable"));
    ui->backendPath->setText(backendPath);
}

void MainWindow::setParamGroups(QStringList groupNames) {
    ui->paramGroupComboBox->addItem("");
    ui->paramGroupComboBox->addItems(groupNames);
    ui->paramGroupComboBox_2->addItem("");
    ui->paramGroupComboBox_2->addItems(groupNames);
}

void MainWindow::setLoggingVariableBox(QStringList variableNames) {
    ui->logVariableList->addItems(variableNames);
    ui->logVariableList->sortItems(Qt::AscendingOrder);
}

void MainWindow::getGroupBoxChange(int index) {
    ui->paramEntriesComboBox->clear();
    QString qStrGroup = ui->paramGroupComboBox->itemText(index);
    std::string check = qStrGroup.toStdString();
    ui->valueVallabel->clear();
    if(!check.compare("")) {
        ui->pb_getParam->setEnabled(false);
    }
    else {
        emit(getGroupEntries(1, qStrGroup));
    }
}

void MainWindow::getGroupBox2Change(int index) {
    ui->paramEntriesComboBox_2->clear();
    QString qStrGroup = ui->paramGroupComboBox_2->itemText(index);
    std::string check = qStrGroup.toStdString();
    if(!check.compare("")) {
        ui->pb_setParam->setEnabled(false);
    }
    else {
        emit(getGroupEntries(2, qStrGroup));
    }
}

void MainWindow::getEntryBoxChange(int index) {
    QString qStrGroup = ui->paramEntriesComboBox->itemText(index);
    std::string check = qStrGroup.toStdString();
    ui->valueVallabel->clear();
    if(!check.compare("")) {
        ui->pb_getParam->setEnabled(false);
    }
    else {
        ui->pb_getParam->setEnabled(true);
    }
}

void MainWindow::getEntryBoxChange2(int index) {
    QString qStrGroup = ui->paramEntriesComboBox_2->itemText(index);
    std::string check = qStrGroup.toStdString();
    if(!check.compare("")) {
        ui->pb_setParam->setEnabled(false);
    }
    else {
        ui->pb_setParam->setEnabled(true);
    }
}

void MainWindow::newGroupEntries(int box, QStringList entries) {
    if(box == 1) {
        ui->paramEntriesComboBox->addItem("");
        ui->paramEntriesComboBox->addItems(entries);
    }
    else {
        ui->paramEntriesComboBox_2->addItem("");
        ui->paramEntriesComboBox_2->addItems(entries);
    }    
}

void MainWindow::on_pb_getParam_click() {
    try{
        ui->valueVallabel->clear();
        ui->pb_getParam->setEnabled(false);
        QString group = ui->paramGroupComboBox->currentText();
        QString entry = ui->paramEntriesComboBox->currentText();
        QString key = group + "." + entry;
        emit(getParamValue(key));
    }
    catch(...){
        dbugfile << "There was an error getting the param... TRY AGAIN\n";
    }
}

void MainWindow::on_pb_setParam_click() {
    QString group = ui->paramGroupComboBox_2->currentText();
    QString entry = ui->paramEntriesComboBox_2->currentText();
    QString key = group + "." + entry;
    double value = ui->setValueSpin->value();
    emit(setParamValue(key, value));
}

void MainWindow::on_pb_setParamJSON_click(){
    try{
        updateJSONParams();
    }
    catch(...){
        dbugfile << "error with the json\n";
    }
}

void MainWindow::openParamsJsonFile() {
    QProcess *fileOpen = new QProcess(this);
    QString exec = "gedit /home/bitcraze/MicroCART/mp4Params.json";
    fileOpen->start(exec);
    ui->JsonParamComplete->setEnabled(false);
}

/**
 * Helper function to parse the values inside mp4Params.json.
 * It then sets all of the params on the crazyflie board.
 * 
 * NOTE: Do we need to wait a specific amount of time between interfacing with backend?
 */ 
void MainWindow::updateJSONParams() {
    double value = 0;
    QFile file("/home/bitcraze/MicroCART/mp4Params.json");
    if(file.open(QIODevice::ReadOnly)){
        if(DEBUG)
            dbugfile << "opened file\n";
        QByteArray bytes = file.readAll();
        file.close();
    
        QJsonDocument jsonDocument = QJsonDocument::fromJson(bytes);
        QJsonObject object = jsonDocument.object();

       
        //parsing for s_pid_rate group
        QJsonObject s_pid_rate = object.take("s_pid_rate").toObject();
        value = s_pid_rate.take("roll_kp").toDouble();
        QString key = "s_pid_rate.roll_kp";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(1);
        value = s_pid_rate.take("roll_ki").toDouble();
        key = "s_pid_rate.roll_ki";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(2);
        value = s_pid_rate.take("roll_kd").toDouble();
        key = "s_pid_rate.roll_kd";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(3);
        value = s_pid_rate.take("pitch_kp").toDouble();
        key = "s_pid_rate.pitch_kp";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(4);
        value = s_pid_rate.take("pitch_ki").toDouble();
        key = "s_pid_rate.pitch_ki";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(5);
        value = s_pid_rate.take("pitch_kd").toDouble();
        key = "s_pid_rate.pitch_kd";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(6);
        value = s_pid_rate.take("yaw_kp").toDouble();
        key = "s_pid_rate.yaw_kp";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(7);
        value = s_pid_rate.take("yaw_ki").toDouble();
        key = "s_pid_rate.yaw_ki";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(8);
        value = s_pid_rate.take("yaw_kd").toDouble();
        key = "s_pid_rate.yaw_kd";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(9);
        //parsing for s_pid_attitude group
        QJsonObject s_pid_attitude = object.take("s_pid_attitude").toObject();
        value = s_pid_attitude.take("roll_kp").toDouble();
        key = "s_pid_attitude.roll_kp";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(10);
        value = s_pid_attitude.take("roll_ki").toDouble();
        key = "s_pid_attitude.roll_ki";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(11);
        value = s_pid_attitude.take("roll_kd").toDouble();
        key = "s_pid_attitude.roll_kd";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(12);
        value = s_pid_attitude.take("pitch_kp").toDouble();
        key = "s_pid_attitude.pitch_kp";
        emit(setParamValue(key, value));
        sleep(1);
       
        ui->progressBar->setValue(13);
        value = s_pid_attitude.take("pitch_ki").toDouble();
        key = "s_pid_attitude.pitch_ki";
        emit(setParamValue(key, value));
        sleep(1);
       
        ui->progressBar->setValue(14);
        value = s_pid_attitude.take("pitch_kd").toDouble();
        key = "s_pid_attitude.pitch_kd";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(15);
        value = s_pid_attitude.take("yaw_kp").toDouble();
        key = "s_pid_attitude.yaw_kp";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(16);
        value = s_pid_attitude.take("yaw_ki").toDouble();
        key = "s_pid_attitude.yaw_ki";
        emit(setParamValue(key, value));
        sleep(1);
        
        ui->progressBar->setValue(17);
        value = s_pid_attitude.take("yaw_kd").toDouble();
        key = "s_pid_attitude.yaw_kd";
        emit(setParamValue(key, value));
        sleep(1);

        ui->progressBar->setValue(18);
        QJsonObject sys = object.take("sys").toObject();
        QJsonValue estop = sys.take("e_stop");
        key = "sys.e_stop";
        emit(setParamValue(key, estop.toDouble()));
        sleep(1);
                        
        ui->progressBar->setValue(19);
        // Display complete banner when finished
        ui->JsonParamComplete->setEnabled(true);

    }
    else{
        if(DEBUG)
            dbugfile << "Could not open file\n";
    }
    if(DEBUG)
        dbugfile.close();
    
     
}

void MainWindow::displayParamValue(QString key, float value) {
    ui->valueVallabel->setNum(value);
    ui->pb_getParam->setEnabled(true);
}

void MainWindow::on_applySetpointButton_clicked()
{
    sp_roll   = ui->rollSetpointBox->text().toFloat();
    sp_pitch  = ui->pitchSetpointBox->text().toFloat();
    sp_yaw    = ui->yawSetpointBox->text().toFloat();
    sp_thrust = ui->tActual -> value();

    if(ui->angleRadioButton->isChecked()) {
        //send as angle setpoint
        emit(angleSetpointSignal(sp_roll, sp_pitch, sp_yaw, sp_thrust));
    }
    else if (ui->rateRadioButton->isChecked()) {
        //send as rate setpoint
        emit(rateSetpointSignal(sp_roll, sp_pitch, sp_yaw, sp_thrust));
    }
    trigger_send_setpoint();

}

void MainWindow::on_stopSetpointButton_clicked()
{
    sp_roll   = 0.0;
    sp_pitch  = 0.0;
    sp_yaw    = 0.0;
    sp_thrust = 0.0;

    if(ui->angleRadioButton->isChecked()) {
        //send as angle setpoint
        emit(angleSetpointSignal(sp_roll, sp_pitch, sp_yaw, sp_thrust));
    }
    else if (ui->rateRadioButton->isChecked()) {
        //send as rate setpoint
        emit(rateSetpointSignal(sp_roll, sp_pitch, sp_yaw, sp_thrust));
    }

    trigger_send_setpoint();
}
void MainWindow::on_tActual_sliderMoved(int position)
{
    QToolTip::showText(QCursor::pos(), QString::number(position), nullptr);

}


/******** Gamepad handlers ********/

void MainWindow::onGamepadDisconnect(){

    emit(mixedSetpointSignal(0, 0, 0, 0));
    emit(rateSetpointSignal(0, 0, 0, 0));
    emit(angleSetpointSignal(0, 0, 0, 0));
   //navigation tab
    ui->pitchSetpointBox->setText("0");
    ui->rollSetpointBox->setText("0");
    ui->yawSetpointBox->setText("0");
    ui->tActual->setValue(0);
    ui->rbManualSetpoint->setChecked(true);

    //gamepad tab
    ui->noGamepadWarning->show();
    ui->rbGamepadControl->setEnabled(false);
    ui->pb_configPitch->setEnabled(false);
    ui->pb_configRoll->setEnabled(false);
    ui->pb_configYaw->setEnabled(false);
    ui->pb_configThrust->setEnabled(false);
    ui->pb_resetConfig->setEnabled(false);
    ui->gamepadRollScale->setEnabled(false);
    ui->rollScaleLabel->setEnabled(false);
    ui->gamepadPitchScale->setEnabled(false);
    ui->pitchScaleLabel->setEnabled(false);
    ui->gamepadYawScale->setEnabled(false);
    ui->yawScaleLabel->setEnabled(false);
    ui->gamepadThrustScale->setEnabled(false);
    ui->thrustScaleLabel->setEnabled(false);
    ui->thrustScaleNote->setEnabled(false);
    ui->degPerSecLabel->setEnabled(false);
    ui->degLabel->setEnabled(false);

}

void MainWindow::onGamepadConnect(){

    //navigation tab
    ui->rbGamepadControl->setEnabled(true);

    //gamepad tab
    ui->noGamepadWarning->hide();
    ui->pb_configPitch->setEnabled(true);
    ui->pb_configRoll->setEnabled(true);
    ui->pb_configYaw->setEnabled(true);
    ui->pb_configThrust->setEnabled(true);
    ui->pb_resetConfig->setEnabled(true);

    ui->gamepadRollScale->setEnabled(true);
    ui->rollScaleLabel->setEnabled(true);
    ui->gamepadPitchScale->setEnabled(true);
    ui->pitchScaleLabel->setEnabled(true);
    ui->gamepadYawScale->setEnabled(true);
    ui->yawScaleLabel->setEnabled(true);
    ui->gamepadThrustScale->setEnabled(true);
    ui->thrustScaleLabel->setEnabled(true);
    ui->thrustScaleNote->setEnabled(true);
    ui->degPerSecLabel->setEnabled(true);
    ui->degLabel->setEnabled(true);

}

void MainWindow::updateGamepad(){
    if(ui->tabWidget->currentWidget() == ui->Gamepad){
        //on gamepad tab
        ui->rollVizBar->setValue((int) gamepadMonitor->getRoll());
        ui->yawVizBar->setValue((int) gamepadMonitor->getYaw());
        ui->pitchVizBar->setValue((int) gamepadMonitor->getPitch());
        ui->thrustVizBar->setValue((int) gamepadMonitor->getThrust());
    }else if(ui->tabWidget->currentWidget() == ui->navigation){
        if(ui->rbGamepadControl->isChecked()){
            float roll = gamepadMonitor->getRoll();
            float pitch = gamepadMonitor->getPitch();
            float yaw = gamepadMonitor->getYaw();
            float thrust = gamepadMonitor->getThrust();

            //create mixed setpoint
            emit(mixedSetpointSignal(roll, pitch, yaw, thrust));

            ui->rollSetpointBox->setText(QString::number(roll));
            ui->pitchSetpointBox->setText(QString::number(pitch));
            ui->yawSetpointBox->setText(QString::number(yaw));
            ui->tActual->setValue((int) thrust);
        }
    }
}

void MainWindow::on_pb_resetConfig_clicked()
{
    gamepadMonitor->resetConfig();
    ui->gamepadRollScale->setValue((int)gamepadMonitor->getRollScale());
    ui->gamepadPitchScale->setValue((int)gamepadMonitor->getPitchScale());
    ui->gamepadYawScale->setValue((int)gamepadMonitor->getYawScale());
    ui->gamepadThrustScale->setValue((int)gamepadMonitor->getThrustScale());
}

void MainWindow::on_pb_configRoll_clicked()
{
    gamepadMonitor->configureAxis("roll");
}

void MainWindow::on_pb_configYaw_clicked()
{
    gamepadMonitor->configureAxis("yaw");
}

void MainWindow::on_pb_configPitch_clicked()
{
    gamepadMonitor->configureAxis("pitch");
}

void MainWindow::on_pb_configThrust_clicked()
{
    gamepadMonitor->configureAxis("thrust");
}

void MainWindow::on_rbManualSetpoint_toggled(bool checked)
{
    if(checked) {
        crazyflieTimer->start(300); //slower rate for manual setpoints
        emit(mixedSetpointSignal(0, 0, 0, 0));
        emit(rateSetpointSignal(0, 0, 0, 0));
        emit(angleSetpointSignal(0, 0, 0, 0));
        
        ui->pitchSetpointBox->setText("0");
        ui->rollSetpointBox->setText("0");
        ui->yawSetpointBox->setText("0");
        ui->tActual->setValue(0);
        ui->rollSetpointBox-> setEnabled(true);
        ui->pitchSetpointBox-> setEnabled(true);
        ui->yawSetpointBox-> setEnabled(true);
        ui->tActual-> setEnabled(true);
        ui->angleRadioButton-> setEnabled(true);
        ui->rateRadioButton-> setEnabled(true);
        ui->applySetpointButton->setEnabled(true);
        ui->stopSetpointButton->setEnabled(true);
    }
}

void MainWindow::on_rbGamepadControl_toggled(bool checked)
{

    if(checked){
        crazyflieTimer->start(100); //faster rate for gamepad
        ui->rollSetpointBox-> setEnabled(false);
        ui->pitchSetpointBox-> setEnabled(false);
        ui->yawSetpointBox-> setEnabled(false);
        ui->tActual-> setEnabled(false);
        ui->angleRadioButton-> setEnabled(false);
        ui->rateRadioButton-> setEnabled(false);
        ui->applySetpointButton->setEnabled(false);
        ui->stopSetpointButton->setEnabled(false);
    }
}

void MainWindow::trigger_send_setpoint(){
    if(ui->rbGamepadControl->isChecked()){
        //using gamepad, send mixed attitude setpoint
        emit(triggerMixedAttSetpointSend());
    }else{
        if(ui->angleRadioButton->isChecked()){
            emit(triggerAttSetpointSend());
        }else{
            emit(triggerAttRateSetpointSend());
        }
    }
}

void MainWindow::on_gamepadYawScale_valueChanged(int arg1)
{
    gamepadMonitor->setYawScale(arg1);
    ui->yawVizBar->setMinimum(std::abs(arg1) * -1);
    ui->yawVizBar->setMaximum(std::abs(arg1));
}

void MainWindow::on_gamepadThrustScale_valueChanged(int arg1)
{
    gamepadMonitor->setThrustScale(arg1);
    //Dont need to change the scale of the thrust viz bar because it is always interpreted as 0 to 60,000
}

void MainWindow::on_gamepadRollScale_valueChanged(int arg1)
{
    gamepadMonitor->setRollScale(arg1);
    ui->rollVizBar->setMinimum(std::abs(arg1) * -1);
    ui->rollVizBar->setMaximum(std::abs(arg1));
}

void MainWindow::on_gamepadPitchScale_valueChanged(int arg1)
{
    gamepadMonitor->setPitchScale(arg1);
    ui->pitchVizBar->setMinimum(std::abs(arg1) * -1);
    ui->pitchVizBar->setMaximum(std::abs(arg1));
}

void MainWindow::graphLogs(QVector<QVector<double>> x, QVector<QVector<double>> y, bool lastSet)
{
    //set data
    for(int i=0; i < 5; i++){
        ui->dataPlot->graph(i)->addData(x[i], y[i], true);
    }

    //find max x
    double xMax = 0;
    for(int i=0; i < x.size(); i++){
        if(!x[i].isEmpty()){
            xMax = (x[i].last() > xMax) ? x[i].last() : xMax;
        }
    }


    ui->dataPlot->xAxis->setRange(xMax, 10, Qt::AlignRight);
    if(ui->cb_autoscale->isChecked()){
        ui->dataPlot->yAxis->rescale();
    }else{
        ui->dataPlot->yAxis->setRange(ui->plot_min->value(), ui->plot_max->value());
    }
}

void MainWindow::openLogBlockFile() {
    QProcess *fileOpen = new QProcess(this);
    QString exec = "gedit " + QDir::currentPath() + "/../loggingBlocks.txt";
    fileOpen->start(exec);
}

void MainWindow::on_pb_lbRefresh() {
    ui->pb_logBlockRefresh->setEnabled(false);
    ui->pb_logBlockResume->setEnabled(false);
    ui->pb_logBlockPause->setEnabled(false);
    ui->pb_logBlockStop->setEnabled(false);
    emit(sendLogBlockCommand(1, -1));
}

void MainWindow::on_pb_lbStop() {
    ui->pb_logBlockRefresh->setEnabled(false);
    ui->pb_logBlockResume->setEnabled(false);
    ui->pb_logBlockPause->setEnabled(false);
    ui->pb_logBlockStop->setEnabled(false);
    emit(sendLogBlockCommand(0, -1));
}

void MainWindow::on_pb_lbResume() {
    ui->pb_logBlockRefresh->setEnabled(false);
    ui->pb_logBlockResume->setEnabled(false);
    ui->pb_logBlockPause->setEnabled(false);
    ui->pb_logBlockStop->setEnabled(false);
    QString blockName = ui->comboBox_logBlocks->currentText();
    emit(requestLogBlockID(blockName, 4));
}

void MainWindow::on_pb_lbPause() {
    ui->pb_logBlockRefresh->setEnabled(false);
    ui->pb_logBlockResume->setEnabled(false);
    ui->pb_logBlockPause->setEnabled(false);
    ui->pb_logBlockStop->setEnabled(false);
    QString blockName = ui->comboBox_logBlocks->currentText();
    emit(requestLogBlockID(blockName, 5));
}

void MainWindow::on_pb_lbDelete() {
    QString blockName = ui->comboBox_logBlocks->currentText();
    emit(requestLogBlockID(blockName, 3));
}

void MainWindow::useLogBlockID(int id, int8_t command) {
    emit(sendLogBlockCommand(command, id));
}

void MainWindow::gotLogBlockNames(QStringList logBlockNames) {
    ui->comboBox_logBlocks->clear();
    ui->comboBox_logBlocks->addItem("Logging Blocks");
    ui->comboBox_logBlocks->addItems(logBlockNames);
}

void MainWindow::setLogButtons(bool isEnabled) {
    if(!ui->comboBox_logBlocks->currentText().toStdString().compare("Logging Blocks")) {
        ui->pb_logBlockResume->setEnabled(false);
        ui->pb_logBlockPause->setEnabled(false);
    }
    else if(!ui->comboBox_logBlocks->currentText().toStdString().compare("MicroCART")) {
        ui->pb_logBlockResume->setEnabled(false);
        ui->pb_logBlockPause->setEnabled(false);
    }
    else {
        ui->pb_logBlockResume->setEnabled(!isEnabled);
        ui->pb_logBlockPause->setEnabled(isEnabled);
    }
}

void MainWindow::fillLogVariablesBox(QStringList activeVars) {

    ui->logEntriesComboBox1->setCurrentIndex(0);
    ui->logEntriesComboBox2->setCurrentIndex(0);
    ui->logEntriesComboBox3->setCurrentIndex(0);
    ui->logEntriesComboBox4->setCurrentIndex(0);
    ui->logEntriesComboBox5->setCurrentIndex(0);
    logVar1 = "Logging Variable 1";
    logVar2 = "Logging Variable 2";
    logVar3 = "Logging Variable 3";
    logVar4 = "Logging Variable 4";
    logVar5 = "Logging Variable 5";
    ui->logEntriesComboBox1->clear();
    ui->logEntriesComboBox1->addItem("Logging Variable 1");
    ui->logEntriesComboBox2->clear();
    ui->logEntriesComboBox2->addItem("Logging Variable 2");
    ui->logEntriesComboBox3->clear();
    ui->logEntriesComboBox3->addItem("Logging Variable 3");
    ui->logEntriesComboBox4->clear();
    ui->logEntriesComboBox4->addItem("Logging Variable 4");
    ui->logEntriesComboBox5->clear();
    ui->logEntriesComboBox5->addItem("Logging Variable 5");
    ui->logEntriesComboBox1->addItems(activeVars);
    ui->logEntriesComboBox2->addItems(activeVars);
    ui->logEntriesComboBox3->addItems(activeVars);
    ui->logEntriesComboBox4->addItems(activeVars);
    ui->logEntriesComboBox5->addItems(activeVars);
    ui->pb_logBlockRefresh->setEnabled(true);
    ui->pb_logBlockResume->setEnabled(true);
    ui->pb_logBlockPause->setEnabled(true);
    ui->pb_logBlockStop->setEnabled(true);
}

void MainWindow::getLogEntryBoxChange1(QString text) {
    if(logVar1.toStdString().compare("Logging Variable 1")) {
        ui->logEntriesComboBox2->addItem(logVar1);
        ui->logEntriesComboBox3->addItem(logVar1);
        ui->logEntriesComboBox4->addItem(logVar1);
        ui->logEntriesComboBox5->addItem(logVar1);
    }
    logVar1 = text;
    int idx = -1;
    if(logVar1.toStdString().compare("Logging Variable 1")) {
        idx = ui->logEntriesComboBox2->findText(logVar1);
        if(idx >= 0) {
           ui->logEntriesComboBox2->removeItem(idx);
        }
        idx = ui->logEntriesComboBox3->findText(logVar1);
        if(idx >= 0) {
           ui->logEntriesComboBox3->removeItem(idx);
        }
        idx = ui->logEntriesComboBox4->findText(logVar1);
        if(idx >= 0) {
           ui->logEntriesComboBox4->removeItem(idx);
        }
        idx = ui->logEntriesComboBox5->findText(logVar1);
        if(idx >= 0) {
           ui->logEntriesComboBox5->removeItem(idx);
        }
    }
}

void MainWindow::getLogEntryBoxChange2(QString text) {
    if(logVar2.toStdString().compare("Logging Variable 2")) {
        ui->logEntriesComboBox1->addItem(logVar2);
        ui->logEntriesComboBox3->addItem(logVar2);
        ui->logEntriesComboBox4->addItem(logVar2);
        ui->logEntriesComboBox5->addItem(logVar2);
    }
    logVar2 = text;
    int idx = -1;
    if(logVar2.toStdString().compare("Logging Variable 2")) {
        idx = ui->logEntriesComboBox1->findText(logVar2);
        if(idx >= 0) {
           ui->logEntriesComboBox1->removeItem(idx);
        }
        idx = ui->logEntriesComboBox3->findText(logVar2);
        if(idx >= 0) {
           ui->logEntriesComboBox3->removeItem(idx);
        }
        idx = ui->logEntriesComboBox4->findText(logVar2);
        if(idx >= 0) {
           ui->logEntriesComboBox4->removeItem(idx);
        }
        idx = ui->logEntriesComboBox5->findText(logVar2);
        if(idx >= 0) {
           ui->logEntriesComboBox5->removeItem(idx);
        }
    }
}

void MainWindow::getLogEntryBoxChange3(QString text) {
    if(logVar3.toStdString().compare("Logging Variable 3")) {
        ui->logEntriesComboBox1->addItem(logVar3);
        ui->logEntriesComboBox2->addItem(logVar3);
        ui->logEntriesComboBox4->addItem(logVar3);
        ui->logEntriesComboBox5->addItem(logVar3);
    }
    logVar3 = text;
    int idx = -1;
    if(logVar3.toStdString().compare("Logging Variable 3")) {
        idx = ui->logEntriesComboBox2->findText(logVar3);
        if(idx >= 0) {
           ui->logEntriesComboBox2->removeItem(idx);
        }
        idx = ui->logEntriesComboBox1->findText(logVar3);
        if(idx >= 0) {
           ui->logEntriesComboBox1->removeItem(idx);
        }
        idx = ui->logEntriesComboBox4->findText(logVar3);
        if(idx >= 0) {
           ui->logEntriesComboBox4->removeItem(idx);
        }
        idx = ui->logEntriesComboBox5->findText(logVar3);
        if(idx >= 0) {
           ui->logEntriesComboBox5->removeItem(idx);
        }
    }
}

void MainWindow::getLogEntryBoxChange4(QString text) {
    if(logVar4.toStdString().compare("Logging Variable 4")) {
        ui->logEntriesComboBox2->addItem(logVar4);
        ui->logEntriesComboBox3->addItem(logVar4);
        ui->logEntriesComboBox1->addItem(logVar4);
        ui->logEntriesComboBox5->addItem(logVar4);
    }
    logVar4 = text;
    int idx = -1;
    if(logVar4.toStdString().compare("Logging Variable 4")) {
        idx = ui->logEntriesComboBox2->findText(logVar4);
        if(idx >= 0) {
           ui->logEntriesComboBox2->removeItem(idx);
        }
        idx = ui->logEntriesComboBox3->findText(logVar4);
        if(idx >= 0) {
           ui->logEntriesComboBox3->removeItem(idx);
        }
        idx = ui->logEntriesComboBox1->findText(logVar4);
        if(idx >= 0) {
           ui->logEntriesComboBox1->removeItem(idx);
        }
        idx = ui->logEntriesComboBox5->findText(logVar4);
        if(idx >= 0) {
           ui->logEntriesComboBox5->removeItem(idx);
        }
    }
}

void MainWindow::getLogEntryBoxChange5(QString text) {
    if(logVar5.toStdString().compare("Logging Variable 5")) {
        ui->logEntriesComboBox2->addItem(logVar5);
        ui->logEntriesComboBox3->addItem(logVar5);
        ui->logEntriesComboBox4->addItem(logVar5);
        ui->logEntriesComboBox5->addItem(logVar5);
    }
    logVar5 = text;
    int idx = -1;
    if(logVar5.toStdString().compare("Logging Variable 5")) {
        idx = ui->logEntriesComboBox2->findText(logVar5);
        if(idx >= 0) {
           ui->logEntriesComboBox2->removeItem(idx);
        }
        idx = ui->logEntriesComboBox3->findText(logVar5);
        if(idx >= 0) {
           ui->logEntriesComboBox3->removeItem(idx);
        }
        idx = ui->logEntriesComboBox4->findText(logVar5);
        if(idx >= 0) {
           ui->logEntriesComboBox4->removeItem(idx);
        }
        idx = ui->logEntriesComboBox1->findText(logVar5);
        if(idx >= 0) {
           ui->logEntriesComboBox1->removeItem(idx);
        }
    }
}

void MainWindow::receiveLogFile(QString filename) {
    emit(beginReadingLogFile(filename));
}

void MainWindow::on_pb_startLog_clicked() {
    ui->pb_stopLog->setEnabled(true);
    ui->pb_startLog->setEnabled(false);
    logTimer->start(100);

    //clear data from all graphs
    for(int i = 0; i < 5; i++){
        ui->dataPlot->graph(i)->data()->clear();
    }

    QString name1 = ui->logEntriesComboBox1->currentText();
    QString name2 = ui->logEntriesComboBox2->currentText();
    QString name3 = ui->logEntriesComboBox3->currentText();
    QString name4 = ui->logEntriesComboBox4->currentText();
    QString name5 = ui->logEntriesComboBox5->currentText();
    logWorker->logColumns = crazyflieWorker->getLogColumns(name1, name2, name3, name4, name5);
    emit(sendLogBlockCommand(8, -1));
    emit(sendStartLogging());
}

void MainWindow::on_pb_stopLog_clicked() {
    ui->pb_startLog->setEnabled(true);
    ui->pb_stopLog->setEnabled(false);
    ui->dataPlot->replot();
    logTimer->stop();

    logWorker->stopLogging();
    emit(sendLogBlockCommand(9, -1));
}

void MainWindow::on_pb_disconnectTS() {
    emit(sendLogBlockCommand(7, -1));
    this->setTestStandButtons();
    ui->pb_disconnectTS->setEnabled(false);
    ui->pb_connectTS->setEnabled(true);
}

void MainWindow::on_pb_connectTS() {
    emit(sendLogBlockCommand(6, -1));
    this->setTestStandButtons();
}
void MainWindow::logBlockOutOfSyncPopup(){
    QMessageBox::warning(
        this,
        tr("MicroCART Groundstation"),
        tr("Crazyflie log block is desynced. Please refresh log block.") );
}

void MainWindow::setTestStandButtons() {
    std::string result = crazyflieWorker->getLogFile(4);
    if(!result.compare("false")) {
        ui->pb_disconnectTS->setEnabled(false);
        ui->pb_connectTS->setEnabled(true);
    }
    else {
        ui->pb_disconnectTS->setEnabled(true);
        ui->pb_connectTS->setEnabled(false);
    }
}

void MainWindow::on_cb_autoscale_toggled(bool checked)
{
    if (checked){
        ui->plot_max_label->setEnabled(false);
        ui->plot_max->setEnabled(false);

        ui->plot_min_label->setEnabled(false);
        ui->plot_min->setEnabled(false);
    }else{
        ui->plot_max_label->setEnabled(true);
        ui->plot_max->setEnabled(true);

        ui->plot_min_label->setEnabled(true);
        ui->plot_min->setEnabled(true);
    }
}

void MainWindow::updateGraph() {
    ui->dataPlot->replot();
}

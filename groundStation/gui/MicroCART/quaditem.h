#ifndef QUADITEM_H
#define QUADITEM_H

#include <QPainter>
#include <QGraphicsItem>
#include <QPixmap>
#include <QLabel>
#include <QtCore>
#include <QtGui>
#include <QTransform>

class QuadItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
   QuadItem();
   QRectF boundingRect() const;
   void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

public slots:
   void updatePos(float longitudinal, float lateral, float height, float pitch, float roll, float yaw);

};

#endif // QUADITEM_H

/****************************************************************************
** Meta object code from reading C++ file 'qattitudeindicator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "qattitudeindicator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qattitudeindicator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_qAttitudeIndicator_t {
    QByteArrayData data[9];
    char stringdata0[78];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_qAttitudeIndicator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_qAttitudeIndicator_t qt_meta_stringdata_qAttitudeIndicator = {
    {
QT_MOC_LITERAL(0, 0, 18), // "qAttitudeIndicator"
QT_MOC_LITERAL(1, 19, 14), // "updateAttitude"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 12), // "longitudinal"
QT_MOC_LITERAL(4, 48, 7), // "lateral"
QT_MOC_LITERAL(5, 56, 6), // "height"
QT_MOC_LITERAL(6, 63, 5), // "pitch"
QT_MOC_LITERAL(7, 69, 4), // "roll"
QT_MOC_LITERAL(8, 74, 3) // "yaw"

    },
    "qAttitudeIndicator\0updateAttitude\0\0"
    "longitudinal\0lateral\0height\0pitch\0"
    "roll\0yaw"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_qAttitudeIndicator[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    6,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float,    3,    4,    5,    6,    7,    8,

       0        // eod
};

void qAttitudeIndicator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        qAttitudeIndicator *_t = static_cast<qAttitudeIndicator *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateAttitude((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< float(*)>(_a[5])),(*reinterpret_cast< float(*)>(_a[6]))); break;
        default: ;
        }
    }
}

const QMetaObject qAttitudeIndicator::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_qAttitudeIndicator.data,
      qt_meta_data_qAttitudeIndicator,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *qAttitudeIndicator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *qAttitudeIndicator::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_qAttitudeIndicator.stringdata0))
        return static_cast<void*>(const_cast< qAttitudeIndicator*>(this));
    return QWidget::qt_metacast(_clname);
}

int qAttitudeIndicator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

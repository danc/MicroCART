#include "gamepadmonitor.h"
#include <QDebug>

GamepadMonitor::GamepadMonitor(QObject *parent) : QObject(parent)
{
    m_gamepadManager = QGamepadManager::instance();

    rollScale = DEFAULT_ROLL_SCALE;
    pitchScale = DEFAULT_PITCH_SCALE;
    yawScale = DEFAULT_YAW_SCALE;
    thrustScale = DEFAULT_THRUST_SCALE;

    connect(m_gamepadManager, SIGNAL(gamepadConnected(int)), this, SLOT(gamepadConnectedHandler(int)));
    connect(m_gamepadManager, SIGNAL(gamepadDisconnected(int)), this, SLOT(gamepadDisconnectedHandler(int)));

    auto gamepads = m_gamepadManager->connectedGamepads();
    if (gamepads.isEmpty()) {
        // Did not find any connected gamepads creating default gamepad;
        m_gamepad = new QGamepad();
        return;
    }

    m_gamepad = new QGamepad(*gamepads.begin(), this);
    qInfo() << "gamepad is connected, deviceId = " << m_gamepad->deviceId();
}

void GamepadMonitor::gamepadConnectedHandler(int deviceId){
    emit(gamepadConnected());
    qInfo() << "gamepad is connected, deviceId = " << deviceId;
    m_gamepad = new QGamepad(deviceId, this);
}

void GamepadMonitor::gamepadDisconnectedHandler(int deviceId){
    emit(gamepadDisconnected());
    qInfo() << "gamepad disconnected";
}

bool GamepadMonitor::isGamepadConnected(){
    return m_gamepad->isConnected();
}

void GamepadMonitor::resetConfig(){
    qInfo() << "gamepad config reset";

    rollScale = DEFAULT_ROLL_SCALE;
    pitchScale = DEFAULT_PITCH_SCALE;
    yawScale = DEFAULT_YAW_SCALE;
    thrustScale = DEFAULT_THRUST_SCALE;

    m_gamepadManager->resetConfiguration(m_gamepad->deviceId());
}

void GamepadMonitor::configureAxis(QString axisName){
    if(axisName == "roll"){
        m_gamepadManager->configureAxis(m_gamepad->deviceId(), QGamepadManager::AxisRightX);
        qInfo() << "roll configured";
    }else if(axisName == "yaw"){
        m_gamepadManager->configureAxis(m_gamepad->deviceId(), QGamepadManager::AxisLeftX);
        qInfo() << "yaw configured";
    }else if(axisName == "pitch"){
        m_gamepadManager->configureAxis(m_gamepad->deviceId(), QGamepadManager::AxisRightY);
        qInfo() << "pitch configured";
    }else if(axisName == "thrust"){
        m_gamepadManager->configureAxis(m_gamepad->deviceId(), QGamepadManager::AxisLeftY);
        qInfo() << "thrust configured";
    }
}

float GamepadMonitor::getRoll(){
    return (float) m_gamepad->axisRightX() * rollScale;
}

float GamepadMonitor::getYaw(){
    return (float) m_gamepad->axisLeftX() * yawScale;
}

float GamepadMonitor::getPitch(){
    return (float) m_gamepad->axisRightY() * pitchScale;
}

float GamepadMonitor::getThrust(){
    //first scale range (-1, 1) to (-scale/2 , scale/2)
    //then shift range to (0, scale)
    //multiplying by range first allows for inverting input
    return (float) (m_gamepad->axisLeftY() * thrustScale/2 ) + (std::abs(thrustScale)/2) ;
}

void GamepadMonitor::setRollScale(float scale){
    rollScale = scale;
}

void GamepadMonitor::setYawScale(float scale){
    yawScale = scale;
}

void GamepadMonitor::setPitchScale(float scale){
    pitchScale = scale;
}

void GamepadMonitor::setThrustScale(float scale){
    thrustScale = scale;
}


float GamepadMonitor::getRollScale(){
    return rollScale;
}
float GamepadMonitor::getPitchScale(){
    return pitchScale;
}
float GamepadMonitor::getYawScale(){
    return yawScale;
}
float GamepadMonitor::getThrustScale(){
    return thrustScale;
}

GamepadMonitor::~GamepadMonitor(){

}

#ifndef CONTROLWORKER_H
#define CONTROLWORKER_H

#include <QObject>
#include <QStringList>
#include <QMap>
#include "frontend_common.h"
#include "frontend_nodes.h"

/**
 * The contol worker handles managing the control graph nodes and edges.
 * It can get, and set node parameters.
 * @brief The ControlWorker class
 */
class ControlWorker : public QObject
{
    Q_OBJECT
public:
    explicit ControlWorker(QObject *parent = 0);
    frontend_node_data getNodeId(QString node);
    ~ControlWorker();

signals:
    void gotNodes(QStringList nodes);
    void gotParams(QStringList params);
    void gotParamValue(QString node, QString param, float value);
    void gotNodeOutput(QString node, float output);
    void gotConstantBlocks(QStringList blocks);
    void paramSet(QString node, QString param);
    void graphRendered(QString graph);
    void connected();
    void disconnected();

public slots:
    void connectBackend();
    void disconnectBackend();
    void getNodes();
    void getParams(QString node);
    void getParamValue(QString node, QString param);
    void setParamValue(QString node, QString name, float value);
    void getNodeOutput(QString node);

private:
    struct backend_conn * conn;
    QMap<QString, frontend_node_data> nodeIdCache;

};

#endif // CONTROLWORKER_H

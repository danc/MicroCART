#include "crazyflieworker.h"
#include <QFileDialog>
#include <iostream>
#include <unistd.h>
#include <sys/stat.h>

CrazyflieWorker::CrazyflieWorker(QObject *parent) : QObject(parent), conn(NULL)
{
}

CrazyflieWorker::~CrazyflieWorker()
{
    disconnectBackend();
}

/**
 * @brief CrazyflieWorker::connectBackend
 * Command the worker to connect to the backend
 * will set the private class struct, conn, once done.
 * The connected() signal is emitted once complete.
 */
void CrazyflieWorker::connectBackend()
{
    if (conn == NULL) {
        conn = ucart_backendConnect();
        emit (connected());
    } else {
        qInfo() << "Attempted to connect crazyflieworker when already connected!";
    }
    isBackendConnected = true;
    loadParamIds();
    loadLogIds();
    loadLogBlocks();
    QStringList logBlockNames = getLogBlockNames();
    emit(changeLogBlock_comboBox(logBlockNames));
    QStringList paramGroupList;
    for(int i = 0; i < paramGroups.size(); i++) {
        QString qgName(paramGroups.at(i).groupName.c_str());
        paramGroupList.append(qgName);
    }
    emit(gotParamGroups(paramGroupList));
    QStringList logVariablesList;
    struct toc_group curGroup;
    for(int i = 0; i < logGroups.size(); i++) {
        curGroup = logGroups.at(i);
        for(int j = 0; j < logGroups.at(i).entryNames.size(); j++) {
            QString qvName(logGroups.at(i).groupName.c_str());
            qvName = qvName.append(".");
            qvName = qvName.append(logGroups.at(i).entryNames.at(j).c_str());
            logVariablesList.append(qvName);
        }
    }
    emit(gotLogVariables(logVariablesList));
    QStringList activeVars;
    logVarsInHeader();
    for(int i = 0; i < logBlocks.size(); i++) {
        if(logBlocks.at(i).isEnabled) {
            for(int j = 0; j < logBlocks.at(i).variableNames.size(); j++) {
                QString fullIdentifier(logBlocks.at(i).variableNames.at(j).strGroup.c_str());
                QString varname(logBlocks.at(i).variableNames.at(j).strName.c_str());
                fullIdentifier = fullIdentifier + "." + varname;
                if(isInHeaderVars(fullIdentifier.toStdString())) {
                    activeVars.append(fullIdentifier);
                }
            }
        }
    }
    emit(gotActiveLogVariables(activeVars));
    std::string realLogfile = getLogFile(0);
    QString qLogfile(realLogfile.c_str());
    emit(gotLogFile(qLogfile));
}

/**
 * @brief CrazyflieWorker::disconnectBackend
 * Command the worker to disconnect from the backend,
 * sets the conn struct to null and emits the
 * disconnected() signal once complete
 */
void CrazyflieWorker::disconnectBackend()
{
    if (conn) {
         ucart_backendDisconnect(conn);
         conn = NULL;
         isBackendConnected = false;
         emit (disconnected());
    }
}

void CrazyflieWorker::setCurrAttSetpoint(float roll, float pitch, float yaw, float throttle)
{
    currAttitudeSetpoint.roll = roll;
    currAttitudeSetpoint.pitch = pitch;
    currAttitudeSetpoint.yaw = yaw;
    currAttitudeSetpoint.throttle = throttle;
}

void CrazyflieWorker::setCurrAttRateSetpoint(float rollRate, float pitchRate, float yawRate, float throttle)
{
    //qInfo() << "set rate";
    currAttitudeRateSetpoint.roll = rollRate;
    currAttitudeRateSetpoint.pitch = pitchRate;
    currAttitudeRateSetpoint.yaw = yawRate;
    currAttitudeRateSetpoint.throttle = throttle;
}

void CrazyflieWorker::setCurrMixedSetpoint(float roll, float pitch, float yawRate, float throttle){
    //qInfo() << "set rate";
    currMixedSetpoint.roll = roll;
    currMixedSetpoint.pitch = pitch;
    currMixedSetpoint.yaw = yawRate;
    currMixedSetpoint.throttle = throttle;
}



void CrazyflieWorker::sendAttRateSetpoint()
{

    if(isBackendConnected){
        //qInfo() << " send att rate" << currAttitudeRateSetpoint.throttle << " " << currAttitudeRateSetpoint.roll;

        struct frontend_override_data data;
        data.enable = 2;    //attitude rate
        data.time = 0;
        data.throttle = currAttitudeRateSetpoint.throttle;
        data.roll = currAttitudeRateSetpoint.roll;
        data.pitch = currAttitudeRateSetpoint.pitch;
        data.yaw = currAttitudeRateSetpoint.yaw;

        //send setpoint
        if(frontend_setoutputoverride(conn, &data) != 0){
            qInfo() << "error sending att rate setpoint";
        }
    }
}


void CrazyflieWorker::sendAttSetpoint()
{

    if(isBackendConnected){
        //qInfo() << " send att" << currAttitudeSetpoint.throttle;

        struct frontend_override_data data;
        data.enable = 1;    //attitude
        data.time = 0;
        data.throttle = currAttitudeSetpoint.throttle;
        data.roll = currAttitudeSetpoint.roll;
        data.pitch = currAttitudeSetpoint.pitch;
        data.yaw = currAttitudeSetpoint.yaw;

        //send setpoint
        if(frontend_setoutputoverride(conn, &data) != 0){
            qInfo() << "error sending att setpoint";
        }
    }
}

void CrazyflieWorker::sendMixedAttSetpoint(){


    if(isBackendConnected){

        struct frontend_override_data data;
        data.enable = 3;    //roll and pitch are attitude, yaw is attitude angle
        data.time = 0;
        data.throttle = currMixedSetpoint.throttle;
        data.roll = currMixedSetpoint.roll;
        data.pitch = currMixedSetpoint.pitch;
        data.yaw = currMixedSetpoint.yaw;
        
        //qInfo() << "send mixed setpoint" << "roll:" << data.roll << " pitch:" << data.pitch;


        //send setpoint
        if(frontend_setoutputoverride(conn, &data) != 0){
            qInfo() << "error sending mixed att setpoint";
        }
    }
}

void CrazyflieWorker::getParamValue(QString paramName){
    struct toc_info info = paramNameLookup(paramName);

    struct frontend_param_data data;
    data.block = 0;
    data.param = info.id;
    data.value = info.value;

    if(frontend_getparam(conn, &data) == 0){
        emit(gotParamValue(paramName, data.value));
    }
}

void CrazyflieWorker::setParamValue(QString paramName, double value) {
    struct toc_info info = paramNameLookup(paramName);

    struct frontend_param_data data;
    data.block = 0;
    data.param = info.id;
    data.value = (float) value;
    
    if(frontend_setparam(conn, &data) != 0){
        //emit(gotParamValue(paramName, data.value));
        qInfo() << "setParamError";
    }
}

struct toc_info CrazyflieWorker::paramNameLookup(QString paramName){
    struct toc_info paramData = paramIds.value(paramName);
    return paramData;
}

std::string CrazyflieWorker::getLogFile(int type) {
    struct frontend_getlogfile_data logfile;
    char fname[256];
    logfile.command = type;
    logfile.name = fname;
    if(frontend_getlogfile(conn, &logfile)) {
        return "";
    }
    std::string result(logfile.name, strlen(logfile.name));
    return result;
}

void CrazyflieWorker::loadParamIds() {
    std::string filename = getLogFile(1);
    std::ifstream paramTOCFile;
    paramTOCFile.open(filename.c_str());
    std::string line;
    //header and top line
    std::getline(paramTOCFile, line);
    std::getline(paramTOCFile, line);
    //actual values;
    while(std::getline(paramTOCFile, line)) {
        struct toc_info values;
        std::stringstream ss(line);
        std::string temp_str;
        for(int i = 0; i < 4; i++) {
            std::getline(ss, temp_str, '\t');
            if(i == 0) {
                values.id = (int16_t) stoi(temp_str);
            }
            else if(i == 1) {
                values.dataType = (int8_t) stoi(temp_str);
            }
            else if(i == 2) {
                values.strGroup = temp_str;
            }
            else if(i == 3) {
                values.strName = temp_str;
            }
        }
        values.value = 0;
        int gFound = findParamGroup(values.strGroup);
        if(gFound == -1) {
            struct toc_group groupx;
            groupx.groupName = values.strGroup;
            groupx.entryNames.push_back(values.strName);
            paramGroups.push_back(groupx);
        }
        else {
            paramGroups.at(gFound).entryNames.push_back(values.strName);
        }

        std::string identifier = values.strGroup + "." + values.strName;
        QString Qidentifier(identifier.c_str());
        paramIds.insert(Qidentifier, values);
    }
    paramTOCFile.close();
}

void CrazyflieWorker::loadLogIds() {
    std::string filename = getLogFile(2);
    std::ifstream logTOCFile;
    logTOCFile.open(filename.c_str());
    std::string line;
    //top line
    std::getline(logTOCFile, line);
    std::getline(logTOCFile, line);
    int maxId = -1;
    //actual values;
    while(std::getline(logTOCFile, line)) {
        struct toc_info values;
        std::stringstream ss(line);
        std::string temp_str;
        for(int i = 0; i < 4; i++) {
            std::getline(ss, temp_str, '\t');
            if(i == 0) {
                values.id = (int16_t) stoi(temp_str);
                if(values.id > maxId) {
                    maxId = values.id;
                }
            }
            else if(i == 1) {
                values.dataType = (int8_t) stoi(temp_str);
            }
            else if(i == 2) {
                values.strGroup = temp_str;
            }
            else if(i == 3) {
                values.strName = temp_str;
            }
        }
        values.value = 0;
        int gFound = findLogGroup(values.strGroup);
        if(gFound == -1) {
            struct toc_group groupx;
            groupx.groupName = values.strGroup;
            groupx.entryNames.push_back(values.strName);
            logGroups.push_back(groupx);
        }
        else {
            logGroups.at(gFound).entryNames.push_back(values.strName);;
        }

        std::string identifier = values.strGroup + "." + values.strName;
        QString Qidentifier(identifier.c_str());
        logIds.insert(Qidentifier, values);
    }
    logTOCFile.close();
    struct toc_group testGroup;
    testGroup.groupName = "MicroCARTTest";
    testGroup.entryNames.push_back("stand");
    logGroups.push_back(testGroup);
    struct toc_info testStand;
    testStand.id = maxId+1;
    testStand.dataType = 9;
    testStand.strGroup = "MicroCARTTest";
    testStand.strName = "stand";
    std::string Midentifier = testStand.strGroup + "." + testStand.strName;
    QString MQidentifier(Midentifier.c_str());
    logIds.insert(MQidentifier, testStand);
}

int CrazyflieWorker::findParamGroup(std::string gName) {
    int result = -1;
    for(int i = 0; i < paramGroups.size(); i++) {
        if(!paramGroups.at(i).groupName.compare(gName)) {
            result = i;
            break;
        }
    }
    return result;
}

int CrazyflieWorker::findLogGroup(std::string gName) {
    int result = -1;
    for(int i = 0; i < logGroups.size(); i++) {
        if(!logGroups.at(i).groupName.compare(gName)) {
            result = i;
            break;
        }
    }
    return result;
}

void CrazyflieWorker::getGroupEntries(int box, QString gName) {
    std::string name = gName.toStdString();
    int index = findParamGroup(name);
    std::vector<std::string> entries = paramGroups.at(index).entryNames;
    QStringList entryList;
    for(int i = 0; i < entries.size(); i++) {
        QString cur(entries.at(i).c_str());
        entryList.append(cur);
    }
    emit(gotGroupEntries(box, entryList));
}

void CrazyflieWorker::frontendLogBlockCommand(int8_t command, int8_t id) {
    struct frontend_logblockcommand_data data;
    data.command = command;
    data.id = id;
    if(frontend_logblockcommand(conn, &data)) {
        qInfo() << "error refreshing log blocks";
        return;
    }
    if(command == 0) {
        logBlocks.clear();
    }
    else if(command == 1) {
        logBlocks.clear();
        loadLogBlocks();
    }
    else if(command == 3) {
        removeLogBlock(id);
    }
    else if(command == 4) {
        enableLogBlock(id, true);
    }
    else if(command == 5) {
        enableLogBlock(id, false);
    }
    else if(command == 6) {
        return;
    }
    else if(command == 7) {
        return;
    }
    else if(command == 8) {
        return;
    }
    else if(command == 9) {
        return;
    }
    // for(int i = 0; i < logBlocks.size(); i++) {
    //     QString blN(logBlocks.at(i).blockName.c_str());
    //     qInfo() << logBlocks.at(i).id << ": " << blN << "enabled: " << logBlocks.at(i).isEnabled;
    // }
    usleep(100000);
    logVarsInHeader();
    QStringList logBlockNames = getLogBlockNames();
    emit(changeLogBlock_comboBox(logBlockNames));
    QStringList activeVars;
    for(int i = 0; i < logBlocks.size(); i++) {
        if(logBlocks.at(i).isEnabled) {
            for(int j = 0; j < logBlocks.at(i).variableNames.size(); j++) {
                QString fullIdentifier(logBlocks.at(i).variableNames.at(j).strGroup.c_str());
                QString varname(logBlocks.at(i).variableNames.at(j).strName.c_str());
                fullIdentifier = fullIdentifier + "." + varname;
                if(isInHeaderVars(fullIdentifier.toStdString())) {
                    activeVars.append(fullIdentifier);
                }
            }
        }
    }
    emit(gotActiveLogVariables(activeVars));
}

bool CrazyflieWorker::isInHeaderVars(std::string name) {
    for(int i = 0; i < this->varsInHeader.size(); i++) {
        if(!name.compare(this->varsInHeader.at(i))) {
            return true;
        }
    }
    return false;
}

void CrazyflieWorker::removeLogBlock(int id) {
    for(int i = 0; i < logBlocks.size(); i++) {
        if(id == logBlocks.at(i).id) {
            logBlocks.erase(logBlocks.begin() + i);
            return;
        }
    }
}

void CrazyflieWorker::enableLogBlock(int id, bool isEnabled) {
    for(int i = 0; i < logBlocks.size(); i++) {
        if(id == logBlocks.at(i).id) {
            logBlocks.at(i).isEnabled = isEnabled;
            return;
        }
    }
}

void CrazyflieWorker::loadLogBlocks() {
    std::ifstream blockFile;
    QString filename(QDir::currentPath() + "/../loggingBlocks.txt");
	blockFile.open(filename.toStdString());
	std::string line = "";
	//0 if not readingBlock 1 is START BLOCK and so on
	int blockLinesRead = 0;
	struct logBlock cur;
    std::string entryName;
    int maxId = -1;

	while(getline(blockFile, line)) {
		if(!line.compare("START BLOCK")) {
			blockLinesRead = 1;
			continue;
		}
		else if(!line.compare("END BLOCK")) {
			cur.isEnabled = true;
            if(cur.id > maxId) {
                maxId = cur.id;
            }
            logBlocks.push_back(cur);
			blockLinesRead = 0;
			cur.id = -1;
			cur.blockName = "";
            cur.isEnabled = false;
            cur.variableNames.clear();
			entryName = "";
			continue;
		}
		else if(blockLinesRead == 1) {
			try {
				cur.id = std::stoi(line);
				bool found = logBlockWithID(cur.id);
				if(found) {
					blockLinesRead = 0;
					cur.id = -1;
					cur.blockName = "";
					entryName = "";
                    cur.isEnabled = false;
                    cur.variableNames.clear();
					continue;
				}
			}
			catch(std::invalid_argument& e) {
				std::cout << "Error: invalid ID. Skipping Block" << std::endl;
				blockLinesRead = 0;
				continue;
			}
			blockLinesRead++;
			continue;
		}
		else if(blockLinesRead == 2) {
			cur.blockName = line;
			blockLinesRead++;
			continue;
		}
		else if(blockLinesRead == 3) {
			try {
				int curFreq = std::stoi(line);
			}
			catch(std::invalid_argument& e) {
				std::cout << "Error: invalid frequency. Skipping Block" << std::endl;
				blockLinesRead = 0;
                cur.id = -1;
				cur.blockName = "";
				entryName = "";
                cur.isEnabled = false;
                cur.variableNames.clear();
				continue;
			}
			blockLinesRead++;
			//logBlocks.push_back(cur);
			continue;
		}
		else if(blockLinesRead > 3) {
			entryName = line;
            struct toc_info entryInfo = tocElementForName(entryName);
			cur.variableNames.push_back(entryInfo);
			blockLinesRead++;
			continue;
		}
		else {
			continue;
		}
	}
	blockFile.close();
    struct logBlock stand;
    stand.id = maxId+1;
    stand.blockName = "MicroCART";
    stand.isEnabled = true;
    struct toc_info standInfo = tocElementForName("MicroCARTTest.stand");
	stand.variableNames.push_back(standInfo);
    logBlocks.push_back(stand);
}

void CrazyflieWorker::findLogBlockID(QString name, int8_t command) {
    std::string lbName = name.toStdString();
    int id = -1;
    for(int i = 0; i < logBlocks.size(); i++) {
        if(!lbName.compare(logBlocks.at(i).blockName)) {
            id = i;
            break;
        }
    }
    if(id == -1) {
        return;
    }
    emit(gotLogBlockID(id, command));
}

bool CrazyflieWorker::logBlockWithID(int id) {
    for(int i = 0; i < logBlocks.size(); i++) {
        if(id == logBlocks.at(i).id) {
            return true;
        }
    }
    return false;
}

QStringList CrazyflieWorker::getLogBlockNames() {
    QStringList names;
    for(int i =0; i < logBlocks.size(); i++) {
        names.append(logBlocks.at(i).blockName.c_str());
    }
    return names;
}

struct toc_info CrazyflieWorker::tocElementForName(std::string name) {
    QString qName(name.c_str());
    struct toc_info element = logIds.value(qName);
    return element;
}

void CrazyflieWorker::logBlockBoxChanged(QString text) {
    bool enabled = false;
    for(int i = 0; i < logBlocks.size(); i++) {
        if(!logBlocks.at(i).blockName.compare(text.toStdString())) {
            enabled = logBlocks.at(i).isEnabled;
            break;
        }
    }
    emit(gotBlockisEnabled(enabled));
}

bool compare_id(const struct logBlock& first, const struct logBlock& second) {
	return (first.id < second.id);
}

// std::vector<int8_t> CrazyflieWorker::getLogColumns(QString log1, QString log2, QString log3, QString log4, QString log5) {
//     std::vector<int8_t> columns(5, -1);
//     int column = 1;
//     std::sort(logBlocks.begin(), logBlocks.end(), compare_id);
//     std::string element;
//     for (int i = 0; i < logBlocks.size(); i++) {
// 		struct logBlock lbCurrent = logBlocks.at(i);
// 		if(lbCurrent.isEnabled) {
// 			for(int j = 0; j < lbCurrent.variableNames.size(); j++) {
// 				struct toc_info teCurrent = lbCurrent.variableNames.at(j);
// 				element = teCurrent.strGroup + "." + teCurrent.strName;
// 				if(!element.compare(log1.toStdString())) {
//                     columns[0] = column;
//                 }
//                 else if(!element.compare(log2.toStdString())) {
//                     columns[1] = column;
//                 }
//                 else if(!element.compare(log3.toStdString())) {
//                     columns[2] = column;
//                 }
//                 else if(!element.compare(log4.toStdString())) {
//                     columns[3] = column;
//                 }
//                 else if(!element.compare(log5.toStdString())) {
//                     columns[4] = column;
//                 }
//                 column++;
// 			}
// 		}			
// 	}
//     return columns;
// }

std::vector<int8_t> CrazyflieWorker::getLogColumns(QString log1, QString log2, QString log3, QString log4, QString log5) {
    std::vector<int8_t> columns(5, -1);
    std::vector<std::string> tokens;
    char fname[256];
    frontend_getlogfile_data data;
    data.command = 3;
    data.name = fname;
    int column = 0;
    if(frontend_getlogfile(conn, &data)) {
        return columns;
    }
    std::string header(data.name);
    std::stringstream ss(header);
    std::string temp;
    std::getline(ss, temp, ',');

    while(std::getline(ss, temp, ',')) {
        tokens.push_back(temp);
    }

    if(log1.toStdString().compare("Logging Variable 1")) {
        column = findColumn(tokens, log1);
        columns[0] = column;
    }

    if(log2.toStdString().compare("Logging Variable 2")) {
        column = findColumn(tokens, log2);
        columns[1] = column;
    }

    if(log3.toStdString().compare("Logging Variable 3")) {
        column = findColumn(tokens, log3);
        columns[2] = column;
    }

    if(log4.toStdString().compare("Logging Variable 4")) {
        column = findColumn(tokens, log4);
        columns[3] = column;
    }

    if(log5.toStdString().compare("Logging Variable 5")) {
        column = findColumn(tokens, log5);
        columns[4] = column;
    }
    
    return columns;
}

int8_t CrazyflieWorker::findColumn(std::vector<std::string> tokens, QString logVar) {
    if(!logVar.toStdString().compare("MicroCARTTest.stand")) {
        return tokens.size() - 1;
    }
    for(int8_t i = 0; i < (int8_t) tokens.size(); i++) {
        if(!tokens.at(i).compare(logVar.toStdString())) {
            return i;
        }
    }
    return -1;
}

void CrazyflieWorker::logVarsInHeader() {
    std::vector<std::string> tokens;
    char fname[256];
    frontend_getlogfile_data data;
    data.command = 3;
    data.name = fname;
    if(frontend_getlogfile(conn, &data)) {
        this->varsInHeader.clear();
        return;
    }
    std::string header(data.name);
    std::stringstream ss(header);
    std::string temp;
    std::getline(ss, temp, ',');
    std::getline(ss, temp, ',');

    while(std::getline(ss, temp, ',')) {
        tokens.push_back(temp);
    }
    this->varsInHeader = tokens;
}
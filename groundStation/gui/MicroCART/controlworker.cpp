#include "controlworker.h"
#include "frontend_nodes.h"
#include "frontend_param.h"
#include "frontend_source.h"
#include "graph_blocks.h"
#include "frontend_output.h"
#include <QProcess>
#include <QDebug>
#include <err.h>

ControlWorker::ControlWorker(QObject *parent) :
    QObject(parent), conn(NULL), nodeIdCache()
{

}

ControlWorker::~ControlWorker()
{
    disconnectBackend();
}

/**
 * @brief ControlWorker::connectBackend
 * Command the worker to connect to the backend
 * will set the private class struct, conn, once done.
 * The connected() signal is emitted once complete.
 */
void ControlWorker::connectBackend()
{
    if (conn == NULL) {
        conn = ucart_backendConnect();
        emit (connected());
    } else {
        qInfo() << "Attempted to connect control worker when already connected!";
    }
}

/**
 * @brief ControlWorker::disconnectBackend
 * Command the worker to disconnect from the backend,
 * sets the conn struct to null and emits the
 * disconnected() signal once complete
 */
void ControlWorker::disconnectBackend()
{
    if (conn) {
         ucart_backendDisconnect(conn);
         conn = NULL;
         emit (disconnected());
    }
}

/**
 * @brief ControlWorker::getNodes
 * Get the nodes from the quad to display
 */
void ControlWorker::getNodes()
{
    if (conn) {
        frontend_node_data * nd = NULL;
        size_t num_nodes = 0;
        frontend_getnodes(conn, &nd, &num_nodes);
        QStringList nodes;
        QStringList const_block_nodes;
        for (size_t i = 0; i < num_nodes; i++) {
            nodes.append(QString(nd[i].name));
            if ((nd[i].type == BLOCK_CONSTANT) || (nd[i].type == BLOCK_ADD)) {
                const_block_nodes.append(nd[i].name);
            }
        }
        emit(gotNodes(nodes));
        emit(gotConstantBlocks(const_block_nodes));

        /* Create a graph for rendering.
         *
         * This leaks memory (NOT MY FAULT!) because there isn't a function to free
         * a graph created with create_graph().
         */
        computation_graph * cgraph = create_graph();

        /* Add nodes */
        for (size_t i = 0; i < num_nodes; i++) {
            graph_add_node_id(cgraph, nd[i].block, nd[i].name, blockDefs[nd[i].type]);
        }

        /* Set sources */
        for (size_t i = 0; i < num_nodes; i++) {
            for (ssize_t j = 0; j < blockDefs[nd[i].type]->n_inputs; j++) {
                frontend_source_data sd;
                sd.dst_block = nd[i].block;
                sd.dst_input = j;
                sd.src_block = 0;
                sd.src_output = 0;
                frontend_getsource(conn, &sd);
                graph_set_source(cgraph, sd.dst_block, sd.dst_input, sd.src_block, sd.src_output);
            }
        }

        /* Set params */
        for (size_t i = 0; i < num_nodes; i++) {
            for (ssize_t j = 0; j < blockDefs[nd[i].type]->n_params; j++) {
                frontend_param_data pd;
                pd.block = nd[i].block;
                pd.param = j;
                pd.value = 0;
                frontend_getparam(conn, &pd);
                graph_set_param_val(cgraph, pd.block, pd.param, pd.value);
            }
        }

        /* This is a massive hack... */
        /* Could be done with popen, but fuck it */
        FILE * dotfile = fopen("/tmp/ucart-tmp-graph.dot", "w");
        if (dotfile) {
            export_dot(cgraph, dotfile, 0);
            fclose(dotfile);
            if (int dot_exit = QProcess::execute("dot /tmp/ucart-tmp-graph.dot -Tpng -o /tmp/ucart-tmp-graph.png")) {
                warnx("dot returned nonzero value (%d)", dot_exit);
            } else {
                emit(graphRendered(QString("/tmp/ucart-tmp-graph.png")));
            }
        } else {
            warn("fopen (/tmp/ucart-tmp-graph.dot)");
        }

        nodeIdCache.clear();
        for (size_t i = 0; i < num_nodes; i++) {
            nodeIdCache[nd[i].name] = nd[i];
        }

        /* And here's where I'd put a call to free_graph(), IF I HAD ONE! */
        frontend_free_node_data(nd, num_nodes);
    }
}

/**
 * Return a node by its ID string
 * @brief ControlWorker::getNodeId
 * @param node
 * @return
 */
frontend_node_data ControlWorker::getNodeId(QString node)
{
    if (nodeIdCache.contains(node)) {
        return nodeIdCache[node];
    } else {
        frontend_node_data nd_invalid;
        nd_invalid.block = -1;
        nd_invalid.name = NULL;
        nd_invalid.type = -1;
        return nd_invalid;
    }
}


/**
 * get the parameters of the given node in the control graph.
 * Result passed back to the main thread through a signal
 * @brief ControlWorker::getParams
 * @param node
 */
void ControlWorker::getParams(QString node)
{
    if (conn) {
        frontend_node_data nd = getNodeId(node);
        if (nd.block == -1) {
            return;
        }

        QStringList params;

        /* Get type definition */
        const struct graph_node_type * type = blockDefs[nd.type];
        /* Iterate through param names to append, and then emit signal */
        for (ssize_t j = 0; j < type->n_params; j++) {
            params.append(QString(type->param_names[j]));
        }
        //pass the data back to the main thread
        emit(gotParams(params));
    }
}


/**
 * Get a specific parameter of a specific node in the control graph.
 * Returned through a signal that must be connected to.
 * @brief ControlWorker::getParamValue
 * @param node
 * @param param
 */
void ControlWorker::getParamValue(QString node, QString param)
{
    if (conn) {
        frontend_node_data nd = getNodeId(node);
        if (nd.block == -1) {
            return;
        }

        frontend_param_data pd;
        pd.block = nd.block;

        /* Get the type definition, then iterate through to find the param */
        const struct graph_node_type * type = blockDefs[nd.type];
        for (ssize_t j = 0; j < type->n_params; j++) {
            /* Found param */
            if (QString(type->param_names[j]) == param) {
                /* Set pd.param and finish the job */
                pd.param = j;
                frontend_getparam(conn, &pd);
                emit(gotParamValue(node, param, pd.value));
            }
        }
    }
}

/**
 * Retrieve a node's output, return through a signal that must be connected to
 * @brief ControlWorker::getNodeOutput
 * @param node
 */
void ControlWorker::getNodeOutput(QString node)
{
    if (conn) {
        frontend_node_data nd = getNodeId(node);
        if (nd.block == -1) {
            return;
        }

        frontend_output_data od;
        od.block = nd.block;

        od.output = 0; // TODO: Get rid of this assumption

        frontend_getoutput(conn, &od);

        emit(gotNodeOutput(node, od.value));
    }
}

/**
 * Set a parameter value of a specifc node in the control graph
 * @brief ControlWorker::setParamValue
 * @param node
 * @param param
 * @param value
 */
void ControlWorker::setParamValue(QString node, QString param, float value)
{
    if (conn) {
        frontend_node_data nd = getNodeId(node);
        if (nd.block == -1) {
            return;
        }

        frontend_param_data pd;
        pd.block = nd.block;

        const struct graph_node_type * type = blockDefs[nd.type];
        for (ssize_t j = 0; j < type->n_params; j++) {
            if (QString(type->param_names[j]) == param) {
                pd.param = j;
                pd.value = value;
                frontend_setparam(conn, &pd);
                emit(paramSet(node, param));
            }
        }
    }
}

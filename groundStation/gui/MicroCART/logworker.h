#ifndef LOGWORKER_H
#define LOGWORKER_H

#include <QObject>
#include <fstream>
#include <sstream>
#include <vector>
#include <QStringList>
#include "frontend_common.h"
#include "frontend_param.h"
#include "frontend_override.h"
#include "frontend_logfile.h"
#include "setpoint.h"
#include <QDebug>


/**
 * @brief The crazyflieworker class
 * This class handles communication for the crazyflie,
 * reading log values and setting parameter values
 */
class LogWorker : public QObject
{
    Q_OBJECT
public:
    explicit LogWorker(QObject *parent = nullptr);
    void stopLogging();
    ~LogWorker();
    

signals:
    void gotLogData(QVector<QVector<double>> x, QVector<QVector<double>> y, bool lastSet);
    void logBlockOutOfSync();
    void connected();
    void disconnected();


public slots:
    void connectBackend();
    void disconnectBackend();
    void openLogFile(QString filename);
    void startLogging();

private:
    struct backend_conn * conn;
    std::ifstream logFile;
    std::string logFileName;


public:
    bool keepLogging = false;
    std::vector<int8_t> logColumns;
};

#endif // LOGWORKER_H

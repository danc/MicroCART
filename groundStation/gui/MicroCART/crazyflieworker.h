#ifndef CRAZYFLIEWORKER_H
#define CRAZYFLIEWORKER_H

#include <QObject>
#include <fstream>
#include <sstream>
#include <vector>
#include <QStringList>
#include "frontend_common.h"
#include "frontend_param.h"
#include "frontend_override.h"
#include "frontend_logfile.h"
#include "setpoint.h"
#include <QDebug>

struct toc_info {
    int16_t id;
    int8_t dataType;
    std::string strGroup;
    std::string strName;
    float value;
};

struct toc_group {
    std::string groupName;
    std::vector<std::string> entryNames;
};

struct logBlock {
    std::string blockName;
    int id;
    std::vector<struct toc_info> variableNames;
    bool isEnabled;
};

/**
 * @brief The crazyflieworker class
 * This class handles communication for the crazyflie,
 * reading log values and setting parameter values
 */
class CrazyflieWorker : public QObject
{
    Q_OBJECT
public:
    explicit CrazyflieWorker(QObject *parent = nullptr);
    ~CrazyflieWorker();
    

signals:
    void gotParamValue(QString paramName, float value);
    void gotParamGroups(QStringList paramGroupList);
    void gotLogVariables(QStringList logVariables);
    void gotGroupEntries(int box, QStringList groupEntries);
    void gotLogBlockID(int id, int8_t command);
    void changeLogBlock_comboBox(QStringList logBlockNames);
    void gotBlockisEnabled(bool isEnabled);
    void gotActiveLogVariables(QStringList activeVars);
    void gotLogFile(QString filename);
    void connected();
    void disconnected();


public slots:
    void connectBackend();
    void disconnectBackend();
    void sendAttSetpoint();
    void sendAttRateSetpoint();
    void sendMixedAttSetpoint();
    void getParamValue(QString paramName);
    void setCurrAttSetpoint(float roll, float pitch, float yaw, float throttle);
    void setCurrAttRateSetpoint(float rollRate, float pitchRate, float yawRate, float throttleRate);
    void setCurrMixedSetpoint(float roll, float pitch, float yawRate, float throttle);
    void setParamValue(QString paramName, double value);
    void getGroupEntries(int box, QString gName);
    void frontendLogBlockCommand(int8_t command, int8_t id);
    void findLogBlockID(QString name, int8_t command);
    void logBlockBoxChanged(QString text);

private:
    struct backend_conn * conn;
    Setpoint currAttitudeSetpoint;
    Setpoint currAttitudeRateSetpoint;
    Setpoint currMixedSetpoint;
    int8_t findColumn(std::vector<std::string> tokens, QString logVar);
    void loadParamIds();
    void loadLogIds();
    void removeLogBlock(int id);
    void enableLogBlock(int id, bool isEnabled);
    void loadLogBlocks();
    bool logBlockWithID(int id);
    struct toc_info tocElementForName(std::string name); 
    QStringList getLogBlockNames();
    int findParamGroup(std::string gName);
    int findLogGroup(std::string gName);
    struct toc_info paramNameLookup(QString paramName);
    QMap<QString, struct toc_info> paramIds;
    QMap<QString, struct toc_info> logIds;
    std::vector<struct toc_group> paramGroups;
    std::vector<struct toc_group> logGroups;
    std::vector<struct logBlock> logBlocks;
    std::vector<std::string> varsInHeader;
    bool isBackendConnected = false;
    void logVarsInHeader();
    bool isInHeaderVars(std::string name);

public:
    std::vector<int8_t> getLogColumns(QString log1, QString log2, QString log3, QString log4, QString log5);
    std::string getLogFile(int type);
};

#endif // CRAZYFLIEWORKER_H

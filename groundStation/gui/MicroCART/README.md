# Ground Station GUI

In order to run the GUI on a windows machine you will need a remote display protocol such as X Window System.

From /MicroCART/groundStation/gui/MicroCART
```bash
    make
```

If you are this on running on windows
```bash
    export DISPLAY=:0
```

To run, simply run MicroCART
```bash
    ./MicroCART
```
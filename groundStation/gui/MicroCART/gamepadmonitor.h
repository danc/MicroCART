#ifndef GAMEPADMONITOR_H
#define GAMEPADMONITOR_H

#include <QObject>
#include <QtGamepad/QGamepad>


#define DEFAULT_ROLL_SCALE 30   //deg
#define DEFAULT_PITCH_SCALE 30  //deg
#define DEFAULT_YAW_SCALE 45    //deg / s
#define DEFAULT_THRUST_SCALE 60000


class GamepadMonitor : public QObject
{
    Q_OBJECT
public:
    explicit GamepadMonitor(QObject *parent = nullptr);
    ~GamepadMonitor();

    bool isGamepadConnected();

    float getRoll();
    float getYaw();
    float getPitch();
    float getThrust();
    void configureAxis(QString axisName);
    void resetConfig();

    void setRollScale(float scale);
    void setPitchScale(float scale);
    void setYawScale(float scale);
    void setThrustScale(float scale);

    float getRollScale();
    float getPitchScale();
    float getYawScale();
    float getThrustScale();


private:
    QGamepad *m_gamepad;
    QGamepadManager *m_gamepadManager;

    float rollScale;
    float pitchScale;
    float yawScale;
    float thrustScale;

signals:
    void gamepadConnected();
    void gamepadDisconnected();
public slots:
    void gamepadConnectedHandler(int deviceId);
    void gamepadDisconnectedHandler(int deviceId);
};

#endif // GAMEPADMONITOR_H

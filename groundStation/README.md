# groundStation

## Make Process

First, if submodules were not recursively added through git. Run this command if you have made any attempt to make vrpn manually.
run
```bash
    git submodule update --init --recursive
```

Now that you have all of the files necessary.

cd into the groundstation folder.
```bash
	cd groundStation
	make vrpn
	make
```

run the program with privileges
```bash
	./BackEnd
```

## Changing Defaults
If you wish to change the way the backend communicates to the quad and vice versa, look at `src/config.h`.
This provides a list of environment variables which you can set and use for your computer or time of use.

## Using

First, if you are using any adapters they must be started before the backend, so do so now. 
By default you will not be using adapters and you can continue if you do not intend to use
this feature.

Next, the backend daemon must be running. Run the backend with
```bash
    ./BackEnd
```
**Note:** The backend requires root for bluetooth, but can run as a normal user with TCP, as long as the
socket path is writable by the user.

Once the backend is running, various CLI tools can be used to view the state of the
quad and send commands. Each of these tools is given as the first argument
to the CLI program, for example, to get a node output from the quad, use `cli getoutput`. Note that
the backend socket environment variable must be set to the same value as it
was for the backend. The CLI program also supports busybox-style symbolic links.
For example, if there is a symlink named `getoutput` that points to the `cli` binary,
running the `getoutput` program will effectively run `cli getoutput`.

The names of the binaries is subject to change.

For a list of cli commands and more in depth usage explanations, use the `--help` flag.
```bash
    ./Cli --help
```

For help with the specific cli command you are running, use the `--help` flag once again.
```bash
    ./Cli getoutput --help
```

### Example
In one terminal or screen, run the backend:

```bash
    ./BackEnd
```

This will activate the quad and the backend, and the backend will be available for
connections from the frontend tools. One useful tool is the `getnodes`. In another
terminal window, run

```bash
    ./Cli getnodes
```

or alternatively with symlinks

```bash
    ./getnodes
```

This will fetch the `block_id`, `type_id` and name of every node in the current graph

You can run any number of any combination of frontend tools at the same time.
If you find it helpful, with this setup you can create a bash script that simply
runs the same commands. This has been found to be of use when we are tuning.

There are a couple of already written bash scripts in the `scripts/` folder.

## Documentation

 - [Getting Started](./documentation/getting_started.md)
 - [Packet Information](./documentation/packets.md)
 - [Adapters](adapters/README.md)
 - [WiFi Access Point](wifiap/README.md)
 - [Manual Assist Mode](./src/manual_assist/README.md)
#!/bin/bash

#SETTINGS
IP=192.168.0.1





if [[ $EUID -ne 0 ]]; then
   printf "\nThis script must be run as root to run the hostapd service\n"
   printf "\tRUN AS ROOT!\n"
   exit 1
fi

printf "checking for and stopping running hostapd...\n"
if (( $(ps -ef | grep -v grep | grep hostapd | wc -l) > 0 )); then
    systemctl stop hostapd.service
    if [ $? -eq 0 ]; then
        printf "\t OK!\n"
    else
        printf "\t FAIL!\n"
        printf "\t failed to stop hostapd service may need to restart\n"
        exit 1
    fi
fi

printf "checking for and stopping the dhcpd service...\n"
if (( $(ps -ef | grep -v grep | grep dhcpd | wc -l) > 0 )); then
    systemctl stop dhcpd.service
    if [ $? -eq 0 ]; then
        printf "\t OK!\n"
    else
        printf "\t FAIL!\n"
        printf "\t failed to stop dhcpd service check run sudo systemct start dhcpd.service and check error messages\n"
        exit 1
    fi
fi

printf "setting up wifi radio...\n"
nmcli radio wifi on
if [ $? -eq 0 ]; then
    printf "\t OK!\n"
else
    printf "\t FAIL!\n"
    printf "\t \"nmcli radio wifi on\" command failed\n"
    exit 1
fi
    
printf "hostapd should now be off check to see if wifi is working!\n"

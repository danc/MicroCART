#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   printf "\nThis script must be run as root to move files around in etc and also run the hostapd service\n"
   printf "\tRUN AS ROOT!\n"
   exit 1
fi

printf "checking for hostapd..."

if ! type "hostapd" > /dev/null ; then 
    printf "\n\thostapd not found\n"
    printf "\tINSTALL HOSTAPD PACKAGE!\n"
    exit 1
fi

printf "\t OK!\n"

printf "moving golden_hostpad.conf to /etc/hostapd/hostapd.conf..."

if [ -f /etc/hostapd/hostapd.conf ]; then
    printf "\n\tfile already exists! Please remove the hostapd.conf from the /etc/hostapd/ directory and try again!\n"
    exit 1
fi

if [ ! -f ./golden_hostapd.conf ]; then
    printf "\n\tcannot find the golden_hostapd.conf file in installer script's directory\n"
    printf "\tRESTORE OR CREATE golden_hostapd.conf\n"
    exit 1
fi

cp golden_hostapd.conf /etc/hostapd/hostapd.conf
#config must be owned by root root otherwise we get an SELinux violation
chown root /etc/hostapd/hostapd.conf
chgrp root /etc/hostapd/hostapd.conf

printf "\t OK!\n"

printf "checking for dhcpd..."

if ! type "dhcpd" > /dev/null ; then 
    printf "\n\tdhcpd not found\n"
    printf "\tINSTALL DHCPD PACKAGE!\n"
    exit 1
fi

printf "\t OK!\n"

printf "moving golden_dhcpd.conf to /etc/dhcp/dhcpd.conf..."

if [ -f /etc/dhcp/dhcpd.conf ]; then
    printf "\n\tfile already exists! Please remove the dhcpd.conf from the /etc/dhcpd/ directory and try again!\n"
    exit 1
fi

if [ ! -f ./golden_dhcpd.conf ]; then
    printf "\n\tcannot find the golden_dhcpd.conf file in installer script's directory\n"
    printf "\tRESTORE OR CREATE golden_dhcpd.conf\n"
    exit 1
fi

cp golden_dhcpd.conf /etc/dhcp/dhcpd.conf
#config must be owned by root root otherwise we get an SELinux violation
chown root /etc/dhcp/dhcpd.conf
chgrp root /etc/dhcp/dhcpd.conf

printf "\t OK!\n"


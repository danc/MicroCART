# WiFi Access Point
## Basic Setup
1.  First install the necessary pacakges to the system  
    On Red Hat:  
    
    ```bash
        sudo yum install dhcp hostapd
    ```  
2.  Next navigate to the Microcart/groundStation/wifiap/ directory  

3.  Once you are in the directory make sure you have the necessary files  
     ```bash
        ls
     ```  

4.  The output should look something like the following image.  

    TODO image.  

5.  Next we will want to check for and backup the hostapd.conf file. The installer script will move the golden version of this file to the correct location.  

    ```bash
         ls /etc/hostapd/hostapd.conf
    ```  
    
    If the file exists you will see output that looks  like the following move on to step 6  
    
    ```bash
        /etc/hostapd/hostapd.conf
    ```  
    If you get the following double check that hostapd is installed and if it is move on to step 7  
    
    ```bash
    ls: cannot access '/etc/hostapd/hostapd.conf': No such file or directory
    ```  
    
6.  Make a backup of the file with the following

    ```bash
        sudo mv /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.backup
    ```  
    
7.  Next we will do the same for the dhcpd.conf file. Check that the file exists  

    ```bash
        ls /etc/dhcp/dhcpd.conf
    ```  
    
    If the file exists you will see output that looks  like the following move on to step 8  
    
    ```bash
        /etc/dhcp/dhcpd.conf
    ```  
    
    If you get the following double check that dhcpd is installed and if it is move on to step 9  
    
    ```bash
        ls: cannot access '/etc/dhcp/dhcpd.conf': No such file or directory
    ```  
    
8.  Make a backup of the file with the following

    ```bash
        sudo mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.backup
    ```  
    

9.    Next run the installerScript.sh with sudo rights
    ```bash
        sudo ./installerScript.sh
    ```  

10. You should now see output with all "OK" meaning that the script succeeded.

## How to Use
1.    Run the start script with the following 

    ```bash
        sudo ./startAP.sh
    ```  
    
    You should now see a WiFi access point Microcart.  
    
2.    Make sure when you are done to run the following to stop  

    ```bash
        sudo ./stopAP.sh
    ```  

#!/bin/bash

#SETTINGS
IP=192.168.1.1





if [[ $EUID -ne 0 ]]; then
   printf "\nThis script must be run as root to run the hostapd service\n"
   printf "\tRUN AS ROOT!\n"
   exit 1
fi

printf "checking for hostapd...\n"
if ! type "hostapd" > /dev/null ; then 
    printf "\n\thostapd not found\n"
    printf "\tINSTALL HOSTAPD PACKAGE AND RUN THE INSTALLER SCRIPT\n"
    exit 1
fi
printf "\t OK!\n"

printf "checking for dhcpd...\n"
if ! type "dhcpd" > /dev/null ; then 
    printf "\n\thostapd not found\n"
    printf "\tINSTALL DHCPD PACKAGE AND RUN THE INSTALLER SCRIPT\n"
    exit 1
fi
printf "\t OK!\n"

printf "checking settings...\n"
if [ ! -f /etc/hostapd/hostapd.conf ]; then
    printf "\n\tMISSING /etc/hostapd/hostapd.conf\n\tmodify golden_hostapd.conf and run the installer script\n"
    exit 1
fi
printf "\t OK!\n"

printf "checking settings...\n"
if [ ! -f /etc/dhcp/dhcpd.conf ]; then
    printf "\n\tMISSING /etc/dhcpd/dhcpd.conf\n\tmodify golden_dhcpd.conf and run the install script\n"
    exit 1
fi
printf "\t OK!\n"

#printf "Setting ip to $IP\n"
#ifconfig wlp2s0 $IP
#printf "\t OK!\n"

printf "setting up wifi radio...\n"
nmcli radio wifi off
if [ $? -eq 0 ]; then
    printf "\t OK!\n"
else
    printf "\t FAIL!\n"
    printf "\t \"nmcli radio wifi off\" command failed\n"
    exit 1
fi

sleep 2

printf "setting up unblocking wifi...\n"
rfkill unblock wifi
if [ $? -eq 0 ]; then
    printf "\t OK!\n"
else
    printf "\t FAIL!\n"
    printf "\t \"rfkill unblock wifi\" command failed\n\treverting wifi state\n"
    nmcli radio wifi on
fi

printf "running the hostapd service...\n"
systemctl start hostapd.service
if [ $? -eq 0 ]; then
    printf "\t OK!\n"
else
    printf "\t FAIL!\n"
    printf "\t failed to start hostapd service check run sudo systemctl start hostapd.service and check error messages\n\t reverting wifi state\n"
    nmcli radio wifi on
    exit 1
fi

printf "Setting ip to $IP\n"
ifconfig wlp2s0 $IP
printf "\t OK!\n"

printf "running the dhcpd service...\n"
systemctl start dhcpd.service
if [ $? -eq 0 ]; then
    printf "\t OK!\n"
else
    printf "\t FAIL!\n"
    printf "\t failed to start dhcpd service check run sudo systemctl start dhcpd.service and check error messages\n\t reverting wifi state and stopping hostapd\n"
    systemctl stop hostapd.service
    nmcli radio wifi on
    exit 1
fi
    
printf "hostapd and dhcpd should now be running, see if you can connect!\n"

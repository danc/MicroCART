#! /bin/bash

if [ -z "$1" ]; then
		echo "No argument supplied"
		exit 0
fi
#cd ..


./setsource 35 1 38 0
./setsource 35 2 38 0
./setsource 35 3 38 0

./setsource 35 0 13 0

while true ; do
	./setparam 13 0 $(( $1 + 1 ))
	sleep 0.4
	./setparam 13 0 $(( $1 - 1 ))
	sleep 0.4
done

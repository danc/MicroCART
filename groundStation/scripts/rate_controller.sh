#! /bin/bash


./setparam 18 0 0.045 
#inner loop
./setparam 'roll rate pid' 'kp' 3000
./setparam 'roll rate pid' 'ki' 0
./setparam 'roll rate pid' 'kd' 500
./setparam 'roll rate pid' 'alpha' 0.88

./setparam 'pitch rate pid' 'kp' 3000
./setparam 'pitch rate pid' 'ki' 0
./setparam 'pitch rate pid' 'kd' 500
./setparam 'pitch rate pid' 'alpha' 0.88

./setparam 'roll pid' 'kp' 35
./setparam 'roll pid' 'ki' 0
./setparam 'roll pid' 'kd' 1
./setparam 'roll pid' 'alpha' 0.88

./setparam 'pitch pid' 'kp' 35
./setparam 'pitch pid' 'ki' 0
./setparam 'pitch pid' 'kd' 1
./setparam 'pitch pid' 'alpha' 0.88

./setparam "X pos PID" "kp" -0.55
./setparam "X pos PID" "ki" -0.0075
./setparam "X pos PID" "kd" -0

./setparam "Y pos PID" "kp" -0.55
./setparam "Y pos PID" "ki" -0.0075
./setparam "Y pos PID" "kd" -0

./setparam "Altitude PID" "kp" -9804
./setparam "Altitude PID" "ki" -817
./setparam "Altitude PID" "kd" -7353
./setparam "Altitude PID" "alpha" 0.88

./setparam "X vel PID" "kp" 0.1
./setparam "X vel PID" "kd" 0.02

./setparam "X vel" "alpha" 0.88

./setparam "Y vel PID" "kp" -0.1
./setparam "Y vel PID" "kd" -0.02

./setparam "y vel" "alpha" 0.88

#!/bin/bash

# Use default filename if it wasn't given at the command line
if [ $# -eq 0 ]; then
    filename="scripts/parameters.txt"
else
    filename=$1
fi

# Define the regular expression used for grep to define accepted line
regex='(lat|long|height|pitch|roll|yaw|pitchv|rollv|yawv) (p|i|d) ([-+]?[0-9]*\.?[0-9]+)'

# the awk program to assemble the correct pieces of the string for a ./Cli call
awk_prog='{print "./Cli setpid" " " "--" $1 " " "-" $2 " " $3}'

# Search, parse, and execute ./Cli commands
grep -E "$regex" $filename | awk "$awk_prog" | bash

#! /bin/bash

./setsource "T trim add" "summand 1" "zero" 0
#./setsource 14 0 46 0
./setparam "Throttle trim" 0 0.35
#./setparam 13 0 0.35

# sleep 2
sleep 2

./setsource "T trim add" "summand 1" "Altitude PID" "Correction"
#./setsource 14 0 8 0
./setparam "Throttle trim" 0 0.5
#./setparam 13 0 0.45
./setparam "Alt Setpoint" 0 -0.88
#./setparam 11 0 -0.5

#! /bin/bash

cut_off="-0.20"
regex='[+-]?[0-9]+\.?[0-9]*$'



alt="$(echo "$(./getparam 'VRPN Alt' 0)" | grep -Eo "$regex")"

while awk 'BEGIN { if ('$alt'>='$cut_off') {exit 1}}'; do
	if (( $(bc <<< "$alt < -0.5") )); then
		alt="$(echo "$(./getparam 'VRPN Alt' 0)" | grep -Eo "$regex")"
		./setparam 'Alt Setpoint' 0 $( bc <<< "$alt + 0.25")
	else
		alt="$(echo "$(./getparam 'VRPN Alt' 0)" | grep -Eo "$regex")"
		./setparam 'Alt Setpoint' 0 $( bc <<< "$alt + 0.35")
	fi
	sleep .1
done

#./setparam "Throttle trim" 0 0
./setparam 'Throttle trim' 0 0
#./setsource "T trim add" "summand 1" "zero" 0
./setsource 'T trim add' 'summand 1' 'zero' 0

#!/bin/bash

./setparam 18 0 0.045
#inner loop
./setparam 'roll rate pid' 'kp' 3000
./setparam 'roll rate pid' 'ki' 0
./setparam 'roll rate pid' 'kd' 500
./setparam 'roll rate pid' 'alpha' 0.88

./setparam 'pitch rate pid' 'kp' 3000
./setparam 'pitch rate pid' 'ki' 0
./setparam 'pitch rate pid' 'kd' 500
./setparam 'pitch rate pid' 'alpha' 0.88

./setparam 'roll pid' 'kp' 50
./setparam 'roll pid' 'ki' 0
./setparam 'roll pid' 'kd' 1
./setparam 'roll pid' 'alpha' 0.88

./setparam 'pitch pid' 'kp' 50
./setparam 'pitch pid' 'ki' 0
./setparam 'pitch pid' 'kd' 1
./setparam 'pitch pid' 'alpha' 0.88

#outer loop
./setparam 'X pos PID' 'kp' -0.015
./setparam 'X pos PID' 'ki' -0
./setparam 'X pos PID' 'kd' -0.25
./setparam 'X pos PID' 'alpha' 0.88

./setparam 'Y pos PID' 'kp' 0.015
./setparam 'Y pos PID' 'ki' 0
./setparam 'Y pos PID' 'kd' 0.25
./setparam 'Y pos PID' 'alpha' 0.88

cd ..
./setparam "y pos pid" "kp" 0.015 
./setparam "y pos pid" "ki" 0.005
./setparam "y pos pid" "kd" 0.03

./setparam "x pos pid" "kp" -0.015
./setparam "x pos pid" "ki" -0.005
./setparam "x pos pid" "kd" -0.03

./setparam "altitude pid" "kp" 9804 
./setparam "altitude pid" "ki" 817
./setparam "altitude pid" "kd" 7353

./setparam "pitch pid" "kp" 4.29
./setparam "pitch pid" "ki" 0
./setparam "pitch pid" "kd" 0

./setparam "roll pid" "kp" 4.29
./setparam "roll pid" "ki" 0
./setparam "roll pid" "kd" 0

./setparam "yaw pid" "kp" 2.6
./setparam "yaw pid" "ki" 0
./setparam "yaw pid" "kd" 0

./setparam "pitch rate pid" "kp" 1138.5
./setparam "pitch rate pid" "ki" 0
./setparam "pitch rate pid" "kd" 0

./setparam "roll rate pid" "kp" 1138.5
./setparam "roll rate pid" "ki" 0
./setparam "roll rate pid" "kd" 0

./setparam "yaw rate pid" "kp" 29700
./setparam "yaw rate pid" "ki" 0
./setparam "yaw rate pid" "kd" 0

#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>

#include "cli.h"
#include "cli_logfile.h"
#include "frontend_logfile.h"

/**
 * Prints the current trackable set in the backend.
 */
int cli_getlogfile(struct backend_conn * conn, int argc, char ** argv) {
    char fname[256];
    struct frontend_getlogfile_data values;
    values.name = fname;
    int needHelp = 0;
    
    if ((needHelp = help_check(argc, argv))) {
        printf("getlogfile gets the name of the log file being used for the current task\n"
               "0: data log file\n"
               "1: param id log file\n"
               "2: logging variable id log file\n");
        printf("Usage Syntax : \n\t./Cli getlogfile <id>\n");
        return 0;
    }

    if (argc < 1) {
        printf("Incorrect Usage, run './cli getlogfile --help' for correct usage.\n");
        return 1;
    }

    int command;
	sscanf(argv[1], "%d", &command); values.command = command;
    
    if (frontend_getlogfile(conn, &values)) {
        return 1;
    }
    
    if(command == 0) {
        printf("Data Logfile: %s\n", values.name);
    }
    else if(command == 1) {
        printf("Param Logfile: %s\n", values.name);
    }
    else if(command == 2) {
        printf("Log Variables Logfile: %s\n", values.name);
    }
    else if(command == 3) {
        printf("Active header: %s\n", values.name);
    }
    else if(command == 4) {
        printf("Test Stand Logging: %s\n", values.name);
    }
    else {
        printf("Error: incorrect command value");
    }
    
    return 0;
}

/**
 * Prints the current trackable set in the backend.
 */
int cli_logblockcommand(struct backend_conn * conn, int argc, char ** argv) {
    int needHelp = 0;
    struct frontend_logblockcommand_data values;
    
    if ((needHelp = help_check(argc, argv))) {
        printf("there are a few commands that can be used to affect Crazyflie logging\n"
        "0 reset all log blocks, this will disable and empty all log block data\n "
        "1 refresh log blocks, this will reset and then start all log blocks identified in the log block file\n"
        "2 load log blocks, this will add any new log blocks identified in the log block file\n" 
        "3 delete log block, will delete the log block with the given id\n"
        "4 enable log block, this will enable or resume the log block with the given id\n"
        "5 disable log block, this will disable or pause the log block with the given id\n");
        printf("Usage Syntax : \n\t./Cli <command> [id] \n");
        return 0;
    }

    if (argc > 3) {
        printf("Incorrect Usage, run './cli getlogfile --help' for correct usage.\n");
        return 1;
    }

    int command;
    int id;
	sscanf(argv[1], "%d", &command); values.command = command;
    if(command >= 3) {
        if(argc < 3) {
            printf("Incorrect Usage, run './cli getlogfile --help' for correct usage.\n");
        }
        else {
            sscanf(argv[2], "%d", &id); values.id = id;
        }  
    }
    else {
        values.id = -1;
    }
	
    
    if (frontend_logblockcommand(conn, &values)) {
        return 1;
    }

    return 0;
}
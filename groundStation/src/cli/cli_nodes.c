#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include "cli.h"
#include "cli_nodes.h"
#include "frontend_nodes.h"
#include "graph_blocks.h"


int cli_getnodes(struct backend_conn * conn, int argc, char ** argv) {
	size_t num_nodes = 0;
	struct frontend_node_data * node_data = NULL;

	int needHelp = 0;
	
	size_t i, n;


	if ((needHelp = help_check(argc, argv))) {
		printf("getnodes gets the number of nodes along with the block_id, type_id and name of each node\n");
		printf("Usage Syntax : \n\t./Cli getnodes\n");
		printf("Symlink Usage Syntax : \n\t./getnodes\n\n");
		return 0;
	}

	if (argc > 1) {
		printf("Incorrect Usage, run './cli getnodes --help' for correct usage.\n");
		return 1;
	}

	/**
	 * node_data must be NULL,
	 * num_nodes must equal zero.
	 */
	if (frontend_getnodes(conn, &node_data, &num_nodes)) {
		free(node_data);
		return 1;
	}

	printf("----------------------------------------------------------------------------------------------------\n");
	printf("The following %lu Nodes have been found:\n", num_nodes);
	printf("----------------------------------------------------------------------------------------------------\n");
	printf("  BLOCK\tTYPE\tNAME\t\tINPUTS\t\tPARAMS\t\tOUTPUTS\n");
	printf("----------------------------------------------------------------------------------------------------\n");
	for(i = 0; i < num_nodes; ++i) {
		printf("  %3d\t%3d\t%s\t", node_data[i].block, node_data[i].type, node_data[i].name);
		
		const struct graph_node_type * node_definition = blockDefs[node_data[i].type];
		printf(" {");
		for(n = 0; n < (size_t)node_definition->n_inputs; ++n) {
			printf(" %-5s ", node_definition->input_names[n]);
		}
		printf("} ");

		printf(" {");
		for(n = 0; n < (size_t)node_definition->n_params; ++n) {
			printf(" %s ", node_definition->param_names[n]);
		}
		printf("} ");
		
		printf(" {");
		for(n = 0; n < (size_t)node_definition->n_outputs; ++n) {
			printf(" %s ", node_definition->output_names[n]);
		}
		printf("}\n");
	}

	frontend_free_node_data(node_data, num_nodes);	
	return 0;
}

int cli_addnode(struct backend_conn * conn, int argc, char ** argv) {
	struct frontend_node_data node_data;

	int needHelp = 0;
	
	if ((needHelp = help_check(argc, argv))) {
		printf("addnodes adds a node of specified type_id and name to the comp_graph\n");
		printf("Usage Syntax : \n\t./Cli addnode type_id name\n");
		printf("Symlink Usage Syntax : \n\t./addnode type_id name\n\n");
		return 0;
	}

	if (argc < 3) {
		printf("Incorrect Usage, run './cli addnode --help' for correct usage.\n");
		return 1;
	}
	
	if ((node_data.name = (char *)malloc(strlen(argv[2]))) == NULL) {
		return 1;
	}

	node_data.type = atoi(argv[1]);
	strncpy(node_data.name, argv[2], strlen(argv[2]));

	if (frontend_addnode(conn, &node_data)) {
		return 1;
	}

	printf("Node created with block_id : %" PRId16 "\n", node_data.block);
	return 0;
}

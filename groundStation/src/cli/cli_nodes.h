#ifndef  _CLI_NODES_H
#define  _CLI_NODES_H

#include "frontend_nodes.h"

int cli_getnodes(struct backend_conn * conn, int argc, char ** argv);
int cli_addnode(struct backend_conn * conn, int argc, char ** argv);

#endif /* _CLI_NODES_H */
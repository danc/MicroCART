/**
 * CLI functions for the getlogfile.
 */

#ifndef  _CLI_LOGFILE_H
#define  _CLI_LOGFILE_H

#include "frontend_logfile.h"

/**
 * Prints the current log file set in the backend.
 */
int cli_getlogfile(struct backend_conn * conn, int argc, char ** argv);

/**
 * Command that sends what type of log block command that should be carried out
 */
int cli_logblockcommand(struct backend_conn * conn, int argc, char ** argv);

#endif /* _CLI_LOGFILE_H */
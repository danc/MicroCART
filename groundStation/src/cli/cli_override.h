#ifndef  _CLI_OVERRIDE_H
#define  _CLI_OVERRIDE_H

#include "frontend_output.h"

int cli_outputoverride(struct backend_conn * conn, int argc, char ** argv);

#endif /* _CLI_OVERRIDE_H */

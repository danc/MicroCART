#ifndef  _CLI_PARAM_H
#define  _CLI_PARAM_H

#include <ctype.h>
#include <string.h>

#include "frontend_param.h"
#include "frontend_nodes.h"
#include "graph_blocks.h"

int cli_getparam(struct backend_conn * conn, int argc, char ** argv);
int cli_setparam(struct backend_conn * conn, int argc, char ** argv);

#endif /* _CLI_PARAM_H */
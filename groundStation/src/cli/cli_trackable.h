/**
 * CLI functions for the gettrackable and settrackable commands.
 */

#ifndef  _CLI_TRACKABLE_H
#define  _CLI_TRACKABLE_H

/**
 * Prints the current trackable set in the backend.
 */
int cli_gettrackable(struct backend_conn * conn, int argc, char ** argv);

/**
 * Sets a new trackable in the backend. The input trackable must be the name of a
 * trackable within the trackables array set in config.c
 */
int cli_settrackable(struct backend_conn * conn, int argc, char ** argv);

#endif /* _CLI_TRACKABLE_H */

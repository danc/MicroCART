#ifndef  _CLI_OUTPUT_H
#define  _CLI_OUTPUT_H

#include "frontend_output.h"

int cli_getoutput(struct backend_conn * conn, int argc, char ** argv);

#endif /* _CLI_OUTPUT_H */
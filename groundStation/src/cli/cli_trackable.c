#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>

#include "cli.h"
#include "cli_trackable.h"
#include "frontend_trackable.h"

/**
 * Prints the current trackable set in the backend.
 */
int cli_gettrackable(struct backend_conn * conn, int argc, char ** argv) {
    char name[64];
    int needHelp = 0;
    
    if ((needHelp = help_check(argc, argv))) {
        printf("gettrackable gets the name of the trackable being communicated with\n");
        printf("Usage Syntax : \n\t./Cli gettrackable\n");
        printf("Symlink Usage Syntax : \n\t./gettrackable\n\n");
        return 0;
    }

    if (argc < 1) {
        printf("Incorrect Usage, run './cli gettrackable --help' for correct usage.\n");
        return 1;
    }
    
    if (frontend_gettrackable(conn, name)) {
        return 1;
    }

    printf("Trackable - %s\n", name);
    return 0;
}

/**
 * Sets a new trackable in the backend. The input trackable must be the name of a
 * trackable within the trackables array set in config.c
 */
int cli_settrackable(struct backend_conn * conn, int argc, char ** argv) {
    int needHelp = 0;
    
    if ((needHelp = help_check(argc, argv))) {
        printf("settrackable sets the trackable being communicated with in the backend\n");
        printf("Usage Syntax : \n\t./Cli settrackable trackable\n");
        printf("Symlink Usage Syntax : \n\t./settrackable trackable\n\n");
        return 0;
    }

    if (argc < 2) {
        printf("Incorrect Usage, run './Cli settrackable --help' for correct usage.\n");
        return 1;
    }

    int i = 0;

    for (i = 0; i < argc; i++) {
        if (strstr(argv[i], "settrackable") != NULL) {
            break;
        }
    }

    if (i >= argc) {
        printf("Incorrect Usage, run '.cli settrackable --help' for correct usage.\n");
        return 1;
    }

    if (frontend_settrackable(conn, argv[++i])) {
        return 1;
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>

#include "cli.h"
#include "cli_param.h"


int cli_getparam(struct backend_conn * conn, int argc, char ** argv) {
	int needHelp = 0;
	struct frontend_param_data param_data;


	if ((needHelp = help_check(argc, argv))) {
		printf("getparam gets the param_val for a specified block_id and param_id\n");
		printf("Usage Syntax : \n\t./Cli getparam <block_id|'block_name'> <param_id|'param_name'>\n");
		printf("Symlink Usage Syntax : \n\t./getparam <block_id|'block_name'> <param_id|'param_name'>\n\n");
		return 0;
	}

	if (argc < 3) {
		printf("Incorrect Usage, run './cli getparam --help' for correct usage.\n");
		return 1;
	}

	if (convert_to_id(conn, argv, (struct convert_data *)&param_data, PARAM)) {
		return 1;
	}
	
	if (frontend_getparam(conn, &param_data)) {
		return 1;
	}

	printf("BLOCK.PARAM = VAL\n" \
		   "   %2" PRId16 ".%2" PRId16 "   =  %lf\n",
		   param_data.block,
		   param_data.param,
		   param_data.value);
	return 0;
}

int cli_setparam(struct backend_conn * conn, int argc, char ** argv) {
	int needHelp = 0;
	struct frontend_param_data param_data;

	if ((needHelp = help_check(argc, argv))) {
		printf("setparam sets the param_val for a specified block_id and param_id\n");
		printf("Usage Syntax : \n\t./Cli setparam <block_id|'block_name'> <param_id|'param_name'> value\n");
		printf("Symlink Usage Syntax : \n\t./setparam <block_id|'block_name'> <param_id|'param_name'> value\n\n");
		return 0;
	}

	if (argc < 4) {
		printf("Incorrect Usage, run './cli setparam --help' for correct usage.\n");
		return 1;
	}


	if (convert_to_id(conn, argv, (struct convert_data *)&param_data, PARAM)) {
		return 1;
	}
	
	param_data.value = atof(argv[3]);
	
	if (frontend_setparam(conn, &param_data)) {
		return 1;
	}
	
	return 0;
}

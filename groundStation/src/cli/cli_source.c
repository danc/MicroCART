#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>

#include "cli.h"
#include "cli_source.h"
#include "frontend_source.h"

int cli_getsource(struct backend_conn * conn, int argc, char ** argv) {
	struct frontend_source_data source_data;
	int needHelp = 0;
	
	if ((needHelp = help_check(argc, argv))) {
		printf("getsource gets the src_block_id and src_output_id for a specified dst_block_id and dst_input_id\n");
		printf("Usage Syntax : \n\t./Cli getsource dst_block_id dst_input_id\n");
		printf("Symlink Usage Syntax : \n\t./getsource dst_block_id dst_input_id\n\n");
		return 0;
	}

	if (argc < 3) {
		printf("Incorrect Usage, run './cli getsource --help' for correct usage.\n");
		return 1;
	}
	
	if (convert_to_id(conn, argv, (struct convert_data *)&source_data, INPUT)) {
		return 1;
	}

	if (frontend_getsource(conn, &source_data)) {
		return 1;
	}

	printf("SRC BLOCK.OUTPUT  --->  DST BLOCK.INPUT\n" \
		   "      %" PRId16 ".%" PRId16 "     --->        %" PRId16 ".%" PRId16 "\n",
		source_data.src_block, source_data.src_output,
		source_data.dst_block, source_data.dst_input);
	return 0;
}

int cli_setsource(struct backend_conn * conn, int argc, char ** argv) {
	struct frontend_source_data source_data;
	struct convert_data conv_data;
	int needHelp = 0;
	
	if ((needHelp = help_check(argc, argv))) {
		printf("setsource sets the src_block_id and src_output_id for a specified dst_block_id and dst_input_id\n");
		printf("Usage Syntax : \n\t./Cli setsource dst_block_id dst_input_id src_block_id src_output_id\n");
		printf("Symlink Usage Syntax : \n\t./setsource dst_block_id dst_input_id src_block_id src_output_id\n\n");
		return 0;
	}

	if (argc < 5) {
		printf("Incorrect Usage, run './cli setsource --help' for correct usage.\n");
		return 1;
	}
	
	if (convert_to_id(conn, argv, &conv_data, INPUT)) {
		return 1;
	}
	source_data.dst_block = conv_data.val_1;
	source_data.dst_input = conv_data.val_2;

	if (convert_to_id(conn, argv+2, &conv_data, OUTPUT)) {
		return 1;
	}
	source_data.src_block = conv_data.val_1;
	source_data.src_output = conv_data.val_2;

	if (frontend_setsource(conn, &source_data)) {
		return 1;
	}
	return 0;
}

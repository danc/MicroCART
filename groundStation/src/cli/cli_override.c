#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>

#include "cli.h"
#include "cli_override.h"
#include "frontend_override.h"

int cli_outputoverride(struct backend_conn * conn, int argc, char ** argv) {
	int needHelp = 0;
	struct frontend_override_data values;

	if ((needHelp = help_check(argc, argv))) {
		printf("outputoverride sets the override state of the hardware platform\n");
		printf("Usage Syntax : \n\t./Cli outputoverride <enable> Time Throttle Pitch Roll Yaw \n");
		return 0;
	}

	if (argc < 6) {
		printf("Incorrect Usage, run './cli outputoverride --help' for correct usage.\n");
		return 1;
	}
	int ena;
	sscanf(argv[1], "%d", &ena); values.enable = ena;
	sscanf(argv[2], "%f", &values.time);
	sscanf(argv[3], "%f", &values.throttle);
	sscanf(argv[4], "%f", &values.pitch);
	sscanf(argv[5], "%f", &values.roll);
	sscanf(argv[6], "%f", &values.yaw);

	if (frontend_setoutputoverride(conn, &values)) {
		return 1;
	}
	return 0;
}

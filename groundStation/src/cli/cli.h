#ifndef _CLI_H
#define _CLI_H

#include "frontend_common.h"
#include "cli_source.h"
#include "cli_param.h"
#include "cli_output.h"
#include "cli_nodes.h"
#include "cli_trackable.h"
#include "cli_override.h"
#include "cli_logfile.h"

struct backend_conn;

enum CommandNameIds{
	CMD_GETSOURCE,
	CMD_SETSOURCE,
	CMD_GETPARAM,
	CMD_SETPARAM,
	CMD_GETOUTPUT,
	CMD_GETNODES,
	CMD_ADDNODE,
	CMD_GETTRACKABLE,
	CMD_SETTRACKABLE,
	CMD_OUTPUTOVERRIDE,
	CMD_GETLOGFILE,
	CMD_LOGBLOCKCOMMAND,
	MAX_COMMANDS
};

typedef int (*cli_function_ptr)(struct backend_conn *, int, char **);
static cli_function_ptr cli_functions[] = {
	&cli_getsource,
	&cli_setsource,
	&cli_getparam,
	&cli_setparam,
	&cli_getoutput,
	&cli_getnodes,
	&cli_addnode,
	&cli_gettrackable,
	&cli_settrackable,
	&cli_outputoverride,
	&cli_getlogfile,
	&cli_logblockcommand
};

static char* commandNames[MAX_COMMANDS] = {
	"getsource",
	"setsource",
	"getparam",
	"setparam",
	"getoutput",
	"getnodes",
	"addnode",
	"gettrackable",
	"settrackable",
	"outputoverride",
	"getlogfile",
	"logblockcommand",
};

/**
 * generic two short struct for use with 'convert_to_id()'
 */
struct convert_data {
	int16_t val_1;
	int16_t val_2;
};

enum conversion_requirements {
	INPUT,
	PARAM,
	OUTPUT
};
/**
 * isNumber takes a char * such as argv[n] and checks to see if it is a number.
 * @param  number  null terminating string (ex. argv[1])
 * @return         1 if 'number' is a number
 *                 0 if 'number' is not a number 
 */
int isNumber(char *number);

/**
 * convert_to_id will take in the convert_data struct and either use the values from argv or 
 * 	find the correct values by using the optional strings passed in from argv
 * @param  conn         backend connection struct
 * @param  argv         argv from the cmd line
 * @param  convert_data saves the necessary converted data in this struct for later use.
 * @return             	returns an error integer
 */
int convert_to_id(struct backend_conn * conn, char **argv, struct convert_data * convert_data, int conversion_requirement);

/* This function is called by other cli functions to check for a help condition */
/**
 * determine if the user asked for help
 * @return      1 if help is needed, else 0.
 */
int help_check(int argc, char ** argv);

#endif /* _CLI_H */

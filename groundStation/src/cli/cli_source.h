#ifndef  _CLI_SOURCE_H
#define  _CLI_SOURCE_H

#include "frontend_source.h"

int cli_getsource(struct backend_conn * conn, int argc, char ** argv);
int cli_setsource(struct backend_conn * conn, int argc, char ** argv);

#endif /* _CLI_SOURCE_H */
#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>

#include "cli.h"
#include "cli_output.h"
#include "frontend_output.h"

int cli_getoutput(struct backend_conn * conn, int argc, char ** argv) {
	int needHelp = 0;
	struct frontend_output_data output_data;

	if ((needHelp = help_check(argc, argv))) {
		printf("getoutput gets the output value of a specified block_id and output_id\n");
		printf("Usage Syntax : \n\t./Cli getoutput <block_id|'block_name'> <output_id|'output_name'>\n");
		printf("Symlink Usage Syntax : \n\t./getoutput <block_id|'block_name'> <output_id|'output_name'>\n\n");
		return 0;
	}

	if (argc < 3) {
		printf("Incorrect Usage, run './cli getoutput --help' for correct usage.\n");
		return 1;
	}

	if (convert_to_id(conn, argv, (struct convert_data *)&output_data, OUTPUT)) {
		return 1;
	}

	if (frontend_getoutput(conn, &output_data)) {
		return 1;
	}

	printf("BLOCK.OUTPUT = VAL\n" \
		   "  %" PRId16 ".%" PRId16 "   =  %lf\n",
		   output_data.block,
		   output_data.output,
		   output_data.value);
	return 0;
}
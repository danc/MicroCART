#include <sys/types.h>
#include <inttypes.h>

#include "logfile.h"
#include "commands.h"
#include "bitwise.h"

enum getlogfileData {
	GL_COMMAND_ID = 0,
	GL_DATA_SIZE = 1
};

/* Creates data and metadata for a respcontrol packet
 * Returns data size.
 */
ssize_t EncodeGetLogfile(
        struct metadata * m,        /* data_len and msg_type will be populated*/
        uint8_t * data,             /* Output buffer */
        size_t data_size,           /* Max buffer size */
        const char * msg)       /* Message to encode */
{
	m->msg_type = GETLOGFILE_ID;
	m->data_len = GL_DATA_SIZE;

	int8_t command;

	sscanf(msg, "getlogfile %hhd", &command);

    data[GL_COMMAND_ID] = command;

	return GL_DATA_SIZE;
}

/* Creates data and metadata for a setcontrol packet
 * Returns data size.
 */
ssize_t EncodeResponseLogfile(
        struct metadata * m,        /* data_len and msg_type will be populated*/
        uint8_t * data,             /* Output buffer */
        size_t data_size,           /* Max buffer size */
        const char * msg)           /* Message to encode */
{
	m->msg_type = RESPGETLOGFILE_ID;
	memcpy(&m->msg_id, msg, sizeof(uint16_t));
	memcpy(&m->data_len, &msg[2], sizeof(uint16_t));

    memcpy(data, &msg[4], m->data_len);

	return m->data_len;
}

/* Decode a metadata and data to populate a controller.
 * Returns bytes written to msg, -1 on failure.
 */
int DecodeResponseLogfile(
        char * msg,     			/* Decoded controller message */
		size_t max_len,				   /* msg buffer max size */
        const struct metadata * m,          /* Metadata to aid in decoding */
        const uint8_t * data)               /* Data to decode */
{
	if (m->msg_type != RESPGETLOGFILE_ID) {
		return -1;
	}

	return snprintf(msg, max_len, "getlogfile %s\n",
		&data[1]);
}

enum LogBlockCommandData {
	LB_BLOCK_COMMAND = 0,
	LB_BLOCK_ID = 1,
	LB_DATA_SIZE = 2
};

ssize_t EncodeLogBlockCommand(       
        struct metadata * m,        /* data_len and msg_type will be populated*/
        uint8_t * data,             /* Output buffer */
        size_t data_size,           /* Max buffer size */
        const char * msg)           /* Message to encode */
{
    m->msg_type = LOGBLOCKCOMMAND_ID;
    m->data_len = LB_DATA_SIZE;

	if (data_size < LB_DATA_SIZE) {
		return -1;
	}

	int8_t command, id;

	sscanf(msg, "logblockcommand %hhd %hhd", &command, &id);

	data[LB_BLOCK_COMMAND] = command;
    data[LB_BLOCK_ID] = id;

	return LB_DATA_SIZE;
}

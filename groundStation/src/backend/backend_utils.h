#ifndef __BACKEND_UTILS_H
#define __BACKEND_UTILS_H

#include <sys/time.h>

#define MAX_HASH_SIZE 50

/**
 * Function for recording latencies.
 */
void findTimeDiff(int respID);
//int timeval_subtract(struct timeval *result, struct timeval *x, struct timeval *y);

/**
 * Time stamp checking.
 */
//struct timeval timeArr[MAX_HASH_SIZE];

/**
 * Creates a path for a log file inside the logs directory within the groundStation directory.
 * If the user inputs a specified name the name of the file will be as follows:
 *     <name>_#.txt
 * Otherwise the format is shown below:
 *     <year>-<month>-<day>_<time>_#.txt
 * In both cases the # will start and 0 and increment for each log file saved during the life
 * of the backend process.
 */
char * create_log_name(char * buffer, size_t max, char * user_specified_log_name);

#endif /*__BACKEND_UTILS_H */

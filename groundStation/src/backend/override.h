#ifndef _OVERRIDE_h
#define _OVERRIDE_h

#include "packet.h"

#include <sys/types.h>


/* Creates data and metadata for a overrideoutput packet.
 * Returns data size.
 */
ssize_t EncodeSetOutputOverride(
		struct metadata *m, /* Out */
		uint8_t *data,      /* Out */
		size_t data_size,   /* Data buffer max size */
		const char * msg); /* Value is not used; only IDs */

#endif

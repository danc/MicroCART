#ifndef _getoutput_h
#define _getoutput_h

#include "packet.h"

#include <sys/types.h>


/* Creates data and metadata for a getcontrol packet.
 * Returns data size.
 */
ssize_t EncodeGetOutput(
		struct metadata *m, /* Out */
		uint8_t *data,      /* Out */
		size_t data_size,   /* Data buffer max size */
		const char * msg); /* Value is not used; only IDs */


/* Decode a metadata and data to populate a controller.
 * Returns bytes written to msg, -1 on failure.
 */
int DecodeResponseOutput(
		char * msg, 				   /* Out */
		size_t max_len,				   /* msg buffer max size */
		const struct metadata *m,      /* In */
		const uint8_t * data);         /* In */


#endif

#ifndef _nodes_h
#define _nodes_h

#include "packet.h"

#include <sys/types.h>


/* Creates data and metadata for a getnodes packet.
 * Returns data size.
 */
ssize_t EncodeGetNodes(
		struct metadata *m, /* Out */
		uint8_t *data,      /* Out */
		size_t data_size,   /* Data buffer max size */
		const char * msg); /* Value is not used; only IDs */

/* Creates data and metadata for a addnode packet
 * Returns data size.
 */
ssize_t EncodeAddNode(
		struct metadata * m,        /* data_len and msg_type will be populated*/
		uint8_t * data,             /* Output buffer */
		size_t data_size,           /* Max buffer size */
		const char * msg);      /* Message to encode */


/* Decode a metadata and data to populate a.
 * Returns bytes written to msg, -1 on failure.
 */
int DecodeResponseGetNodes(
		char ** msg, 				   /* Out */
		size_t max_len,				   /* msg buffer max size */
		const struct metadata *m,      /* In */
		const uint8_t * data);         /* In */


/* Decode a metadata and data to populate a controller.
 * Returns bytes written to msg, -1 on failure.
 */
int DecodeResponseAddNode(
		char * msg, 				   /* Out */
		size_t max_len,				   /* msg buffer max size */
		const struct metadata *m,      /* In */
		const uint8_t * data);         /* In */

#endif
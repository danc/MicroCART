#ifndef _logfile_h
#define _logfile_h

#include "packet.h"
#include "backend_adapter.h"

#include <sys/types.h>


/* Creates data and metadata for a getcontrol packet.
 * Returns data size.
 */
ssize_t EncodeGetLogfile(
		struct metadata *m, /* Out */
		uint8_t *data,      /* Out */
		size_t data_size,   /* Data buffer max size */
		const char * msg); /* Value is not used; only IDs */

/* Creates data and metadata for a setcontrol packet
 * Returns data size.
 */
ssize_t EncodeResponseLogfile(
		struct metadata * m,        /* data_len and msg_type will be populated*/
		uint8_t * data,             /* Output buffer */
		size_t data_size,           /* Max buffer size */
		const char * msg);      /* Message to encode */

/* Decode a metadata and data to populate a controller.
 * Returns bytes written to msg, -1 on failure.
 */
int DecodeResponseLogfile(
		char * msg, 				   /* Out */
		size_t max_len,				   /* msg buffer max size */
		const struct metadata *m,      /* In */
		const uint8_t * data);         /* In */

ssize_t EncodeLogBlockCommand(
		struct metadata *m, /* Out */
		uint8_t *data,      /* Out */
		size_t data_size,   /* Data buffer max size */
		const char * msg); /* Value is not used; only IDs */
#endif

#include <sys/types.h>
#include <inttypes.h>

#include "source.h"
#include "commands.h"
#include "bitwise.h"


enum GetsourceData {
	GS_BLOCK_ID_L,
	GS_BLOCK_ID_H,
	GS_INPUT_ID_L,
	GS_INPUT_ID_H,
	GS_DATA_SIZE
};

/* Creates data and metadata for a setcontrol packet
 * Returns data size.
 */
ssize_t EncodeGetSource(
        struct metadata * m,        /* data_len and msg_type will be populated*/
        uint8_t * data,             /* Output buffer */
        size_t data_size,           /* Max buffer size */
        const char * msg)          /* Message to encode */
{
	m->msg_type = GETSOURCE_ID;
	m->data_len = GS_DATA_SIZE;

	if (data_size < GS_DATA_SIZE) {
		return -1;
	}
	int16_t block, input;

	sscanf(msg, "getsource %" SCNd16 " %" SCNd16, &block, &input);

	data[GS_BLOCK_ID_L] = LSByte16(block);
	data[GS_BLOCK_ID_H] = MSByte16(block);
	data[GS_INPUT_ID_L] = LSByte16(input);
	data[GS_INPUT_ID_H] = MSByte16(input);

	return GS_DATA_SIZE;
}

enum SetsourceData {
	SS_DST_BLOCK_ID_L,
	SS_DST_BLOCK_ID_H,
	SS_DST_INPUT_ID_L,
	SS_DST_INPUT_ID_H,
	SS_SRC_BLOCK_ID_L,
	SS_SRC_BLOCK_ID_H,
	SS_SRC_OUTPUT_ID_L,
	SS_SRC_OUTPUT_ID_H,
	SS_DATA_SIZE
};

/* Creates data and metadata for a setcontrol packet
 * Returns data size.
 */
ssize_t EncodeSetSource(
        struct metadata * m,        /* data_len and msg_type will be populated*/
        uint8_t * data,             /* Output buffer */
        size_t data_size,           /* Max buffer size */
        const char * msg)          /* Message to encode */
{
	m->msg_type = SETSOURCE_ID;
	m->data_len = SS_DATA_SIZE;

	if (data_size < SS_DATA_SIZE) {
		return -1;
	}

	int16_t dst_block, dst_input;
	int16_t src_block, src_output;

	sscanf(msg, "setsource %" SCNd16 " %" SCNd16 " %" SCNd16 " %" SCNd16, &dst_block, &dst_input, &src_block, &src_output);

	data[SS_DST_BLOCK_ID_L] = LSByte16(dst_block);
	data[SS_DST_BLOCK_ID_H] = MSByte16(dst_block);
	data[SS_DST_INPUT_ID_L] = LSByte16(dst_input);
	data[SS_DST_INPUT_ID_H] = MSByte16(dst_input);

	data[SS_SRC_BLOCK_ID_L] = LSByte16(src_block);
	data[SS_SRC_BLOCK_ID_H] = MSByte16(src_block);
	data[SS_SRC_OUTPUT_ID_L] = LSByte16(src_output);
	data[SS_SRC_OUTPUT_ID_H] = MSByte16(src_output);

	return SS_DATA_SIZE;
}


enum ResponseData {
	RESP_DST_BLOCK_ID_L,
	RESP_DST_BLOCK_ID_H,
	RESP_DST_INPUT_ID_L,
	RESP_DST_INPUT_ID_H,
	RESP_SRC_BLOCK_ID_L,
	RESP_SRC_BLOCK_ID_H,
	RESP_SRC_OUTPUT_ID_L,
	RESP_SRC_OUTPUT_ID_H,
	RESP_DATA_SIZE
};

/* Decode a metadata and data to populate a controller.
 * Returns bytes written to msg, -1 on failure.
 */
int DecodeResponseSource(
        char * msg,     /* Decoded controller message */
		size_t max_len,				   /* msg buffer max size */
        const struct metadata * m,          /* Metadata to aid in decoding */
        const uint8_t * data)               /* Data to decode */
{
	if (m->data_len < RESP_DATA_SIZE) {
		return -1;
	}
	if (m->msg_type != RESPSOURCE_ID) {
		return -1;
	}

	return snprintf(msg, max_len, "getsource %" PRId16 " %" PRId16 " %" PRId16" %" PRId16 "\n",
		BytesTo16(data[RESP_DST_BLOCK_ID_L], data[RESP_DST_BLOCK_ID_H]),
		BytesTo16(data[RESP_DST_INPUT_ID_L], data[RESP_DST_INPUT_ID_H]),
		BytesTo16(data[RESP_SRC_BLOCK_ID_L], data[RESP_SRC_BLOCK_ID_H]),
		BytesTo16(data[RESP_SRC_OUTPUT_ID_L], data[RESP_SRC_OUTPUT_ID_H]));
}

/**
 * Config.h defines how to setup the backend when run. User may use many of
 * the defined variables to change default communication schemes. If you are
 * planning on using any of these env vars and you have exported them with
 * normal user rights, you will need to run the backend with sudo elevation
 * and with --preserve-env or -E flag.
 * 
 * Example environment variable usage:
 *     
 *     export UCART_SOCKET=./backend.socket     
 */
#ifndef __CONFIG_H
#define __CONFIG_H

#include <stdio.h>
#include <pthread.h>
#include <stdint.h>

#define FRONTEND_DEBUG_PRINT 0
#define ADAPTER_DEBUG_PRINT 1
#define VRPN_DEBUG_PRINT 0
#define BACKEND_DEBUG_PRINT 1

/**
 * Socket used to communicate with the backend.
 */
#define DEFAULT_SOCKET "./ucart.socket"

/**
 * Defines the name of an environmnet variable that the user may use to 
 * set a different path for the backend socket. 
 * ex. export UCART_SOCKET=./backend.socket
 */ 
#define SOCKET_ENV "UCART_SOCKET"

/**
 * Defines the name of environment variables used to config the backend. 
 * The value that these variables are set to does not matter, if they exist
 * in the system the backend will adjust accordingly.
 */
#define NOQUAD_ENV "UCART_NO_QUAD"
#define NOVRPN_ENV "UCART_NO_VRPN"

/**
 * Defines the name of environment variable used to config the backend 
 * comm channel.
 */
#define QUAD_COMM_ENV "UCART_COMM_CHANNEL"

/**
 * Default local communication configuration variables. Use is same as 
 * socket above.
 */
#define QUAD_LOCAL_RX "UCART_LOCAL_RX"
#define QUAD_LOCAL_TX "UCART_LOCAL_TX"
#define QUAD_LOCAL_DEFAULT_RX "../quad/bin/virt-quad-fifos/uart-rx"
#define QUAD_LOCAL_DEFAULT_TX "../quad/bin/virt-quad-fifos/uart-tx"

/**
 * Adapter connection struct for socket communication. Same as struct 
 * used by CLI and GUI to communicate with the backend.
 */
struct adapter_conn {
    FILE *socket;
    size_t len;
    char *buf;
};

/**
 * Struct that defines a trackable object.
 *
 * name - name of the trackable object, used by the get and set trackable commands
 * socket - socket that the trackable is connected to, set to 0, will be adjusted by the backend
 * quad_ip - IP of the MicroCART Quadcopter, set when using wifi 
 *           Default - 191.168.1.1
 * quad_port - Port of the the MicroCART Quadcopter, set when using wifi
 *             Default - 8080
 * isAdapter - 0 if using Quad or other communication, 1 if using Adapter
 * adapter_path - path to the adapter, set to NULL if not using an adapter
 * socket_path - path to the socket for an adapter, set to NULL if not 
 *               using an adapter
 * conn - pointer to struct used by an adapter to communicate between generic trackables, set to NULL
 * isLocal - 1 if using local Fifo (virtual quad), 0 otherwise
 * fifo_tx - Tx Fifo, set to 0, will be set by backend
 * fifo_rx - Rx Fifo, set to 0, will be set by backend
 * fifo_tx_path - Path to tx fifo, set to NULL if not using Local
 *                Default - ../quad/bin/virt-quad-fifos/uart-tx
 * fifo_rx_path - Path to rx fifo, set to NULL if not using Local
 *                Default - ../quad/bin/virt-quad-fifos/uart-rx
 * isBluetooth - 1 if using bluetooth communication to quad, 0 otherwise
 * bt_channel - bluetooth channel for the quad, 0 if not using bluetooth
 *              Default - 0x01
 * bt_addr - bluetooth address for the quad, NULL if not using bluetooth
 *              Default - 00:06:66:65:61:D6
 * server_name - Server name for the VRPN system <NAME>:<IP>:<PORT>
 *               Default IP: 192.168.1.1
 *               Default Port: 8080
 * tracker - pointer to vrpn tracker for a trackable, set to NULL
 * socket_mutex - mutex used when communicating with a trackable, set to PTHREAD_MUTEX_INITIALIZER
 */ 
typedef struct {
    char * name; 
    int socket;
    //Wifi Variables
    char * quad_ip;
    uint16_t quad_port;
    //Adapter Variables
    int isAdapter; 
    char * adapter_path; 
    char * socket_path; 
    struct adapter_conn * conn;
    //Local Communication Variables
    int isLocal;
    int fifo_tx;
    int fifo_rx;
    char * fifo_tx_path;
    char * fifo_rx_path;
    //Bluetooth Variables
    int isBluetooth;
    uint8_t bt_channel;
    char * bt_addr;
    //VRPN Variables
    char * server_name; 
    struct ucart_vrpn_tracker * tracker;
    //Mutex for trackable
    pthread_mutex_t socket_mutex;
} trackable_t;

/**
 * Defines the number of flying objects controlled through the backend. 
 * Needs to be at least one to accomodate for Quad.
 */
#define NUM_TRACKABLES 1

/**
 * Trackables array defines all trackables that will be used in flight. 
 * The quad should always be the first trackable in the array.
 */
extern trackable_t trackables[];

#endif /* __CONFIG_H */

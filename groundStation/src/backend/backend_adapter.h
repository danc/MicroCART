/**
 * Contains methods to connect to, disconnect from, and write to an adapter
 * socket.
 */
#ifndef __BACKEND_ADAPTER_H
#define __BACKEND_ADAPTER_H

#include <stdlib.h>
#include "config.h"
#include "commands.h"

enum AdapterOnlyMessageTypeID {
    GETLOGFILE_ID = MAX_TYPE_ID,
    RESPGETLOGFILE_ID = MAX_TYPE_ID + 1,
    LOGBLOCKCOMMAND_ID = MAX_TYPE_ID + 2,
    MAX_ADAPTER_ID = MAX_TYPE_ID + 3,
};

/**
 * Opens a connection to an adapter based upon the data in the trackables
 * struct.
 *
 * trackable: adapter represented by trackable struct to be connected to.
 */
int adapterConnect(trackable_t * trackable);

/**
 * Writes a packet to the adapter socket.
 *
 * conn - Adapter connection
 * line - data to write
 * count - number of bytes
 */
int writeToAdapter(struct adapter_conn * conn, const char * line, size_t count);

/**
 * Disconnect from an adapter.
 *
 * trackable: adapter represented by trackable struct to be connected to.
 */
int adapterDisconnect(trackable_t * trackable);



#endif /* __BACKEND_ADAPTER_H */

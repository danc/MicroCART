#include "config.h"

//DEFAULTS
//#define QUAD_LOCAL_DEFAULT_RX "../quad/bin/virt-quad-fifos/uart-rx"
//#define QUAD_LOCAL_DEFAULT_TX "../quad/bin/virt-quad-fifos/uart-tx"
#define QUAD_IP_DEFAULT "192.168.1.1"
#define QUAD_PORT_DEFAULT 8080
#define QUAD_BT_ADDR_DEFAULT "00:06:66:64:61:D6"
#define QUAD_BT_CHANNEL_DEFAULT 0x01
#define QUAD_SERVER_NAME_DEFAULT "UAV@192.168.0.120:3883"

/**
 * Trackables array defines all trackables that will be used in flight. 
 * The quad should always be the first trackable in the array.
 */
trackable_t trackables[] = {
    //Original Quadcopter
    [0] = {
        "UAV", //Name
        0, //Set to 0, the backend will modify on start
        //Wifi Variables
        NULL,
        0,
        //Adapter Variables
        1,
        //"../adapters/crazyflie",
        //"../adapters/crazyflie/cf_adapter.socket",
        "../cflib_groundstation", 
        "../cflib_groundstation/cflib_groundstation.socket", 
        NULL,
        //Local Communication Variables
        0, //Set to 1 if Intending to use local Fifo
        0, //Will be set by Backend
        0, //Will be set by Backend
        NULL,
        NULL,
        //Bluetooth Variables
        0, //Set to 1 if using bluetooth
        0,
        NULL,
        //VRPN Variables
        QUAD_SERVER_NAME_DEFAULT,
        NULL, //Created by backend
        PTHREAD_MUTEX_INITIALIZER
    }
    /*Crazyflie 45
    [1] = {
        "cf1",
        0, 
        NULL,
        0,
        1,
        "./adapters/test_adapter",
        "./adapters/adapter.socket",
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        0,
        0,
        NULL,
        "Crazyflie22@192.168.0.120:3883",
        NULL,
        PTHREAD_MUTEX_INITIALIZER
    },*/  
};

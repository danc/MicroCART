#ifndef _packet_h
#define _packet_h

#include <stdint.h>
#include <sys/types.h>

#include "commands.h"

enum PacketHeader {
	BEGIN,
	MTYPE_L,
	MTYPE_H,
	ID_L,
	ID_H,
	DLEN_L,
	DLEN_H,
	HDR_SIZE
};

enum ChecksumFormat {
	CSUM_L,
	CSUM_SIZE
};

/* Combine metadata and data to form a wire-sendable packet.
 * Returns the size of the encoded packet 
 */
ssize_t EncodePacket(
		uint8_t * packet,           /* Buffer to encode into */
		size_t packet_size,         /* Max buffer size */
		const struct metadata * m,  /* Metadata to encode */
		const uint8_t * data);      /* Data to encode */

/* Break apart packet, populating metadata. Data is copied
 * into the space pointed to by data.
 * Returns the size of the data.
 */
ssize_t DecodePacket(
		struct metadata * m,        /* Decoded metadata (includes data_len)*/
		uint8_t * data,             /* Data is copied into this buffer */
		size_t data_size,           /* Max buffer size */
		const uint8_t * packet,     /* Packet to decode */
		size_t packet_size);        /* Size of packet to decode */

/* Compute a checksum. Requires a packet and the entire packet length */
uint8_t PacketChecksum(const uint8_t * packet, size_t);

/* Compute the size of the entire packet */
size_t PacketSize(const struct metadata *m);

#endif

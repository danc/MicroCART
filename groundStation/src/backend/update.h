#ifndef _update_h
#define _update_h

#include "packet.h"

#include <sys/types.h>

struct position_update {
	uint32_t id;
	float x;
	float y;
	float z;
	float pitch;
	float roll;
	float yaw;
};

/* Creates data and metadata for an update packet.
 * Returns data size.
 */
ssize_t EncodeUpdate(
		struct metadata *m, /* Out */
		uint8_t *data,      /* Out */
		size_t data_size,   /* Data buffer max size */
		const struct position_update *u); /* In */

/* Decode a metadata and data to populate an update
 * Returns 0 on success, -1 on failure
 */
int DecodeUpdate(
		struct position_update *u, /* Out */
		const struct metadata *m,      /* In */
		const uint8_t * data);         /* In */


#endif

/**
 * Contains methods to connect to, disconnect from, and write to an adapter
 * socket.
 */
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdint.h>
#include <unistd.h>
#include <err.h>
#include <errno.h>

#include "backend_adapter.h"

/**
 * Writes a packet onto the adapter socket.
 *
 * conn - Adapter connection
 * line - data to write
 * count - number of bytes
 */
int writeToAdapter(struct adapter_conn *conn, const char * line, size_t count) {
#if ADAPTER_DEBUG_PRINT == 1
    //Print out packet being sent to the adapter.
    printf("packetToAdapter = '");
    for(int i = 0; i < count; ++i) {
      printf(" %.2x ", ((const uint8_t *) line)[i]);
    }
    printf("'\n");
#endif
    return fwrite((const char *) line, sizeof(uint8_t), count, conn->socket);
}

/**
 * Opens a connection to an adapter based upon the data in the trackables
 * struct.
 *
 * trackable: adapter represented by trackable stuct to be connected to.
 */
int adapterConnect(trackable_t * trackable) {
    int s;
    struct sockaddr_un remote;
    char str[100];

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    struct adapter_conn ** conn = &(trackable->conn);

    printf("Trying to connect...");

    memset(&remote, '\000', sizeof(remote));

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, trackable->socket_path);
    
    if (connect(s, (struct sockaddr *)&remote, sizeof(remote)) == -1) {
        //printf(stderr, "%s\n", explain_connect(s, (struct sockaddr *)&remote, sizeof(remote)));
        printf("%s\n", strerror(errno));
        perror("connect");
        return 1;
    }

    *conn = malloc(sizeof(struct adapter_conn));
    if (*conn == NULL) {
        perror("malloc");
        close(s);
        printf("Failure\n");
        return 2;
    }

    (*conn)->len = 0;
    (*conn)->buf = NULL;
    (*conn)->socket = fdopen(s, "r+");
    if ((*conn)->socket == NULL) {
        perror("fdopen");
        free(*conn);
        *conn = NULL;
        close(s);
        printf("Failure\n");
        return 3;
    }
    if (setvbuf((*conn)->socket, NULL, _IONBF, 0)) {
        warn("setvbuf");
    }

    trackable->socket = s;

    printf("Success\n");

    return 0;
}

/**
 * Disconnect from an adapter.
 *
 * trackable: adapter represented by trackable stuct to be connected to.
 */
int adapterDisconnect(trackable_t * trackable) {
    fclose(trackable->conn->socket);
    if (trackable->conn->buf) {
        free(trackable->conn->buf);
    }
    free(trackable->conn);
    return 0;
}

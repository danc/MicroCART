#include <stdio.h>
#include <time.h>
#include <string.h>

#include "backend_utils.h"

#define MAX_HASH_SIZE 50

static struct timeval timeArr[MAX_HASH_SIZE];

static int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y) {
    /* Perform the carry for the later subtraction by updating y. */
    if (x->tv_usec < y->tv_usec) {
        int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }
    /* Compute the time remaing to wait.
     * tv_usec is certainly positive. */
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;
    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

void findTimeDiff(int respID) {
    struct timeval result, tend;
    gettimeofday(&tend, NULL);
    timeval_subtract(&result, &tend, &timeArr[respID%MAX_HASH_SIZE]);
    printf("(BackEnd): Elapsed time = %ld ms\n", result.tv_usec/1000);
}

char * create_log_name(char * buffer, size_t max, char * user_specified_log_name) {
	static const char * prefix = "logs";
	static size_t num_logs = 0;
	static const char * format_string =  "%F_%-l:%M";

	time_t curr_time;
	char c_time_string[256];
	struct tm * tmp;

	curr_time = time(NULL);
	tmp = localtime(&curr_time);
	strftime(c_time_string, 256, format_string, tmp);

	if(strcmp(user_specified_log_name, "") == 0) {
		sprintf(buffer, "%s/%s_%lu.txt", prefix, c_time_string, num_logs++);
	} else {
		sprintf(buffer, "%s/%s_%lu.txt", prefix, user_specified_log_name, num_logs++);
	}

	return buffer;
}

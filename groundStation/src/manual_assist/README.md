# Manual Assist Mode
## About
Manual Assist Mode is a mode where the quad is set to autonomous mode and then a usb controller is used to control a set point that the quad will move to. This is a seperate application that interfaces with a running backend instance.

## Make Process
Plug in the usb controller and then check that the device file is there by using the following command
```bash
ls /dev/input/
```
In the output you should see the a device file that is called **jsX** where X is a number. ( normally **js0** )

**Check the path** at the top of [manual_assist_main.c](./manual_assist_main.c#L13) to **make sure it is mapping to the correct device file** (the jsX file found in the prev step).

Finally run the main [makefile](../../Makefile) for groundstation located in the MicroCART/groundStation/ directory.

## Using
To use the Manual Assist Mode you will have to initially make the groundStation and ManualAssist executable as described above.

Next **start up a backend instance** the standard way as described by [groundStation README](../../README.md)

Next run through the [standard demo initialization](../../../documentation/how_to_demo.md) steps to **get to the autonomous flight** step.

Once you get the quad past the manual flight mode test, set it to autonomous mode by flipping the **FLAP SWITCH** to **0** on the manual mode normal controller **(The NON USB controller)**

Finally run the ManualAssist program by running the following command from the MicroCART/groundStation/ directory by running the following command
```bash
    ./ManualAssist
```

You can now use the controller to take off by flipping the **channel 7 switch** to the **up** position  

Use Joysticks to move your set point within the safety range defined in your [manual_assist_mode.c](manual_assist_main.c)  

When finished with your flight flip the **channel 7 switch** to the **down** position to land and flip the switches on the manual mode controller (**NON USB**) to manual and killed  

Remember to **end the ManualAssist program** by using the **ctrl** + **c** key combination  
## Controller

![alt text](../../documentation/images/usb-controller_edit.jpg "USB Controller")

 - **Channel 5 switch**  
   UP = DEBUG ON  
   DOWN = DEBUG OFF
 - **Channel 7 switch**  
   UP = TAKEOFF script / controlling flight  
   DOWN = LAND script  / done flying  
 - **Left Joystick**  
   UP = QUAD height up
   CENTER = QUAD keep height  
   DOWN = QUAD height down 
   RIGHT / LEFT = nothing
 - **Right Joystick**  
   UP = QUAD forward  
   DOWN = QUAD backward  
   RIGHT = QUAD right  
   LEFT = QUAD left  
   CENTER = keep X and Y

**NOTE: When flying keep in mind the directions of the camera system!**

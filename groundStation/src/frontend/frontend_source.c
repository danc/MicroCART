#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <inttypes.h>

#include "frontend_source.h"


/* Get the block.output that corresponds 
 *      to the block.input in question.
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getsource(
		struct backend_conn * conn,
		struct frontend_source_data * source_data) {

	char msg[64] = "";
	int written;

	snprintf(msg, 64, "getsource %" PRId16 " %" PRId16 "\n", source_data->dst_block, source_data->dst_input);

	if((written = ucart_backendWrite(conn, msg)) < 0) {
		return 1;
	}

	size_t pendingResponses = 1;
	char * response;

	while (pendingResponses) {
		response = ucart_backendGetline(conn);
		if (response == NULL) {
			warnx("Line not returned from backend");
			return 1;
		}

		if (strncmp(response, "getsource", 9) == 0) {
			sscanf(response, "getsource %" SCNd16 " %" SCNd16 " %" SCNd16 " %" SCNd16, 
				&source_data->dst_block, &source_data->dst_input,
				&source_data->src_block, &source_data->src_output);
			pendingResponses--;
		}
	}
	
	return 0;
}

/* Set the connect the block.output from one block to 
 *       another block.input.
 *
 * Returns 0 on success, 1 on error
 */
int frontend_setsource(
		struct backend_conn * conn,
		struct frontend_source_data * source_data) {
	char msg[64] = "";
	int written;

	snprintf(msg, 64, "setsource %" PRId16 " %" PRId16 " %" PRId16 " %" PRId16 "\n", 
		source_data->dst_block, source_data->dst_input, 
		source_data->src_block, source_data->src_output);

	if((written = ucart_backendWrite(conn, msg)) < 0) {
		return 1;
	}
	
	return 0;
}

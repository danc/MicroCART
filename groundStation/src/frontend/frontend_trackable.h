#ifndef _FRONTEND_TRACKABLE_H
#define _FRONTEND_TRACKABLE_H

#include "frontend_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Get the trackable name that is being communicated with.
 *
 * conn: backend connection.
 * trackable: on success this will contain the name of the trackable.
 * 
 * Returns 0 on success, 1 on error
 */
int frontend_gettrackable(
        struct backend_conn * conn,
        char * trackable);

/* Set the trackable that is being communicated with.
 *
 * conn: backend connection.
 * trackable: name of the trackable (names in the ground_station)
 *
 * Returns 0 on success, 1 on error
 */
int frontend_settrackable(
        struct backend_conn * conn,
        const char * trackable);

#ifdef __cplusplus
}
#endif
#endif /* _FRONTEND_TRACKABLE_H */

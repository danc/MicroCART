/**
 * frontend_output writes the getoutput command to the backend connection.
 */

#ifndef _FRONTEND_OVERRIDE_H
#define _FRONTEND_OVERRIDE_H

#include "frontend_common.h"

/* Set the override state of outputs
 *
 * Returns 0 on success, 1 on error
 */
#ifdef __cplusplus
extern "C" {
#endif
int frontend_setoutputoverride(
		struct backend_conn * conn,
		struct frontend_override_data * values);
#ifdef __cplusplus
}
#endif


#endif /* __FRONTEND_OVERRIDE_H */

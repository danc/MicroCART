#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <inttypes.h>

#include "frontend_trackable.h"


/* Get the trackable name that is being communicated with.
 *
 * conn: backend connection.
 * trackable: on success this will contain the name of the trackable.
 * 
 * Returns 0 on success, 1 on error
 */
int frontend_gettrackable(
        struct backend_conn * conn,
        char * trackable) {

    char msg[64] = "";
    int written;

    snprintf(msg, 64, "gettrackable\n");

    if((written = ucart_backendWrite(conn, msg)) < 0) {
        return 1;
    }

    size_t pendingResponses = 1;
    char * response;

    while (pendingResponses) {
        response = ucart_backendGetline(conn);
        if (response == NULL) {
            warnx("Line not returned from backend");
            return 1;
        }

        if (strncmp(response, "gettrackable", 12) == 0) {
            sscanf(response, "gettrackable %s", 
                trackable);
            pendingResponses--;
        }
    }
    
    return 0;
}

/* Set the trackable that is being communicated with.
 *
 * conn: backend connection.
 * trackable: name of the trackable (names in the ground_station)
 *
 * Returns 0 on success, 1 on error
 */
int frontend_settrackable(
        struct backend_conn * conn,
        const char * trackable) {
    char msg[64] = "";
    int written;

    snprintf(msg, 64, "settrackable %s\n", trackable);

    if((written = ucart_backendWrite(conn, msg)) < 0) {
        return 1;
    }

    return 0;
}

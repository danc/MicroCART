/**
 * Frontend contains the functions used by both the GUI and CLI to communicate
 * with the quad and VRPN system.
 *
 * frontend_common.c has the varius structs used within the frontend to pass
 * data between the system. It also contains the methods to connect, disconnect,
 * and write from/to the quad. 
 */
#ifndef _FRONTEND_COMMON_H
#define _FRONTEND_COMMON_H
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct backend_conn;

/* Start connection to quad */
struct backend_conn * ucart_backendConnect(void);

/* Stop connection to quad */
void ucart_backendDisconnect(struct backend_conn * conn);

/* Get a line from the backend.
 *
 * The line will remain valid until the next call to ucart_backendGetline.
 */
char * ucart_backendGetline(struct backend_conn * conn);

/* Write a line to the backend */
int ucart_backendWrite(struct backend_conn * backend, const char * line);

struct frontend_output_data {
	int16_t block;
	int16_t output;
	float value;
};

struct frontend_param_data {
	int16_t block;
	int16_t param;
	float value;
};

struct frontend_source_data {
	int16_t dst_block;
	int16_t dst_input;
	int16_t src_block;
	int16_t src_output;
};

struct frontend_node_data {
	int16_t block;
	int16_t type;
	char *name;
};

struct frontend_override_data {
	int8_t enable;
	float time;
	float throttle;
	float pitch;
	float roll;
	float yaw;
};

struct frontend_getlogfile_data {
	int8_t command;
	char *name;
};

struct frontend_logblockcommand_data {
	int8_t command;
	int8_t id;
};

#ifdef __cplusplus
}
#endif
#endif /* __FRONTEND_COMMON_H */

/**
 * frontend_source writes the getsource and setsource command to the backend
 * connection.
 */

#ifndef _FRONTEND_SOURCE_H
#define _FRONTEND_SOURCE_H

#include "frontend_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Get the block.output that corresponds 
 *      to the block.input in question.
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getsource(
		struct backend_conn * conn,
		struct frontend_source_data * source_data);

/* Set the connect the block.output from one block to 
 *       another block.input.
 *
 * Returns 0 on success, 1 on error
 */
int frontend_setsource(
		struct backend_conn * conn,
		struct frontend_source_data * source_data);

#ifdef __cplusplus
}
#endif
#endif /* _FRONTEND_SOURCE_H */

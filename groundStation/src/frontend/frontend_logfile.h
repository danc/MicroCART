#ifndef _FRONTEND_LOGFILE_H
#define _FRONTEND_LOGFILE_H

#include "frontend_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Get the logfile path that is being used.
 *
 * conn: backend connection.
 * logfile: on success this will contain the name of the log file.
 * 
 * Returns 0 on success, 1 on error
 */
int frontend_getlogfile(
        struct backend_conn * conn,
        struct frontend_getlogfile_data * logfile);

int frontend_logblockcommand(
        struct backend_conn * conn, struct frontend_logblockcommand_data * values);
       
#ifdef __cplusplus
}
#endif
#endif /* _FRONTEND_LOGFILE_H */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <inttypes.h>

#include "frontend_param.h"


/* Get the value of block.param
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getparam(
		struct backend_conn * conn,
		struct frontend_param_data * param_data) {

	char msg[64] = "";
	int written;

	snprintf(msg, 64, "getparam %" PRId16 " %" PRId16 "\n", param_data->block, param_data->param);

	if((written = ucart_backendWrite(conn, msg)) < 0) {
		return 1;
	}

	size_t pendingResponses = 1;
	char * response;

	while (pendingResponses) {
		response = ucart_backendGetline(conn);
		if (response == NULL) {
			warnx("Line not returned from backend");
			return 1;
		}

		if (strncmp(response, "getparam", 8) == 0) {
			sscanf(response, "getparam %" SCNd16 " %" SCNd16 " %f", 
				&param_data->block, &param_data->param,
				&param_data->value);
			pendingResponses--;
		}
	}
	
	return 0;
}

/* Set the value of block.param
 *
 * Returns 0 on success, 1 on error
 */
int frontend_setparam(
		struct backend_conn * conn,
		struct frontend_param_data * param_data) {

	char msg[64] = "";
	int written;

	snprintf(msg, 64, "setparam %" PRId16 " %" PRId16 " %f\n", param_data->block, param_data->param, param_data->value);

	if((written = ucart_backendWrite(conn, msg)) < 0) {
		return 1;
	}
	
	return 0;
}
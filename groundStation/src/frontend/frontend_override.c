#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <inttypes.h>

#include "frontend_override.h"

/* Set the override state of outputs
 *
 * Returns 0 on success, 1 on error
 */
int frontend_setoutputoverride(
		struct backend_conn * conn,
		struct frontend_override_data * values) {

	char msg[128] = "";
	int written;

	snprintf(msg, 128, "outputoverride %" PRId8 " %f %f %f %f %f\n",
		values->enable,
		values->time,
		values->throttle, values->pitch, values->roll, values->yaw);

	if((written = ucart_backendWrite(conn, msg)) < 0) {
		return 1;
	}

	size_t pendingResponses = 0;
	char * response;

	while (pendingResponses) {
		response = ucart_backendGetline(conn);
		if (response == NULL) {
			warnx("Line not returned from backend");
			return 1;
		}


	}
	
	return 0;
}

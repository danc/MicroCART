/**
 * frontend_param writes the getparam or setparam command to the backend connection.
 */

#ifndef _FRONTEND_PARAM_H
#define _FRONTEND_PARAM_H

#include "frontend_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Get the value of block.param
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getparam(
		struct backend_conn * conn,
		struct frontend_param_data * param_data);

/* Set the value of block.param
 *
 * Returns 0 on success, 1 on error
 */
int frontend_setparam(
		struct backend_conn * conn,
		struct frontend_param_data * param_data);

#ifdef __cplusplus
}
#endif


#endif /* __FRONTEND_PARAM_H */

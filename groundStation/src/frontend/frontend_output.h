/**
 * frontend_output writes the getoutput command to the backend connection.
 */

#ifndef _FRONTEND_OUTPUT_H
#define _FRONTEND_OUTPUT_H

#include "frontend_common.h"

/* Get the value that corresponds 
 *      to the block.output in question.
 *
 * Returns 0 on success, 1 on error
 */
#ifdef __cplusplus
extern "C" {
#endif
int frontend_getoutput(
		struct backend_conn * conn,
		struct frontend_output_data * output_data);
#ifdef __cplusplus
}
#endif


#endif /* __FRONTEND_OUTPUT_H */

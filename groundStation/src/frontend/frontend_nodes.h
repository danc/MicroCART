/**
 * frontend_nodes writes the getnodes or setnodes command to the backend connection.
 */

#ifndef _FRONTEND_NODES_H
#define _FRONTEND_NODES_H

#include "frontend_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Get the block_id, type_id and name of
 * all of the nodes in the current comp_graph.
 *
 * Returns 0 on success, 1 on error
 *
 * node_data and num_nodes are out pointers
 * node_data must point to a NULL pointer
 * num_nodes must point to zero
 *
 */
int frontend_getnodes(
		struct backend_conn * conn,
		struct frontend_node_data ** node_data,
		size_t * num_nodes);


/* Adds a node of specified type_id
 *
 * Returns 0 on success, 1 on error
 */
int frontend_addnode(
		struct backend_conn * conn,
		struct frontend_node_data * node_data);

/* Free a node_data array */
void frontend_free_node_data(
		struct frontend_node_data * nd,
		size_t num_nodes);

#ifdef __cplusplus
}
#endif

#endif /* __FRONTEND_NODES_H */

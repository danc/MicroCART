/**
 * frontend_tracker contains the struct for VRPN data and provides the method to
 * get the data from the frontend by writing TRACKERDATA to the backend.
 */

#ifndef __FRONTEND_TRACKER_H
#define __FRONTEND_TRACKER_H

#include "frontend_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Struct containing pos/att data */
struct frontend_tracker_data {
		double height;
		double lateral;
		double longitudinal;
		double pitch;
		double roll;
		double yaw;
};

/* Get pos/att data from the tracking system
 *
 * conn: IN Connection to quad
 * data: OUT Data is written to this struct
 *
 * Returns 0 on success, nonzero on error
 *
 */
int frontend_track(struct backend_conn * conn, 
		struct frontend_tracker_data *data);

#ifdef __cplusplus
}
#endif

#endif /* __FRONTEND_TRACKER_H */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <inttypes.h>

#include "frontend_logfile.h"


/* Get the trackable name that is being communicated with.
 *
 * conn: backend connection.
 * trackable: on success this will contain the name of the trackable.
 * 
 * Returns 0 on success, 1 on error
 */
int frontend_getlogfile(
        struct backend_conn * conn,
        struct frontend_getlogfile_data * logfile) {

    char msg[64] = "";
    int written;

    snprintf(msg, 64, "getlogfile %hhd \n", logfile->command);

    if((written = ucart_backendWrite(conn, msg)) < 0) {
        return 1;
    }

    size_t pendingResponses = 1;
    char * response;

    while (pendingResponses) {
        response = ucart_backendGetline(conn);
        if (response == NULL) {
            warnx("Line not returned from backend");
            return 1;
        }
        if(logfile->command == 3) {
            for(int i = 0; i < strlen(response); i++) {
                if(response[i] == '\t') {
                    response[i] = ',';
                }
            }
        }

        if (strncmp(response, "getlogfile", 10) == 0) {
            sscanf(response, "getlogfile %s", 
                logfile->name);
            pendingResponses--;
        }
    }
    
    return 0;
}

int frontend_logblockcommand(struct backend_conn * conn,
                    struct frontend_logblockcommand_data * values) {
    char msg[70] = "";
	int written;

	snprintf(msg, 64, "logblockcommand %" PRId8 " %" PRId8 "\n",
		values->command,
		values->id);

	if((written = ucart_backendWrite(conn, msg)) < 0) {
		return 1;
	}

	size_t pendingResponses = 0;
    if(values->command == 10) {
        pendingResponses = 1;
    }
	char * response;

	while (pendingResponses) {
		response = ucart_backendGetline(conn);
		if (response == NULL) {
			warnx("Line not returned from backend");
			return 1;
		}


	}
	
	return 0;
}
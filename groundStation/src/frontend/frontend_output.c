#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <inttypes.h>

#include "frontend_output.h"

/* Get the value that corresponds 
 *      to the block.output in question.
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getoutput(
		struct backend_conn * conn,
		struct frontend_output_data * output_data) {

	char msg[64] = "";
	int written;

	snprintf(msg, 64, "getoutput %" PRId16 " %" PRId16 "\n", output_data->block, output_data->output);

	if((written = ucart_backendWrite(conn, msg)) < 0) {
		return 1;
	}

	size_t pendingResponses = 1;
	char * response;

	while (pendingResponses) {
		response = ucart_backendGetline(conn);
		if (response == NULL) {
			warnx("Line not returned from backend");
			return 1;
		}

		if (strncmp(response, "getoutput", 9) == 0) {
			sscanf(response, "getoutput %" SCNd16 " %" SCNd16" %f", 
				&output_data->block, &output_data->output,
				&output_data->value);
			pendingResponses--;
		}
	}
	
	return 0;
}
# ADAPTERS
Adapters are what allows for the current microcart groundstation to communicate with other vehicles. The adapters act as a translator by receiving packets from groundstation and then call the packets corresponding callback function. The developer can then modify callback functions to communicate.

There is one
adapter created in house that allows the use of a Crazyflie through the Matt_test branch of [Custom_Crazyflie_Software](https://git.ece.iastate.edu/danc/Custom_CrazyFlie_Software/tree/Matt_test_branch) repo.
This repository is part of the DANC lab and can be access similar to the MicroCART repositry. This branch makes their ground 
station allow connects in the same manner that our backend does. In order to use this adapter
you must first start the lateral on quad ground station in the repository mentioned above, then run the crazyflie adapter.
After this process is setup make sure that the backend config is set properly for this new trackable and it can be controlled
as if it were one of our quads.

## CREATE NEW:
There are 3 major components that are provided for creation of a new custom adapter.

 - adapterlib - has functions to retrieve single groundstation packets from a socket
 - callbacks - has predefined functions for every type of instruction supported by groundstation
 - demoadapter - example code to set up a unix socket, read from it, and call the corresponding callback function

These files rely upon groundstation header files and c files which are linked by the makefile and should be included when compiling (for example see the crazyflie adapter makefile)

To create your own adapter:

 - You will need to modify the callbacks c file to interface with your vehicle.  
 - Modify demoadapter as needed to set up communications with your vehicle.  
 - Modify the trackables struct in groundstation. 

## WARNINGS:
Running the executables provided use relative links that will be relative to your current working directory.

## FUNCTIONS:
This is a list of what groundstation and quad software functions are required.

 - packet.c/h - DecodePacket()
 - commands.h - metadata struct
 - commands.c - MessageTypes, findCommand()
 - callbacks.h - Typedef for command_cb
  
### INCLUDES LOCATIONS:
This is a list of all the required files to include to be able to use the function listed above. *(Note these paths are relative to this README)*

 - ../../quad/inc/biquad_filter.h
 - ../src/backend/bitwise.h
 - ../../quad/inc/cb_default.h
 - ../../quad/src/commands/commands.c
 - ../../quad/inc/commands.h
 - ../../quad/inc/communication.h
 - ../../quad/inc/computation_graph.h
 - ../../quad/inc/controllers.h
 - ../../quad/inc/hw_iface.h
 - ../../quad/inc/log_data.h
 - ../src/backend/packet.c
 - ../src/backend/packet.h
 - ../../quad/inc/PID.h
 - ../../quad/inc/timer.h
 - ../../quad/inc/type_def.h
 - ../../quad/src/quad_app/util.c
 - ../../quad/src/quad_app/util.h  
 
See Crazyflie [Makefile](crazyflie/Makefile) for an example

## TIPS:
using ncat (nc) you can created sockets for testing your adapter 
```bash
    nc -l -U <socketfilename>       #creates a unix socket server for testing
    nc -U <socketfilename>          #creates a unix socket cient for testing
```


## ADAPTERS:
- [Crazyflie](crazyflie/README.md)


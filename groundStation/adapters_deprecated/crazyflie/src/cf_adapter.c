#include "cf_adapter.h"

//path to socket that groundstation will read/write
#define SOCKPATH "./cf_adapter.socket"

//socket that the crazyflie groundstation will read from, check the README for information
//on how to setup this path.
#define DEFAULT_ADAPTER_SOCKET "../../../crazyflie_groundstation/crazyflie_groundstation.socket"
//#define DEFAULT_ADAPTER_SOCKET "./crazyflie_groundstation.socket"
#define SOCKET_ENV "ADAPTER_SOCKET"

struct backend_conn * cfsocketconn = NULL;
struct backend_conn * mcsocketconn = NULL;

//socket that groundstation will read/write
int adptsockfd;


/**
 *  @brief writes to a socket
 *  @param conn socket to be written to
 *  @param line char array to write to socket
 *  @return 0 on success else error
 */
ssize_t adapterWrite(struct backend_conn *conn, const char * line, size_t psize) {
    //return fwrite((const char *) line, sizeof(uint8_t), psize, conn->socket);
    return send(conn->socketfd, line, psize, 0);
}

/**
 * @brief creates a new connection to crazyflie groundstation
 * @param conn is the struct that will contain the new connection
 * @return 0 if successful else error
 */
 struct backend_conn* connectToCrazyflieGroundStation() {
    int s;
    struct sockaddr_un remote;
    char str[100];

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        return NULL;
    }

    struct backend_conn * conn = NULL;

    printf("Trying to connect...");

    memset(&remote, '\000', sizeof(remote));

    remote.sun_family = AF_UNIX;
    char * sock_env = getenv(SOCKET_ENV);
    strcpy(remote.sun_path, sock_env ? sock_env : DEFAULT_ADAPTER_SOCKET);

    printf("Path: %s\n", remote.sun_path);
    
    if (connect(s, (struct sockaddr *)&remote, sizeof(remote))  == -1) {
        perror("connect");
        return NULL;
    }

    conn = malloc(sizeof(struct backend_conn));
    if (conn == NULL) {
        perror("malloc");
        return NULL;
    }

    printf("socket: %i\n", s);
    int check = fcntl(s, F_GETFD);
    printf("check before: %i\n", check);

    printf("pointer before: %p\n", conn->socket);

    conn->len = 0;
    conn->buf = NULL;
    //conn->socket = fdopen(s, "r+");
    conn->socket = NULL;
    conn->socketfd = s;
    printf("pointer after: %p\n", conn->socket);
    check = fcntl(s, F_GETFD);
    printf("check after: %i\n", check);
    /*if (conn->socket == NULL) {
        perror("fdopen");
        return NULL;
    }
    */
    //TODO: seg faults, no such file or directory
    /*May be redundant code, 
    if (setvbuf(conn->socket, NULL, _IONBF, 0)) {
        warn("setvbuf");
    }
    */
    
    
    printf("Success\n");
    fflush(stdout);

    /* success */
    return conn;
}
/**
 * @brief handles termination signals and cleans up all socket files
 * @param sigNum the signal number that was sent
 */
void signalHandler(int sigNum){
    printf("\n%d signal recieved, cleaning up sockets\n",sigNum);
    close(adptsockfd);
    unlink(SOCKPATH);
    printf("done...\n");
    exit(0);
}

int main(int argc, char *args[]) {

    //setup signal handlers to properly shutdown adapter
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGQUIT, signalHandler);
    signal(SIGHUP, signalHandler);

    unlink(SOCKPATH);

    cfsocketconn = connectToCrazyflieGroundStation();
    if (cfsocketconn == NULL) {
        perror("Failed to connect to cf_groundstation");
        close(adptsockfd);
        unlink(SOCKPATH);
        return 1;
    }

    //create socket and bind
    printf("Adapter started\n\r");
    struct sockaddr_un adptsockinfo;
    char adptsockpath[] = SOCKPATH;

    adptsockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(adptsockfd < 0){
        perror("failed to create groundstation socket\n");
    }
    else{
        printf("created groundstation socket\n\r");
    }

    adptsockinfo.sun_family = AF_UNIX;
    strcpy(adptsockinfo.sun_path, SOCKPATH);
    if(bind(adptsockfd, (struct sockaddr *) &adptsockinfo, sizeof(adptsockinfo))){
        perror("failed to bind to groundstation socket\n");
    }
    else{
        printf("bound groundstation socket\n\r");
    }

    //wait for connection, connect, and set up receive buffer
    if(listen(adptsockfd,16)==-1){
        close(adptsockfd);
        unlink(SOCKPATH);
        perror("failed listen on groundstation socket\n");
        exit(1);
    }
    else {
        printf("listening on %s\n", adptsockpath);
    }

    struct sockaddr_un gssockinfo;
    int len;
    int gssockfd = accept(adptsockfd, (struct sockaddr *) &gssockinfo, &len);
    if(gssockfd == -1){
        close(adptsockfd);
        unlink(SOCKPATH);
        perror("failed to accept groundstation connection\n");
        exit(1);	
    }
    else {
        printf("received a connection on groundstation socket\n\r");
    }

    mcsocketconn = malloc(sizeof(struct backend_conn));
    mcsocketconn->len=0;
    mcsocketconn->buf=NULL;
    mcsocketconn->socket=NULL;
    mcsocketconn->socketfd = gssockfd;

    char recvbuf[1000];
    memset(recvbuf,0,1000);
    int keepRunning = 1;

    struct metadata metad; //metadata struct
    uint8_t data[MAX_PACKET_SIZE]; //array used to store the data bytes of the packet
    memset(data,0,sizeof(data));
    ssize_t data_len = 0;
    ssize_t packetsize = -1; 
    int cbreturn = 0;

    struct MessageType AdapterTypes[MAX_ADAPTER_ID];
    for(int i = 0; i < MAX_TYPE_ID; i++) {
        AdapterTypes[i] = MessageTypes[i];
    }

    AdapterTypes[GETLOGFILE_ID] = MessageTypes[MAX_TYPE_ID];
    AdapterTypes[RESPGETLOGFILE_ID] = MessageTypes[MAX_TYPE_ID+1];
    AdapterTypes[LOGBLOCKCOMMAND_ID] = MessageTypes[MAX_TYPE_ID+2];

    // cfsocketconn = connectToCrazyflieGroundStation();
    // if (cfsocketconn == NULL) {
    //     perror("Failed to connect to cf_groundstation");
    //     close(adptsockfd);
    //     unlink(SOCKPATH);
    //     return 1;
    // }
    
    printf("connected to CF groundstation\n");
    while(keepRunning){
        
        //grab a single packet blocking call
        packetsize = getSinglePacket(gssockfd,recvbuf, sizeof(recvbuf));
        if(packetsize <0){
            perror("getSinglePacket returned with a negative value");
            close(adptsockfd);
            unlink(SOCKPATH);
            return 1;
        }
        printf("\tpacktsize %d\n", packetsize);

        //pass the single packet to the DecodePacket()
        DecodePacket(&metad,data, MAX_PACKET_SIZE, recvbuf, packetsize);
        printf("\tdata: %s psize %d msgtype %d\n", data, packetsize, metad.msg_type);


        //call the appropriate callback 
        //example line 87 in MicroCART/quad/src/quad_app/communication.c 
        command_cb* func_ptr = AdapterTypes[metad.msg_type].functionPtr;
        if (metad.msg_type < MAX_ADAPTER_ID && func_ptr) {
            cbreturn = (* func_ptr)(NULL,&metad,data, metad.data_len);
            if(cbreturn <0){
                perror("callback return in error");
            }
        }

        //clear all the buffers and reset vars
        memset(&metad, 0,sizeof(metad));
        memset(data,0,sizeof(data));
        memset(recvbuf,0,sizeof(recvbuf));
    }


}
/** TODO move to adapterlib.c eventually
 * @brief polling logic that will return with a full packet
 * @param fd the socket to read from
 * @param packet an array of max size MAX_PACKET_SIZE to be filled with single packet
 * @param pSize the size of the packet array parameter, must be MAX_PACKET_SIZE
 * @returns size of the packet being put into packet, -1 means error;
 */
size_t getSinglePacket(int fd, uint8_t * packet, int pSize){

    //local vars
    int8_t curLoc = 0;//current index in the packet 
    int8_t error = 0; //used to check error of recvs
    int totalPacketSize;

    //check if array is big enough
    if(pSize < MAX_PACKET_SIZE){
        printf("\t\tpsize = %d MPS = %d", pSize, MAX_PACKET_SIZE);
        perror("getSinglePacket pSize var is not MAX_PACKET_SIZE");
        return -1;
    }

    //clear packet
    memset(packet,0,MAX_PACKET_SIZE);

    //Get to data size bytes to find out total packet size
    while(curLoc<6){
        curLoc += recv(fd, packet,7,MSG_WAITALL);
    }

    //get the total packet size
    totalPacketSize = packetSize(*(packet+6)<<8 | (*(packet +5)));
    printf("\t\ttotal packet size %d\n",totalPacketSize);

    //check that the total packet size isnt unreasonable TODO is this a valid check?
    if(totalPacketSize > MAX_PACKET_SIZE){
        perror("failed packet size sanity check when recving");
        return -1;
    }

    //grab the rest of the single packet
    printf("\t\tattempting to grab %d more bytes\n",totalPacketSize-curLoc);
    printf("\t\tstart at packet[ %d ]\n",curLoc);
    error = recv(fd, packet + curLoc, totalPacketSize-curLoc, MSG_WAITALL);// blocking recv
    if(error == -1){
        perror("error reading packet after data size");
        return -1;
    }

    //debug print full received packet 
    printf("\t\tfull packet ");
    for(int i=0; i <totalPacketSize; i++) {
        printf("%2x ",*(packet+i));
    }
    printf("\n");


    return totalPacketSize; 
}

/**
 * @brief used to calculate entire packet size based off of the variable data size
 * @param dataSize size of data in bytes
 * @return total packet size in bytes
 */
size_t packetSize(int dataSize){
    return 8+dataSize;
}


#ifndef _CF_ADAPTER_H
#define _CF_ADAPTER_H
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <pthread.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stdlib.h>

#include "commands.h"
#include "communication.h"
#include "packet.h"
#include "param.h"
#include "logfile.h"
#include "type_def.h"
#include "cf_callbacks.h"
#include "backend_adapter.h"

struct backend_conn {
    FILE * socket;
    int socketfd;
    size_t len;
    char * buf;
};

void signalHandler(int sigNum);

ssize_t adapterWrite(struct backend_conn *conn, const char * line, size_t psize);

int connectToCrazyflieBackend(struct backend_conn ** conn);

int main(int argc, char *argv[]);

size_t getSinglePacket(int fd, uint8_t * packet, int pSize);

size_t packetSize(int dataSize);

#endif /* _CF_ADAPTER_H */


//#include "adapterlib.h"
#include "cf_callbacks.h"

/* Misc. callbacks */ 
//TODO there should be a better way to do this
extern struct backend_conn * cfsocketconn;
extern struct backend_conn * mcsocketconn;

/**
 * Currently does nothing.
 */
int cb_debug(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length){
    return -1;
}

/**
 * counts the number of packet logs.
 */
int cb_packetlog(struct modular_structs* structs, struct metadata *meta, u8 *data, u16 length) {
    return -1;
}

/**
 * Handles a get packet logs request and sends a response
 * with the packet log data.
 */
int cb_getpacketlogs(struct modular_structs* structs, struct metadata *meta, u8 *data, u16 length) {
    return -1;
}

/*
 * Handles receiving new location updates.
 */
int cb_update(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length){
    return -1;
}

/**
 * This is called on the ground station to begin sending VRPN to the quad.
 */
int cb_beginupdate(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    return -1;
}

int cb_overrideoutput(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    uint8_t cmd[100];
    memset(cmd, 0, sizeof(cmd));
    char dimension;
    ssize_t error;

    //TODO for debugging
    printf("\t\tchecking setpoint ");
    for(int i = 0; i<8; i++){
        printf("%2x ",*(data +i)); 
    }
    printf("\n");

    //check if the connection is initiallized
    if(cfsocketconn == NULL){ 
        perror("\t\tcf groundstation connection not initialized\n");
        return -2;
    }

    printf("\t\tabout to write to cf groundstation\n");

    memcpy(cmd, data, meta->data_len);
    uint8_t packet[64];
    size_t psize = EncodePacket(packet, 64, meta, cmd);
    error = adapterWrite(cfsocketconn,packet, psize);

    return error;
}

/*
 * -----------------------------------------------
 * Callbacks for control network modification/info
 * -----------------------------------------------
 */

// Struct for holding a node ID and either a parameter, output, or input index
struct node_ids {
    int16_t id;
    int16_t sub_id;
};

/*
 * Given a data array, returns a node_ids struct retrieved from the array.
 * Assumes the given array is at least 4 bytes to hold the data.
 */
struct node_ids get_node_ids(u8 *data) {
}

/**
 * Handles a command to set a node parameter on the quad.
 *
 * NOTE:
 * Expects the uart buff to have data in the following format:
 * |--------------------------------------------------------|
 * |  data index ||    0 - 1    |    2 - 3    |    4 - 7    |
 * |--------------------------------------------------------|
 * |   parameter ||   node ID   | node parmID |  param val  |
 * |--------------------------------------------------------|
 * |       bytes ||      2      |      2      |      4      |
 * |--------------------------------------------------------|
 *
 * returns >0 if successful 
 * returns -1 if param not permitted
 * returns -2 other type of error
 */
int cb_setparam(struct modular_structs *structs, struct metadata *meta, uint8_t *data, unsigned short length)
{
    uint8_t cmd[100];
    memset(cmd, 0, sizeof(cmd));
    char dimension;
    ssize_t error;

    //TODO for debugging
    printf("\t\tchecking parameter ");
    for(int i = 0; i<8; i++){
        printf("%2x ",*(data +i)); 
    }
    printf("\n");

    //check if the connection is initiallized
    if(cfsocketconn == NULL){ 
        perror("\t\tcf groundstation connection not initialized\n");
        return -2;
    }


    /*if node is the x, y, or z
    if(*(data) == 9){
        dimension = 'x';
    }
    else if(*(data)== 10){
        dimension = 'y';
    }
    else if(*(data)== 11){
        dimension = 'z';
    }
    else{
        //not supported node to set
        return -1;
    }
    */

    printf("\t\tabout to write to cf groundstation\n");
    //build float value of the the param value
    //send to cf groundstation over socket
    //create command string
    /*
    switch(dimension) {
        case'x':
            //sprintf(cmd, "1x%1.2f",build_float(data+4));
            sprintf(cmd, "1t\n",build_float(data+4));
            error = adapterWrite(cfsocketconn,cmd);
            sprintf(cmd, "1k\n",build_float(data+4));
            error = adapterWrite(cfsocketconn,cmd);
            break;
        case 'y':
            sprintf(cmd, "1y%1.2f",build_float(data+4));
            error = adapterWrite(cfsocketconn,cmd);
            break;
        case 'z':
            sprintf(cmd, "1z%1.2f",build_float(data+4));
            error = adapterWrite(cfsocketconn,cmd);
            break;
        default:
            perror("cb_setparam reached default in switch statement");
            return(-2);
    }
    */
    //sprintf(cmd, "1y%1.2f",build_float(data+4));
    memcpy(cmd, data, meta->data_len);
    uint8_t packet[64];
    size_t psize = EncodePacket(packet, 64, meta, cmd);
    error = adapterWrite(cfsocketconn,packet, psize);

    return error;
}

/**
 * Handles a command to get a controller parameter from the quad.
 *
 * NOTE:
 * Expects the uart buff to have data in the following format:
 * |-------------------------------------------|
 * |  data index ||    0 - 1     |    2 - 3    |
 * |-------------------------------------------|
 * |   parameter ||    node ID   | node parmID |
 * |-------------------------------------------|
 * |       bytes ||       2      |      2      |
 * |-------------------------------------------|
 *
 * Sends a response of type RESPPARAM_ID.
 * The response will have a message ID equal to the one originally received.
 * The data of the response will be in the following format:
 * |--------------------------------------------------------|
 * |  data index ||    0 - 1    |    2 - 3    |    4 - 7    |
 * |--------------------------------------------------------|
 * |   parameter ||   node ID   | node parmID |  param val  |
 * |--------------------------------------------------------|
 * |       bytes ||      2      |      2      |      4      |
 * |--------------------------------------------------------|
 */
int cb_getparam(struct modular_structs *structs, struct metadata *meta, uint8_t *data, unsigned short length)
{
    char cmd[100];
    memset(cmd, 0, sizeof(cmd));
    char dimension;
    ssize_t error;

    //TODO for debugging
    printf("\t\tchecking parameter ");
    for(int i = 0; i<8; i++){
        printf("%2x ",*(data +i)); 
    }
    printf("\n");

    //check if the connection is initiallized
    if(cfsocketconn == NULL){ 
        perror("\t\tcf groundstation connection not initialized\n");
        return -2;
    }

    memcpy(cmd, data, meta->data_len);
    uint8_t packet[64];
    size_t psize = EncodePacket(packet, 64, meta, cmd);
    error = adapterWrite(cfsocketconn,packet, psize);

    char buffer[100];
	ssize_t r = 0;

	//char * bufferptr = buffer;

	while(r<=0) {
        r = read(cfsocketconn->socketfd, buffer, 100);
    }
    
    //printf("ID: %d", buffer[1]);

    uint8_t packetr[256];
    struct metadata mr;
    uint8_t datar[128];
    //ssize_t resultr;
    ssize_t psizer;

    EncodeResponseParam(&mr, datar, 128, buffer);

    psizer = EncodePacket(packetr, 64, &mr, datar);

    error = adapterWrite(mcsocketconn, packetr, psizer);

    //free(datar);

    return error;
}

/**
 * Handles a command to set a node's input source
 *
 * NOTE:
 * Expects the uart buff to have data in the following format:
 * |---------------------------------------------------------------------------|
 * |  data index ||     0 - 1    |     2 - 3     |    4 - 5    |     6 - 7     |
 * |---------------------------------------------------------------------------|
 * |   parameter || dest node ID | dest input ID | src node ID | src output ID |
 * |---------------------------------------------------------------------------|
 * |       bytes ||       2      |       2       |      2      |       2       |
 * |---------------------------------------------------------------------------|
 * 
 * Does not send anything in response.
 */
int cb_setsource(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    return -1;
}

/**
 * Handles a command to get the source of a node's input
 *
 * NOTE:
 * Expects the uart buff to have data in the following format:
 * |---------------------------------------------|
 * |  data index ||    0 - 1     |     2 - 3     |
 * |---------------------------------------------|
 * |   parameter ||    node ID   | node input ID |
 * |---------------------------------------------|
 * |       bytes ||       2      |       2       |
 * |---------------------------------------------|
 *
 * Sends a response of type RESPSOURCE_ID.
 * The response will have a message ID equal to the one originally received.
 * The data of the response will be in the following format:
 * |---------------------------------------------------------------------------|
 * |  data index ||     0 - 1    |     2 - 3     |    4 - 5    |     6 - 7     |
 * |---------------------------------------------------------------------------|
 * |   parameter || dest node ID | dest input ID | src node ID | src output ID |
 * |---------------------------------------------------------------------------|
 * |       bytes ||       2      |       2       |      2      |       2       |
 * |---------------------------------------------------------------------------|
 */
int cb_getsource(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    return -1;
}


/**
 * Handles a command to get a node output value from the quad.
 * Packet structure is the same as getparam
 */
int cb_getoutput(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length)
{
    return -1;
}

/*
 * Handles a request for the list of nodes in the graph
 * For N total nodes, returns data in the following format:
 * The node IDs and type IDs are consecutive shorts
 * The node names are null-separated
 * |---------------------------------------------------------------|
 * |  data index ||   0 - 2*N-1  |  2*N - 4*N-1  | 4*N - (< 4096)  |
 * |---------------------------------------------------------------|
 * |   parameter ||  Node IDs   | Node type IDs  |    Node names   |
 * |---------------------------------------------------------------|
 * |       bytes ||     2*N     |      2*N       |      < 4096     |
 * |---------------------------------------------------------------|
 */
int cb_getnodes(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    return -1;
}

/*
 * Handles adding a new node with a particular type and name
 * Expects the uart buff to have data in the following format:
 * |---------------------------------------------|
 * |  data index ||    0 - 1     |     2 - ?     |
 * |---------------------------------------------|
 * |   parameter ||    type ID   | New node name |
 * |---------------------------------------------|
 * |       bytes ||       2      |       ?       |
 * |---------------------------------------------|
 *
 * Returns the new node ID in the following format:
 * |-----------------------------|
 * |  data index ||    0 - 1     | 
 * |-----------------------------|
 * |   parameter ||    node ID   |
 * |------------------------------
 * |       bytes ||       2      |
 * |-----------------------------|
 */
int cb_addnode(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    return -1;
}

int cb_getlogfile(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    char cmd[100];
    memset(cmd, 0, sizeof(cmd));
    char dimension;
    ssize_t error;

    //check if the connection is initiallized
    if(cfsocketconn == NULL){ 
        perror("\t\tcf groundstation connection not initialized\n");
        return -2;
    }

    memcpy(cmd, data, meta->data_len);
    uint8_t packet[64];
    size_t psize = EncodePacket(packet, 64, meta, cmd);
    error = adapterWrite(cfsocketconn,packet, psize);

    char buffer[256];
    memset(buffer, '\000', 256);
	ssize_t r = 0;

	//char * bufferptr = buffer;

	while(r<=0) {
        r = read(cfsocketconn->socketfd, buffer, 1024);
    }
    
    //printf("ID: %d", buffer[1]);

    uint8_t packetr[r+8];
    memset(packetr, '\000', r);
    struct metadata mr;
    uint8_t datar[r];
    memset(datar, '\000', r);
    //ssize_t resultr;
    ssize_t psizer;

    EncodeResponseLogfile(&mr, datar, r, buffer);

    psizer = EncodePacket(packetr, r+8, &mr, datar);

    error = adapterWrite(mcsocketconn, packetr, psizer);

    return error;
}

int cb_logblockcommand(struct modular_structs *structs, struct metadata *meta, unsigned char *data, unsigned short length) {
    uint8_t cmd[100];
    memset(cmd, 0, sizeof(cmd));
    char dimension;
    ssize_t error;

    //check if the connection is initiallized
    if(cfsocketconn == NULL){ 
        perror("\t\tcf groundstation connection not initialized\n");
        return -2;
    }

    printf("\t\tabout to write to cf groundstation\n");
    memcpy(cmd, data, meta->data_len);
    uint8_t packet[64];
    size_t psize = EncodePacket(packet, 64, meta, cmd);
    error = adapterWrite(cfsocketconn,packet, psize);

    return error;
}

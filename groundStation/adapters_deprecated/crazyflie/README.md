# Adapter Setup

For the Crazyflie Adapter there is a modified CrazyFlie groundstation that must be used to allow communication. 
This is available in the DANC group in the Custom_CrazyFlie_Software repo in the [Matt_test_branch](https://git.ece.iastate.edu/danc/Custom_CrazyFlie_Software/tree/Matt_test_branch).
Clone this repo and navigate into the groundstation-LateralOnQuad. Inside of the src
directory open the quadcopterData.cpp file. There are two variables that you have to check:
radios and crazyflie_info. For radios everything should be uncommented, and for crazyflie_info
make sure that you have the crazyflie that you are intending to use uncommented. The fourth
member of the struct is the channel number that specifies that crazyflie, and this should
match the piece of tape connected to the crazyflie you are using. Then go to the inc 
directory in the quadcopterData.hpp and make sure that NUM_RADIOS and NUM_QUADS are set
correctly, usually 1. Now from base directory in the groundstation run make and there 
should be a crazyflieGroundstation that can be run. When run, a socket will be created by 
the name of crazyflie_groundstation.socket in the same directory. The path to this socket
needs to be specified in [cf_adapter.c](./src/cf_adapter.c#L12).

# HOW TO BUILD:
from the Microcart/groundstation/adapters/crazyflie/ directory 
```bash
    make clean
    make all
```

this will compile the adapter to the ./bin directory

# TO RUN ADAPTER:

First, turn on the crazyflie you want to use and run the Crazyflie groundstation metioned in Adapter Setup. This will connect to and setup the crazyflie and will prompt you to press 
enter. Upon doing so move onto the next steps to run the adapter.


***NOTE to change where the sockets are located you must change the define statements in ./src/cf_adaper.c***

***NOTE 2 Running the executables provided use relative links that will be relative to your current working directory.***

- Ensure that the socket that the adapter is creating will be at the same location that the groundstation will look (see notes above).
- compile the adapter (see HOW TO BUILD above)
- from the Microcart/groundstation/adapters/crazyflie/ directory run the executable with the following command

```bash
    ./bin/cf_adapter
```



#include "adapterlib.h" 

/**
 * @brief polling logic that will return with a full packet
 * @param fd the socket to read from
 * @param packet an array of max size MAX_PACKET_SIZE to be filled with single packet
 * @param pSize the size of the packet array parameter, must be MAX_PACKET_SIZE
 * @returns size of the packet being put into packet, -1 means error;
 */
size_t getSinglePacket(int fd, uint8_t * packet, int pSize){

    //local vars
    int8_t curLoc = 0;//current index in the packet 
    int8_t error = 0; //used to check error of recvs
    int totalPacketSize = packetSize(*(packet+4)<<8 | (*(packet +5)));

    //check if array is big enough
    if(pSize < MAX_PACKET_SIZE){
        printf("\t\tpsize = %d MPS = %d", pSize, MAX_PACKET_SIZE);
        perror("getSinglePacket pSize var is not MAX_PACKET_SIZE");
        return -1;
    }

    //clear packet
    memset(packet,0,MAX_PACKET_SIZE);

    //Get to data size bytes to find out total packet size
    while(curLoc<6){
        curLoc += recv(fd, packet,6,MSG_WAITALL);
    }

    //get the total packet size
    totalPacketSize = packetSize(*(packet+4)<<8 | (*(packet +5)));
    printf("\t\ttotal packet size %d\n",totalPacketSize);

    //check that the total packet size isnt unreasonable TODO is this a valid check?
    if(totalPacketSize > MAX_PACKET_SIZE){
        perror("failed packet size sanity check when recving");
        return -1;
    }

    //grab the rest of the single packet
    printf("\t\tattempting to grab %d more bytes\n",totalPacketSize-curLoc);
    printf("\t\tstart at packet[ %d ]\n",curLoc);
    error = recv(fd, packet + curLoc, totalPacketSize-curLoc, MSG_WAITALL);// blocking recv
    if(error == -1){
        perror("error reading packet after data size");
        return -1;
    }

    //debug print full received packet 
    printf("\t\tfull packet ");
    for(int i=0; i <totalPacketSize; i++) {
        printf("%2x ",*(packet+i));
    }
    printf("\n");


    return totalPacketSize; 
}

/**
 * @brief used to calculate entire packet size ased off of the variable data size
 * @param dataSize size of data in bytes
 * @return total packet size in bytes
 */
size_t packetSize(int dataSize){
    return 8+dataSize;
}

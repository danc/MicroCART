#include "adapterlib.h"

//TODO should be moved to a config file or define
//TODO how do we handle multiple adapters of same type?
//TODO create configfile and add functionality to read from config.
#define SOCKPATH "adapter.socket"

int main(int argc, char *args[]) {

    //create socket and bind
    printf("Adapter started\n\r");
    int adptsockfd;
    struct sockaddr_un adptsockinfo;
    char adptsockpath[] = SOCKPATH;

    adptsockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(adptsockfd < 0){
        perror("failed to create socket\n");
        //TODO check if file already exists and delete it? 
    }
    else{
        printf("created socket\n\r");
    }

    adptsockinfo.sun_family = AF_UNIX;
    strcpy(adptsockinfo.sun_path, SOCKPATH);
    if(bind(adptsockfd, (struct sockaddr *) &adptsockinfo, sizeof(adptsockinfo))){
        perror("failed to bind\n");
    }
    else{
        printf("bound socket\n\r");
    }

    //wait for connection, connect, and set up receive buffer
    if(listen(adptsockfd,16)==-1){
        close(adptsockfd);
        unlink(SOCKPATH);
        perror("failed listen\n");
        exit(1);
    }
    else {
        printf("listening on %s\n", adptsockpath);
    }

    struct sockaddr_un gssockinfo;
    int len;
    int gssockfd = accept(adptsockfd, (struct sockaddr *) &gssockinfo, &len);
    if(gssockfd == -1){
        close(adptsockfd);
        unlink(SOCKPATH);
        perror("failed to accept groundstation connection\n");
        exit(1);	
    }
    else {
        printf("received a connection\n\r");
    }

    char recvbuf[1000];
    memset(recvbuf,0,1000);
    int keepRunning = 1;

    /* needed files
     * MicroCART/groundStation/src/backend/packet.c - DecodePacket()
     * MicroCART/quad/inc/commands.h - metadata struct
     * MicroCART/quad/src/commands/commands.c - MessageTypes, findCommand()
     *  - callbacks.h. Typedef for command_cb
     *  - cb_default.h. Implementation of cb_default.
     *  - callbacks.c file. Contains implemented callbacks.
     */

    struct metadata metad; //metadata struct
    uint8_t data[MAX_PACKET_SIZE]; //array used to store the data bytes of the packet
    memset(data,0,sizeof(data));
    ssize_t data_len = 0;
    ssize_t packetsize = -1; 
    int cbreturn = 0;

    while(keepRunning){
        printf("in main loop\n");
        //grab a single packet blocking call
        packetsize = getSinglePacket(gssockfd,recvbuf, sizeof(recvbuf));
        printf("\tpacktsize %d\n", packetsize);


        //pass the single packet to the DecodePacket()
        DecodePacket(&metad,data, MAX_PACKET_SIZE, recvbuf, packetsize);
        printf("\tdata: %s psize %d msgtype %d\n", data, packetsize, metad.msg_type);


        //call the appropriate callback example line 87 in MicroCART/quad/src/quad_app/communication.c 
        command_cb* func_ptr = MessageTypes[metad.msg_type].functionPtr;
        if (metad.msg_type < MAX_TYPE_ID && func_ptr) {
            cbreturn = (* func_ptr)(NULL,&metad,data, metad.data_len); 
            if(cbreturn <0){
                perror("callback return in error");
            }
        }

        //clear all the buffers and reset vars
        memset(&metad, 0,sizeof(metad));
        memset(data,0,sizeof(data));
        memset(recvbuf,0,sizeof(recvbuf));



    }

    close(adptsockfd);
    unlink(SOCKPATH);

}


#ifndef _ADAPTERLIB_H
#define _ADAPTERLIB_H

#include "packet.h" //for parsing and encoding
#include "commands.h" //for messagetypeid
#include <stdint.h>

/**
 * @brief polling logic that will return with a full packet from the groundstation backend
 * @param fd the socket to read from
 * @param packet an array of max size MAX_PACKET_SIZE to be filled with single packet
 * @param pSize the size of the packet array parameter, must be MAX_PACKET_SIZE
 * @returns size of the packet being put into packet, -1 means error;
 */
size_t getSinglePacket(int fd, uint8_t * packet, int pSize);

/**
 * @brief used to calculate entire packet size ased off of the variable data size
 * @param dataSize size of data in bytes
 * @return total packet size in bytes
 */
size_t packetSize(int dataSize);


#endif /* _ADAPTERLIB_H */ 



Getting Started - Ground Station
================================

The Ground Station as a whole allows all the different systems and hardware to
communicate with each other. The Ground Station has four main components including:

	- Command Line Interface (CLI)
	- Graphical User Interface (GUI)
	- Frontend
	- Backend

Both the CLI and GUI are user interfaces. The GUI uses all the same commands as the 
CLI but has extra functionality. The GUI shows the controls graph, provides extra
navigation help, and will soon support real time graphing of the data. The Frontend
does all the reading from and writing to the backend via a socket connected to the 
backend. The backend is primarily a server that manages all connections and where all
data passes through between the VRPN (Camera System), the quad, and user interfaces.
**Figure 1** shows the main components and their interaction. Both user interfaces use
the frontend to manage the writing and parsing of return messages from the backend.

![GroundStationComponents](images/groundStationComponents.jpg)

**Figure 1** Ground Station main components

**Figure 2** Shows a sample connection with the VRPN system, a packet sent or recieved
from the quad, an initialization file, and the Crazyflie adapter. The initialization file
and Crazyflie adapter are both proposed changes to the system. The VRPN connection gets
trackable information from the camera system with an identifier for the trackable object.
This allows for distinguishing between multiple trackables. The packets used between the 
quad and the ground station are custom packets created specifically for reliable communication
over wifi to the quad. The initialization file will help with connecting multiple quads, or
other flying machines to be connected into the lab setup. The Crazyflie adapter will allow
the Crazyflies to work with the MicroCART system.


![GroundStationDiagram](images/groundStationDiagram.jpg)

**Figure 2** Ground Station connections and packets

Ground Station Commands
-----------------------

All commands can be used directly when using the CLI rather than the GUI. Shown below are the main commands
as symbolic links, they can also be used by the CLI like follows `./Cli getsource`.

```
./getsource
./setsource
./getparam
./setparam
./getouput
./getnode
./addnode
```

Example Command and Flow through Ground Station
-----------------------------------------------

Here is an example of how a press of the "Send to quad" button in the GUI is processed and the command sent to the quad.


![GroundStationFlowchart1](images/buttonPressCallStackFlowchart.png)  

**Figure 3** Ground Station call stack flowchart


/* mmu_cfg.c */
#include "mmu.h"
#include "mmu_cfg.h"

/* Page table configuration array */
struct ptc_t pt_config[NUM_PT_CONFIGS] =
{
    { /* Code region (Read only) */
        .addr = 0x10000000ULL,
        .size = SIZE_2M,
        .executable = XN_OFF,
        .sharable = NON_SHARABLE,
        .permission = READ_WRITE,
        .policy = TYPE_MEM_CACHE_WB,
    },
    { /* Data region */
        .addr = 0x10200000ULL,
        .size = SIZE_4M,
        .executable = XN_ON,
        .sharable = INNER_SHARABLE,
        .permission = READ_WRITE,
        .policy = TYPE_MEM_CACHE_WB,
    },
    { /* Data region (Shared for OpenAMP) */
        .addr = 0x10600000ULL,
        .size = SIZE_2M,
        .executable = XN_ON,
        .sharable = OUTER_SHARABLE,
        .permission = READ_WRITE,
        .policy = TYPE_MEM_CACHE_WT,
    },
    { /* Page table (Private) */
        .addr = 0x10800000ULL,
        .size = SIZE_2M,
        .executable = XN_ON,
        .sharable = NON_SHARABLE,
        .permission = READ_WRITE,
        .policy = TYPE_MEM_CACHE_WB,
    },
    { /* Peripehral devices (MMIO) */
        .addr = 0x7E000000ULL,
        .size = SIZE_64M,
        .executable = XN_ON,
        .sharable = OUTER_SHARABLE,
        .permission = READ_WRITE,
        .policy = TYPE_DEVICE,
    },
};


#include <stddef.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

#include "uart.h"

/* 
 * Prototypes for the standard FreeRTOS callback/hook functions implemented
 * within this file.
 */
void vApplicationMallocFailedHook( void );
void vApplicationIdleHook( void );

static inline void io_halt(void)
{
    asm volatile ("wfi");
    return;
}
/*-----------------------------------------------------------*/

void TaskA(void *pvParameters)
{
    (void) pvParameters;

    for( ;; )
    {
        uart_puts("\r\n");
        uart_puthex(xTaskGetTickCount());
        uart_puts("\r\n");
        vTaskDelay(1000 / portTICK_RATE_MS);
    }

    return; /* Never reach this line */
}

/*-----------------------------------------------------------*/

TimerHandle_t timer;
uint32_t count=0;
void interval_func(TimerHandle_t pxTimer)
{
    (void) pxTimer;
    uint8_t buf[2] = {0};
    uint32_t len = 0;

    len = uart_read_bytes(buf, sizeof(buf) - 1);
    if (len)
        uart_puts((char *)buf);

    return;
}
/*-----------------------------------------------------------*/

#define ARM_IO_BASE   0x3F000000
#define GPIO_BASE     (ARM_IO_BASE + 0x200000)

#define GPIO_FSEL0    (GPIO_BASE + 0x00)

#define GPIO_SET0     (GPIO_BASE + 0x1C)
#define GPIO_CLR0     (GPIO_BASE + 0x28)


typedef unsigned int uint;

//-------------------------------------------------------------------------
void ledblink()
{
  const uint pin = 17;

  uint reg1 = GPIO_FSEL0 + ((pin / 10) << 2);
  uint shift = (pin % 10) * 3;

  uint val = *(volatile uint *)reg1;
  val &= ~(7 << shift);
  val |=  (1 << shift);
  *(volatile uint *)reg1 = val;

  uint reg_offset = (pin >> 5) << 2; // (pin / 32) * 4;

  register volatile uint * reg_offset0 = (uint *)(GPIO_CLR0 + reg_offset);
  register volatile uint * reg_offset1 = (uint *)(GPIO_SET0 + reg_offset);

  register uint reg_mask = 1 << (pin & 0x1f); // % 32

  while ( 1 )
  {
    *reg_offset0 = reg_mask;
    *reg_offset1 = reg_mask;
  }
}


void main(void)
{
    TaskHandle_t task_a;
    
    ledblink();
    //uart_init();
    //uart_puts("\r\n****************************\r\n");
    //uart_puts("\r\n    FreeRTOS UART Sample\r\n");
    //uart_puts("\r\n****************************\r\n");

    xTaskCreate(TaskA, "Task A", 512, NULL, 0x10, &task_a);

    timer = xTimerCreate("print_every_10ms",(10 / portTICK_RATE_MS), pdTRUE, (void *)0, interval_func);
    if(timer != NULL)
    {
         xTimerStart(timer, 0);
    }

    vTaskStartScheduler();
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
}

/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
}

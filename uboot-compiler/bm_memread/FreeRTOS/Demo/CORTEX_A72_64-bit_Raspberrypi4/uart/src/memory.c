#include "memory.h"

void shm_loop()
{
    volatile int* shared_memory = (volatile int*) SHARED_MEMORY_ADDRESS;
    while(1)
    {
        if(*shared_memory == 0xFF)
        {
            uart_puthex(*(shared_memory + 8));
            uart_putchar(*(shared_memory + 8));
            uart_putchar(*(shared_memory + 9));
            uart_putchar(*(shared_memory + 10));
            uart_putchar(*(shared_memory + 11));
            
            uart_puts("\r\noutput read\r\n");
            *shared_memory = 0x00;
        }
    }
}
/* uart.c */
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "board.h"
#include "interrupt.h"
#include "semphr.h"
#include "uart.h"

/* PL011 UART on Raspberry pi 4B */
#define UART_BASE  (0xFE201400U) /* UART2 */
#define UART_DR   (*(volatile unsigned int *)(UART_BASE))
#define UART_FR   (*(volatile unsigned int *)(UART_BASE+0x18U))
#define UART_IBRD (*(volatile unsigned int *)(UART_BASE+0x24U))
#define UART_FBRD (*(volatile unsigned int *)(UART_BASE+0x28U))
#define UART_LCRH (*(volatile unsigned int *)(UART_BASE+0x2CU))
#define UART_CR   (*(volatile unsigned int *)(UART_BASE+0x30U))
#define UART_IFLS (*(volatile unsigned int *)(UART_BASE+0x34U))
#define UART_IMSC (*(volatile unsigned int *)(UART_BASE+0x38U))
#define UART_ICR  (*(volatile unsigned int *)(UART_BASE+0x44U))

/* GPIO */
#define GPIO_BASE (0xFE200000U)
#define GPFSEL0   (*(volatile unsigned int *)(GPIO_BASE))
#define GPIO_PUP_PDN_CNTRL_REG0 (*(volatile unsigned int *)(GPIO_BASE+0xE4U))

struct UARTCTL {
    SemaphoreHandle_t tx_mux;
    QueueHandle_t     rx_queue;
};
struct UARTCTL *uartctl;

//-------------------------------------------------------------------------
void pinblink(uint pin, int on)
{

  uint reg1 = GPIO_FSEL0 + ((pin / 10) << 2);
  uint shift = (pin % 10) * 3;

  uint val = *(volatile uint *)reg1;
  val &= ~(7 << shift);
  //Set for output
  val |=  (1 << shift);
  *(volatile uint *)reg1 = val;

  uint reg_offset = (pin >> 5) << 2; // (pin / 32) * 4;

  register volatile uint * reg_offset0 = (uint *)(GPIO_CLR0 + reg_offset);
  register volatile uint * reg_offset1 = (uint *)(GPIO_SET0 + reg_offset);

  register uint reg_mask = 1 << (pin & 0x1f); // % 32
  if(on)
  {
    *reg_offset1 = reg_mask;
  }
  else 
  {
    *reg_offset0 = reg_mask;
  }
}

int pinread(uint pin) {
  uint reg1 = GPIO_FSEL0 + ((pin / 10) << 2);
  uint shift = (pin % 10) * 3;

  uint val = *(volatile uint *)reg1;
  val &= ~(7 << shift);
  //Set for input
  val |=  (0 << shift);
  *(volatile uint *)reg1 = val;

  uint reg_offset = (pin >> 5) << 2; // (pin / 32) * 4;

  register volatile uint * reg_offset0 = (uint *)(GPIO_LEV0 + reg_offset);

  register uint reg_mask = 1 << (pin & 0x1f); // % 32
  return *reg_offset0 & reg_mask;
}
    
    

void uart_putchar(uint8_t c)
{
    // xSemaphoreTake(uartctl->tx_mux, (portTickType) portMAX_DELAY);
    // /* wait until tx becomes idle. */
    // while ( UART_FR & (0x20) ) { }
    // UART_DR = c;
    // asm volatile ("isb");
    // xSemaphoreGive(uartctl->tx_mux);
}
/*-----------------------------------------------------------*/

void uart_putchar_isr(uint8_t c)
{
    // xSemaphoreTakeFromISR(uartctl->tx_mux, NULL);
    // /* wait mini uart for tx idle. */
    // while ( (UART_FR & 0x20) ) { }
    // UART_DR = c;
    // asm volatile ("isb");
    // xSemaphoreGiveFromISR(uartctl->tx_mux, NULL);
}
/*-----------------------------------------------------------*/

void uart_puts(const char* str)
{
    // for (size_t i = 0; str[i] != '\0'; i ++)
    //     uart_putchar((uint8_t)str[i]);
}
/*-----------------------------------------------------------*/

void uart_puthex(uint64_t v)
{
    // const char *hexdigits = "0123456789ABCDEF";
    // for (int i = 60; i >= 0; i -= 4)
    //     uart_putchar(hexdigits[(v >> i) & 0xf]);
}
/*-----------------------------------------------------------*/

uint32_t uart_read_bytes(uint8_t *buf, uint32_t length)
{
    // uint32_t num = uxQueueMessagesWaiting(uartctl->rx_queue);
    // uint32_t i;

    // for (i = 0; i < num || i < length; i++) {
    //     xQueueReceive(uartctl->rx_queue, &buf[i], (portTickType) portMAX_DELAY);
    // }

    //return i;
    return 0;
}
/*-----------------------------------------------------------*/

void uart_isr(void)
{
    // /* RX data */
    // if( !(UART_FR & (0x1U << 4)) ) {
    //     uint8_t c = (uint8_t) 0xFF & UART_DR;
    //     xQueueSendToBackFromISR(uartctl->rx_queue, &c, NULL);
    // }
}
/*-----------------------------------------------------------*/

/* 
 * wait_linux()
 * This is a busy loop function to wait until Linux completes GIC initialization
 */
static void wait_linux(void)
{
    pinblink(17, 0);
    pinblink(27, 0);
    pinblink(23, 1);
    pinblink(24, 1);
    
    while(pinread(22) == 0)
    {
        pinblink(17, pinread(22));
        pinblink(27, pinread(22));
        pinblink(23, pinread(22));
        pinblink(24, pinread(22));
    }
    
    pinblink(17, 0);
    pinblink(27, 1);
    pinblink(23, 0);
    pinblink(24, 1);
    return;
}
/*-----------------------------------------------------------*/

void uart_init(void)
{
//     uint32_t r;

//     /* GPIO0 GPIO1 settings for UART2 */
//     r = GPFSEL0;
//     r &= ~((0x7U << 3) | 0x7U);
//     r |= ((0x3U << 3) | 0x3U); /* ALT4 */
//     GPFSEL0 = r;

//     r = GPIO_PUP_PDN_CNTRL_REG0;
//     r &= ~((0x3U << 2) | 0x3U);
//     GPIO_PUP_PDN_CNTRL_REG0 = r;

//     /* PL011 settings with assumption of 48MHz clock */
//     UART_ICR  = 0x7FFU;         /* Clears an interrupt */
//     UART_IBRD = 0x1AU;          /* 115200 baud */
//     UART_FBRD = 0x3U;
//     UART_LCRH = ((0x3U << 5) | (0x0U << 4));    /* 8/n/1, FIFO disabled */
//     UART_IMSC = (0x1U << 4);    /* RX interrupt enabled */
//     UART_CR   = 0x301;          /* Enables Tx, Rx and UART */
//     asm volatile ("isb");

//     uartctl = pvPortMalloc(sizeof (struct UARTCTL));
//     uartctl->tx_mux = xSemaphoreCreateMutex();
//     uartctl->rx_queue = xQueueCreate(16, sizeof (uint8_t));

#if defined(__LINUX__)
//     uart_puts("\r\nWaiting until Linux starts booting up ...\r\n");
     wait_linux();
 #endif

//     isr_register(IRQ_VC_UART, UART_PRIORITY, (0x1U << 0x3U), uart_isr);
//     return;
}
/*-----------------------------------------------------------*/


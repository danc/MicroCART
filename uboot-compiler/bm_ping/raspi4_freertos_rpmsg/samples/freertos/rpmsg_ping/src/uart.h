#define UART_PRIORITY (0xA0U)


#define ARM_IO_BASE   0x3F000000
#define GPIO_BASEPIN     (ARM_IO_BASE + 0x200000)

#define GPIO_FSEL0    (GPIO_BASEPIN + 0x00)

#define GPIO_SET0     (GPIO_BASEPIN + 0x1C)
#define GPIO_CLR0     (GPIO_BASEPIN + 0x28)
#define GPIO_LEV0     (GPIO_BASEPIN + 0x34)


typedef unsigned int uint;


void uart_putchar(uint8_t c);
void uart_puts(const char* str);
void uart_puthex(uint64_t v);
uint32_t uart_read_bytes(uint8_t *buf, uint32_t length);
void uart_init(void);
void pinblink(uint pin, int on);

int pinread(uint pin);

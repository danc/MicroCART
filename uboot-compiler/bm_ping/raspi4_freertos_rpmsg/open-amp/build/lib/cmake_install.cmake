# Install script for directory: /home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/lib

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "TRUE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/build/lib/libopen_amp.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/lib/include/openamp")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/build/lib/virtio/cmake_install.cmake")
  include("/home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/build/lib/rpmsg/cmake_install.cmake")
  include("/home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/build/lib/remoteproc/cmake_install.cmake")
  include("/home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/build/lib/proxy/cmake_install.cmake")

endif()


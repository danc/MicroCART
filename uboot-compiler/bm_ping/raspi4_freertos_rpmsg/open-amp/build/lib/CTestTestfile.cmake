# CMake generated Testfile for 
# Source directory: /home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/lib
# Build directory: /home/bitcraze/MicroCART/uboot-compiler/bm_ping/raspi4_freertos_rpmsg/open-amp/build/lib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("virtio")
subdirs("rpmsg")
subdirs("remoteproc")
subdirs("proxy")

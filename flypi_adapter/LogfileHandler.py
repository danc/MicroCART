
from time import time
from typing import List
import time
import os

from multiprocessing import Queue
#import groundstation_socket as gs
from cflib.crazyflie.log import LogConfig




class LogfileHandler:
    """
    Handles all interactions with cflib.
    """

    def __init__(self):
        """
        Initialize the start time, the logging queue which is given to the
        plotting window, and set the synchronous crazyflie connection by
        default to None. This should be set to the crazyflie when connecting
        to one.
        """

        # Get the start time, so that the timestamp returned to the user will
        # be in seconds since startup.
        self.start_time = time.time()

        self.scf = None
        self.is_connected = False
        self.param_callback_count = 0
        self.logging_configs = []
        self.data_log = None
        self.data_log_name = ""
        self.header_id = -1
        self.header = []
        self.logging_queue = Queue()

        self.create_log_data_file()

    
    """
    Creates a file and writes the toc to it
    """
    def CopyTocToFile(self, toc :dict, isParams):
        if isParams:
            logType = "Param"
        else:
            logType = "Log"

        filename = os.getcwd()    
        filename += f"/logs/cflie1_{logType}_toc_{time.strftime('%Y_%m_%d_%H:%M:%S', time.localtime())}.txt"

        types = {'uint8_t'  : 0x08,
                 'uint16_t' : 0x09,
                 'uint32_t' : 0x0A,
                 'uint64_t' : 0x0B,
                 'int8_t'   : 0x00,
                 'int16_t'  : 0x01,
                 'int32_t'  : 0x02,
                 'int64_t'  : 0x03,
                 'FP16'     : 0x05,
                 'float'    : 0x06,
                 'double'   : 0x07}

        file = open(filename, "w")
        file.write(f"{logType} ID\tType\tGroup\tIdentifier Name\t\n")
        file.close()
        file = open(filename, "a")
        for group in list(toc.keys()):
            for name in list(toc[group].keys()):
                file.write(f"{toc[group][name].ident}\t{types.get(toc[group][name].ctype)}\t{group}\t{name}\n")

                   

        file.close()
        return filename

    
    """
    Reads all of the logging blocks out of the logging blocks file and then converts them into 
    the cflib LogConfig objects to store in a array for future use.
    """
    def read_all_active_blocks(self, scf):
        active_blocks = []
        logBlockFile = open("/home/bitcraze/Desktop/groundstation/crazyflie_groundstation/loggingBlocks.txt", "r")
        line = "BEGIN"

        # read until EOF
        while line != "":
            line = logBlockFile.readline()
            formatLine = line.strip()

            # Don't read comments or empty lines
            if len(formatLine) == 0 or formatLine[0] == '#':
                continue

            #A new block is being read
            elif formatLine == "START BLOCK":
                data = []
                i = 0
                newLine = logBlockFile.readline().strip()

                #continue until end block is found
                while newLine != "END BLOCK":
                    #Block is too short so raise an exception and stop execution
                    if newLine == "" or newLine == "END BLOCK":
                        print("New Line: " + newLine)
                        raise Exception("loggingBlocks.txt is not formatted correctly")
                        
                    if i == 0 or i == 2:
                        data.append(int(newLine)) 
                    else:
                        data.append(newLine)
                    newLine = logBlockFile.readline().strip()
                    i += 1

                #Block had no end marker so raise an exception
                if logBlockFile.readline().strip() == "START BLOCK":
                    print(data)
                    raise Exception("loggingBlocks.txt is not formatted correctly")

                #Convert the data into LogConfig objects and set them up correctly
                config = LogConfig(data[1], data[2])
                for i in range(3, len(data)):
                    config.add_variable(data[i], 'float')
                config.data_received_cb.add_callback(
                self.logging_callback)
                scf.cf.log.add_config(config)
                active_blocks.append(config)
                
        self.add_config_headers(active_blocks)
        return active_blocks
        
    #Create a new data log when the program starts to hold logs.
    def create_log_data_file(self):
        self.data_log_name = os.getcwd()
        self.data_log_name += f"/logs/cflie1_{time.strftime('%Y_%m_%d_%H:%M:%S', time.localtime())}.txt"
        self.data_log = open(self.data_log_name, 'w')
        self.data_log.close()
        self.data_log = open(self.data_log_name, 'a')
        self.data_log.write("#Crazyflie\r")
        self.data_log.write("#Controller:Unknown\r")
        self.data_log.flush()

    #Add a new header when variables are changed.
    def add_config_headers(self, config_list: List[LogConfig]):
        self.header = []
        self.header_id += 1
        header = "#" + str(self.header_id) + "\ttime"
        for config in config_list:
            for variable in config.variables:
                header += "\t" + variable.name
                self.header.append(variable.name)
        self.data_log.write(header + "\r")
        self.data_log.flush()

    #Take a line of data points then write them to the file in the correct order.
    # If there is no data for a given variable then put 'nan'    
    def write_data_points(self, data):
        line = str(data[0]['timestamp']) + "\t"
        reorganize = [ 'nan' ] * 12
        for point in data:
            try:
                reorganize[self.header.index(point['signal'])] = point['data']
            except:
                print('No header for ' + point['signal'])


        for flt in reorganize:
            if flt == 'nan':
                line += 'nan\t'
            else:
                line += str('%.4f'%(flt)) + "\t"
        line += "\t\r"
        #print(line)
        self.data_log.write(line)
        self.data_log.flush()

    #Function is called whenever a data packet is recieved from the drone. The data is then unpacked and placed into the logging queue.
    def logging_callback(self, _timestamp, data, _logconf):
        """ Whenever data comes in from the logging, it is sent here,
        which routes it into our specific format for the logging queue. """

        timestamp1 = time.time() - self.start_time
        for key in data.keys():
            value_pair = {'timestamp': timestamp1, 'data': data[key],
                          'signal': key}
            self.logging_queue.put(value_pair)
                    





"""
crazyflie_connection handles the actual interaction with the crazyflie.
The only reason it exists is to serve as an intermediary between the groundstation
and the crazyflie itself so that it can handle all interactions with the
cflib library.
"""

from multiprocessing import Process, Queue
from time import time
import time
import struct

import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
import uCartCommander
from groundstation_socket import MessageTypeID
from SetpointHandler import SetpointHandler
from LogfileHandler import LogfileHandler


class CrazyflieConnection:

    """
    Handles all interactions with cflib.
    """

    def __init__(self):
        

        # Get the start time, so that the timestamp returned to the user will
        # be in seconds since startup.
        self.start_time = time.time()

        self.scf = None
        self.is_connected = False
        self.param_callback_count = 0
        self.logging_configs = []
        self.logging_thread = None
        self.stop_thread = False
        self.setpoint_handler = SetpointHandler()
        self.logfile_handler = LogfileHandler()
        
        self.timestamp = 0

    def connect(self, uri: str):
        """
        Handles connecting to a crazyflie. Bitcraze has excellent
        documentation on how to use the synchronous crazyflie object in order
        to send setpoints, set parameters or retrieve logging.
        :param uri: Radio channel
        """
        cflib.crtp.init_drivers()
        self.scf = SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache'))
        self.scf.open_link()
        self.scf.wait_for_params()
        self.iC_connected = True
        print("Connect quad")

        # sets commander
        self.scf.cf.commander = uCartCommander.Commander(self.scf.cf)

        self.logging_configs = self.logfile_handler.read_all_active_blocks(self.scf)

        # connect the crazyflie commander to the setpoint handler
        # refresh the logging page so that it displays the toc
        # refresh the parameter page so that it displays the correct information
        self.setpoint_handler.setCommander(self.scf.cf.commander)
        self.setpoint_handler.startSetpointThread()

    def OverrideOuput(self, command): 
        """Sends all setpoints for a given amount of time"""

        # Gets the data from the command packet
        mode = command['data'][0]
        time = command['data'][1:5] # Currently sent every 20ms by setpoint_handler may change
        thrust =  command['data'][5:9]
        pitch =  command['data'][9:13]
        roll =  command['data'][13:17]
        yaw =  command['data'][17:21]

        try:
            time = struct.unpack('f', bytes(time))[0]
            yaw = struct.unpack('f', bytes(yaw))[0]
            pitch = struct.unpack('f', bytes(pitch))[0]
            roll = struct.unpack('f', bytes(roll))[0]
            thrust = struct.unpack('f', bytes(thrust))[0]
        except ValueError:
            raise Exception

        # Check that setpoint_handler has a commander set
        if self.setpoint_handler.commander:
            # Lets crazyflie know we are about to start flying
            self.setpoint_handler.commander.start_flying()

            if mode == 0: # Stop?
                self.setpoint_handler.stopFlying()
            elif mode == 2: # Rate
                self.setpoint_handler.setRateMode()
            elif mode == 1: # Angle
                self.setpoint_handler.setAttitudeMode()
            elif mode == 3: # Mixed
                self.setpoint_handler.setMixedAttitudeMode()
            elif mode == 4: # Position
                raise Exception # Not implemented
            else :
                raise Exception

            #If no time was specified set the setpoint to expire in 0.1 secs
            if time == 0.0:
                time = 0.1

            # If the setpoint is identical to the last one, only update the time
            if self.setpoint_handler.setpoint.pitch != pitch or self.setpoint_handler.setpoint.yaw != yaw or self.setpoint_handler.setpoint.roll != roll or self.setpoint_handler.setpoint.thrust != thrust:
                self.setpoint_handler.setSetpoint(yaw, pitch, roll, thrust)
            self.setpoint_handler.setpoint_time = time
            self.setpoint_handler.curr_time = 0

    def SetParam(self, command):
        """ Set a crazyflie parameter value. """
        name = int.from_bytes(command['data'][2:4], 'little')
        value = struct.unpack('f', bytes(command['data'][4:8]))[0]
        try:
            if self.scf.is_link_open():
                element = self.scf.cf.param.toc.get_element_by_id(name)
                full_name = element.group + "." + element.name
                cf = self.scf.cf

                cf.param.set_value(full_name, value)

        except AttributeError:
            print("Nothing connected")

    def GetParam(self, command, outputQueue: Queue): #group: str, name: str,
        """ Retrieve parameter value from crazyflie toc. """
        #Bytes 0 and 1 are node ID, ie group
        #Bytes 2 and 3 are node paramID, ie name
        print("Getting Param...")
        name = int.from_bytes(command['data'][2:4], 'little')

        try:
            if self.scf.is_link_open():
                element = self.scf.cf.param.toc.get_element_by_id(name)
                full_name = element.group + "." + element.name
                actual = self.scf.cf.param.get_value(full_name)

        except AttributeError:
            actual = -1.234567
            #return -1.234567890  # 1234567890 should be pretty obvious that
            # something has gone wrong.
            pass
        
        #Create a packet with the parameter to send back.
        data = bytearray()
        data += command['data'][0:2]
        data += command['data'][2:4]
        actual = float(actual)
        flBytes = struct.pack('f', actual)
        data += flBytes
        responsedata = {
        "msg_type": (MessageTypeID.RESPPARAM_ID),
        "msg_id": command['msg_id'],
        "data_len": 8,
        "data": data
        }
        outputQueue.put(responsedata)

        
    def get_logging_toc(self):
        """ Retrieve entire logging table of contents. Used in order to
        display list in logging tab. """

        try:
            if self.scf.is_link_open():
                toc = self.scf.cf.log.toc.toc
                return toc
            else:
                return []
        except AttributeError:
            pass
        return []

    def get_param_toc(self):
        """ Retrieve entire param table of contents. Used in order to
        display params.  """

        try:
            if self.scf.is_link_open():
                toc = self.scf.cf.param.toc.toc
                return toc
        except AttributeError:
            pass
        return {}

    """
    The GUI needs a path to a file to read logging and parameter variables. This functions returns the necessary path.
    """
    def GetLogFile(self, command, outputQueue: Queue): 
        print("Getting LogFile...")
        id = command['data'][0]
        data = bytearray()
        if id == 0: # logdata?
            if self.logfile_handler.data_log_name == "":
                self.logfile_handler.create_log_data_file()
            print(self.logfile_handler.data_log_name)
            filename = self.logfile_handler.data_log_name
            data += bytes("_" + filename, 'utf-8')
            
        elif id == 1: # param toc
            params = self.get_param_toc()
            filename = self.logfile_handler.CopyTocToFile(params, True)
            
            data += bytes("_" + filename, 'utf-8')
            
        elif id == 2: # logging toc
            logs = self.get_logging_toc()
            filename = self.logfile_handler.CopyTocToFile(logs, False)
            data += bytes("_" + filename, 'utf-8')

        elif id == 3: # current data headers 
            header = "_" + str(self.logfile_handler.header_id) + ":,time,"
            for config in self.logging_configs:
                for variable in config.variables:
                    header += variable.name + ","
            data += bytes(header, 'utf-8')

        elif id == 4: # state of test stand connection
            data += bytes("_false", 'utf-8')
        
        else:
            raise Exception
        responsedata = {
            "msg_type": (MessageTypeID.RESPLOGFILE_ID),
            "msg_id": command['msg_id'],
            "data_len": len(data),
            "data": data
        }
        outputQueue.put(responsedata)
        
    def LogBlockCommand(self, command): 
        print("Log Block Command")
        id = command['data'][0]
        if id == 0: # Delete all log blocks
            self.delete_log_blocks()
        elif id == 1: # refresh log blocks
            self.delete_log_blocks()
            self.logging_configs = self.logfile_handler.read_all_active_blocks(self.scf)
        elif id == 2: #Read all log blocks
            self.logging_configs = self.logfile_handler.read_all_active_blocks(self.scf)
        elif id == 3: # delete a specific log block
            block_id = command['data'][1]
            self.logging_configs.remove(self.logging_configs[block_id])
        elif id == 4: #Not needed by the current implementation
            print(4)
            
        elif id == 5: #Not needed by the current implementation
            print(5)
        elif id == 8: #start logging
            self.enable_logging()
            self.start_logging()
        elif id == 9: # stop logging
            self.stop_logging()
            self.disable_logging()
            
        

    def enable_logging(self):
        """ Begins logging all configured logging blocks. This is used from
        the controls tab when hitting begin logging. """
        for i in range(0, len(self.logging_configs)):
            self.logging_configs[i].start()
        

    def disable_logging(self):
        """ Stops logging all configured logging blocks. This is used from
        the controls tab when hitting pause logging. """
        for i in range(0, len(self.logging_configs)):
            self.logging_configs[i].stop()
    
    def delete_log_blocks(self):
        self.logging_configs = []
    
    #Begin the other process to log everything
    def start_logging(self):
        self.stop_thread = False
        self.logging_thread = Process(target=self.continous_log, args=(self.logfile_handler.logging_queue, self.logfile_handler,))
        self.logging_thread.start()
        

    #Stop the logging process
    def stop_logging(self):
        self.logging_thread.join(timeout=1.0)
        self.stop_thread = True

    #Continously log 
    def continous_log(self, logging_queue, logfile_handler :LogfileHandler):
        print("Starting thread")
        while True:
            if logging_queue.qsize() > 3:
                data = []
                for i in range(0, 12):
                    point = logging_queue.get()
                    data.append(point)
                #print(data)
                logfile_handler.write_data_points(data)

    #Disconnect from the crazyflie
    def disconnect(self):
        print("Disconnect quad")
        if self.is_connected:
            self.scf.close_link()
            self.scf = None
            self.is_connected = False
        self.logging_thread.kill()


    



from queue import Queue
from threading import Thread
#from groundstation_socket import GroundstationSocket, MessageTypeID
#from crazyflie_connection import CrazyflieConnection
from memory_handler import MemoryHandler
import sys


class main():
    def __init__(self) -> None:

        #incoming and outgoing queues
        self.inputQueue = Queue()
        self.outputQueue = Queue()
        self.shm = MemoryHandler()
        

    def start(self, radio_channel):
        #print("starting cflib groundstation")
        #gs = GroundstationSocket()

        #Begin the thread to handle incoming messages.
        #self.inThread = Thread(target = gs.groundstation_connect, args = (self.inputQueue,))
        #self.inThread.start()
        self.shm.map_shm()
        if self.shm.memory is not None:
            data = bytes([1,2,3,4])
            for i in range(5):
                self.shm.shm_queue.put(data)
            print("data put")
            self.shm.shm_loop()
        else:
            print("Shared memory wasn't mapped")



    def processCommands(self):
        while True:
            #Call the appropriate function based on the message type of the incoming message on the queue.
            if self.inputQueue.not_empty:
                command = self.inputQueue.get()
                msg_type = command["msg_type"]
                if msg_type == MessageTypeID.OUTPUT_OVERRIDE_ID.value:
                    self.cfConnect.OverrideOuput(command)
                elif msg_type == MessageTypeID.SETPARAM_ID.value:
                    self.cfConnect.SetParam(command)
                elif msg_type == MessageTypeID.GETPARAM_ID.value:
                    self.cfConnect.GetParam(command, self.outputQueue)
                elif msg_type == MessageTypeID.GETLOGFILE_ID.value:
                    self.cfConnect.GetLogFile(command, self.outputQueue)
                elif msg_type == MessageTypeID.LOGBLOCKCOMMAND_ID.value:
                    self.cfConnect.LogBlockCommand(command)

            

if __name__ == '__main__':
    print(sys.argv)
    begin = True
    #If no radio channel is specified the program should end.
    if len(sys.argv) < 2:
        print("No radio channel specified")
        begin = False
    else:
        try:
            radio_channel = int(sys.argv[1])  
        except:
            print("Radio channel is not a number.")
            begin = False
    if begin:
        m = main()
        try:
            m.start(sys.argv[1])
        except KeyboardInterrupt:
            #Make sure that the disconnection from the crazyflie is handled if user inputs Ctrl+C
            m.cfConnect.disconnect()
    
    

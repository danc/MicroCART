from asyncore import write
import os, mmap
from queue import Queue

SHM_ADDR = 0x40000000
SHM_LENGTH = 0x1000

class MemoryHandler:


    def __init__(self):
        self.file = None
        self.memory = None
        self.shm_queue = Queue()

    def map_shm(self):
        try:
            self.file = os.open("/dev/mem", os.O_RDWR)
            self.memory = mmap.mmap(self.file, SHM_LENGTH, mmap.MAP_SHARED, access=mmap.ACCESS_WRITE, offset=SHM_ADDR)
        except:
            print("Could not mmap memory correctly")

    def write_shm(self, len, data, loc):
        if(len > SHM_LENGTH - 1):
            raise Exception
        self.memory.seek(loc)
        self.memory.write(data)
        

    def write_shm_byte(self, byte, loc):
        self.memory.seek(loc)
        self.memory.write_byte(byte)

    def shm_loop(self):
        print("starting loop")
        while not self.shm_queue.empty():
            wrtbytes = self.shm_queue.get()
            mem_clear = False
            while not mem_clear:
                self.memory.seek(0)
                if self.memory.read_byte() == 0x0:
                    print("mem clear")
                    mem_clear = True
            self.write_shm(len(wrtbytes), wrtbytes, 8)
            self.write_shm_byte(0xFF, 0)
            print("written")

        

#!/bin/bash

set -e

# Quad Hardware Tests
source /remote/Xilinx/2018.3/Vivado/2018.3/settings64.sh
(cd quad/ip_repo && make build-tests)

# Quad Libraries and Boot image
#(cd quad && make deep-clean && make)

# Ground station
#git submodule update --init --recursive
#(cd groundStation && make vrpn && make) # QT is breaking things right now

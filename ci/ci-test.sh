#!/bin/bash

set -e

# Quad HW
source /remote/Xilinx/2018.3/Vivado/2018.3/settings64.sh
(cd quad/ip_repo && make run-tests)

# Quad
(cd quad && make test)

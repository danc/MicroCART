# What's here?
This folder contains the scripts run by the CI pipeline on GitLab.
The [`.gitlab-ci.yml`](.gitlab-ci.yml) at the top level of the repo
specifies the two stages of the pipeline, __Build__ and __Test__.

## Build Stage
The build stage is defined by the [`ci-build.sh`](ci/ci-build.sh) file.
It runs [make](quad/Makefile) under the quad directory to build the project.

## Test Stage
The test stage is defined by the [`ci-test.sh`](ci/ci-test.sh) file.
It runs [make test](quad/Makefile#L36) under the quad directory to run the tests.

# CI Server
The CI server for this project is at `microcart.ece.iastate.edu`
login information should be handed off outside this repo for security reasons.
The gitlab runner uses docker to launch a container, in which the scripts are run.
The VM is running Red Hat Enterprise Linux, which as of Oct. 6, 2018 was licensed.
**Note:** RHEL uses `yum` for package management, but the docker image uses `apt-get`.

## Accessing System Folders
If you need to access any folders on the VM from within the docker,
the information [here](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#volumes-in-the-runners-docker-section) should help
Specifically this part:

### Example 2: mount a host directory as a data volume
In addition to creating a volume using you can also mount a directory from your
Docker daemon's host into a container. It's useful when you want to store builds
outside the container.

    [runners.docker]
      ... whatever config is already there ...
      volumes = ["/path/to/persistent/data/in/container", /path/to/bind/from/host:/path/to/bind/in/container:rw"]

## Accessing a remote share on the VM
To get to the Xilinx tools for hardware simulation, we need access to
`logic.ece.iastate.edu/export/Xilinx` as a mounted folder. This can be done with NFS.
NFS is installed and the folder mounted. If it becomes unmounted, then run 

    $ sudo mount logic.ece.iastate.edu:/export/Xilinx /remote/Xilinx

on the VM. This may not be doable from within the CI run as it is in a container (Docker image) that may behave differently or have different permissions.

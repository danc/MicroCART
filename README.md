[![pipeline status](https://git.ece.iastate.edu/danc/MicroCART/badges/master/pipeline.svg)](https://git.ece.iastate.edu/danc/MicroCART/commits/master)


# MicroCART 
_Microprocessor Controlled Aerial Robotics Team_

**Project Statement**: "To create a modular platform for research in controls and embedded systems."

Since 1998, MicroCART has been building aerial robots. Currently, we are modifying a small off the shelf
quad, [The Crazyflie 2.1](https://www.bitcraze.io/products/crazyflie-2-1/), to suit our lab environment 
where students explore embedded systems and control theory. 

This year, MicroCART has 3 areas of development:
- The **quadcopter firmware**
  - The quadcopter firmware is largely already written and supported well by Bitcraze,
    the creators of the Crazyflie. We are building on top of the stock firmware
    to introduce students to the code in a guided manner.
- The **ground station**
  - The ground station is responsible for issuing important data to the quad
    (like manual control and stabilization parameters) and receiving data from the quad 
    like battery level and orientation.
- The **test stand**
  - The test stand is a new bit of kit designed to hold the Crazyflie while testing stabilization
    parameters. It is designed to limit the quad to one axis of rotation at a time to isolate 
    testing.

## Folder Sections
- [Crazyflie Firmware](crazyflie_software/crazyflie-firmware-2021.06)
- [Ground Station](groundStation)
- [Test Stand](crazyflie_hardware)
- *Legacy*
    - [Controls](controls)  
    - [Quadcopter](quad/README.md)  


## [Documentation](../../wikis/home)
### [Search the wiki ->](https://git.ece.iastate.edu/search?group_id=631&project_id=740&repository_ref=&scope=wiki_blobs&snippets=false)
All documentation is maintained in the [repository wiki](../../wikis/home). This makes our documentation easily *searchable*.

- [Getting started](../../wikis/home)
- [Continuous Integration FAQ](../../wikis/CI-CD)  
- [How to document things on Gitlab](../../wikis/How-to-Document-Things-on-Gitlab)  
- [How to update the website](../../wikis/Website-Management)  
- [How to use Git](../../wikis/How-to-Use-Git)
- [How to Calibrate the Camera System](../../wikis/How-to-Calibrate-the-Camera-System)
- [Crazyflie Firmware Development](../../wikis/Crazyflie-Firmware/Crazyflie-Firmware-Development)
- *Legacy*
    - [How to demo the quadcopter](../../wikis/Legacy/How-to-Demo)  
    - [How to charge the LiPo batteries](../../wikis/Legacy/How-to-Charge-Lipo)  

# Stable Releases
To browse stable releases from previous teams, view the [tags page](https://git.ece.iastate.edu/danc/MicroCART/-/tags).

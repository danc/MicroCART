Title: Measuring Moment of Inertia
Date: 2016-10-25
Authors: Brendan
Category: Highlights
thumbnail: "/images/measuring_moi.jpg"

In order to develop our physical model of the quadcopter, we need to know the moment of inertia around all axes of the quadcopter. After some unreliable results from our lab ECP machine, we needed a new method to measure the moment of inertia, and we decided to resort to the methods used in our classical physics courses.

<a href="/images/measuring_moi.jpg">
<figure>
<img src="/images/measuring_moi.jpg">
</figure>
</a>

With a bizarre setup of clamps, we setup the quad to measure the MOIs of every axis. A simple mass-pully-string system provides a known torque on the turn-table, and the spinning of the turn-table is measured in time using an encoder. From this data, we were able to calculate the moment of inertia. Thanks to the Physics Deptartment for their help!

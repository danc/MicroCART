Title: A new and flexible way to compute
Date: 2017-02-01
Authors: Brendan
Category: Highlights
thumbnail: "/images/computation_graph.png"

The controls team wants to start tuning the controller on the quadcopter; they also want  to have the ability to modify the controller in order to better characterize each part. Currently, there is no way to do that except to re-write the code on the quad, which bleeds into their development cycle. David took the challenge to solve this issue, and developed the idea of a computation graph.

<a href="/images/computation_graph.png">
<figure>
<img src="/images/computation_graph.png">
</figure>
</a>

The idea is a typical graph data structure, specifically a directed acyclic multigraph with weighted edges. However, when normally the nodes in a graph represent some data, the nodes in the computation represent functions, and each edge represents a value that gets passed from the "output" of one node to the "input" of another node. Computing the whole graph is simply a matter of performing a depth-first-search algorithm, where the "inputs" of a node represent the node's children.
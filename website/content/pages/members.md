Title: Team Members
date: 2016-11-20
sortorder: 010

<image src="/images/GroupPicture.jpg" style="width: 100%; padding: 0; max-width: 600px">

From left to right: Eric, Brendan, Kris, Andy, Jake, Joe, Tara, David

## David Wehr
**Team Leader**  
Computer Engineer  
dawehr@iastate.edu  

## Tara Mina
**Communications Leader**  
Electrical Engineer  
tymina@iastate.edu  

## Jake Drahos
**Webmaster**  
Computer Engineer  
drahos@iastate.edu 

## Eric Middleton
**Hardward Maintainer**  
Electrical/Computer Engineer  
ericm@iastate.edu  

## Joe Bush
**Quadcopter Software Key Concept Holder**  
Computer Engineer  
jpbush@iastate.edu 

## Kris Burney
**Ground Station Key Concept Holder**  
Computer Engineer  
burneykb@iastate.edu  

## Andy Snawerdt
**Controls Systems Key Concept Holder**  
Electrical Engineer  
snawerdt@iastate.edu  

## Brendan Bartels
**Controls Software Key Concept Holder**  
Electrical Engineer  
bbartels@iastate.edu  

# Advisors

## Dr. Phillip Jones
## Dr. Nicola Elia

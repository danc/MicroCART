Title: About
url:
save_as: index.html
date: 2016-11-19
sortorder: 001

# MicroCART
## Microprocessor Controlled Aerial Robot Team

**Senior Design Group**: may1716

<img align="right" src="/images/quad_pic.png" width="40%">

MicroCART is an ongoing senior design project focused on the
development of a quadcopter as a research platform for controls and
embedded systems. This year, our team is responsible for advancing the
modular structure the platform, developing a controls model, and
improving the autonomous flight capabilities of the quadcopter.

Team members should consult the [Gitlab project page][1] for technical
documentation and instructions.

[1]: https://git.ece.iastate.edu/danc/MicroCART

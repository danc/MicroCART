Title: Simulink Model Update
Date: 2017-02-10
Authors: Brendan
Category: Highlights
thumbnail: "/images/model_pitch_rate.png"

Andy and Tara have been working to flesh out the Simulink model of our quadcopter. They have obtained the measurements they need to physically describe the model, now they are finishing up the Simulink model. Here is an example output of the pitch position controller.

<a href="/images/model_pitch_rate.png">
<figure>
<img src="/images/model_pitch_rate.png">
</figure>
</a>

This controller represents one of many controllers that we consider to be "inner loop" controllers. Right now, the inner loop controllers in the model are stable, like this pitch position controller. The controls team is now working through PID constants to determine a stable controller for the outer loop controllers.
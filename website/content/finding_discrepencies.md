Title: Tracking Down Discrepancies
Date: 2017-03-28
Authors: Brendan
Category: Highlights
thumbnail: "/images/discrepancies_bad.png"

David, Andy, and Tara have been trying to track down discrepancies between the quadcopter controller implementation and the Simulink model. With each discovered discrepancy, the model keeps getting better. Here is one example of where the quad code and model were performing the calculation for the complementary filter differently.

<a href="/images/discrepancies_bad.png">
<figure>
<img src="/images/discrepancies_bad.png">
</figure>
</a>

This is looking at the output of a simulation in the Simulink model, overlayed with actual logged data from a flight test of the quadcopter. After resolving the discrepency, the model predicts the behavior much more closely.

<a href="/images/discrepancies_good.png">
<figure>
<img src="/images/discrepancies_good.png">
</figure>
</a>
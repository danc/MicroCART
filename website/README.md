# Making Changes to the Website

## Setup Environment

Before making any changes, setup your environment with:

```
make setup
```

This will setup Pelican, so that you can build and deploy the website.
You should only have to run the above command once.

## Making changes

To make changes to the website, keep the following in mind:
- "Pages" go in `content/pages`. Pages are written in Markdown so use the .md
  extension
- Images go in `content/images`. An exmple of how to put an image in a page
  can be seen in `content/pages/home.md`.
- Files go in `content/files`. An example of how to put a file linke on a page
  can be seen in `conent/pages/documents.md`.

## Building the website

To build the website:

```
make build
```

## Deploying the website

Before deploying, ensure you have 2 files in the website root directory: the
`PASSWORD` file and the `TEAM` file. The `PASSWORD` file should contain the sftp
password that was emailed to you for your website. The `TEAM` file should contain
the team name, something like `mayXXXX` or `decXXXX`.

The `PASSWORD` and `TEAM` files should contain NO SPACES and NO NEWLINES.

When ready to deploy:
```
make deploy
```

#!/bin/bash


if [ ! -e PASSWORD ] || [ ! -e TEAM ]; then
    echo "

*****
ERROR: File PASSWORD or file TEAM is missing.
Create 2 files in the website root directory, one named PASSWORD and another
named TEAM, and put the sftp password and senior design team name in these
files, respectively. No spaces. No newlines.
(The SFTP password was given to us in an email. Just search 'sftp password' in
your CyMail and it will probably be first result. The senior design team
name is something like mayXXXX or decXXXX)
*****

"
    exit 1
fi

TEAM=$(cat TEAM)
PASSWORD=$(cat PASSWORD)
HOST=sdweb.ece.iastate.edu
USERNAME=$TEAM

chmod -R 755 output || exit 1
#cd output || exit 1

echo "----"
echo "sftp password (copy and paste this into the password prompt)"
echo $PASSWORD
echo "----"

sftp -o PubkeyAuthentication=false $USERNAME@$HOST <<EOF
put -r output/* www/
exit
EOF

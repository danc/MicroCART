from queue import Queue
import socket
import os
import collections
from enum import IntEnum, Enum

# Set the path for the Unix socket
socket_path = './cflib_groundstation.socket'

class GroundstationSocket():
    metadata = collections.namedtuple("metadata", ["msg_type", "msg_id", "data_len"])

    """
    Creates a TCP connection that will be connected to by the backend.
    """
    def groundstation_connect(self, inputQueue: Queue):
        # remove the socket file if it already exists
        try:
            os.unlink(socket_path)
        except OSError:
            if os.path.exists(socket_path):
                raise
        
        # Create the Unix socket server
        server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        print("Opened groundstation socket.")

        # Bind the socket to the path
        server.bind(socket_path)

        # Listen for incoming connections
        server.listen(1)

        # accept connections
        print('Server is listening for incoming connections...')
        self.connection, client_address = server.accept()

        try:
            print('Connection from', str(self.connection).split(", ")[0][-4:])

            # receive data from the client
            while True:
                data = self.connection.recv(1000)
                if not data:
                    break
                message = self.decodePacket(data, 256, len(data))
                if message == None:
                    print("There was an error decoding the packet")
                    break

                #Add the message to the main function queue, which will
                #which will send the appropriate command to the quad.
                inputQueue.put(message)
        finally:
            # close the connection
            self.connection.close()
            # remove the socket file
            os.unlink(socket_path)
    
    """
    Decodes the bytes that are coming in from backend and puts them into a struct for the other
    classes to use. Message Type determines which action should be performed
    """
    def decodePacket(self, data, data_size, packet_size):

        #Initial checks to ensure the packet is valid
        if data[PacketHeader.BEGIN.value] != Message.BEGIN_CHAR.value:
            return
        if len(data) < PacketHeader.HDR_SIZE.value + ChecksumFormat.CSUM_SIZE.value:
            return
        
        #Get the message type, id and data length
        messagedata = {
            "msg_type": int.from_bytes(data[PacketHeader.MTYPE_L.value:PacketHeader.MTYPE_H.value], 'big'),
            "msg_id": int.from_bytes(data[PacketHeader.ID_L.value:PacketHeader.ID_H.value], 'big'),
            "data_len": int.from_bytes(data[PacketHeader.DLEN_L.value:PacketHeader.DLEN_H.value], 'big'),
        }

        #Check if the size is correct
        if packet_size < PacketHeader.HDR_SIZE.value + messagedata["data_len"] + ChecksumFormat.CSUM_SIZE.value:
            return
        if data_size < messagedata["data_len"]:
            return

        #Get the checksum from the packet and then ensure it is correct
        checkSum = self.packetChecksum(data, PacketHeader.HDR_SIZE.value + messagedata["data_len"] + ChecksumFormat.CSUM_SIZE.value)
        if checkSum != data[PacketHeader.HDR_SIZE.value + messagedata["data_len"]]:
            return

        #Get the message data
        messagedata["data"] = data[PacketHeader.HDR_SIZE.value:PacketHeader.HDR_SIZE.value + messagedata["data_len"]]
        return messagedata

    #Computes the checksum for a packet's data
    def packetChecksum(self, data, packet_size):
        checkSum = 0
        for i in range(0, packet_size - ChecksumFormat.CSUM_SIZE.value):
            checkSum ^= data[i]
        return checkSum

    #Converts the message data dictionary into a byte array to be sent
    def EncodePacket(self, messagedata):
        bytedata = bytearray()
        bytedata += b"\xbe"        
        bytedata.append((messagedata["msg_type"].value & 0xFF))
        bytedata.append((messagedata["msg_type"].value >> 8) & 0xFF)
        bytedata.append(messagedata["msg_id"] & 0xFF)
        bytedata.append((messagedata["msg_id"] >> 8) & 0xFF)
        bytedata.append(messagedata["data_len"] & 0xFF)
        bytedata.append((messagedata["data_len"] >> 8) & 0xFF)
        bytedata += messagedata["data"]
        bytedata.append(self.packetChecksum(bytedata, PacketHeader.HDR_SIZE.value + messagedata["data_len"] + ChecksumFormat.CSUM_SIZE.value))
        return bytedata

    #Sends a packet over the socket to the backend
    def WriteToBackend(self, message):
        messagedata = self.EncodePacket(message)
        self.connection.send(messagedata)





#Enums necessary for packet decoding/encoding
class PacketHeader(IntEnum):
    BEGIN = 0
    MTYPE_L = 1
    MTYPE_H = 2
    ID_L = 3
    ID_H = 4
    DLEN_L = 5
    DLEN_H = 6
    HDR_SIZE = 7

class ChecksumFormat(IntEnum):
    CSUM_L = 0
    CSUM_SIZE = 1

class Message(Enum):
    BEGIN_CHAR = 0xBE
    END_CHAR = 0xED

# Enumeration of the data types that a callback function may use
# doubleType should get added here at some point
class DataType(IntEnum):
    floatType = 0
    intType = 0
    stringType = 2
"""
  Message type IDs used to know what kind omessagedata enum or you will break backwards compatibility.
  Add new message types in the slot between MAX_TYPE_ID and the one before it
  DO NOT change this enum without also updating the "MessageTypes" array
  in commands.c to match.
 """
class MessageTypeID(Enum):
    GETPACKETLOGS_ID = 2
    UPDATE_ID = 3
    BEGINUPDATE_ID = 4
    LOG_ID = 5
    LOG_END_ID = 6
    SETPARAM_ID = 7
    GETPARAM_ID = 8
    RESPPARAM_ID = 9
    SETSOURCE_ID = 10
    GETSOURCE_ID = 11
    RESPSOURCE_ID = 12
    GETOUTPUT_ID = 13
    RESPOUTPUT_ID = 14
    GETNODES_ID = 15
    RESPNODES_ID = 16
    ADDNODE_ID = 17
    RESPADDNODE_ID = 18
    OUTPUT_OVERRIDE_ID = 19
    SEND_RT_ID = 20
    #MAX_TYPE_ID = 21
    GETLOGFILE_ID = 21
    RESPLOGFILE_ID = 22
    LOGBLOCKCOMMAND_ID = 23





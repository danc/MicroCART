
from queue import Queue
from threading import Thread
from groundstation_socket import GroundstationSocket, MessageTypeID
from crazyflie_connection import CrazyflieConnection
import sys


class main():
    def __init__(self) -> None:

        #incoming and outgoing queues
        self.inputQueue = Queue()
        self.outputQueue = Queue()
        

    def start(self, radio_channel):
        print("starting cflib groundstation")
        gs = GroundstationSocket()

        #Begin the thread to handle incoming messages.
        self.inThread = Thread(target = gs.groundstation_connect, args = (self.inputQueue,))
        self.inThread.start()

        #Begin the thread to handle crazyflie communication.
        self.cfConnect = CrazyflieConnection()
        self.cfConnect.connect("radio://0/" + radio_channel + "/2M/E7E7E7E7E7")
        self.commandThread = Thread(target = self.processCommands)
        self.commandThread.start()
        while True:
            #If messages need to be sent to groundstation, pull them out of the outputQueue
            if self.outputQueue.not_empty:
                message = self.outputQueue.get()
                gs.WriteToBackend(message)

    def processCommands(self):
        while True:
            #Call the appropriate function based on the message type of the incoming message on the queue.
            if self.inputQueue.not_empty:
                command = self.inputQueue.get()
                msg_type = command["msg_type"]
                if msg_type == MessageTypeID.OUTPUT_OVERRIDE_ID.value:
                    self.cfConnect.OverrideOuput(command)
                elif msg_type == MessageTypeID.SETPARAM_ID.value:
                    self.cfConnect.SetParam(command)
                elif msg_type == MessageTypeID.GETPARAM_ID.value:
                    self.cfConnect.GetParam(command, self.outputQueue)
                elif msg_type == MessageTypeID.GETLOGFILE_ID.value:
                    self.cfConnect.GetLogFile(command, self.outputQueue)
                elif msg_type == MessageTypeID.LOGBLOCKCOMMAND_ID.value:
                    self.cfConnect.LogBlockCommand(command)

            

if __name__ == '__main__':
    print(sys.argv)
    begin = True
    #If no radio channel is specified the program should end.
    if len(sys.argv) < 2:
        print("No radio channel specified")
        begin = False
    else:
        try:
            radio_channel = int(sys.argv[1])  
        except:
            print("Radio channel is not a number.")
            begin = False
    if begin:
        m = main()
        try:
            m.start(sys.argv[1])
        except KeyboardInterrupt:
            #Make sure that the disconnection from the crazyflie is handled if user inputs Ctrl+C
            m.cfConnect.disconnect()
    
    
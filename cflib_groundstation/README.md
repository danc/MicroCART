## Crazyflie Groundstation using cflib
The crazyflie groundstation was rewritten by sdmay24-32 to replace the previous system of having a crazyflie adapter (written in C) and the crazyflie groundstation (also written in C). At the request of sdmay23, this was done in Python using cflib, a python library used for controlling crazyflie drones released by Bitcraze. 

**How to run cflib_groundstation:** 
In the cflib_groundstation folder, run:

    python3 main.py <desired radio channel>
  For instructions on how to run the backend and frontend after this, see [Building and Running](https://git.ece.iastate.edu/danc/MicroCART/-/wikis/Ground-Station/Building-and-Running)

## Overview of Components
A brief overview of the components of the system and their uses are described below:
**![](https://lh7-us.googleusercontent.com/RFuYI9nyKgl1jHaJObfGSoxznTWwdwATOuuVlorNIclnCmbnOZYOJCEixYqekcisGx6ieBp1U03wYYMgAyWQE5fvptTuwpid93YgTp8dmo14Kst4lGMY47MMPCe89aGc8cv6RRyva1Np)**
 **main**
main holds the input queue for incoming messages and the output queue for outgoing messages. When a message is pulled out of the input queue, it decides which function needs to be run to handle the action desired by the message.

**groundstation_socket**
groundstation_socket opens a socket for the backend to connect with, then it decodes incoming messages and places them on the input queue. It also takes messages off the output queue and encodes them to be sent to the backend on the socket.

**crazyflie_connection**
crazyflie_connection handles all drone communication. Each function roughly corresponds to a packet type.

 - connect(): Connect to the drone. Will raise an exception if another application already established a connection to the drone specified by the radio number.
 - disconnect(): Frees the connection to the drone.
 - OverrideOutput(): Send a new setpoint to the drone, with a time and a mode. See [Groundstation CLI](https://git.ece.iastate.edu/danc/MicroCART/-/wikis/groundstation/CLI)
 - SetParam(): Sets a specific drone parameter (Yaw Kp, Roll rate Ki, etc.) to specific value.
 - GetParam(): Given the id of a param, it returns the assoociated value for that parameter.
 - get_logging_toc(): Gets the full table of contents of all logging variable on the drone.
 - get_param_toc(): Gets the full table of contents of all parameters on the drone.
 - GetLogFile() and LogBlockCommand(): See [Groundstation CLI](https://git.ece.iastate.edu/danc/MicroCART/-/wikis/groundstation/CLI) for more details on functionality. The remainder of the functions are helper functions for these commands.

**LogfileHandler**
Although maybe not the most efficient system, all logging data and tocs are saved to file and then the frontend is sent a path to those files. This class handles the writing of those files. Whenever a logging point is received from the drone, it is placed into a queue. A line is written to the file periodically by pulling a certain number of items from the queue and checking if any variables are duplicated. If any variable is not represented, a Nan is placed in the file instead of a value.